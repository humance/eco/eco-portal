package de.humance.eco.model;

import com.liferay.portal.kernel.util.StringPool;

import de.humance.common.util.JsonUtil;

public class IpcEventMessage {
	private String property = StringPool.BLANK;
	private String refTable = StringPool.BLANK;
	private String refField = StringPool.BLANK;
	private String refRecord = StringPool.BLANK;
	private String oldValue = StringPool.BLANK;
	private String newValue = StringPool.BLANK;

	public IpcEventMessage() {

	}

	public IpcEventMessage(String property, String refTable, String refField,
			String refRecord, String oldValue, String newValue) {
		this.property = property;
		this.refTable = refTable;
		this.refField = refField;
		this.refRecord = refRecord;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getRefTable() {
		return refTable;
	}

	public void setRefTable(String refTable) {
		this.refTable = refTable;
	}

	public String getRefField() {
		return refField;
	}

	public void setRefField(String refField) {
		this.refField = refField;
	}

	public String getRefRecord() {
		return refRecord;
	}

	public void setRefRecord(String refRecord) {
		this.refRecord = refRecord;
	}

	public String getOldValue() {
		return oldValue;
	}

	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	public String getNewValue() {
		return newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	public String toString() {
		return JsonUtil.convertToJson(this);
	}

	public static IpcEventMessage fromString(String jsonObject) {
		return JsonUtil.convertToObject(jsonObject, IpcEventMessage.class);
	}
}
