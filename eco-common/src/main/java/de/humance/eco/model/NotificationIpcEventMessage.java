package de.humance.eco.model;

import com.liferay.portal.kernel.util.StringPool;

import de.humance.common.util.JsonUtil;

public class NotificationIpcEventMessage {
	private String event = StringPool.BLANK;

	public NotificationIpcEventMessage(String event) {
		this.event = event;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String toString() {
		return JsonUtil.convertToJson(this);
	}

	public static NotificationIpcEventMessage fromString(String jsonObject) {
		return JsonUtil.convertToObject(jsonObject,
				NotificationIpcEventMessage.class);
	}
}
