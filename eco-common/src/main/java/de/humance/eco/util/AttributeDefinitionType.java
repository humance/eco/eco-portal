package de.humance.eco.util;

public enum AttributeDefinitionType {
	STRING("String"),
	INTEGER("Integer"),
	DOUBLE("Double"),
	BOOLEAN("Boolean");

	private String caption = null;

	private AttributeDefinitionType(String caption) {
		this.caption = caption;
	}

	public String getCaption() {
		return caption;
	}
}
