package de.humance.eco.util;

public class CommonConstants {
	// Vaadin theme
	public static final String VAADIN_THEME_ECO = "eco";

	// IPC Events
	public static final String IPC_EVENT_LOCALE_CHANGED = "IPC_EVENT_LOCALE_CHANGED";
	public static final String IPC_EVENT_NEW_CURRENT_ORG = "IPC_EVENT_NEW_CURRENT_ORG";
	public static final String IPC_EVENT_UPDATE_LEARNING_STATUS = "IPC_EVENT_UPDATE_LEARNING_STATUS";
}
