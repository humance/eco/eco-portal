package de.humance.eco.util;

import com.vaadin.event.Action;

public enum ContextMenuAction {
	VIEW("View"),
	ADD("Add"),
	EDIT("Edit"),
	DELETE("Delete"),
	SET_AS_NEXT_OF_KIN("Set as Next of Kin"),
	RENAME("Rename");

	private String caption = null;
	private Action action = null;

	private ContextMenuAction(String caption) {
		this.caption = caption;
		this.action = new Action(caption);
	}

	public String getCaption() {
		return caption;
	}

	public Action getAction() {
		return action;
	}

}
