package de.humance.eco.util;

public class ExpandoConstants {
	public static final String OPEN_ID_CONNECT_ISSUER = "openIdConnectIssuer";
	public static final String OPEN_ID_CONNECT_SUBJECT = "openIdConnectSubject";
}
