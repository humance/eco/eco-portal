package de.humance.eco.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import com.liferay.portal.kernel.image.ImageToolUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;

public class ImageUtil {
	private static Log log = LogFactoryUtil.getLog(ImageUtil.class);

	public static StreamResource scale(StreamSource source, int height,
			int width, String filename) {
		final ByteArrayOutputStream os = new ByteArrayOutputStream(10240);
		BufferedImage bufferedImage = null;

		try {
			bufferedImage = ImageIO.read(source.getStream());
			bufferedImage = (BufferedImage) ImageToolUtil.scale(bufferedImage,
					100, 200);

			ImageIO.write(bufferedImage, "png", os);

			source = toStreamSource(os);

		} catch (IOException e) {
			log.error("could not get buffer");
			return null;
		}
		return new StreamResource(source, filename);
	}

	public static StreamSource toStreamSource(final ByteArrayOutputStream os) {
		return new StreamSource() {
			private static final long serialVersionUID = -4905654404647215809L;

			public InputStream getStream() {
				return new ByteArrayInputStream(os.toByteArray());
			}
		};
	}

	public static byte[] toByteArray(StreamSource resource) throws IOException {
		ByteArrayOutputStream os = new ByteArrayOutputStream(10240);
		BufferedImage bufferedImage = ImageIO.read(resource.getStream());
		ImageIO.write(bufferedImage, "png", os);
		return os.toByteArray();
	}
}
