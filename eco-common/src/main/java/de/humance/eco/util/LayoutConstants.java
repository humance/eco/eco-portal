package de.humance.eco.util;

public class LayoutConstants {
	// Liferay theme
	public static final String THEME_ID = "eistheme_WAR_eislfrtheme";
	public static final String THEME_DEFAULT_COLOR_SCHEME = "eis-default";

	// Page Layout
	public static final String TPL_ID_1_COLUMN = "1_column";

	// Layout columns
	public static final String COL_ID_1 = "column-1";

	// Portlet Ids
	public static final String PID_PROFILE_GENERAL = "eisgeneralprofileportlet_WAR_eisprofileportlet";
	public static final String PID_PROFILE_BASE_INFO = "eisbaseinfoprofileportlet_WAR_eisprofileportlet";
	public static final String PID_PROFILE_QUALIF = "eisqualifprofileportlet_WAR_eisprofileportlet";
	public static final String PID_PROFILE_CERT = "eiscertprofileportlet_WAR_eisprofileportlet";
	public static final String PID_PROFILE_AVAIL = "eisavailprofileportlet_WAR_eisprofileportlet";
	public static final String PID_PROFILE_TRAVEL = "eistravelprofileportlet_WAR_eisprofileportlet";
	public static final String PID_PROFILE_ASSIGN = "eisassignprofileportlet_WAR_eisprofileportlet";
	public static final String PID_PROFILE_PAYROLL = "eispayrollprofileportlet_WAR_eisprofileportlet";
	public static final String PID_PROFILE_MODIF_WATCHER = "eismodifwatcherportlet_WAR_eisprofileportlet";
	public static final String PID_CLEARING_RECORDS = "eisclearingrecordsportlet_WAR_eisprofileportlet";
	public static final String PID_SETTINGS = "eisgeneralsettingsportlet_WAR_eisprofileportlet";

	public static final String PID_COURSE_EXPLORER = "eiseducationexplorerportlet_WAR_eiseducationportlet";
	public static final String PID_COURSE_CREATION = "eisecmpcreatorportlet_WAR_eiseducationportlet";
	public static final String PID_COURSE_REVIEW = "eisecmpreviewerportlet_WAR_eiseducationportlet";
	public static final String PID_COURSE_PUBLISH = "eisecmppublisherportlet_WAR_eiseducationportlet";
	public static final String PID_EDUCATION_STATUS = "eiseducationdashboardportlet_WAR_eiseducationportlet";

	public static final String PID_FRIENDS = "eisfriendsportlet_WAR_eiscommunityportlet";
	public static final String PID_MESSAGING = "eismessagingportlet_WAR_eiscommunityportlet";
	public static final String PID_CALENDAR = "eiscalendarportlet_WAR_eiscommunityportlet";

	// URLs
	public static final String URL_WELCOME = "/web/guest/home";
	public static final String URL_IMPRINT = "/web/guest/imprint";
	public static final String URL_PRIVACY = "/web/guest/privacy";
	public static final String URL_TERMS_OF_USE = "/web/guest/terms-of-use";
	public static final String URL_CONTACT = "/web/guest/contact";
	public static final String URL_DASHBOARD = "/group/guest/dashboard";
	public static final String URL_PROFILE = "/group/guest/profile";
	public static final String URL_FRIENDS = "/group/guest/friends";
	public static final String URL_MESSAGING = "/group/guest/messaging";
	public static final String URL_CALENDAR = "/group/guest/calendar";
	public static final String URL_SHOP = "/group/guest/shop";
	public static final String URL_SETTINGS = "/group/guest/settings";
	public static final String URL_CLEARING = "/group/guest/clearing";
	public static final String URL_ORDER_HISTORY = "/group/guest/order-history";
	public static final String URL_SUPPORT = "/group/guest/support";

	public static final String URL_PROFILE_GENERAL = "/group/guest/profile#p_eisgeneralprofileportlet_WAR_eisprofileportlet";
	public static final String URL_PROFILE_BASE = "/group/guest/profile#p_eisbaseinfoprofileportlet_WAR_eisprofileportlet";
	public static final String URL_PROFILE_QUALIF = "/group/guest/profile#p_eisqualifprofileportlet_WAR_eisprofileportlet";
	public static final String URL_PROFILE_CERTS = "/group/guest/profile#p_eiscertprofileportlet_WAR_eisprofileportlet";
	public static final String URL_PROFILE_ASSIGN = "/group/guest/profile#p_eisavailprofileportlet_WAR_eisprofileportlet";
	public static final String URL_PROFILE_TRAVEL = "/group/guest/profile#p_eistravelprofileportlet_WAR_eisprofileportlet";
	public static final String URL_PROFILE_PLANNING = "/group/guest/profile#p_eisassignprofileportlet_WAR_eisprofileportlet";
	public static final String URL_PROFILE_PAYROLL = "/group/guest/profile#p_eispayrollprofileportlet_WAR_eisprofileportlet";

	public static final String URL_EDUCATION_COURSE_EXPLORER = "/group/guest/course-explorer";
	public static final String URL_EDUCATION_COURSE_CREATION = "/group/guest/content-creation";
	public static final String URL_EDUCATION_COURSE_REVIEW = "/group/guest/content-review";
	public static final String URL_EDUCATION_COURSE_PUBLISH = "/group/guest/content-publishing";
	public static final String URL_EDUCATION_COURSE_ASSIGNMENT = "/group/guest/course-assignment";

	// Friendly URLs
	public static final String FRIENDLY_URL_WELCOME = "/home";
	public static final String FRIENDLY_URL_IMPRINT = "/imprint";
	public static final String FRIENDLY_URL_PRIVACY = "/privacy";
	public static final String FRIENDLY_URL_TERMS_OF_USE = "/terms-of-use";
	public static final String FRIENDLY_URL_CONTACT = "/contact";
	public static final String FRIENDLY_URL_DASHBOARD = "/dashboard";
	public static final String FRIENDLY_URL_PROFILE = "/profile";
	public static final String FRIENDLY_URL_FRIENDS = "/friends";
	public static final String FRIENDLY_URL_MESSAGING = "/messaging";
	public static final String FRIENDLY_URL_CALENDAR = "/calendar";
	public static final String FRIENDLY_URL_SHOP = "/shop";
	public static final String FRIENDLY_URL_SETTINGS = "/settings";
	public static final String FRIENDLY_URL_CLEARING = "/clearing";
	public static final String FRIENDLY_URL_ORDER_HISTORY = "/order-history";
	public static final String FRIENDLY_URL_SUPPORT = "/support";

	public static final String FRIENDLY_URL_PROFILE_GENERAL = "/profile#p_eisgeneralprofileportlet_WAR_eisprofileportlet";
	public static final String FRIENDLY_URL_PROFILE_BASE = "/profile#p_eisbaseinfoprofileportlet_WAR_eisprofileportlet";
	public static final String FRIENDLY_URL_PROFILE_QUALIF = "/profile#p_eisqualifprofileportlet_WAR_eisprofileportlet";
	public static final String FRIENDLY_URL_PROFILE_CERTS = "/profile#p_eiscertprofileportlet_WAR_eisprofileportlet";
	public static final String FRIENDLY_URL_PROFILE_ASSIGN = "/profile#p_eisavailprofileportlet_WAR_eisprofileportlet";
	public static final String FRIENDLY_URL_PROFILE_TRAVEL = "/profile#p_eistravelprofileportlet_WAR_eisprofileportlet";
	public static final String FRIENDLY_URL_PROFILE_PLANNING = "/profile#p_eisassignprofileportlet_WAR_eisprofileportlet";
	public static final String FRIENDLY_URL_PROFILE_PAYROLL = "/profile#p_eispayrollprofileportlet_WAR_eisprofileportlet";

	public static final String FRIENDLY_URL_EDUCATION_STATUS = "/education-status";
	public static final String FRIENDLY_URL_COURSE_EXPLORER = "/education";
	public static final String FRIENDLY_URL_COURSE_CREATION = "/content-creation";
	public static final String FRIENDLY_URL_COURSE_REVIEW = "/content-review";
	public static final String FRIENDLY_URL_COURSE_PUBLISH = "/content-publishing";

	// Page names
	public static final String PAGE_NAME_WELCOME = "Welcome";
	public static final String PAGE_NAME_IMPRINT = "Imprint";
	public static final String PAGE_NAME_PRIVACY = "Privacy";
	public static final String PAGE_NAME_TERMS_OF_USE = "Terms of Use";
	public static final String PAGE_NAME_CONTACT = "Contact";
	public static final String PAGE_NAME_HOME = "Home";
	public static final String PAGE_NAME_EDUCATION = "Training";
	public static final String PAGE_NAME_COMMUNITY = "Community";
	public static final String PAGE_NAME_FRIENDS = "Friends";
	public static final String PAGE_NAME_MESSAGING = "Messaging";
	public static final String PAGE_NAME_CALENDAR = "Calendar";
	public static final String PAGE_NAME_SHOP = "Shop";
	public static final String PAGE_NAME_SETTINGS = "Settings";
	public static final String PAGE_NAME_CLEARING = "Clearing";
	public static final String PAGE_NAME_ORDER_HISTORY = "Order History";
	public static final String PAGE_NAME_SUPPORT = "Support";

	public static final String PAGE_NAME_PROFILE = "Profile";
	public static final String PAGE_NAME_PROFILE_GENERAL = "General";
	public static final String PAGE_NAME_PROFILE_BASE = "Base Information";
	public static final String PAGE_NAME_PROFILE_QUALIF = "Qualifications";
	public static final String PAGE_NAME_PROFILE_CERTS = "Certificates";
	public static final String PAGE_NAME_PROFILE_ASSIGN = "Assignments";
	public static final String PAGE_NAME_PROFILE_TRAVEL = "Travel";
	public static final String PAGE_NAME_PROFILE_PLANNING = "Planning";
	public static final String PAGE_NAME_PROFILE_PAYROLL = "Payroll";

	public static final String PAGE_NAME_EDUCATION_STATUS = "Learning Status";
	public static final String PAGE_NAME_COURSE_EXPLORER = "Course Explorer";
	public static final String PAGE_NAME_COURSE_CREATION = "Content Creation";
	public static final String PAGE_NAME_COURSE_REVIEW = "Content Review";
	public static final String PAGE_NAME_COURSE_PUBLISH = "Content Publishing";
}
