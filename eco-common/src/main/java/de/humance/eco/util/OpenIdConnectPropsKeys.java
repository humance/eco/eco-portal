package de.humance.eco.util;

public interface OpenIdConnectPropsKeys {
	public static final String ISSUER_ID = "openidconnect.issuer.id";
	public static final String ISSUER_NAME = "openidconnect.issuer.name";
	public static final String ISSUER_LOGO_URL = "openidconnect.issuer.logo.url";
	public static final String CLIENT_ID = "openidconnect.client.id";
	public static final String CLIENT_SECRET = "openidconnect.client.secret";
	public static final String ENABLED = "openidconnect.auth.enabled";
	public static final String GRAPH_URL = "openidconnect.graph.url";
	public static final String AUTH_URL = "openidconnect.auth.url";
	public static final String TOKEN_URL = "openidconnect.token.url";
	public static final String VERIFIED_ACCOUNT_REQUIRED = "openidconnect.verified.account.required";
	public static final String REVOKE_URL = "openidconnect.revoke.url";
	public static final String AUTH_SCOPE = "openidconnect.scope";
	public static final String NONCE_ENABLED = "openidconnect.nonce.enabled";
}
