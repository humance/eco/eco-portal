package de.humance.eco.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;

import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Address;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.User;
import com.liferay.portal.service.AddressLocalServiceUtil;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;

import de.humance.common.util.ListTypeUtil;
import de.humance.eco.model.UserInfo;

public class OpenIdConnectUtil {
	public static final String EXPANDO_SUBJECT = "openIdConnectSubject";
	public static final String EXPANDO_ISSUER = "openIdConnectIssuer";
	public static final String EXPANDO_ACCESS_TOKEN = "openIdConnectAccessToken";
	private static Log log = LogFactoryUtil.getLog(OpenIdConnectUtil.class);
	private static SimpleDateFormat dateFormatter = new SimpleDateFormat(
			"yyyy-MM-dd");

	public static void updateUserProfile(User user) throws Exception {
		if (user == null) {
			return;
		}

		try {
			log.debug("Updating IDP profile for User with id "
					+ user.getUserId());
			Credential cred = new Credential.Builder(
					BearerToken.authorizationHeaderAccessMethod())
					.setTransport(new NetHttpTransport())
					.setJsonFactory(new JacksonFactory()).build()
					.setAccessToken(getAccessToken(user));

			GenericUrl url = new GenericUrl(getIssuerId(user.getCompanyId())
					+ "/api/users/" + getSubject(user));
			HttpContent content = new JsonHttpContent(new JacksonFactory(),
					getInfo(user));
			cred.getTransport().createRequestFactory()
					.buildPutRequest(url, content).setInterceptor(cred)
					.execute();
			log.info("IDP profile for User with id " + user.getUserId()
					+ " updated");
		} catch (Exception e) {
			log.error(
					"Could not update IDP profile for User with id "
							+ user.getUserId(), e);
			throw new Exception(
					"Could not update IDP profile for User with id "
							+ user.getUserId(), e);
		}
	}

	private static UserInfo getInfo(User user) throws PortalException,
			SystemException {
		UserInfo info = new UserInfo();
		info.setGivenName(user.getFirstName());
		info.setMiddleName(user.getMiddleName());
		info.setFamilyName(user.getLastName());
		info.set("nickname", user.getScreenName());
		String gender = "male";
		if (user.isFemale()) {
			gender = "female";
		}
		info.setGender(gender);
		info.set("birthdate", dateFormatter.format(user.getBirthday()));
		Address address = getAddress(user);
		if (address != null) {
			info.set("postal_code", address.getZip());
			info.set("locality", address.getCity());
			info.set("country", address.getCountry().getA2());
		} else {
			info.set("postal_code", StringPool.BLANK);
			info.set("locality", StringPool.BLANK);
			info.set("country", StringPool.BLANK);
		}
		info.set("locale", user.getLocale().getLanguage());
		return info;
	}

	private static Address getAddress(User user) {
		List<Address> availableAddresses = null;
		try {
			availableAddresses = AddressLocalServiceUtil.getAddresses(
					user.getCompanyId(), Contact.class.getName(),
					user.getContactId());
		} catch (Exception e) {
			log.error(
					"Could not get Addresses for User with id "
							+ user.getUserId(), e);
		}
		if (availableAddresses == null) {
			log.warn("No Address found for User with id " + user.getUserId());
			return null;
		}

		for (Address address : availableAddresses) {
			if (address.getTypeId() == ListTypeUtil.getInstance()
					.getContactAddressPersonal().getListTypeId()) {
				return address;
			}
		}
		log.warn("No "
				+ ListTypeUtil.getInstance().getContactAddressPersonal()
						.getName() + " Address found for User with id "
				+ user.getUserId());
		return null;
	}

	public static String getSubject(User user) {
		return getExpando(user, EXPANDO_SUBJECT);
	}

	public static String getIssuer(User user) {
		return getExpando(user, EXPANDO_ISSUER);
	}

	public static String getAccessToken(User user) {
		return getExpando(user, EXPANDO_ACCESS_TOKEN);
	}

	private static String getExpando(User user, String expandoColumn) {
		if (user == null) {
			return null;
		}

		try {
			ExpandoValue value = ExpandoValueLocalServiceUtil.getValue(
					user.getCompanyId(), User.class.getName(),
					ExpandoTableConstants.DEFAULT_TABLE_NAME, expandoColumn,
					user.getUserId());
			if (value != null) {
				return value.getData();
			}
		} catch (Exception e) {
			log.error("Could not get " + expandoColumn
					+ " ExpandoValue for User with id " + user.getUserId(), e);
		}
		return null;
	}

	public static String getIssuerId(long companyId) throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.ISSUER_ID);
	}

	public static String getIssuerName(long companyId) throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.ISSUER_NAME);
	}

	public static String getIssuerLogoUrl(long companyId)
			throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.ISSUER_LOGO_URL);
	}

	public static String getAccessTokenURL(long companyId)
			throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.TOKEN_URL);
	}

	public static String getClientId(long companyId) throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.CLIENT_ID);
	}

	public static String getClientSecret(long companyId) throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.CLIENT_SECRET);
	}

	public static String getAuthURL(long companyId) throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.AUTH_URL);
	}

	public static String getGraphURL(long companyId) throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.GRAPH_URL);
	}

	public static Collection<String> getProfileScope(long companyId)
			throws SystemException {
		String[] stringArray = PrefsPropsUtil.getStringArray(companyId,
				OpenIdConnectPropsKeys.AUTH_SCOPE, StringPool.COMMA);

		return ListUtil.fromArray(stringArray);
	}

	public static boolean isEnabled(long companyId) throws SystemException {
		return PrefsPropsUtil.getBoolean(companyId,
				OpenIdConnectPropsKeys.ENABLED);
	}

	public static boolean isVerifiedAccountRequired(long companyId)
			throws SystemException {
		return PrefsPropsUtil.getBoolean(companyId,
				OpenIdConnectPropsKeys.VERIFIED_ACCOUNT_REQUIRED);
	}

	public static boolean isNonceEnabled(long companyId) throws SystemException {
		return PrefsPropsUtil.getBoolean(companyId,
				OpenIdConnectPropsKeys.NONCE_ENABLED);
	}

	public static String getRevokeURL(long companyId) throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				OpenIdConnectPropsKeys.REVOKE_URL);
	}

	public static void revokeAccess(long companyId, String accessToken)
			throws SystemException, IOException {
		Credential cred = new Credential.Builder(
				BearerToken.authorizationHeaderAccessMethod())
				.setTransport(new NetHttpTransport())
				.setJsonFactory(new JacksonFactory()).build()
				.setAccessToken(accessToken);
		revokeAccess(companyId, cred);
	}

	public static void revokeAccess(long companyId, Credential credentials)
			throws SystemException, IOException {
		String revokeUrlStr = getRevokeURL(companyId);

		if (Validator.isNull(revokeUrlStr))
			return;

		GenericUrl revokeUrl = new GenericUrl(revokeUrlStr);
		revokeUrl.set("token", credentials.getAccessToken());

		credentials
				.getTransport()
				.createRequestFactory(credentials)
				.buildGetRequest(revokeUrl)
				.setParser(
						credentials.getJsonFactory().createJsonObjectParser())
				.execute();
	}
}
