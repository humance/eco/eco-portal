package de.humance.eco.util;

public enum SocialRelationStatus {
	ACTIVE("Active"), ARCHIVED("Archived");

	private String caption = null;

	private SocialRelationStatus(String caption) {
		this.caption = caption;
	}

	public String getCaption() {
		return caption;
	}
}
