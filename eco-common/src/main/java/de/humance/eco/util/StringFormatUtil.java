package de.humance.eco.util;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

public class StringFormatUtil {
	private static Log log = LogFactoryUtil.getLog(StringFormatUtil.class);

	public static String getFormatedStringForMaxTwoLines(String stringToFormat,
			int numberOfLettersOneLine, int numberOfLettersTwoLines) {
		// CASES:
		// < numberOfLettersOneLine : nothing to do
		// > numberOfLettersTwoLines : linebreak and smaller fontsize. each row
		// can hold numberOfLettersTwoLines M's
		// > numberOfLettersTwoLines and no " " in first 19 places:
		// add " " between numberOfLettersTwoLines and
		// numberOfLettersTwoLines+1
		// > 2*numberOfLettersTwoLines: take string from 0 -
		// 2*numberOfLettersTwoLines and add ...

		stringToFormat = stringToFormat.trim();
		boolean hasWhiteSpace = stringToFormat.contains(" ");
		boolean stringValidator = false;
		int whiteSpaceDevider = 0;

		if (stringToFormat.length() > numberOfLettersOneLine) {

			if (stringToFormat.length() > numberOfLettersTwoLines) {
				// CASE > numberOfLettersTwoLines and no WhiteSpaces
				if (!hasWhiteSpace) {
					stringToFormat = insertWhiteSpaceAtPosition(stringToFormat,
							numberOfLettersTwoLines);
					// CASE > numberOfLettersOneLine and WhiteSpaces
				} else {
					// gathering whiteSpace positions
					List<Integer> whiteSpaces = new ArrayList<Integer>();
					int lastFound = 0;
					for (int i = 0; i < stringToFormat.length(); i++) {
						int whiteSpace = stringToFormat.indexOf(" ", i);
						if (whiteSpace != -1 && whiteSpace != lastFound) {
							whiteSpaces.add(whiteSpace);
							lastFound = whiteSpace;
						}
					}
					// if there is a whiteSpace, which devides String correctly
					// do nothing
					if (whiteSpaces.size() > 0) {
						for (int i = whiteSpaces.size(); i > 0; i--) {
							String firstPart = stringToFormat.substring(0,
									whiteSpaces.get(i - 1));
							if (firstPart.length() <= numberOfLettersTwoLines) {
								whiteSpaceDevider = whiteSpaces.get(i - 1);
								stringValidator = true;
								break;
							}
						}
					}
					// if not, insert whitespace at position
					// numberOfLettersTwoLines
					if (!stringValidator) {
						stringToFormat = insertWhiteSpaceAtPosition(
								stringToFormat, numberOfLettersTwoLines);
					}
				}
			}
		}

		// if text does not match, shorten the string (depending of previously
		// detected whitespaces)
		if (!stringValidator) {
			if (stringToFormat.length() > (2 * numberOfLettersTwoLines)) {
				stringToFormat = stringToFormat.substring(0,
						(2 * numberOfLettersTwoLines));
				stringToFormat = stringToFormat.concat("...");
			}
		} else {
			String firstPart = stringToFormat.substring(0, whiteSpaceDevider);
			String secondPart = stringToFormat.substring(whiteSpaceDevider,
					stringToFormat.length());
			if (secondPart.length() > numberOfLettersTwoLines) {
				secondPart = secondPart.substring(0, numberOfLettersTwoLines);
				secondPart = secondPart.concat("...");
				stringToFormat = firstPart.concat(secondPart);
			}
		}
		log.debug("String formatted : " + stringToFormat);
		return stringToFormat;
	}

	private static String insertWhiteSpaceAtPosition(String string, int position) {
		String firstPart = string.substring(0, position);
		String secondPart = string.substring(position, string.length());
		string = firstPart.concat(StringPool.BLANK).concat(secondPart);
		return string;
	}
}
