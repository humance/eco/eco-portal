package de.humance.eco.util;

import java.util.List;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;

public class UserUtil {
	private static Log log = LogFactoryUtil.getLog(UserUtil.class);

	public static User getUserByOpenId(long companyId, String openIdIssuer,
			String openIdSubject) {
		List<ExpandoValue> subjectValues = null;
		try {
			subjectValues = ExpandoValueLocalServiceUtil.getColumnValues(
					companyId, User.class.getName(),
					ExpandoTableConstants.DEFAULT_TABLE_NAME,
					ExpandoConstants.OPEN_ID_CONNECT_SUBJECT, openIdSubject,
					QueryUtil.ALL_POS, QueryUtil.ALL_POS);
		} catch (Exception e) {
			log.error("Could not get \""
					+ ExpandoConstants.OPEN_ID_CONNECT_SUBJECT
					+ "\" ExpandoValues with data=\"" + openIdSubject + "\"", e);
			return null;
		}

		User user = null;
		for (ExpandoValue subjectValue : subjectValues) {
			try {
				String issuer = (String) ExpandoValueLocalServiceUtil.getData(
						companyId, User.class.getName(),
						ExpandoTableConstants.DEFAULT_TABLE_NAME,
						ExpandoConstants.OPEN_ID_CONNECT_ISSUER,
						subjectValue.getClassPK());
				if (openIdIssuer.equals(issuer)) {
					user = UserLocalServiceUtil.getUserById(companyId,
							subjectValue.getClassPK());
					break;
				}
			} catch (Exception e) {
				log.error(
						"Could not get \""
								+ ExpandoConstants.OPEN_ID_CONNECT_ISSUER
								+ "\" ExpandoValue for classPK="
								+ subjectValue.getClassPK(), e);
				continue;
			}
		}
		return user;
	}
	
	public static String getOpenIdFromUser (long userId){
		long companyId = PortalUtil.getDefaultCompanyId();
		ExpandoValue expandoValue = null;
		try {
			expandoValue = ExpandoValueLocalServiceUtil.getValue(companyId,
					User.class.getName(),
					ExpandoTableConstants.DEFAULT_TABLE_NAME,
					ExpandoConstants.OPEN_ID_CONNECT_SUBJECT, userId);
		} catch (Exception e) {
			log.error("Could not get ExpandoValue from column \""
					+ ExpandoConstants.OPEN_ID_CONNECT_SUBJECT + "\" for User with id "
					+ userId, e);
		}
		if (expandoValue == null) {
			return StringPool.BLANK;
		}

		return expandoValue.getData();
	}
}
