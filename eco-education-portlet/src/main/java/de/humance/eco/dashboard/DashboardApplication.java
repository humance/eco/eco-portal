package de.humance.eco.dashboard;

import com.vaadin.annotations.Theme;

import de.humance.eco.util.CommonConstants;
import de.humance.education.dashboard.BaseDashboardApplication;
import de.humance.education.util.CourseType;

@Theme(CommonConstants.VAADIN_THEME_ECO)
public class DashboardApplication extends BaseDashboardApplication {
	private static final long serialVersionUID = -6199907894521417955L;

	@Override
	public CourseType getCourseType() {
		return CourseType.COURSE;
	}
}
