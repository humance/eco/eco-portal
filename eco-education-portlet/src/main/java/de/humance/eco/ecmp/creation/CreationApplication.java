package de.humance.eco.ecmp.creation;

import com.vaadin.annotations.Theme;

import de.humance.eco.util.CommonConstants;
import de.humance.education.ecmp.creation.BaseCreationApplication;
import de.humance.education.util.CourseType;

@Theme(CommonConstants.VAADIN_THEME_ECO)
public class CreationApplication extends BaseCreationApplication {
	private static final long serialVersionUID = 354852951094885922L;

	@Override
	public CourseType getCourseType() {
		return CourseType.COURSE;
	}
}