package de.humance.eco.ecmp.publishing;

import com.vaadin.annotations.Theme;

import de.humance.eco.util.CommonConstants;
import de.humance.education.ecmp.publishing.BasePublishingApplication;
import de.humance.education.util.CourseType;

@Theme(CommonConstants.VAADIN_THEME_ECO)
public class PublishingApplication extends BasePublishingApplication {
	private static final long serialVersionUID = 4320752416118022904L;

	@Override
	public CourseType getCourseType() {
		return CourseType.COURSE;
	}
}