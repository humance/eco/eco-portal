package de.humance.eco.ecmp.review;

import com.vaadin.annotations.Theme;

import de.humance.eco.util.CommonConstants;
import de.humance.education.ecmp.review.BaseReviewApplication;
import de.humance.education.util.CourseType;

@Theme(CommonConstants.VAADIN_THEME_ECO)
public class ReviewApplication extends BaseReviewApplication {
	private static final long serialVersionUID = 3934703203821703194L;

	@Override
	public CourseType getCourseType() {
		return CourseType.COURSE;
	}
}