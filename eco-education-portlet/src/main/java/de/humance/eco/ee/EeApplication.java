package de.humance.eco.ee;

import com.vaadin.annotations.Theme;

import de.humance.eco.util.CommonConstants;
import de.humance.education.ee.BaseEeApplication;
import de.humance.education.util.CourseType;

@Theme(CommonConstants.VAADIN_THEME_ECO)
public class EeApplication extends BaseEeApplication {
	private static final long serialVersionUID = -8648561170165223796L;

	@Override
	public CourseType getCourseType() {
		return CourseType.COURSE;
	}

}
