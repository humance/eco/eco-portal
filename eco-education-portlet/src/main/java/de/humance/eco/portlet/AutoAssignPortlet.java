package de.humance.eco.portlet;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

import de.humance.eco.util.LayoutConstants;
import de.humance.education.model.Course;
import de.humance.education.model.LearningProfile;
import de.humance.education.service.CourseLocalServiceUtil;
import de.humance.education.service.LearningProfileLocalServiceUtil;
import de.humance.education.util.CourseStatus;
import de.humance.education.util.EducationAuditEventPublisher;

public class AutoAssignPortlet extends MVCPortlet {
	private Log log = LogFactoryUtil.getLog(getClass());

	@Override
	public void render(RenderRequest renderRequest,
			RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil
				.getHttpServletRequest(renderRequest);
		HttpServletRequest origRequest = PortalUtil
				.getOriginalServletRequest(request);
		String courseIdParam = origRequest.getParameter("courseId");
		if (Validator.isNull(courseIdParam)) {
			log.debug("No courseId parameter found");
			SessionErrors.add(renderRequest, "no-course-id-found");
		} else {
			associateCourseAndLearningProfile(renderRequest, courseIdParam);
		}

		// Set the redirection URL
		String redirectUrl = PortalUtil.getPortalURL(null,
				PortalUtil.getPortalPort(false), false)
				+ LayoutConstants.URL_EDUCATION_COURSE_EXPLORER;
		if (Validator.isNotNull(courseIdParam)) {
			redirectUrl = redirectUrl + StringPool.POUND + "courseId="
					+ courseIdParam;
		}
		renderRequest.setAttribute("redirectUrl", redirectUrl);

		super.render(renderRequest, renderResponse);
	}

	private void associateCourseAndLearningProfile(RenderRequest renderRequest,
			String courseIdParam) {
		// Get the course id
		long courseId = 0;
		try {
			courseId = Long.parseLong(courseIdParam);
		} catch (Exception e) {
			log.error("Could not parse courseId to long: " + courseIdParam, e);
		}
		if (courseId <= 0) {
			SessionErrors.add(renderRequest, "no-course-id-found");
			return;
		}

		// Check if the course exists
		renderRequest.setAttribute("courseId", courseId);
		Course course = null;
		try {
			course = CourseLocalServiceUtil.getCourse(courseId);
		} catch (Exception e) {
			log.error("Could not get Course with id " + courseId, e);
		}
		if (course == null) {
			SessionErrors.add(renderRequest, "no-course-found");
			return;
		}

		CourseStatus status = CourseStatus.valueOf(course.getStatus());
		if (status != CourseStatus.PUBLISHED && status != CourseStatus.ARCHIVED) {
			log.debug("Course with id " + courseId
					+ " is not published or archived");
			SessionErrors.add(renderRequest, "no-course-found");
			return;
		}

		// Get the current user
		User user = null;
		try {
			user = PortalUtil.getUser(renderRequest);
		} catch (Exception e) {
			log.error("Could not get User from HttpServletRequest", e);
		}
		if (user == null) {
			SessionErrors.add(renderRequest, "no-user-found");
			return;
		}

		// Get the associated learning profile
		LearningProfile profile = null;
		try {
			profile = LearningProfileLocalServiceUtil.findByUserId(user
					.getUserId());
		} catch (Exception e) {
			log.error(
					"Could not find LearningProfile for User with id "
							+ user.getUserId(), e);
		}
		if (profile == null) {
			log.debug("No LearningProfile found for current User");
			SessionErrors.add(renderRequest, "no-learning-profile-found");
			return;
		}

		// Associate the course and the learning profile
		renderRequest.setAttribute("learningProfileId",
				profile.getLearningProfileId());

		boolean isAlreadyAssigned = false;
		try {
			isAlreadyAssigned = LearningProfileLocalServiceUtil.containsCourse(
					profile.getLearningProfileId(), courseId);
		} catch (Exception e) {
			log.error(
					"Could not check if Course with id " + courseId
							+ " is already assigned to User with id "
							+ profile.getUserId(), e);
			SessionErrors.add(renderRequest, "no-learning-profile-found");
			return;
		}
		if (isAlreadyAssigned) {
			log.debug("Course with id " + courseId
					+ " is already assigned to User with id "
					+ profile.getUserId());
			return;
		}

		try {
			LearningProfileLocalServiceUtil.assignCourse(
					profile.getLearningProfileId(), courseId);
			log.debug("Course with id " + courseId
					+ " and LearningProfile with id "
					+ profile.getLearningProfileId() + " are now associated");

			// Audit event
			EducationAuditEventPublisher.publishCourseAssignedEvent(
					PortalUtil.getCompanyId(renderRequest),
					profile.getUserId(), profile.getFirstName()
							+ StringPool.SPACE + profile.getLastName(), course);

		} catch (Exception e) {
			log.error(
					"Could not assign Course with id " + courseId
							+ " to LearningProfile with id "
							+ profile.getLearningProfileId(), e);
			SessionErrors.add(renderRequest, "association-failed");
		}
	}
}
