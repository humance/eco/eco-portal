<%@ include file="../init.jsp"%>

<%
Long courseId = (Long) renderRequest.getAttribute("courseId");
Long learningProfileId = (Long) renderRequest.getAttribute("learningProfileId");
String redirectUrl = (String) renderRequest.getAttribute("redirectUrl");
%>

<liferay-ui:error key="no-course-id-found" message="Course id parameter not found" />
<liferay-ui:error key="no-course-found" message="No course found with the specified id" />
<liferay-ui:error key="no-user-found" message="No user found" />
<liferay-ui:error key="no-learning-profile-found" message="No learning profile found for current user" />
<liferay-ui:error key="association-failed" message="Could not associate Course and LearningProfile" />

<div class="container-fluid well">
	<div class="row-fluid">
		<h3 class="span12 text-center">Please wait while we are preparing your course...</h3>
	</div>
	<div class="row-fluid">
		<div class="span12 text-center">
			<a href="<%= redirectUrl %>">If you are not redirected automatically, click here to get redirected.</a>
		</div>
	</div>
</div>
<script>
setTimeout(function() {
	window.location.replace("<%= redirectUrl %>");
}, 2500);
</script>
