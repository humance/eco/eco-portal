package de.humance.eco.portlet;

import com.vaadin.annotations.Theme;

import de.humance.eco.util.CommonConstants;
import de.humance.eco.view.BaseInfoProfileView;
import de.humance.vaadin.portlet.BootstrapApplication;

@Theme(CommonConstants.VAADIN_THEME_ECO)
public class BaseInfoProfileApplication extends BootstrapApplication {
	private static final long serialVersionUID = -6673631081967373170L;

	@SuppressWarnings("rawtypes")
	@Override
	public Class getDefaultView() {
		return BaseInfoProfileView.class;
	}

}