package de.humance.eco.portlet;

import com.vaadin.annotations.Theme;

import de.humance.eco.util.CommonConstants;
import de.humance.eco.view.GeneralProfileView;
import de.humance.vaadin.portlet.BootstrapApplication;

@Theme(CommonConstants.VAADIN_THEME_ECO)
public class GeneralProfileApplication extends BootstrapApplication {
	private static final long serialVersionUID = 5277978935984408379L;

	@SuppressWarnings("rawtypes")
	@Override
	public Class getDefaultView() {
		return GeneralProfileView.class;
	}

}