package de.humance.eco.presenter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.Address;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.Country;
import com.liferay.portal.model.ListType;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.Phone;
import com.liferay.portal.model.User;
import com.liferay.portal.model.Website;
import com.liferay.portal.service.AddressLocalServiceUtil;
import com.liferay.portal.service.CountryServiceUtil;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.PhoneLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.WebsiteLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

import de.humance.common.util.ListTypeUtil;
import de.humance.eco.portlet.BaseInfoProfileApplication;
import de.humance.eco.util.BaseInfoProfileField;
import de.humance.eco.util.OpenIdConnectUtil;
import de.humance.eco.view.BaseInfoProfileView;
import de.humance.vaadin.presenter.BaseViewPresenter;

public class BaseInfoProfileViewPresenter extends
		BaseViewPresenter<BaseInfoProfileView, BaseInfoProfileApplication> {
	private static final long serialVersionUID = 6673060224607930288L;
	private Log log = LogFactoryUtil.getLog(getClass());
	private ServiceContext serviceContext = null;

	public BaseInfoProfileViewPresenter(BaseInfoProfileView view,
			BaseInfoProfileApplication application) {
		super(view, application);
	}

	public ServiceContext getServiceContext() {
		if (serviceContext == null) {
			serviceContext = new ServiceContext();
			serviceContext.setScopeGroupId(application.getGroup().getGroupId());
		}

		return serviceContext;
	}

	private Address getAddress() {
		Address primaryAddress = null;
		List<Address> availableAddresses = null;
		try {
			availableAddresses = AddressLocalServiceUtil.getAddresses(
					application.getCompany().getCompanyId(), Contact.class
							.getName(), application.getUser().getContactId());
		} catch (Exception e) {
			log.error("Could not get Addresses for User with id "
					+ application.getUser().getUserId(), e);
		}
		if (availableAddresses != null && !availableAddresses.isEmpty()) {
			// Look up for the primary address
			for (Address address : availableAddresses) {
				if (address.getTypeId() == ListTypeUtil.getInstance()
						.getContactAddressPersonal().getListTypeId()) {
					primaryAddress = address;
					break;
				}
			}

		} else {
			log.warn("No Address found for User with id "
					+ application.getUser().getUserId());
		}

		if (primaryAddress == null) {
			// CASE : User has no address, create a new one and use it as
			// primary
			try {
				log.info("Adding Primary Address for User with id "
						+ application.getUser().getUserId());
				primaryAddress = AddressLocalServiceUtil.addAddress(application
						.getUser().getUserId(), Contact.class.getName(),
						application.getUser().getContactId(), StringPool.DASH,
						StringPool.DASH, StringPool.DASH, StringPool.DASH,
						StringPool.DASH, (long) 0, (long) 0, ListTypeUtil
								.getInstance().getContactAddressPersonal()
								.getListTypeId(), false, true,
						getServiceContext());
				log.info("Primary Address for User with id "
						+ application.getUser().getUserId() + " added");
			} catch (Exception e1) {
				log.error("Could not add Primary Address for User with id "
						+ application.getUser().getUserId(), e1);
			}
		}

		return primaryAddress;
	}

	public String getStreet1() {
		Address address = getAddress();
		if (address == null) {
			return StringPool.BLANK;
		}
		return address.getStreet1();
	}

	public String getStreet2() {
		Address address = getAddress();
		if (address == null) {
			return StringPool.BLANK;
		}
		return address.getStreet2();
	}

	public String getStreet3() {
		Address address = getAddress();
		if (address == null) {
			return StringPool.BLANK;
		}
		return address.getStreet3();
	}

	public String getCity() {
		Address address = getAddress();
		if (address == null) {
			return StringPool.BLANK;
		}
		return address.getCity();
	}

	public String getZip() {
		Address address = getAddress();
		if (address == null) {
			return StringPool.BLANK;
		}
		return address.getZip();
	}

	public List<Country> getAvailableCountries() {
		try {
			return CountryServiceUtil.getCountries();
		} catch (Exception e) {
			log.error("Could not get available Countries", e);
		}
		return Collections.emptyList();
	}

	public long getAddressCountryId() {
		Address address = getAddress();
		if (address == null) {
			return 0;
		}
		return address.getCountryId();
	}

	public String getCountryLabel(long countryId) {
		try {
			return CountryServiceUtil.getCountry(countryId).getName();
		} catch (Exception e) {
			log.error("Could not get Country with id " + countryId, e);
			return StringPool.BLANK;
		}
	}

	private Phone getPhone(ListType type) {
		Phone searchedPhone = null;
		List<Phone> phones = null;
		try {
			phones = PhoneLocalServiceUtil.getPhones(application.getCompany()
					.getCompanyId(), Contact.class.getName(), application
					.getUser().getContactId());
		} catch (Exception e) {
			log.error("Could not get Phones for User with id "
					+ application.getUser().getUserId(), e);
		}
		if (phones != null && !phones.isEmpty()) {
			// Look up for the correct phone
			for (Phone phone : phones) {
				if (phone.getTypeId() == type.getListTypeId()) {
					searchedPhone = phone;
					break;
				}
			}

		} else {
			log.warn("No Phone found for User with id "
					+ application.getUser().getUserId());
		}

		if (searchedPhone == null) {
			try {
				log.info("Adding \"" + type.getName()
						+ "\" Phone for User with id "
						+ application.getUser().getUserId());
				searchedPhone = PhoneLocalServiceUtil.addPhone(application
						.getUser().getUserId(), Contact.class.getName(),
						application.getUser().getContactId(), StringPool.DASH,
						null, type.getListTypeId(), false, getServiceContext());
				log.info("\"" + type.getName() + "\" Phone for User with id "
						+ application.getUser().getUserId() + " added");
			} catch (Exception e1) {
				log.error("Could not add \"" + type.getName()
						+ "\" Phone for User with id "
						+ application.getUser().getUserId(), e1);
			}
		}

		return searchedPhone;
	}

	public Phone getPersonalPhone() {
		return getPhone(ListTypeUtil.getInstance().getContactPhonePersonal());
	}

	public String getPersonalPhoneNumber() {
		Phone phone = getPhone(ListTypeUtil.getInstance()
				.getContactPhonePersonal());
		if (phone == null) {
			return StringPool.DASH;
		} else {
			return phone.getNumber();
		}
	}

	public Phone getMobilePhone() {
		return getPhone(ListTypeUtil.getInstance().getContactPhoneMobilePhone());
	}

	public String getMobilePhoneNumber() {
		Phone phone = getPhone(ListTypeUtil.getInstance()
				.getContactPhoneMobilePhone());
		if (phone == null) {
			return StringPool.DASH;
		} else {
			return phone.getNumber();
		}
	}

	public Phone getBusinessPhone() {
		return getPhone(ListTypeUtil.getInstance().getContactPhoneBusiness());
	}

	public String getBusinessPhoneNumber() {
		Phone phone = getPhone(ListTypeUtil.getInstance()
				.getContactPhoneBusiness());
		if (phone == null) {
			return StringPool.DASH;
		} else {
			return phone.getNumber();
		}
	}

	public List<Organization> getAvailableOrganizations() {
		List<Organization> orgs = new ArrayList<Organization>();
		try {
			orgs.addAll(OrganizationLocalServiceUtil.getOrganizations(
					QueryUtil.ALL_POS, QueryUtil.ALL_POS));
		} catch (Exception e) {
			log.error("Could not get available Organisations", e);
		}
		return orgs;
	}

	public Organization getCurrentOrganization() {
		try {
			List<Organization> orgs = application.getUser().getOrganizations();
			if (orgs != null && !orgs.isEmpty()) {
				return orgs.get(0);
			}
		} catch (Exception e) {
			log.info("Could not get Organizations for User with id "
					+ application.getUser().getUserId(), e);
		}
		return null;
	}

	public long getCurrentOrganizationId() {
		long orgId = 0;
		Organization org = getCurrentOrganization();
		if (org != null) {
			orgId = org.getOrganizationId();
		}
		return orgId;
	}

	public String getOrgAddress(long orgId) {
		String addressLabel = StringPool.DASH;
		long companyId = PortalUtil.getDefaultCompanyId();
		List<Address> availableAddresses = null;
		try {
			availableAddresses = AddressLocalServiceUtil.getAddresses(
					companyId, Organization.class.getName(), orgId);
		} catch (Exception e) {
			log.error("Could not get Addresses for Organization with id "
					+ orgId, e);
		}
		if (availableAddresses == null) {
			log.warn("No Address found for Organization with id " + orgId);
			return addressLabel;
		}

		for (Address address : availableAddresses) {
			if (address.getTypeId() == ListTypeUtil.getInstance()
					.getOrgAddressPoBox().getListTypeId()) {
				addressLabel = address.getStreet1() + StringPool.SPACE
						+ address.getStreet2() + "<br/>" + address.getZip()
						+ StringPool.SPACE + address.getCity();
				try {
					Country country = address.getCountry();
					if (country != null) {
						addressLabel = addressLabel + "<br/>"
								+ country.getName();
					}
				} catch (Exception e) {
					log.error(
							"Could not get Country with id "
									+ address.getCountryId(), e);
				}
				return addressLabel;
			}
		}
		log.warn("No "
				+ ListTypeUtil.getInstance().getOrgAddressPoBox().getName()
				+ " Address found for Organization with id " + orgId);
		return addressLabel;
	}

	public String getOrgWebsite(long orgId) {
		String label = StringPool.DASH;
		long companyId = PortalUtil.getDefaultCompanyId();
		List<Website> websites = null;
		try {
			websites = WebsiteLocalServiceUtil.getWebsites(companyId,
					Organization.class.getName(), orgId);
		} catch (Exception e) {
			log.error("Could not get Websites for Organization with id "
					+ orgId, e);
		}
		if (websites == null) {
			log.warn("No Website found for Organization with id " + orgId);
			return label;
		}

		for (Website website : websites) {
			if (website.getTypeId() == ListTypeUtil.getInstance()
					.getOrgWebsitePublic().getListTypeId()) {
				label = website.getUrl();
				return label;
			}
		}

		log.warn("No "
				+ ListTypeUtil.getInstance().getOrgWebsitePublic().getName()
				+ " Website found for Organization with id " + orgId);
		return label;
	}

	private Phone getOrgPhone(long orgId, ListType type) {
		long companyId = PortalUtil.getDefaultCompanyId();
		List<Phone> phones = null;
		try {
			phones = PhoneLocalServiceUtil.getPhones(companyId,
					Organization.class.getName(), orgId);
		} catch (Exception e) {
			log.error("Could not get Phones for Organization with id " + orgId,
					e);
		}
		if (phones == null) {
			log.warn("No Phone found for Organization with id " + orgId);
			return null;
		}

		for (Phone phone : phones) {
			if (phone.getTypeId() == type.getListTypeId()) {
				return phone;
			}
		}

		log.warn("No " + type.getName()
				+ " Phone found for Organization with id " + orgId);
		return null;
	}

	public String getOrgPhone(long orgId) {
		String label = StringPool.BLANK;
		Phone phone = getOrgPhone(orgId, ListTypeUtil.getInstance()
				.getOrgPhoneLocal());
		if (phone != null) {
			label = phone.getNumber();
		}
		return label;
	}

	public String getOrgFax(long orgId) {
		String label = StringPool.BLANK;
		Phone phone = getOrgPhone(orgId, ListTypeUtil.getInstance()
				.getOrgPhoneFax());
		if (phone != null) {
			label = phone.getNumber();
		}
		return label;
	}

	public void commitChanges(BaseInfoProfileField field, Object value) {
		User currentUser = application.getUser();
		switch (field) {
		case Street1:
		case Street2:
		case Street3:
		case City:
		case Zip:
		case Country:
			Address address = getAddress();
			if (address == null) {
				break;
			}
			switch (field) {
			case Street1:
				address.setStreet1((String) value);
				try {
					AddressLocalServiceUtil.updateAddress(address);
					log.info("Street1 for User with id "
							+ currentUser.getUserId() + " updated");
				} catch (Exception e) {
					log.error("Could not update Street1 for User with id "
							+ currentUser.getUserId(), e);
				}
				break;

			case Street2:
				address.setStreet2((String) value);
				try {
					AddressLocalServiceUtil.updateAddress(address);
					log.info("Street2 for User with id "
							+ currentUser.getUserId() + " updated");
				} catch (Exception e) {
					log.error("Could not update Street2 for User with id "
							+ currentUser.getUserId(), e);
				}
				break;

			case Street3:
				address.setStreet3((String) value);
				try {
					AddressLocalServiceUtil.updateAddress(address);
					log.info("Street3 for User with id "
							+ currentUser.getUserId() + " updated");
				} catch (Exception e) {
					log.error("Could not update Street3 for User with id "
							+ currentUser.getUserId(), e);
				}
				break;

			case City:
				address.setCity((String) value);
				try {
					AddressLocalServiceUtil.updateAddress(address);
					log.info("City for User with id " + currentUser.getUserId()
							+ " updated");
					updateIdpProfile();
				} catch (Exception e) {
					log.error("Could not update City for User with id "
							+ currentUser.getUserId(), e);
				}
				break;

			case Zip:
				address.setZip((String) value);
				try {
					AddressLocalServiceUtil.updateAddress(address);
					log.info("Zip for User with id " + currentUser.getUserId()
							+ " updated");
					updateIdpProfile();
				} catch (Exception e) {
					log.error("Could not update Zip for User with id "
							+ currentUser.getUserId(), e);
				}
				break;

			case Country:
				address.setCountryId((Long) value);
				try {
					AddressLocalServiceUtil.updateAddress(address);
					log.info("Country for User with id "
							+ currentUser.getUserId() + " updated");
					updateIdpProfile();
				} catch (Exception e) {
					log.error("Could not update Country for User with id "
							+ currentUser.getUserId(), e);
				}
				break;

			default:
				break;
			}
			break;

		case PersonalPhoneNumber:
			Phone personPhone = getPersonalPhone();
			if (personPhone == null) {
				break;
			}

			personPhone.setNumber((String) value);
			try {
				PhoneLocalServiceUtil.updatePhone(personPhone);
				log.info("PersonalPhone Number for User with id "
						+ currentUser.getUserId() + " updated");
			} catch (Exception e) {
				log.error(
						"Could not update PersonalPhone Number for User with id "
								+ currentUser.getUserId(), e);
			}
			break;

		case MobilePhoneNumber:
			Phone mobilePhone = getMobilePhone();
			if (mobilePhone == null) {
				break;
			}

			mobilePhone.setNumber((String) value);
			try {
				PhoneLocalServiceUtil.updatePhone(mobilePhone);
				log.info("MobilePhone Number for User with id "
						+ currentUser.getUserId() + " updated");
			} catch (Exception e) {
				log.error(
						"Could not update MobilePhone Number for User with id "
								+ currentUser.getUserId(), e);
			}
			break;

		case BusinessPhoneNumber:
			Phone businessPhone = getBusinessPhone();
			if (businessPhone == null) {
				break;
			}

			businessPhone.setNumber((String) value);
			try {
				PhoneLocalServiceUtil.updatePhone(businessPhone);
				log.info("BusinessPhone Number for User with id "
						+ currentUser.getUserId() + " updated");
			} catch (Exception e) {
				log.error(
						"Could not update BusinessPhone Number for User with id "
								+ currentUser.getUserId(), e);
			}
			break;

		case Organization:
			try {
				long[] organizationIds = currentUser.getOrganizationIds();
				long users[] = { currentUser.getUserId() };

				// remove user from other organizations
				for (long organizationId : organizationIds) {
					UserLocalServiceUtil.unsetOrganizationUsers(organizationId,
							users);
				}

				if (value != null) {
					UserLocalServiceUtil.addOrganizationUsers((Long) value,
							users);
				}
				log.info("Organization for User with id "
						+ currentUser.getUserId() + " updated");
			} catch (Exception e) {
				log.error("Could not update Organization for User with id "
						+ currentUser.getUserId(), e);
			}

			break;

		default:
			log.warn("Unknown field. Nothing will be commited");
			break;
		}
	}

	private void updateIdpProfile() {
		try {
			// Remark: retrieve the User from the database to get the up-to-date
			// data.
			User currentUser = UserLocalServiceUtil.getUser(application
					.getUser().getUserId());
			OpenIdConnectUtil.updateUserProfile(currentUser);
		} catch (Exception e) {
			log.error("Could not update IDP profile of User with id "
					+ application.getUser().getUserId(), e);
		}
	}
}
