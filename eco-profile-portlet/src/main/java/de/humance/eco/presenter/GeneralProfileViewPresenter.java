package de.humance.eco.presenter;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ContactLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Resource;
import com.vaadin.server.StreamResource.StreamSource;

import de.humance.eco.portlet.GeneralProfileApplication;
import de.humance.eco.util.GeneralProfileField;
import de.humance.eco.util.ImageUtil;
import de.humance.eco.util.OpenIdConnectUtil;
import de.humance.eco.view.GeneralProfileView;
import de.humance.vaadin.presenter.BaseViewPresenter;

public class GeneralProfileViewPresenter extends
		BaseViewPresenter<GeneralProfileView, GeneralProfileApplication> {
	private static final long serialVersionUID = 6673060224607930288L;
	private Log log = LogFactoryUtil.getLog(getClass());

	public GeneralProfileViewPresenter(GeneralProfileView view,
			GeneralProfileApplication application) {
		super(view, application);
	}

	public Resource getPortraitResource() {
		try {
			return new ExternalResource(application.getUser().getPortraitURL(
					application.getThemeDisplay()));
		} catch (Exception e) {
			log.error("Could not get Portrait URL for User with id "
					+ application.getUser().getUserId(), e);
			return null;
		}
	}

	public String getFirstName() {
		return application.getUser().getFirstName();
	}

	public String getMiddleName() {
		return application.getUser().getMiddleName();
	}

	public String getLastName() {
		return application.getUser().getLastName();
	}

	public String getScreenName() {
		return application.getUser().getScreenName();
	}

	public String getEmail() {
		return application.getUser().getEmailAddress();
	}

	public String getJobTitle() {
		return application.getUser().getJobTitle();
	}

	public Date getDateOfBirth() {
		try {
			return application.getUser().getBirthday();
		} catch (Exception e) {
			log.error("Could not get Birthday for User with id "
					+ application.getUser().getUserId(), e);
			return null;
		}
	}

	public String[] getAvailableGenders() {
		String[] genders = new String[2];
		genders[0] = "male";
		genders[1] = "female";
		return genders;
	}

	public String getGenderLabel(String gender) {
		return LanguageUtil.get(application.getUserLocale(), gender);
	}

	public Boolean getGenderBoolean(String gender) {
		if (gender == "male") {
			return true;
		} else {
			return false;
		}
	}

	public String getSelectedGender() {
		boolean isMale = true;
		try {
			isMale = application.getUser().isMale();
		} catch (Exception e) {
			log.error("Could not get the gender of User with id "
					+ application.getUser().getUserId(), e);
		}
		if (isMale) {
			return "male";
		} else {
			return "female";
		}
	}

	public Locale[] getAvailableLocales() {
		return LanguageUtil.getAvailableLocales();
	}

	private String getLanguageId(Locale locale) {
		return LanguageUtil.getLanguageId(locale);
	}

	public String getLanguageLabel(Locale locale) {
		return locale.getDisplayLanguage(application.getUserLocale());
	}

	public Locale getSelectedLanguage() {
		return application.getUserLocale();
	}

	public Organization getCurrentOrg() {
		try {
			List<Organization> orgs = application.getUser().getOrganizations();
			if (orgs != null && !orgs.isEmpty()) {
				return orgs.get(0);
			}
		} catch (Exception e) {
			log.error("Could not get associated Organization for User with id "
					+ application.getUser().getUserId());
		}
		return null;
	}

	public String getCurrentOrgName() {
		String orgName = StringPool.DASH;
		Organization org = getCurrentOrg();
		if (org != null) {
			orgName = org.getName();
		}
		return orgName;
	}

	public Resource getCurrentOrgLogoResource() {
		Resource orgLogoResource = null;
		Organization org = getCurrentOrg();
		if (org != null) {
			orgLogoResource = new ExternalResource(application
					.getThemeDisplay().getPathImage()
					+ "/organization_logo?img_id=" + org.getLogoId());
		}
		return orgLogoResource;
	}

	public void commitChanges(GeneralProfileField field, Object value) {
		User currentUser = application.getUser();
		Contact contact = null;
		try {
			contact = currentUser.getContact();
		} catch (Exception e) {
			log.warn("Could not get corresponding Contact for User with id "
					+ currentUser.getUserId(), e);
		}
		switch (field) {
		case FirstName:
			try {
				currentUser.setFirstName((String) value);
				contact.setFirstName((String) value);
				UserLocalServiceUtil.updateUser(currentUser);
				ContactLocalServiceUtil.updateContact(contact);
				log.info("FirstName for User with id "
						+ currentUser.getUserId() + " updated");
				updateIdpProfile();
			} catch (Exception e) {
				log.error("Could not update FirstName for User with id "
						+ currentUser.getUserId(), e);
			}
			break;

		case MiddleName:
			try {
				currentUser.setMiddleName((String) value);
				contact.setMiddleName((String) value);
				UserLocalServiceUtil.updateUser(currentUser);
				ContactLocalServiceUtil.updateContact(contact);
				log.info("MiddleName for User with id "
						+ currentUser.getUserId() + " updated");
				updateIdpProfile();
			} catch (Exception e) {
				log.error("Could not update MiddleName for User with id "
						+ currentUser.getUserId(), e);
			}
			break;

		case LastName:
			try {
				currentUser.setLastName((String) value);
				contact.setLastName((String) value);
				UserLocalServiceUtil.updateUser(currentUser);
				ContactLocalServiceUtil.updateContact(contact);
				log.info("LastName for User with id " + currentUser.getUserId()
						+ " updated");
				updateIdpProfile();
			} catch (Exception e) {
				log.error("Could not update LastName for User with id "
						+ currentUser.getUserId(), e);
			}
			break;

		case ScreenName:
			try {
				currentUser.setScreenName((String) value);
				UserLocalServiceUtil.updateUser(currentUser);
				log.info("ScreenName for User with id "
						+ currentUser.getUserId() + " updated");
				updateIdpProfile();
			} catch (Exception e) {
				log.error("Could not update ScreenName for User with id "
						+ currentUser.getUserId(), e);
			}
			break;

		case Email:
			try {
				currentUser.setEmailAddress((String) value);
				contact.setEmailAddress((String) value);
				UserLocalServiceUtil.updateUser(currentUser);
				ContactLocalServiceUtil.updateContact(contact);
				log.info("Email for User with id " + currentUser.getUserId()
						+ " updated");
				updateIdpProfile();
			} catch (Exception e) {
				log.error("Could not update Email for User with id "
						+ currentUser.getUserId(), e);
			}
			break;

		case DateOfBirth:
			try {
				contact.setBirthday((Date) value);
				ContactLocalServiceUtil.updateContact(contact);
				log.info("DateOfBirth for User with id "
						+ currentUser.getUserId() + " updated");
				updateIdpProfile();
			} catch (Exception e) {
				log.error("Could not update DateOfBirth for User with id "
						+ currentUser.getUserId(), e);
			}
			break;

		case Gender:
			try {
				contact.setMale((Boolean) value);
				ContactLocalServiceUtil.updateContact(contact);
				log.info("Gender for User with id " + currentUser.getUserId()
						+ " updated");
				updateIdpProfile();
			} catch (Exception e) {
				log.error("Could not update Gender value '" + value
						+ "' for User with id " + currentUser.getUserId(), e);
			}
			break;

		case JobTitle:
			try {
				contact.setJobTitle((String) value);
				currentUser.setJobTitle((String) value);
				ContactLocalServiceUtil.updateContact(contact);
				UserLocalServiceUtil.updateUser(currentUser);
				log.info("JobTitle for User with id " + currentUser.getUserId()
						+ " updated");
			} catch (Exception e) {
				log.error("Could not update JobTitle for User with id "
						+ currentUser.getUserId(), e);
			}
			break;

		case Language:
			try {
				String languageId = getLanguageId((Locale) value);
				currentUser.setLanguageId(languageId);
				UserLocalServiceUtil.updateUser(currentUser);
				log.info("Language for User with id " + currentUser.getUserId()
						+ " updated");
				updateIdpProfile();
				application.getI18N().setLocale((Locale) value);
			} catch (Exception e) {
				log.error("Could not update Language for User with id "
						+ currentUser.getUserId(), e);
			}
			break;

		default:
			log.warn("Unknown field. Nothing will be commited");
			break;
		}

	}

	private void updateIdpProfile() {
		try {
			// Remark: retrieve the User from the database to get the up-to-date
			// data.
			User currentUser = UserLocalServiceUtil.getUser(application
					.getUser().getUserId());
			OpenIdConnectUtil.updateUserProfile(currentUser);
		} catch (Exception e) {
			log.error("Could not update IDP profile of User with id "
					+ application.getUser().getUserId(), e);
		}
	}

	public void commitPortrait(StreamSource portraitSource) {
		try {
			UserLocalServiceUtil.updatePortrait(application.getUser()
					.getUserId(), ImageUtil.toByteArray(portraitSource));
			view.portraitCommited();
		} catch (Exception e) {
			log.info("Could not update Portrait for User with id "
					+ application.getUser().getUserId());
			view.portraitCommitFailed();
		}
	}
}
