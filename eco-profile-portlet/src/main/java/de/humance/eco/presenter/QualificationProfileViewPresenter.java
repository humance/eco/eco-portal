package de.humance.eco.presenter;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Country;
import com.liferay.portal.service.CountryServiceUtil;

import de.humance.common.util.AttributeDefinitionType;
import de.humance.eco.portlet.QualificationProfileApplication;
import de.humance.eco.profile.model.Qualification;
import de.humance.eco.profile.model.QualificationAttribute;
import de.humance.eco.profile.model.QualificationAttributeDefinition;
import de.humance.eco.profile.model.QualificationType;
import de.humance.eco.profile.service.QualificationAttributeDefinitionLocalServiceUtil;
import de.humance.eco.profile.service.QualificationAttributeLocalServiceUtil;
import de.humance.eco.profile.service.QualificationLocalServiceUtil;
import de.humance.eco.profile.service.QualificationTypeLocalServiceUtil;
import de.humance.eco.view.QualificationProfileView;
import de.humance.vaadin.presenter.BaseViewPresenter;
import de.humance.vaadin.ui.misc.AttributeBooleanWidget;
import de.humance.vaadin.ui.misc.AttributeDateWidget;
import de.humance.vaadin.ui.misc.AttributeDoubleWidget;
import de.humance.vaadin.ui.misc.AttributeIntegerWidget;
import de.humance.vaadin.ui.misc.AttributeStringWidget;
import de.humance.vaadin.ui.misc.BaseAttributeWidget;

public class QualificationProfileViewPresenter
		extends
		BaseViewPresenter<QualificationProfileView, QualificationProfileApplication> {
	private static final long serialVersionUID = 6014508724583280099L;
	private Log log = LogFactoryUtil.getLog(getClass());
	private QualificationComparator qualifComparator = new QualificationComparator();
	private String documentDir = null;

	private class QualificationComparator implements Comparator<Qualification> {

		public int compare(Qualification o1, Qualification o2) {
			return o1.getExpirationDate().compareTo(o2.getExpirationDate());
		}
	}

	public QualificationProfileViewPresenter(QualificationProfileView view,
			QualificationProfileApplication application) {
		super(view, application);
	}

	public List<String> getTabNames() {
		List<String> tabNames = new ArrayList<String>();
		List<QualificationType> types = getQualificationTypes();
		for (QualificationType type : types) {
			tabNames.add(type.getName());
		}

		Collections.sort(tabNames, new Comparator<String>() {

			public int compare(String o1, String o2) {
				return o1.compareToIgnoreCase(o2);
			}
		});

		return tabNames;
	}

	public QualificationType getQualificationType(String typeName) {
		QualificationType type = null;
		try {
			List<QualificationType> types = QualificationTypeLocalServiceUtil
					.findByName(typeName);
			if (types != null && types.size() > 0) {
				type = types.get(0);
			}
		} catch (Exception e) {
			log.error("Could not find QualificationType with name \""
					+ typeName + "\"", e);
		}
		return type;
	}

	public QualificationType getQualificationType(long qualificationTypeId) {
		QualificationType type = null;
		try {
			type = QualificationTypeLocalServiceUtil
					.getQualificationType(qualificationTypeId);
		} catch (Exception e) {
			log.error("Could not get QualificationType with Id "
					+ qualificationTypeId, e);
		}
		return type;
	}

	public String getQualificationTypeLabel(long qualificationTypeId) {
		String label = StringPool.DASH;
		QualificationType type = getQualificationType(qualificationTypeId);
		if (type != null) {
			label = type.getName();
		}
		return label;
	}

	public List<QualificationType> getQualificationTypes() {
		List<QualificationType> types = new ArrayList<QualificationType>();
		try {
			types.addAll(QualificationTypeLocalServiceUtil
					.getQualificationTypes(QueryUtil.ALL_POS, QueryUtil.ALL_POS));
		} catch (Exception e) {
			log.error("Could not get all QualificationTypes", e);
		}
		return types;
	}

	public List<Qualification> getAvailableQualifications() {
		List<Qualification> qualifs = new ArrayList<Qualification>();
		List<QualificationType> types = getQualificationTypes();
		for (QualificationType type : types) {
			try {
				qualifs.addAll(QualificationLocalServiceUtil
						.findByOwnerIdAndTypeId(application.getUser()
								.getUserId(), type.getQualificationTypeId()));
			} catch (Exception e) {
				log.error(
						"Could not get all Qualification for User with id "
								+ application.getUser().getUserId()
								+ " and QualificationType with id "
								+ type.getQualificationTypeId(), e);
			}
		}

		Collections.sort(qualifs, qualifComparator);

		return qualifs;
	}

	public Qualification getQualification(long qualificationId) {
		Qualification qualification = null;
		try {
			qualification = QualificationLocalServiceUtil
					.getQualification(qualificationId);
		} catch (Exception e) {
			log.error("Could not get Qualification with id " + qualificationId,
					e);
		}
		return qualification;
	}

	public List<Country> getAvailableCountries() {
		List<Country> countries = new ArrayList<Country>();
		try {
			countries.addAll(CountryServiceUtil.getCountries());
		} catch (Exception e) {
			log.error("Could not get all available Countries", e);
		}
		return countries;
	}

	public String getCountryLabel(long countryId) {
		String label = StringPool.BLANK;
		try {
			Country country = CountryServiceUtil.getCountry(countryId);
			label = country.getName();
		} catch (Exception e) {
			log.error("Could not get name for Country with id " + countryId, e);
		}
		return label;
	}

	public List<QualificationAttributeDefinition> getAttrDefinitions(long typeId) {
		List<QualificationAttributeDefinition> definitions = new ArrayList<QualificationAttributeDefinition>();
		try {
			definitions.addAll(QualificationAttributeDefinitionLocalServiceUtil
					.findByQualificationTypeId(typeId));
		} catch (Exception e) {
			log.error(
					"Could not get QualificationAttributeDefinitions for QualificationType with id "
							+ typeId, e);
		}
		return definitions;
	}

	public QualificationAttribute getAttribute(long qualificationId,
			long attrDefinitionId) {
		QualificationAttribute attribute = null;
		try {
			List<QualificationAttribute> attributes = QualificationAttributeLocalServiceUtil
					.findByQualificationIdAndAttributeDefinitionId(
							qualificationId, attrDefinitionId);
			if (attributes != null && attributes.size() > 0) {
				attribute = attributes.get(0);
			}
		} catch (Exception e) {
			log.error(
					"Could not get QualificationAttribute for Qualification with id "
							+ qualificationId
							+ " and AttributeDefinition with id "
							+ attrDefinitionId, e);
		}
		return attribute;
	}

	@SuppressWarnings("rawtypes")
	public void commitCertificate(long qualificationId, long typeId,
			String name, String certNumber, Date issueDate, Date expiryDate,
			String issuingInstitution, String issuingCountry,
			List<BaseAttributeWidget> attrWidgets) {
		try {
			Qualification qualif = null;
			if (qualificationId == 0) {
				qualif = QualificationLocalServiceUtil.addQualification(
						application.getUser().getUserId(), application
								.getUser().getFullName(), application.getUser()
								.getUserId(), typeId, name, certNumber,
						issueDate, expiryDate, issuingCountry,
						issuingInstitution);

			} else {
				qualif = QualificationLocalServiceUtil.updateQualification(
						qualificationId, application.getUser().getUserId(),
						application.getUser().getFullName(), application
								.getUser().getUserId(), typeId, name,
						certNumber, issueDate, expiryDate, issuingCountry,
						issuingInstitution);
			}

			if (attrWidgets != null) {
				int index = 0;
				for (BaseAttributeWidget attrWidget : attrWidgets) {
					AttributeDefinitionType type = AttributeDefinitionType.STRING;
					String attributeValue = null;
					if (attrWidget instanceof AttributeStringWidget) {
						type = AttributeDefinitionType.STRING;
						attributeValue = ((AttributeStringWidget) attrWidget)
								.getValue();
					} else if (attrWidget instanceof AttributeIntegerWidget) {
						type = AttributeDefinitionType.INTEGER;
						attributeValue = Integer
								.toString(((AttributeIntegerWidget) attrWidget)
										.getValue());
					} else if (attrWidget instanceof AttributeDoubleWidget) {
						type = AttributeDefinitionType.DOUBLE;
						attributeValue = Double
								.toString(((AttributeDoubleWidget) attrWidget)
										.getValue());
					} else if (attrWidget instanceof AttributeDateWidget) {
						type = AttributeDefinitionType.DATE;
						attributeValue = Long
								.toString(((AttributeDateWidget) attrWidget)
										.getValue().getTime());
					} else if (attrWidget instanceof AttributeBooleanWidget) {
						type = AttributeDefinitionType.BOOLEAN;
						attributeValue = ((AttributeBooleanWidget) attrWidget)
								.getValue().toString();
					}

					if (attrWidget.getAttributeId() > 0) {
						QualificationAttributeLocalServiceUtil
								.updateQualificationAttribute(
										attrWidget.getAttributeId(),
										qualif.getQualificationId(),
										attrWidget.getDefinitionId(),
										type.toString(), index, attributeValue);
					} else {
						QualificationAttributeLocalServiceUtil
								.addQualificationAttribute(
										qualif.getQualificationId(),
										attrWidget.getDefinitionId(),
										type.toString(), index, attributeValue);
					}
				}
			}

			view.certificateCommitted();
		} catch (Exception e) {
			log.error("Could not add Qualification for User with id "
					+ application.getUser().getUserId(), e);
			view.certificateCommitFailed(e);
		}
	}

	public void deleteQualification(long qualificationId) {
		try {
			QualificationLocalServiceUtil.deleteQualification(qualificationId);
			view.certificateDeleted();

		} catch (Exception e) {
			log.error("Could not delete Qualification with id "
					+ qualificationId, e);
			view.certificateDeleteFailed(e);
		}
	}

	public boolean hasDocumentFile(long qualificationId) {
		// TODO : Implement "hasDocumentFile"
		return false;
	}

	public File getDocumentFile(String qualifSyncId) {
		if (Validator.isNull(documentDir)) {
			log.warn("No Document directory defined for Certificate. Check the portal properties.");
			return null;
		}

		// TODO : Implement "getDocumentFile"
		return null;
	}
}
