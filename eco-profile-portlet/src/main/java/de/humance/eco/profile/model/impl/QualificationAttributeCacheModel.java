package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import de.humance.eco.profile.model.QualificationAttribute;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing QualificationAttribute in entity cache.
 *
 * @author Humance
 * @see QualificationAttribute
 * @generated
 */
public class QualificationAttributeCacheModel implements CacheModel<QualificationAttribute>,
    Externalizable {
    public long qualificationAttributeId;
    public long qualificationId;
    public long qualificationAttributeDefinitionId;
    public String type;
    public long sequenceNumber;
    public String attributeValue;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{qualificationAttributeId=");
        sb.append(qualificationAttributeId);
        sb.append(", qualificationId=");
        sb.append(qualificationId);
        sb.append(", qualificationAttributeDefinitionId=");
        sb.append(qualificationAttributeDefinitionId);
        sb.append(", type=");
        sb.append(type);
        sb.append(", sequenceNumber=");
        sb.append(sequenceNumber);
        sb.append(", attributeValue=");
        sb.append(attributeValue);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public QualificationAttribute toEntityModel() {
        QualificationAttributeImpl qualificationAttributeImpl = new QualificationAttributeImpl();

        qualificationAttributeImpl.setQualificationAttributeId(qualificationAttributeId);
        qualificationAttributeImpl.setQualificationId(qualificationId);
        qualificationAttributeImpl.setQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);

        if (type == null) {
            qualificationAttributeImpl.setType(StringPool.BLANK);
        } else {
            qualificationAttributeImpl.setType(type);
        }

        qualificationAttributeImpl.setSequenceNumber(sequenceNumber);

        if (attributeValue == null) {
            qualificationAttributeImpl.setAttributeValue(StringPool.BLANK);
        } else {
            qualificationAttributeImpl.setAttributeValue(attributeValue);
        }

        qualificationAttributeImpl.resetOriginalValues();

        return qualificationAttributeImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        qualificationAttributeId = objectInput.readLong();
        qualificationId = objectInput.readLong();
        qualificationAttributeDefinitionId = objectInput.readLong();
        type = objectInput.readUTF();
        sequenceNumber = objectInput.readLong();
        attributeValue = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(qualificationAttributeId);
        objectOutput.writeLong(qualificationId);
        objectOutput.writeLong(qualificationAttributeDefinitionId);

        if (type == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(type);
        }

        objectOutput.writeLong(sequenceNumber);

        if (attributeValue == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(attributeValue);
        }
    }
}
