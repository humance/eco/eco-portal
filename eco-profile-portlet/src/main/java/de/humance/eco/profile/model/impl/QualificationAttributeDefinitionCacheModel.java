package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import de.humance.eco.profile.model.QualificationAttributeDefinition;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing QualificationAttributeDefinition in entity cache.
 *
 * @author Humance
 * @see QualificationAttributeDefinition
 * @generated
 */
public class QualificationAttributeDefinitionCacheModel implements CacheModel<QualificationAttributeDefinition>,
    Externalizable {
    public long qualificationAttributeDefinitionId;
    public long creatorId;
    public String creatorName;
    public long createDate;
    public long modifierId;
    public String modifierName;
    public long modifiedDate;
    public long qualificationTypeId;
    public String type;
    public String title;
    public double minRangeValue;
    public double maxRangeValue;
    public String unit;
    public long sequenceNumber;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(29);

        sb.append("{qualificationAttributeDefinitionId=");
        sb.append(qualificationAttributeDefinitionId);
        sb.append(", creatorId=");
        sb.append(creatorId);
        sb.append(", creatorName=");
        sb.append(creatorName);
        sb.append(", createDate=");
        sb.append(createDate);
        sb.append(", modifierId=");
        sb.append(modifierId);
        sb.append(", modifierName=");
        sb.append(modifierName);
        sb.append(", modifiedDate=");
        sb.append(modifiedDate);
        sb.append(", qualificationTypeId=");
        sb.append(qualificationTypeId);
        sb.append(", type=");
        sb.append(type);
        sb.append(", title=");
        sb.append(title);
        sb.append(", minRangeValue=");
        sb.append(minRangeValue);
        sb.append(", maxRangeValue=");
        sb.append(maxRangeValue);
        sb.append(", unit=");
        sb.append(unit);
        sb.append(", sequenceNumber=");
        sb.append(sequenceNumber);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public QualificationAttributeDefinition toEntityModel() {
        QualificationAttributeDefinitionImpl qualificationAttributeDefinitionImpl =
            new QualificationAttributeDefinitionImpl();

        qualificationAttributeDefinitionImpl.setQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);
        qualificationAttributeDefinitionImpl.setCreatorId(creatorId);

        if (creatorName == null) {
            qualificationAttributeDefinitionImpl.setCreatorName(StringPool.BLANK);
        } else {
            qualificationAttributeDefinitionImpl.setCreatorName(creatorName);
        }

        if (createDate == Long.MIN_VALUE) {
            qualificationAttributeDefinitionImpl.setCreateDate(null);
        } else {
            qualificationAttributeDefinitionImpl.setCreateDate(new Date(
                    createDate));
        }

        qualificationAttributeDefinitionImpl.setModifierId(modifierId);

        if (modifierName == null) {
            qualificationAttributeDefinitionImpl.setModifierName(StringPool.BLANK);
        } else {
            qualificationAttributeDefinitionImpl.setModifierName(modifierName);
        }

        if (modifiedDate == Long.MIN_VALUE) {
            qualificationAttributeDefinitionImpl.setModifiedDate(null);
        } else {
            qualificationAttributeDefinitionImpl.setModifiedDate(new Date(
                    modifiedDate));
        }

        qualificationAttributeDefinitionImpl.setQualificationTypeId(qualificationTypeId);

        if (type == null) {
            qualificationAttributeDefinitionImpl.setType(StringPool.BLANK);
        } else {
            qualificationAttributeDefinitionImpl.setType(type);
        }

        if (title == null) {
            qualificationAttributeDefinitionImpl.setTitle(StringPool.BLANK);
        } else {
            qualificationAttributeDefinitionImpl.setTitle(title);
        }

        qualificationAttributeDefinitionImpl.setMinRangeValue(minRangeValue);
        qualificationAttributeDefinitionImpl.setMaxRangeValue(maxRangeValue);

        if (unit == null) {
            qualificationAttributeDefinitionImpl.setUnit(StringPool.BLANK);
        } else {
            qualificationAttributeDefinitionImpl.setUnit(unit);
        }

        qualificationAttributeDefinitionImpl.setSequenceNumber(sequenceNumber);

        qualificationAttributeDefinitionImpl.resetOriginalValues();

        return qualificationAttributeDefinitionImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        qualificationAttributeDefinitionId = objectInput.readLong();
        creatorId = objectInput.readLong();
        creatorName = objectInput.readUTF();
        createDate = objectInput.readLong();
        modifierId = objectInput.readLong();
        modifierName = objectInput.readUTF();
        modifiedDate = objectInput.readLong();
        qualificationTypeId = objectInput.readLong();
        type = objectInput.readUTF();
        title = objectInput.readUTF();
        minRangeValue = objectInput.readDouble();
        maxRangeValue = objectInput.readDouble();
        unit = objectInput.readUTF();
        sequenceNumber = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(qualificationAttributeDefinitionId);
        objectOutput.writeLong(creatorId);

        if (creatorName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(creatorName);
        }

        objectOutput.writeLong(createDate);
        objectOutput.writeLong(modifierId);

        if (modifierName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(modifierName);
        }

        objectOutput.writeLong(modifiedDate);
        objectOutput.writeLong(qualificationTypeId);

        if (type == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(type);
        }

        if (title == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(title);
        }

        objectOutput.writeDouble(minRangeValue);
        objectOutput.writeDouble(maxRangeValue);

        if (unit == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(unit);
        }

        objectOutput.writeLong(sequenceNumber);
    }
}
