package de.humance.eco.profile.model.impl;

/**
 * The extended model implementation for the QualificationAttributeDefinition service. Represents a row in the &quot;Profile_QualificationAttributeDefinition&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link de.humance.eco.profile.model.QualificationAttributeDefinition} interface.
 * </p>
 *
 * @author Humance
 */
public class QualificationAttributeDefinitionImpl
    extends QualificationAttributeDefinitionBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a qualification attribute definition model instance should use the {@link de.humance.eco.profile.model.QualificationAttributeDefinition} interface instead.
     */
    public QualificationAttributeDefinitionImpl() {
    }
}
