package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import de.humance.eco.profile.model.QualificationAttributeDefinitionResource;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing QualificationAttributeDefinitionResource in entity cache.
 *
 * @author Humance
 * @see QualificationAttributeDefinitionResource
 * @generated
 */
public class QualificationAttributeDefinitionResourceCacheModel
    implements CacheModel<QualificationAttributeDefinitionResource>,
        Externalizable {
    public long qualificationAttributeDefinitionResourceId;
    public long qualificationAttributeDefinitionId;
    public String country;
    public String language;
    public String title;
    public String unit;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{qualificationAttributeDefinitionResourceId=");
        sb.append(qualificationAttributeDefinitionResourceId);
        sb.append(", qualificationAttributeDefinitionId=");
        sb.append(qualificationAttributeDefinitionId);
        sb.append(", country=");
        sb.append(country);
        sb.append(", language=");
        sb.append(language);
        sb.append(", title=");
        sb.append(title);
        sb.append(", unit=");
        sb.append(unit);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public QualificationAttributeDefinitionResource toEntityModel() {
        QualificationAttributeDefinitionResourceImpl qualificationAttributeDefinitionResourceImpl =
            new QualificationAttributeDefinitionResourceImpl();

        qualificationAttributeDefinitionResourceImpl.setQualificationAttributeDefinitionResourceId(qualificationAttributeDefinitionResourceId);
        qualificationAttributeDefinitionResourceImpl.setQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);

        if (country == null) {
            qualificationAttributeDefinitionResourceImpl.setCountry(StringPool.BLANK);
        } else {
            qualificationAttributeDefinitionResourceImpl.setCountry(country);
        }

        if (language == null) {
            qualificationAttributeDefinitionResourceImpl.setLanguage(StringPool.BLANK);
        } else {
            qualificationAttributeDefinitionResourceImpl.setLanguage(language);
        }

        if (title == null) {
            qualificationAttributeDefinitionResourceImpl.setTitle(StringPool.BLANK);
        } else {
            qualificationAttributeDefinitionResourceImpl.setTitle(title);
        }

        if (unit == null) {
            qualificationAttributeDefinitionResourceImpl.setUnit(StringPool.BLANK);
        } else {
            qualificationAttributeDefinitionResourceImpl.setUnit(unit);
        }

        qualificationAttributeDefinitionResourceImpl.resetOriginalValues();

        return qualificationAttributeDefinitionResourceImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        qualificationAttributeDefinitionResourceId = objectInput.readLong();
        qualificationAttributeDefinitionId = objectInput.readLong();
        country = objectInput.readUTF();
        language = objectInput.readUTF();
        title = objectInput.readUTF();
        unit = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(qualificationAttributeDefinitionResourceId);
        objectOutput.writeLong(qualificationAttributeDefinitionId);

        if (country == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(country);
        }

        if (language == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(language);
        }

        if (title == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(title);
        }

        if (unit == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(unit);
        }
    }
}
