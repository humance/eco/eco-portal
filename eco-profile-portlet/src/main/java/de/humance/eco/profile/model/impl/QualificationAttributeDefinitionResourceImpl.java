package de.humance.eco.profile.model.impl;

/**
 * The extended model implementation for the QualificationAttributeDefinitionResource service. Represents a row in the &quot;Profile_QualificationAttributeDefinitionResource&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link de.humance.eco.profile.model.QualificationAttributeDefinitionResource} interface.
 * </p>
 *
 * @author Humance
 */
public class QualificationAttributeDefinitionResourceImpl
    extends QualificationAttributeDefinitionResourceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a qualification attribute definition resource model instance should use the {@link de.humance.eco.profile.model.QualificationAttributeDefinitionResource} interface instead.
     */
    public QualificationAttributeDefinitionResourceImpl() {
    }
}
