package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import de.humance.eco.profile.model.Qualification;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Qualification in entity cache.
 *
 * @author Humance
 * @see Qualification
 * @generated
 */
public class QualificationCacheModel implements CacheModel<Qualification>,
    Externalizable {
    public long qualificationId;
    public long creatorId;
    public String creatorName;
    public long createDate;
    public long modifierId;
    public String modifierName;
    public long modifiedDate;
    public long ownerId;
    public long typeId;
    public String name;
    public String certNumber;
    public long deliveryDate;
    public long expirationDate;
    public String issuingCountry;
    public String issuingInstitution;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(31);

        sb.append("{qualificationId=");
        sb.append(qualificationId);
        sb.append(", creatorId=");
        sb.append(creatorId);
        sb.append(", creatorName=");
        sb.append(creatorName);
        sb.append(", createDate=");
        sb.append(createDate);
        sb.append(", modifierId=");
        sb.append(modifierId);
        sb.append(", modifierName=");
        sb.append(modifierName);
        sb.append(", modifiedDate=");
        sb.append(modifiedDate);
        sb.append(", ownerId=");
        sb.append(ownerId);
        sb.append(", typeId=");
        sb.append(typeId);
        sb.append(", name=");
        sb.append(name);
        sb.append(", certNumber=");
        sb.append(certNumber);
        sb.append(", deliveryDate=");
        sb.append(deliveryDate);
        sb.append(", expirationDate=");
        sb.append(expirationDate);
        sb.append(", issuingCountry=");
        sb.append(issuingCountry);
        sb.append(", issuingInstitution=");
        sb.append(issuingInstitution);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Qualification toEntityModel() {
        QualificationImpl qualificationImpl = new QualificationImpl();

        qualificationImpl.setQualificationId(qualificationId);
        qualificationImpl.setCreatorId(creatorId);

        if (creatorName == null) {
            qualificationImpl.setCreatorName(StringPool.BLANK);
        } else {
            qualificationImpl.setCreatorName(creatorName);
        }

        if (createDate == Long.MIN_VALUE) {
            qualificationImpl.setCreateDate(null);
        } else {
            qualificationImpl.setCreateDate(new Date(createDate));
        }

        qualificationImpl.setModifierId(modifierId);

        if (modifierName == null) {
            qualificationImpl.setModifierName(StringPool.BLANK);
        } else {
            qualificationImpl.setModifierName(modifierName);
        }

        if (modifiedDate == Long.MIN_VALUE) {
            qualificationImpl.setModifiedDate(null);
        } else {
            qualificationImpl.setModifiedDate(new Date(modifiedDate));
        }

        qualificationImpl.setOwnerId(ownerId);
        qualificationImpl.setTypeId(typeId);

        if (name == null) {
            qualificationImpl.setName(StringPool.BLANK);
        } else {
            qualificationImpl.setName(name);
        }

        if (certNumber == null) {
            qualificationImpl.setCertNumber(StringPool.BLANK);
        } else {
            qualificationImpl.setCertNumber(certNumber);
        }

        if (deliveryDate == Long.MIN_VALUE) {
            qualificationImpl.setDeliveryDate(null);
        } else {
            qualificationImpl.setDeliveryDate(new Date(deliveryDate));
        }

        if (expirationDate == Long.MIN_VALUE) {
            qualificationImpl.setExpirationDate(null);
        } else {
            qualificationImpl.setExpirationDate(new Date(expirationDate));
        }

        if (issuingCountry == null) {
            qualificationImpl.setIssuingCountry(StringPool.BLANK);
        } else {
            qualificationImpl.setIssuingCountry(issuingCountry);
        }

        if (issuingInstitution == null) {
            qualificationImpl.setIssuingInstitution(StringPool.BLANK);
        } else {
            qualificationImpl.setIssuingInstitution(issuingInstitution);
        }

        qualificationImpl.resetOriginalValues();

        return qualificationImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        qualificationId = objectInput.readLong();
        creatorId = objectInput.readLong();
        creatorName = objectInput.readUTF();
        createDate = objectInput.readLong();
        modifierId = objectInput.readLong();
        modifierName = objectInput.readUTF();
        modifiedDate = objectInput.readLong();
        ownerId = objectInput.readLong();
        typeId = objectInput.readLong();
        name = objectInput.readUTF();
        certNumber = objectInput.readUTF();
        deliveryDate = objectInput.readLong();
        expirationDate = objectInput.readLong();
        issuingCountry = objectInput.readUTF();
        issuingInstitution = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(qualificationId);
        objectOutput.writeLong(creatorId);

        if (creatorName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(creatorName);
        }

        objectOutput.writeLong(createDate);
        objectOutput.writeLong(modifierId);

        if (modifierName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(modifierName);
        }

        objectOutput.writeLong(modifiedDate);
        objectOutput.writeLong(ownerId);
        objectOutput.writeLong(typeId);

        if (name == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(name);
        }

        if (certNumber == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(certNumber);
        }

        objectOutput.writeLong(deliveryDate);
        objectOutput.writeLong(expirationDate);

        if (issuingCountry == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(issuingCountry);
        }

        if (issuingInstitution == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(issuingInstitution);
        }
    }
}
