package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import de.humance.eco.profile.model.QualificationType;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing QualificationType in entity cache.
 *
 * @author Humance
 * @see QualificationType
 * @generated
 */
public class QualificationTypeCacheModel implements CacheModel<QualificationType>,
    Externalizable {
    public long qualificationTypeId;
    public long creatorId;
    public String creatorName;
    public long createDate;
    public long modifierId;
    public String modifierName;
    public long modifiedDate;
    public String name;
    public String description;
    public String note;
    public long iconId;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(23);

        sb.append("{qualificationTypeId=");
        sb.append(qualificationTypeId);
        sb.append(", creatorId=");
        sb.append(creatorId);
        sb.append(", creatorName=");
        sb.append(creatorName);
        sb.append(", createDate=");
        sb.append(createDate);
        sb.append(", modifierId=");
        sb.append(modifierId);
        sb.append(", modifierName=");
        sb.append(modifierName);
        sb.append(", modifiedDate=");
        sb.append(modifiedDate);
        sb.append(", name=");
        sb.append(name);
        sb.append(", description=");
        sb.append(description);
        sb.append(", note=");
        sb.append(note);
        sb.append(", iconId=");
        sb.append(iconId);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public QualificationType toEntityModel() {
        QualificationTypeImpl qualificationTypeImpl = new QualificationTypeImpl();

        qualificationTypeImpl.setQualificationTypeId(qualificationTypeId);
        qualificationTypeImpl.setCreatorId(creatorId);

        if (creatorName == null) {
            qualificationTypeImpl.setCreatorName(StringPool.BLANK);
        } else {
            qualificationTypeImpl.setCreatorName(creatorName);
        }

        if (createDate == Long.MIN_VALUE) {
            qualificationTypeImpl.setCreateDate(null);
        } else {
            qualificationTypeImpl.setCreateDate(new Date(createDate));
        }

        qualificationTypeImpl.setModifierId(modifierId);

        if (modifierName == null) {
            qualificationTypeImpl.setModifierName(StringPool.BLANK);
        } else {
            qualificationTypeImpl.setModifierName(modifierName);
        }

        if (modifiedDate == Long.MIN_VALUE) {
            qualificationTypeImpl.setModifiedDate(null);
        } else {
            qualificationTypeImpl.setModifiedDate(new Date(modifiedDate));
        }

        if (name == null) {
            qualificationTypeImpl.setName(StringPool.BLANK);
        } else {
            qualificationTypeImpl.setName(name);
        }

        if (description == null) {
            qualificationTypeImpl.setDescription(StringPool.BLANK);
        } else {
            qualificationTypeImpl.setDescription(description);
        }

        if (note == null) {
            qualificationTypeImpl.setNote(StringPool.BLANK);
        } else {
            qualificationTypeImpl.setNote(note);
        }

        qualificationTypeImpl.setIconId(iconId);

        qualificationTypeImpl.resetOriginalValues();

        return qualificationTypeImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        qualificationTypeId = objectInput.readLong();
        creatorId = objectInput.readLong();
        creatorName = objectInput.readUTF();
        createDate = objectInput.readLong();
        modifierId = objectInput.readLong();
        modifierName = objectInput.readUTF();
        modifiedDate = objectInput.readLong();
        name = objectInput.readUTF();
        description = objectInput.readUTF();
        note = objectInput.readUTF();
        iconId = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(qualificationTypeId);
        objectOutput.writeLong(creatorId);

        if (creatorName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(creatorName);
        }

        objectOutput.writeLong(createDate);
        objectOutput.writeLong(modifierId);

        if (modifierName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(modifierName);
        }

        objectOutput.writeLong(modifiedDate);

        if (name == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(name);
        }

        if (description == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(description);
        }

        if (note == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(note);
        }

        objectOutput.writeLong(iconId);
    }
}
