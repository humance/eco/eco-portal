package de.humance.eco.profile.model.impl;

/**
 * The extended model implementation for the QualificationType service. Represents a row in the &quot;Profile_QualificationType&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link de.humance.eco.profile.model.QualificationType} interface.
 * </p>
 *
 * @author Humance
 */
public class QualificationTypeImpl extends QualificationTypeBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a qualification type model instance should use the {@link de.humance.eco.profile.model.QualificationType} interface instead.
     */
    public QualificationTypeImpl() {
    }
}
