package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import de.humance.eco.profile.model.QualificationTypeResource;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing QualificationTypeResource in entity cache.
 *
 * @author Humance
 * @see QualificationTypeResource
 * @generated
 */
public class QualificationTypeResourceCacheModel implements CacheModel<QualificationTypeResource>,
    Externalizable {
    public long qualificationTypeResourceId;
    public long qualificationTypeId;
    public String country;
    public String language;
    public String name;
    public String description;
    public String note;
    public long iconId;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(17);

        sb.append("{qualificationTypeResourceId=");
        sb.append(qualificationTypeResourceId);
        sb.append(", qualificationTypeId=");
        sb.append(qualificationTypeId);
        sb.append(", country=");
        sb.append(country);
        sb.append(", language=");
        sb.append(language);
        sb.append(", name=");
        sb.append(name);
        sb.append(", description=");
        sb.append(description);
        sb.append(", note=");
        sb.append(note);
        sb.append(", iconId=");
        sb.append(iconId);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public QualificationTypeResource toEntityModel() {
        QualificationTypeResourceImpl qualificationTypeResourceImpl = new QualificationTypeResourceImpl();

        qualificationTypeResourceImpl.setQualificationTypeResourceId(qualificationTypeResourceId);
        qualificationTypeResourceImpl.setQualificationTypeId(qualificationTypeId);

        if (country == null) {
            qualificationTypeResourceImpl.setCountry(StringPool.BLANK);
        } else {
            qualificationTypeResourceImpl.setCountry(country);
        }

        if (language == null) {
            qualificationTypeResourceImpl.setLanguage(StringPool.BLANK);
        } else {
            qualificationTypeResourceImpl.setLanguage(language);
        }

        if (name == null) {
            qualificationTypeResourceImpl.setName(StringPool.BLANK);
        } else {
            qualificationTypeResourceImpl.setName(name);
        }

        if (description == null) {
            qualificationTypeResourceImpl.setDescription(StringPool.BLANK);
        } else {
            qualificationTypeResourceImpl.setDescription(description);
        }

        if (note == null) {
            qualificationTypeResourceImpl.setNote(StringPool.BLANK);
        } else {
            qualificationTypeResourceImpl.setNote(note);
        }

        qualificationTypeResourceImpl.setIconId(iconId);

        qualificationTypeResourceImpl.resetOriginalValues();

        return qualificationTypeResourceImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        qualificationTypeResourceId = objectInput.readLong();
        qualificationTypeId = objectInput.readLong();
        country = objectInput.readUTF();
        language = objectInput.readUTF();
        name = objectInput.readUTF();
        description = objectInput.readUTF();
        note = objectInput.readUTF();
        iconId = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(qualificationTypeResourceId);
        objectOutput.writeLong(qualificationTypeId);

        if (country == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(country);
        }

        if (language == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(language);
        }

        if (name == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(name);
        }

        if (description == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(description);
        }

        if (note == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(note);
        }

        objectOutput.writeLong(iconId);
    }
}
