package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import de.humance.eco.profile.model.Trailer;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Trailer in entity cache.
 *
 * @author Humance
 * @see Trailer
 * @generated
 */
public class TrailerCacheModel implements CacheModel<Trailer>, Externalizable {
    public long trailerId;
    public long creatorId;
    public String creatorName;
    public long createDate;
    public long modifierId;
    public String modifierName;
    public long modifiedDate;
    public long driverId;
    public long typeId;
    public long manufacturerId;
    public long organizationId;
    public String modelName;
    public long dimensionHeight;
    public long dimensionWidth;
    public long dimensionDepth;
    public String licensePlate;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(33);

        sb.append("{trailerId=");
        sb.append(trailerId);
        sb.append(", creatorId=");
        sb.append(creatorId);
        sb.append(", creatorName=");
        sb.append(creatorName);
        sb.append(", createDate=");
        sb.append(createDate);
        sb.append(", modifierId=");
        sb.append(modifierId);
        sb.append(", modifierName=");
        sb.append(modifierName);
        sb.append(", modifiedDate=");
        sb.append(modifiedDate);
        sb.append(", driverId=");
        sb.append(driverId);
        sb.append(", typeId=");
        sb.append(typeId);
        sb.append(", manufacturerId=");
        sb.append(manufacturerId);
        sb.append(", organizationId=");
        sb.append(organizationId);
        sb.append(", modelName=");
        sb.append(modelName);
        sb.append(", dimensionHeight=");
        sb.append(dimensionHeight);
        sb.append(", dimensionWidth=");
        sb.append(dimensionWidth);
        sb.append(", dimensionDepth=");
        sb.append(dimensionDepth);
        sb.append(", licensePlate=");
        sb.append(licensePlate);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Trailer toEntityModel() {
        TrailerImpl trailerImpl = new TrailerImpl();

        trailerImpl.setTrailerId(trailerId);
        trailerImpl.setCreatorId(creatorId);

        if (creatorName == null) {
            trailerImpl.setCreatorName(StringPool.BLANK);
        } else {
            trailerImpl.setCreatorName(creatorName);
        }

        if (createDate == Long.MIN_VALUE) {
            trailerImpl.setCreateDate(null);
        } else {
            trailerImpl.setCreateDate(new Date(createDate));
        }

        trailerImpl.setModifierId(modifierId);

        if (modifierName == null) {
            trailerImpl.setModifierName(StringPool.BLANK);
        } else {
            trailerImpl.setModifierName(modifierName);
        }

        if (modifiedDate == Long.MIN_VALUE) {
            trailerImpl.setModifiedDate(null);
        } else {
            trailerImpl.setModifiedDate(new Date(modifiedDate));
        }

        trailerImpl.setDriverId(driverId);
        trailerImpl.setTypeId(typeId);
        trailerImpl.setManufacturerId(manufacturerId);
        trailerImpl.setOrganizationId(organizationId);

        if (modelName == null) {
            trailerImpl.setModelName(StringPool.BLANK);
        } else {
            trailerImpl.setModelName(modelName);
        }

        trailerImpl.setDimensionHeight(dimensionHeight);
        trailerImpl.setDimensionWidth(dimensionWidth);
        trailerImpl.setDimensionDepth(dimensionDepth);

        if (licensePlate == null) {
            trailerImpl.setLicensePlate(StringPool.BLANK);
        } else {
            trailerImpl.setLicensePlate(licensePlate);
        }

        trailerImpl.resetOriginalValues();

        return trailerImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        trailerId = objectInput.readLong();
        creatorId = objectInput.readLong();
        creatorName = objectInput.readUTF();
        createDate = objectInput.readLong();
        modifierId = objectInput.readLong();
        modifierName = objectInput.readUTF();
        modifiedDate = objectInput.readLong();
        driverId = objectInput.readLong();
        typeId = objectInput.readLong();
        manufacturerId = objectInput.readLong();
        organizationId = objectInput.readLong();
        modelName = objectInput.readUTF();
        dimensionHeight = objectInput.readLong();
        dimensionWidth = objectInput.readLong();
        dimensionDepth = objectInput.readLong();
        licensePlate = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(trailerId);
        objectOutput.writeLong(creatorId);

        if (creatorName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(creatorName);
        }

        objectOutput.writeLong(createDate);
        objectOutput.writeLong(modifierId);

        if (modifierName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(modifierName);
        }

        objectOutput.writeLong(modifiedDate);
        objectOutput.writeLong(driverId);
        objectOutput.writeLong(typeId);
        objectOutput.writeLong(manufacturerId);
        objectOutput.writeLong(organizationId);

        if (modelName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(modelName);
        }

        objectOutput.writeLong(dimensionHeight);
        objectOutput.writeLong(dimensionWidth);
        objectOutput.writeLong(dimensionDepth);

        if (licensePlate == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(licensePlate);
        }
    }
}
