package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import de.humance.eco.profile.model.TrailerType;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing TrailerType in entity cache.
 *
 * @author Humance
 * @see TrailerType
 * @generated
 */
public class TrailerTypeCacheModel implements CacheModel<TrailerType>,
    Externalizable {
    public long trailerTypeId;
    public long creatorId;
    public String creatorName;
    public long createDate;
    public long modifierId;
    public String modifierName;
    public long modifiedDate;
    public String name;
    public long iconId;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(19);

        sb.append("{trailerTypeId=");
        sb.append(trailerTypeId);
        sb.append(", creatorId=");
        sb.append(creatorId);
        sb.append(", creatorName=");
        sb.append(creatorName);
        sb.append(", createDate=");
        sb.append(createDate);
        sb.append(", modifierId=");
        sb.append(modifierId);
        sb.append(", modifierName=");
        sb.append(modifierName);
        sb.append(", modifiedDate=");
        sb.append(modifiedDate);
        sb.append(", name=");
        sb.append(name);
        sb.append(", iconId=");
        sb.append(iconId);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public TrailerType toEntityModel() {
        TrailerTypeImpl trailerTypeImpl = new TrailerTypeImpl();

        trailerTypeImpl.setTrailerTypeId(trailerTypeId);
        trailerTypeImpl.setCreatorId(creatorId);

        if (creatorName == null) {
            trailerTypeImpl.setCreatorName(StringPool.BLANK);
        } else {
            trailerTypeImpl.setCreatorName(creatorName);
        }

        if (createDate == Long.MIN_VALUE) {
            trailerTypeImpl.setCreateDate(null);
        } else {
            trailerTypeImpl.setCreateDate(new Date(createDate));
        }

        trailerTypeImpl.setModifierId(modifierId);

        if (modifierName == null) {
            trailerTypeImpl.setModifierName(StringPool.BLANK);
        } else {
            trailerTypeImpl.setModifierName(modifierName);
        }

        if (modifiedDate == Long.MIN_VALUE) {
            trailerTypeImpl.setModifiedDate(null);
        } else {
            trailerTypeImpl.setModifiedDate(new Date(modifiedDate));
        }

        if (name == null) {
            trailerTypeImpl.setName(StringPool.BLANK);
        } else {
            trailerTypeImpl.setName(name);
        }

        trailerTypeImpl.setIconId(iconId);

        trailerTypeImpl.resetOriginalValues();

        return trailerTypeImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        trailerTypeId = objectInput.readLong();
        creatorId = objectInput.readLong();
        creatorName = objectInput.readUTF();
        createDate = objectInput.readLong();
        modifierId = objectInput.readLong();
        modifierName = objectInput.readUTF();
        modifiedDate = objectInput.readLong();
        name = objectInput.readUTF();
        iconId = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(trailerTypeId);
        objectOutput.writeLong(creatorId);

        if (creatorName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(creatorName);
        }

        objectOutput.writeLong(createDate);
        objectOutput.writeLong(modifierId);

        if (modifierName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(modifierName);
        }

        objectOutput.writeLong(modifiedDate);

        if (name == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(name);
        }

        objectOutput.writeLong(iconId);
    }
}
