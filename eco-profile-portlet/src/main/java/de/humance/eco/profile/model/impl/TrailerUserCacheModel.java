package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import de.humance.eco.profile.model.TrailerUser;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing TrailerUser in entity cache.
 *
 * @author Humance
 * @see TrailerUser
 * @generated
 */
public class TrailerUserCacheModel implements CacheModel<TrailerUser>,
    Externalizable {
    public long trailerUserId;
    public long userId;
    public long trailerId;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{trailerUserId=");
        sb.append(trailerUserId);
        sb.append(", userId=");
        sb.append(userId);
        sb.append(", trailerId=");
        sb.append(trailerId);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public TrailerUser toEntityModel() {
        TrailerUserImpl trailerUserImpl = new TrailerUserImpl();

        trailerUserImpl.setTrailerUserId(trailerUserId);
        trailerUserImpl.setUserId(userId);
        trailerUserImpl.setTrailerId(trailerId);

        trailerUserImpl.resetOriginalValues();

        return trailerUserImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        trailerUserId = objectInput.readLong();
        userId = objectInput.readLong();
        trailerId = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(trailerUserId);
        objectOutput.writeLong(userId);
        objectOutput.writeLong(trailerId);
    }
}
