package de.humance.eco.profile.model.impl;

/**
 * The extended model implementation for the TrailerUser service. Represents a row in the &quot;Profile_TrailerUser&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link de.humance.eco.profile.model.TrailerUser} interface.
 * </p>
 *
 * @author Humance
 */
public class TrailerUserImpl extends TrailerUserBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a trailer user model instance should use the {@link de.humance.eco.profile.model.TrailerUser} interface instead.
     */
    public TrailerUserImpl() {
    }
}
