package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import de.humance.eco.profile.model.VehicleAttribute;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing VehicleAttribute in entity cache.
 *
 * @author Humance
 * @see VehicleAttribute
 * @generated
 */
public class VehicleAttributeCacheModel implements CacheModel<VehicleAttribute>,
    Externalizable {
    public long vehicleAttributeId;
    public long vehicleId;
    public long vehicleAttributeDefinitionId;
    public String type;
    public long sequenceNumber;
    public String attributeValue;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{vehicleAttributeId=");
        sb.append(vehicleAttributeId);
        sb.append(", vehicleId=");
        sb.append(vehicleId);
        sb.append(", vehicleAttributeDefinitionId=");
        sb.append(vehicleAttributeDefinitionId);
        sb.append(", type=");
        sb.append(type);
        sb.append(", sequenceNumber=");
        sb.append(sequenceNumber);
        sb.append(", attributeValue=");
        sb.append(attributeValue);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public VehicleAttribute toEntityModel() {
        VehicleAttributeImpl vehicleAttributeImpl = new VehicleAttributeImpl();

        vehicleAttributeImpl.setVehicleAttributeId(vehicleAttributeId);
        vehicleAttributeImpl.setVehicleId(vehicleId);
        vehicleAttributeImpl.setVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);

        if (type == null) {
            vehicleAttributeImpl.setType(StringPool.BLANK);
        } else {
            vehicleAttributeImpl.setType(type);
        }

        vehicleAttributeImpl.setSequenceNumber(sequenceNumber);

        if (attributeValue == null) {
            vehicleAttributeImpl.setAttributeValue(StringPool.BLANK);
        } else {
            vehicleAttributeImpl.setAttributeValue(attributeValue);
        }

        vehicleAttributeImpl.resetOriginalValues();

        return vehicleAttributeImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        vehicleAttributeId = objectInput.readLong();
        vehicleId = objectInput.readLong();
        vehicleAttributeDefinitionId = objectInput.readLong();
        type = objectInput.readUTF();
        sequenceNumber = objectInput.readLong();
        attributeValue = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(vehicleAttributeId);
        objectOutput.writeLong(vehicleId);
        objectOutput.writeLong(vehicleAttributeDefinitionId);

        if (type == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(type);
        }

        objectOutput.writeLong(sequenceNumber);

        if (attributeValue == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(attributeValue);
        }
    }
}
