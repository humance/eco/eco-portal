package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.VehicleAttributeDefinition;
import de.humance.eco.profile.service.VehicleAttributeDefinitionLocalServiceUtil;

/**
 * The extended model base implementation for the VehicleAttributeDefinition service. Represents a row in the &quot;Profile_VehicleAttributeDefinition&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This class exists only as a container for the default extended model level methods generated by ServiceBuilder. Helper methods and all application logic should be put in {@link VehicleAttributeDefinitionImpl}.
 * </p>
 *
 * @author Humance
 * @see VehicleAttributeDefinitionImpl
 * @see de.humance.eco.profile.model.VehicleAttributeDefinition
 * @generated
 */
public abstract class VehicleAttributeDefinitionBaseImpl
    extends VehicleAttributeDefinitionModelImpl
    implements VehicleAttributeDefinition {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. All methods that expect a vehicle attribute definition model instance should use the {@link VehicleAttributeDefinition} interface instead.
     */
    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            VehicleAttributeDefinitionLocalServiceUtil.addVehicleAttributeDefinition(this);
        } else {
            VehicleAttributeDefinitionLocalServiceUtil.updateVehicleAttributeDefinition(this);
        }
    }
}
