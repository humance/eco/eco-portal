package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import de.humance.eco.profile.model.VehicleAttributeDefinition;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing VehicleAttributeDefinition in entity cache.
 *
 * @author Humance
 * @see VehicleAttributeDefinition
 * @generated
 */
public class VehicleAttributeDefinitionCacheModel implements CacheModel<VehicleAttributeDefinition>,
    Externalizable {
    public long vehicleAttributeDefinitionId;
    public long creatorId;
    public String creatorName;
    public long createDate;
    public long modifierId;
    public String modifierName;
    public long modifiedDate;
    public long vehicleTypeId;
    public String type;
    public String title;
    public double minRangeValue;
    public double maxRangeValue;
    public String unit;
    public long sequenceNumber;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(29);

        sb.append("{vehicleAttributeDefinitionId=");
        sb.append(vehicleAttributeDefinitionId);
        sb.append(", creatorId=");
        sb.append(creatorId);
        sb.append(", creatorName=");
        sb.append(creatorName);
        sb.append(", createDate=");
        sb.append(createDate);
        sb.append(", modifierId=");
        sb.append(modifierId);
        sb.append(", modifierName=");
        sb.append(modifierName);
        sb.append(", modifiedDate=");
        sb.append(modifiedDate);
        sb.append(", vehicleTypeId=");
        sb.append(vehicleTypeId);
        sb.append(", type=");
        sb.append(type);
        sb.append(", title=");
        sb.append(title);
        sb.append(", minRangeValue=");
        sb.append(minRangeValue);
        sb.append(", maxRangeValue=");
        sb.append(maxRangeValue);
        sb.append(", unit=");
        sb.append(unit);
        sb.append(", sequenceNumber=");
        sb.append(sequenceNumber);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public VehicleAttributeDefinition toEntityModel() {
        VehicleAttributeDefinitionImpl vehicleAttributeDefinitionImpl = new VehicleAttributeDefinitionImpl();

        vehicleAttributeDefinitionImpl.setVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
        vehicleAttributeDefinitionImpl.setCreatorId(creatorId);

        if (creatorName == null) {
            vehicleAttributeDefinitionImpl.setCreatorName(StringPool.BLANK);
        } else {
            vehicleAttributeDefinitionImpl.setCreatorName(creatorName);
        }

        if (createDate == Long.MIN_VALUE) {
            vehicleAttributeDefinitionImpl.setCreateDate(null);
        } else {
            vehicleAttributeDefinitionImpl.setCreateDate(new Date(createDate));
        }

        vehicleAttributeDefinitionImpl.setModifierId(modifierId);

        if (modifierName == null) {
            vehicleAttributeDefinitionImpl.setModifierName(StringPool.BLANK);
        } else {
            vehicleAttributeDefinitionImpl.setModifierName(modifierName);
        }

        if (modifiedDate == Long.MIN_VALUE) {
            vehicleAttributeDefinitionImpl.setModifiedDate(null);
        } else {
            vehicleAttributeDefinitionImpl.setModifiedDate(new Date(
                    modifiedDate));
        }

        vehicleAttributeDefinitionImpl.setVehicleTypeId(vehicleTypeId);

        if (type == null) {
            vehicleAttributeDefinitionImpl.setType(StringPool.BLANK);
        } else {
            vehicleAttributeDefinitionImpl.setType(type);
        }

        if (title == null) {
            vehicleAttributeDefinitionImpl.setTitle(StringPool.BLANK);
        } else {
            vehicleAttributeDefinitionImpl.setTitle(title);
        }

        vehicleAttributeDefinitionImpl.setMinRangeValue(minRangeValue);
        vehicleAttributeDefinitionImpl.setMaxRangeValue(maxRangeValue);

        if (unit == null) {
            vehicleAttributeDefinitionImpl.setUnit(StringPool.BLANK);
        } else {
            vehicleAttributeDefinitionImpl.setUnit(unit);
        }

        vehicleAttributeDefinitionImpl.setSequenceNumber(sequenceNumber);

        vehicleAttributeDefinitionImpl.resetOriginalValues();

        return vehicleAttributeDefinitionImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        vehicleAttributeDefinitionId = objectInput.readLong();
        creatorId = objectInput.readLong();
        creatorName = objectInput.readUTF();
        createDate = objectInput.readLong();
        modifierId = objectInput.readLong();
        modifierName = objectInput.readUTF();
        modifiedDate = objectInput.readLong();
        vehicleTypeId = objectInput.readLong();
        type = objectInput.readUTF();
        title = objectInput.readUTF();
        minRangeValue = objectInput.readDouble();
        maxRangeValue = objectInput.readDouble();
        unit = objectInput.readUTF();
        sequenceNumber = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(vehicleAttributeDefinitionId);
        objectOutput.writeLong(creatorId);

        if (creatorName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(creatorName);
        }

        objectOutput.writeLong(createDate);
        objectOutput.writeLong(modifierId);

        if (modifierName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(modifierName);
        }

        objectOutput.writeLong(modifiedDate);
        objectOutput.writeLong(vehicleTypeId);

        if (type == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(type);
        }

        if (title == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(title);
        }

        objectOutput.writeDouble(minRangeValue);
        objectOutput.writeDouble(maxRangeValue);

        if (unit == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(unit);
        }

        objectOutput.writeLong(sequenceNumber);
    }
}
