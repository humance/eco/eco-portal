package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import de.humance.eco.profile.model.VehicleAttributeDefinitionResource;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing VehicleAttributeDefinitionResource in entity cache.
 *
 * @author Humance
 * @see VehicleAttributeDefinitionResource
 * @generated
 */
public class VehicleAttributeDefinitionResourceCacheModel implements CacheModel<VehicleAttributeDefinitionResource>,
    Externalizable {
    public long vehicleAttributeDefinitionResourceId;
    public long vehicleAttributeDefinitionId;
    public String country;
    public String language;
    public String title;
    public String unit;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{vehicleAttributeDefinitionResourceId=");
        sb.append(vehicleAttributeDefinitionResourceId);
        sb.append(", vehicleAttributeDefinitionId=");
        sb.append(vehicleAttributeDefinitionId);
        sb.append(", country=");
        sb.append(country);
        sb.append(", language=");
        sb.append(language);
        sb.append(", title=");
        sb.append(title);
        sb.append(", unit=");
        sb.append(unit);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public VehicleAttributeDefinitionResource toEntityModel() {
        VehicleAttributeDefinitionResourceImpl vehicleAttributeDefinitionResourceImpl =
            new VehicleAttributeDefinitionResourceImpl();

        vehicleAttributeDefinitionResourceImpl.setVehicleAttributeDefinitionResourceId(vehicleAttributeDefinitionResourceId);
        vehicleAttributeDefinitionResourceImpl.setVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);

        if (country == null) {
            vehicleAttributeDefinitionResourceImpl.setCountry(StringPool.BLANK);
        } else {
            vehicleAttributeDefinitionResourceImpl.setCountry(country);
        }

        if (language == null) {
            vehicleAttributeDefinitionResourceImpl.setLanguage(StringPool.BLANK);
        } else {
            vehicleAttributeDefinitionResourceImpl.setLanguage(language);
        }

        if (title == null) {
            vehicleAttributeDefinitionResourceImpl.setTitle(StringPool.BLANK);
        } else {
            vehicleAttributeDefinitionResourceImpl.setTitle(title);
        }

        if (unit == null) {
            vehicleAttributeDefinitionResourceImpl.setUnit(StringPool.BLANK);
        } else {
            vehicleAttributeDefinitionResourceImpl.setUnit(unit);
        }

        vehicleAttributeDefinitionResourceImpl.resetOriginalValues();

        return vehicleAttributeDefinitionResourceImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        vehicleAttributeDefinitionResourceId = objectInput.readLong();
        vehicleAttributeDefinitionId = objectInput.readLong();
        country = objectInput.readUTF();
        language = objectInput.readUTF();
        title = objectInput.readUTF();
        unit = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(vehicleAttributeDefinitionResourceId);
        objectOutput.writeLong(vehicleAttributeDefinitionId);

        if (country == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(country);
        }

        if (language == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(language);
        }

        if (title == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(title);
        }

        if (unit == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(unit);
        }
    }
}
