package de.humance.eco.profile.model.impl;

/**
 * The extended model implementation for the VehicleAttribute service. Represents a row in the &quot;Profile_VehicleAttribute&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link de.humance.eco.profile.model.VehicleAttribute} interface.
 * </p>
 *
 * @author Humance
 */
public class VehicleAttributeImpl extends VehicleAttributeBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a vehicle attribute model instance should use the {@link de.humance.eco.profile.model.VehicleAttribute} interface instead.
     */
    public VehicleAttributeImpl() {
    }
}
