package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import de.humance.eco.profile.model.Vehicle;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Vehicle in entity cache.
 *
 * @author Humance
 * @see Vehicle
 * @generated
 */
public class VehicleCacheModel implements CacheModel<Vehicle>, Externalizable {
    public long vehicleId;
    public long creatorId;
    public String creatorName;
    public long createDate;
    public long modifierId;
    public String modifierName;
    public long modifiedDate;
    public long driverId;
    public long typeId;
    public long manufacturerId;
    public long organizationId;
    public String modelName;
    public long dimensionHeight;
    public long dimensionWidth;
    public long dimensionDepth;
    public String licensePlate;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(33);

        sb.append("{vehicleId=");
        sb.append(vehicleId);
        sb.append(", creatorId=");
        sb.append(creatorId);
        sb.append(", creatorName=");
        sb.append(creatorName);
        sb.append(", createDate=");
        sb.append(createDate);
        sb.append(", modifierId=");
        sb.append(modifierId);
        sb.append(", modifierName=");
        sb.append(modifierName);
        sb.append(", modifiedDate=");
        sb.append(modifiedDate);
        sb.append(", driverId=");
        sb.append(driverId);
        sb.append(", typeId=");
        sb.append(typeId);
        sb.append(", manufacturerId=");
        sb.append(manufacturerId);
        sb.append(", organizationId=");
        sb.append(organizationId);
        sb.append(", modelName=");
        sb.append(modelName);
        sb.append(", dimensionHeight=");
        sb.append(dimensionHeight);
        sb.append(", dimensionWidth=");
        sb.append(dimensionWidth);
        sb.append(", dimensionDepth=");
        sb.append(dimensionDepth);
        sb.append(", licensePlate=");
        sb.append(licensePlate);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Vehicle toEntityModel() {
        VehicleImpl vehicleImpl = new VehicleImpl();

        vehicleImpl.setVehicleId(vehicleId);
        vehicleImpl.setCreatorId(creatorId);

        if (creatorName == null) {
            vehicleImpl.setCreatorName(StringPool.BLANK);
        } else {
            vehicleImpl.setCreatorName(creatorName);
        }

        if (createDate == Long.MIN_VALUE) {
            vehicleImpl.setCreateDate(null);
        } else {
            vehicleImpl.setCreateDate(new Date(createDate));
        }

        vehicleImpl.setModifierId(modifierId);

        if (modifierName == null) {
            vehicleImpl.setModifierName(StringPool.BLANK);
        } else {
            vehicleImpl.setModifierName(modifierName);
        }

        if (modifiedDate == Long.MIN_VALUE) {
            vehicleImpl.setModifiedDate(null);
        } else {
            vehicleImpl.setModifiedDate(new Date(modifiedDate));
        }

        vehicleImpl.setDriverId(driverId);
        vehicleImpl.setTypeId(typeId);
        vehicleImpl.setManufacturerId(manufacturerId);
        vehicleImpl.setOrganizationId(organizationId);

        if (modelName == null) {
            vehicleImpl.setModelName(StringPool.BLANK);
        } else {
            vehicleImpl.setModelName(modelName);
        }

        vehicleImpl.setDimensionHeight(dimensionHeight);
        vehicleImpl.setDimensionWidth(dimensionWidth);
        vehicleImpl.setDimensionDepth(dimensionDepth);

        if (licensePlate == null) {
            vehicleImpl.setLicensePlate(StringPool.BLANK);
        } else {
            vehicleImpl.setLicensePlate(licensePlate);
        }

        vehicleImpl.resetOriginalValues();

        return vehicleImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        vehicleId = objectInput.readLong();
        creatorId = objectInput.readLong();
        creatorName = objectInput.readUTF();
        createDate = objectInput.readLong();
        modifierId = objectInput.readLong();
        modifierName = objectInput.readUTF();
        modifiedDate = objectInput.readLong();
        driverId = objectInput.readLong();
        typeId = objectInput.readLong();
        manufacturerId = objectInput.readLong();
        organizationId = objectInput.readLong();
        modelName = objectInput.readUTF();
        dimensionHeight = objectInput.readLong();
        dimensionWidth = objectInput.readLong();
        dimensionDepth = objectInput.readLong();
        licensePlate = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(vehicleId);
        objectOutput.writeLong(creatorId);

        if (creatorName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(creatorName);
        }

        objectOutput.writeLong(createDate);
        objectOutput.writeLong(modifierId);

        if (modifierName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(modifierName);
        }

        objectOutput.writeLong(modifiedDate);
        objectOutput.writeLong(driverId);
        objectOutput.writeLong(typeId);
        objectOutput.writeLong(manufacturerId);
        objectOutput.writeLong(organizationId);

        if (modelName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(modelName);
        }

        objectOutput.writeLong(dimensionHeight);
        objectOutput.writeLong(dimensionWidth);
        objectOutput.writeLong(dimensionDepth);

        if (licensePlate == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(licensePlate);
        }
    }
}
