package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.VehicleManufacturer;
import de.humance.eco.profile.service.VehicleManufacturerLocalServiceUtil;

/**
 * The extended model base implementation for the VehicleManufacturer service. Represents a row in the &quot;Profile_VehicleManufacturer&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This class exists only as a container for the default extended model level methods generated by ServiceBuilder. Helper methods and all application logic should be put in {@link VehicleManufacturerImpl}.
 * </p>
 *
 * @author Humance
 * @see VehicleManufacturerImpl
 * @see de.humance.eco.profile.model.VehicleManufacturer
 * @generated
 */
public abstract class VehicleManufacturerBaseImpl
    extends VehicleManufacturerModelImpl implements VehicleManufacturer {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. All methods that expect a vehicle manufacturer model instance should use the {@link VehicleManufacturer} interface instead.
     */
    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            VehicleManufacturerLocalServiceUtil.addVehicleManufacturer(this);
        } else {
            VehicleManufacturerLocalServiceUtil.updateVehicleManufacturer(this);
        }
    }
}
