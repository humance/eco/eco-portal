package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import de.humance.eco.profile.model.VehicleManufacturer;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing VehicleManufacturer in entity cache.
 *
 * @author Humance
 * @see VehicleManufacturer
 * @generated
 */
public class VehicleManufacturerCacheModel implements CacheModel<VehicleManufacturer>,
    Externalizable {
    public long vehicleManufacturerId;
    public long creatorId;
    public String creatorName;
    public long createDate;
    public long modifierId;
    public String modifierName;
    public long modifiedDate;
    public String name;
    public long iconId;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(19);

        sb.append("{vehicleManufacturerId=");
        sb.append(vehicleManufacturerId);
        sb.append(", creatorId=");
        sb.append(creatorId);
        sb.append(", creatorName=");
        sb.append(creatorName);
        sb.append(", createDate=");
        sb.append(createDate);
        sb.append(", modifierId=");
        sb.append(modifierId);
        sb.append(", modifierName=");
        sb.append(modifierName);
        sb.append(", modifiedDate=");
        sb.append(modifiedDate);
        sb.append(", name=");
        sb.append(name);
        sb.append(", iconId=");
        sb.append(iconId);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public VehicleManufacturer toEntityModel() {
        VehicleManufacturerImpl vehicleManufacturerImpl = new VehicleManufacturerImpl();

        vehicleManufacturerImpl.setVehicleManufacturerId(vehicleManufacturerId);
        vehicleManufacturerImpl.setCreatorId(creatorId);

        if (creatorName == null) {
            vehicleManufacturerImpl.setCreatorName(StringPool.BLANK);
        } else {
            vehicleManufacturerImpl.setCreatorName(creatorName);
        }

        if (createDate == Long.MIN_VALUE) {
            vehicleManufacturerImpl.setCreateDate(null);
        } else {
            vehicleManufacturerImpl.setCreateDate(new Date(createDate));
        }

        vehicleManufacturerImpl.setModifierId(modifierId);

        if (modifierName == null) {
            vehicleManufacturerImpl.setModifierName(StringPool.BLANK);
        } else {
            vehicleManufacturerImpl.setModifierName(modifierName);
        }

        if (modifiedDate == Long.MIN_VALUE) {
            vehicleManufacturerImpl.setModifiedDate(null);
        } else {
            vehicleManufacturerImpl.setModifiedDate(new Date(modifiedDate));
        }

        if (name == null) {
            vehicleManufacturerImpl.setName(StringPool.BLANK);
        } else {
            vehicleManufacturerImpl.setName(name);
        }

        vehicleManufacturerImpl.setIconId(iconId);

        vehicleManufacturerImpl.resetOriginalValues();

        return vehicleManufacturerImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        vehicleManufacturerId = objectInput.readLong();
        creatorId = objectInput.readLong();
        creatorName = objectInput.readUTF();
        createDate = objectInput.readLong();
        modifierId = objectInput.readLong();
        modifierName = objectInput.readUTF();
        modifiedDate = objectInput.readLong();
        name = objectInput.readUTF();
        iconId = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(vehicleManufacturerId);
        objectOutput.writeLong(creatorId);

        if (creatorName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(creatorName);
        }

        objectOutput.writeLong(createDate);
        objectOutput.writeLong(modifierId);

        if (modifierName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(modifierName);
        }

        objectOutput.writeLong(modifiedDate);

        if (name == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(name);
        }

        objectOutput.writeLong(iconId);
    }
}
