package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import de.humance.eco.profile.model.VehicleType;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing VehicleType in entity cache.
 *
 * @author Humance
 * @see VehicleType
 * @generated
 */
public class VehicleTypeCacheModel implements CacheModel<VehicleType>,
    Externalizable {
    public long vehicleTypeId;
    public long creatorId;
    public String creatorName;
    public long createDate;
    public long modifierId;
    public String modifierName;
    public long modifiedDate;
    public String name;
    public long iconId;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(19);

        sb.append("{vehicleTypeId=");
        sb.append(vehicleTypeId);
        sb.append(", creatorId=");
        sb.append(creatorId);
        sb.append(", creatorName=");
        sb.append(creatorName);
        sb.append(", createDate=");
        sb.append(createDate);
        sb.append(", modifierId=");
        sb.append(modifierId);
        sb.append(", modifierName=");
        sb.append(modifierName);
        sb.append(", modifiedDate=");
        sb.append(modifiedDate);
        sb.append(", name=");
        sb.append(name);
        sb.append(", iconId=");
        sb.append(iconId);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public VehicleType toEntityModel() {
        VehicleTypeImpl vehicleTypeImpl = new VehicleTypeImpl();

        vehicleTypeImpl.setVehicleTypeId(vehicleTypeId);
        vehicleTypeImpl.setCreatorId(creatorId);

        if (creatorName == null) {
            vehicleTypeImpl.setCreatorName(StringPool.BLANK);
        } else {
            vehicleTypeImpl.setCreatorName(creatorName);
        }

        if (createDate == Long.MIN_VALUE) {
            vehicleTypeImpl.setCreateDate(null);
        } else {
            vehicleTypeImpl.setCreateDate(new Date(createDate));
        }

        vehicleTypeImpl.setModifierId(modifierId);

        if (modifierName == null) {
            vehicleTypeImpl.setModifierName(StringPool.BLANK);
        } else {
            vehicleTypeImpl.setModifierName(modifierName);
        }

        if (modifiedDate == Long.MIN_VALUE) {
            vehicleTypeImpl.setModifiedDate(null);
        } else {
            vehicleTypeImpl.setModifiedDate(new Date(modifiedDate));
        }

        if (name == null) {
            vehicleTypeImpl.setName(StringPool.BLANK);
        } else {
            vehicleTypeImpl.setName(name);
        }

        vehicleTypeImpl.setIconId(iconId);

        vehicleTypeImpl.resetOriginalValues();

        return vehicleTypeImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        vehicleTypeId = objectInput.readLong();
        creatorId = objectInput.readLong();
        creatorName = objectInput.readUTF();
        createDate = objectInput.readLong();
        modifierId = objectInput.readLong();
        modifierName = objectInput.readUTF();
        modifiedDate = objectInput.readLong();
        name = objectInput.readUTF();
        iconId = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(vehicleTypeId);
        objectOutput.writeLong(creatorId);

        if (creatorName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(creatorName);
        }

        objectOutput.writeLong(createDate);
        objectOutput.writeLong(modifierId);

        if (modifierName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(modifierName);
        }

        objectOutput.writeLong(modifiedDate);

        if (name == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(name);
        }

        objectOutput.writeLong(iconId);
    }
}
