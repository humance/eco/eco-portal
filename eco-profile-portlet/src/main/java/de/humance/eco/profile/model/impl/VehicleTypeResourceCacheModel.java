package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import de.humance.eco.profile.model.VehicleTypeResource;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing VehicleTypeResource in entity cache.
 *
 * @author Humance
 * @see VehicleTypeResource
 * @generated
 */
public class VehicleTypeResourceCacheModel implements CacheModel<VehicleTypeResource>,
    Externalizable {
    public long vehicleTypeResourceId;
    public long vehicleTypeId;
    public String country;
    public String language;
    public String name;
    public long iconId;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{vehicleTypeResourceId=");
        sb.append(vehicleTypeResourceId);
        sb.append(", vehicleTypeId=");
        sb.append(vehicleTypeId);
        sb.append(", country=");
        sb.append(country);
        sb.append(", language=");
        sb.append(language);
        sb.append(", name=");
        sb.append(name);
        sb.append(", iconId=");
        sb.append(iconId);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public VehicleTypeResource toEntityModel() {
        VehicleTypeResourceImpl vehicleTypeResourceImpl = new VehicleTypeResourceImpl();

        vehicleTypeResourceImpl.setVehicleTypeResourceId(vehicleTypeResourceId);
        vehicleTypeResourceImpl.setVehicleTypeId(vehicleTypeId);

        if (country == null) {
            vehicleTypeResourceImpl.setCountry(StringPool.BLANK);
        } else {
            vehicleTypeResourceImpl.setCountry(country);
        }

        if (language == null) {
            vehicleTypeResourceImpl.setLanguage(StringPool.BLANK);
        } else {
            vehicleTypeResourceImpl.setLanguage(language);
        }

        if (name == null) {
            vehicleTypeResourceImpl.setName(StringPool.BLANK);
        } else {
            vehicleTypeResourceImpl.setName(name);
        }

        vehicleTypeResourceImpl.setIconId(iconId);

        vehicleTypeResourceImpl.resetOriginalValues();

        return vehicleTypeResourceImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        vehicleTypeResourceId = objectInput.readLong();
        vehicleTypeId = objectInput.readLong();
        country = objectInput.readUTF();
        language = objectInput.readUTF();
        name = objectInput.readUTF();
        iconId = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(vehicleTypeResourceId);
        objectOutput.writeLong(vehicleTypeId);

        if (country == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(country);
        }

        if (language == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(language);
        }

        if (name == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(name);
        }

        objectOutput.writeLong(iconId);
    }
}
