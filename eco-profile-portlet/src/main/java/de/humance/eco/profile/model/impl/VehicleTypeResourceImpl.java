package de.humance.eco.profile.model.impl;

/**
 * The extended model implementation for the VehicleTypeResource service. Represents a row in the &quot;Profile_VehicleTypeResource&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link de.humance.eco.profile.model.VehicleTypeResource} interface.
 * </p>
 *
 * @author Humance
 */
public class VehicleTypeResourceImpl extends VehicleTypeResourceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a vehicle type resource model instance should use the {@link de.humance.eco.profile.model.VehicleTypeResource} interface instead.
     */
    public VehicleTypeResourceImpl() {
    }
}
