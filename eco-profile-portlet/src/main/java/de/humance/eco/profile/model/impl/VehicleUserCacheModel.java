package de.humance.eco.profile.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import de.humance.eco.profile.model.VehicleUser;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing VehicleUser in entity cache.
 *
 * @author Humance
 * @see VehicleUser
 * @generated
 */
public class VehicleUserCacheModel implements CacheModel<VehicleUser>,
    Externalizable {
    public long vehicleUserId;
    public long userId;
    public long vehicleId;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{vehicleUserId=");
        sb.append(vehicleUserId);
        sb.append(", userId=");
        sb.append(userId);
        sb.append(", vehicleId=");
        sb.append(vehicleId);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public VehicleUser toEntityModel() {
        VehicleUserImpl vehicleUserImpl = new VehicleUserImpl();

        vehicleUserImpl.setVehicleUserId(vehicleUserId);
        vehicleUserImpl.setUserId(userId);
        vehicleUserImpl.setVehicleId(vehicleId);

        vehicleUserImpl.resetOriginalValues();

        return vehicleUserImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        vehicleUserId = objectInput.readLong();
        userId = objectInput.readLong();
        vehicleId = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(vehicleUserId);
        objectOutput.writeLong(userId);
        objectOutput.writeLong(vehicleId);
    }
}
