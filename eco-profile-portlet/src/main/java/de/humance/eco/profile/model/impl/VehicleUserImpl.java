package de.humance.eco.profile.model.impl;

/**
 * The extended model implementation for the VehicleUser service. Represents a row in the &quot;Profile_VehicleUser&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link de.humance.eco.profile.model.VehicleUser} interface.
 * </p>
 *
 * @author Humance
 */
public class VehicleUserImpl extends VehicleUserBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a vehicle user model instance should use the {@link de.humance.eco.profile.model.VehicleUser} interface instead.
     */
    public VehicleUserImpl() {
    }
}
