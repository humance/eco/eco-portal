package de.humance.eco.profile.service.base;

import de.humance.eco.profile.service.QualificationAttributeDefinitionResourceLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Humance
 * @generated
 */
public class QualificationAttributeDefinitionResourceLocalServiceClpInvoker {
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName96;
    private String[] _methodParameterTypes96;
    private String _methodName97;
    private String[] _methodParameterTypes97;
    private String _methodName102;
    private String[] _methodParameterTypes102;
    private String _methodName103;
    private String[] _methodParameterTypes103;
    private String _methodName104;
    private String[] _methodParameterTypes104;
    private String _methodName105;
    private String[] _methodParameterTypes105;
    private String _methodName106;
    private String[] _methodParameterTypes106;
    private String _methodName107;
    private String[] _methodParameterTypes107;

    public QualificationAttributeDefinitionResourceLocalServiceClpInvoker() {
        _methodName0 = "addQualificationAttributeDefinitionResource";

        _methodParameterTypes0 = new String[] {
                "de.humance.eco.profile.model.QualificationAttributeDefinitionResource"
            };

        _methodName1 = "createQualificationAttributeDefinitionResource";

        _methodParameterTypes1 = new String[] { "long" };

        _methodName2 = "deleteQualificationAttributeDefinitionResource";

        _methodParameterTypes2 = new String[] { "long" };

        _methodName3 = "deleteQualificationAttributeDefinitionResource";

        _methodParameterTypes3 = new String[] {
                "de.humance.eco.profile.model.QualificationAttributeDefinitionResource"
            };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "dynamicQueryCount";

        _methodParameterTypes9 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery",
                "com.liferay.portal.kernel.dao.orm.Projection"
            };

        _methodName10 = "fetchQualificationAttributeDefinitionResource";

        _methodParameterTypes10 = new String[] { "long" };

        _methodName11 = "getQualificationAttributeDefinitionResource";

        _methodParameterTypes11 = new String[] { "long" };

        _methodName12 = "getPersistedModel";

        _methodParameterTypes12 = new String[] { "java.io.Serializable" };

        _methodName13 = "getQualificationAttributeDefinitionResources";

        _methodParameterTypes13 = new String[] { "int", "int" };

        _methodName14 = "getQualificationAttributeDefinitionResourcesCount";

        _methodParameterTypes14 = new String[] {  };

        _methodName15 = "updateQualificationAttributeDefinitionResource";

        _methodParameterTypes15 = new String[] {
                "de.humance.eco.profile.model.QualificationAttributeDefinitionResource"
            };

        _methodName96 = "getBeanIdentifier";

        _methodParameterTypes96 = new String[] {  };

        _methodName97 = "setBeanIdentifier";

        _methodParameterTypes97 = new String[] { "java.lang.String" };

        _methodName102 = "findByDefinitionId";

        _methodParameterTypes102 = new String[] { "long" };

        _methodName103 = "findByDefinitionIdAndCoutry";

        _methodParameterTypes103 = new String[] { "long", "java.lang.String" };

        _methodName104 = "findByDefinitionIdAndLanguage";

        _methodParameterTypes104 = new String[] { "long", "java.lang.String" };

        _methodName105 = "findByDefinitionIdAndLocale";

        _methodParameterTypes105 = new String[] {
                "long", "java.lang.String", "java.lang.String"
            };

        _methodName106 = "addQualificationAttributeDefinitionResource";

        _methodParameterTypes106 = new String[] {
                "long", "java.lang.String", "java.lang.String",
                "java.lang.String", "java.lang.String"
            };

        _methodName107 = "updateQualificationAttributeDefinitionResource";

        _methodParameterTypes107 = new String[] {
                "long", "long", "java.lang.String", "java.lang.String",
                "java.lang.String", "java.lang.String"
            };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName0.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.addQualificationAttributeDefinitionResource((de.humance.eco.profile.model.QualificationAttributeDefinitionResource) arguments[0]);
        }

        if (_methodName1.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.createQualificationAttributeDefinitionResource(((Long) arguments[0]).longValue());
        }

        if (_methodName2.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.deleteQualificationAttributeDefinitionResource(((Long) arguments[0]).longValue());
        }

        if (_methodName3.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.deleteQualificationAttributeDefinitionResource((de.humance.eco.profile.model.QualificationAttributeDefinitionResource) arguments[0]);
        }

        if (_methodName4.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.dynamicQuery();
        }

        if (_methodName5.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName6.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName7.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[3]);
        }

        if (_methodName8.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName9.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                (com.liferay.portal.kernel.dao.orm.Projection) arguments[1]);
        }

        if (_methodName10.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.fetchQualificationAttributeDefinitionResource(((Long) arguments[0]).longValue());
        }

        if (_methodName11.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.getQualificationAttributeDefinitionResource(((Long) arguments[0]).longValue());
        }

        if (_methodName12.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.getPersistedModel((java.io.Serializable) arguments[0]);
        }

        if (_methodName13.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.getQualificationAttributeDefinitionResources(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName14.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.getQualificationAttributeDefinitionResourcesCount();
        }

        if (_methodName15.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.updateQualificationAttributeDefinitionResource((de.humance.eco.profile.model.QualificationAttributeDefinitionResource) arguments[0]);
        }

        if (_methodName96.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes96, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName97.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes97, parameterTypes)) {
            QualificationAttributeDefinitionResourceLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName102.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes102, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.findByDefinitionId(((Long) arguments[0]).longValue());
        }

        if (_methodName103.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes103, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.findByDefinitionIdAndCoutry(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1]);
        }

        if (_methodName104.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes104, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.findByDefinitionIdAndLanguage(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1]);
        }

        if (_methodName105.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes105, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.findByDefinitionIdAndLocale(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1], (java.lang.String) arguments[2]);
        }

        if (_methodName106.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes106, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.addQualificationAttributeDefinitionResource(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1],
                (java.lang.String) arguments[2],
                (java.lang.String) arguments[3], (java.lang.String) arguments[4]);
        }

        if (_methodName107.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes107, parameterTypes)) {
            return QualificationAttributeDefinitionResourceLocalServiceUtil.updateQualificationAttributeDefinitionResource(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue(),
                (java.lang.String) arguments[2],
                (java.lang.String) arguments[3],
                (java.lang.String) arguments[4], (java.lang.String) arguments[5]);
        }

        throw new UnsupportedOperationException();
    }
}
