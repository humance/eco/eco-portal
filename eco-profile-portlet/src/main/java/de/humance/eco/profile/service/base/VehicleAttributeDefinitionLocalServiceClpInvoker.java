package de.humance.eco.profile.service.base;

import de.humance.eco.profile.service.VehicleAttributeDefinitionLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Humance
 * @generated
 */
public class VehicleAttributeDefinitionLocalServiceClpInvoker {
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName96;
    private String[] _methodParameterTypes96;
    private String _methodName97;
    private String[] _methodParameterTypes97;
    private String _methodName102;
    private String[] _methodParameterTypes102;
    private String _methodName103;
    private String[] _methodParameterTypes103;
    private String _methodName104;
    private String[] _methodParameterTypes104;
    private String _methodName105;
    private String[] _methodParameterTypes105;
    private String _methodName106;
    private String[] _methodParameterTypes106;
    private String _methodName107;
    private String[] _methodParameterTypes107;

    public VehicleAttributeDefinitionLocalServiceClpInvoker() {
        _methodName0 = "addVehicleAttributeDefinition";

        _methodParameterTypes0 = new String[] {
                "de.humance.eco.profile.model.VehicleAttributeDefinition"
            };

        _methodName1 = "createVehicleAttributeDefinition";

        _methodParameterTypes1 = new String[] { "long" };

        _methodName2 = "deleteVehicleAttributeDefinition";

        _methodParameterTypes2 = new String[] { "long" };

        _methodName3 = "deleteVehicleAttributeDefinition";

        _methodParameterTypes3 = new String[] {
                "de.humance.eco.profile.model.VehicleAttributeDefinition"
            };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "dynamicQueryCount";

        _methodParameterTypes9 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery",
                "com.liferay.portal.kernel.dao.orm.Projection"
            };

        _methodName10 = "fetchVehicleAttributeDefinition";

        _methodParameterTypes10 = new String[] { "long" };

        _methodName11 = "getVehicleAttributeDefinition";

        _methodParameterTypes11 = new String[] { "long" };

        _methodName12 = "getPersistedModel";

        _methodParameterTypes12 = new String[] { "java.io.Serializable" };

        _methodName13 = "getVehicleAttributeDefinitions";

        _methodParameterTypes13 = new String[] { "int", "int" };

        _methodName14 = "getVehicleAttributeDefinitionsCount";

        _methodParameterTypes14 = new String[] {  };

        _methodName15 = "updateVehicleAttributeDefinition";

        _methodParameterTypes15 = new String[] {
                "de.humance.eco.profile.model.VehicleAttributeDefinition"
            };

        _methodName96 = "getBeanIdentifier";

        _methodParameterTypes96 = new String[] {  };

        _methodName97 = "setBeanIdentifier";

        _methodParameterTypes97 = new String[] { "java.lang.String" };

        _methodName102 = "findByTitle";

        _methodParameterTypes102 = new String[] { "java.lang.String" };

        _methodName103 = "findByTitleLike";

        _methodParameterTypes103 = new String[] { "java.lang.String" };

        _methodName104 = "findByVehicleTypeId";

        _methodParameterTypes104 = new String[] { "long" };

        _methodName105 = "findByModifiedDate";

        _methodParameterTypes105 = new String[] { "java.util.Date" };

        _methodName106 = "addVehicleAttributeDefinition";

        _methodParameterTypes106 = new String[] {
                "long", "long", "java.lang.String", "java.lang.String", "double",
                "double", "java.lang.String", "long"
            };

        _methodName107 = "updateVehicleAttributeDefinition";

        _methodParameterTypes107 = new String[] {
                "long", "long", "long", "java.lang.String", "java.lang.String",
                "double", "double", "java.lang.String", "long"
            };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName0.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.addVehicleAttributeDefinition((de.humance.eco.profile.model.VehicleAttributeDefinition) arguments[0]);
        }

        if (_methodName1.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.createVehicleAttributeDefinition(((Long) arguments[0]).longValue());
        }

        if (_methodName2.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.deleteVehicleAttributeDefinition(((Long) arguments[0]).longValue());
        }

        if (_methodName3.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.deleteVehicleAttributeDefinition((de.humance.eco.profile.model.VehicleAttributeDefinition) arguments[0]);
        }

        if (_methodName4.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.dynamicQuery();
        }

        if (_methodName5.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName6.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName7.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[3]);
        }

        if (_methodName8.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName9.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                (com.liferay.portal.kernel.dao.orm.Projection) arguments[1]);
        }

        if (_methodName10.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.fetchVehicleAttributeDefinition(((Long) arguments[0]).longValue());
        }

        if (_methodName11.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.getVehicleAttributeDefinition(((Long) arguments[0]).longValue());
        }

        if (_methodName12.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.getPersistedModel((java.io.Serializable) arguments[0]);
        }

        if (_methodName13.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.getVehicleAttributeDefinitions(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName14.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.getVehicleAttributeDefinitionsCount();
        }

        if (_methodName15.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.updateVehicleAttributeDefinition((de.humance.eco.profile.model.VehicleAttributeDefinition) arguments[0]);
        }

        if (_methodName96.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes96, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName97.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes97, parameterTypes)) {
            VehicleAttributeDefinitionLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName102.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes102, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.findByTitle((java.lang.String) arguments[0]);
        }

        if (_methodName103.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes103, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.findByTitleLike((java.lang.String) arguments[0]);
        }

        if (_methodName104.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes104, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.findByVehicleTypeId(((Long) arguments[0]).longValue());
        }

        if (_methodName105.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes105, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.findByModifiedDate((java.util.Date) arguments[0]);
        }

        if (_methodName106.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes106, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.addVehicleAttributeDefinition(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue(),
                (java.lang.String) arguments[2],
                (java.lang.String) arguments[3],
                ((Double) arguments[4]).doubleValue(),
                ((Double) arguments[5]).doubleValue(),
                (java.lang.String) arguments[6],
                ((Long) arguments[7]).longValue());
        }

        if (_methodName107.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes107, parameterTypes)) {
            return VehicleAttributeDefinitionLocalServiceUtil.updateVehicleAttributeDefinition(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue(),
                ((Long) arguments[2]).longValue(),
                (java.lang.String) arguments[3],
                (java.lang.String) arguments[4],
                ((Double) arguments[5]).doubleValue(),
                ((Double) arguments[6]).doubleValue(),
                (java.lang.String) arguments[7],
                ((Long) arguments[8]).longValue());
        }

        throw new UnsupportedOperationException();
    }
}
