package de.humance.eco.profile.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

import de.humance.eco.profile.model.QualificationAttributeDefinition;
import de.humance.eco.profile.service.QualificationAttributeDefinitionLocalServiceUtil;
import de.humance.eco.profile.service.base.QualificationAttributeDefinitionLocalServiceBaseImpl;

/**
 * The implementation of the qualification attribute definition local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link de.humance.eco.profile.service.QualificationAttributeDefinitionLocalService}
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Humance
 * @see de.humance.eco.profile.service.base.QualificationAttributeDefinitionLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.QualificationAttributeDefinitionLocalServiceUtil
 */
public class QualificationAttributeDefinitionLocalServiceImpl extends
		QualificationAttributeDefinitionLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * de.humance.eco.
	 * profile.service.QualificationAttributeDefinitionLocalServiceUtil} to
	 * access the qualification attribute definition local service.
	 */

	public List<QualificationAttributeDefinition> findByQualificationTypeId(
			long qualificationTypeId) throws SystemException {
		return qualificationAttributeDefinitionPersistence
				.findByQualificationTypeId(qualificationTypeId);
	}

	public List<QualificationAttributeDefinition> findByTitle(String title)
			throws SystemException {
		return qualificationAttributeDefinitionPersistence.findByTitle(title);
	}

	public List<QualificationAttributeDefinition> findByTitleLike(String title)
			throws SystemException {
		return qualificationAttributeDefinitionPersistence
				.findByTitleLike(title);
	}

	public List<QualificationAttributeDefinition> findByModifiedDate(
			Date modifiedDate) throws SystemException {
		List<QualificationAttributeDefinition> result = new ArrayList<QualificationAttributeDefinition>();
		List<QualificationAttributeDefinition> qualificationAttributeDefinitions = QualificationAttributeDefinitionLocalServiceUtil
				.getQualificationAttributeDefinitions(0,
						QualificationAttributeDefinitionLocalServiceUtil
								.getQualificationAttributeDefinitionsCount());
		for (QualificationAttributeDefinition qualificationAttributeDefinition : qualificationAttributeDefinitions) {
			if (modifiedDate.before(qualificationAttributeDefinition
					.getModifiedDate())) {
				result.add(qualificationAttributeDefinition);
			}
		}
		return result;
	}

	public QualificationAttributeDefinition addQualificationAttributeDefinition(
			long creatorId, long qualificationTypeId, String type,
			String title, double minRangeValue, double maxRangeValue,
			String unit, long sequenceNumber) throws PortalException,
			SystemException {

		User user = UserLocalServiceUtil.getUser(creatorId);

		// QualificationAttributeDefinition
		long qualificationAttributeDefinitionId = counterLocalService
				.increment();
		QualificationAttributeDefinition qualificationAttributeDefinition = qualificationAttributeDefinitionPersistence
				.create(qualificationAttributeDefinitionId);
		qualificationAttributeDefinition
				.setQualificationTypeId(qualificationTypeId);
		qualificationAttributeDefinition.setType(type);
		qualificationAttributeDefinition.setTitle(title);
		qualificationAttributeDefinition.setMinRangeValue(minRangeValue);
		qualificationAttributeDefinition.setMaxRangeValue(maxRangeValue);
		qualificationAttributeDefinition.setUnit(unit);
		qualificationAttributeDefinition.setSequenceNumber(sequenceNumber);
		qualificationAttributeDefinition.setCreatorId(creatorId);
		qualificationAttributeDefinition.setCreatorName(user.getScreenName());
		qualificationAttributeDefinition.setCreateDate(new Date());
		qualificationAttributeDefinition.setModifierId(creatorId);
		qualificationAttributeDefinition.setModifierName(user.getScreenName());
		qualificationAttributeDefinition.setModifiedDate(new Date());

		qualificationAttributeDefinitionPersistence
				.update(qualificationAttributeDefinition);

		return qualificationAttributeDefinition;
	}

	public QualificationAttributeDefinition updateQualificationAttributeDefinition(
			long modifierId, long qualificationAttributeDefinitionId,
			long qualificationTypeId, String type, String title,
			double minRangeValue, double maxRangeValue, String unit,
			long sequenceNumber) throws PortalException, SystemException {

		User user = UserLocalServiceUtil.getUser(modifierId);

		// QualificationAttributeDefinition
		QualificationAttributeDefinition qualificationAttributeDefinition = qualificationAttributeDefinitionPersistence
				.findByPrimaryKey(qualificationAttributeDefinitionId);
		qualificationAttributeDefinition
				.setQualificationTypeId(qualificationTypeId);
		qualificationAttributeDefinition.setType(type);
		qualificationAttributeDefinition.setTitle(title);
		qualificationAttributeDefinition.setMinRangeValue(minRangeValue);
		qualificationAttributeDefinition.setMaxRangeValue(maxRangeValue);
		qualificationAttributeDefinition.setUnit(unit);
		qualificationAttributeDefinition.setSequenceNumber(sequenceNumber);
		qualificationAttributeDefinition.setModifierId(modifierId);
		qualificationAttributeDefinition.setModifierName(user.getScreenName());
		qualificationAttributeDefinition.setModifiedDate(new Date());

		qualificationAttributeDefinitionPersistence
				.update(qualificationAttributeDefinition);

		return qualificationAttributeDefinition;
	}
}
