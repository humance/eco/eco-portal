package de.humance.eco.profile.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.QualificationAttributeDefinitionResource;
import de.humance.eco.profile.service.base.QualificationAttributeDefinitionResourceLocalServiceBaseImpl;

/**
 * The implementation of the qualification attribute definition resource local
 * service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link de.humance.eco.profile.service.QualificationAttributeDefinitionResourceLocalService}
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Humance
 * @see de.humance.eco.profile.service.base.QualificationAttributeDefinitionResourceLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.QualificationAttributeDefinitionResourceLocalServiceUtil
 */
public class QualificationAttributeDefinitionResourceLocalServiceImpl extends
		QualificationAttributeDefinitionResourceLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * de.humance.eco.
	 * profile.service.QualificationAttributeDefinitionResourceLocalServiceUtil}
	 * to access the qualification attribute definition resource local service.
	 */

	public List<QualificationAttributeDefinitionResource> findByDefinitionId(
			long qualificationAttributeDefinitionId) throws SystemException {
		return qualificationAttributeDefinitionResourcePersistence
				.findByQualificationAttribDefId(qualificationAttributeDefinitionId);
	}

	public List<QualificationAttributeDefinitionResource> findByDefinitionIdAndCoutry(
			long qualificationAttributeDefinitionId, String country)
			throws SystemException {
		return qualificationAttributeDefinitionResourcePersistence
				.findByQualificationAttribDefIdAndCountry(
						qualificationAttributeDefinitionId, country);
	}

	public List<QualificationAttributeDefinitionResource> findByDefinitionIdAndLanguage(
			long qualificationAttributeDefinitionId, String language)
			throws SystemException {
		return qualificationAttributeDefinitionResourcePersistence
				.findByQualificationAttribDefIdAndLanguage(
						qualificationAttributeDefinitionId, language);
	}

	public List<QualificationAttributeDefinitionResource> findByDefinitionIdAndLocale(
			long qualificationAttributeDefinitionId, String country,
			String language) throws SystemException {
		return qualificationAttributeDefinitionResourcePersistence
				.findByQualificationAttribDefIdAndLocale(
						qualificationAttributeDefinitionId, country, language);
	}

	public QualificationAttributeDefinitionResource addQualificationAttributeDefinitionResource(
			long qualificationAttributeDefinitionId, String country,
			String language, String title, String unit) throws PortalException,
			SystemException {

		// QualificationAttributeDefinitionResource
		long qualificationAttributeDefinitionResourceId = counterLocalService
				.increment();
		QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource = qualificationAttributeDefinitionResourcePersistence
				.create(qualificationAttributeDefinitionResourceId);
		qualificationAttributeDefinitionResource
				.setQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);
		qualificationAttributeDefinitionResource.setCountry(country);
		qualificationAttributeDefinitionResource.setLanguage(language);
		qualificationAttributeDefinitionResource.setTitle(title);
		qualificationAttributeDefinitionResource.setUnit(unit);
		qualificationAttributeDefinitionResourcePersistence
				.update(qualificationAttributeDefinitionResource);

		return qualificationAttributeDefinitionResource;
	}

	public QualificationAttributeDefinitionResource updateQualificationAttributeDefinitionResource(
			long qualificationAttributeDefinitionResourceId,
			long qualificationAttributeDefinitionId, String country,
			String language, String title, String unit) throws PortalException,
			SystemException {

		// QualificationAttributeDefinitionResource
		QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource = qualificationAttributeDefinitionResourcePersistence
				.findByPrimaryKey(qualificationAttributeDefinitionResourceId);
		qualificationAttributeDefinitionResource
				.setQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);
		qualificationAttributeDefinitionResource.setCountry(country);
		qualificationAttributeDefinitionResource.setLanguage(language);
		qualificationAttributeDefinitionResource.setTitle(title);
		qualificationAttributeDefinitionResource.setUnit(unit);
		qualificationAttributeDefinitionResourcePersistence
				.update(qualificationAttributeDefinitionResource);

		return qualificationAttributeDefinitionResource;
	}
}
