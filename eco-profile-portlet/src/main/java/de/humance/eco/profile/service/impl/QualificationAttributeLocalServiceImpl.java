package de.humance.eco.profile.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.QualificationAttribute;
import de.humance.eco.profile.service.base.QualificationAttributeLocalServiceBaseImpl;

/**
 * The implementation of the qualification attribute local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link de.humance.eco.profile.service.QualificationAttributeLocalService}
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Humance
 * @see de.humance.eco.profile.service.base.QualificationAttributeLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.QualificationAttributeLocalServiceUtil
 */
public class QualificationAttributeLocalServiceImpl extends
		QualificationAttributeLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * de.humance.eco.profile.service.QualificationAttributeLocalServiceUtil} to
	 * access the qualification attribute local service.
	 */

	public List<QualificationAttribute> findByAttributeDefinitionId(
			long qualificationAttributeDefinitionId) throws SystemException {
		return qualificationAttributePersistence
				.findByQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);
	}

	public List<QualificationAttribute> findByQualificationId(
			long qualificationId) throws SystemException {
		return qualificationAttributePersistence
				.findByQualificationId(qualificationId);
	}

	public List<QualificationAttribute> findByQualificationIdAndAttributeDefinitionId(
			long qualificationId, long qualificationAttributeDefinitionId)
			throws SystemException {
		return qualificationAttributePersistence
				.findByQualificationIdAndAttributeDefinitionId(qualificationId,
						qualificationAttributeDefinitionId);
	}

	public QualificationAttribute addQualificationAttribute(
			long qualificationId, long qualificationAttributeDefinitionId,
			String type, long sequenceNumber, String attributeValue)
			throws PortalException, SystemException {

		// QualificationAttribute
		long qualificationAttributeId = counterLocalService.increment();
		QualificationAttribute qualificationAttribute = qualificationAttributePersistence
				.create(qualificationAttributeId);
		qualificationAttribute.setQualificationId(qualificationId);
		qualificationAttribute
				.setQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);
		qualificationAttribute.setType(type);
		qualificationAttribute.setSequenceNumber(sequenceNumber);
		qualificationAttribute.setAttributeValue(attributeValue);
		qualificationAttributePersistence.update(qualificationAttribute);

		return qualificationAttribute;
	}

	public QualificationAttribute updateQualificationAttribute(
			long qualificationAttributeId, long qualificationId,
			long qualificationAttributeDefinitionId, String type,
			long sequenceNumber, String attributeValue) throws PortalException,
			SystemException {

		// QualificationAttribute
		QualificationAttribute qualificationAttribute = qualificationAttributePersistence
				.findByPrimaryKey(qualificationAttributeId);
		qualificationAttribute.setQualificationId(qualificationId);
		qualificationAttribute
				.setQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);
		qualificationAttribute.setType(type);
		qualificationAttribute.setSequenceNumber(sequenceNumber);
		qualificationAttribute.setAttributeValue(attributeValue);
		qualificationAttributePersistence.update(qualificationAttribute);

		return qualificationAttribute;
	}
}
