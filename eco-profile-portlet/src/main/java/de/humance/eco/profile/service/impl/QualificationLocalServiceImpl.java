package de.humance.eco.profile.service.impl;

import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.Qualification;
import de.humance.eco.profile.service.base.QualificationLocalServiceBaseImpl;

/**
 * The implementation of the qualification local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link de.humance.eco.profile.service.QualificationLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Humance
 * @see de.humance.eco.profile.service.base.QualificationLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.QualificationLocalServiceUtil
 */
public class QualificationLocalServiceImpl extends
		QualificationLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * de.humance.eco.profile.service.QualificationLocalServiceUtil} to access
	 * the qualification local service.
	 */

	public List<Qualification> findByOwnerId(long ownerId)
			throws SystemException {
		return qualificationPersistence.findByOwnerId(ownerId);
	}

	public List<Qualification> findByTypeId(long typeId) throws SystemException {
		return qualificationPersistence.findByTypeId(typeId);
	}

	public List<Qualification> findByOwnerIdAndTypeId(long ownerId, long typeId)
			throws SystemException {
		return qualificationPersistence.findByOwnerIdAndTypeId(ownerId, typeId);
	}

	public Qualification addQualification(long creatorId, String creatorName,
			long ownerId, long typeId, String name, String certNumber,
			Date deliveryDate, Date expirationDate, String issuingCountry,
			String issuingInstitution) throws PortalException, SystemException {

		// Qualification
		long qualificationId = counterLocalService.increment();
		Qualification qualification = qualificationPersistence
				.create(qualificationId);
		qualification.setCreatorId(creatorId);
		qualification.setCreatorName(creatorName);
		qualification.setCreateDate(new Date());
		qualification.setOwnerId(ownerId);
		qualification.setTypeId(typeId);
		qualification.setName(name);
		qualification.setCertNumber(certNumber);
		qualification.setDeliveryDate(deliveryDate);
		qualification.setExpirationDate(expirationDate);
		qualification.setIssuingCountry(issuingCountry);
		qualification.setIssuingInstitution(issuingInstitution);
		qualificationPersistence.update(qualification);

		return qualification;
	}

	public Qualification updateQualification(long qualificationId,
			long modifierId, String modifierName, long ownerId, long typeId,
			String name, String certNumber, Date deliveryDate,
			Date expirationDate, String issuingCountry,
			String issuingInstitution) throws PortalException, SystemException {

		// Qualification
		Qualification qualification = qualificationPersistence
				.findByPrimaryKey(qualificationId);
		qualification.setModifierId(modifierId);
		qualification.setModifierName(modifierName);
		qualification.setModifiedDate(new Date());
		qualification.setOwnerId(ownerId);
		qualification.setTypeId(typeId);
		qualification.setName(name);
		qualification.setCertNumber(certNumber);
		qualification.setDeliveryDate(deliveryDate);
		qualification.setExpirationDate(expirationDate);
		qualification.setIssuingCountry(issuingCountry);
		qualification.setIssuingInstitution(issuingInstitution);
		qualificationPersistence.update(qualification);

		return qualification;
	}
}
