package de.humance.eco.profile.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.QualificationType;
import de.humance.eco.profile.service.QualificationTypeLocalServiceUtil;
import de.humance.eco.profile.service.base.QualificationTypeLocalServiceBaseImpl;

/**
 * The implementation of the qualification type local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link de.humance.eco.profile.service.QualificationTypeLocalService}
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Humance
 * @see de.humance.eco.profile.service.base.QualificationTypeLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.QualificationTypeLocalServiceUtil
 */
public class QualificationTypeLocalServiceImpl extends
		QualificationTypeLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * de.humance.eco.profile.service.QualificationTypeLocalServiceUtil} to
	 * access the qualification type local service.
	 */

	public List<QualificationType> findByName(String name)
			throws SystemException {
		return qualificationTypePersistence.findByName(name);
	}

	public List<QualificationType> findByNameLike(String name)
			throws SystemException {
		return qualificationTypePersistence.findByNameLike(name);
	}

	public List<QualificationType> findByModifiedDate(Date modifiedDate)
			throws SystemException {
		List<QualificationType> result = new ArrayList<QualificationType>();
		List<QualificationType> qualificationTypes = QualificationTypeLocalServiceUtil
				.getQualificationTypes(0, QualificationTypeLocalServiceUtil
						.getQualificationTypesCount());
		for (QualificationType qualificationType : qualificationTypes) {
			if (modifiedDate.before(qualificationType.getModifiedDate())) {
				result.add(qualificationType);
			}
		}
		return result;
	}

	public QualificationType addQualificationType(long creatorId,
			String creatorName, String name, String description, String note,
			long iconId) throws PortalException, SystemException {

		// QualificationType
		long qualificationTypeId = counterLocalService.increment();
		QualificationType qualificationType = qualificationTypePersistence
				.create(qualificationTypeId);
		qualificationType.setCreatorId(creatorId);
		qualificationType.setCreatorName(creatorName);
		qualificationType.setCreateDate(new Date());
		qualificationType.setModifierId(creatorId);
		qualificationType.setModifierName(creatorName);
		qualificationType.setModifiedDate(new Date());
		qualificationType.setName(name);
		qualificationType.setDescription(description);
		qualificationType.setNote(note);
		qualificationType.setIconId(iconId);
		qualificationTypePersistence.update(qualificationType);

		return qualificationType;
	}

	public QualificationType updateQualificationType(long qualificationTypeId,
			long modifierId, String modifierName, String name,
			String description, String note, long iconId)
			throws PortalException, SystemException {

		// QualificationType
		QualificationType qualificationType = qualificationTypePersistence
				.findByPrimaryKey(qualificationTypeId);
		qualificationType.setModifierId(modifierId);
		qualificationType.setModifierName(modifierName);
		qualificationType.setModifiedDate(new Date());
		qualificationType.setName(name);
		qualificationType.setDescription(description);
		qualificationType.setNote(note);
		qualificationType.setIconId(iconId);
		qualificationTypePersistence.update(qualificationType);

		return qualificationType;
	}
}
