package de.humance.eco.profile.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.QualificationTypeResource;
import de.humance.eco.profile.service.base.QualificationTypeResourceLocalServiceBaseImpl;

/**
 * The implementation of the qualification type resource local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link de.humance.eco.profile.service.QualificationTypeResourceLocalService}
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Humance
 * @see de.humance.eco.profile.service.base.QualificationTypeResourceLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.QualificationTypeResourceLocalServiceUtil
 */
public class QualificationTypeResourceLocalServiceImpl extends
		QualificationTypeResourceLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * de.humance.eco.profile.service.QualificationTypeResourceLocalServiceUtil}
	 * to access the qualification type resource local service.
	 */

	public List<QualificationTypeResource> findByTypeId(long qualificationTypeId)
			throws SystemException {
		return qualificationTypeResourcePersistence
				.findByQualificationTypeId(qualificationTypeId);
	}

	public List<QualificationTypeResource> findByTypeIdAndCountry(
			long qualificationTypeId, String country) throws SystemException {
		return qualificationTypeResourcePersistence
				.findByQualificationTypeIdAndCountry(qualificationTypeId,
						country);
	}

	public List<QualificationTypeResource> findByTypeIdAndLanguage(
			long qualificationTypeId, String language) throws SystemException {
		return qualificationTypeResourcePersistence
				.findByQualificationTypeIdAndLanguage(qualificationTypeId,
						language);
	}

	public List<QualificationTypeResource> findByTypeIdAndLocale(
			long qualificationTypeId, String country, String language)
			throws SystemException {
		return qualificationTypeResourcePersistence
				.findByQualificationTypeIdAndLocale(qualificationTypeId,
						country, language);
	}

	public QualificationTypeResource addQualificationTypeResource(
			long qualificationTypeId, String country, String language,
			String name, long iconId) throws PortalException, SystemException {

		// QualificationTypeResource
		long qualificationTypeResourceId = counterLocalService.increment();
		QualificationTypeResource qualificationTypeResource = qualificationTypeResourcePersistence
				.create(qualificationTypeResourceId);
		qualificationTypeResource.setQualificationTypeId(qualificationTypeId);
		qualificationTypeResource.setCountry(country);
		qualificationTypeResource.setLanguage(language);
		qualificationTypeResource.setName(name);
		qualificationTypeResource.setIconId(iconId);
		qualificationTypeResourcePersistence.update(qualificationTypeResource);

		return qualificationTypeResource;
	}

	public QualificationTypeResource updateQualificationTypeResource(
			long qualificationTypeResourceId, long qualificationTypeId,
			String country, String language, String name, long iconId)
			throws PortalException, SystemException {

		// QualificationTypeResource
		QualificationTypeResource qualificationTypeResource = qualificationTypeResourcePersistence
				.findByPrimaryKey(qualificationTypeResourceId);
		qualificationTypeResource.setQualificationTypeId(qualificationTypeId);
		qualificationTypeResource.setCountry(country);
		qualificationTypeResource.setLanguage(language);
		qualificationTypeResource.setName(name);
		qualificationTypeResource.setIconId(iconId);
		qualificationTypeResourcePersistence.update(qualificationTypeResource);

		return qualificationTypeResource;
	}
}
