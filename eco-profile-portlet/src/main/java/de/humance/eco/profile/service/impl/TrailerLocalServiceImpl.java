package de.humance.eco.profile.service.impl;

import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.Trailer;
import de.humance.eco.profile.service.base.TrailerLocalServiceBaseImpl;

/**
 * The implementation of the trailer local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link de.humance.eco.profile.service.TrailerLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Humance
 * @see de.humance.eco.profile.service.base.TrailerLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.TrailerLocalServiceUtil
 */
public class TrailerLocalServiceImpl extends TrailerLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * de.humance.eco.profile.service.TrailerLocalServiceUtil} to access the
	 * trailer local service.
	 */

	public List<Trailer> findByDriverId(long driverId) throws SystemException {
		return trailerPersistence.findByDriverId(driverId);
	}

	public List<Trailer> findByDriverIdAndOrgaId(long driverId,
			long organizationId) throws SystemException {
		return trailerPersistence.findByDriverIdAndOrgaId(driverId,
				organizationId);
	}

	public List<Trailer> findByManufacturerId(long manufacturerId)
			throws SystemException {
		return trailerPersistence.findByManufacturerId(manufacturerId);
	}

	public List<Trailer> findByModelName(String modelName)
			throws SystemException {
		return trailerPersistence.findByModelName(modelName);
	}

	public List<Trailer> findByModelNameLike(String modelName)
			throws SystemException {
		return trailerPersistence.findByModelNameLike(modelName);
	}

	public List<Trailer> findByTypeId(long typeId) throws SystemException {
		return trailerPersistence.findByTypeId(typeId);
	}

	public List<Trailer> findByDriverIdAndTypeId(long driverId, long typeId)
			throws SystemException {
		return trailerPersistence.findByDriverIdAndTypeId(driverId, typeId);
	}

	public List<Trailer> findByLicensePlate(String licensePlate)
			throws SystemException {
		return trailerPersistence.findByLicensePlate(licensePlate);
	}

	public Trailer addTrailer(long creatorId, String creatorName,
			long driverId, long typeId, long manufacturerId, String modelName,
			String licensePlate, long dimensionHeight, long dimensionWidth,
			long dimensionDepth, long organizationId) throws PortalException,
			SystemException {

		// Trailer
		long trailerId = counterLocalService.increment();
		Trailer trailer = trailerPersistence.create(trailerId);
		trailer.setCreatorId(creatorId);
		trailer.setCreatorName(creatorName);
		trailer.setCreateDate(new Date());
		trailer.setDriverId(driverId);
		trailer.setTypeId(typeId);
		trailer.setManufacturerId(manufacturerId);
		trailer.setModelName(modelName);
		trailer.setLicensePlate(licensePlate);
		trailer.setDimensionHeight(dimensionHeight);
		trailer.setDimensionWidth(dimensionWidth);
		trailer.setDimensionDepth(dimensionDepth);
		trailer.setOrganizationId(organizationId);
		trailerPersistence.update(trailer);

		return trailer;
	}

	public Trailer updateTrailer(long trailerId, long modifierId,
			String modifierName, long driverId, long typeId,
			long manufacturerId, String modelName, String licensePlate,
			long dimensionHeight, long dimensionWidth, long dimensionDepth,
			long organizationId) throws PortalException, SystemException {

		// Trailer
		Trailer trailer = trailerPersistence.findByPrimaryKey(trailerId);
		trailer.setModifierId(modifierId);
		trailer.setModifierName(modifierName);
		trailer.setModifiedDate(new Date());
		trailer.setDriverId(driverId);
		trailer.setTypeId(typeId);
		trailer.setManufacturerId(manufacturerId);
		trailer.setModelName(modelName);
		trailer.setLicensePlate(licensePlate);
		trailer.setDimensionHeight(dimensionHeight);
		trailer.setDimensionWidth(dimensionWidth);
		trailer.setDimensionDepth(dimensionDepth);
		trailer.setOrganizationId(organizationId);
		trailerPersistence.update(trailer);

		return trailer;
	}
}
