package de.humance.eco.profile.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

import de.humance.eco.profile.model.TrailerType;
import de.humance.eco.profile.service.TrailerTypeLocalServiceUtil;
import de.humance.eco.profile.service.base.TrailerTypeLocalServiceBaseImpl;

/**
 * The implementation of the trailer type local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link de.humance.eco.profile.service.TrailerTypeLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Humance
 * @see de.humance.eco.profile.service.base.TrailerTypeLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.TrailerTypeLocalServiceUtil
 */
public class TrailerTypeLocalServiceImpl extends
		TrailerTypeLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * de.humance.eco.profile.service.TrailerTypeLocalServiceUtil} to access the
	 * trailer type local service.
	 */

	public List<TrailerType> findByName(String name) throws SystemException {
		return trailerTypePersistence.findByName(name);
	}

	public List<TrailerType> findByNameLike(String name) throws SystemException {
		return trailerTypePersistence.findByNameLike(name);
	}

	public List<TrailerType> findByModifiedDate(Date modifiedDate)
			throws SystemException {
		List<TrailerType> result = new ArrayList<TrailerType>();
		List<TrailerType> trailerTypes = TrailerTypeLocalServiceUtil
				.getTrailerTypes(0,
						TrailerTypeLocalServiceUtil.getTrailerTypesCount());
		for (TrailerType trailerType : trailerTypes) {
			if (modifiedDate.before(trailerType.getModifiedDate())) {
				result.add(trailerType);
			}
		}
		return result;
	}

	public TrailerType addTrailerType(long creatorId, String name, long iconId)
			throws PortalException, SystemException {

		User user = UserLocalServiceUtil.getUser(creatorId);

		// TrailerType
		long trailerTypeId = counterLocalService.increment();
		TrailerType trailerType = trailerTypePersistence.create(trailerTypeId);
		trailerType.setName(name);
		trailerType.setIconId(iconId);
		trailerType.setCreatorId(creatorId);
		trailerType.setCreatorName(user.getScreenName());
		trailerType.setCreateDate(new Date());
		trailerType.setModifierId(creatorId);
		trailerType.setModifierName(user.getScreenName());
		trailerType.setModifiedDate(new Date());

		trailerTypePersistence.update(trailerType);

		return trailerType;
	}

	public TrailerType updateTrailerType(long modifierId, long trailerTypeId,
			String name, long iconId) throws PortalException, SystemException {

		User user = UserLocalServiceUtil.getUser(modifierId);

		// TrailerType
		TrailerType trailerType = trailerTypePersistence
				.findByPrimaryKey(trailerTypeId);
		trailerType.setName(name);
		trailerType.setIconId(iconId);
		trailerType.setModifierId(modifierId);
		trailerType.setModifierName(user.getScreenName());
		trailerType.setModifiedDate(new Date());

		trailerTypePersistence.update(trailerType);

		return trailerType;
	}
}
