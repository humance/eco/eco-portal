package de.humance.eco.profile.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.TrailerUser;
import de.humance.eco.profile.service.base.TrailerUserLocalServiceBaseImpl;

/**
 * The implementation of the trailer user local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link de.humance.eco.profile.service.TrailerUserLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Humance
 * @see de.humance.eco.profile.service.base.TrailerUserLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.TrailerUserLocalServiceUtil
 */
public class TrailerUserLocalServiceImpl extends
		TrailerUserLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * de.humance.eco.profile.service.TrailerUserLocalServiceUtil} to access the
	 * trailer user local service.
	 */

	public List<TrailerUser> findByUserId(long userId) throws SystemException {
		return trailerUserPersistence.findByUserId(userId);
	}

	public List<TrailerUser> findByTrailerId(long trailerId)
			throws SystemException {
		return trailerUserPersistence.findByTrailerId(trailerId);
	}

	public TrailerUser addTrailerUser(long userId, long trailerId)
			throws PortalException, SystemException {

		// Vehicle
		long TrailerUserId = counterLocalService.increment();
		TrailerUser trailerUser = trailerUserPersistence.create(TrailerUserId);
		trailerUser.setUserId(userId);
		trailerUser.setTrailerId(trailerId);
		trailerUserPersistence.update(trailerUser);

		return trailerUser;
	}
}
