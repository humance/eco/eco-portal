package de.humance.eco.profile.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

import de.humance.eco.profile.model.VehicleAttributeDefinition;
import de.humance.eco.profile.service.VehicleAttributeDefinitionLocalServiceUtil;
import de.humance.eco.profile.service.base.VehicleAttributeDefinitionLocalServiceBaseImpl;

/**
 * The implementation of the vehicle attribute definition local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link de.humance.eco.profile.service.VehicleAttributeDefinitionLocalService}
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Humance
 * @see de.humance.eco.profile.service.base.VehicleAttributeDefinitionLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.VehicleAttributeDefinitionLocalServiceUtil
 */
public class VehicleAttributeDefinitionLocalServiceImpl extends
		VehicleAttributeDefinitionLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * de.humance.eco.
	 * profile.service.VehicleAttributeDefinitionLocalServiceUtil} to access the
	 * vehicle attribute definition local service.
	 */

	public List<VehicleAttributeDefinition> findByTitle(String title)
			throws SystemException {
		return vehicleAttributeDefinitionPersistence.findByTitle(title);
	}

	public List<VehicleAttributeDefinition> findByTitleLike(String title)
			throws SystemException {
		return vehicleAttributeDefinitionPersistence.findByTitleLike(title);
	}

	public List<VehicleAttributeDefinition> findByVehicleTypeId(
			long vehicleTypeId) throws SystemException {
		return vehicleAttributeDefinitionPersistence
				.findByVehicleTypeId(vehicleTypeId);
	}

	public List<VehicleAttributeDefinition> findByModifiedDate(Date modifiedDate)
			throws SystemException {
		List<VehicleAttributeDefinition> result = new ArrayList<VehicleAttributeDefinition>();
		List<VehicleAttributeDefinition> vehicleAttributeDefinitions = VehicleAttributeDefinitionLocalServiceUtil
				.getVehicleAttributeDefinitions(0,
						VehicleAttributeDefinitionLocalServiceUtil
								.getVehicleAttributeDefinitionsCount());
		for (VehicleAttributeDefinition vehicleAttributeDefinition : vehicleAttributeDefinitions) {
			if (modifiedDate.before(vehicleAttributeDefinition
					.getModifiedDate())) {
				result.add(vehicleAttributeDefinition);
			}
		}
		return result;
	}

	public VehicleAttributeDefinition addVehicleAttributeDefinition(
			long creatorId, long vehicleTypeId, String type, String title,
			double minRangeValue, double maxRangeValue, String unit,
			long sequenceNumber) throws PortalException, SystemException {

		User user = UserLocalServiceUtil.getUser(creatorId);

		// VehicleAttributeDefinition
		long vehicleAttributeDefinitionId = counterLocalService.increment();
		VehicleAttributeDefinition vehicleAttributeDefinition = vehicleAttributeDefinitionPersistence
				.create(vehicleAttributeDefinitionId);
		vehicleAttributeDefinition.setVehicleTypeId(vehicleTypeId);
		vehicleAttributeDefinition.setType(type);
		vehicleAttributeDefinition.setTitle(title);
		vehicleAttributeDefinition.setMinRangeValue(minRangeValue);
		vehicleAttributeDefinition.setMaxRangeValue(maxRangeValue);
		vehicleAttributeDefinition.setUnit(unit);
		vehicleAttributeDefinition.setSequenceNumber(sequenceNumber);
		vehicleAttributeDefinition.setCreatorId(creatorId);
		vehicleAttributeDefinition.setCreatorName(user.getScreenName());
		vehicleAttributeDefinition.setCreateDate(new Date());
		vehicleAttributeDefinition.setModifierId(creatorId);
		vehicleAttributeDefinition.setModifierName(user.getScreenName());
		vehicleAttributeDefinition.setModifiedDate(new Date());

		vehicleAttributeDefinitionPersistence
				.update(vehicleAttributeDefinition);

		return vehicleAttributeDefinition;
	}

	public VehicleAttributeDefinition updateVehicleAttributeDefinition(
			long modifierId, long vehicleAttributeDefinitionId,
			long vehicleTypeId, String type, String title,
			double minRangeValue, double maxRangeValue, String unit,
			long sequenceNumber) throws PortalException, SystemException {

		User user = UserLocalServiceUtil.getUser(modifierId);

		// VehicleAttributeDefinition
		VehicleAttributeDefinition vehicleAttributeDefinition = vehicleAttributeDefinitionPersistence
				.findByPrimaryKey(vehicleAttributeDefinitionId);
		vehicleAttributeDefinition.setVehicleTypeId(vehicleTypeId);
		vehicleAttributeDefinition.setType(type);
		vehicleAttributeDefinition.setTitle(title);
		vehicleAttributeDefinition.setMinRangeValue(minRangeValue);
		vehicleAttributeDefinition.setMaxRangeValue(maxRangeValue);
		vehicleAttributeDefinition.setUnit(unit);
		vehicleAttributeDefinition.setSequenceNumber(sequenceNumber);
		vehicleAttributeDefinition.setModifierId(modifierId);
		vehicleAttributeDefinition.setModifierName(user.getScreenName());
		vehicleAttributeDefinition.setModifiedDate(new Date());

		vehicleAttributeDefinitionPersistence
				.update(vehicleAttributeDefinition);

		return vehicleAttributeDefinition;
	}
}
