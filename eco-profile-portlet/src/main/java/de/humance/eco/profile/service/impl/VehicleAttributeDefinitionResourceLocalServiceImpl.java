package de.humance.eco.profile.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.VehicleAttributeDefinitionResource;
import de.humance.eco.profile.service.base.VehicleAttributeDefinitionResourceLocalServiceBaseImpl;

/**
 * The implementation of the vehicle attribute definition resource local
 * service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link de.humance.eco.profile.service.VehicleAttributeDefinitionResourceLocalService}
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Humance
 * @see de.humance.eco.profile.service.base.VehicleAttributeDefinitionResourceLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.VehicleAttributeDefinitionResourceLocalServiceUtil
 */
public class VehicleAttributeDefinitionResourceLocalServiceImpl extends
		VehicleAttributeDefinitionResourceLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * de.humance.eco.
	 * profile.service.VehicleAttributeDefinitionResourceLocalServiceUtil} to
	 * access the vehicle attribute definition resource local service.
	 */

	public List<VehicleAttributeDefinitionResource> findByDefinitionId(
			long vehicleAttributeDefinitionId) throws SystemException {
		return vehicleAttributeDefinitionResourcePersistence
				.findByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
	}

	public List<VehicleAttributeDefinitionResource> findByDefinitionIdAndCoutry(
			long vehicleAttributeDefinitionId, String country)
			throws SystemException {
		return vehicleAttributeDefinitionResourcePersistence
				.findByVehicleAttributeDefinitionIdAndCountry(
						vehicleAttributeDefinitionId, country);
	}

	public List<VehicleAttributeDefinitionResource> findByDefinitionIdAndLanguage(
			long vehicleAttributeDefinitionId, String language)
			throws SystemException {
		return vehicleAttributeDefinitionResourcePersistence
				.findByVehicleAttributeDefinitionIdAndLanguage(
						vehicleAttributeDefinitionId, language);
	}

	public List<VehicleAttributeDefinitionResource> findByDefinitionIdAndLocale(
			long vehicleAttributeDefinitionId, String country, String language)
			throws SystemException {
		return vehicleAttributeDefinitionResourcePersistence
				.findByVehicleAttributeDefinitionIdAndLocale(
						vehicleAttributeDefinitionId, country, language);
	}

	public VehicleAttributeDefinitionResource addVehicleAttributeDefinitionResource(
			long vehicleAttributeDefinitionId, String country, String language,
			String title, String unit) throws PortalException, SystemException {

		// VehicleAttributeDefinitionResource
		long vehicleAttributeDefinitionResourceId = counterLocalService
				.increment();
		VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource = vehicleAttributeDefinitionResourcePersistence
				.create(vehicleAttributeDefinitionResourceId);
		vehicleAttributeDefinitionResource
				.setVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
		vehicleAttributeDefinitionResource.setCountry(country);
		vehicleAttributeDefinitionResource.setLanguage(language);
		vehicleAttributeDefinitionResource.setTitle(title);
		vehicleAttributeDefinitionResource.setUnit(unit);
		vehicleAttributeDefinitionResourcePersistence
				.update(vehicleAttributeDefinitionResource);

		return vehicleAttributeDefinitionResource;
	}

	public VehicleAttributeDefinitionResource updateVehicleAttributeDefinitionResource(
			long vehicleAttributeDefinitionResourceId,
			long vehicleAttributeDefinitionId, String country, String language,
			String title, String unit) throws PortalException, SystemException {

		// VehicleAttributeDefinitionResource
		VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource = vehicleAttributeDefinitionResourcePersistence
				.findByPrimaryKey(vehicleAttributeDefinitionResourceId);
		vehicleAttributeDefinitionResource
				.setVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
		vehicleAttributeDefinitionResource.setCountry(country);
		vehicleAttributeDefinitionResource.setLanguage(language);
		vehicleAttributeDefinitionResource.setTitle(title);
		vehicleAttributeDefinitionResource.setUnit(unit);
		vehicleAttributeDefinitionResourcePersistence
				.update(vehicleAttributeDefinitionResource);

		return vehicleAttributeDefinitionResource;
	}
}
