package de.humance.eco.profile.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.VehicleAttribute;
import de.humance.eco.profile.service.base.VehicleAttributeLocalServiceBaseImpl;

/**
 * The implementation of the vehicle attribute local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link de.humance.eco.profile.service.VehicleAttributeLocalService}
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Humance
 * @see de.humance.eco.profile.service.base.VehicleAttributeLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.VehicleAttributeLocalServiceUtil
 */
public class VehicleAttributeLocalServiceImpl extends
		VehicleAttributeLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * de.humance.eco.profile.service.VehicleAttributeLocalServiceUtil} to
	 * access the vehicle attribute local service.
	 */

	public List<VehicleAttribute> findByVehicleIdAndAttributeDefinitionId(
			long vehicleId, long vehicleAttributeDefinitionId)
			throws SystemException {
		return vehicleAttributePersistence
				.findByVehicleIdAndAttributeDefinitionId(vehicleId,
						vehicleAttributeDefinitionId);
	}

	public List<VehicleAttribute> findByAttributeDefinitionId(
			long vehicleAttributeDefinitionId) throws SystemException {
		return vehicleAttributePersistence
				.findByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
	}

	public List<VehicleAttribute> findByVehicleId(long vehicleId)
			throws SystemException {
		return vehicleAttributePersistence.findByVehicleId(vehicleId);
	}

	public VehicleAttribute addVehicleAttribute(long vehicleId,
			long vehicleAttributeDefinitionId, String type,
			long sequenceNumber, String attributeValue) throws PortalException,
			SystemException {

		// VehicleAttribute
		long vehicleAttributeId = counterLocalService.increment();
		VehicleAttribute vehicleAttribute = vehicleAttributePersistence
				.create(vehicleAttributeId);
		vehicleAttribute.setVehicleId(vehicleId);
		vehicleAttribute
				.setVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
		vehicleAttribute.setType(type);
		vehicleAttribute.setSequenceNumber(sequenceNumber);
		vehicleAttribute.setAttributeValue(attributeValue);
		vehicleAttributePersistence.update(vehicleAttribute);

		return vehicleAttribute;
	}

	public VehicleAttribute updateVehicleAttribute(long vehicleAttributeId,
			long vehicleId, long vehicleAttributeDefinitionId, String type,
			long sequenceNumber, String attributeValue) throws PortalException,
			SystemException {

		// VehicleAttribute
		VehicleAttribute vehicleAttribute = vehicleAttributePersistence
				.findByPrimaryKey(vehicleAttributeId);
		vehicleAttribute.setVehicleId(vehicleId);
		vehicleAttribute
				.setVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
		vehicleAttribute.setType(type);
		vehicleAttribute.setSequenceNumber(sequenceNumber);
		vehicleAttribute.setAttributeValue(attributeValue);
		vehicleAttributePersistence.update(vehicleAttribute);

		return vehicleAttribute;
	}
}
