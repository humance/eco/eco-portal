package de.humance.eco.profile.service.impl;

import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.Vehicle;
import de.humance.eco.profile.service.base.VehicleLocalServiceBaseImpl;

/**
 * The implementation of the vehicle local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link de.humance.eco.profile.service.VehicleLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Humance
 * @see de.humance.eco.profile.service.base.VehicleLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.VehicleLocalServiceUtil
 */
public class VehicleLocalServiceImpl extends VehicleLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * de.humance.eco.profile.service.VehicleLocalServiceUtil} to access the
	 * vehicle local service.
	 */

	public List<Vehicle> findByDriverId(long driverId) throws SystemException {
		return vehiclePersistence.findByDriverId(driverId);
	}

	public List<Vehicle> findByDriverIdAndOrgaId(long driverId,
			long organizationId) throws SystemException {
		return vehiclePersistence.findByDriverIdAndOrgaId(driverId,
				organizationId);
	}

	public List<Vehicle> findByManufacturerId(long manufacturerId)
			throws SystemException {
		return vehiclePersistence.findByManufacturerId(manufacturerId);
	}

	public List<Vehicle> findByModelName(String modelName)
			throws SystemException {
		return vehiclePersistence.findByModelName(modelName);
	}

	public List<Vehicle> findByModelNameLike(String modelName)
			throws SystemException {
		return vehiclePersistence.findByModelNameLike(modelName);
	}

	public List<Vehicle> findByTypeId(long typeId) throws SystemException {
		return vehiclePersistence.findByTypeId(typeId);
	}

	public List<Vehicle> findByDriverIdAndTypeId(long driverId, long typeId)
			throws SystemException {
		return vehiclePersistence.findByDriverIdAndTypeId(driverId, typeId);
	}

	public List<Vehicle> findByLicensePlate(String licensePlate)
			throws SystemException {
		return vehiclePersistence.findByLicensePlate(licensePlate);
	}

	public Vehicle addVehicle(long creatorId, String creatorName,
			long driverId, long typeId, long manufacturerId, String modelName,
			String licensePlate, long dimensionHeight, long dimensionWidth,
			long dimensionDepth, long organizationId) throws PortalException,
			SystemException {

		// Vehicle
		long vehicleId = counterLocalService.increment();
		Vehicle vehicle = vehiclePersistence.create(vehicleId);
		vehicle.setCreatorId(creatorId);
		vehicle.setCreatorName(creatorName);
		vehicle.setCreateDate(new Date());
		vehicle.setModifierId(creatorId);
		vehicle.setModifierName(creatorName);
		vehicle.setModifiedDate(new Date());
		vehicle.setDriverId(driverId);
		vehicle.setTypeId(typeId);
		vehicle.setManufacturerId(manufacturerId);
		vehicle.setModelName(modelName);
		vehicle.setLicensePlate(licensePlate);
		vehicle.setDimensionHeight(dimensionHeight);
		vehicle.setDimensionWidth(dimensionWidth);
		vehicle.setDimensionDepth(dimensionDepth);
		vehicle.setOrganizationId(organizationId);
		vehiclePersistence.update(vehicle);

		return vehicle;
	}

	public Vehicle updateVehicle(long vehicleId, long modifierId,
			String modifierName, long driverId, long typeId,
			long manufacturerId, String modelName, String licensePlate,
			long dimensionHeight, long dimensionWidth, long dimensionDepth,
			long organizationId) throws PortalException, SystemException {

		// Vehicle
		Vehicle vehicle = vehiclePersistence.findByPrimaryKey(vehicleId);
		vehicle.setModifierId(modifierId);
		vehicle.setModifierName(modifierName);
		vehicle.setModifiedDate(new Date());
		vehicle.setDriverId(driverId);
		vehicle.setTypeId(typeId);
		vehicle.setManufacturerId(manufacturerId);
		vehicle.setModelName(modelName);
		vehicle.setLicensePlate(licensePlate);
		vehicle.setDimensionHeight(dimensionHeight);
		vehicle.setDimensionWidth(dimensionWidth);
		vehicle.setDimensionDepth(dimensionDepth);
		vehicle.setOrganizationId(organizationId);
		vehiclePersistence.update(vehicle);

		return vehicle;
	}
}
