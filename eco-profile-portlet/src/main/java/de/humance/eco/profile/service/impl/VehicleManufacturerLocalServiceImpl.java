package de.humance.eco.profile.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

import de.humance.eco.profile.model.VehicleManufacturer;
import de.humance.eco.profile.service.VehicleManufacturerLocalServiceUtil;
import de.humance.eco.profile.service.base.VehicleManufacturerLocalServiceBaseImpl;

/**
 * The implementation of the vehicle manufacturer local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link de.humance.eco.profile.service.VehicleManufacturerLocalService}
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Humance
 * @see de.humance.eco.profile.service.base.VehicleManufacturerLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.VehicleManufacturerLocalServiceUtil
 */
public class VehicleManufacturerLocalServiceImpl extends
		VehicleManufacturerLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * de.humance.eco.profile.service.VehicleManufacturerLocalServiceUtil} to
	 * access the vehicle manufacturer local service.
	 */

	public List<VehicleManufacturer> findByName(String name)
			throws SystemException {
		return vehicleManufacturerPersistence.findByName(name);
	}

	public List<VehicleManufacturer> findByNameLike(String name)
			throws SystemException {
		return vehicleManufacturerPersistence.findByNameLike(name);
	}

	public List<VehicleManufacturer> findByModifiedDate(Date modifiedDate)
			throws SystemException {
		List<VehicleManufacturer> result = new ArrayList<VehicleManufacturer>();
		List<VehicleManufacturer> vehicleManufacturers = VehicleManufacturerLocalServiceUtil
				.getVehicleManufacturers(0, VehicleManufacturerLocalServiceUtil
						.getVehicleManufacturersCount());
		for (VehicleManufacturer vehicleManufacturer : vehicleManufacturers) {
			if (modifiedDate.before(vehicleManufacturer.getModifiedDate())) {
				result.add(vehicleManufacturer);
			}
		}
		return result;
	}

	public VehicleManufacturer addVehicleManufacturer(long creatorId,
			String name, long iconId) throws PortalException, SystemException {

		User user = UserLocalServiceUtil.getUser(creatorId);

		// VehicleManufacturer
		long vehicleManufacturerId = counterLocalService.increment();
		VehicleManufacturer vehicleManufacturer = vehicleManufacturerPersistence
				.create(vehicleManufacturerId);
		vehicleManufacturer.setName(name);
		vehicleManufacturer.setIconId(iconId);
		vehicleManufacturer.setCreatorId(creatorId);
		vehicleManufacturer.setCreatorName(user.getScreenName());
		vehicleManufacturer.setCreateDate(new Date());
		vehicleManufacturer.setModifierId(creatorId);
		vehicleManufacturer.setModifierName(user.getScreenName());
		vehicleManufacturer.setModifiedDate(new Date());

		vehicleManufacturerPersistence.update(vehicleManufacturer);

		return vehicleManufacturer;
	}

	public VehicleManufacturer updateVehicleManufacturer(long modifierId,
			long vehicleManufacturerId, String name, long iconId)
			throws PortalException, SystemException {

		User user = UserLocalServiceUtil.getUser(modifierId);

		// VehicleManufacturer
		VehicleManufacturer vehicleManufacturer = vehicleManufacturerPersistence
				.findByPrimaryKey(vehicleManufacturerId);
		vehicleManufacturer.setName(name);
		vehicleManufacturer.setIconId(iconId);
		vehicleManufacturer.setModifierId(modifierId);
		vehicleManufacturer.setModifierName(user.getScreenName());
		vehicleManufacturer.setModifiedDate(new Date());

		vehicleManufacturerPersistence.update(vehicleManufacturer);

		return vehicleManufacturer;
	}
}
