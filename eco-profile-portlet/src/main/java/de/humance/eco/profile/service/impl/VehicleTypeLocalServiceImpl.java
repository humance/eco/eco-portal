package de.humance.eco.profile.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

import de.humance.eco.profile.model.VehicleType;
import de.humance.eco.profile.service.VehicleTypeLocalServiceUtil;
import de.humance.eco.profile.service.base.VehicleTypeLocalServiceBaseImpl;

/**
 * The implementation of the vehicle type local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link de.humance.eco.profile.service.VehicleTypeLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Humance
 * @see de.humance.eco.profile.service.base.VehicleTypeLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.VehicleTypeLocalServiceUtil
 */
public class VehicleTypeLocalServiceImpl extends
		VehicleTypeLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * de.humance.eco.profile.service.VehicleTypeLocalServiceUtil} to access the
	 * vehicle type local service.
	 */

	public List<VehicleType> findByName(String name) throws SystemException {
		return vehicleTypePersistence.findByName(name);
	}

	public List<VehicleType> findByNameLike(String name) throws SystemException {
		return vehicleTypePersistence.findByNameLike(name);
	}

	public List<VehicleType> findByModifiedDate(Date modifiedDate)
			throws SystemException {
		List<VehicleType> result = new ArrayList<VehicleType>();
		List<VehicleType> vehicleTypes = VehicleTypeLocalServiceUtil
				.getVehicleTypes(0,
						VehicleTypeLocalServiceUtil.getVehicleTypesCount());
		for (VehicleType vehicleType : vehicleTypes) {
			if (modifiedDate.before(vehicleType.getModifiedDate())) {
				result.add(vehicleType);
			}
		}
		return result;
	}

	public VehicleType addVehicleType(long creatorId, String name, long iconId)
			throws PortalException, SystemException {

		User user = UserLocalServiceUtil.getUser(creatorId);

		// VehicleType
		long vehicleTypeId = counterLocalService.increment();
		VehicleType vehicleType = vehicleTypePersistence.create(vehicleTypeId);
		vehicleType.setName(name);
		vehicleType.setIconId(iconId);
		vehicleType.setCreatorId(creatorId);
		vehicleType.setCreatorName(user.getScreenName());
		vehicleType.setCreateDate(new Date());
		vehicleType.setModifierId(creatorId);
		vehicleType.setModifierName(user.getScreenName());
		vehicleType.setModifiedDate(new Date());

		vehicleTypePersistence.update(vehicleType);

		return vehicleType;
	}

	public VehicleType updateVehicleType(long modifierId, long vehicleTypeId,
			String name, long iconId) throws PortalException, SystemException {

		User user = UserLocalServiceUtil.getUser(modifierId);

		// VehicleType
		VehicleType vehicleType = vehicleTypePersistence
				.findByPrimaryKey(vehicleTypeId);
		vehicleType.setName(name);
		vehicleType.setIconId(iconId);
		vehicleType.setModifierId(modifierId);
		vehicleType.setModifierName(user.getScreenName());
		vehicleType.setModifiedDate(new Date());

		vehicleTypePersistence.update(vehicleType);

		return vehicleType;
	}
}
