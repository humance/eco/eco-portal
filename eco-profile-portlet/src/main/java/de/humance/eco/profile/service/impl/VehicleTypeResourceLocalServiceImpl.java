package de.humance.eco.profile.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.VehicleTypeResource;
import de.humance.eco.profile.service.base.VehicleTypeResourceLocalServiceBaseImpl;

/**
 * The implementation of the vehicle type resource local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link de.humance.eco.profile.service.VehicleTypeResourceLocalService}
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Humance
 * @see de.humance.eco.profile.service.base.VehicleTypeResourceLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.VehicleTypeResourceLocalServiceUtil
 */
public class VehicleTypeResourceLocalServiceImpl extends
		VehicleTypeResourceLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * de.humance.eco.profile.service.VehicleTypeResourceLocalServiceUtil} to
	 * access the vehicle type resource local service.
	 */

	public List<VehicleTypeResource> findByTypeId(long vehicleTypeId)
			throws SystemException {
		return vehicleTypeResourcePersistence
				.findByVehicleTypeId(vehicleTypeId);
	}

	public List<VehicleTypeResource> findByTypeIdAndCountry(long vehicleTypeId,
			String country) throws SystemException {
		return vehicleTypeResourcePersistence.findByVehicleTypeIdAndCountry(
				vehicleTypeId, country);
	}

	public List<VehicleTypeResource> findByTypeIdAndLanguage(
			long vehicleTypeId, String language) throws SystemException {
		return vehicleTypeResourcePersistence.findByVehicleTypeIdAndLanguage(
				vehicleTypeId, language);
	}

	public List<VehicleTypeResource> findByTypeIdAndLocale(long vehicleTypeId,
			String country, String language) throws SystemException {
		return vehicleTypeResourcePersistence.findByVehicleTypeIdAndLocale(
				vehicleTypeId, country, language);
	}

	public VehicleTypeResource addVehicleTypeResource(long vehicleTypeId,
			String country, String language, String name, long iconId)
			throws PortalException, SystemException {

		// VehicleTypeResource
		long vehicleTypeResourceId = counterLocalService.increment();
		VehicleTypeResource vehicleTypeResource = vehicleTypeResourcePersistence
				.create(vehicleTypeResourceId);
		vehicleTypeResource.setVehicleTypeId(vehicleTypeId);
		vehicleTypeResource.setCountry(country);
		vehicleTypeResource.setLanguage(language);
		vehicleTypeResource.setName(name);
		vehicleTypeResource.setIconId(iconId);
		vehicleTypeResourcePersistence.update(vehicleTypeResource);

		return vehicleTypeResource;
	}

	public VehicleTypeResource updateVehicleTypeResource(
			long vehicleTypeResourceId, long vehicleTypeId, String country,
			String language, String name, long iconId) throws PortalException,
			SystemException {

		// VehicleTypeResource
		VehicleTypeResource vehicleTypeResource = vehicleTypeResourcePersistence
				.findByPrimaryKey(vehicleTypeResourceId);
		vehicleTypeResource.setVehicleTypeId(vehicleTypeId);
		vehicleTypeResource.setCountry(country);
		vehicleTypeResource.setLanguage(language);
		vehicleTypeResource.setName(name);
		vehicleTypeResource.setIconId(iconId);
		vehicleTypeResourcePersistence.update(vehicleTypeResource);

		return vehicleTypeResource;
	}
}
