package de.humance.eco.profile.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.VehicleUser;
import de.humance.eco.profile.service.base.VehicleUserLocalServiceBaseImpl;

/**
 * The implementation of the vehicle user local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link de.humance.eco.profile.service.VehicleUserLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Humance
 * @see de.humance.eco.profile.service.base.VehicleUserLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.VehicleUserLocalServiceUtil
 */
public class VehicleUserLocalServiceImpl extends
		VehicleUserLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * de.humance.eco.profile.service.VehicleUserLocalServiceUtil} to access the
	 * vehicle user local service.
	 */

	public List<VehicleUser> findByUserId(long userId) throws SystemException {
		return vehicleUserPersistence.findByUserId(userId);
	}

	public List<VehicleUser> findByVehicleId(long vehicleId)
			throws SystemException {
		return vehicleUserPersistence.findByVehicleId(vehicleId);
	}

	public VehicleUser addVehicleUser(long userId, long vehicleId)
			throws PortalException, SystemException {

		// Vehicle
		long vehicleUserId = counterLocalService.increment();
		VehicleUser vehicleUser = vehicleUserPersistence.create(vehicleUserId);
		vehicleUser.setUserId(userId);
		vehicleUser.setVehicleId(vehicleId);
		vehicleUserPersistence.update(vehicleUser);

		return vehicleUser;
	}
}
