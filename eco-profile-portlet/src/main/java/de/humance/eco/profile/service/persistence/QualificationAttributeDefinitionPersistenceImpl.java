package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException;
import de.humance.eco.profile.model.QualificationAttributeDefinition;
import de.humance.eco.profile.model.impl.QualificationAttributeDefinitionImpl;
import de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl;
import de.humance.eco.profile.service.persistence.QualificationAttributeDefinitionPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the qualification attribute definition service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see QualificationAttributeDefinitionPersistence
 * @see QualificationAttributeDefinitionUtil
 * @generated
 */
public class QualificationAttributeDefinitionPersistenceImpl
    extends BasePersistenceImpl<QualificationAttributeDefinition>
    implements QualificationAttributeDefinitionPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link QualificationAttributeDefinitionUtil} to access the qualification attribute definition persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = QualificationAttributeDefinitionImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeDefinitionImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeDefinitionImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
            new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONTYPEID =
        new FinderPath(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeDefinitionImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByQualificationTypeId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEID =
        new FinderPath(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeDefinitionImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByQualificationTypeId", new String[] { Long.class.getName() },
            QualificationAttributeDefinitionModelImpl.QUALIFICATIONTYPEID_COLUMN_BITMASK |
            QualificationAttributeDefinitionModelImpl.SEQUENCENUMBER_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEID = new FinderPath(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByQualificationTypeId", new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_QUALIFICATIONTYPEID_QUALIFICATIONTYPEID_2 =
        "qualificationAttributeDefinition.qualificationTypeId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TITLE = new FinderPath(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeDefinitionImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTitle",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE = new FinderPath(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeDefinitionImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByTitle",
            new String[] { String.class.getName() },
            QualificationAttributeDefinitionModelImpl.TITLE_COLUMN_BITMASK |
            QualificationAttributeDefinitionModelImpl.QUALIFICATIONTYPEID_COLUMN_BITMASK |
            QualificationAttributeDefinitionModelImpl.SEQUENCENUMBER_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_TITLE = new FinderPath(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByTitle", new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_TITLE_TITLE_1 = "qualificationAttributeDefinition.title IS NULL";
    private static final String _FINDER_COLUMN_TITLE_TITLE_2 = "lower(qualificationAttributeDefinition.title) = ?";
    private static final String _FINDER_COLUMN_TITLE_TITLE_3 = "(qualificationAttributeDefinition.title IS NULL OR qualificationAttributeDefinition.title = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TITLELIKE =
        new FinderPath(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeDefinitionImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTitleLike",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_TITLELIKE =
        new FinderPath(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "countByTitleLike", new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_TITLELIKE_TITLE_1 = "qualificationAttributeDefinition.title LIKE NULL";
    private static final String _FINDER_COLUMN_TITLELIKE_TITLE_2 = "lower(qualificationAttributeDefinition.title) LIKE ?";
    private static final String _FINDER_COLUMN_TITLELIKE_TITLE_3 = "(qualificationAttributeDefinition.title IS NULL OR qualificationAttributeDefinition.title LIKE '')";
    private static final String _SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITION = "SELECT qualificationAttributeDefinition FROM QualificationAttributeDefinition qualificationAttributeDefinition";
    private static final String _SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITION_WHERE =
        "SELECT qualificationAttributeDefinition FROM QualificationAttributeDefinition qualificationAttributeDefinition WHERE ";
    private static final String _SQL_COUNT_QUALIFICATIONATTRIBUTEDEFINITION = "SELECT COUNT(qualificationAttributeDefinition) FROM QualificationAttributeDefinition qualificationAttributeDefinition";
    private static final String _SQL_COUNT_QUALIFICATIONATTRIBUTEDEFINITION_WHERE =
        "SELECT COUNT(qualificationAttributeDefinition) FROM QualificationAttributeDefinition qualificationAttributeDefinition WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "qualificationAttributeDefinition.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No QualificationAttributeDefinition exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No QualificationAttributeDefinition exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(QualificationAttributeDefinitionPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "type"
            });
    private static QualificationAttributeDefinition _nullQualificationAttributeDefinition =
        new QualificationAttributeDefinitionImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<QualificationAttributeDefinition> toCacheModel() {
                return _nullQualificationAttributeDefinitionCacheModel;
            }
        };

    private static CacheModel<QualificationAttributeDefinition> _nullQualificationAttributeDefinitionCacheModel =
        new CacheModel<QualificationAttributeDefinition>() {
            @Override
            public QualificationAttributeDefinition toEntityModel() {
                return _nullQualificationAttributeDefinition;
            }
        };

    public QualificationAttributeDefinitionPersistenceImpl() {
        setModelClass(QualificationAttributeDefinition.class);
    }

    /**
     * Returns all the qualification attribute definitions where qualificationTypeId = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @return the matching qualification attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinition> findByQualificationTypeId(
        long qualificationTypeId) throws SystemException {
        return findByQualificationTypeId(qualificationTypeId,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification attribute definitions where qualificationTypeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationTypeId the qualification type ID
     * @param start the lower bound of the range of qualification attribute definitions
     * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
     * @return the range of matching qualification attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinition> findByQualificationTypeId(
        long qualificationTypeId, int start, int end) throws SystemException {
        return findByQualificationTypeId(qualificationTypeId, start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification attribute definitions where qualificationTypeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationTypeId the qualification type ID
     * @param start the lower bound of the range of qualification attribute definitions
     * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualification attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinition> findByQualificationTypeId(
        long qualificationTypeId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEID;
            finderArgs = new Object[] { qualificationTypeId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONTYPEID;
            finderArgs = new Object[] {
                    qualificationTypeId,
                    
                    start, end, orderByComparator
                };
        }

        List<QualificationAttributeDefinition> list = (List<QualificationAttributeDefinition>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (QualificationAttributeDefinition qualificationAttributeDefinition : list) {
                if ((qualificationTypeId != qualificationAttributeDefinition.getQualificationTypeId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITION_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEID_QUALIFICATIONTYPEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationAttributeDefinitionModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationTypeId);

                if (!pagination) {
                    list = (List<QualificationAttributeDefinition>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationAttributeDefinition>(list);
                } else {
                    list = (List<QualificationAttributeDefinition>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification attribute definition in the ordered set where qualificationTypeId = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute definition
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a matching qualification attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition findByQualificationTypeId_First(
        long qualificationTypeId, OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionException, SystemException {
        QualificationAttributeDefinition qualificationAttributeDefinition = fetchByQualificationTypeId_First(qualificationTypeId,
                orderByComparator);

        if (qualificationAttributeDefinition != null) {
            return qualificationAttributeDefinition;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationTypeId=");
        msg.append(qualificationTypeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeDefinitionException(msg.toString());
    }

    /**
     * Returns the first qualification attribute definition in the ordered set where qualificationTypeId = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute definition, or <code>null</code> if a matching qualification attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition fetchByQualificationTypeId_First(
        long qualificationTypeId, OrderByComparator orderByComparator)
        throws SystemException {
        List<QualificationAttributeDefinition> list = findByQualificationTypeId(qualificationTypeId,
                0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification attribute definition in the ordered set where qualificationTypeId = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute definition
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a matching qualification attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition findByQualificationTypeId_Last(
        long qualificationTypeId, OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionException, SystemException {
        QualificationAttributeDefinition qualificationAttributeDefinition = fetchByQualificationTypeId_Last(qualificationTypeId,
                orderByComparator);

        if (qualificationAttributeDefinition != null) {
            return qualificationAttributeDefinition;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationTypeId=");
        msg.append(qualificationTypeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeDefinitionException(msg.toString());
    }

    /**
     * Returns the last qualification attribute definition in the ordered set where qualificationTypeId = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute definition, or <code>null</code> if a matching qualification attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition fetchByQualificationTypeId_Last(
        long qualificationTypeId, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByQualificationTypeId(qualificationTypeId);

        if (count == 0) {
            return null;
        }

        List<QualificationAttributeDefinition> list = findByQualificationTypeId(qualificationTypeId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualification attribute definitions before and after the current qualification attribute definition in the ordered set where qualificationTypeId = &#63;.
     *
     * @param qualificationAttributeDefinitionId the primary key of the current qualification attribute definition
     * @param qualificationTypeId the qualification type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification attribute definition
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a qualification attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition[] findByQualificationTypeId_PrevAndNext(
        long qualificationAttributeDefinitionId, long qualificationTypeId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionException, SystemException {
        QualificationAttributeDefinition qualificationAttributeDefinition = findByPrimaryKey(qualificationAttributeDefinitionId);

        Session session = null;

        try {
            session = openSession();

            QualificationAttributeDefinition[] array = new QualificationAttributeDefinitionImpl[3];

            array[0] = getByQualificationTypeId_PrevAndNext(session,
                    qualificationAttributeDefinition, qualificationTypeId,
                    orderByComparator, true);

            array[1] = qualificationAttributeDefinition;

            array[2] = getByQualificationTypeId_PrevAndNext(session,
                    qualificationAttributeDefinition, qualificationTypeId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected QualificationAttributeDefinition getByQualificationTypeId_PrevAndNext(
        Session session,
        QualificationAttributeDefinition qualificationAttributeDefinition,
        long qualificationTypeId, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITION_WHERE);

        query.append(_FINDER_COLUMN_QUALIFICATIONTYPEID_QUALIFICATIONTYPEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationAttributeDefinitionModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(qualificationTypeId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualificationAttributeDefinition);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<QualificationAttributeDefinition> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualification attribute definitions where qualificationTypeId = &#63; from the database.
     *
     * @param qualificationTypeId the qualification type ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByQualificationTypeId(long qualificationTypeId)
        throws SystemException {
        for (QualificationAttributeDefinition qualificationAttributeDefinition : findByQualificationTypeId(
                qualificationTypeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(qualificationAttributeDefinition);
        }
    }

    /**
     * Returns the number of qualification attribute definitions where qualificationTypeId = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @return the number of matching qualification attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByQualificationTypeId(long qualificationTypeId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEID;

        Object[] finderArgs = new Object[] { qualificationTypeId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_QUALIFICATIONATTRIBUTEDEFINITION_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEID_QUALIFICATIONTYPEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationTypeId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the qualification attribute definitions where title = &#63;.
     *
     * @param title the title
     * @return the matching qualification attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinition> findByTitle(String title)
        throws SystemException {
        return findByTitle(title, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification attribute definitions where title = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param title the title
     * @param start the lower bound of the range of qualification attribute definitions
     * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
     * @return the range of matching qualification attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinition> findByTitle(String title,
        int start, int end) throws SystemException {
        return findByTitle(title, start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification attribute definitions where title = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param title the title
     * @param start the lower bound of the range of qualification attribute definitions
     * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualification attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinition> findByTitle(String title,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE;
            finderArgs = new Object[] { title };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TITLE;
            finderArgs = new Object[] { title, start, end, orderByComparator };
        }

        List<QualificationAttributeDefinition> list = (List<QualificationAttributeDefinition>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (QualificationAttributeDefinition qualificationAttributeDefinition : list) {
                if (!Validator.equals(title,
                            qualificationAttributeDefinition.getTitle())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITION_WHERE);

            boolean bindTitle = false;

            if (title == null) {
                query.append(_FINDER_COLUMN_TITLE_TITLE_1);
            } else if (title.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_TITLE_TITLE_3);
            } else {
                bindTitle = true;

                query.append(_FINDER_COLUMN_TITLE_TITLE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationAttributeDefinitionModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindTitle) {
                    qPos.add(title.toLowerCase());
                }

                if (!pagination) {
                    list = (List<QualificationAttributeDefinition>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationAttributeDefinition>(list);
                } else {
                    list = (List<QualificationAttributeDefinition>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification attribute definition in the ordered set where title = &#63;.
     *
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute definition
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a matching qualification attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition findByTitle_First(String title,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionException, SystemException {
        QualificationAttributeDefinition qualificationAttributeDefinition = fetchByTitle_First(title,
                orderByComparator);

        if (qualificationAttributeDefinition != null) {
            return qualificationAttributeDefinition;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("title=");
        msg.append(title);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeDefinitionException(msg.toString());
    }

    /**
     * Returns the first qualification attribute definition in the ordered set where title = &#63;.
     *
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute definition, or <code>null</code> if a matching qualification attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition fetchByTitle_First(String title,
        OrderByComparator orderByComparator) throws SystemException {
        List<QualificationAttributeDefinition> list = findByTitle(title, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification attribute definition in the ordered set where title = &#63;.
     *
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute definition
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a matching qualification attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition findByTitle_Last(String title,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionException, SystemException {
        QualificationAttributeDefinition qualificationAttributeDefinition = fetchByTitle_Last(title,
                orderByComparator);

        if (qualificationAttributeDefinition != null) {
            return qualificationAttributeDefinition;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("title=");
        msg.append(title);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeDefinitionException(msg.toString());
    }

    /**
     * Returns the last qualification attribute definition in the ordered set where title = &#63;.
     *
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute definition, or <code>null</code> if a matching qualification attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition fetchByTitle_Last(String title,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByTitle(title);

        if (count == 0) {
            return null;
        }

        List<QualificationAttributeDefinition> list = findByTitle(title,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualification attribute definitions before and after the current qualification attribute definition in the ordered set where title = &#63;.
     *
     * @param qualificationAttributeDefinitionId the primary key of the current qualification attribute definition
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification attribute definition
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a qualification attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition[] findByTitle_PrevAndNext(
        long qualificationAttributeDefinitionId, String title,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionException, SystemException {
        QualificationAttributeDefinition qualificationAttributeDefinition = findByPrimaryKey(qualificationAttributeDefinitionId);

        Session session = null;

        try {
            session = openSession();

            QualificationAttributeDefinition[] array = new QualificationAttributeDefinitionImpl[3];

            array[0] = getByTitle_PrevAndNext(session,
                    qualificationAttributeDefinition, title, orderByComparator,
                    true);

            array[1] = qualificationAttributeDefinition;

            array[2] = getByTitle_PrevAndNext(session,
                    qualificationAttributeDefinition, title, orderByComparator,
                    false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected QualificationAttributeDefinition getByTitle_PrevAndNext(
        Session session,
        QualificationAttributeDefinition qualificationAttributeDefinition,
        String title, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITION_WHERE);

        boolean bindTitle = false;

        if (title == null) {
            query.append(_FINDER_COLUMN_TITLE_TITLE_1);
        } else if (title.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_TITLE_TITLE_3);
        } else {
            bindTitle = true;

            query.append(_FINDER_COLUMN_TITLE_TITLE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationAttributeDefinitionModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindTitle) {
            qPos.add(title.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualificationAttributeDefinition);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<QualificationAttributeDefinition> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualification attribute definitions where title = &#63; from the database.
     *
     * @param title the title
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByTitle(String title) throws SystemException {
        for (QualificationAttributeDefinition qualificationAttributeDefinition : findByTitle(
                title, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(qualificationAttributeDefinition);
        }
    }

    /**
     * Returns the number of qualification attribute definitions where title = &#63;.
     *
     * @param title the title
     * @return the number of matching qualification attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByTitle(String title) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_TITLE;

        Object[] finderArgs = new Object[] { title };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_QUALIFICATIONATTRIBUTEDEFINITION_WHERE);

            boolean bindTitle = false;

            if (title == null) {
                query.append(_FINDER_COLUMN_TITLE_TITLE_1);
            } else if (title.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_TITLE_TITLE_3);
            } else {
                bindTitle = true;

                query.append(_FINDER_COLUMN_TITLE_TITLE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindTitle) {
                    qPos.add(title.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the qualification attribute definitions where title LIKE &#63;.
     *
     * @param title the title
     * @return the matching qualification attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinition> findByTitleLike(String title)
        throws SystemException {
        return findByTitleLike(title, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification attribute definitions where title LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param title the title
     * @param start the lower bound of the range of qualification attribute definitions
     * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
     * @return the range of matching qualification attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinition> findByTitleLike(
        String title, int start, int end) throws SystemException {
        return findByTitleLike(title, start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification attribute definitions where title LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param title the title
     * @param start the lower bound of the range of qualification attribute definitions
     * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualification attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinition> findByTitleLike(
        String title, int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TITLELIKE;
        finderArgs = new Object[] { title, start, end, orderByComparator };

        List<QualificationAttributeDefinition> list = (List<QualificationAttributeDefinition>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (QualificationAttributeDefinition qualificationAttributeDefinition : list) {
                if (!StringUtil.wildcardMatches(
                            qualificationAttributeDefinition.getTitle(), title,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, false)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITION_WHERE);

            boolean bindTitle = false;

            if (title == null) {
                query.append(_FINDER_COLUMN_TITLELIKE_TITLE_1);
            } else if (title.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_TITLELIKE_TITLE_3);
            } else {
                bindTitle = true;

                query.append(_FINDER_COLUMN_TITLELIKE_TITLE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationAttributeDefinitionModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindTitle) {
                    qPos.add(title.toLowerCase());
                }

                if (!pagination) {
                    list = (List<QualificationAttributeDefinition>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationAttributeDefinition>(list);
                } else {
                    list = (List<QualificationAttributeDefinition>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification attribute definition in the ordered set where title LIKE &#63;.
     *
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute definition
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a matching qualification attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition findByTitleLike_First(
        String title, OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionException, SystemException {
        QualificationAttributeDefinition qualificationAttributeDefinition = fetchByTitleLike_First(title,
                orderByComparator);

        if (qualificationAttributeDefinition != null) {
            return qualificationAttributeDefinition;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("title=");
        msg.append(title);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeDefinitionException(msg.toString());
    }

    /**
     * Returns the first qualification attribute definition in the ordered set where title LIKE &#63;.
     *
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute definition, or <code>null</code> if a matching qualification attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition fetchByTitleLike_First(
        String title, OrderByComparator orderByComparator)
        throws SystemException {
        List<QualificationAttributeDefinition> list = findByTitleLike(title, 0,
                1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification attribute definition in the ordered set where title LIKE &#63;.
     *
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute definition
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a matching qualification attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition findByTitleLike_Last(String title,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionException, SystemException {
        QualificationAttributeDefinition qualificationAttributeDefinition = fetchByTitleLike_Last(title,
                orderByComparator);

        if (qualificationAttributeDefinition != null) {
            return qualificationAttributeDefinition;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("title=");
        msg.append(title);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeDefinitionException(msg.toString());
    }

    /**
     * Returns the last qualification attribute definition in the ordered set where title LIKE &#63;.
     *
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute definition, or <code>null</code> if a matching qualification attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition fetchByTitleLike_Last(
        String title, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByTitleLike(title);

        if (count == 0) {
            return null;
        }

        List<QualificationAttributeDefinition> list = findByTitleLike(title,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualification attribute definitions before and after the current qualification attribute definition in the ordered set where title LIKE &#63;.
     *
     * @param qualificationAttributeDefinitionId the primary key of the current qualification attribute definition
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification attribute definition
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a qualification attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition[] findByTitleLike_PrevAndNext(
        long qualificationAttributeDefinitionId, String title,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionException, SystemException {
        QualificationAttributeDefinition qualificationAttributeDefinition = findByPrimaryKey(qualificationAttributeDefinitionId);

        Session session = null;

        try {
            session = openSession();

            QualificationAttributeDefinition[] array = new QualificationAttributeDefinitionImpl[3];

            array[0] = getByTitleLike_PrevAndNext(session,
                    qualificationAttributeDefinition, title, orderByComparator,
                    true);

            array[1] = qualificationAttributeDefinition;

            array[2] = getByTitleLike_PrevAndNext(session,
                    qualificationAttributeDefinition, title, orderByComparator,
                    false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected QualificationAttributeDefinition getByTitleLike_PrevAndNext(
        Session session,
        QualificationAttributeDefinition qualificationAttributeDefinition,
        String title, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITION_WHERE);

        boolean bindTitle = false;

        if (title == null) {
            query.append(_FINDER_COLUMN_TITLELIKE_TITLE_1);
        } else if (title.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_TITLELIKE_TITLE_3);
        } else {
            bindTitle = true;

            query.append(_FINDER_COLUMN_TITLELIKE_TITLE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationAttributeDefinitionModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindTitle) {
            qPos.add(title.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualificationAttributeDefinition);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<QualificationAttributeDefinition> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualification attribute definitions where title LIKE &#63; from the database.
     *
     * @param title the title
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByTitleLike(String title) throws SystemException {
        for (QualificationAttributeDefinition qualificationAttributeDefinition : findByTitleLike(
                title, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(qualificationAttributeDefinition);
        }
    }

    /**
     * Returns the number of qualification attribute definitions where title LIKE &#63;.
     *
     * @param title the title
     * @return the number of matching qualification attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByTitleLike(String title) throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_TITLELIKE;

        Object[] finderArgs = new Object[] { title };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_QUALIFICATIONATTRIBUTEDEFINITION_WHERE);

            boolean bindTitle = false;

            if (title == null) {
                query.append(_FINDER_COLUMN_TITLELIKE_TITLE_1);
            } else if (title.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_TITLELIKE_TITLE_3);
            } else {
                bindTitle = true;

                query.append(_FINDER_COLUMN_TITLELIKE_TITLE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindTitle) {
                    qPos.add(title.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the qualification attribute definition in the entity cache if it is enabled.
     *
     * @param qualificationAttributeDefinition the qualification attribute definition
     */
    @Override
    public void cacheResult(
        QualificationAttributeDefinition qualificationAttributeDefinition) {
        EntityCacheUtil.putResult(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionImpl.class,
            qualificationAttributeDefinition.getPrimaryKey(),
            qualificationAttributeDefinition);

        qualificationAttributeDefinition.resetOriginalValues();
    }

    /**
     * Caches the qualification attribute definitions in the entity cache if it is enabled.
     *
     * @param qualificationAttributeDefinitions the qualification attribute definitions
     */
    @Override
    public void cacheResult(
        List<QualificationAttributeDefinition> qualificationAttributeDefinitions) {
        for (QualificationAttributeDefinition qualificationAttributeDefinition : qualificationAttributeDefinitions) {
            if (EntityCacheUtil.getResult(
                        QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
                        QualificationAttributeDefinitionImpl.class,
                        qualificationAttributeDefinition.getPrimaryKey()) == null) {
                cacheResult(qualificationAttributeDefinition);
            } else {
                qualificationAttributeDefinition.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all qualification attribute definitions.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(QualificationAttributeDefinitionImpl.class.getName());
        }

        EntityCacheUtil.clearCache(QualificationAttributeDefinitionImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the qualification attribute definition.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(
        QualificationAttributeDefinition qualificationAttributeDefinition) {
        EntityCacheUtil.removeResult(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionImpl.class,
            qualificationAttributeDefinition.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(
        List<QualificationAttributeDefinition> qualificationAttributeDefinitions) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (QualificationAttributeDefinition qualificationAttributeDefinition : qualificationAttributeDefinitions) {
            EntityCacheUtil.removeResult(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
                QualificationAttributeDefinitionImpl.class,
                qualificationAttributeDefinition.getPrimaryKey());
        }
    }

    /**
     * Creates a new qualification attribute definition with the primary key. Does not add the qualification attribute definition to the database.
     *
     * @param qualificationAttributeDefinitionId the primary key for the new qualification attribute definition
     * @return the new qualification attribute definition
     */
    @Override
    public QualificationAttributeDefinition create(
        long qualificationAttributeDefinitionId) {
        QualificationAttributeDefinition qualificationAttributeDefinition = new QualificationAttributeDefinitionImpl();

        qualificationAttributeDefinition.setNew(true);
        qualificationAttributeDefinition.setPrimaryKey(qualificationAttributeDefinitionId);

        return qualificationAttributeDefinition;
    }

    /**
     * Removes the qualification attribute definition with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param qualificationAttributeDefinitionId the primary key of the qualification attribute definition
     * @return the qualification attribute definition that was removed
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a qualification attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition remove(
        long qualificationAttributeDefinitionId)
        throws NoSuchQualificationAttributeDefinitionException, SystemException {
        return remove((Serializable) qualificationAttributeDefinitionId);
    }

    /**
     * Removes the qualification attribute definition with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the qualification attribute definition
     * @return the qualification attribute definition that was removed
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a qualification attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition remove(Serializable primaryKey)
        throws NoSuchQualificationAttributeDefinitionException, SystemException {
        Session session = null;

        try {
            session = openSession();

            QualificationAttributeDefinition qualificationAttributeDefinition = (QualificationAttributeDefinition) session.get(QualificationAttributeDefinitionImpl.class,
                    primaryKey);

            if (qualificationAttributeDefinition == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchQualificationAttributeDefinitionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(qualificationAttributeDefinition);
        } catch (NoSuchQualificationAttributeDefinitionException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected QualificationAttributeDefinition removeImpl(
        QualificationAttributeDefinition qualificationAttributeDefinition)
        throws SystemException {
        qualificationAttributeDefinition = toUnwrappedModel(qualificationAttributeDefinition);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(qualificationAttributeDefinition)) {
                qualificationAttributeDefinition = (QualificationAttributeDefinition) session.get(QualificationAttributeDefinitionImpl.class,
                        qualificationAttributeDefinition.getPrimaryKeyObj());
            }

            if (qualificationAttributeDefinition != null) {
                session.delete(qualificationAttributeDefinition);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (qualificationAttributeDefinition != null) {
            clearCache(qualificationAttributeDefinition);
        }

        return qualificationAttributeDefinition;
    }

    @Override
    public QualificationAttributeDefinition updateImpl(
        de.humance.eco.profile.model.QualificationAttributeDefinition qualificationAttributeDefinition)
        throws SystemException {
        qualificationAttributeDefinition = toUnwrappedModel(qualificationAttributeDefinition);

        boolean isNew = qualificationAttributeDefinition.isNew();

        QualificationAttributeDefinitionModelImpl qualificationAttributeDefinitionModelImpl =
            (QualificationAttributeDefinitionModelImpl) qualificationAttributeDefinition;

        Session session = null;

        try {
            session = openSession();

            if (qualificationAttributeDefinition.isNew()) {
                session.save(qualificationAttributeDefinition);

                qualificationAttributeDefinition.setNew(false);
            } else {
                session.merge(qualificationAttributeDefinition);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew ||
                !QualificationAttributeDefinitionModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((qualificationAttributeDefinitionModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        qualificationAttributeDefinitionModelImpl.getOriginalQualificationTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEID,
                    args);

                args = new Object[] {
                        qualificationAttributeDefinitionModelImpl.getQualificationTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEID,
                    args);
            }

            if ((qualificationAttributeDefinitionModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        qualificationAttributeDefinitionModelImpl.getOriginalTitle()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TITLE, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE,
                    args);

                args = new Object[] {
                        qualificationAttributeDefinitionModelImpl.getTitle()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TITLE, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE,
                    args);
            }
        }

        EntityCacheUtil.putResult(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionImpl.class,
            qualificationAttributeDefinition.getPrimaryKey(),
            qualificationAttributeDefinition);

        return qualificationAttributeDefinition;
    }

    protected QualificationAttributeDefinition toUnwrappedModel(
        QualificationAttributeDefinition qualificationAttributeDefinition) {
        if (qualificationAttributeDefinition instanceof QualificationAttributeDefinitionImpl) {
            return qualificationAttributeDefinition;
        }

        QualificationAttributeDefinitionImpl qualificationAttributeDefinitionImpl =
            new QualificationAttributeDefinitionImpl();

        qualificationAttributeDefinitionImpl.setNew(qualificationAttributeDefinition.isNew());
        qualificationAttributeDefinitionImpl.setPrimaryKey(qualificationAttributeDefinition.getPrimaryKey());

        qualificationAttributeDefinitionImpl.setQualificationAttributeDefinitionId(qualificationAttributeDefinition.getQualificationAttributeDefinitionId());
        qualificationAttributeDefinitionImpl.setCreatorId(qualificationAttributeDefinition.getCreatorId());
        qualificationAttributeDefinitionImpl.setCreatorName(qualificationAttributeDefinition.getCreatorName());
        qualificationAttributeDefinitionImpl.setCreateDate(qualificationAttributeDefinition.getCreateDate());
        qualificationAttributeDefinitionImpl.setModifierId(qualificationAttributeDefinition.getModifierId());
        qualificationAttributeDefinitionImpl.setModifierName(qualificationAttributeDefinition.getModifierName());
        qualificationAttributeDefinitionImpl.setModifiedDate(qualificationAttributeDefinition.getModifiedDate());
        qualificationAttributeDefinitionImpl.setQualificationTypeId(qualificationAttributeDefinition.getQualificationTypeId());
        qualificationAttributeDefinitionImpl.setType(qualificationAttributeDefinition.getType());
        qualificationAttributeDefinitionImpl.setTitle(qualificationAttributeDefinition.getTitle());
        qualificationAttributeDefinitionImpl.setMinRangeValue(qualificationAttributeDefinition.getMinRangeValue());
        qualificationAttributeDefinitionImpl.setMaxRangeValue(qualificationAttributeDefinition.getMaxRangeValue());
        qualificationAttributeDefinitionImpl.setUnit(qualificationAttributeDefinition.getUnit());
        qualificationAttributeDefinitionImpl.setSequenceNumber(qualificationAttributeDefinition.getSequenceNumber());

        return qualificationAttributeDefinitionImpl;
    }

    /**
     * Returns the qualification attribute definition with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the qualification attribute definition
     * @return the qualification attribute definition
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a qualification attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition findByPrimaryKey(
        Serializable primaryKey)
        throws NoSuchQualificationAttributeDefinitionException, SystemException {
        QualificationAttributeDefinition qualificationAttributeDefinition = fetchByPrimaryKey(primaryKey);

        if (qualificationAttributeDefinition == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchQualificationAttributeDefinitionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return qualificationAttributeDefinition;
    }

    /**
     * Returns the qualification attribute definition with the primary key or throws a {@link de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException} if it could not be found.
     *
     * @param qualificationAttributeDefinitionId the primary key of the qualification attribute definition
     * @return the qualification attribute definition
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a qualification attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition findByPrimaryKey(
        long qualificationAttributeDefinitionId)
        throws NoSuchQualificationAttributeDefinitionException, SystemException {
        return findByPrimaryKey((Serializable) qualificationAttributeDefinitionId);
    }

    /**
     * Returns the qualification attribute definition with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the qualification attribute definition
     * @return the qualification attribute definition, or <code>null</code> if a qualification attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition fetchByPrimaryKey(
        Serializable primaryKey) throws SystemException {
        QualificationAttributeDefinition qualificationAttributeDefinition = (QualificationAttributeDefinition) EntityCacheUtil.getResult(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
                QualificationAttributeDefinitionImpl.class, primaryKey);

        if (qualificationAttributeDefinition == _nullQualificationAttributeDefinition) {
            return null;
        }

        if (qualificationAttributeDefinition == null) {
            Session session = null;

            try {
                session = openSession();

                qualificationAttributeDefinition = (QualificationAttributeDefinition) session.get(QualificationAttributeDefinitionImpl.class,
                        primaryKey);

                if (qualificationAttributeDefinition != null) {
                    cacheResult(qualificationAttributeDefinition);
                } else {
                    EntityCacheUtil.putResult(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
                        QualificationAttributeDefinitionImpl.class, primaryKey,
                        _nullQualificationAttributeDefinition);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(QualificationAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
                    QualificationAttributeDefinitionImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return qualificationAttributeDefinition;
    }

    /**
     * Returns the qualification attribute definition with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param qualificationAttributeDefinitionId the primary key of the qualification attribute definition
     * @return the qualification attribute definition, or <code>null</code> if a qualification attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinition fetchByPrimaryKey(
        long qualificationAttributeDefinitionId) throws SystemException {
        return fetchByPrimaryKey((Serializable) qualificationAttributeDefinitionId);
    }

    /**
     * Returns all the qualification attribute definitions.
     *
     * @return the qualification attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinition> findAll()
        throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification attribute definitions.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of qualification attribute definitions
     * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
     * @return the range of qualification attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinition> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification attribute definitions.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of qualification attribute definitions
     * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of qualification attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinition> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<QualificationAttributeDefinition> list = (List<QualificationAttributeDefinition>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITION);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITION;

                if (pagination) {
                    sql = sql.concat(QualificationAttributeDefinitionModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<QualificationAttributeDefinition>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationAttributeDefinition>(list);
                } else {
                    list = (List<QualificationAttributeDefinition>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the qualification attribute definitions from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (QualificationAttributeDefinition qualificationAttributeDefinition : findAll()) {
            remove(qualificationAttributeDefinition);
        }
    }

    /**
     * Returns the number of qualification attribute definitions.
     *
     * @return the number of qualification attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_QUALIFICATIONATTRIBUTEDEFINITION);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the qualification attribute definition persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.de.humance.eco.profile.model.QualificationAttributeDefinition")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<QualificationAttributeDefinition>> listenersList =
                    new ArrayList<ModelListener<QualificationAttributeDefinition>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<QualificationAttributeDefinition>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(QualificationAttributeDefinitionImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
