package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException;
import de.humance.eco.profile.model.QualificationAttributeDefinitionResource;
import de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceImpl;
import de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl;
import de.humance.eco.profile.service.persistence.QualificationAttributeDefinitionResourcePersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the qualification attribute definition resource service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see QualificationAttributeDefinitionResourcePersistence
 * @see QualificationAttributeDefinitionResourceUtil
 * @generated
 */
public class QualificationAttributeDefinitionResourcePersistenceImpl
    extends BasePersistenceImpl<QualificationAttributeDefinitionResource>
    implements QualificationAttributeDefinitionResourcePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link QualificationAttributeDefinitionResourceUtil} to access the qualification attribute definition resource persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = QualificationAttributeDefinitionResourceImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
            new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFID =
        new FinderPath(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByQualificationAttribDefId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFID =
        new FinderPath(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByQualificationAttribDefId",
            new String[] { Long.class.getName() },
            QualificationAttributeDefinitionResourceModelImpl.QUALIFICATIONATTRIBUTEDEFINITIONID_COLUMN_BITMASK |
            QualificationAttributeDefinitionResourceModelImpl.COUNTRY_COLUMN_BITMASK |
            QualificationAttributeDefinitionResourceModelImpl.LANGUAGE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBDEFID =
        new FinderPath(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByQualificationAttribDefId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_QUALIFICATIONATTRIBDEFID_QUALIFICATIONATTRIBUTEDEFINITIONID_2 =
        "qualificationAttributeDefinitionResource.qualificationAttributeDefinitionId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDCOUNTRY =
        new FinderPath(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByQualificationAttribDefIdAndCountry",
            new String[] {
                Long.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDCOUNTRY =
        new FinderPath(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByQualificationAttribDefIdAndCountry",
            new String[] { Long.class.getName(), String.class.getName() },
            QualificationAttributeDefinitionResourceModelImpl.QUALIFICATIONATTRIBUTEDEFINITIONID_COLUMN_BITMASK |
            QualificationAttributeDefinitionResourceModelImpl.COUNTRY_COLUMN_BITMASK |
            QualificationAttributeDefinitionResourceModelImpl.LANGUAGE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBDEFIDANDCOUNTRY =
        new FinderPath(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByQualificationAttribDefIdAndCountry",
            new String[] { Long.class.getName(), String.class.getName() });
    private static final String _FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDCOUNTRY_QUALIFICATIONATTRIBUTEDEFINITIONID_2 =
        "qualificationAttributeDefinitionResource.qualificationAttributeDefinitionId = ? AND ";
    private static final String _FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDCOUNTRY_COUNTRY_1 =
        "qualificationAttributeDefinitionResource.country IS NULL";
    private static final String _FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDCOUNTRY_COUNTRY_2 =
        "lower(qualificationAttributeDefinitionResource.country) = ?";
    private static final String _FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDCOUNTRY_COUNTRY_3 =
        "(qualificationAttributeDefinitionResource.country IS NULL OR qualificationAttributeDefinitionResource.country = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDLANGUAGE =
        new FinderPath(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByQualificationAttribDefIdAndLanguage",
            new String[] {
                Long.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDLANGUAGE =
        new FinderPath(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByQualificationAttribDefIdAndLanguage",
            new String[] { Long.class.getName(), String.class.getName() },
            QualificationAttributeDefinitionResourceModelImpl.QUALIFICATIONATTRIBUTEDEFINITIONID_COLUMN_BITMASK |
            QualificationAttributeDefinitionResourceModelImpl.LANGUAGE_COLUMN_BITMASK |
            QualificationAttributeDefinitionResourceModelImpl.COUNTRY_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBDEFIDANDLANGUAGE =
        new FinderPath(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByQualificationAttribDefIdAndLanguage",
            new String[] { Long.class.getName(), String.class.getName() });
    private static final String _FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLANGUAGE_QUALIFICATIONATTRIBUTEDEFINITIONID_2 =
        "qualificationAttributeDefinitionResource.qualificationAttributeDefinitionId = ? AND ";
    private static final String _FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLANGUAGE_LANGUAGE_1 =
        "qualificationAttributeDefinitionResource.language IS NULL";
    private static final String _FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLANGUAGE_LANGUAGE_2 =
        "lower(qualificationAttributeDefinitionResource.language) = ?";
    private static final String _FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLANGUAGE_LANGUAGE_3 =
        "(qualificationAttributeDefinitionResource.language IS NULL OR qualificationAttributeDefinitionResource.language = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDLOCALE =
        new FinderPath(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByQualificationAttribDefIdAndLocale",
            new String[] {
                Long.class.getName(), String.class.getName(),
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDLOCALE =
        new FinderPath(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByQualificationAttribDefIdAndLocale",
            new String[] {
                Long.class.getName(), String.class.getName(),
                String.class.getName()
            },
            QualificationAttributeDefinitionResourceModelImpl.QUALIFICATIONATTRIBUTEDEFINITIONID_COLUMN_BITMASK |
            QualificationAttributeDefinitionResourceModelImpl.COUNTRY_COLUMN_BITMASK |
            QualificationAttributeDefinitionResourceModelImpl.LANGUAGE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBDEFIDANDLOCALE =
        new FinderPath(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByQualificationAttribDefIdAndLocale",
            new String[] {
                Long.class.getName(), String.class.getName(),
                String.class.getName()
            });
    private static final String _FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_QUALIFICATIONATTRIBUTEDEFINITIONID_2 =
        "qualificationAttributeDefinitionResource.qualificationAttributeDefinitionId = ? AND ";
    private static final String _FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_COUNTRY_1 =
        "qualificationAttributeDefinitionResource.country IS NULL AND ";
    private static final String _FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_COUNTRY_2 =
        "lower(qualificationAttributeDefinitionResource.country) = ? AND ";
    private static final String _FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_COUNTRY_3 =
        "(qualificationAttributeDefinitionResource.country IS NULL OR qualificationAttributeDefinitionResource.country = '') AND ";
    private static final String _FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_LANGUAGE_1 =
        "qualificationAttributeDefinitionResource.language IS NULL";
    private static final String _FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_LANGUAGE_2 =
        "lower(qualificationAttributeDefinitionResource.language) = ?";
    private static final String _FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_LANGUAGE_3 =
        "(qualificationAttributeDefinitionResource.language IS NULL OR qualificationAttributeDefinitionResource.language = '')";
    private static final String _SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE =
        "SELECT qualificationAttributeDefinitionResource FROM QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource";
    private static final String _SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE_WHERE =
        "SELECT qualificationAttributeDefinitionResource FROM QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource WHERE ";
    private static final String _SQL_COUNT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE =
        "SELECT COUNT(qualificationAttributeDefinitionResource) FROM QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource";
    private static final String _SQL_COUNT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE_WHERE =
        "SELECT COUNT(qualificationAttributeDefinitionResource) FROM QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "qualificationAttributeDefinitionResource.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No QualificationAttributeDefinitionResource exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No QualificationAttributeDefinitionResource exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(QualificationAttributeDefinitionResourcePersistenceImpl.class);
    private static QualificationAttributeDefinitionResource _nullQualificationAttributeDefinitionResource =
        new QualificationAttributeDefinitionResourceImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<QualificationAttributeDefinitionResource> toCacheModel() {
                return _nullQualificationAttributeDefinitionResourceCacheModel;
            }
        };

    private static CacheModel<QualificationAttributeDefinitionResource> _nullQualificationAttributeDefinitionResourceCacheModel =
        new CacheModel<QualificationAttributeDefinitionResource>() {
            @Override
            public QualificationAttributeDefinitionResource toEntityModel() {
                return _nullQualificationAttributeDefinitionResource;
            }
        };

    public QualificationAttributeDefinitionResourcePersistenceImpl() {
        setModelClass(QualificationAttributeDefinitionResource.class);
    }

    /**
     * Returns all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @return the matching qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinitionResource> findByQualificationAttribDefId(
        long qualificationAttributeDefinitionId) throws SystemException {
        return findByQualificationAttribDefId(qualificationAttributeDefinitionId,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param start the lower bound of the range of qualification attribute definition resources
     * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
     * @return the range of matching qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinitionResource> findByQualificationAttribDefId(
        long qualificationAttributeDefinitionId, int start, int end)
        throws SystemException {
        return findByQualificationAttribDefId(qualificationAttributeDefinitionId,
            start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param start the lower bound of the range of qualification attribute definition resources
     * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinitionResource> findByQualificationAttribDefId(
        long qualificationAttributeDefinitionId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFID;
            finderArgs = new Object[] { qualificationAttributeDefinitionId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFID;
            finderArgs = new Object[] {
                    qualificationAttributeDefinitionId,
                    
                    start, end, orderByComparator
                };
        }

        List<QualificationAttributeDefinitionResource> list = (List<QualificationAttributeDefinitionResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource : list) {
                if ((qualificationAttributeDefinitionId != qualificationAttributeDefinitionResource.getQualificationAttributeDefinitionId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFID_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationAttributeDefinitionId);

                if (!pagination) {
                    list = (List<QualificationAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationAttributeDefinitionResource>(list);
                } else {
                    list = (List<QualificationAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute definition resource
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a matching qualification attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource findByQualificationAttribDefId_First(
        long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionResourceException,
            SystemException {
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource =
            fetchByQualificationAttribDefId_First(qualificationAttributeDefinitionId,
                orderByComparator);

        if (qualificationAttributeDefinitionResource != null) {
            return qualificationAttributeDefinitionResource;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationAttributeDefinitionId=");
        msg.append(qualificationAttributeDefinitionId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeDefinitionResourceException(msg.toString());
    }

    /**
     * Returns the first qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute definition resource, or <code>null</code> if a matching qualification attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource fetchByQualificationAttribDefId_First(
        long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator) throws SystemException {
        List<QualificationAttributeDefinitionResource> list = findByQualificationAttribDefId(qualificationAttributeDefinitionId,
                0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute definition resource
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a matching qualification attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource findByQualificationAttribDefId_Last(
        long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionResourceException,
            SystemException {
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource =
            fetchByQualificationAttribDefId_Last(qualificationAttributeDefinitionId,
                orderByComparator);

        if (qualificationAttributeDefinitionResource != null) {
            return qualificationAttributeDefinitionResource;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationAttributeDefinitionId=");
        msg.append(qualificationAttributeDefinitionId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeDefinitionResourceException(msg.toString());
    }

    /**
     * Returns the last qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute definition resource, or <code>null</code> if a matching qualification attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource fetchByQualificationAttribDefId_Last(
        long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByQualificationAttribDefId(qualificationAttributeDefinitionId);

        if (count == 0) {
            return null;
        }

        List<QualificationAttributeDefinitionResource> list = findByQualificationAttribDefId(qualificationAttributeDefinitionId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualification attribute definition resources before and after the current qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationAttributeDefinitionResourceId the primary key of the current qualification attribute definition resource
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification attribute definition resource
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a qualification attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource[] findByQualificationAttribDefId_PrevAndNext(
        long qualificationAttributeDefinitionResourceId,
        long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionResourceException,
            SystemException {
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource =
            findByPrimaryKey(qualificationAttributeDefinitionResourceId);

        Session session = null;

        try {
            session = openSession();

            QualificationAttributeDefinitionResource[] array = new QualificationAttributeDefinitionResourceImpl[3];

            array[0] = getByQualificationAttribDefId_PrevAndNext(session,
                    qualificationAttributeDefinitionResource,
                    qualificationAttributeDefinitionId, orderByComparator, true);

            array[1] = qualificationAttributeDefinitionResource;

            array[2] = getByQualificationAttribDefId_PrevAndNext(session,
                    qualificationAttributeDefinitionResource,
                    qualificationAttributeDefinitionId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected QualificationAttributeDefinitionResource getByQualificationAttribDefId_PrevAndNext(
        Session session,
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource,
        long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE_WHERE);

        query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFID_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(qualificationAttributeDefinitionId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualificationAttributeDefinitionResource);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<QualificationAttributeDefinitionResource> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; from the database.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByQualificationAttribDefId(
        long qualificationAttributeDefinitionId) throws SystemException {
        for (QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource : findByQualificationAttribDefId(
                qualificationAttributeDefinitionId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(qualificationAttributeDefinitionResource);
        }
    }

    /**
     * Returns the number of qualification attribute definition resources where qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @return the number of matching qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByQualificationAttribDefId(
        long qualificationAttributeDefinitionId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBDEFID;

        Object[] finderArgs = new Object[] { qualificationAttributeDefinitionId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFID_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationAttributeDefinitionId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @return the matching qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndCountry(
        long qualificationAttributeDefinitionId, String country)
        throws SystemException {
        return findByQualificationAttribDefIdAndCountry(qualificationAttributeDefinitionId,
            country, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @param start the lower bound of the range of qualification attribute definition resources
     * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
     * @return the range of matching qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndCountry(
        long qualificationAttributeDefinitionId, String country, int start,
        int end) throws SystemException {
        return findByQualificationAttribDefIdAndCountry(qualificationAttributeDefinitionId,
            country, start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @param start the lower bound of the range of qualification attribute definition resources
     * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndCountry(
        long qualificationAttributeDefinitionId, String country, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDCOUNTRY;
            finderArgs = new Object[] {
                    qualificationAttributeDefinitionId, country
                };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDCOUNTRY;
            finderArgs = new Object[] {
                    qualificationAttributeDefinitionId, country,
                    
                    start, end, orderByComparator
                };
        }

        List<QualificationAttributeDefinitionResource> list = (List<QualificationAttributeDefinitionResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource : list) {
                if ((qualificationAttributeDefinitionId != qualificationAttributeDefinitionResource.getQualificationAttributeDefinitionId()) ||
                        !Validator.equals(country,
                            qualificationAttributeDefinitionResource.getCountry())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDCOUNTRY_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

            boolean bindCountry = false;

            if (country == null) {
                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDCOUNTRY_COUNTRY_1);
            } else if (country.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDCOUNTRY_COUNTRY_3);
            } else {
                bindCountry = true;

                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDCOUNTRY_COUNTRY_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationAttributeDefinitionId);

                if (bindCountry) {
                    qPos.add(country.toLowerCase());
                }

                if (!pagination) {
                    list = (List<QualificationAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationAttributeDefinitionResource>(list);
                } else {
                    list = (List<QualificationAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute definition resource
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a matching qualification attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource findByQualificationAttribDefIdAndCountry_First(
        long qualificationAttributeDefinitionId, String country,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionResourceException,
            SystemException {
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource =
            fetchByQualificationAttribDefIdAndCountry_First(qualificationAttributeDefinitionId,
                country, orderByComparator);

        if (qualificationAttributeDefinitionResource != null) {
            return qualificationAttributeDefinitionResource;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationAttributeDefinitionId=");
        msg.append(qualificationAttributeDefinitionId);

        msg.append(", country=");
        msg.append(country);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeDefinitionResourceException(msg.toString());
    }

    /**
     * Returns the first qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute definition resource, or <code>null</code> if a matching qualification attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource fetchByQualificationAttribDefIdAndCountry_First(
        long qualificationAttributeDefinitionId, String country,
        OrderByComparator orderByComparator) throws SystemException {
        List<QualificationAttributeDefinitionResource> list = findByQualificationAttribDefIdAndCountry(qualificationAttributeDefinitionId,
                country, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute definition resource
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a matching qualification attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource findByQualificationAttribDefIdAndCountry_Last(
        long qualificationAttributeDefinitionId, String country,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionResourceException,
            SystemException {
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource =
            fetchByQualificationAttribDefIdAndCountry_Last(qualificationAttributeDefinitionId,
                country, orderByComparator);

        if (qualificationAttributeDefinitionResource != null) {
            return qualificationAttributeDefinitionResource;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationAttributeDefinitionId=");
        msg.append(qualificationAttributeDefinitionId);

        msg.append(", country=");
        msg.append(country);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeDefinitionResourceException(msg.toString());
    }

    /**
     * Returns the last qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute definition resource, or <code>null</code> if a matching qualification attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource fetchByQualificationAttribDefIdAndCountry_Last(
        long qualificationAttributeDefinitionId, String country,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByQualificationAttribDefIdAndCountry(qualificationAttributeDefinitionId,
                country);

        if (count == 0) {
            return null;
        }

        List<QualificationAttributeDefinitionResource> list = findByQualificationAttribDefIdAndCountry(qualificationAttributeDefinitionId,
                country, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualification attribute definition resources before and after the current qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63;.
     *
     * @param qualificationAttributeDefinitionResourceId the primary key of the current qualification attribute definition resource
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification attribute definition resource
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a qualification attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource[] findByQualificationAttribDefIdAndCountry_PrevAndNext(
        long qualificationAttributeDefinitionResourceId,
        long qualificationAttributeDefinitionId, String country,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionResourceException,
            SystemException {
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource =
            findByPrimaryKey(qualificationAttributeDefinitionResourceId);

        Session session = null;

        try {
            session = openSession();

            QualificationAttributeDefinitionResource[] array = new QualificationAttributeDefinitionResourceImpl[3];

            array[0] = getByQualificationAttribDefIdAndCountry_PrevAndNext(session,
                    qualificationAttributeDefinitionResource,
                    qualificationAttributeDefinitionId, country,
                    orderByComparator, true);

            array[1] = qualificationAttributeDefinitionResource;

            array[2] = getByQualificationAttribDefIdAndCountry_PrevAndNext(session,
                    qualificationAttributeDefinitionResource,
                    qualificationAttributeDefinitionId, country,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected QualificationAttributeDefinitionResource getByQualificationAttribDefIdAndCountry_PrevAndNext(
        Session session,
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource,
        long qualificationAttributeDefinitionId, String country,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE_WHERE);

        query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDCOUNTRY_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

        boolean bindCountry = false;

        if (country == null) {
            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDCOUNTRY_COUNTRY_1);
        } else if (country.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDCOUNTRY_COUNTRY_3);
        } else {
            bindCountry = true;

            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDCOUNTRY_COUNTRY_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(qualificationAttributeDefinitionId);

        if (bindCountry) {
            qPos.add(country.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualificationAttributeDefinitionResource);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<QualificationAttributeDefinitionResource> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63; from the database.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByQualificationAttribDefIdAndCountry(
        long qualificationAttributeDefinitionId, String country)
        throws SystemException {
        for (QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource : findByQualificationAttribDefIdAndCountry(
                qualificationAttributeDefinitionId, country, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(qualificationAttributeDefinitionResource);
        }
    }

    /**
     * Returns the number of qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @return the number of matching qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByQualificationAttribDefIdAndCountry(
        long qualificationAttributeDefinitionId, String country)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBDEFIDANDCOUNTRY;

        Object[] finderArgs = new Object[] {
                qualificationAttributeDefinitionId, country
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDCOUNTRY_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

            boolean bindCountry = false;

            if (country == null) {
                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDCOUNTRY_COUNTRY_1);
            } else if (country.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDCOUNTRY_COUNTRY_3);
            } else {
                bindCountry = true;

                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDCOUNTRY_COUNTRY_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationAttributeDefinitionId);

                if (bindCountry) {
                    qPos.add(country.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and language = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param language the language
     * @return the matching qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndLanguage(
        long qualificationAttributeDefinitionId, String language)
        throws SystemException {
        return findByQualificationAttribDefIdAndLanguage(qualificationAttributeDefinitionId,
            language, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and language = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param language the language
     * @param start the lower bound of the range of qualification attribute definition resources
     * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
     * @return the range of matching qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndLanguage(
        long qualificationAttributeDefinitionId, String language, int start,
        int end) throws SystemException {
        return findByQualificationAttribDefIdAndLanguage(qualificationAttributeDefinitionId,
            language, start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and language = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param language the language
     * @param start the lower bound of the range of qualification attribute definition resources
     * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndLanguage(
        long qualificationAttributeDefinitionId, String language, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDLANGUAGE;
            finderArgs = new Object[] {
                    qualificationAttributeDefinitionId, language
                };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDLANGUAGE;
            finderArgs = new Object[] {
                    qualificationAttributeDefinitionId, language,
                    
                    start, end, orderByComparator
                };
        }

        List<QualificationAttributeDefinitionResource> list = (List<QualificationAttributeDefinitionResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource : list) {
                if ((qualificationAttributeDefinitionId != qualificationAttributeDefinitionResource.getQualificationAttributeDefinitionId()) ||
                        !Validator.equals(language,
                            qualificationAttributeDefinitionResource.getLanguage())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLANGUAGE_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

            boolean bindLanguage = false;

            if (language == null) {
                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLANGUAGE_LANGUAGE_1);
            } else if (language.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLANGUAGE_LANGUAGE_3);
            } else {
                bindLanguage = true;

                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLANGUAGE_LANGUAGE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationAttributeDefinitionId);

                if (bindLanguage) {
                    qPos.add(language.toLowerCase());
                }

                if (!pagination) {
                    list = (List<QualificationAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationAttributeDefinitionResource>(list);
                } else {
                    list = (List<QualificationAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and language = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute definition resource
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a matching qualification attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource findByQualificationAttribDefIdAndLanguage_First(
        long qualificationAttributeDefinitionId, String language,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionResourceException,
            SystemException {
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource =
            fetchByQualificationAttribDefIdAndLanguage_First(qualificationAttributeDefinitionId,
                language, orderByComparator);

        if (qualificationAttributeDefinitionResource != null) {
            return qualificationAttributeDefinitionResource;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationAttributeDefinitionId=");
        msg.append(qualificationAttributeDefinitionId);

        msg.append(", language=");
        msg.append(language);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeDefinitionResourceException(msg.toString());
    }

    /**
     * Returns the first qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and language = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute definition resource, or <code>null</code> if a matching qualification attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource fetchByQualificationAttribDefIdAndLanguage_First(
        long qualificationAttributeDefinitionId, String language,
        OrderByComparator orderByComparator) throws SystemException {
        List<QualificationAttributeDefinitionResource> list = findByQualificationAttribDefIdAndLanguage(qualificationAttributeDefinitionId,
                language, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and language = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute definition resource
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a matching qualification attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource findByQualificationAttribDefIdAndLanguage_Last(
        long qualificationAttributeDefinitionId, String language,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionResourceException,
            SystemException {
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource =
            fetchByQualificationAttribDefIdAndLanguage_Last(qualificationAttributeDefinitionId,
                language, orderByComparator);

        if (qualificationAttributeDefinitionResource != null) {
            return qualificationAttributeDefinitionResource;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationAttributeDefinitionId=");
        msg.append(qualificationAttributeDefinitionId);

        msg.append(", language=");
        msg.append(language);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeDefinitionResourceException(msg.toString());
    }

    /**
     * Returns the last qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and language = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute definition resource, or <code>null</code> if a matching qualification attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource fetchByQualificationAttribDefIdAndLanguage_Last(
        long qualificationAttributeDefinitionId, String language,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByQualificationAttribDefIdAndLanguage(qualificationAttributeDefinitionId,
                language);

        if (count == 0) {
            return null;
        }

        List<QualificationAttributeDefinitionResource> list = findByQualificationAttribDefIdAndLanguage(qualificationAttributeDefinitionId,
                language, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualification attribute definition resources before and after the current qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and language = &#63;.
     *
     * @param qualificationAttributeDefinitionResourceId the primary key of the current qualification attribute definition resource
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification attribute definition resource
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a qualification attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource[] findByQualificationAttribDefIdAndLanguage_PrevAndNext(
        long qualificationAttributeDefinitionResourceId,
        long qualificationAttributeDefinitionId, String language,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionResourceException,
            SystemException {
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource =
            findByPrimaryKey(qualificationAttributeDefinitionResourceId);

        Session session = null;

        try {
            session = openSession();

            QualificationAttributeDefinitionResource[] array = new QualificationAttributeDefinitionResourceImpl[3];

            array[0] = getByQualificationAttribDefIdAndLanguage_PrevAndNext(session,
                    qualificationAttributeDefinitionResource,
                    qualificationAttributeDefinitionId, language,
                    orderByComparator, true);

            array[1] = qualificationAttributeDefinitionResource;

            array[2] = getByQualificationAttribDefIdAndLanguage_PrevAndNext(session,
                    qualificationAttributeDefinitionResource,
                    qualificationAttributeDefinitionId, language,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected QualificationAttributeDefinitionResource getByQualificationAttribDefIdAndLanguage_PrevAndNext(
        Session session,
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource,
        long qualificationAttributeDefinitionId, String language,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE_WHERE);

        query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLANGUAGE_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

        boolean bindLanguage = false;

        if (language == null) {
            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLANGUAGE_LANGUAGE_1);
        } else if (language.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLANGUAGE_LANGUAGE_3);
        } else {
            bindLanguage = true;

            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLANGUAGE_LANGUAGE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(qualificationAttributeDefinitionId);

        if (bindLanguage) {
            qPos.add(language.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualificationAttributeDefinitionResource);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<QualificationAttributeDefinitionResource> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and language = &#63; from the database.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param language the language
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByQualificationAttribDefIdAndLanguage(
        long qualificationAttributeDefinitionId, String language)
        throws SystemException {
        for (QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource : findByQualificationAttribDefIdAndLanguage(
                qualificationAttributeDefinitionId, language,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(qualificationAttributeDefinitionResource);
        }
    }

    /**
     * Returns the number of qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and language = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param language the language
     * @return the number of matching qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByQualificationAttribDefIdAndLanguage(
        long qualificationAttributeDefinitionId, String language)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBDEFIDANDLANGUAGE;

        Object[] finderArgs = new Object[] {
                qualificationAttributeDefinitionId, language
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLANGUAGE_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

            boolean bindLanguage = false;

            if (language == null) {
                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLANGUAGE_LANGUAGE_1);
            } else if (language.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLANGUAGE_LANGUAGE_3);
            } else {
                bindLanguage = true;

                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLANGUAGE_LANGUAGE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationAttributeDefinitionId);

                if (bindLanguage) {
                    qPos.add(language.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @param language the language
     * @return the matching qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndLocale(
        long qualificationAttributeDefinitionId, String country, String language)
        throws SystemException {
        return findByQualificationAttribDefIdAndLocale(qualificationAttributeDefinitionId,
            country, language, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @param language the language
     * @param start the lower bound of the range of qualification attribute definition resources
     * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
     * @return the range of matching qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndLocale(
        long qualificationAttributeDefinitionId, String country,
        String language, int start, int end) throws SystemException {
        return findByQualificationAttribDefIdAndLocale(qualificationAttributeDefinitionId,
            country, language, start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @param language the language
     * @param start the lower bound of the range of qualification attribute definition resources
     * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndLocale(
        long qualificationAttributeDefinitionId, String country,
        String language, int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDLOCALE;
            finderArgs = new Object[] {
                    qualificationAttributeDefinitionId, country, language
                };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDLOCALE;
            finderArgs = new Object[] {
                    qualificationAttributeDefinitionId, country, language,
                    
                    start, end, orderByComparator
                };
        }

        List<QualificationAttributeDefinitionResource> list = (List<QualificationAttributeDefinitionResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource : list) {
                if ((qualificationAttributeDefinitionId != qualificationAttributeDefinitionResource.getQualificationAttributeDefinitionId()) ||
                        !Validator.equals(country,
                            qualificationAttributeDefinitionResource.getCountry()) ||
                        !Validator.equals(language,
                            qualificationAttributeDefinitionResource.getLanguage())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(5 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(5);
            }

            query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

            boolean bindCountry = false;

            if (country == null) {
                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_COUNTRY_1);
            } else if (country.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_COUNTRY_3);
            } else {
                bindCountry = true;

                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_COUNTRY_2);
            }

            boolean bindLanguage = false;

            if (language == null) {
                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_LANGUAGE_1);
            } else if (language.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_LANGUAGE_3);
            } else {
                bindLanguage = true;

                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_LANGUAGE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationAttributeDefinitionId);

                if (bindCountry) {
                    qPos.add(country.toLowerCase());
                }

                if (bindLanguage) {
                    qPos.add(language.toLowerCase());
                }

                if (!pagination) {
                    list = (List<QualificationAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationAttributeDefinitionResource>(list);
                } else {
                    list = (List<QualificationAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute definition resource
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a matching qualification attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource findByQualificationAttribDefIdAndLocale_First(
        long qualificationAttributeDefinitionId, String country,
        String language, OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionResourceException,
            SystemException {
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource =
            fetchByQualificationAttribDefIdAndLocale_First(qualificationAttributeDefinitionId,
                country, language, orderByComparator);

        if (qualificationAttributeDefinitionResource != null) {
            return qualificationAttributeDefinitionResource;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationAttributeDefinitionId=");
        msg.append(qualificationAttributeDefinitionId);

        msg.append(", country=");
        msg.append(country);

        msg.append(", language=");
        msg.append(language);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeDefinitionResourceException(msg.toString());
    }

    /**
     * Returns the first qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute definition resource, or <code>null</code> if a matching qualification attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource fetchByQualificationAttribDefIdAndLocale_First(
        long qualificationAttributeDefinitionId, String country,
        String language, OrderByComparator orderByComparator)
        throws SystemException {
        List<QualificationAttributeDefinitionResource> list = findByQualificationAttribDefIdAndLocale(qualificationAttributeDefinitionId,
                country, language, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute definition resource
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a matching qualification attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource findByQualificationAttribDefIdAndLocale_Last(
        long qualificationAttributeDefinitionId, String country,
        String language, OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionResourceException,
            SystemException {
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource =
            fetchByQualificationAttribDefIdAndLocale_Last(qualificationAttributeDefinitionId,
                country, language, orderByComparator);

        if (qualificationAttributeDefinitionResource != null) {
            return qualificationAttributeDefinitionResource;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationAttributeDefinitionId=");
        msg.append(qualificationAttributeDefinitionId);

        msg.append(", country=");
        msg.append(country);

        msg.append(", language=");
        msg.append(language);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeDefinitionResourceException(msg.toString());
    }

    /**
     * Returns the last qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute definition resource, or <code>null</code> if a matching qualification attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource fetchByQualificationAttribDefIdAndLocale_Last(
        long qualificationAttributeDefinitionId, String country,
        String language, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByQualificationAttribDefIdAndLocale(qualificationAttributeDefinitionId,
                country, language);

        if (count == 0) {
            return null;
        }

        List<QualificationAttributeDefinitionResource> list = findByQualificationAttribDefIdAndLocale(qualificationAttributeDefinitionId,
                country, language, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualification attribute definition resources before and after the current qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * @param qualificationAttributeDefinitionResourceId the primary key of the current qualification attribute definition resource
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification attribute definition resource
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a qualification attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource[] findByQualificationAttribDefIdAndLocale_PrevAndNext(
        long qualificationAttributeDefinitionResourceId,
        long qualificationAttributeDefinitionId, String country,
        String language, OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeDefinitionResourceException,
            SystemException {
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource =
            findByPrimaryKey(qualificationAttributeDefinitionResourceId);

        Session session = null;

        try {
            session = openSession();

            QualificationAttributeDefinitionResource[] array = new QualificationAttributeDefinitionResourceImpl[3];

            array[0] = getByQualificationAttribDefIdAndLocale_PrevAndNext(session,
                    qualificationAttributeDefinitionResource,
                    qualificationAttributeDefinitionId, country, language,
                    orderByComparator, true);

            array[1] = qualificationAttributeDefinitionResource;

            array[2] = getByQualificationAttribDefIdAndLocale_PrevAndNext(session,
                    qualificationAttributeDefinitionResource,
                    qualificationAttributeDefinitionId, country, language,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected QualificationAttributeDefinitionResource getByQualificationAttribDefIdAndLocale_PrevAndNext(
        Session session,
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource,
        long qualificationAttributeDefinitionId, String country,
        String language, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE_WHERE);

        query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

        boolean bindCountry = false;

        if (country == null) {
            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_COUNTRY_1);
        } else if (country.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_COUNTRY_3);
        } else {
            bindCountry = true;

            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_COUNTRY_2);
        }

        boolean bindLanguage = false;

        if (language == null) {
            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_LANGUAGE_1);
        } else if (language.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_LANGUAGE_3);
        } else {
            bindLanguage = true;

            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_LANGUAGE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(qualificationAttributeDefinitionId);

        if (bindCountry) {
            qPos.add(country.toLowerCase());
        }

        if (bindLanguage) {
            qPos.add(language.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualificationAttributeDefinitionResource);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<QualificationAttributeDefinitionResource> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63; from the database.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @param language the language
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByQualificationAttribDefIdAndLocale(
        long qualificationAttributeDefinitionId, String country, String language)
        throws SystemException {
        for (QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource : findByQualificationAttribDefIdAndLocale(
                qualificationAttributeDefinitionId, country, language,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(qualificationAttributeDefinitionResource);
        }
    }

    /**
     * Returns the number of qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param country the country
     * @param language the language
     * @return the number of matching qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByQualificationAttribDefIdAndLocale(
        long qualificationAttributeDefinitionId, String country, String language)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBDEFIDANDLOCALE;

        Object[] finderArgs = new Object[] {
                qualificationAttributeDefinitionId, country, language
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_COUNT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

            boolean bindCountry = false;

            if (country == null) {
                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_COUNTRY_1);
            } else if (country.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_COUNTRY_3);
            } else {
                bindCountry = true;

                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_COUNTRY_2);
            }

            boolean bindLanguage = false;

            if (language == null) {
                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_LANGUAGE_1);
            } else if (language.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_LANGUAGE_3);
            } else {
                bindLanguage = true;

                query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBDEFIDANDLOCALE_LANGUAGE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationAttributeDefinitionId);

                if (bindCountry) {
                    qPos.add(country.toLowerCase());
                }

                if (bindLanguage) {
                    qPos.add(language.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the qualification attribute definition resource in the entity cache if it is enabled.
     *
     * @param qualificationAttributeDefinitionResource the qualification attribute definition resource
     */
    @Override
    public void cacheResult(
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource) {
        EntityCacheUtil.putResult(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceImpl.class,
            qualificationAttributeDefinitionResource.getPrimaryKey(),
            qualificationAttributeDefinitionResource);

        qualificationAttributeDefinitionResource.resetOriginalValues();
    }

    /**
     * Caches the qualification attribute definition resources in the entity cache if it is enabled.
     *
     * @param qualificationAttributeDefinitionResources the qualification attribute definition resources
     */
    @Override
    public void cacheResult(
        List<QualificationAttributeDefinitionResource> qualificationAttributeDefinitionResources) {
        for (QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource : qualificationAttributeDefinitionResources) {
            if (EntityCacheUtil.getResult(
                        QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
                        QualificationAttributeDefinitionResourceImpl.class,
                        qualificationAttributeDefinitionResource.getPrimaryKey()) == null) {
                cacheResult(qualificationAttributeDefinitionResource);
            } else {
                qualificationAttributeDefinitionResource.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all qualification attribute definition resources.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(QualificationAttributeDefinitionResourceImpl.class.getName());
        }

        EntityCacheUtil.clearCache(QualificationAttributeDefinitionResourceImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the qualification attribute definition resource.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource) {
        EntityCacheUtil.removeResult(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceImpl.class,
            qualificationAttributeDefinitionResource.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(
        List<QualificationAttributeDefinitionResource> qualificationAttributeDefinitionResources) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource : qualificationAttributeDefinitionResources) {
            EntityCacheUtil.removeResult(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
                QualificationAttributeDefinitionResourceImpl.class,
                qualificationAttributeDefinitionResource.getPrimaryKey());
        }
    }

    /**
     * Creates a new qualification attribute definition resource with the primary key. Does not add the qualification attribute definition resource to the database.
     *
     * @param qualificationAttributeDefinitionResourceId the primary key for the new qualification attribute definition resource
     * @return the new qualification attribute definition resource
     */
    @Override
    public QualificationAttributeDefinitionResource create(
        long qualificationAttributeDefinitionResourceId) {
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource =
            new QualificationAttributeDefinitionResourceImpl();

        qualificationAttributeDefinitionResource.setNew(true);
        qualificationAttributeDefinitionResource.setPrimaryKey(qualificationAttributeDefinitionResourceId);

        return qualificationAttributeDefinitionResource;
    }

    /**
     * Removes the qualification attribute definition resource with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param qualificationAttributeDefinitionResourceId the primary key of the qualification attribute definition resource
     * @return the qualification attribute definition resource that was removed
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a qualification attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource remove(
        long qualificationAttributeDefinitionResourceId)
        throws NoSuchQualificationAttributeDefinitionResourceException,
            SystemException {
        return remove((Serializable) qualificationAttributeDefinitionResourceId);
    }

    /**
     * Removes the qualification attribute definition resource with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the qualification attribute definition resource
     * @return the qualification attribute definition resource that was removed
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a qualification attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource remove(
        Serializable primaryKey)
        throws NoSuchQualificationAttributeDefinitionResourceException,
            SystemException {
        Session session = null;

        try {
            session = openSession();

            QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource =
                (QualificationAttributeDefinitionResource) session.get(QualificationAttributeDefinitionResourceImpl.class,
                    primaryKey);

            if (qualificationAttributeDefinitionResource == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchQualificationAttributeDefinitionResourceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(qualificationAttributeDefinitionResource);
        } catch (NoSuchQualificationAttributeDefinitionResourceException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected QualificationAttributeDefinitionResource removeImpl(
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource)
        throws SystemException {
        qualificationAttributeDefinitionResource = toUnwrappedModel(qualificationAttributeDefinitionResource);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(qualificationAttributeDefinitionResource)) {
                qualificationAttributeDefinitionResource = (QualificationAttributeDefinitionResource) session.get(QualificationAttributeDefinitionResourceImpl.class,
                        qualificationAttributeDefinitionResource.getPrimaryKeyObj());
            }

            if (qualificationAttributeDefinitionResource != null) {
                session.delete(qualificationAttributeDefinitionResource);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (qualificationAttributeDefinitionResource != null) {
            clearCache(qualificationAttributeDefinitionResource);
        }

        return qualificationAttributeDefinitionResource;
    }

    @Override
    public QualificationAttributeDefinitionResource updateImpl(
        de.humance.eco.profile.model.QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource)
        throws SystemException {
        qualificationAttributeDefinitionResource = toUnwrappedModel(qualificationAttributeDefinitionResource);

        boolean isNew = qualificationAttributeDefinitionResource.isNew();

        QualificationAttributeDefinitionResourceModelImpl qualificationAttributeDefinitionResourceModelImpl =
            (QualificationAttributeDefinitionResourceModelImpl) qualificationAttributeDefinitionResource;

        Session session = null;

        try {
            session = openSession();

            if (qualificationAttributeDefinitionResource.isNew()) {
                session.save(qualificationAttributeDefinitionResource);

                qualificationAttributeDefinitionResource.setNew(false);
            } else {
                session.merge(qualificationAttributeDefinitionResource);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew ||
                !QualificationAttributeDefinitionResourceModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((qualificationAttributeDefinitionResourceModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        qualificationAttributeDefinitionResourceModelImpl.getOriginalQualificationAttributeDefinitionId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBDEFID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFID,
                    args);

                args = new Object[] {
                        qualificationAttributeDefinitionResourceModelImpl.getQualificationAttributeDefinitionId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBDEFID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFID,
                    args);
            }

            if ((qualificationAttributeDefinitionResourceModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDCOUNTRY.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        qualificationAttributeDefinitionResourceModelImpl.getOriginalQualificationAttributeDefinitionId(),
                        qualificationAttributeDefinitionResourceModelImpl.getOriginalCountry()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBDEFIDANDCOUNTRY,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDCOUNTRY,
                    args);

                args = new Object[] {
                        qualificationAttributeDefinitionResourceModelImpl.getQualificationAttributeDefinitionId(),
                        qualificationAttributeDefinitionResourceModelImpl.getCountry()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBDEFIDANDCOUNTRY,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDCOUNTRY,
                    args);
            }

            if ((qualificationAttributeDefinitionResourceModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDLANGUAGE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        qualificationAttributeDefinitionResourceModelImpl.getOriginalQualificationAttributeDefinitionId(),
                        qualificationAttributeDefinitionResourceModelImpl.getOriginalLanguage()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBDEFIDANDLANGUAGE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDLANGUAGE,
                    args);

                args = new Object[] {
                        qualificationAttributeDefinitionResourceModelImpl.getQualificationAttributeDefinitionId(),
                        qualificationAttributeDefinitionResourceModelImpl.getLanguage()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBDEFIDANDLANGUAGE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDLANGUAGE,
                    args);
            }

            if ((qualificationAttributeDefinitionResourceModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDLOCALE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        qualificationAttributeDefinitionResourceModelImpl.getOriginalQualificationAttributeDefinitionId(),
                        qualificationAttributeDefinitionResourceModelImpl.getOriginalCountry(),
                        qualificationAttributeDefinitionResourceModelImpl.getOriginalLanguage()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBDEFIDANDLOCALE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDLOCALE,
                    args);

                args = new Object[] {
                        qualificationAttributeDefinitionResourceModelImpl.getQualificationAttributeDefinitionId(),
                        qualificationAttributeDefinitionResourceModelImpl.getCountry(),
                        qualificationAttributeDefinitionResourceModelImpl.getLanguage()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBDEFIDANDLOCALE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBDEFIDANDLOCALE,
                    args);
            }
        }

        EntityCacheUtil.putResult(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeDefinitionResourceImpl.class,
            qualificationAttributeDefinitionResource.getPrimaryKey(),
            qualificationAttributeDefinitionResource);

        return qualificationAttributeDefinitionResource;
    }

    protected QualificationAttributeDefinitionResource toUnwrappedModel(
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource) {
        if (qualificationAttributeDefinitionResource instanceof QualificationAttributeDefinitionResourceImpl) {
            return qualificationAttributeDefinitionResource;
        }

        QualificationAttributeDefinitionResourceImpl qualificationAttributeDefinitionResourceImpl =
            new QualificationAttributeDefinitionResourceImpl();

        qualificationAttributeDefinitionResourceImpl.setNew(qualificationAttributeDefinitionResource.isNew());
        qualificationAttributeDefinitionResourceImpl.setPrimaryKey(qualificationAttributeDefinitionResource.getPrimaryKey());

        qualificationAttributeDefinitionResourceImpl.setQualificationAttributeDefinitionResourceId(qualificationAttributeDefinitionResource.getQualificationAttributeDefinitionResourceId());
        qualificationAttributeDefinitionResourceImpl.setQualificationAttributeDefinitionId(qualificationAttributeDefinitionResource.getQualificationAttributeDefinitionId());
        qualificationAttributeDefinitionResourceImpl.setCountry(qualificationAttributeDefinitionResource.getCountry());
        qualificationAttributeDefinitionResourceImpl.setLanguage(qualificationAttributeDefinitionResource.getLanguage());
        qualificationAttributeDefinitionResourceImpl.setTitle(qualificationAttributeDefinitionResource.getTitle());
        qualificationAttributeDefinitionResourceImpl.setUnit(qualificationAttributeDefinitionResource.getUnit());

        return qualificationAttributeDefinitionResourceImpl;
    }

    /**
     * Returns the qualification attribute definition resource with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the qualification attribute definition resource
     * @return the qualification attribute definition resource
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a qualification attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource findByPrimaryKey(
        Serializable primaryKey)
        throws NoSuchQualificationAttributeDefinitionResourceException,
            SystemException {
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource =
            fetchByPrimaryKey(primaryKey);

        if (qualificationAttributeDefinitionResource == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchQualificationAttributeDefinitionResourceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return qualificationAttributeDefinitionResource;
    }

    /**
     * Returns the qualification attribute definition resource with the primary key or throws a {@link de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException} if it could not be found.
     *
     * @param qualificationAttributeDefinitionResourceId the primary key of the qualification attribute definition resource
     * @return the qualification attribute definition resource
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a qualification attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource findByPrimaryKey(
        long qualificationAttributeDefinitionResourceId)
        throws NoSuchQualificationAttributeDefinitionResourceException,
            SystemException {
        return findByPrimaryKey((Serializable) qualificationAttributeDefinitionResourceId);
    }

    /**
     * Returns the qualification attribute definition resource with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the qualification attribute definition resource
     * @return the qualification attribute definition resource, or <code>null</code> if a qualification attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource fetchByPrimaryKey(
        Serializable primaryKey) throws SystemException {
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource =
            (QualificationAttributeDefinitionResource) EntityCacheUtil.getResult(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
                QualificationAttributeDefinitionResourceImpl.class, primaryKey);

        if (qualificationAttributeDefinitionResource == _nullQualificationAttributeDefinitionResource) {
            return null;
        }

        if (qualificationAttributeDefinitionResource == null) {
            Session session = null;

            try {
                session = openSession();

                qualificationAttributeDefinitionResource = (QualificationAttributeDefinitionResource) session.get(QualificationAttributeDefinitionResourceImpl.class,
                        primaryKey);

                if (qualificationAttributeDefinitionResource != null) {
                    cacheResult(qualificationAttributeDefinitionResource);
                } else {
                    EntityCacheUtil.putResult(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
                        QualificationAttributeDefinitionResourceImpl.class,
                        primaryKey,
                        _nullQualificationAttributeDefinitionResource);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(QualificationAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
                    QualificationAttributeDefinitionResourceImpl.class,
                    primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return qualificationAttributeDefinitionResource;
    }

    /**
     * Returns the qualification attribute definition resource with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param qualificationAttributeDefinitionResourceId the primary key of the qualification attribute definition resource
     * @return the qualification attribute definition resource, or <code>null</code> if a qualification attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttributeDefinitionResource fetchByPrimaryKey(
        long qualificationAttributeDefinitionResourceId)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) qualificationAttributeDefinitionResourceId);
    }

    /**
     * Returns all the qualification attribute definition resources.
     *
     * @return the qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinitionResource> findAll()
        throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification attribute definition resources.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of qualification attribute definition resources
     * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
     * @return the range of qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinitionResource> findAll(int start,
        int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification attribute definition resources.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of qualification attribute definition resources
     * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttributeDefinitionResource> findAll(int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<QualificationAttributeDefinitionResource> list = (List<QualificationAttributeDefinitionResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE;

                if (pagination) {
                    sql = sql.concat(QualificationAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<QualificationAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationAttributeDefinitionResource>(list);
                } else {
                    list = (List<QualificationAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the qualification attribute definition resources from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource : findAll()) {
            remove(qualificationAttributeDefinitionResource);
        }
    }

    /**
     * Returns the number of qualification attribute definition resources.
     *
     * @return the number of qualification attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_QUALIFICATIONATTRIBUTEDEFINITIONRESOURCE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the qualification attribute definition resource persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.de.humance.eco.profile.model.QualificationAttributeDefinitionResource")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<QualificationAttributeDefinitionResource>> listenersList =
                    new ArrayList<ModelListener<QualificationAttributeDefinitionResource>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<QualificationAttributeDefinitionResource>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(QualificationAttributeDefinitionResourceImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
