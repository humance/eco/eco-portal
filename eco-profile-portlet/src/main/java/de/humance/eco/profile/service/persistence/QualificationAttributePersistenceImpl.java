package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.humance.eco.profile.NoSuchQualificationAttributeException;
import de.humance.eco.profile.model.QualificationAttribute;
import de.humance.eco.profile.model.impl.QualificationAttributeImpl;
import de.humance.eco.profile.model.impl.QualificationAttributeModelImpl;
import de.humance.eco.profile.service.persistence.QualificationAttributePersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the qualification attribute service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see QualificationAttributePersistence
 * @see QualificationAttributeUtil
 * @generated
 */
public class QualificationAttributePersistenceImpl extends BasePersistenceImpl<QualificationAttribute>
    implements QualificationAttributePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link QualificationAttributeUtil} to access the qualification attribute persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = QualificationAttributeImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONID =
        new FinderPath(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByQualificationId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONID =
        new FinderPath(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByQualificationId",
            new String[] { Long.class.getName() },
            QualificationAttributeModelImpl.QUALIFICATIONID_COLUMN_BITMASK |
            QualificationAttributeModelImpl.SEQUENCENUMBER_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_QUALIFICATIONID = new FinderPath(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByQualificationId", new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_QUALIFICATIONID_QUALIFICATIONID_2 =
        "qualificationAttribute.qualificationId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID =
        new FinderPath(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByQualificationIdAndAttributeDefinitionId",
            new String[] {
                Long.class.getName(), Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID =
        new FinderPath(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByQualificationIdAndAttributeDefinitionId",
            new String[] { Long.class.getName(), Long.class.getName() },
            QualificationAttributeModelImpl.QUALIFICATIONID_COLUMN_BITMASK |
            QualificationAttributeModelImpl.QUALIFICATIONATTRIBUTEDEFINITIONID_COLUMN_BITMASK |
            QualificationAttributeModelImpl.SEQUENCENUMBER_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID =
        new FinderPath(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByQualificationIdAndAttributeDefinitionId",
            new String[] { Long.class.getName(), Long.class.getName() });
    private static final String _FINDER_COLUMN_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID_QUALIFICATIONID_2 =
        "qualificationAttribute.qualificationId = ? AND ";
    private static final String _FINDER_COLUMN_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID_QUALIFICATIONATTRIBUTEDEFINITIONID_2 =
        "qualificationAttribute.qualificationAttributeDefinitionId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONATTRIBUTEDEFINITIONID =
        new FinderPath(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByQualificationAttributeDefinitionId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBUTEDEFINITIONID =
        new FinderPath(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeModelImpl.FINDER_CACHE_ENABLED,
            QualificationAttributeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByQualificationAttributeDefinitionId",
            new String[] { Long.class.getName() },
            QualificationAttributeModelImpl.QUALIFICATIONATTRIBUTEDEFINITIONID_COLUMN_BITMASK |
            QualificationAttributeModelImpl.QUALIFICATIONID_COLUMN_BITMASK |
            QualificationAttributeModelImpl.SEQUENCENUMBER_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBUTEDEFINITIONID =
        new FinderPath(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByQualificationAttributeDefinitionId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_QUALIFICATIONATTRIBUTEDEFINITIONID_QUALIFICATIONATTRIBUTEDEFINITIONID_2 =
        "qualificationAttribute.qualificationAttributeDefinitionId = ?";
    private static final String _SQL_SELECT_QUALIFICATIONATTRIBUTE = "SELECT qualificationAttribute FROM QualificationAttribute qualificationAttribute";
    private static final String _SQL_SELECT_QUALIFICATIONATTRIBUTE_WHERE = "SELECT qualificationAttribute FROM QualificationAttribute qualificationAttribute WHERE ";
    private static final String _SQL_COUNT_QUALIFICATIONATTRIBUTE = "SELECT COUNT(qualificationAttribute) FROM QualificationAttribute qualificationAttribute";
    private static final String _SQL_COUNT_QUALIFICATIONATTRIBUTE_WHERE = "SELECT COUNT(qualificationAttribute) FROM QualificationAttribute qualificationAttribute WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "qualificationAttribute.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No QualificationAttribute exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No QualificationAttribute exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(QualificationAttributePersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "type"
            });
    private static QualificationAttribute _nullQualificationAttribute = new QualificationAttributeImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<QualificationAttribute> toCacheModel() {
                return _nullQualificationAttributeCacheModel;
            }
        };

    private static CacheModel<QualificationAttribute> _nullQualificationAttributeCacheModel =
        new CacheModel<QualificationAttribute>() {
            @Override
            public QualificationAttribute toEntityModel() {
                return _nullQualificationAttribute;
            }
        };

    public QualificationAttributePersistenceImpl() {
        setModelClass(QualificationAttribute.class);
    }

    /**
     * Returns all the qualification attributes where qualificationId = &#63;.
     *
     * @param qualificationId the qualification ID
     * @return the matching qualification attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttribute> findByQualificationId(
        long qualificationId) throws SystemException {
        return findByQualificationId(qualificationId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification attributes where qualificationId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationId the qualification ID
     * @param start the lower bound of the range of qualification attributes
     * @param end the upper bound of the range of qualification attributes (not inclusive)
     * @return the range of matching qualification attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttribute> findByQualificationId(
        long qualificationId, int start, int end) throws SystemException {
        return findByQualificationId(qualificationId, start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification attributes where qualificationId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationId the qualification ID
     * @param start the lower bound of the range of qualification attributes
     * @param end the upper bound of the range of qualification attributes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualification attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttribute> findByQualificationId(
        long qualificationId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONID;
            finderArgs = new Object[] { qualificationId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONID;
            finderArgs = new Object[] {
                    qualificationId,
                    
                    start, end, orderByComparator
                };
        }

        List<QualificationAttribute> list = (List<QualificationAttribute>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (QualificationAttribute qualificationAttribute : list) {
                if ((qualificationId != qualificationAttribute.getQualificationId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONID_QUALIFICATIONID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationAttributeModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationId);

                if (!pagination) {
                    list = (List<QualificationAttribute>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationAttribute>(list);
                } else {
                    list = (List<QualificationAttribute>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification attribute in the ordered set where qualificationId = &#63;.
     *
     * @param qualificationId the qualification ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute findByQualificationId_First(
        long qualificationId, OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeException, SystemException {
        QualificationAttribute qualificationAttribute = fetchByQualificationId_First(qualificationId,
                orderByComparator);

        if (qualificationAttribute != null) {
            return qualificationAttribute;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationId=");
        msg.append(qualificationId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeException(msg.toString());
    }

    /**
     * Returns the first qualification attribute in the ordered set where qualificationId = &#63;.
     *
     * @param qualificationId the qualification ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute fetchByQualificationId_First(
        long qualificationId, OrderByComparator orderByComparator)
        throws SystemException {
        List<QualificationAttribute> list = findByQualificationId(qualificationId,
                0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification attribute in the ordered set where qualificationId = &#63;.
     *
     * @param qualificationId the qualification ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute findByQualificationId_Last(
        long qualificationId, OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeException, SystemException {
        QualificationAttribute qualificationAttribute = fetchByQualificationId_Last(qualificationId,
                orderByComparator);

        if (qualificationAttribute != null) {
            return qualificationAttribute;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationId=");
        msg.append(qualificationId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeException(msg.toString());
    }

    /**
     * Returns the last qualification attribute in the ordered set where qualificationId = &#63;.
     *
     * @param qualificationId the qualification ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute fetchByQualificationId_Last(
        long qualificationId, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByQualificationId(qualificationId);

        if (count == 0) {
            return null;
        }

        List<QualificationAttribute> list = findByQualificationId(qualificationId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualification attributes before and after the current qualification attribute in the ordered set where qualificationId = &#63;.
     *
     * @param qualificationAttributeId the primary key of the current qualification attribute
     * @param qualificationId the qualification ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification attribute
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a qualification attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute[] findByQualificationId_PrevAndNext(
        long qualificationAttributeId, long qualificationId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeException, SystemException {
        QualificationAttribute qualificationAttribute = findByPrimaryKey(qualificationAttributeId);

        Session session = null;

        try {
            session = openSession();

            QualificationAttribute[] array = new QualificationAttributeImpl[3];

            array[0] = getByQualificationId_PrevAndNext(session,
                    qualificationAttribute, qualificationId, orderByComparator,
                    true);

            array[1] = qualificationAttribute;

            array[2] = getByQualificationId_PrevAndNext(session,
                    qualificationAttribute, qualificationId, orderByComparator,
                    false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected QualificationAttribute getByQualificationId_PrevAndNext(
        Session session, QualificationAttribute qualificationAttribute,
        long qualificationId, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTE_WHERE);

        query.append(_FINDER_COLUMN_QUALIFICATIONID_QUALIFICATIONID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationAttributeModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(qualificationId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualificationAttribute);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<QualificationAttribute> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualification attributes where qualificationId = &#63; from the database.
     *
     * @param qualificationId the qualification ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByQualificationId(long qualificationId)
        throws SystemException {
        for (QualificationAttribute qualificationAttribute : findByQualificationId(
                qualificationId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(qualificationAttribute);
        }
    }

    /**
     * Returns the number of qualification attributes where qualificationId = &#63;.
     *
     * @param qualificationId the qualification ID
     * @return the number of matching qualification attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByQualificationId(long qualificationId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_QUALIFICATIONID;

        Object[] finderArgs = new Object[] { qualificationId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_QUALIFICATIONATTRIBUTE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONID_QUALIFICATIONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the qualification attributes where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationId the qualification ID
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @return the matching qualification attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttribute> findByQualificationIdAndAttributeDefinitionId(
        long qualificationId, long qualificationAttributeDefinitionId)
        throws SystemException {
        return findByQualificationIdAndAttributeDefinitionId(qualificationId,
            qualificationAttributeDefinitionId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification attributes where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationId the qualification ID
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param start the lower bound of the range of qualification attributes
     * @param end the upper bound of the range of qualification attributes (not inclusive)
     * @return the range of matching qualification attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttribute> findByQualificationIdAndAttributeDefinitionId(
        long qualificationId, long qualificationAttributeDefinitionId,
        int start, int end) throws SystemException {
        return findByQualificationIdAndAttributeDefinitionId(qualificationId,
            qualificationAttributeDefinitionId, start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification attributes where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationId the qualification ID
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param start the lower bound of the range of qualification attributes
     * @param end the upper bound of the range of qualification attributes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualification attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttribute> findByQualificationIdAndAttributeDefinitionId(
        long qualificationId, long qualificationAttributeDefinitionId,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID;
            finderArgs = new Object[] {
                    qualificationId, qualificationAttributeDefinitionId
                };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID;
            finderArgs = new Object[] {
                    qualificationId, qualificationAttributeDefinitionId,
                    
                    start, end, orderByComparator
                };
        }

        List<QualificationAttribute> list = (List<QualificationAttribute>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (QualificationAttribute qualificationAttribute : list) {
                if ((qualificationId != qualificationAttribute.getQualificationId()) ||
                        (qualificationAttributeDefinitionId != qualificationAttribute.getQualificationAttributeDefinitionId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID_QUALIFICATIONID_2);

            query.append(_FINDER_COLUMN_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationAttributeModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationId);

                qPos.add(qualificationAttributeDefinitionId);

                if (!pagination) {
                    list = (List<QualificationAttribute>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationAttribute>(list);
                } else {
                    list = (List<QualificationAttribute>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification attribute in the ordered set where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationId the qualification ID
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute findByQualificationIdAndAttributeDefinitionId_First(
        long qualificationId, long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeException, SystemException {
        QualificationAttribute qualificationAttribute = fetchByQualificationIdAndAttributeDefinitionId_First(qualificationId,
                qualificationAttributeDefinitionId, orderByComparator);

        if (qualificationAttribute != null) {
            return qualificationAttribute;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationId=");
        msg.append(qualificationId);

        msg.append(", qualificationAttributeDefinitionId=");
        msg.append(qualificationAttributeDefinitionId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeException(msg.toString());
    }

    /**
     * Returns the first qualification attribute in the ordered set where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationId the qualification ID
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute fetchByQualificationIdAndAttributeDefinitionId_First(
        long qualificationId, long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator) throws SystemException {
        List<QualificationAttribute> list = findByQualificationIdAndAttributeDefinitionId(qualificationId,
                qualificationAttributeDefinitionId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification attribute in the ordered set where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationId the qualification ID
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute findByQualificationIdAndAttributeDefinitionId_Last(
        long qualificationId, long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeException, SystemException {
        QualificationAttribute qualificationAttribute = fetchByQualificationIdAndAttributeDefinitionId_Last(qualificationId,
                qualificationAttributeDefinitionId, orderByComparator);

        if (qualificationAttribute != null) {
            return qualificationAttribute;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationId=");
        msg.append(qualificationId);

        msg.append(", qualificationAttributeDefinitionId=");
        msg.append(qualificationAttributeDefinitionId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeException(msg.toString());
    }

    /**
     * Returns the last qualification attribute in the ordered set where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationId the qualification ID
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute fetchByQualificationIdAndAttributeDefinitionId_Last(
        long qualificationId, long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByQualificationIdAndAttributeDefinitionId(qualificationId,
                qualificationAttributeDefinitionId);

        if (count == 0) {
            return null;
        }

        List<QualificationAttribute> list = findByQualificationIdAndAttributeDefinitionId(qualificationId,
                qualificationAttributeDefinitionId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualification attributes before and after the current qualification attribute in the ordered set where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationAttributeId the primary key of the current qualification attribute
     * @param qualificationId the qualification ID
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification attribute
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a qualification attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute[] findByQualificationIdAndAttributeDefinitionId_PrevAndNext(
        long qualificationAttributeId, long qualificationId,
        long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeException, SystemException {
        QualificationAttribute qualificationAttribute = findByPrimaryKey(qualificationAttributeId);

        Session session = null;

        try {
            session = openSession();

            QualificationAttribute[] array = new QualificationAttributeImpl[3];

            array[0] = getByQualificationIdAndAttributeDefinitionId_PrevAndNext(session,
                    qualificationAttribute, qualificationId,
                    qualificationAttributeDefinitionId, orderByComparator, true);

            array[1] = qualificationAttribute;

            array[2] = getByQualificationIdAndAttributeDefinitionId_PrevAndNext(session,
                    qualificationAttribute, qualificationId,
                    qualificationAttributeDefinitionId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected QualificationAttribute getByQualificationIdAndAttributeDefinitionId_PrevAndNext(
        Session session, QualificationAttribute qualificationAttribute,
        long qualificationId, long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTE_WHERE);

        query.append(_FINDER_COLUMN_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID_QUALIFICATIONID_2);

        query.append(_FINDER_COLUMN_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationAttributeModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(qualificationId);

        qPos.add(qualificationAttributeDefinitionId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualificationAttribute);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<QualificationAttribute> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualification attributes where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63; from the database.
     *
     * @param qualificationId the qualification ID
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByQualificationIdAndAttributeDefinitionId(
        long qualificationId, long qualificationAttributeDefinitionId)
        throws SystemException {
        for (QualificationAttribute qualificationAttribute : findByQualificationIdAndAttributeDefinitionId(
                qualificationId, qualificationAttributeDefinitionId,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(qualificationAttribute);
        }
    }

    /**
     * Returns the number of qualification attributes where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationId the qualification ID
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @return the number of matching qualification attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByQualificationIdAndAttributeDefinitionId(
        long qualificationId, long qualificationAttributeDefinitionId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID;

        Object[] finderArgs = new Object[] {
                qualificationId, qualificationAttributeDefinitionId
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_QUALIFICATIONATTRIBUTE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID_QUALIFICATIONID_2);

            query.append(_FINDER_COLUMN_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationId);

                qPos.add(qualificationAttributeDefinitionId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the qualification attributes where qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @return the matching qualification attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttribute> findByQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId) throws SystemException {
        return findByQualificationAttributeDefinitionId(qualificationAttributeDefinitionId,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification attributes where qualificationAttributeDefinitionId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param start the lower bound of the range of qualification attributes
     * @param end the upper bound of the range of qualification attributes (not inclusive)
     * @return the range of matching qualification attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttribute> findByQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId, int start, int end)
        throws SystemException {
        return findByQualificationAttributeDefinitionId(qualificationAttributeDefinitionId,
            start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification attributes where qualificationAttributeDefinitionId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param start the lower bound of the range of qualification attributes
     * @param end the upper bound of the range of qualification attributes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualification attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttribute> findByQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBUTEDEFINITIONID;
            finderArgs = new Object[] { qualificationAttributeDefinitionId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONATTRIBUTEDEFINITIONID;
            finderArgs = new Object[] {
                    qualificationAttributeDefinitionId,
                    
                    start, end, orderByComparator
                };
        }

        List<QualificationAttribute> list = (List<QualificationAttribute>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (QualificationAttribute qualificationAttribute : list) {
                if ((qualificationAttributeDefinitionId != qualificationAttribute.getQualificationAttributeDefinitionId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBUTEDEFINITIONID_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationAttributeModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationAttributeDefinitionId);

                if (!pagination) {
                    list = (List<QualificationAttribute>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationAttribute>(list);
                } else {
                    list = (List<QualificationAttribute>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification attribute in the ordered set where qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute findByQualificationAttributeDefinitionId_First(
        long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeException, SystemException {
        QualificationAttribute qualificationAttribute = fetchByQualificationAttributeDefinitionId_First(qualificationAttributeDefinitionId,
                orderByComparator);

        if (qualificationAttribute != null) {
            return qualificationAttribute;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationAttributeDefinitionId=");
        msg.append(qualificationAttributeDefinitionId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeException(msg.toString());
    }

    /**
     * Returns the first qualification attribute in the ordered set where qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute fetchByQualificationAttributeDefinitionId_First(
        long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator) throws SystemException {
        List<QualificationAttribute> list = findByQualificationAttributeDefinitionId(qualificationAttributeDefinitionId,
                0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification attribute in the ordered set where qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute findByQualificationAttributeDefinitionId_Last(
        long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeException, SystemException {
        QualificationAttribute qualificationAttribute = fetchByQualificationAttributeDefinitionId_Last(qualificationAttributeDefinitionId,
                orderByComparator);

        if (qualificationAttribute != null) {
            return qualificationAttribute;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationAttributeDefinitionId=");
        msg.append(qualificationAttributeDefinitionId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationAttributeException(msg.toString());
    }

    /**
     * Returns the last qualification attribute in the ordered set where qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute fetchByQualificationAttributeDefinitionId_Last(
        long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);

        if (count == 0) {
            return null;
        }

        List<QualificationAttribute> list = findByQualificationAttributeDefinitionId(qualificationAttributeDefinitionId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualification attributes before and after the current qualification attribute in the ordered set where qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationAttributeId the primary key of the current qualification attribute
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification attribute
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a qualification attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute[] findByQualificationAttributeDefinitionId_PrevAndNext(
        long qualificationAttributeId, long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationAttributeException, SystemException {
        QualificationAttribute qualificationAttribute = findByPrimaryKey(qualificationAttributeId);

        Session session = null;

        try {
            session = openSession();

            QualificationAttribute[] array = new QualificationAttributeImpl[3];

            array[0] = getByQualificationAttributeDefinitionId_PrevAndNext(session,
                    qualificationAttribute, qualificationAttributeDefinitionId,
                    orderByComparator, true);

            array[1] = qualificationAttribute;

            array[2] = getByQualificationAttributeDefinitionId_PrevAndNext(session,
                    qualificationAttribute, qualificationAttributeDefinitionId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected QualificationAttribute getByQualificationAttributeDefinitionId_PrevAndNext(
        Session session, QualificationAttribute qualificationAttribute,
        long qualificationAttributeDefinitionId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTE_WHERE);

        query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBUTEDEFINITIONID_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationAttributeModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(qualificationAttributeDefinitionId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualificationAttribute);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<QualificationAttribute> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualification attributes where qualificationAttributeDefinitionId = &#63; from the database.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId) throws SystemException {
        for (QualificationAttribute qualificationAttribute : findByQualificationAttributeDefinitionId(
                qualificationAttributeDefinitionId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(qualificationAttribute);
        }
    }

    /**
     * Returns the number of qualification attributes where qualificationAttributeDefinitionId = &#63;.
     *
     * @param qualificationAttributeDefinitionId the qualification attribute definition ID
     * @return the number of matching qualification attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBUTEDEFINITIONID;

        Object[] finderArgs = new Object[] { qualificationAttributeDefinitionId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_QUALIFICATIONATTRIBUTE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONATTRIBUTEDEFINITIONID_QUALIFICATIONATTRIBUTEDEFINITIONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationAttributeDefinitionId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the qualification attribute in the entity cache if it is enabled.
     *
     * @param qualificationAttribute the qualification attribute
     */
    @Override
    public void cacheResult(QualificationAttribute qualificationAttribute) {
        EntityCacheUtil.putResult(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeImpl.class,
            qualificationAttribute.getPrimaryKey(), qualificationAttribute);

        qualificationAttribute.resetOriginalValues();
    }

    /**
     * Caches the qualification attributes in the entity cache if it is enabled.
     *
     * @param qualificationAttributes the qualification attributes
     */
    @Override
    public void cacheResult(
        List<QualificationAttribute> qualificationAttributes) {
        for (QualificationAttribute qualificationAttribute : qualificationAttributes) {
            if (EntityCacheUtil.getResult(
                        QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
                        QualificationAttributeImpl.class,
                        qualificationAttribute.getPrimaryKey()) == null) {
                cacheResult(qualificationAttribute);
            } else {
                qualificationAttribute.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all qualification attributes.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(QualificationAttributeImpl.class.getName());
        }

        EntityCacheUtil.clearCache(QualificationAttributeImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the qualification attribute.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(QualificationAttribute qualificationAttribute) {
        EntityCacheUtil.removeResult(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeImpl.class,
            qualificationAttribute.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<QualificationAttribute> qualificationAttributes) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (QualificationAttribute qualificationAttribute : qualificationAttributes) {
            EntityCacheUtil.removeResult(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
                QualificationAttributeImpl.class,
                qualificationAttribute.getPrimaryKey());
        }
    }

    /**
     * Creates a new qualification attribute with the primary key. Does not add the qualification attribute to the database.
     *
     * @param qualificationAttributeId the primary key for the new qualification attribute
     * @return the new qualification attribute
     */
    @Override
    public QualificationAttribute create(long qualificationAttributeId) {
        QualificationAttribute qualificationAttribute = new QualificationAttributeImpl();

        qualificationAttribute.setNew(true);
        qualificationAttribute.setPrimaryKey(qualificationAttributeId);

        return qualificationAttribute;
    }

    /**
     * Removes the qualification attribute with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param qualificationAttributeId the primary key of the qualification attribute
     * @return the qualification attribute that was removed
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a qualification attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute remove(long qualificationAttributeId)
        throws NoSuchQualificationAttributeException, SystemException {
        return remove((Serializable) qualificationAttributeId);
    }

    /**
     * Removes the qualification attribute with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the qualification attribute
     * @return the qualification attribute that was removed
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a qualification attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute remove(Serializable primaryKey)
        throws NoSuchQualificationAttributeException, SystemException {
        Session session = null;

        try {
            session = openSession();

            QualificationAttribute qualificationAttribute = (QualificationAttribute) session.get(QualificationAttributeImpl.class,
                    primaryKey);

            if (qualificationAttribute == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchQualificationAttributeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(qualificationAttribute);
        } catch (NoSuchQualificationAttributeException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected QualificationAttribute removeImpl(
        QualificationAttribute qualificationAttribute)
        throws SystemException {
        qualificationAttribute = toUnwrappedModel(qualificationAttribute);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(qualificationAttribute)) {
                qualificationAttribute = (QualificationAttribute) session.get(QualificationAttributeImpl.class,
                        qualificationAttribute.getPrimaryKeyObj());
            }

            if (qualificationAttribute != null) {
                session.delete(qualificationAttribute);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (qualificationAttribute != null) {
            clearCache(qualificationAttribute);
        }

        return qualificationAttribute;
    }

    @Override
    public QualificationAttribute updateImpl(
        de.humance.eco.profile.model.QualificationAttribute qualificationAttribute)
        throws SystemException {
        qualificationAttribute = toUnwrappedModel(qualificationAttribute);

        boolean isNew = qualificationAttribute.isNew();

        QualificationAttributeModelImpl qualificationAttributeModelImpl = (QualificationAttributeModelImpl) qualificationAttribute;

        Session session = null;

        try {
            session = openSession();

            if (qualificationAttribute.isNew()) {
                session.save(qualificationAttribute);

                qualificationAttribute.setNew(false);
            } else {
                session.merge(qualificationAttribute);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !QualificationAttributeModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((qualificationAttributeModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        qualificationAttributeModelImpl.getOriginalQualificationId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONID,
                    args);

                args = new Object[] {
                        qualificationAttributeModelImpl.getQualificationId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONID,
                    args);
            }

            if ((qualificationAttributeModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        qualificationAttributeModelImpl.getOriginalQualificationId(),
                        qualificationAttributeModelImpl.getOriginalQualificationAttributeDefinitionId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID,
                    args);

                args = new Object[] {
                        qualificationAttributeModelImpl.getQualificationId(),
                        qualificationAttributeModelImpl.getQualificationAttributeDefinitionId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONIDANDATTRIBUTEDEFINITIONID,
                    args);
            }

            if ((qualificationAttributeModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBUTEDEFINITIONID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        qualificationAttributeModelImpl.getOriginalQualificationAttributeDefinitionId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBUTEDEFINITIONID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBUTEDEFINITIONID,
                    args);

                args = new Object[] {
                        qualificationAttributeModelImpl.getQualificationAttributeDefinitionId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONATTRIBUTEDEFINITIONID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONATTRIBUTEDEFINITIONID,
                    args);
            }
        }

        EntityCacheUtil.putResult(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationAttributeImpl.class,
            qualificationAttribute.getPrimaryKey(), qualificationAttribute);

        return qualificationAttribute;
    }

    protected QualificationAttribute toUnwrappedModel(
        QualificationAttribute qualificationAttribute) {
        if (qualificationAttribute instanceof QualificationAttributeImpl) {
            return qualificationAttribute;
        }

        QualificationAttributeImpl qualificationAttributeImpl = new QualificationAttributeImpl();

        qualificationAttributeImpl.setNew(qualificationAttribute.isNew());
        qualificationAttributeImpl.setPrimaryKey(qualificationAttribute.getPrimaryKey());

        qualificationAttributeImpl.setQualificationAttributeId(qualificationAttribute.getQualificationAttributeId());
        qualificationAttributeImpl.setQualificationId(qualificationAttribute.getQualificationId());
        qualificationAttributeImpl.setQualificationAttributeDefinitionId(qualificationAttribute.getQualificationAttributeDefinitionId());
        qualificationAttributeImpl.setType(qualificationAttribute.getType());
        qualificationAttributeImpl.setSequenceNumber(qualificationAttribute.getSequenceNumber());
        qualificationAttributeImpl.setAttributeValue(qualificationAttribute.getAttributeValue());

        return qualificationAttributeImpl;
    }

    /**
     * Returns the qualification attribute with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the qualification attribute
     * @return the qualification attribute
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a qualification attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute findByPrimaryKey(Serializable primaryKey)
        throws NoSuchQualificationAttributeException, SystemException {
        QualificationAttribute qualificationAttribute = fetchByPrimaryKey(primaryKey);

        if (qualificationAttribute == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchQualificationAttributeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return qualificationAttribute;
    }

    /**
     * Returns the qualification attribute with the primary key or throws a {@link de.humance.eco.profile.NoSuchQualificationAttributeException} if it could not be found.
     *
     * @param qualificationAttributeId the primary key of the qualification attribute
     * @return the qualification attribute
     * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a qualification attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute findByPrimaryKey(
        long qualificationAttributeId)
        throws NoSuchQualificationAttributeException, SystemException {
        return findByPrimaryKey((Serializable) qualificationAttributeId);
    }

    /**
     * Returns the qualification attribute with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the qualification attribute
     * @return the qualification attribute, or <code>null</code> if a qualification attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        QualificationAttribute qualificationAttribute = (QualificationAttribute) EntityCacheUtil.getResult(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
                QualificationAttributeImpl.class, primaryKey);

        if (qualificationAttribute == _nullQualificationAttribute) {
            return null;
        }

        if (qualificationAttribute == null) {
            Session session = null;

            try {
                session = openSession();

                qualificationAttribute = (QualificationAttribute) session.get(QualificationAttributeImpl.class,
                        primaryKey);

                if (qualificationAttribute != null) {
                    cacheResult(qualificationAttribute);
                } else {
                    EntityCacheUtil.putResult(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
                        QualificationAttributeImpl.class, primaryKey,
                        _nullQualificationAttribute);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(QualificationAttributeModelImpl.ENTITY_CACHE_ENABLED,
                    QualificationAttributeImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return qualificationAttribute;
    }

    /**
     * Returns the qualification attribute with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param qualificationAttributeId the primary key of the qualification attribute
     * @return the qualification attribute, or <code>null</code> if a qualification attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationAttribute fetchByPrimaryKey(
        long qualificationAttributeId) throws SystemException {
        return fetchByPrimaryKey((Serializable) qualificationAttributeId);
    }

    /**
     * Returns all the qualification attributes.
     *
     * @return the qualification attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttribute> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification attributes.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of qualification attributes
     * @param end the upper bound of the range of qualification attributes (not inclusive)
     * @return the range of qualification attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttribute> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification attributes.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of qualification attributes
     * @param end the upper bound of the range of qualification attributes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of qualification attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationAttribute> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<QualificationAttribute> list = (List<QualificationAttribute>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_QUALIFICATIONATTRIBUTE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_QUALIFICATIONATTRIBUTE;

                if (pagination) {
                    sql = sql.concat(QualificationAttributeModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<QualificationAttribute>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationAttribute>(list);
                } else {
                    list = (List<QualificationAttribute>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the qualification attributes from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (QualificationAttribute qualificationAttribute : findAll()) {
            remove(qualificationAttribute);
        }
    }

    /**
     * Returns the number of qualification attributes.
     *
     * @return the number of qualification attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_QUALIFICATIONATTRIBUTE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the qualification attribute persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.de.humance.eco.profile.model.QualificationAttribute")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<QualificationAttribute>> listenersList = new ArrayList<ModelListener<QualificationAttribute>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<QualificationAttribute>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(QualificationAttributeImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
