package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.humance.eco.profile.NoSuchQualificationException;
import de.humance.eco.profile.model.Qualification;
import de.humance.eco.profile.model.impl.QualificationImpl;
import de.humance.eco.profile.model.impl.QualificationModelImpl;
import de.humance.eco.profile.service.persistence.QualificationPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the qualification service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see QualificationPersistence
 * @see QualificationUtil
 * @generated
 */
public class QualificationPersistenceImpl extends BasePersistenceImpl<Qualification>
    implements QualificationPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link QualificationUtil} to access the qualification persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = QualificationImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(QualificationModelImpl.ENTITY_CACHE_ENABLED,
            QualificationModelImpl.FINDER_CACHE_ENABLED,
            QualificationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(QualificationModelImpl.ENTITY_CACHE_ENABLED,
            QualificationModelImpl.FINDER_CACHE_ENABLED,
            QualificationImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(QualificationModelImpl.ENTITY_CACHE_ENABLED,
            QualificationModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_OWNERID = new FinderPath(QualificationModelImpl.ENTITY_CACHE_ENABLED,
            QualificationModelImpl.FINDER_CACHE_ENABLED,
            QualificationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByOwnerId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNERID =
        new FinderPath(QualificationModelImpl.ENTITY_CACHE_ENABLED,
            QualificationModelImpl.FINDER_CACHE_ENABLED,
            QualificationImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByOwnerId", new String[] { Long.class.getName() },
            QualificationModelImpl.OWNERID_COLUMN_BITMASK |
            QualificationModelImpl.TYPEID_COLUMN_BITMASK |
            QualificationModelImpl.DELIVERYDATE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_OWNERID = new FinderPath(QualificationModelImpl.ENTITY_CACHE_ENABLED,
            QualificationModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOwnerId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_OWNERID_OWNERID_2 = "qualification.ownerId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TYPEID = new FinderPath(QualificationModelImpl.ENTITY_CACHE_ENABLED,
            QualificationModelImpl.FINDER_CACHE_ENABLED,
            QualificationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByTypeId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPEID =
        new FinderPath(QualificationModelImpl.ENTITY_CACHE_ENABLED,
            QualificationModelImpl.FINDER_CACHE_ENABLED,
            QualificationImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByTypeId", new String[] { Long.class.getName() },
            QualificationModelImpl.TYPEID_COLUMN_BITMASK |
            QualificationModelImpl.OWNERID_COLUMN_BITMASK |
            QualificationModelImpl.DELIVERYDATE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_TYPEID = new FinderPath(QualificationModelImpl.ENTITY_CACHE_ENABLED,
            QualificationModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTypeId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_TYPEID_TYPEID_2 = "qualification.typeId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_OWNERIDANDTYPEID =
        new FinderPath(QualificationModelImpl.ENTITY_CACHE_ENABLED,
            QualificationModelImpl.FINDER_CACHE_ENABLED,
            QualificationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByOwnerIdAndTypeId",
            new String[] {
                Long.class.getName(), Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNERIDANDTYPEID =
        new FinderPath(QualificationModelImpl.ENTITY_CACHE_ENABLED,
            QualificationModelImpl.FINDER_CACHE_ENABLED,
            QualificationImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByOwnerIdAndTypeId",
            new String[] { Long.class.getName(), Long.class.getName() },
            QualificationModelImpl.OWNERID_COLUMN_BITMASK |
            QualificationModelImpl.TYPEID_COLUMN_BITMASK |
            QualificationModelImpl.DELIVERYDATE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_OWNERIDANDTYPEID = new FinderPath(QualificationModelImpl.ENTITY_CACHE_ENABLED,
            QualificationModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByOwnerIdAndTypeId",
            new String[] { Long.class.getName(), Long.class.getName() });
    private static final String _FINDER_COLUMN_OWNERIDANDTYPEID_OWNERID_2 = "qualification.ownerId = ? AND ";
    private static final String _FINDER_COLUMN_OWNERIDANDTYPEID_TYPEID_2 = "qualification.typeId = ?";
    private static final String _SQL_SELECT_QUALIFICATION = "SELECT qualification FROM Qualification qualification";
    private static final String _SQL_SELECT_QUALIFICATION_WHERE = "SELECT qualification FROM Qualification qualification WHERE ";
    private static final String _SQL_COUNT_QUALIFICATION = "SELECT COUNT(qualification) FROM Qualification qualification";
    private static final String _SQL_COUNT_QUALIFICATION_WHERE = "SELECT COUNT(qualification) FROM Qualification qualification WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "qualification.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Qualification exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Qualification exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(QualificationPersistenceImpl.class);
    private static Qualification _nullQualification = new QualificationImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Qualification> toCacheModel() {
                return _nullQualificationCacheModel;
            }
        };

    private static CacheModel<Qualification> _nullQualificationCacheModel = new CacheModel<Qualification>() {
            @Override
            public Qualification toEntityModel() {
                return _nullQualification;
            }
        };

    public QualificationPersistenceImpl() {
        setModelClass(Qualification.class);
    }

    /**
     * Returns all the qualifications where ownerId = &#63;.
     *
     * @param ownerId the owner ID
     * @return the matching qualifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Qualification> findByOwnerId(long ownerId)
        throws SystemException {
        return findByOwnerId(ownerId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualifications where ownerId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param ownerId the owner ID
     * @param start the lower bound of the range of qualifications
     * @param end the upper bound of the range of qualifications (not inclusive)
     * @return the range of matching qualifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Qualification> findByOwnerId(long ownerId, int start, int end)
        throws SystemException {
        return findByOwnerId(ownerId, start, end, null);
    }

    /**
     * Returns an ordered range of all the qualifications where ownerId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param ownerId the owner ID
     * @param start the lower bound of the range of qualifications
     * @param end the upper bound of the range of qualifications (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Qualification> findByOwnerId(long ownerId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNERID;
            finderArgs = new Object[] { ownerId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_OWNERID;
            finderArgs = new Object[] { ownerId, start, end, orderByComparator };
        }

        List<Qualification> list = (List<Qualification>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Qualification qualification : list) {
                if ((ownerId != qualification.getOwnerId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_QUALIFICATION_WHERE);

            query.append(_FINDER_COLUMN_OWNERID_OWNERID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(ownerId);

                if (!pagination) {
                    list = (List<Qualification>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Qualification>(list);
                } else {
                    list = (List<Qualification>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification in the ordered set where ownerId = &#63;.
     *
     * @param ownerId the owner ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification
     * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification findByOwnerId_First(long ownerId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationException, SystemException {
        Qualification qualification = fetchByOwnerId_First(ownerId,
                orderByComparator);

        if (qualification != null) {
            return qualification;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("ownerId=");
        msg.append(ownerId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationException(msg.toString());
    }

    /**
     * Returns the first qualification in the ordered set where ownerId = &#63;.
     *
     * @param ownerId the owner ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification, or <code>null</code> if a matching qualification could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification fetchByOwnerId_First(long ownerId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Qualification> list = findByOwnerId(ownerId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification in the ordered set where ownerId = &#63;.
     *
     * @param ownerId the owner ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification
     * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification findByOwnerId_Last(long ownerId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationException, SystemException {
        Qualification qualification = fetchByOwnerId_Last(ownerId,
                orderByComparator);

        if (qualification != null) {
            return qualification;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("ownerId=");
        msg.append(ownerId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationException(msg.toString());
    }

    /**
     * Returns the last qualification in the ordered set where ownerId = &#63;.
     *
     * @param ownerId the owner ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification, or <code>null</code> if a matching qualification could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification fetchByOwnerId_Last(long ownerId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByOwnerId(ownerId);

        if (count == 0) {
            return null;
        }

        List<Qualification> list = findByOwnerId(ownerId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualifications before and after the current qualification in the ordered set where ownerId = &#63;.
     *
     * @param qualificationId the primary key of the current qualification
     * @param ownerId the owner ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification
     * @throws de.humance.eco.profile.NoSuchQualificationException if a qualification with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification[] findByOwnerId_PrevAndNext(long qualificationId,
        long ownerId, OrderByComparator orderByComparator)
        throws NoSuchQualificationException, SystemException {
        Qualification qualification = findByPrimaryKey(qualificationId);

        Session session = null;

        try {
            session = openSession();

            Qualification[] array = new QualificationImpl[3];

            array[0] = getByOwnerId_PrevAndNext(session, qualification,
                    ownerId, orderByComparator, true);

            array[1] = qualification;

            array[2] = getByOwnerId_PrevAndNext(session, qualification,
                    ownerId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Qualification getByOwnerId_PrevAndNext(Session session,
        Qualification qualification, long ownerId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATION_WHERE);

        query.append(_FINDER_COLUMN_OWNERID_OWNERID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(ownerId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualification);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Qualification> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualifications where ownerId = &#63; from the database.
     *
     * @param ownerId the owner ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByOwnerId(long ownerId) throws SystemException {
        for (Qualification qualification : findByOwnerId(ownerId,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(qualification);
        }
    }

    /**
     * Returns the number of qualifications where ownerId = &#63;.
     *
     * @param ownerId the owner ID
     * @return the number of matching qualifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByOwnerId(long ownerId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_OWNERID;

        Object[] finderArgs = new Object[] { ownerId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_QUALIFICATION_WHERE);

            query.append(_FINDER_COLUMN_OWNERID_OWNERID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(ownerId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the qualifications where typeId = &#63;.
     *
     * @param typeId the type ID
     * @return the matching qualifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Qualification> findByTypeId(long typeId)
        throws SystemException {
        return findByTypeId(typeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualifications where typeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param typeId the type ID
     * @param start the lower bound of the range of qualifications
     * @param end the upper bound of the range of qualifications (not inclusive)
     * @return the range of matching qualifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Qualification> findByTypeId(long typeId, int start, int end)
        throws SystemException {
        return findByTypeId(typeId, start, end, null);
    }

    /**
     * Returns an ordered range of all the qualifications where typeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param typeId the type ID
     * @param start the lower bound of the range of qualifications
     * @param end the upper bound of the range of qualifications (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Qualification> findByTypeId(long typeId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPEID;
            finderArgs = new Object[] { typeId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TYPEID;
            finderArgs = new Object[] { typeId, start, end, orderByComparator };
        }

        List<Qualification> list = (List<Qualification>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Qualification qualification : list) {
                if ((typeId != qualification.getTypeId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_QUALIFICATION_WHERE);

            query.append(_FINDER_COLUMN_TYPEID_TYPEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(typeId);

                if (!pagination) {
                    list = (List<Qualification>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Qualification>(list);
                } else {
                    list = (List<Qualification>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification in the ordered set where typeId = &#63;.
     *
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification
     * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification findByTypeId_First(long typeId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationException, SystemException {
        Qualification qualification = fetchByTypeId_First(typeId,
                orderByComparator);

        if (qualification != null) {
            return qualification;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("typeId=");
        msg.append(typeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationException(msg.toString());
    }

    /**
     * Returns the first qualification in the ordered set where typeId = &#63;.
     *
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification, or <code>null</code> if a matching qualification could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification fetchByTypeId_First(long typeId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Qualification> list = findByTypeId(typeId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification in the ordered set where typeId = &#63;.
     *
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification
     * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification findByTypeId_Last(long typeId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationException, SystemException {
        Qualification qualification = fetchByTypeId_Last(typeId,
                orderByComparator);

        if (qualification != null) {
            return qualification;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("typeId=");
        msg.append(typeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationException(msg.toString());
    }

    /**
     * Returns the last qualification in the ordered set where typeId = &#63;.
     *
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification, or <code>null</code> if a matching qualification could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification fetchByTypeId_Last(long typeId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByTypeId(typeId);

        if (count == 0) {
            return null;
        }

        List<Qualification> list = findByTypeId(typeId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualifications before and after the current qualification in the ordered set where typeId = &#63;.
     *
     * @param qualificationId the primary key of the current qualification
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification
     * @throws de.humance.eco.profile.NoSuchQualificationException if a qualification with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification[] findByTypeId_PrevAndNext(long qualificationId,
        long typeId, OrderByComparator orderByComparator)
        throws NoSuchQualificationException, SystemException {
        Qualification qualification = findByPrimaryKey(qualificationId);

        Session session = null;

        try {
            session = openSession();

            Qualification[] array = new QualificationImpl[3];

            array[0] = getByTypeId_PrevAndNext(session, qualification, typeId,
                    orderByComparator, true);

            array[1] = qualification;

            array[2] = getByTypeId_PrevAndNext(session, qualification, typeId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Qualification getByTypeId_PrevAndNext(Session session,
        Qualification qualification, long typeId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATION_WHERE);

        query.append(_FINDER_COLUMN_TYPEID_TYPEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(typeId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualification);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Qualification> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualifications where typeId = &#63; from the database.
     *
     * @param typeId the type ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByTypeId(long typeId) throws SystemException {
        for (Qualification qualification : findByTypeId(typeId,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(qualification);
        }
    }

    /**
     * Returns the number of qualifications where typeId = &#63;.
     *
     * @param typeId the type ID
     * @return the number of matching qualifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByTypeId(long typeId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_TYPEID;

        Object[] finderArgs = new Object[] { typeId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_QUALIFICATION_WHERE);

            query.append(_FINDER_COLUMN_TYPEID_TYPEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(typeId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the qualifications where ownerId = &#63; and typeId = &#63;.
     *
     * @param ownerId the owner ID
     * @param typeId the type ID
     * @return the matching qualifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Qualification> findByOwnerIdAndTypeId(long ownerId, long typeId)
        throws SystemException {
        return findByOwnerIdAndTypeId(ownerId, typeId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualifications where ownerId = &#63; and typeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param ownerId the owner ID
     * @param typeId the type ID
     * @param start the lower bound of the range of qualifications
     * @param end the upper bound of the range of qualifications (not inclusive)
     * @return the range of matching qualifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Qualification> findByOwnerIdAndTypeId(long ownerId,
        long typeId, int start, int end) throws SystemException {
        return findByOwnerIdAndTypeId(ownerId, typeId, start, end, null);
    }

    /**
     * Returns an ordered range of all the qualifications where ownerId = &#63; and typeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param ownerId the owner ID
     * @param typeId the type ID
     * @param start the lower bound of the range of qualifications
     * @param end the upper bound of the range of qualifications (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Qualification> findByOwnerIdAndTypeId(long ownerId,
        long typeId, int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNERIDANDTYPEID;
            finderArgs = new Object[] { ownerId, typeId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_OWNERIDANDTYPEID;
            finderArgs = new Object[] {
                    ownerId, typeId,
                    
                    start, end, orderByComparator
                };
        }

        List<Qualification> list = (List<Qualification>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Qualification qualification : list) {
                if ((ownerId != qualification.getOwnerId()) ||
                        (typeId != qualification.getTypeId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_QUALIFICATION_WHERE);

            query.append(_FINDER_COLUMN_OWNERIDANDTYPEID_OWNERID_2);

            query.append(_FINDER_COLUMN_OWNERIDANDTYPEID_TYPEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(ownerId);

                qPos.add(typeId);

                if (!pagination) {
                    list = (List<Qualification>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Qualification>(list);
                } else {
                    list = (List<Qualification>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification in the ordered set where ownerId = &#63; and typeId = &#63;.
     *
     * @param ownerId the owner ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification
     * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification findByOwnerIdAndTypeId_First(long ownerId,
        long typeId, OrderByComparator orderByComparator)
        throws NoSuchQualificationException, SystemException {
        Qualification qualification = fetchByOwnerIdAndTypeId_First(ownerId,
                typeId, orderByComparator);

        if (qualification != null) {
            return qualification;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("ownerId=");
        msg.append(ownerId);

        msg.append(", typeId=");
        msg.append(typeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationException(msg.toString());
    }

    /**
     * Returns the first qualification in the ordered set where ownerId = &#63; and typeId = &#63;.
     *
     * @param ownerId the owner ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification, or <code>null</code> if a matching qualification could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification fetchByOwnerIdAndTypeId_First(long ownerId,
        long typeId, OrderByComparator orderByComparator)
        throws SystemException {
        List<Qualification> list = findByOwnerIdAndTypeId(ownerId, typeId, 0,
                1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification in the ordered set where ownerId = &#63; and typeId = &#63;.
     *
     * @param ownerId the owner ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification
     * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification findByOwnerIdAndTypeId_Last(long ownerId, long typeId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationException, SystemException {
        Qualification qualification = fetchByOwnerIdAndTypeId_Last(ownerId,
                typeId, orderByComparator);

        if (qualification != null) {
            return qualification;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("ownerId=");
        msg.append(ownerId);

        msg.append(", typeId=");
        msg.append(typeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationException(msg.toString());
    }

    /**
     * Returns the last qualification in the ordered set where ownerId = &#63; and typeId = &#63;.
     *
     * @param ownerId the owner ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification, or <code>null</code> if a matching qualification could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification fetchByOwnerIdAndTypeId_Last(long ownerId,
        long typeId, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByOwnerIdAndTypeId(ownerId, typeId);

        if (count == 0) {
            return null;
        }

        List<Qualification> list = findByOwnerIdAndTypeId(ownerId, typeId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualifications before and after the current qualification in the ordered set where ownerId = &#63; and typeId = &#63;.
     *
     * @param qualificationId the primary key of the current qualification
     * @param ownerId the owner ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification
     * @throws de.humance.eco.profile.NoSuchQualificationException if a qualification with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification[] findByOwnerIdAndTypeId_PrevAndNext(
        long qualificationId, long ownerId, long typeId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationException, SystemException {
        Qualification qualification = findByPrimaryKey(qualificationId);

        Session session = null;

        try {
            session = openSession();

            Qualification[] array = new QualificationImpl[3];

            array[0] = getByOwnerIdAndTypeId_PrevAndNext(session,
                    qualification, ownerId, typeId, orderByComparator, true);

            array[1] = qualification;

            array[2] = getByOwnerIdAndTypeId_PrevAndNext(session,
                    qualification, ownerId, typeId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Qualification getByOwnerIdAndTypeId_PrevAndNext(Session session,
        Qualification qualification, long ownerId, long typeId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATION_WHERE);

        query.append(_FINDER_COLUMN_OWNERIDANDTYPEID_OWNERID_2);

        query.append(_FINDER_COLUMN_OWNERIDANDTYPEID_TYPEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(ownerId);

        qPos.add(typeId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualification);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Qualification> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualifications where ownerId = &#63; and typeId = &#63; from the database.
     *
     * @param ownerId the owner ID
     * @param typeId the type ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByOwnerIdAndTypeId(long ownerId, long typeId)
        throws SystemException {
        for (Qualification qualification : findByOwnerIdAndTypeId(ownerId,
                typeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(qualification);
        }
    }

    /**
     * Returns the number of qualifications where ownerId = &#63; and typeId = &#63;.
     *
     * @param ownerId the owner ID
     * @param typeId the type ID
     * @return the number of matching qualifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByOwnerIdAndTypeId(long ownerId, long typeId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_OWNERIDANDTYPEID;

        Object[] finderArgs = new Object[] { ownerId, typeId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_QUALIFICATION_WHERE);

            query.append(_FINDER_COLUMN_OWNERIDANDTYPEID_OWNERID_2);

            query.append(_FINDER_COLUMN_OWNERIDANDTYPEID_TYPEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(ownerId);

                qPos.add(typeId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the qualification in the entity cache if it is enabled.
     *
     * @param qualification the qualification
     */
    @Override
    public void cacheResult(Qualification qualification) {
        EntityCacheUtil.putResult(QualificationModelImpl.ENTITY_CACHE_ENABLED,
            QualificationImpl.class, qualification.getPrimaryKey(),
            qualification);

        qualification.resetOriginalValues();
    }

    /**
     * Caches the qualifications in the entity cache if it is enabled.
     *
     * @param qualifications the qualifications
     */
    @Override
    public void cacheResult(List<Qualification> qualifications) {
        for (Qualification qualification : qualifications) {
            if (EntityCacheUtil.getResult(
                        QualificationModelImpl.ENTITY_CACHE_ENABLED,
                        QualificationImpl.class, qualification.getPrimaryKey()) == null) {
                cacheResult(qualification);
            } else {
                qualification.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all qualifications.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(QualificationImpl.class.getName());
        }

        EntityCacheUtil.clearCache(QualificationImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the qualification.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Qualification qualification) {
        EntityCacheUtil.removeResult(QualificationModelImpl.ENTITY_CACHE_ENABLED,
            QualificationImpl.class, qualification.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Qualification> qualifications) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Qualification qualification : qualifications) {
            EntityCacheUtil.removeResult(QualificationModelImpl.ENTITY_CACHE_ENABLED,
                QualificationImpl.class, qualification.getPrimaryKey());
        }
    }

    /**
     * Creates a new qualification with the primary key. Does not add the qualification to the database.
     *
     * @param qualificationId the primary key for the new qualification
     * @return the new qualification
     */
    @Override
    public Qualification create(long qualificationId) {
        Qualification qualification = new QualificationImpl();

        qualification.setNew(true);
        qualification.setPrimaryKey(qualificationId);

        return qualification;
    }

    /**
     * Removes the qualification with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param qualificationId the primary key of the qualification
     * @return the qualification that was removed
     * @throws de.humance.eco.profile.NoSuchQualificationException if a qualification with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification remove(long qualificationId)
        throws NoSuchQualificationException, SystemException {
        return remove((Serializable) qualificationId);
    }

    /**
     * Removes the qualification with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the qualification
     * @return the qualification that was removed
     * @throws de.humance.eco.profile.NoSuchQualificationException if a qualification with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification remove(Serializable primaryKey)
        throws NoSuchQualificationException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Qualification qualification = (Qualification) session.get(QualificationImpl.class,
                    primaryKey);

            if (qualification == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchQualificationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(qualification);
        } catch (NoSuchQualificationException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Qualification removeImpl(Qualification qualification)
        throws SystemException {
        qualification = toUnwrappedModel(qualification);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(qualification)) {
                qualification = (Qualification) session.get(QualificationImpl.class,
                        qualification.getPrimaryKeyObj());
            }

            if (qualification != null) {
                session.delete(qualification);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (qualification != null) {
            clearCache(qualification);
        }

        return qualification;
    }

    @Override
    public Qualification updateImpl(
        de.humance.eco.profile.model.Qualification qualification)
        throws SystemException {
        qualification = toUnwrappedModel(qualification);

        boolean isNew = qualification.isNew();

        QualificationModelImpl qualificationModelImpl = (QualificationModelImpl) qualification;

        Session session = null;

        try {
            session = openSession();

            if (qualification.isNew()) {
                session.save(qualification);

                qualification.setNew(false);
            } else {
                session.merge(qualification);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !QualificationModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((qualificationModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNERID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        qualificationModelImpl.getOriginalOwnerId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_OWNERID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNERID,
                    args);

                args = new Object[] { qualificationModelImpl.getOwnerId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_OWNERID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNERID,
                    args);
            }

            if ((qualificationModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPEID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        qualificationModelImpl.getOriginalTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TYPEID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPEID,
                    args);

                args = new Object[] { qualificationModelImpl.getTypeId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TYPEID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPEID,
                    args);
            }

            if ((qualificationModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNERIDANDTYPEID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        qualificationModelImpl.getOriginalOwnerId(),
                        qualificationModelImpl.getOriginalTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_OWNERIDANDTYPEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNERIDANDTYPEID,
                    args);

                args = new Object[] {
                        qualificationModelImpl.getOwnerId(),
                        qualificationModelImpl.getTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_OWNERIDANDTYPEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNERIDANDTYPEID,
                    args);
            }
        }

        EntityCacheUtil.putResult(QualificationModelImpl.ENTITY_CACHE_ENABLED,
            QualificationImpl.class, qualification.getPrimaryKey(),
            qualification);

        return qualification;
    }

    protected Qualification toUnwrappedModel(Qualification qualification) {
        if (qualification instanceof QualificationImpl) {
            return qualification;
        }

        QualificationImpl qualificationImpl = new QualificationImpl();

        qualificationImpl.setNew(qualification.isNew());
        qualificationImpl.setPrimaryKey(qualification.getPrimaryKey());

        qualificationImpl.setQualificationId(qualification.getQualificationId());
        qualificationImpl.setCreatorId(qualification.getCreatorId());
        qualificationImpl.setCreatorName(qualification.getCreatorName());
        qualificationImpl.setCreateDate(qualification.getCreateDate());
        qualificationImpl.setModifierId(qualification.getModifierId());
        qualificationImpl.setModifierName(qualification.getModifierName());
        qualificationImpl.setModifiedDate(qualification.getModifiedDate());
        qualificationImpl.setOwnerId(qualification.getOwnerId());
        qualificationImpl.setTypeId(qualification.getTypeId());
        qualificationImpl.setName(qualification.getName());
        qualificationImpl.setCertNumber(qualification.getCertNumber());
        qualificationImpl.setDeliveryDate(qualification.getDeliveryDate());
        qualificationImpl.setExpirationDate(qualification.getExpirationDate());
        qualificationImpl.setIssuingCountry(qualification.getIssuingCountry());
        qualificationImpl.setIssuingInstitution(qualification.getIssuingInstitution());

        return qualificationImpl;
    }

    /**
     * Returns the qualification with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the qualification
     * @return the qualification
     * @throws de.humance.eco.profile.NoSuchQualificationException if a qualification with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification findByPrimaryKey(Serializable primaryKey)
        throws NoSuchQualificationException, SystemException {
        Qualification qualification = fetchByPrimaryKey(primaryKey);

        if (qualification == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchQualificationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return qualification;
    }

    /**
     * Returns the qualification with the primary key or throws a {@link de.humance.eco.profile.NoSuchQualificationException} if it could not be found.
     *
     * @param qualificationId the primary key of the qualification
     * @return the qualification
     * @throws de.humance.eco.profile.NoSuchQualificationException if a qualification with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification findByPrimaryKey(long qualificationId)
        throws NoSuchQualificationException, SystemException {
        return findByPrimaryKey((Serializable) qualificationId);
    }

    /**
     * Returns the qualification with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the qualification
     * @return the qualification, or <code>null</code> if a qualification with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Qualification qualification = (Qualification) EntityCacheUtil.getResult(QualificationModelImpl.ENTITY_CACHE_ENABLED,
                QualificationImpl.class, primaryKey);

        if (qualification == _nullQualification) {
            return null;
        }

        if (qualification == null) {
            Session session = null;

            try {
                session = openSession();

                qualification = (Qualification) session.get(QualificationImpl.class,
                        primaryKey);

                if (qualification != null) {
                    cacheResult(qualification);
                } else {
                    EntityCacheUtil.putResult(QualificationModelImpl.ENTITY_CACHE_ENABLED,
                        QualificationImpl.class, primaryKey, _nullQualification);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(QualificationModelImpl.ENTITY_CACHE_ENABLED,
                    QualificationImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return qualification;
    }

    /**
     * Returns the qualification with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param qualificationId the primary key of the qualification
     * @return the qualification, or <code>null</code> if a qualification with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Qualification fetchByPrimaryKey(long qualificationId)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) qualificationId);
    }

    /**
     * Returns all the qualifications.
     *
     * @return the qualifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Qualification> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualifications.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of qualifications
     * @param end the upper bound of the range of qualifications (not inclusive)
     * @return the range of qualifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Qualification> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the qualifications.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of qualifications
     * @param end the upper bound of the range of qualifications (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of qualifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Qualification> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Qualification> list = (List<Qualification>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_QUALIFICATION);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_QUALIFICATION;

                if (pagination) {
                    sql = sql.concat(QualificationModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Qualification>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Qualification>(list);
                } else {
                    list = (List<Qualification>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the qualifications from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Qualification qualification : findAll()) {
            remove(qualification);
        }
    }

    /**
     * Returns the number of qualifications.
     *
     * @return the number of qualifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_QUALIFICATION);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the qualification persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.de.humance.eco.profile.model.Qualification")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Qualification>> listenersList = new ArrayList<ModelListener<Qualification>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Qualification>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(QualificationImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
