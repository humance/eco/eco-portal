package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.humance.eco.profile.NoSuchQualificationTypeException;
import de.humance.eco.profile.model.QualificationType;
import de.humance.eco.profile.model.impl.QualificationTypeImpl;
import de.humance.eco.profile.model.impl.QualificationTypeModelImpl;
import de.humance.eco.profile.service.persistence.QualificationTypePersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the qualification type service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see QualificationTypePersistence
 * @see QualificationTypeUtil
 * @generated
 */
public class QualificationTypePersistenceImpl extends BasePersistenceImpl<QualificationType>
    implements QualificationTypePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link QualificationTypeUtil} to access the qualification type persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = QualificationTypeImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(QualificationTypeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeModelImpl.FINDER_CACHE_ENABLED,
            QualificationTypeImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(QualificationTypeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeModelImpl.FINDER_CACHE_ENABLED,
            QualificationTypeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(QualificationTypeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME = new FinderPath(QualificationTypeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeModelImpl.FINDER_CACHE_ENABLED,
            QualificationTypeImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByName",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME = new FinderPath(QualificationTypeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeModelImpl.FINDER_CACHE_ENABLED,
            QualificationTypeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByName",
            new String[] { String.class.getName() },
            QualificationTypeModelImpl.NAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_NAME = new FinderPath(QualificationTypeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByName",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_NAME_NAME_1 = "qualificationType.name IS NULL";
    private static final String _FINDER_COLUMN_NAME_NAME_2 = "lower(qualificationType.name) = ?";
    private static final String _FINDER_COLUMN_NAME_NAME_3 = "(qualificationType.name IS NULL OR qualificationType.name = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NAMELIKE = new FinderPath(QualificationTypeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeModelImpl.FINDER_CACHE_ENABLED,
            QualificationTypeImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByNameLike",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_NAMELIKE =
        new FinderPath(QualificationTypeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByNameLike",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_NAMELIKE_NAME_1 = "qualificationType.name LIKE NULL";
    private static final String _FINDER_COLUMN_NAMELIKE_NAME_2 = "lower(qualificationType.name) LIKE ?";
    private static final String _FINDER_COLUMN_NAMELIKE_NAME_3 = "(qualificationType.name IS NULL OR qualificationType.name LIKE '')";
    private static final String _SQL_SELECT_QUALIFICATIONTYPE = "SELECT qualificationType FROM QualificationType qualificationType";
    private static final String _SQL_SELECT_QUALIFICATIONTYPE_WHERE = "SELECT qualificationType FROM QualificationType qualificationType WHERE ";
    private static final String _SQL_COUNT_QUALIFICATIONTYPE = "SELECT COUNT(qualificationType) FROM QualificationType qualificationType";
    private static final String _SQL_COUNT_QUALIFICATIONTYPE_WHERE = "SELECT COUNT(qualificationType) FROM QualificationType qualificationType WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "qualificationType.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No QualificationType exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No QualificationType exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(QualificationTypePersistenceImpl.class);
    private static QualificationType _nullQualificationType = new QualificationTypeImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<QualificationType> toCacheModel() {
                return _nullQualificationTypeCacheModel;
            }
        };

    private static CacheModel<QualificationType> _nullQualificationTypeCacheModel =
        new CacheModel<QualificationType>() {
            @Override
            public QualificationType toEntityModel() {
                return _nullQualificationType;
            }
        };

    public QualificationTypePersistenceImpl() {
        setModelClass(QualificationType.class);
    }

    /**
     * Returns all the qualification types where name = &#63;.
     *
     * @param name the name
     * @return the matching qualification types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationType> findByName(String name)
        throws SystemException {
        return findByName(name, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification types where name = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param name the name
     * @param start the lower bound of the range of qualification types
     * @param end the upper bound of the range of qualification types (not inclusive)
     * @return the range of matching qualification types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationType> findByName(String name, int start, int end)
        throws SystemException {
        return findByName(name, start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification types where name = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param name the name
     * @param start the lower bound of the range of qualification types
     * @param end the upper bound of the range of qualification types (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualification types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationType> findByName(String name, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME;
            finderArgs = new Object[] { name };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME;
            finderArgs = new Object[] { name, start, end, orderByComparator };
        }

        List<QualificationType> list = (List<QualificationType>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (QualificationType qualificationType : list) {
                if (!Validator.equals(name, qualificationType.getName())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_QUALIFICATIONTYPE_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAME_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAME_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAME_NAME_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationTypeModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name.toLowerCase());
                }

                if (!pagination) {
                    list = (List<QualificationType>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationType>(list);
                } else {
                    list = (List<QualificationType>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification type in the ordered set where name = &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification type
     * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a matching qualification type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationType findByName_First(String name,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeException, SystemException {
        QualificationType qualificationType = fetchByName_First(name,
                orderByComparator);

        if (qualificationType != null) {
            return qualificationType;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("name=");
        msg.append(name);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationTypeException(msg.toString());
    }

    /**
     * Returns the first qualification type in the ordered set where name = &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification type, or <code>null</code> if a matching qualification type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationType fetchByName_First(String name,
        OrderByComparator orderByComparator) throws SystemException {
        List<QualificationType> list = findByName(name, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification type in the ordered set where name = &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification type
     * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a matching qualification type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationType findByName_Last(String name,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeException, SystemException {
        QualificationType qualificationType = fetchByName_Last(name,
                orderByComparator);

        if (qualificationType != null) {
            return qualificationType;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("name=");
        msg.append(name);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationTypeException(msg.toString());
    }

    /**
     * Returns the last qualification type in the ordered set where name = &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification type, or <code>null</code> if a matching qualification type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationType fetchByName_Last(String name,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByName(name);

        if (count == 0) {
            return null;
        }

        List<QualificationType> list = findByName(name, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualification types before and after the current qualification type in the ordered set where name = &#63;.
     *
     * @param qualificationTypeId the primary key of the current qualification type
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification type
     * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a qualification type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationType[] findByName_PrevAndNext(
        long qualificationTypeId, String name,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeException, SystemException {
        QualificationType qualificationType = findByPrimaryKey(qualificationTypeId);

        Session session = null;

        try {
            session = openSession();

            QualificationType[] array = new QualificationTypeImpl[3];

            array[0] = getByName_PrevAndNext(session, qualificationType, name,
                    orderByComparator, true);

            array[1] = qualificationType;

            array[2] = getByName_PrevAndNext(session, qualificationType, name,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected QualificationType getByName_PrevAndNext(Session session,
        QualificationType qualificationType, String name,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATIONTYPE_WHERE);

        boolean bindName = false;

        if (name == null) {
            query.append(_FINDER_COLUMN_NAME_NAME_1);
        } else if (name.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_NAME_NAME_3);
        } else {
            bindName = true;

            query.append(_FINDER_COLUMN_NAME_NAME_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationTypeModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindName) {
            qPos.add(name.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualificationType);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<QualificationType> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualification types where name = &#63; from the database.
     *
     * @param name the name
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByName(String name) throws SystemException {
        for (QualificationType qualificationType : findByName(name,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(qualificationType);
        }
    }

    /**
     * Returns the number of qualification types where name = &#63;.
     *
     * @param name the name
     * @return the number of matching qualification types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByName(String name) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_NAME;

        Object[] finderArgs = new Object[] { name };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_QUALIFICATIONTYPE_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAME_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAME_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAME_NAME_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the qualification types where name LIKE &#63;.
     *
     * @param name the name
     * @return the matching qualification types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationType> findByNameLike(String name)
        throws SystemException {
        return findByNameLike(name, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification types where name LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param name the name
     * @param start the lower bound of the range of qualification types
     * @param end the upper bound of the range of qualification types (not inclusive)
     * @return the range of matching qualification types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationType> findByNameLike(String name, int start,
        int end) throws SystemException {
        return findByNameLike(name, start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification types where name LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param name the name
     * @param start the lower bound of the range of qualification types
     * @param end the upper bound of the range of qualification types (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualification types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationType> findByNameLike(String name, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NAMELIKE;
        finderArgs = new Object[] { name, start, end, orderByComparator };

        List<QualificationType> list = (List<QualificationType>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (QualificationType qualificationType : list) {
                if (!StringUtil.wildcardMatches(qualificationType.getName(),
                            name, CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, false)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_QUALIFICATIONTYPE_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAMELIKE_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAMELIKE_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAMELIKE_NAME_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationTypeModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name.toLowerCase());
                }

                if (!pagination) {
                    list = (List<QualificationType>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationType>(list);
                } else {
                    list = (List<QualificationType>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification type in the ordered set where name LIKE &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification type
     * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a matching qualification type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationType findByNameLike_First(String name,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeException, SystemException {
        QualificationType qualificationType = fetchByNameLike_First(name,
                orderByComparator);

        if (qualificationType != null) {
            return qualificationType;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("name=");
        msg.append(name);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationTypeException(msg.toString());
    }

    /**
     * Returns the first qualification type in the ordered set where name LIKE &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification type, or <code>null</code> if a matching qualification type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationType fetchByNameLike_First(String name,
        OrderByComparator orderByComparator) throws SystemException {
        List<QualificationType> list = findByNameLike(name, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification type in the ordered set where name LIKE &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification type
     * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a matching qualification type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationType findByNameLike_Last(String name,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeException, SystemException {
        QualificationType qualificationType = fetchByNameLike_Last(name,
                orderByComparator);

        if (qualificationType != null) {
            return qualificationType;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("name=");
        msg.append(name);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationTypeException(msg.toString());
    }

    /**
     * Returns the last qualification type in the ordered set where name LIKE &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification type, or <code>null</code> if a matching qualification type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationType fetchByNameLike_Last(String name,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByNameLike(name);

        if (count == 0) {
            return null;
        }

        List<QualificationType> list = findByNameLike(name, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualification types before and after the current qualification type in the ordered set where name LIKE &#63;.
     *
     * @param qualificationTypeId the primary key of the current qualification type
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification type
     * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a qualification type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationType[] findByNameLike_PrevAndNext(
        long qualificationTypeId, String name,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeException, SystemException {
        QualificationType qualificationType = findByPrimaryKey(qualificationTypeId);

        Session session = null;

        try {
            session = openSession();

            QualificationType[] array = new QualificationTypeImpl[3];

            array[0] = getByNameLike_PrevAndNext(session, qualificationType,
                    name, orderByComparator, true);

            array[1] = qualificationType;

            array[2] = getByNameLike_PrevAndNext(session, qualificationType,
                    name, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected QualificationType getByNameLike_PrevAndNext(Session session,
        QualificationType qualificationType, String name,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATIONTYPE_WHERE);

        boolean bindName = false;

        if (name == null) {
            query.append(_FINDER_COLUMN_NAMELIKE_NAME_1);
        } else if (name.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_NAMELIKE_NAME_3);
        } else {
            bindName = true;

            query.append(_FINDER_COLUMN_NAMELIKE_NAME_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationTypeModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindName) {
            qPos.add(name.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualificationType);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<QualificationType> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualification types where name LIKE &#63; from the database.
     *
     * @param name the name
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByNameLike(String name) throws SystemException {
        for (QualificationType qualificationType : findByNameLike(name,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(qualificationType);
        }
    }

    /**
     * Returns the number of qualification types where name LIKE &#63;.
     *
     * @param name the name
     * @return the number of matching qualification types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByNameLike(String name) throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_NAMELIKE;

        Object[] finderArgs = new Object[] { name };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_QUALIFICATIONTYPE_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAMELIKE_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAMELIKE_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAMELIKE_NAME_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the qualification type in the entity cache if it is enabled.
     *
     * @param qualificationType the qualification type
     */
    @Override
    public void cacheResult(QualificationType qualificationType) {
        EntityCacheUtil.putResult(QualificationTypeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeImpl.class, qualificationType.getPrimaryKey(),
            qualificationType);

        qualificationType.resetOriginalValues();
    }

    /**
     * Caches the qualification types in the entity cache if it is enabled.
     *
     * @param qualificationTypes the qualification types
     */
    @Override
    public void cacheResult(List<QualificationType> qualificationTypes) {
        for (QualificationType qualificationType : qualificationTypes) {
            if (EntityCacheUtil.getResult(
                        QualificationTypeModelImpl.ENTITY_CACHE_ENABLED,
                        QualificationTypeImpl.class,
                        qualificationType.getPrimaryKey()) == null) {
                cacheResult(qualificationType);
            } else {
                qualificationType.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all qualification types.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(QualificationTypeImpl.class.getName());
        }

        EntityCacheUtil.clearCache(QualificationTypeImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the qualification type.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(QualificationType qualificationType) {
        EntityCacheUtil.removeResult(QualificationTypeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeImpl.class, qualificationType.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<QualificationType> qualificationTypes) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (QualificationType qualificationType : qualificationTypes) {
            EntityCacheUtil.removeResult(QualificationTypeModelImpl.ENTITY_CACHE_ENABLED,
                QualificationTypeImpl.class, qualificationType.getPrimaryKey());
        }
    }

    /**
     * Creates a new qualification type with the primary key. Does not add the qualification type to the database.
     *
     * @param qualificationTypeId the primary key for the new qualification type
     * @return the new qualification type
     */
    @Override
    public QualificationType create(long qualificationTypeId) {
        QualificationType qualificationType = new QualificationTypeImpl();

        qualificationType.setNew(true);
        qualificationType.setPrimaryKey(qualificationTypeId);

        return qualificationType;
    }

    /**
     * Removes the qualification type with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param qualificationTypeId the primary key of the qualification type
     * @return the qualification type that was removed
     * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a qualification type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationType remove(long qualificationTypeId)
        throws NoSuchQualificationTypeException, SystemException {
        return remove((Serializable) qualificationTypeId);
    }

    /**
     * Removes the qualification type with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the qualification type
     * @return the qualification type that was removed
     * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a qualification type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationType remove(Serializable primaryKey)
        throws NoSuchQualificationTypeException, SystemException {
        Session session = null;

        try {
            session = openSession();

            QualificationType qualificationType = (QualificationType) session.get(QualificationTypeImpl.class,
                    primaryKey);

            if (qualificationType == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchQualificationTypeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(qualificationType);
        } catch (NoSuchQualificationTypeException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected QualificationType removeImpl(QualificationType qualificationType)
        throws SystemException {
        qualificationType = toUnwrappedModel(qualificationType);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(qualificationType)) {
                qualificationType = (QualificationType) session.get(QualificationTypeImpl.class,
                        qualificationType.getPrimaryKeyObj());
            }

            if (qualificationType != null) {
                session.delete(qualificationType);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (qualificationType != null) {
            clearCache(qualificationType);
        }

        return qualificationType;
    }

    @Override
    public QualificationType updateImpl(
        de.humance.eco.profile.model.QualificationType qualificationType)
        throws SystemException {
        qualificationType = toUnwrappedModel(qualificationType);

        boolean isNew = qualificationType.isNew();

        QualificationTypeModelImpl qualificationTypeModelImpl = (QualificationTypeModelImpl) qualificationType;

        Session session = null;

        try {
            session = openSession();

            if (qualificationType.isNew()) {
                session.save(qualificationType);

                qualificationType.setNew(false);
            } else {
                session.merge(qualificationType);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !QualificationTypeModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((qualificationTypeModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        qualificationTypeModelImpl.getOriginalName()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
                    args);

                args = new Object[] { qualificationTypeModelImpl.getName() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
                    args);
            }
        }

        EntityCacheUtil.putResult(QualificationTypeModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeImpl.class, qualificationType.getPrimaryKey(),
            qualificationType);

        return qualificationType;
    }

    protected QualificationType toUnwrappedModel(
        QualificationType qualificationType) {
        if (qualificationType instanceof QualificationTypeImpl) {
            return qualificationType;
        }

        QualificationTypeImpl qualificationTypeImpl = new QualificationTypeImpl();

        qualificationTypeImpl.setNew(qualificationType.isNew());
        qualificationTypeImpl.setPrimaryKey(qualificationType.getPrimaryKey());

        qualificationTypeImpl.setQualificationTypeId(qualificationType.getQualificationTypeId());
        qualificationTypeImpl.setCreatorId(qualificationType.getCreatorId());
        qualificationTypeImpl.setCreatorName(qualificationType.getCreatorName());
        qualificationTypeImpl.setCreateDate(qualificationType.getCreateDate());
        qualificationTypeImpl.setModifierId(qualificationType.getModifierId());
        qualificationTypeImpl.setModifierName(qualificationType.getModifierName());
        qualificationTypeImpl.setModifiedDate(qualificationType.getModifiedDate());
        qualificationTypeImpl.setName(qualificationType.getName());
        qualificationTypeImpl.setDescription(qualificationType.getDescription());
        qualificationTypeImpl.setNote(qualificationType.getNote());
        qualificationTypeImpl.setIconId(qualificationType.getIconId());

        return qualificationTypeImpl;
    }

    /**
     * Returns the qualification type with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the qualification type
     * @return the qualification type
     * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a qualification type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationType findByPrimaryKey(Serializable primaryKey)
        throws NoSuchQualificationTypeException, SystemException {
        QualificationType qualificationType = fetchByPrimaryKey(primaryKey);

        if (qualificationType == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchQualificationTypeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return qualificationType;
    }

    /**
     * Returns the qualification type with the primary key or throws a {@link de.humance.eco.profile.NoSuchQualificationTypeException} if it could not be found.
     *
     * @param qualificationTypeId the primary key of the qualification type
     * @return the qualification type
     * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a qualification type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationType findByPrimaryKey(long qualificationTypeId)
        throws NoSuchQualificationTypeException, SystemException {
        return findByPrimaryKey((Serializable) qualificationTypeId);
    }

    /**
     * Returns the qualification type with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the qualification type
     * @return the qualification type, or <code>null</code> if a qualification type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationType fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        QualificationType qualificationType = (QualificationType) EntityCacheUtil.getResult(QualificationTypeModelImpl.ENTITY_CACHE_ENABLED,
                QualificationTypeImpl.class, primaryKey);

        if (qualificationType == _nullQualificationType) {
            return null;
        }

        if (qualificationType == null) {
            Session session = null;

            try {
                session = openSession();

                qualificationType = (QualificationType) session.get(QualificationTypeImpl.class,
                        primaryKey);

                if (qualificationType != null) {
                    cacheResult(qualificationType);
                } else {
                    EntityCacheUtil.putResult(QualificationTypeModelImpl.ENTITY_CACHE_ENABLED,
                        QualificationTypeImpl.class, primaryKey,
                        _nullQualificationType);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(QualificationTypeModelImpl.ENTITY_CACHE_ENABLED,
                    QualificationTypeImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return qualificationType;
    }

    /**
     * Returns the qualification type with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param qualificationTypeId the primary key of the qualification type
     * @return the qualification type, or <code>null</code> if a qualification type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationType fetchByPrimaryKey(long qualificationTypeId)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) qualificationTypeId);
    }

    /**
     * Returns all the qualification types.
     *
     * @return the qualification types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationType> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification types.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of qualification types
     * @param end the upper bound of the range of qualification types (not inclusive)
     * @return the range of qualification types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationType> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification types.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of qualification types
     * @param end the upper bound of the range of qualification types (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of qualification types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationType> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<QualificationType> list = (List<QualificationType>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_QUALIFICATIONTYPE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_QUALIFICATIONTYPE;

                if (pagination) {
                    sql = sql.concat(QualificationTypeModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<QualificationType>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationType>(list);
                } else {
                    list = (List<QualificationType>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the qualification types from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (QualificationType qualificationType : findAll()) {
            remove(qualificationType);
        }
    }

    /**
     * Returns the number of qualification types.
     *
     * @return the number of qualification types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_QUALIFICATIONTYPE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the qualification type persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.de.humance.eco.profile.model.QualificationType")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<QualificationType>> listenersList = new ArrayList<ModelListener<QualificationType>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<QualificationType>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(QualificationTypeImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
