package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.humance.eco.profile.NoSuchQualificationTypeResourceException;
import de.humance.eco.profile.model.QualificationTypeResource;
import de.humance.eco.profile.model.impl.QualificationTypeResourceImpl;
import de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl;
import de.humance.eco.profile.service.persistence.QualificationTypeResourcePersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the qualification type resource service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see QualificationTypeResourcePersistence
 * @see QualificationTypeResourceUtil
 * @generated
 */
public class QualificationTypeResourcePersistenceImpl
    extends BasePersistenceImpl<QualificationTypeResource>
    implements QualificationTypeResourcePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link QualificationTypeResourceUtil} to access the qualification type resource persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = QualificationTypeResourceImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
            new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONTYPEID =
        new FinderPath(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByQualificationTypeId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEID =
        new FinderPath(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByQualificationTypeId", new String[] { Long.class.getName() },
            QualificationTypeResourceModelImpl.QUALIFICATIONTYPEID_COLUMN_BITMASK |
            QualificationTypeResourceModelImpl.COUNTRY_COLUMN_BITMASK |
            QualificationTypeResourceModelImpl.LANGUAGE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEID = new FinderPath(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByQualificationTypeId", new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_QUALIFICATIONTYPEID_QUALIFICATIONTYPEID_2 =
        "qualificationTypeResource.qualificationTypeId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDCOUNTRY =
        new FinderPath(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByQualificationTypeIdAndCountry",
            new String[] {
                Long.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDCOUNTRY =
        new FinderPath(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByQualificationTypeIdAndCountry",
            new String[] { Long.class.getName(), String.class.getName() },
            QualificationTypeResourceModelImpl.QUALIFICATIONTYPEID_COLUMN_BITMASK |
            QualificationTypeResourceModelImpl.COUNTRY_COLUMN_BITMASK |
            QualificationTypeResourceModelImpl.LANGUAGE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEIDANDCOUNTRY =
        new FinderPath(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByQualificationTypeIdAndCountry",
            new String[] { Long.class.getName(), String.class.getName() });
    private static final String _FINDER_COLUMN_QUALIFICATIONTYPEIDANDCOUNTRY_QUALIFICATIONTYPEID_2 =
        "qualificationTypeResource.qualificationTypeId = ? AND ";
    private static final String _FINDER_COLUMN_QUALIFICATIONTYPEIDANDCOUNTRY_COUNTRY_1 =
        "qualificationTypeResource.country IS NULL";
    private static final String _FINDER_COLUMN_QUALIFICATIONTYPEIDANDCOUNTRY_COUNTRY_2 =
        "lower(qualificationTypeResource.country) = ?";
    private static final String _FINDER_COLUMN_QUALIFICATIONTYPEIDANDCOUNTRY_COUNTRY_3 =
        "(qualificationTypeResource.country IS NULL OR qualificationTypeResource.country = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDLANGUAGE =
        new FinderPath(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByQualificationTypeIdAndLanguage",
            new String[] {
                Long.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDLANGUAGE =
        new FinderPath(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByQualificationTypeIdAndLanguage",
            new String[] { Long.class.getName(), String.class.getName() },
            QualificationTypeResourceModelImpl.QUALIFICATIONTYPEID_COLUMN_BITMASK |
            QualificationTypeResourceModelImpl.LANGUAGE_COLUMN_BITMASK |
            QualificationTypeResourceModelImpl.COUNTRY_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEIDANDLANGUAGE =
        new FinderPath(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByQualificationTypeIdAndLanguage",
            new String[] { Long.class.getName(), String.class.getName() });
    private static final String _FINDER_COLUMN_QUALIFICATIONTYPEIDANDLANGUAGE_QUALIFICATIONTYPEID_2 =
        "qualificationTypeResource.qualificationTypeId = ? AND ";
    private static final String _FINDER_COLUMN_QUALIFICATIONTYPEIDANDLANGUAGE_LANGUAGE_1 =
        "qualificationTypeResource.language IS NULL";
    private static final String _FINDER_COLUMN_QUALIFICATIONTYPEIDANDLANGUAGE_LANGUAGE_2 =
        "lower(qualificationTypeResource.language) = ?";
    private static final String _FINDER_COLUMN_QUALIFICATIONTYPEIDANDLANGUAGE_LANGUAGE_3 =
        "(qualificationTypeResource.language IS NULL OR qualificationTypeResource.language = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDLOCALE =
        new FinderPath(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByQualificationTypeIdAndLocale",
            new String[] {
                Long.class.getName(), String.class.getName(),
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDLOCALE =
        new FinderPath(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            QualificationTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByQualificationTypeIdAndLocale",
            new String[] {
                Long.class.getName(), String.class.getName(),
                String.class.getName()
            },
            QualificationTypeResourceModelImpl.QUALIFICATIONTYPEID_COLUMN_BITMASK |
            QualificationTypeResourceModelImpl.COUNTRY_COLUMN_BITMASK |
            QualificationTypeResourceModelImpl.LANGUAGE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEIDANDLOCALE =
        new FinderPath(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByQualificationTypeIdAndLocale",
            new String[] {
                Long.class.getName(), String.class.getName(),
                String.class.getName()
            });
    private static final String _FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_QUALIFICATIONTYPEID_2 =
        "qualificationTypeResource.qualificationTypeId = ? AND ";
    private static final String _FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_COUNTRY_1 =
        "qualificationTypeResource.country IS NULL AND ";
    private static final String _FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_COUNTRY_2 =
        "lower(qualificationTypeResource.country) = ? AND ";
    private static final String _FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_COUNTRY_3 =
        "(qualificationTypeResource.country IS NULL OR qualificationTypeResource.country = '') AND ";
    private static final String _FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_LANGUAGE_1 =
        "qualificationTypeResource.language IS NULL";
    private static final String _FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_LANGUAGE_2 =
        "lower(qualificationTypeResource.language) = ?";
    private static final String _FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_LANGUAGE_3 =
        "(qualificationTypeResource.language IS NULL OR qualificationTypeResource.language = '')";
    private static final String _SQL_SELECT_QUALIFICATIONTYPERESOURCE = "SELECT qualificationTypeResource FROM QualificationTypeResource qualificationTypeResource";
    private static final String _SQL_SELECT_QUALIFICATIONTYPERESOURCE_WHERE = "SELECT qualificationTypeResource FROM QualificationTypeResource qualificationTypeResource WHERE ";
    private static final String _SQL_COUNT_QUALIFICATIONTYPERESOURCE = "SELECT COUNT(qualificationTypeResource) FROM QualificationTypeResource qualificationTypeResource";
    private static final String _SQL_COUNT_QUALIFICATIONTYPERESOURCE_WHERE = "SELECT COUNT(qualificationTypeResource) FROM QualificationTypeResource qualificationTypeResource WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "qualificationTypeResource.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No QualificationTypeResource exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No QualificationTypeResource exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(QualificationTypeResourcePersistenceImpl.class);
    private static QualificationTypeResource _nullQualificationTypeResource = new QualificationTypeResourceImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<QualificationTypeResource> toCacheModel() {
                return _nullQualificationTypeResourceCacheModel;
            }
        };

    private static CacheModel<QualificationTypeResource> _nullQualificationTypeResourceCacheModel =
        new CacheModel<QualificationTypeResource>() {
            @Override
            public QualificationTypeResource toEntityModel() {
                return _nullQualificationTypeResource;
            }
        };

    public QualificationTypeResourcePersistenceImpl() {
        setModelClass(QualificationTypeResource.class);
    }

    /**
     * Returns all the qualification type resources where qualificationTypeId = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @return the matching qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationTypeResource> findByQualificationTypeId(
        long qualificationTypeId) throws SystemException {
        return findByQualificationTypeId(qualificationTypeId,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification type resources where qualificationTypeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationTypeId the qualification type ID
     * @param start the lower bound of the range of qualification type resources
     * @param end the upper bound of the range of qualification type resources (not inclusive)
     * @return the range of matching qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationTypeResource> findByQualificationTypeId(
        long qualificationTypeId, int start, int end) throws SystemException {
        return findByQualificationTypeId(qualificationTypeId, start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification type resources where qualificationTypeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationTypeId the qualification type ID
     * @param start the lower bound of the range of qualification type resources
     * @param end the upper bound of the range of qualification type resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationTypeResource> findByQualificationTypeId(
        long qualificationTypeId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEID;
            finderArgs = new Object[] { qualificationTypeId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONTYPEID;
            finderArgs = new Object[] {
                    qualificationTypeId,
                    
                    start, end, orderByComparator
                };
        }

        List<QualificationTypeResource> list = (List<QualificationTypeResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (QualificationTypeResource qualificationTypeResource : list) {
                if ((qualificationTypeId != qualificationTypeResource.getQualificationTypeId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_QUALIFICATIONTYPERESOURCE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEID_QUALIFICATIONTYPEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationTypeResourceModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationTypeId);

                if (!pagination) {
                    list = (List<QualificationTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationTypeResource>(list);
                } else {
                    list = (List<QualificationTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification type resource
     * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource findByQualificationTypeId_First(
        long qualificationTypeId, OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeResourceException, SystemException {
        QualificationTypeResource qualificationTypeResource = fetchByQualificationTypeId_First(qualificationTypeId,
                orderByComparator);

        if (qualificationTypeResource != null) {
            return qualificationTypeResource;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationTypeId=");
        msg.append(qualificationTypeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationTypeResourceException(msg.toString());
    }

    /**
     * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource fetchByQualificationTypeId_First(
        long qualificationTypeId, OrderByComparator orderByComparator)
        throws SystemException {
        List<QualificationTypeResource> list = findByQualificationTypeId(qualificationTypeId,
                0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification type resource
     * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource findByQualificationTypeId_Last(
        long qualificationTypeId, OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeResourceException, SystemException {
        QualificationTypeResource qualificationTypeResource = fetchByQualificationTypeId_Last(qualificationTypeId,
                orderByComparator);

        if (qualificationTypeResource != null) {
            return qualificationTypeResource;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationTypeId=");
        msg.append(qualificationTypeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationTypeResourceException(msg.toString());
    }

    /**
     * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource fetchByQualificationTypeId_Last(
        long qualificationTypeId, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByQualificationTypeId(qualificationTypeId);

        if (count == 0) {
            return null;
        }

        List<QualificationTypeResource> list = findByQualificationTypeId(qualificationTypeId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualification type resources before and after the current qualification type resource in the ordered set where qualificationTypeId = &#63;.
     *
     * @param qualificationTypeResourceId the primary key of the current qualification type resource
     * @param qualificationTypeId the qualification type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification type resource
     * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource[] findByQualificationTypeId_PrevAndNext(
        long qualificationTypeResourceId, long qualificationTypeId,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeResourceException, SystemException {
        QualificationTypeResource qualificationTypeResource = findByPrimaryKey(qualificationTypeResourceId);

        Session session = null;

        try {
            session = openSession();

            QualificationTypeResource[] array = new QualificationTypeResourceImpl[3];

            array[0] = getByQualificationTypeId_PrevAndNext(session,
                    qualificationTypeResource, qualificationTypeId,
                    orderByComparator, true);

            array[1] = qualificationTypeResource;

            array[2] = getByQualificationTypeId_PrevAndNext(session,
                    qualificationTypeResource, qualificationTypeId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected QualificationTypeResource getByQualificationTypeId_PrevAndNext(
        Session session, QualificationTypeResource qualificationTypeResource,
        long qualificationTypeId, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATIONTYPERESOURCE_WHERE);

        query.append(_FINDER_COLUMN_QUALIFICATIONTYPEID_QUALIFICATIONTYPEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationTypeResourceModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(qualificationTypeId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualificationTypeResource);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<QualificationTypeResource> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualification type resources where qualificationTypeId = &#63; from the database.
     *
     * @param qualificationTypeId the qualification type ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByQualificationTypeId(long qualificationTypeId)
        throws SystemException {
        for (QualificationTypeResource qualificationTypeResource : findByQualificationTypeId(
                qualificationTypeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(qualificationTypeResource);
        }
    }

    /**
     * Returns the number of qualification type resources where qualificationTypeId = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @return the number of matching qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByQualificationTypeId(long qualificationTypeId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEID;

        Object[] finderArgs = new Object[] { qualificationTypeId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_QUALIFICATIONTYPERESOURCE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEID_QUALIFICATIONTYPEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationTypeId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the qualification type resources where qualificationTypeId = &#63; and country = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @return the matching qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationTypeResource> findByQualificationTypeIdAndCountry(
        long qualificationTypeId, String country) throws SystemException {
        return findByQualificationTypeIdAndCountry(qualificationTypeId,
            country, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification type resources where qualificationTypeId = &#63; and country = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @param start the lower bound of the range of qualification type resources
     * @param end the upper bound of the range of qualification type resources (not inclusive)
     * @return the range of matching qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationTypeResource> findByQualificationTypeIdAndCountry(
        long qualificationTypeId, String country, int start, int end)
        throws SystemException {
        return findByQualificationTypeIdAndCountry(qualificationTypeId,
            country, start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification type resources where qualificationTypeId = &#63; and country = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @param start the lower bound of the range of qualification type resources
     * @param end the upper bound of the range of qualification type resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationTypeResource> findByQualificationTypeIdAndCountry(
        long qualificationTypeId, String country, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDCOUNTRY;
            finderArgs = new Object[] { qualificationTypeId, country };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDCOUNTRY;
            finderArgs = new Object[] {
                    qualificationTypeId, country,
                    
                    start, end, orderByComparator
                };
        }

        List<QualificationTypeResource> list = (List<QualificationTypeResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (QualificationTypeResource qualificationTypeResource : list) {
                if ((qualificationTypeId != qualificationTypeResource.getQualificationTypeId()) ||
                        !Validator.equals(country,
                            qualificationTypeResource.getCountry())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_QUALIFICATIONTYPERESOURCE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDCOUNTRY_QUALIFICATIONTYPEID_2);

            boolean bindCountry = false;

            if (country == null) {
                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDCOUNTRY_COUNTRY_1);
            } else if (country.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDCOUNTRY_COUNTRY_3);
            } else {
                bindCountry = true;

                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDCOUNTRY_COUNTRY_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationTypeResourceModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationTypeId);

                if (bindCountry) {
                    qPos.add(country.toLowerCase());
                }

                if (!pagination) {
                    list = (List<QualificationTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationTypeResource>(list);
                } else {
                    list = (List<QualificationTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification type resource
     * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource findByQualificationTypeIdAndCountry_First(
        long qualificationTypeId, String country,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeResourceException, SystemException {
        QualificationTypeResource qualificationTypeResource = fetchByQualificationTypeIdAndCountry_First(qualificationTypeId,
                country, orderByComparator);

        if (qualificationTypeResource != null) {
            return qualificationTypeResource;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationTypeId=");
        msg.append(qualificationTypeId);

        msg.append(", country=");
        msg.append(country);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationTypeResourceException(msg.toString());
    }

    /**
     * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource fetchByQualificationTypeIdAndCountry_First(
        long qualificationTypeId, String country,
        OrderByComparator orderByComparator) throws SystemException {
        List<QualificationTypeResource> list = findByQualificationTypeIdAndCountry(qualificationTypeId,
                country, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification type resource
     * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource findByQualificationTypeIdAndCountry_Last(
        long qualificationTypeId, String country,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeResourceException, SystemException {
        QualificationTypeResource qualificationTypeResource = fetchByQualificationTypeIdAndCountry_Last(qualificationTypeId,
                country, orderByComparator);

        if (qualificationTypeResource != null) {
            return qualificationTypeResource;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationTypeId=");
        msg.append(qualificationTypeId);

        msg.append(", country=");
        msg.append(country);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationTypeResourceException(msg.toString());
    }

    /**
     * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource fetchByQualificationTypeIdAndCountry_Last(
        long qualificationTypeId, String country,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByQualificationTypeIdAndCountry(qualificationTypeId,
                country);

        if (count == 0) {
            return null;
        }

        List<QualificationTypeResource> list = findByQualificationTypeIdAndCountry(qualificationTypeId,
                country, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualification type resources before and after the current qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63;.
     *
     * @param qualificationTypeResourceId the primary key of the current qualification type resource
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification type resource
     * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource[] findByQualificationTypeIdAndCountry_PrevAndNext(
        long qualificationTypeResourceId, long qualificationTypeId,
        String country, OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeResourceException, SystemException {
        QualificationTypeResource qualificationTypeResource = findByPrimaryKey(qualificationTypeResourceId);

        Session session = null;

        try {
            session = openSession();

            QualificationTypeResource[] array = new QualificationTypeResourceImpl[3];

            array[0] = getByQualificationTypeIdAndCountry_PrevAndNext(session,
                    qualificationTypeResource, qualificationTypeId, country,
                    orderByComparator, true);

            array[1] = qualificationTypeResource;

            array[2] = getByQualificationTypeIdAndCountry_PrevAndNext(session,
                    qualificationTypeResource, qualificationTypeId, country,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected QualificationTypeResource getByQualificationTypeIdAndCountry_PrevAndNext(
        Session session, QualificationTypeResource qualificationTypeResource,
        long qualificationTypeId, String country,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATIONTYPERESOURCE_WHERE);

        query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDCOUNTRY_QUALIFICATIONTYPEID_2);

        boolean bindCountry = false;

        if (country == null) {
            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDCOUNTRY_COUNTRY_1);
        } else if (country.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDCOUNTRY_COUNTRY_3);
        } else {
            bindCountry = true;

            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDCOUNTRY_COUNTRY_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationTypeResourceModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(qualificationTypeId);

        if (bindCountry) {
            qPos.add(country.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualificationTypeResource);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<QualificationTypeResource> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualification type resources where qualificationTypeId = &#63; and country = &#63; from the database.
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByQualificationTypeIdAndCountry(
        long qualificationTypeId, String country) throws SystemException {
        for (QualificationTypeResource qualificationTypeResource : findByQualificationTypeIdAndCountry(
                qualificationTypeId, country, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(qualificationTypeResource);
        }
    }

    /**
     * Returns the number of qualification type resources where qualificationTypeId = &#63; and country = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @return the number of matching qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByQualificationTypeIdAndCountry(long qualificationTypeId,
        String country) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEIDANDCOUNTRY;

        Object[] finderArgs = new Object[] { qualificationTypeId, country };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_QUALIFICATIONTYPERESOURCE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDCOUNTRY_QUALIFICATIONTYPEID_2);

            boolean bindCountry = false;

            if (country == null) {
                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDCOUNTRY_COUNTRY_1);
            } else if (country.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDCOUNTRY_COUNTRY_3);
            } else {
                bindCountry = true;

                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDCOUNTRY_COUNTRY_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationTypeId);

                if (bindCountry) {
                    qPos.add(country.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the qualification type resources where qualificationTypeId = &#63; and language = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param language the language
     * @return the matching qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationTypeResource> findByQualificationTypeIdAndLanguage(
        long qualificationTypeId, String language) throws SystemException {
        return findByQualificationTypeIdAndLanguage(qualificationTypeId,
            language, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification type resources where qualificationTypeId = &#63; and language = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationTypeId the qualification type ID
     * @param language the language
     * @param start the lower bound of the range of qualification type resources
     * @param end the upper bound of the range of qualification type resources (not inclusive)
     * @return the range of matching qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationTypeResource> findByQualificationTypeIdAndLanguage(
        long qualificationTypeId, String language, int start, int end)
        throws SystemException {
        return findByQualificationTypeIdAndLanguage(qualificationTypeId,
            language, start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification type resources where qualificationTypeId = &#63; and language = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationTypeId the qualification type ID
     * @param language the language
     * @param start the lower bound of the range of qualification type resources
     * @param end the upper bound of the range of qualification type resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationTypeResource> findByQualificationTypeIdAndLanguage(
        long qualificationTypeId, String language, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDLANGUAGE;
            finderArgs = new Object[] { qualificationTypeId, language };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDLANGUAGE;
            finderArgs = new Object[] {
                    qualificationTypeId, language,
                    
                    start, end, orderByComparator
                };
        }

        List<QualificationTypeResource> list = (List<QualificationTypeResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (QualificationTypeResource qualificationTypeResource : list) {
                if ((qualificationTypeId != qualificationTypeResource.getQualificationTypeId()) ||
                        !Validator.equals(language,
                            qualificationTypeResource.getLanguage())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_QUALIFICATIONTYPERESOURCE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLANGUAGE_QUALIFICATIONTYPEID_2);

            boolean bindLanguage = false;

            if (language == null) {
                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLANGUAGE_LANGUAGE_1);
            } else if (language.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLANGUAGE_LANGUAGE_3);
            } else {
                bindLanguage = true;

                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLANGUAGE_LANGUAGE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationTypeResourceModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationTypeId);

                if (bindLanguage) {
                    qPos.add(language.toLowerCase());
                }

                if (!pagination) {
                    list = (List<QualificationTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationTypeResource>(list);
                } else {
                    list = (List<QualificationTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and language = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification type resource
     * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource findByQualificationTypeIdAndLanguage_First(
        long qualificationTypeId, String language,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeResourceException, SystemException {
        QualificationTypeResource qualificationTypeResource = fetchByQualificationTypeIdAndLanguage_First(qualificationTypeId,
                language, orderByComparator);

        if (qualificationTypeResource != null) {
            return qualificationTypeResource;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationTypeId=");
        msg.append(qualificationTypeId);

        msg.append(", language=");
        msg.append(language);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationTypeResourceException(msg.toString());
    }

    /**
     * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and language = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource fetchByQualificationTypeIdAndLanguage_First(
        long qualificationTypeId, String language,
        OrderByComparator orderByComparator) throws SystemException {
        List<QualificationTypeResource> list = findByQualificationTypeIdAndLanguage(qualificationTypeId,
                language, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and language = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification type resource
     * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource findByQualificationTypeIdAndLanguage_Last(
        long qualificationTypeId, String language,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeResourceException, SystemException {
        QualificationTypeResource qualificationTypeResource = fetchByQualificationTypeIdAndLanguage_Last(qualificationTypeId,
                language, orderByComparator);

        if (qualificationTypeResource != null) {
            return qualificationTypeResource;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationTypeId=");
        msg.append(qualificationTypeId);

        msg.append(", language=");
        msg.append(language);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationTypeResourceException(msg.toString());
    }

    /**
     * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and language = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource fetchByQualificationTypeIdAndLanguage_Last(
        long qualificationTypeId, String language,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByQualificationTypeIdAndLanguage(qualificationTypeId,
                language);

        if (count == 0) {
            return null;
        }

        List<QualificationTypeResource> list = findByQualificationTypeIdAndLanguage(qualificationTypeId,
                language, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualification type resources before and after the current qualification type resource in the ordered set where qualificationTypeId = &#63; and language = &#63;.
     *
     * @param qualificationTypeResourceId the primary key of the current qualification type resource
     * @param qualificationTypeId the qualification type ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification type resource
     * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource[] findByQualificationTypeIdAndLanguage_PrevAndNext(
        long qualificationTypeResourceId, long qualificationTypeId,
        String language, OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeResourceException, SystemException {
        QualificationTypeResource qualificationTypeResource = findByPrimaryKey(qualificationTypeResourceId);

        Session session = null;

        try {
            session = openSession();

            QualificationTypeResource[] array = new QualificationTypeResourceImpl[3];

            array[0] = getByQualificationTypeIdAndLanguage_PrevAndNext(session,
                    qualificationTypeResource, qualificationTypeId, language,
                    orderByComparator, true);

            array[1] = qualificationTypeResource;

            array[2] = getByQualificationTypeIdAndLanguage_PrevAndNext(session,
                    qualificationTypeResource, qualificationTypeId, language,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected QualificationTypeResource getByQualificationTypeIdAndLanguage_PrevAndNext(
        Session session, QualificationTypeResource qualificationTypeResource,
        long qualificationTypeId, String language,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATIONTYPERESOURCE_WHERE);

        query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLANGUAGE_QUALIFICATIONTYPEID_2);

        boolean bindLanguage = false;

        if (language == null) {
            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLANGUAGE_LANGUAGE_1);
        } else if (language.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLANGUAGE_LANGUAGE_3);
        } else {
            bindLanguage = true;

            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLANGUAGE_LANGUAGE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationTypeResourceModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(qualificationTypeId);

        if (bindLanguage) {
            qPos.add(language.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualificationTypeResource);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<QualificationTypeResource> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualification type resources where qualificationTypeId = &#63; and language = &#63; from the database.
     *
     * @param qualificationTypeId the qualification type ID
     * @param language the language
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByQualificationTypeIdAndLanguage(
        long qualificationTypeId, String language) throws SystemException {
        for (QualificationTypeResource qualificationTypeResource : findByQualificationTypeIdAndLanguage(
                qualificationTypeId, language, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(qualificationTypeResource);
        }
    }

    /**
     * Returns the number of qualification type resources where qualificationTypeId = &#63; and language = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param language the language
     * @return the number of matching qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByQualificationTypeIdAndLanguage(long qualificationTypeId,
        String language) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEIDANDLANGUAGE;

        Object[] finderArgs = new Object[] { qualificationTypeId, language };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_QUALIFICATIONTYPERESOURCE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLANGUAGE_QUALIFICATIONTYPEID_2);

            boolean bindLanguage = false;

            if (language == null) {
                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLANGUAGE_LANGUAGE_1);
            } else if (language.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLANGUAGE_LANGUAGE_3);
            } else {
                bindLanguage = true;

                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLANGUAGE_LANGUAGE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationTypeId);

                if (bindLanguage) {
                    qPos.add(language.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the qualification type resources where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @param language the language
     * @return the matching qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationTypeResource> findByQualificationTypeIdAndLocale(
        long qualificationTypeId, String country, String language)
        throws SystemException {
        return findByQualificationTypeIdAndLocale(qualificationTypeId, country,
            language, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification type resources where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @param language the language
     * @param start the lower bound of the range of qualification type resources
     * @param end the upper bound of the range of qualification type resources (not inclusive)
     * @return the range of matching qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationTypeResource> findByQualificationTypeIdAndLocale(
        long qualificationTypeId, String country, String language, int start,
        int end) throws SystemException {
        return findByQualificationTypeIdAndLocale(qualificationTypeId, country,
            language, start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification type resources where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @param language the language
     * @param start the lower bound of the range of qualification type resources
     * @param end the upper bound of the range of qualification type resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationTypeResource> findByQualificationTypeIdAndLocale(
        long qualificationTypeId, String country, String language, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDLOCALE;
            finderArgs = new Object[] { qualificationTypeId, country, language };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDLOCALE;
            finderArgs = new Object[] {
                    qualificationTypeId, country, language,
                    
                    start, end, orderByComparator
                };
        }

        List<QualificationTypeResource> list = (List<QualificationTypeResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (QualificationTypeResource qualificationTypeResource : list) {
                if ((qualificationTypeId != qualificationTypeResource.getQualificationTypeId()) ||
                        !Validator.equals(country,
                            qualificationTypeResource.getCountry()) ||
                        !Validator.equals(language,
                            qualificationTypeResource.getLanguage())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(5 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(5);
            }

            query.append(_SQL_SELECT_QUALIFICATIONTYPERESOURCE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_QUALIFICATIONTYPEID_2);

            boolean bindCountry = false;

            if (country == null) {
                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_COUNTRY_1);
            } else if (country.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_COUNTRY_3);
            } else {
                bindCountry = true;

                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_COUNTRY_2);
            }

            boolean bindLanguage = false;

            if (language == null) {
                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_LANGUAGE_1);
            } else if (language.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_LANGUAGE_3);
            } else {
                bindLanguage = true;

                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_LANGUAGE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(QualificationTypeResourceModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationTypeId);

                if (bindCountry) {
                    qPos.add(country.toLowerCase());
                }

                if (bindLanguage) {
                    qPos.add(language.toLowerCase());
                }

                if (!pagination) {
                    list = (List<QualificationTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationTypeResource>(list);
                } else {
                    list = (List<QualificationTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification type resource
     * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource findByQualificationTypeIdAndLocale_First(
        long qualificationTypeId, String country, String language,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeResourceException, SystemException {
        QualificationTypeResource qualificationTypeResource = fetchByQualificationTypeIdAndLocale_First(qualificationTypeId,
                country, language, orderByComparator);

        if (qualificationTypeResource != null) {
            return qualificationTypeResource;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationTypeId=");
        msg.append(qualificationTypeId);

        msg.append(", country=");
        msg.append(country);

        msg.append(", language=");
        msg.append(language);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationTypeResourceException(msg.toString());
    }

    /**
     * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource fetchByQualificationTypeIdAndLocale_First(
        long qualificationTypeId, String country, String language,
        OrderByComparator orderByComparator) throws SystemException {
        List<QualificationTypeResource> list = findByQualificationTypeIdAndLocale(qualificationTypeId,
                country, language, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification type resource
     * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource findByQualificationTypeIdAndLocale_Last(
        long qualificationTypeId, String country, String language,
        OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeResourceException, SystemException {
        QualificationTypeResource qualificationTypeResource = fetchByQualificationTypeIdAndLocale_Last(qualificationTypeId,
                country, language, orderByComparator);

        if (qualificationTypeResource != null) {
            return qualificationTypeResource;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("qualificationTypeId=");
        msg.append(qualificationTypeId);

        msg.append(", country=");
        msg.append(country);

        msg.append(", language=");
        msg.append(language);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchQualificationTypeResourceException(msg.toString());
    }

    /**
     * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource fetchByQualificationTypeIdAndLocale_Last(
        long qualificationTypeId, String country, String language,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByQualificationTypeIdAndLocale(qualificationTypeId,
                country, language);

        if (count == 0) {
            return null;
        }

        List<QualificationTypeResource> list = findByQualificationTypeIdAndLocale(qualificationTypeId,
                country, language, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the qualification type resources before and after the current qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * @param qualificationTypeResourceId the primary key of the current qualification type resource
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next qualification type resource
     * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource[] findByQualificationTypeIdAndLocale_PrevAndNext(
        long qualificationTypeResourceId, long qualificationTypeId,
        String country, String language, OrderByComparator orderByComparator)
        throws NoSuchQualificationTypeResourceException, SystemException {
        QualificationTypeResource qualificationTypeResource = findByPrimaryKey(qualificationTypeResourceId);

        Session session = null;

        try {
            session = openSession();

            QualificationTypeResource[] array = new QualificationTypeResourceImpl[3];

            array[0] = getByQualificationTypeIdAndLocale_PrevAndNext(session,
                    qualificationTypeResource, qualificationTypeId, country,
                    language, orderByComparator, true);

            array[1] = qualificationTypeResource;

            array[2] = getByQualificationTypeIdAndLocale_PrevAndNext(session,
                    qualificationTypeResource, qualificationTypeId, country,
                    language, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected QualificationTypeResource getByQualificationTypeIdAndLocale_PrevAndNext(
        Session session, QualificationTypeResource qualificationTypeResource,
        long qualificationTypeId, String country, String language,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_QUALIFICATIONTYPERESOURCE_WHERE);

        query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_QUALIFICATIONTYPEID_2);

        boolean bindCountry = false;

        if (country == null) {
            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_COUNTRY_1);
        } else if (country.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_COUNTRY_3);
        } else {
            bindCountry = true;

            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_COUNTRY_2);
        }

        boolean bindLanguage = false;

        if (language == null) {
            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_LANGUAGE_1);
        } else if (language.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_LANGUAGE_3);
        } else {
            bindLanguage = true;

            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_LANGUAGE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(QualificationTypeResourceModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(qualificationTypeId);

        if (bindCountry) {
            qPos.add(country.toLowerCase());
        }

        if (bindLanguage) {
            qPos.add(language.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(qualificationTypeResource);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<QualificationTypeResource> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the qualification type resources where qualificationTypeId = &#63; and country = &#63; and language = &#63; from the database.
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @param language the language
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByQualificationTypeIdAndLocale(long qualificationTypeId,
        String country, String language) throws SystemException {
        for (QualificationTypeResource qualificationTypeResource : findByQualificationTypeIdAndLocale(
                qualificationTypeId, country, language, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(qualificationTypeResource);
        }
    }

    /**
     * Returns the number of qualification type resources where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * @param qualificationTypeId the qualification type ID
     * @param country the country
     * @param language the language
     * @return the number of matching qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByQualificationTypeIdAndLocale(long qualificationTypeId,
        String country, String language) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEIDANDLOCALE;

        Object[] finderArgs = new Object[] {
                qualificationTypeId, country, language
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_COUNT_QUALIFICATIONTYPERESOURCE_WHERE);

            query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_QUALIFICATIONTYPEID_2);

            boolean bindCountry = false;

            if (country == null) {
                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_COUNTRY_1);
            } else if (country.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_COUNTRY_3);
            } else {
                bindCountry = true;

                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_COUNTRY_2);
            }

            boolean bindLanguage = false;

            if (language == null) {
                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_LANGUAGE_1);
            } else if (language.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_LANGUAGE_3);
            } else {
                bindLanguage = true;

                query.append(_FINDER_COLUMN_QUALIFICATIONTYPEIDANDLOCALE_LANGUAGE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(qualificationTypeId);

                if (bindCountry) {
                    qPos.add(country.toLowerCase());
                }

                if (bindLanguage) {
                    qPos.add(language.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the qualification type resource in the entity cache if it is enabled.
     *
     * @param qualificationTypeResource the qualification type resource
     */
    @Override
    public void cacheResult(QualificationTypeResource qualificationTypeResource) {
        EntityCacheUtil.putResult(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceImpl.class,
            qualificationTypeResource.getPrimaryKey(), qualificationTypeResource);

        qualificationTypeResource.resetOriginalValues();
    }

    /**
     * Caches the qualification type resources in the entity cache if it is enabled.
     *
     * @param qualificationTypeResources the qualification type resources
     */
    @Override
    public void cacheResult(
        List<QualificationTypeResource> qualificationTypeResources) {
        for (QualificationTypeResource qualificationTypeResource : qualificationTypeResources) {
            if (EntityCacheUtil.getResult(
                        QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
                        QualificationTypeResourceImpl.class,
                        qualificationTypeResource.getPrimaryKey()) == null) {
                cacheResult(qualificationTypeResource);
            } else {
                qualificationTypeResource.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all qualification type resources.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(QualificationTypeResourceImpl.class.getName());
        }

        EntityCacheUtil.clearCache(QualificationTypeResourceImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the qualification type resource.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(QualificationTypeResource qualificationTypeResource) {
        EntityCacheUtil.removeResult(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceImpl.class,
            qualificationTypeResource.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(
        List<QualificationTypeResource> qualificationTypeResources) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (QualificationTypeResource qualificationTypeResource : qualificationTypeResources) {
            EntityCacheUtil.removeResult(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
                QualificationTypeResourceImpl.class,
                qualificationTypeResource.getPrimaryKey());
        }
    }

    /**
     * Creates a new qualification type resource with the primary key. Does not add the qualification type resource to the database.
     *
     * @param qualificationTypeResourceId the primary key for the new qualification type resource
     * @return the new qualification type resource
     */
    @Override
    public QualificationTypeResource create(long qualificationTypeResourceId) {
        QualificationTypeResource qualificationTypeResource = new QualificationTypeResourceImpl();

        qualificationTypeResource.setNew(true);
        qualificationTypeResource.setPrimaryKey(qualificationTypeResourceId);

        return qualificationTypeResource;
    }

    /**
     * Removes the qualification type resource with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param qualificationTypeResourceId the primary key of the qualification type resource
     * @return the qualification type resource that was removed
     * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource remove(long qualificationTypeResourceId)
        throws NoSuchQualificationTypeResourceException, SystemException {
        return remove((Serializable) qualificationTypeResourceId);
    }

    /**
     * Removes the qualification type resource with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the qualification type resource
     * @return the qualification type resource that was removed
     * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource remove(Serializable primaryKey)
        throws NoSuchQualificationTypeResourceException, SystemException {
        Session session = null;

        try {
            session = openSession();

            QualificationTypeResource qualificationTypeResource = (QualificationTypeResource) session.get(QualificationTypeResourceImpl.class,
                    primaryKey);

            if (qualificationTypeResource == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchQualificationTypeResourceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(qualificationTypeResource);
        } catch (NoSuchQualificationTypeResourceException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected QualificationTypeResource removeImpl(
        QualificationTypeResource qualificationTypeResource)
        throws SystemException {
        qualificationTypeResource = toUnwrappedModel(qualificationTypeResource);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(qualificationTypeResource)) {
                qualificationTypeResource = (QualificationTypeResource) session.get(QualificationTypeResourceImpl.class,
                        qualificationTypeResource.getPrimaryKeyObj());
            }

            if (qualificationTypeResource != null) {
                session.delete(qualificationTypeResource);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (qualificationTypeResource != null) {
            clearCache(qualificationTypeResource);
        }

        return qualificationTypeResource;
    }

    @Override
    public QualificationTypeResource updateImpl(
        de.humance.eco.profile.model.QualificationTypeResource qualificationTypeResource)
        throws SystemException {
        qualificationTypeResource = toUnwrappedModel(qualificationTypeResource);

        boolean isNew = qualificationTypeResource.isNew();

        QualificationTypeResourceModelImpl qualificationTypeResourceModelImpl = (QualificationTypeResourceModelImpl) qualificationTypeResource;

        Session session = null;

        try {
            session = openSession();

            if (qualificationTypeResource.isNew()) {
                session.save(qualificationTypeResource);

                qualificationTypeResource.setNew(false);
            } else {
                session.merge(qualificationTypeResource);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew ||
                !QualificationTypeResourceModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((qualificationTypeResourceModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        qualificationTypeResourceModelImpl.getOriginalQualificationTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEID,
                    args);

                args = new Object[] {
                        qualificationTypeResourceModelImpl.getQualificationTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEID,
                    args);
            }

            if ((qualificationTypeResourceModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDCOUNTRY.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        qualificationTypeResourceModelImpl.getOriginalQualificationTypeId(),
                        qualificationTypeResourceModelImpl.getOriginalCountry()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEIDANDCOUNTRY,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDCOUNTRY,
                    args);

                args = new Object[] {
                        qualificationTypeResourceModelImpl.getQualificationTypeId(),
                        qualificationTypeResourceModelImpl.getCountry()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEIDANDCOUNTRY,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDCOUNTRY,
                    args);
            }

            if ((qualificationTypeResourceModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDLANGUAGE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        qualificationTypeResourceModelImpl.getOriginalQualificationTypeId(),
                        qualificationTypeResourceModelImpl.getOriginalLanguage()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEIDANDLANGUAGE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDLANGUAGE,
                    args);

                args = new Object[] {
                        qualificationTypeResourceModelImpl.getQualificationTypeId(),
                        qualificationTypeResourceModelImpl.getLanguage()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEIDANDLANGUAGE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDLANGUAGE,
                    args);
            }

            if ((qualificationTypeResourceModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDLOCALE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        qualificationTypeResourceModelImpl.getOriginalQualificationTypeId(),
                        qualificationTypeResourceModelImpl.getOriginalCountry(),
                        qualificationTypeResourceModelImpl.getOriginalLanguage()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEIDANDLOCALE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDLOCALE,
                    args);

                args = new Object[] {
                        qualificationTypeResourceModelImpl.getQualificationTypeId(),
                        qualificationTypeResourceModelImpl.getCountry(),
                        qualificationTypeResourceModelImpl.getLanguage()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_QUALIFICATIONTYPEIDANDLOCALE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUALIFICATIONTYPEIDANDLOCALE,
                    args);
            }
        }

        EntityCacheUtil.putResult(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            QualificationTypeResourceImpl.class,
            qualificationTypeResource.getPrimaryKey(), qualificationTypeResource);

        return qualificationTypeResource;
    }

    protected QualificationTypeResource toUnwrappedModel(
        QualificationTypeResource qualificationTypeResource) {
        if (qualificationTypeResource instanceof QualificationTypeResourceImpl) {
            return qualificationTypeResource;
        }

        QualificationTypeResourceImpl qualificationTypeResourceImpl = new QualificationTypeResourceImpl();

        qualificationTypeResourceImpl.setNew(qualificationTypeResource.isNew());
        qualificationTypeResourceImpl.setPrimaryKey(qualificationTypeResource.getPrimaryKey());

        qualificationTypeResourceImpl.setQualificationTypeResourceId(qualificationTypeResource.getQualificationTypeResourceId());
        qualificationTypeResourceImpl.setQualificationTypeId(qualificationTypeResource.getQualificationTypeId());
        qualificationTypeResourceImpl.setCountry(qualificationTypeResource.getCountry());
        qualificationTypeResourceImpl.setLanguage(qualificationTypeResource.getLanguage());
        qualificationTypeResourceImpl.setName(qualificationTypeResource.getName());
        qualificationTypeResourceImpl.setDescription(qualificationTypeResource.getDescription());
        qualificationTypeResourceImpl.setNote(qualificationTypeResource.getNote());
        qualificationTypeResourceImpl.setIconId(qualificationTypeResource.getIconId());

        return qualificationTypeResourceImpl;
    }

    /**
     * Returns the qualification type resource with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the qualification type resource
     * @return the qualification type resource
     * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource findByPrimaryKey(Serializable primaryKey)
        throws NoSuchQualificationTypeResourceException, SystemException {
        QualificationTypeResource qualificationTypeResource = fetchByPrimaryKey(primaryKey);

        if (qualificationTypeResource == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchQualificationTypeResourceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return qualificationTypeResource;
    }

    /**
     * Returns the qualification type resource with the primary key or throws a {@link de.humance.eco.profile.NoSuchQualificationTypeResourceException} if it could not be found.
     *
     * @param qualificationTypeResourceId the primary key of the qualification type resource
     * @return the qualification type resource
     * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource findByPrimaryKey(
        long qualificationTypeResourceId)
        throws NoSuchQualificationTypeResourceException, SystemException {
        return findByPrimaryKey((Serializable) qualificationTypeResourceId);
    }

    /**
     * Returns the qualification type resource with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the qualification type resource
     * @return the qualification type resource, or <code>null</code> if a qualification type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        QualificationTypeResource qualificationTypeResource = (QualificationTypeResource) EntityCacheUtil.getResult(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
                QualificationTypeResourceImpl.class, primaryKey);

        if (qualificationTypeResource == _nullQualificationTypeResource) {
            return null;
        }

        if (qualificationTypeResource == null) {
            Session session = null;

            try {
                session = openSession();

                qualificationTypeResource = (QualificationTypeResource) session.get(QualificationTypeResourceImpl.class,
                        primaryKey);

                if (qualificationTypeResource != null) {
                    cacheResult(qualificationTypeResource);
                } else {
                    EntityCacheUtil.putResult(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
                        QualificationTypeResourceImpl.class, primaryKey,
                        _nullQualificationTypeResource);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(QualificationTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
                    QualificationTypeResourceImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return qualificationTypeResource;
    }

    /**
     * Returns the qualification type resource with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param qualificationTypeResourceId the primary key of the qualification type resource
     * @return the qualification type resource, or <code>null</code> if a qualification type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public QualificationTypeResource fetchByPrimaryKey(
        long qualificationTypeResourceId) throws SystemException {
        return fetchByPrimaryKey((Serializable) qualificationTypeResourceId);
    }

    /**
     * Returns all the qualification type resources.
     *
     * @return the qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationTypeResource> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the qualification type resources.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of qualification type resources
     * @param end the upper bound of the range of qualification type resources (not inclusive)
     * @return the range of qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationTypeResource> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the qualification type resources.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of qualification type resources
     * @param end the upper bound of the range of qualification type resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<QualificationTypeResource> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<QualificationTypeResource> list = (List<QualificationTypeResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_QUALIFICATIONTYPERESOURCE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_QUALIFICATIONTYPERESOURCE;

                if (pagination) {
                    sql = sql.concat(QualificationTypeResourceModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<QualificationTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<QualificationTypeResource>(list);
                } else {
                    list = (List<QualificationTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the qualification type resources from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (QualificationTypeResource qualificationTypeResource : findAll()) {
            remove(qualificationTypeResource);
        }
    }

    /**
     * Returns the number of qualification type resources.
     *
     * @return the number of qualification type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_QUALIFICATIONTYPERESOURCE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the qualification type resource persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.de.humance.eco.profile.model.QualificationTypeResource")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<QualificationTypeResource>> listenersList = new ArrayList<ModelListener<QualificationTypeResource>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<QualificationTypeResource>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(QualificationTypeResourceImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
