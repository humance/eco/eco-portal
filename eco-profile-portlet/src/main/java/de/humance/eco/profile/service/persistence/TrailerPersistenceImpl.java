package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.humance.eco.profile.NoSuchTrailerException;
import de.humance.eco.profile.model.Trailer;
import de.humance.eco.profile.model.impl.TrailerImpl;
import de.humance.eco.profile.model.impl.TrailerModelImpl;
import de.humance.eco.profile.service.persistence.TrailerPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the trailer service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see TrailerPersistence
 * @see TrailerUtil
 * @generated
 */
public class TrailerPersistenceImpl extends BasePersistenceImpl<Trailer>
    implements TrailerPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link TrailerUtil} to access the trailer persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = TrailerImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DRIVERID = new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByDriverId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERID =
        new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByDriverId",
            new String[] { Long.class.getName() },
            TrailerModelImpl.DRIVERID_COLUMN_BITMASK |
            TrailerModelImpl.MANUFACTURERID_COLUMN_BITMASK |
            TrailerModelImpl.MODELNAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_DRIVERID = new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByDriverId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_DRIVERID_DRIVERID_2 = "trailer.driverId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DRIVERIDANDORGAID =
        new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByDriverIdAndOrgaId",
            new String[] {
                Long.class.getName(), Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDORGAID =
        new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByDriverIdAndOrgaId",
            new String[] { Long.class.getName(), Long.class.getName() },
            TrailerModelImpl.DRIVERID_COLUMN_BITMASK |
            TrailerModelImpl.ORGANIZATIONID_COLUMN_BITMASK |
            TrailerModelImpl.MANUFACTURERID_COLUMN_BITMASK |
            TrailerModelImpl.MODELNAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_DRIVERIDANDORGAID = new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByDriverIdAndOrgaId",
            new String[] { Long.class.getName(), Long.class.getName() });
    private static final String _FINDER_COLUMN_DRIVERIDANDORGAID_DRIVERID_2 = "trailer.driverId = ? AND ";
    private static final String _FINDER_COLUMN_DRIVERIDANDORGAID_ORGANIZATIONID_2 =
        "trailer.organizationId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TYPEID = new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTypeId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPEID =
        new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByTypeId",
            new String[] { Long.class.getName() },
            TrailerModelImpl.TYPEID_COLUMN_BITMASK |
            TrailerModelImpl.MANUFACTURERID_COLUMN_BITMASK |
            TrailerModelImpl.MODELNAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_TYPEID = new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTypeId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_TYPEID_TYPEID_2 = "trailer.typeId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DRIVERIDANDTYPEID =
        new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByDriverIdAndTypeId",
            new String[] {
                Long.class.getName(), Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDTYPEID =
        new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByDriverIdAndTypeId",
            new String[] { Long.class.getName(), Long.class.getName() },
            TrailerModelImpl.DRIVERID_COLUMN_BITMASK |
            TrailerModelImpl.TYPEID_COLUMN_BITMASK |
            TrailerModelImpl.MANUFACTURERID_COLUMN_BITMASK |
            TrailerModelImpl.MODELNAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_DRIVERIDANDTYPEID = new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByDriverIdAndTypeId",
            new String[] { Long.class.getName(), Long.class.getName() });
    private static final String _FINDER_COLUMN_DRIVERIDANDTYPEID_DRIVERID_2 = "trailer.driverId = ? AND ";
    private static final String _FINDER_COLUMN_DRIVERIDANDTYPEID_TYPEID_2 = "trailer.typeId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MANUFACTURERID =
        new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByManufacturerId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MANUFACTURERID =
        new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByManufacturerId",
            new String[] { Long.class.getName() },
            TrailerModelImpl.MANUFACTURERID_COLUMN_BITMASK |
            TrailerModelImpl.MODELNAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_MANUFACTURERID = new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByManufacturerId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_MANUFACTURERID_MANUFACTURERID_2 = "trailer.manufacturerId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MODELNAME =
        new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByModelName",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODELNAME =
        new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByModelName",
            new String[] { String.class.getName() },
            TrailerModelImpl.MODELNAME_COLUMN_BITMASK |
            TrailerModelImpl.MANUFACTURERID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_MODELNAME = new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByModelName",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_MODELNAME_MODELNAME_1 = "trailer.modelName IS NULL";
    private static final String _FINDER_COLUMN_MODELNAME_MODELNAME_2 = "lower(trailer.modelName) = ?";
    private static final String _FINDER_COLUMN_MODELNAME_MODELNAME_3 = "(trailer.modelName IS NULL OR trailer.modelName = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MODELNAMELIKE =
        new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByModelNameLike",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_MODELNAMELIKE =
        new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByModelNameLike",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_MODELNAMELIKE_MODELNAME_1 = "trailer.modelName LIKE NULL";
    private static final String _FINDER_COLUMN_MODELNAMELIKE_MODELNAME_2 = "lower(trailer.modelName) LIKE ?";
    private static final String _FINDER_COLUMN_MODELNAMELIKE_MODELNAME_3 = "(trailer.modelName IS NULL OR trailer.modelName LIKE '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LICENSEPLATE =
        new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByLicensePlate",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LICENSEPLATE =
        new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByLicensePlate",
            new String[] { String.class.getName() },
            TrailerModelImpl.LICENSEPLATE_COLUMN_BITMASK |
            TrailerModelImpl.MANUFACTURERID_COLUMN_BITMASK |
            TrailerModelImpl.MODELNAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_LICENSEPLATE = new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLicensePlate",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_1 = "trailer.licensePlate IS NULL";
    private static final String _FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_2 = "lower(trailer.licensePlate) = ?";
    private static final String _FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_3 = "(trailer.licensePlate IS NULL OR trailer.licensePlate = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LICENSEPLATELIKE =
        new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, TrailerImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByLicensePlateLike",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_LICENSEPLATELIKE =
        new FinderPath(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByLicensePlateLike",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_1 = "trailer.licensePlate LIKE NULL";
    private static final String _FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_2 = "lower(trailer.licensePlate) LIKE ?";
    private static final String _FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_3 = "(trailer.licensePlate IS NULL OR trailer.licensePlate LIKE '')";
    private static final String _SQL_SELECT_TRAILER = "SELECT trailer FROM Trailer trailer";
    private static final String _SQL_SELECT_TRAILER_WHERE = "SELECT trailer FROM Trailer trailer WHERE ";
    private static final String _SQL_COUNT_TRAILER = "SELECT COUNT(trailer) FROM Trailer trailer";
    private static final String _SQL_COUNT_TRAILER_WHERE = "SELECT COUNT(trailer) FROM Trailer trailer WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "trailer.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Trailer exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Trailer exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(TrailerPersistenceImpl.class);
    private static Trailer _nullTrailer = new TrailerImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Trailer> toCacheModel() {
                return _nullTrailerCacheModel;
            }
        };

    private static CacheModel<Trailer> _nullTrailerCacheModel = new CacheModel<Trailer>() {
            @Override
            public Trailer toEntityModel() {
                return _nullTrailer;
            }
        };

    public TrailerPersistenceImpl() {
        setModelClass(Trailer.class);
    }

    /**
     * Returns all the trailers where driverId = &#63;.
     *
     * @param driverId the driver ID
     * @return the matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByDriverId(long driverId)
        throws SystemException {
        return findByDriverId(driverId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the trailers where driverId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param driverId the driver ID
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @return the range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByDriverId(long driverId, int start, int end)
        throws SystemException {
        return findByDriverId(driverId, start, end, null);
    }

    /**
     * Returns an ordered range of all the trailers where driverId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param driverId the driver ID
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByDriverId(long driverId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERID;
            finderArgs = new Object[] { driverId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DRIVERID;
            finderArgs = new Object[] { driverId, start, end, orderByComparator };
        }

        List<Trailer> list = (List<Trailer>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Trailer trailer : list) {
                if ((driverId != trailer.getDriverId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_TRAILER_WHERE);

            query.append(_FINDER_COLUMN_DRIVERID_DRIVERID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TrailerModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(driverId);

                if (!pagination) {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Trailer>(list);
                } else {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first trailer in the ordered set where driverId = &#63;.
     *
     * @param driverId the driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByDriverId_First(long driverId,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByDriverId_First(driverId, orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("driverId=");
        msg.append(driverId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the first trailer in the ordered set where driverId = &#63;.
     *
     * @param driverId the driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByDriverId_First(long driverId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Trailer> list = findByDriverId(driverId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last trailer in the ordered set where driverId = &#63;.
     *
     * @param driverId the driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByDriverId_Last(long driverId,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByDriverId_Last(driverId, orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("driverId=");
        msg.append(driverId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the last trailer in the ordered set where driverId = &#63;.
     *
     * @param driverId the driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByDriverId_Last(long driverId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByDriverId(driverId);

        if (count == 0) {
            return null;
        }

        List<Trailer> list = findByDriverId(driverId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the trailers before and after the current trailer in the ordered set where driverId = &#63;.
     *
     * @param trailerId the primary key of the current trailer
     * @param driverId the driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer[] findByDriverId_PrevAndNext(long trailerId, long driverId,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = findByPrimaryKey(trailerId);

        Session session = null;

        try {
            session = openSession();

            Trailer[] array = new TrailerImpl[3];

            array[0] = getByDriverId_PrevAndNext(session, trailer, driverId,
                    orderByComparator, true);

            array[1] = trailer;

            array[2] = getByDriverId_PrevAndNext(session, trailer, driverId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Trailer getByDriverId_PrevAndNext(Session session,
        Trailer trailer, long driverId, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TRAILER_WHERE);

        query.append(_FINDER_COLUMN_DRIVERID_DRIVERID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TrailerModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(driverId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(trailer);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Trailer> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the trailers where driverId = &#63; from the database.
     *
     * @param driverId the driver ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByDriverId(long driverId) throws SystemException {
        for (Trailer trailer : findByDriverId(driverId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(trailer);
        }
    }

    /**
     * Returns the number of trailers where driverId = &#63;.
     *
     * @param driverId the driver ID
     * @return the number of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByDriverId(long driverId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_DRIVERID;

        Object[] finderArgs = new Object[] { driverId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_TRAILER_WHERE);

            query.append(_FINDER_COLUMN_DRIVERID_DRIVERID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(driverId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the trailers where driverId = &#63; and organizationId = &#63;.
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @return the matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByDriverIdAndOrgaId(long driverId,
        long organizationId) throws SystemException {
        return findByDriverIdAndOrgaId(driverId, organizationId,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the trailers where driverId = &#63; and organizationId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @return the range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByDriverIdAndOrgaId(long driverId,
        long organizationId, int start, int end) throws SystemException {
        return findByDriverIdAndOrgaId(driverId, organizationId, start, end,
            null);
    }

    /**
     * Returns an ordered range of all the trailers where driverId = &#63; and organizationId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByDriverIdAndOrgaId(long driverId,
        long organizationId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDORGAID;
            finderArgs = new Object[] { driverId, organizationId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DRIVERIDANDORGAID;
            finderArgs = new Object[] {
                    driverId, organizationId,
                    
                    start, end, orderByComparator
                };
        }

        List<Trailer> list = (List<Trailer>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Trailer trailer : list) {
                if ((driverId != trailer.getDriverId()) ||
                        (organizationId != trailer.getOrganizationId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_TRAILER_WHERE);

            query.append(_FINDER_COLUMN_DRIVERIDANDORGAID_DRIVERID_2);

            query.append(_FINDER_COLUMN_DRIVERIDANDORGAID_ORGANIZATIONID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TrailerModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(driverId);

                qPos.add(organizationId);

                if (!pagination) {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Trailer>(list);
                } else {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first trailer in the ordered set where driverId = &#63; and organizationId = &#63;.
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByDriverIdAndOrgaId_First(long driverId,
        long organizationId, OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByDriverIdAndOrgaId_First(driverId,
                organizationId, orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("driverId=");
        msg.append(driverId);

        msg.append(", organizationId=");
        msg.append(organizationId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the first trailer in the ordered set where driverId = &#63; and organizationId = &#63;.
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByDriverIdAndOrgaId_First(long driverId,
        long organizationId, OrderByComparator orderByComparator)
        throws SystemException {
        List<Trailer> list = findByDriverIdAndOrgaId(driverId, organizationId,
                0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last trailer in the ordered set where driverId = &#63; and organizationId = &#63;.
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByDriverIdAndOrgaId_Last(long driverId,
        long organizationId, OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByDriverIdAndOrgaId_Last(driverId,
                organizationId, orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("driverId=");
        msg.append(driverId);

        msg.append(", organizationId=");
        msg.append(organizationId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the last trailer in the ordered set where driverId = &#63; and organizationId = &#63;.
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByDriverIdAndOrgaId_Last(long driverId,
        long organizationId, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByDriverIdAndOrgaId(driverId, organizationId);

        if (count == 0) {
            return null;
        }

        List<Trailer> list = findByDriverIdAndOrgaId(driverId, organizationId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the trailers before and after the current trailer in the ordered set where driverId = &#63; and organizationId = &#63;.
     *
     * @param trailerId the primary key of the current trailer
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer[] findByDriverIdAndOrgaId_PrevAndNext(long trailerId,
        long driverId, long organizationId, OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = findByPrimaryKey(trailerId);

        Session session = null;

        try {
            session = openSession();

            Trailer[] array = new TrailerImpl[3];

            array[0] = getByDriverIdAndOrgaId_PrevAndNext(session, trailer,
                    driverId, organizationId, orderByComparator, true);

            array[1] = trailer;

            array[2] = getByDriverIdAndOrgaId_PrevAndNext(session, trailer,
                    driverId, organizationId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Trailer getByDriverIdAndOrgaId_PrevAndNext(Session session,
        Trailer trailer, long driverId, long organizationId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TRAILER_WHERE);

        query.append(_FINDER_COLUMN_DRIVERIDANDORGAID_DRIVERID_2);

        query.append(_FINDER_COLUMN_DRIVERIDANDORGAID_ORGANIZATIONID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TrailerModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(driverId);

        qPos.add(organizationId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(trailer);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Trailer> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the trailers where driverId = &#63; and organizationId = &#63; from the database.
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByDriverIdAndOrgaId(long driverId, long organizationId)
        throws SystemException {
        for (Trailer trailer : findByDriverIdAndOrgaId(driverId,
                organizationId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(trailer);
        }
    }

    /**
     * Returns the number of trailers where driverId = &#63; and organizationId = &#63;.
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @return the number of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByDriverIdAndOrgaId(long driverId, long organizationId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_DRIVERIDANDORGAID;

        Object[] finderArgs = new Object[] { driverId, organizationId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_TRAILER_WHERE);

            query.append(_FINDER_COLUMN_DRIVERIDANDORGAID_DRIVERID_2);

            query.append(_FINDER_COLUMN_DRIVERIDANDORGAID_ORGANIZATIONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(driverId);

                qPos.add(organizationId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the trailers where typeId = &#63;.
     *
     * @param typeId the type ID
     * @return the matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByTypeId(long typeId) throws SystemException {
        return findByTypeId(typeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the trailers where typeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param typeId the type ID
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @return the range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByTypeId(long typeId, int start, int end)
        throws SystemException {
        return findByTypeId(typeId, start, end, null);
    }

    /**
     * Returns an ordered range of all the trailers where typeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param typeId the type ID
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByTypeId(long typeId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPEID;
            finderArgs = new Object[] { typeId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TYPEID;
            finderArgs = new Object[] { typeId, start, end, orderByComparator };
        }

        List<Trailer> list = (List<Trailer>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Trailer trailer : list) {
                if ((typeId != trailer.getTypeId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_TRAILER_WHERE);

            query.append(_FINDER_COLUMN_TYPEID_TYPEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TrailerModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(typeId);

                if (!pagination) {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Trailer>(list);
                } else {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first trailer in the ordered set where typeId = &#63;.
     *
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByTypeId_First(long typeId,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByTypeId_First(typeId, orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("typeId=");
        msg.append(typeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the first trailer in the ordered set where typeId = &#63;.
     *
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByTypeId_First(long typeId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Trailer> list = findByTypeId(typeId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last trailer in the ordered set where typeId = &#63;.
     *
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByTypeId_Last(long typeId,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByTypeId_Last(typeId, orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("typeId=");
        msg.append(typeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the last trailer in the ordered set where typeId = &#63;.
     *
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByTypeId_Last(long typeId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByTypeId(typeId);

        if (count == 0) {
            return null;
        }

        List<Trailer> list = findByTypeId(typeId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the trailers before and after the current trailer in the ordered set where typeId = &#63;.
     *
     * @param trailerId the primary key of the current trailer
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer[] findByTypeId_PrevAndNext(long trailerId, long typeId,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = findByPrimaryKey(trailerId);

        Session session = null;

        try {
            session = openSession();

            Trailer[] array = new TrailerImpl[3];

            array[0] = getByTypeId_PrevAndNext(session, trailer, typeId,
                    orderByComparator, true);

            array[1] = trailer;

            array[2] = getByTypeId_PrevAndNext(session, trailer, typeId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Trailer getByTypeId_PrevAndNext(Session session, Trailer trailer,
        long typeId, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TRAILER_WHERE);

        query.append(_FINDER_COLUMN_TYPEID_TYPEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TrailerModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(typeId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(trailer);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Trailer> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the trailers where typeId = &#63; from the database.
     *
     * @param typeId the type ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByTypeId(long typeId) throws SystemException {
        for (Trailer trailer : findByTypeId(typeId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(trailer);
        }
    }

    /**
     * Returns the number of trailers where typeId = &#63;.
     *
     * @param typeId the type ID
     * @return the number of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByTypeId(long typeId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_TYPEID;

        Object[] finderArgs = new Object[] { typeId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_TRAILER_WHERE);

            query.append(_FINDER_COLUMN_TYPEID_TYPEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(typeId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the trailers where driverId = &#63; and typeId = &#63;.
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @return the matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByDriverIdAndTypeId(long driverId, long typeId)
        throws SystemException {
        return findByDriverIdAndTypeId(driverId, typeId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the trailers where driverId = &#63; and typeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @return the range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByDriverIdAndTypeId(long driverId, long typeId,
        int start, int end) throws SystemException {
        return findByDriverIdAndTypeId(driverId, typeId, start, end, null);
    }

    /**
     * Returns an ordered range of all the trailers where driverId = &#63; and typeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByDriverIdAndTypeId(long driverId, long typeId,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDTYPEID;
            finderArgs = new Object[] { driverId, typeId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DRIVERIDANDTYPEID;
            finderArgs = new Object[] {
                    driverId, typeId,
                    
                    start, end, orderByComparator
                };
        }

        List<Trailer> list = (List<Trailer>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Trailer trailer : list) {
                if ((driverId != trailer.getDriverId()) ||
                        (typeId != trailer.getTypeId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_TRAILER_WHERE);

            query.append(_FINDER_COLUMN_DRIVERIDANDTYPEID_DRIVERID_2);

            query.append(_FINDER_COLUMN_DRIVERIDANDTYPEID_TYPEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TrailerModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(driverId);

                qPos.add(typeId);

                if (!pagination) {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Trailer>(list);
                } else {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first trailer in the ordered set where driverId = &#63; and typeId = &#63;.
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByDriverIdAndTypeId_First(long driverId, long typeId,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByDriverIdAndTypeId_First(driverId, typeId,
                orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("driverId=");
        msg.append(driverId);

        msg.append(", typeId=");
        msg.append(typeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the first trailer in the ordered set where driverId = &#63; and typeId = &#63;.
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByDriverIdAndTypeId_First(long driverId, long typeId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Trailer> list = findByDriverIdAndTypeId(driverId, typeId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last trailer in the ordered set where driverId = &#63; and typeId = &#63;.
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByDriverIdAndTypeId_Last(long driverId, long typeId,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByDriverIdAndTypeId_Last(driverId, typeId,
                orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("driverId=");
        msg.append(driverId);

        msg.append(", typeId=");
        msg.append(typeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the last trailer in the ordered set where driverId = &#63; and typeId = &#63;.
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByDriverIdAndTypeId_Last(long driverId, long typeId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByDriverIdAndTypeId(driverId, typeId);

        if (count == 0) {
            return null;
        }

        List<Trailer> list = findByDriverIdAndTypeId(driverId, typeId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the trailers before and after the current trailer in the ordered set where driverId = &#63; and typeId = &#63;.
     *
     * @param trailerId the primary key of the current trailer
     * @param driverId the driver ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer[] findByDriverIdAndTypeId_PrevAndNext(long trailerId,
        long driverId, long typeId, OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = findByPrimaryKey(trailerId);

        Session session = null;

        try {
            session = openSession();

            Trailer[] array = new TrailerImpl[3];

            array[0] = getByDriverIdAndTypeId_PrevAndNext(session, trailer,
                    driverId, typeId, orderByComparator, true);

            array[1] = trailer;

            array[2] = getByDriverIdAndTypeId_PrevAndNext(session, trailer,
                    driverId, typeId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Trailer getByDriverIdAndTypeId_PrevAndNext(Session session,
        Trailer trailer, long driverId, long typeId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TRAILER_WHERE);

        query.append(_FINDER_COLUMN_DRIVERIDANDTYPEID_DRIVERID_2);

        query.append(_FINDER_COLUMN_DRIVERIDANDTYPEID_TYPEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TrailerModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(driverId);

        qPos.add(typeId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(trailer);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Trailer> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the trailers where driverId = &#63; and typeId = &#63; from the database.
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByDriverIdAndTypeId(long driverId, long typeId)
        throws SystemException {
        for (Trailer trailer : findByDriverIdAndTypeId(driverId, typeId,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(trailer);
        }
    }

    /**
     * Returns the number of trailers where driverId = &#63; and typeId = &#63;.
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @return the number of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByDriverIdAndTypeId(long driverId, long typeId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_DRIVERIDANDTYPEID;

        Object[] finderArgs = new Object[] { driverId, typeId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_TRAILER_WHERE);

            query.append(_FINDER_COLUMN_DRIVERIDANDTYPEID_DRIVERID_2);

            query.append(_FINDER_COLUMN_DRIVERIDANDTYPEID_TYPEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(driverId);

                qPos.add(typeId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the trailers where manufacturerId = &#63;.
     *
     * @param manufacturerId the manufacturer ID
     * @return the matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByManufacturerId(long manufacturerId)
        throws SystemException {
        return findByManufacturerId(manufacturerId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the trailers where manufacturerId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param manufacturerId the manufacturer ID
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @return the range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByManufacturerId(long manufacturerId, int start,
        int end) throws SystemException {
        return findByManufacturerId(manufacturerId, start, end, null);
    }

    /**
     * Returns an ordered range of all the trailers where manufacturerId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param manufacturerId the manufacturer ID
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByManufacturerId(long manufacturerId, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MANUFACTURERID;
            finderArgs = new Object[] { manufacturerId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MANUFACTURERID;
            finderArgs = new Object[] {
                    manufacturerId,
                    
                    start, end, orderByComparator
                };
        }

        List<Trailer> list = (List<Trailer>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Trailer trailer : list) {
                if ((manufacturerId != trailer.getManufacturerId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_TRAILER_WHERE);

            query.append(_FINDER_COLUMN_MANUFACTURERID_MANUFACTURERID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TrailerModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(manufacturerId);

                if (!pagination) {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Trailer>(list);
                } else {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first trailer in the ordered set where manufacturerId = &#63;.
     *
     * @param manufacturerId the manufacturer ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByManufacturerId_First(long manufacturerId,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByManufacturerId_First(manufacturerId,
                orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("manufacturerId=");
        msg.append(manufacturerId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the first trailer in the ordered set where manufacturerId = &#63;.
     *
     * @param manufacturerId the manufacturer ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByManufacturerId_First(long manufacturerId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Trailer> list = findByManufacturerId(manufacturerId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last trailer in the ordered set where manufacturerId = &#63;.
     *
     * @param manufacturerId the manufacturer ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByManufacturerId_Last(long manufacturerId,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByManufacturerId_Last(manufacturerId,
                orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("manufacturerId=");
        msg.append(manufacturerId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the last trailer in the ordered set where manufacturerId = &#63;.
     *
     * @param manufacturerId the manufacturer ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByManufacturerId_Last(long manufacturerId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByManufacturerId(manufacturerId);

        if (count == 0) {
            return null;
        }

        List<Trailer> list = findByManufacturerId(manufacturerId, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the trailers before and after the current trailer in the ordered set where manufacturerId = &#63;.
     *
     * @param trailerId the primary key of the current trailer
     * @param manufacturerId the manufacturer ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer[] findByManufacturerId_PrevAndNext(long trailerId,
        long manufacturerId, OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = findByPrimaryKey(trailerId);

        Session session = null;

        try {
            session = openSession();

            Trailer[] array = new TrailerImpl[3];

            array[0] = getByManufacturerId_PrevAndNext(session, trailer,
                    manufacturerId, orderByComparator, true);

            array[1] = trailer;

            array[2] = getByManufacturerId_PrevAndNext(session, trailer,
                    manufacturerId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Trailer getByManufacturerId_PrevAndNext(Session session,
        Trailer trailer, long manufacturerId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TRAILER_WHERE);

        query.append(_FINDER_COLUMN_MANUFACTURERID_MANUFACTURERID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TrailerModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(manufacturerId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(trailer);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Trailer> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the trailers where manufacturerId = &#63; from the database.
     *
     * @param manufacturerId the manufacturer ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByManufacturerId(long manufacturerId)
        throws SystemException {
        for (Trailer trailer : findByManufacturerId(manufacturerId,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(trailer);
        }
    }

    /**
     * Returns the number of trailers where manufacturerId = &#63;.
     *
     * @param manufacturerId the manufacturer ID
     * @return the number of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByManufacturerId(long manufacturerId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_MANUFACTURERID;

        Object[] finderArgs = new Object[] { manufacturerId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_TRAILER_WHERE);

            query.append(_FINDER_COLUMN_MANUFACTURERID_MANUFACTURERID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(manufacturerId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the trailers where modelName = &#63;.
     *
     * @param modelName the model name
     * @return the matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByModelName(String modelName)
        throws SystemException {
        return findByModelName(modelName, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the trailers where modelName = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param modelName the model name
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @return the range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByModelName(String modelName, int start, int end)
        throws SystemException {
        return findByModelName(modelName, start, end, null);
    }

    /**
     * Returns an ordered range of all the trailers where modelName = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param modelName the model name
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByModelName(String modelName, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODELNAME;
            finderArgs = new Object[] { modelName };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MODELNAME;
            finderArgs = new Object[] { modelName, start, end, orderByComparator };
        }

        List<Trailer> list = (List<Trailer>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Trailer trailer : list) {
                if (!Validator.equals(modelName, trailer.getModelName())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_TRAILER_WHERE);

            boolean bindModelName = false;

            if (modelName == null) {
                query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_1);
            } else if (modelName.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_3);
            } else {
                bindModelName = true;

                query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TrailerModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindModelName) {
                    qPos.add(modelName.toLowerCase());
                }

                if (!pagination) {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Trailer>(list);
                } else {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first trailer in the ordered set where modelName = &#63;.
     *
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByModelName_First(String modelName,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByModelName_First(modelName, orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("modelName=");
        msg.append(modelName);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the first trailer in the ordered set where modelName = &#63;.
     *
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByModelName_First(String modelName,
        OrderByComparator orderByComparator) throws SystemException {
        List<Trailer> list = findByModelName(modelName, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last trailer in the ordered set where modelName = &#63;.
     *
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByModelName_Last(String modelName,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByModelName_Last(modelName, orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("modelName=");
        msg.append(modelName);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the last trailer in the ordered set where modelName = &#63;.
     *
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByModelName_Last(String modelName,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByModelName(modelName);

        if (count == 0) {
            return null;
        }

        List<Trailer> list = findByModelName(modelName, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the trailers before and after the current trailer in the ordered set where modelName = &#63;.
     *
     * @param trailerId the primary key of the current trailer
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer[] findByModelName_PrevAndNext(long trailerId,
        String modelName, OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = findByPrimaryKey(trailerId);

        Session session = null;

        try {
            session = openSession();

            Trailer[] array = new TrailerImpl[3];

            array[0] = getByModelName_PrevAndNext(session, trailer, modelName,
                    orderByComparator, true);

            array[1] = trailer;

            array[2] = getByModelName_PrevAndNext(session, trailer, modelName,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Trailer getByModelName_PrevAndNext(Session session,
        Trailer trailer, String modelName, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TRAILER_WHERE);

        boolean bindModelName = false;

        if (modelName == null) {
            query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_1);
        } else if (modelName.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_3);
        } else {
            bindModelName = true;

            query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TrailerModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindModelName) {
            qPos.add(modelName.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(trailer);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Trailer> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the trailers where modelName = &#63; from the database.
     *
     * @param modelName the model name
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByModelName(String modelName) throws SystemException {
        for (Trailer trailer : findByModelName(modelName, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(trailer);
        }
    }

    /**
     * Returns the number of trailers where modelName = &#63;.
     *
     * @param modelName the model name
     * @return the number of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByModelName(String modelName) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_MODELNAME;

        Object[] finderArgs = new Object[] { modelName };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_TRAILER_WHERE);

            boolean bindModelName = false;

            if (modelName == null) {
                query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_1);
            } else if (modelName.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_3);
            } else {
                bindModelName = true;

                query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindModelName) {
                    qPos.add(modelName.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the trailers where modelName LIKE &#63;.
     *
     * @param modelName the model name
     * @return the matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByModelNameLike(String modelName)
        throws SystemException {
        return findByModelNameLike(modelName, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the trailers where modelName LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param modelName the model name
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @return the range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByModelNameLike(String modelName, int start,
        int end) throws SystemException {
        return findByModelNameLike(modelName, start, end, null);
    }

    /**
     * Returns an ordered range of all the trailers where modelName LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param modelName the model name
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByModelNameLike(String modelName, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MODELNAMELIKE;
        finderArgs = new Object[] { modelName, start, end, orderByComparator };

        List<Trailer> list = (List<Trailer>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Trailer trailer : list) {
                if (!StringUtil.wildcardMatches(trailer.getModelName(),
                            modelName, CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, false)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_TRAILER_WHERE);

            boolean bindModelName = false;

            if (modelName == null) {
                query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_1);
            } else if (modelName.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_3);
            } else {
                bindModelName = true;

                query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TrailerModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindModelName) {
                    qPos.add(modelName.toLowerCase());
                }

                if (!pagination) {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Trailer>(list);
                } else {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first trailer in the ordered set where modelName LIKE &#63;.
     *
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByModelNameLike_First(String modelName,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByModelNameLike_First(modelName,
                orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("modelName=");
        msg.append(modelName);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the first trailer in the ordered set where modelName LIKE &#63;.
     *
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByModelNameLike_First(String modelName,
        OrderByComparator orderByComparator) throws SystemException {
        List<Trailer> list = findByModelNameLike(modelName, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last trailer in the ordered set where modelName LIKE &#63;.
     *
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByModelNameLike_Last(String modelName,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByModelNameLike_Last(modelName, orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("modelName=");
        msg.append(modelName);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the last trailer in the ordered set where modelName LIKE &#63;.
     *
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByModelNameLike_Last(String modelName,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByModelNameLike(modelName);

        if (count == 0) {
            return null;
        }

        List<Trailer> list = findByModelNameLike(modelName, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the trailers before and after the current trailer in the ordered set where modelName LIKE &#63;.
     *
     * @param trailerId the primary key of the current trailer
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer[] findByModelNameLike_PrevAndNext(long trailerId,
        String modelName, OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = findByPrimaryKey(trailerId);

        Session session = null;

        try {
            session = openSession();

            Trailer[] array = new TrailerImpl[3];

            array[0] = getByModelNameLike_PrevAndNext(session, trailer,
                    modelName, orderByComparator, true);

            array[1] = trailer;

            array[2] = getByModelNameLike_PrevAndNext(session, trailer,
                    modelName, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Trailer getByModelNameLike_PrevAndNext(Session session,
        Trailer trailer, String modelName, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TRAILER_WHERE);

        boolean bindModelName = false;

        if (modelName == null) {
            query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_1);
        } else if (modelName.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_3);
        } else {
            bindModelName = true;

            query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TrailerModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindModelName) {
            qPos.add(modelName.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(trailer);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Trailer> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the trailers where modelName LIKE &#63; from the database.
     *
     * @param modelName the model name
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByModelNameLike(String modelName)
        throws SystemException {
        for (Trailer trailer : findByModelNameLike(modelName,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(trailer);
        }
    }

    /**
     * Returns the number of trailers where modelName LIKE &#63;.
     *
     * @param modelName the model name
     * @return the number of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByModelNameLike(String modelName) throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_MODELNAMELIKE;

        Object[] finderArgs = new Object[] { modelName };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_TRAILER_WHERE);

            boolean bindModelName = false;

            if (modelName == null) {
                query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_1);
            } else if (modelName.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_3);
            } else {
                bindModelName = true;

                query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindModelName) {
                    qPos.add(modelName.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the trailers where licensePlate = &#63;.
     *
     * @param licensePlate the license plate
     * @return the matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByLicensePlate(String licensePlate)
        throws SystemException {
        return findByLicensePlate(licensePlate, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the trailers where licensePlate = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param licensePlate the license plate
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @return the range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByLicensePlate(String licensePlate, int start,
        int end) throws SystemException {
        return findByLicensePlate(licensePlate, start, end, null);
    }

    /**
     * Returns an ordered range of all the trailers where licensePlate = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param licensePlate the license plate
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByLicensePlate(String licensePlate, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LICENSEPLATE;
            finderArgs = new Object[] { licensePlate };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LICENSEPLATE;
            finderArgs = new Object[] {
                    licensePlate,
                    
                    start, end, orderByComparator
                };
        }

        List<Trailer> list = (List<Trailer>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Trailer trailer : list) {
                if (!Validator.equals(licensePlate, trailer.getLicensePlate())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_TRAILER_WHERE);

            boolean bindLicensePlate = false;

            if (licensePlate == null) {
                query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_1);
            } else if (licensePlate.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_3);
            } else {
                bindLicensePlate = true;

                query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TrailerModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindLicensePlate) {
                    qPos.add(licensePlate.toLowerCase());
                }

                if (!pagination) {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Trailer>(list);
                } else {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first trailer in the ordered set where licensePlate = &#63;.
     *
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByLicensePlate_First(String licensePlate,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByLicensePlate_First(licensePlate,
                orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("licensePlate=");
        msg.append(licensePlate);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the first trailer in the ordered set where licensePlate = &#63;.
     *
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByLicensePlate_First(String licensePlate,
        OrderByComparator orderByComparator) throws SystemException {
        List<Trailer> list = findByLicensePlate(licensePlate, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last trailer in the ordered set where licensePlate = &#63;.
     *
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByLicensePlate_Last(String licensePlate,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByLicensePlate_Last(licensePlate,
                orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("licensePlate=");
        msg.append(licensePlate);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the last trailer in the ordered set where licensePlate = &#63;.
     *
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByLicensePlate_Last(String licensePlate,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByLicensePlate(licensePlate);

        if (count == 0) {
            return null;
        }

        List<Trailer> list = findByLicensePlate(licensePlate, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the trailers before and after the current trailer in the ordered set where licensePlate = &#63;.
     *
     * @param trailerId the primary key of the current trailer
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer[] findByLicensePlate_PrevAndNext(long trailerId,
        String licensePlate, OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = findByPrimaryKey(trailerId);

        Session session = null;

        try {
            session = openSession();

            Trailer[] array = new TrailerImpl[3];

            array[0] = getByLicensePlate_PrevAndNext(session, trailer,
                    licensePlate, orderByComparator, true);

            array[1] = trailer;

            array[2] = getByLicensePlate_PrevAndNext(session, trailer,
                    licensePlate, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Trailer getByLicensePlate_PrevAndNext(Session session,
        Trailer trailer, String licensePlate,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TRAILER_WHERE);

        boolean bindLicensePlate = false;

        if (licensePlate == null) {
            query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_1);
        } else if (licensePlate.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_3);
        } else {
            bindLicensePlate = true;

            query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TrailerModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindLicensePlate) {
            qPos.add(licensePlate.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(trailer);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Trailer> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the trailers where licensePlate = &#63; from the database.
     *
     * @param licensePlate the license plate
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByLicensePlate(String licensePlate)
        throws SystemException {
        for (Trailer trailer : findByLicensePlate(licensePlate,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(trailer);
        }
    }

    /**
     * Returns the number of trailers where licensePlate = &#63;.
     *
     * @param licensePlate the license plate
     * @return the number of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByLicensePlate(String licensePlate)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_LICENSEPLATE;

        Object[] finderArgs = new Object[] { licensePlate };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_TRAILER_WHERE);

            boolean bindLicensePlate = false;

            if (licensePlate == null) {
                query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_1);
            } else if (licensePlate.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_3);
            } else {
                bindLicensePlate = true;

                query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindLicensePlate) {
                    qPos.add(licensePlate.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the trailers where licensePlate LIKE &#63;.
     *
     * @param licensePlate the license plate
     * @return the matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByLicensePlateLike(String licensePlate)
        throws SystemException {
        return findByLicensePlateLike(licensePlate, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the trailers where licensePlate LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param licensePlate the license plate
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @return the range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByLicensePlateLike(String licensePlate, int start,
        int end) throws SystemException {
        return findByLicensePlateLike(licensePlate, start, end, null);
    }

    /**
     * Returns an ordered range of all the trailers where licensePlate LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param licensePlate the license plate
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findByLicensePlateLike(String licensePlate, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LICENSEPLATELIKE;
        finderArgs = new Object[] { licensePlate, start, end, orderByComparator };

        List<Trailer> list = (List<Trailer>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Trailer trailer : list) {
                if (!StringUtil.wildcardMatches(trailer.getLicensePlate(),
                            licensePlate, CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, false)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_TRAILER_WHERE);

            boolean bindLicensePlate = false;

            if (licensePlate == null) {
                query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_1);
            } else if (licensePlate.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_3);
            } else {
                bindLicensePlate = true;

                query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TrailerModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindLicensePlate) {
                    qPos.add(licensePlate.toLowerCase());
                }

                if (!pagination) {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Trailer>(list);
                } else {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first trailer in the ordered set where licensePlate LIKE &#63;.
     *
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByLicensePlateLike_First(String licensePlate,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByLicensePlateLike_First(licensePlate,
                orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("licensePlate=");
        msg.append(licensePlate);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the first trailer in the ordered set where licensePlate LIKE &#63;.
     *
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByLicensePlateLike_First(String licensePlate,
        OrderByComparator orderByComparator) throws SystemException {
        List<Trailer> list = findByLicensePlateLike(licensePlate, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last trailer in the ordered set where licensePlate LIKE &#63;.
     *
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByLicensePlateLike_Last(String licensePlate,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByLicensePlateLike_Last(licensePlate,
                orderByComparator);

        if (trailer != null) {
            return trailer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("licensePlate=");
        msg.append(licensePlate);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerException(msg.toString());
    }

    /**
     * Returns the last trailer in the ordered set where licensePlate LIKE &#63;.
     *
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByLicensePlateLike_Last(String licensePlate,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByLicensePlateLike(licensePlate);

        if (count == 0) {
            return null;
        }

        List<Trailer> list = findByLicensePlateLike(licensePlate, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the trailers before and after the current trailer in the ordered set where licensePlate LIKE &#63;.
     *
     * @param trailerId the primary key of the current trailer
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer[] findByLicensePlateLike_PrevAndNext(long trailerId,
        String licensePlate, OrderByComparator orderByComparator)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = findByPrimaryKey(trailerId);

        Session session = null;

        try {
            session = openSession();

            Trailer[] array = new TrailerImpl[3];

            array[0] = getByLicensePlateLike_PrevAndNext(session, trailer,
                    licensePlate, orderByComparator, true);

            array[1] = trailer;

            array[2] = getByLicensePlateLike_PrevAndNext(session, trailer,
                    licensePlate, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Trailer getByLicensePlateLike_PrevAndNext(Session session,
        Trailer trailer, String licensePlate,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TRAILER_WHERE);

        boolean bindLicensePlate = false;

        if (licensePlate == null) {
            query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_1);
        } else if (licensePlate.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_3);
        } else {
            bindLicensePlate = true;

            query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TrailerModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindLicensePlate) {
            qPos.add(licensePlate.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(trailer);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Trailer> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the trailers where licensePlate LIKE &#63; from the database.
     *
     * @param licensePlate the license plate
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByLicensePlateLike(String licensePlate)
        throws SystemException {
        for (Trailer trailer : findByLicensePlateLike(licensePlate,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(trailer);
        }
    }

    /**
     * Returns the number of trailers where licensePlate LIKE &#63;.
     *
     * @param licensePlate the license plate
     * @return the number of matching trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByLicensePlateLike(String licensePlate)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_LICENSEPLATELIKE;

        Object[] finderArgs = new Object[] { licensePlate };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_TRAILER_WHERE);

            boolean bindLicensePlate = false;

            if (licensePlate == null) {
                query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_1);
            } else if (licensePlate.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_3);
            } else {
                bindLicensePlate = true;

                query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindLicensePlate) {
                    qPos.add(licensePlate.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the trailer in the entity cache if it is enabled.
     *
     * @param trailer the trailer
     */
    @Override
    public void cacheResult(Trailer trailer) {
        EntityCacheUtil.putResult(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerImpl.class, trailer.getPrimaryKey(), trailer);

        trailer.resetOriginalValues();
    }

    /**
     * Caches the trailers in the entity cache if it is enabled.
     *
     * @param trailers the trailers
     */
    @Override
    public void cacheResult(List<Trailer> trailers) {
        for (Trailer trailer : trailers) {
            if (EntityCacheUtil.getResult(
                        TrailerModelImpl.ENTITY_CACHE_ENABLED,
                        TrailerImpl.class, trailer.getPrimaryKey()) == null) {
                cacheResult(trailer);
            } else {
                trailer.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all trailers.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(TrailerImpl.class.getName());
        }

        EntityCacheUtil.clearCache(TrailerImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the trailer.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Trailer trailer) {
        EntityCacheUtil.removeResult(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerImpl.class, trailer.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Trailer> trailers) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Trailer trailer : trailers) {
            EntityCacheUtil.removeResult(TrailerModelImpl.ENTITY_CACHE_ENABLED,
                TrailerImpl.class, trailer.getPrimaryKey());
        }
    }

    /**
     * Creates a new trailer with the primary key. Does not add the trailer to the database.
     *
     * @param trailerId the primary key for the new trailer
     * @return the new trailer
     */
    @Override
    public Trailer create(long trailerId) {
        Trailer trailer = new TrailerImpl();

        trailer.setNew(true);
        trailer.setPrimaryKey(trailerId);

        return trailer;
    }

    /**
     * Removes the trailer with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param trailerId the primary key of the trailer
     * @return the trailer that was removed
     * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer remove(long trailerId)
        throws NoSuchTrailerException, SystemException {
        return remove((Serializable) trailerId);
    }

    /**
     * Removes the trailer with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the trailer
     * @return the trailer that was removed
     * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer remove(Serializable primaryKey)
        throws NoSuchTrailerException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Trailer trailer = (Trailer) session.get(TrailerImpl.class,
                    primaryKey);

            if (trailer == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchTrailerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(trailer);
        } catch (NoSuchTrailerException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Trailer removeImpl(Trailer trailer) throws SystemException {
        trailer = toUnwrappedModel(trailer);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(trailer)) {
                trailer = (Trailer) session.get(TrailerImpl.class,
                        trailer.getPrimaryKeyObj());
            }

            if (trailer != null) {
                session.delete(trailer);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (trailer != null) {
            clearCache(trailer);
        }

        return trailer;
    }

    @Override
    public Trailer updateImpl(de.humance.eco.profile.model.Trailer trailer)
        throws SystemException {
        trailer = toUnwrappedModel(trailer);

        boolean isNew = trailer.isNew();

        TrailerModelImpl trailerModelImpl = (TrailerModelImpl) trailer;

        Session session = null;

        try {
            session = openSession();

            if (trailer.isNew()) {
                session.save(trailer);

                trailer.setNew(false);
            } else {
                session.merge(trailer);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !TrailerModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((trailerModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        trailerModelImpl.getOriginalDriverId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DRIVERID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERID,
                    args);

                args = new Object[] { trailerModelImpl.getDriverId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DRIVERID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERID,
                    args);
            }

            if ((trailerModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDORGAID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        trailerModelImpl.getOriginalDriverId(),
                        trailerModelImpl.getOriginalOrganizationId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DRIVERIDANDORGAID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDORGAID,
                    args);

                args = new Object[] {
                        trailerModelImpl.getDriverId(),
                        trailerModelImpl.getOrganizationId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DRIVERIDANDORGAID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDORGAID,
                    args);
            }

            if ((trailerModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPEID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        trailerModelImpl.getOriginalTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TYPEID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPEID,
                    args);

                args = new Object[] { trailerModelImpl.getTypeId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TYPEID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPEID,
                    args);
            }

            if ((trailerModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDTYPEID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        trailerModelImpl.getOriginalDriverId(),
                        trailerModelImpl.getOriginalTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DRIVERIDANDTYPEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDTYPEID,
                    args);

                args = new Object[] {
                        trailerModelImpl.getDriverId(),
                        trailerModelImpl.getTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DRIVERIDANDTYPEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDTYPEID,
                    args);
            }

            if ((trailerModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MANUFACTURERID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        trailerModelImpl.getOriginalManufacturerId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MANUFACTURERID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MANUFACTURERID,
                    args);

                args = new Object[] { trailerModelImpl.getManufacturerId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MANUFACTURERID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MANUFACTURERID,
                    args);
            }

            if ((trailerModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODELNAME.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        trailerModelImpl.getOriginalModelName()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MODELNAME,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODELNAME,
                    args);

                args = new Object[] { trailerModelImpl.getModelName() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MODELNAME,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODELNAME,
                    args);
            }

            if ((trailerModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LICENSEPLATE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        trailerModelImpl.getOriginalLicensePlate()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LICENSEPLATE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LICENSEPLATE,
                    args);

                args = new Object[] { trailerModelImpl.getLicensePlate() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LICENSEPLATE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LICENSEPLATE,
                    args);
            }
        }

        EntityCacheUtil.putResult(TrailerModelImpl.ENTITY_CACHE_ENABLED,
            TrailerImpl.class, trailer.getPrimaryKey(), trailer);

        return trailer;
    }

    protected Trailer toUnwrappedModel(Trailer trailer) {
        if (trailer instanceof TrailerImpl) {
            return trailer;
        }

        TrailerImpl trailerImpl = new TrailerImpl();

        trailerImpl.setNew(trailer.isNew());
        trailerImpl.setPrimaryKey(trailer.getPrimaryKey());

        trailerImpl.setTrailerId(trailer.getTrailerId());
        trailerImpl.setCreatorId(trailer.getCreatorId());
        trailerImpl.setCreatorName(trailer.getCreatorName());
        trailerImpl.setCreateDate(trailer.getCreateDate());
        trailerImpl.setModifierId(trailer.getModifierId());
        trailerImpl.setModifierName(trailer.getModifierName());
        trailerImpl.setModifiedDate(trailer.getModifiedDate());
        trailerImpl.setDriverId(trailer.getDriverId());
        trailerImpl.setTypeId(trailer.getTypeId());
        trailerImpl.setManufacturerId(trailer.getManufacturerId());
        trailerImpl.setOrganizationId(trailer.getOrganizationId());
        trailerImpl.setModelName(trailer.getModelName());
        trailerImpl.setDimensionHeight(trailer.getDimensionHeight());
        trailerImpl.setDimensionWidth(trailer.getDimensionWidth());
        trailerImpl.setDimensionDepth(trailer.getDimensionDepth());
        trailerImpl.setLicensePlate(trailer.getLicensePlate());

        return trailerImpl;
    }

    /**
     * Returns the trailer with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the trailer
     * @return the trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByPrimaryKey(Serializable primaryKey)
        throws NoSuchTrailerException, SystemException {
        Trailer trailer = fetchByPrimaryKey(primaryKey);

        if (trailer == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchTrailerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return trailer;
    }

    /**
     * Returns the trailer with the primary key or throws a {@link de.humance.eco.profile.NoSuchTrailerException} if it could not be found.
     *
     * @param trailerId the primary key of the trailer
     * @return the trailer
     * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer findByPrimaryKey(long trailerId)
        throws NoSuchTrailerException, SystemException {
        return findByPrimaryKey((Serializable) trailerId);
    }

    /**
     * Returns the trailer with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the trailer
     * @return the trailer, or <code>null</code> if a trailer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Trailer trailer = (Trailer) EntityCacheUtil.getResult(TrailerModelImpl.ENTITY_CACHE_ENABLED,
                TrailerImpl.class, primaryKey);

        if (trailer == _nullTrailer) {
            return null;
        }

        if (trailer == null) {
            Session session = null;

            try {
                session = openSession();

                trailer = (Trailer) session.get(TrailerImpl.class, primaryKey);

                if (trailer != null) {
                    cacheResult(trailer);
                } else {
                    EntityCacheUtil.putResult(TrailerModelImpl.ENTITY_CACHE_ENABLED,
                        TrailerImpl.class, primaryKey, _nullTrailer);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(TrailerModelImpl.ENTITY_CACHE_ENABLED,
                    TrailerImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return trailer;
    }

    /**
     * Returns the trailer with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param trailerId the primary key of the trailer
     * @return the trailer, or <code>null</code> if a trailer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Trailer fetchByPrimaryKey(long trailerId) throws SystemException {
        return fetchByPrimaryKey((Serializable) trailerId);
    }

    /**
     * Returns all the trailers.
     *
     * @return the trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the trailers.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @return the range of trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the trailers.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of trailers
     * @param end the upper bound of the range of trailers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Trailer> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Trailer> list = (List<Trailer>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_TRAILER);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_TRAILER;

                if (pagination) {
                    sql = sql.concat(TrailerModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Trailer>(list);
                } else {
                    list = (List<Trailer>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the trailers from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Trailer trailer : findAll()) {
            remove(trailer);
        }
    }

    /**
     * Returns the number of trailers.
     *
     * @return the number of trailers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_TRAILER);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the trailer persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.de.humance.eco.profile.model.Trailer")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Trailer>> listenersList = new ArrayList<ModelListener<Trailer>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Trailer>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(TrailerImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
