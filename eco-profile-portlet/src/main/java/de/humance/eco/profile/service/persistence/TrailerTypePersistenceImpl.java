package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.humance.eco.profile.NoSuchTrailerTypeException;
import de.humance.eco.profile.model.TrailerType;
import de.humance.eco.profile.model.impl.TrailerTypeImpl;
import de.humance.eco.profile.model.impl.TrailerTypeModelImpl;
import de.humance.eco.profile.service.persistence.TrailerTypePersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the trailer type service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see TrailerTypePersistence
 * @see TrailerTypeUtil
 * @generated
 */
public class TrailerTypePersistenceImpl extends BasePersistenceImpl<TrailerType>
    implements TrailerTypePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link TrailerTypeUtil} to access the trailer type persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = TrailerTypeImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TrailerTypeModelImpl.ENTITY_CACHE_ENABLED,
            TrailerTypeModelImpl.FINDER_CACHE_ENABLED, TrailerTypeImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TrailerTypeModelImpl.ENTITY_CACHE_ENABLED,
            TrailerTypeModelImpl.FINDER_CACHE_ENABLED, TrailerTypeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TrailerTypeModelImpl.ENTITY_CACHE_ENABLED,
            TrailerTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME = new FinderPath(TrailerTypeModelImpl.ENTITY_CACHE_ENABLED,
            TrailerTypeModelImpl.FINDER_CACHE_ENABLED, TrailerTypeImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByName",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME = new FinderPath(TrailerTypeModelImpl.ENTITY_CACHE_ENABLED,
            TrailerTypeModelImpl.FINDER_CACHE_ENABLED, TrailerTypeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByName",
            new String[] { String.class.getName() },
            TrailerTypeModelImpl.NAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_NAME = new FinderPath(TrailerTypeModelImpl.ENTITY_CACHE_ENABLED,
            TrailerTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByName",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_NAME_NAME_1 = "trailerType.name IS NULL";
    private static final String _FINDER_COLUMN_NAME_NAME_2 = "lower(trailerType.name) = ?";
    private static final String _FINDER_COLUMN_NAME_NAME_3 = "(trailerType.name IS NULL OR trailerType.name = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NAMELIKE = new FinderPath(TrailerTypeModelImpl.ENTITY_CACHE_ENABLED,
            TrailerTypeModelImpl.FINDER_CACHE_ENABLED, TrailerTypeImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByNameLike",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_NAMELIKE =
        new FinderPath(TrailerTypeModelImpl.ENTITY_CACHE_ENABLED,
            TrailerTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByNameLike",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_NAMELIKE_NAME_1 = "trailerType.name LIKE NULL";
    private static final String _FINDER_COLUMN_NAMELIKE_NAME_2 = "lower(trailerType.name) LIKE ?";
    private static final String _FINDER_COLUMN_NAMELIKE_NAME_3 = "(trailerType.name IS NULL OR trailerType.name LIKE '')";
    private static final String _SQL_SELECT_TRAILERTYPE = "SELECT trailerType FROM TrailerType trailerType";
    private static final String _SQL_SELECT_TRAILERTYPE_WHERE = "SELECT trailerType FROM TrailerType trailerType WHERE ";
    private static final String _SQL_COUNT_TRAILERTYPE = "SELECT COUNT(trailerType) FROM TrailerType trailerType";
    private static final String _SQL_COUNT_TRAILERTYPE_WHERE = "SELECT COUNT(trailerType) FROM TrailerType trailerType WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "trailerType.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No TrailerType exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No TrailerType exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(TrailerTypePersistenceImpl.class);
    private static TrailerType _nullTrailerType = new TrailerTypeImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<TrailerType> toCacheModel() {
                return _nullTrailerTypeCacheModel;
            }
        };

    private static CacheModel<TrailerType> _nullTrailerTypeCacheModel = new CacheModel<TrailerType>() {
            @Override
            public TrailerType toEntityModel() {
                return _nullTrailerType;
            }
        };

    public TrailerTypePersistenceImpl() {
        setModelClass(TrailerType.class);
    }

    /**
     * Returns all the trailer types where name = &#63;.
     *
     * @param name the name
     * @return the matching trailer types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerType> findByName(String name) throws SystemException {
        return findByName(name, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the trailer types where name = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param name the name
     * @param start the lower bound of the range of trailer types
     * @param end the upper bound of the range of trailer types (not inclusive)
     * @return the range of matching trailer types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerType> findByName(String name, int start, int end)
        throws SystemException {
        return findByName(name, start, end, null);
    }

    /**
     * Returns an ordered range of all the trailer types where name = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param name the name
     * @param start the lower bound of the range of trailer types
     * @param end the upper bound of the range of trailer types (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching trailer types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerType> findByName(String name, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME;
            finderArgs = new Object[] { name };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME;
            finderArgs = new Object[] { name, start, end, orderByComparator };
        }

        List<TrailerType> list = (List<TrailerType>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (TrailerType trailerType : list) {
                if (!Validator.equals(name, trailerType.getName())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_TRAILERTYPE_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAME_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAME_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAME_NAME_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TrailerTypeModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name.toLowerCase());
                }

                if (!pagination) {
                    list = (List<TrailerType>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<TrailerType>(list);
                } else {
                    list = (List<TrailerType>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first trailer type in the ordered set where name = &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer type
     * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a matching trailer type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerType findByName_First(String name,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerTypeException, SystemException {
        TrailerType trailerType = fetchByName_First(name, orderByComparator);

        if (trailerType != null) {
            return trailerType;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("name=");
        msg.append(name);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerTypeException(msg.toString());
    }

    /**
     * Returns the first trailer type in the ordered set where name = &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer type, or <code>null</code> if a matching trailer type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerType fetchByName_First(String name,
        OrderByComparator orderByComparator) throws SystemException {
        List<TrailerType> list = findByName(name, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last trailer type in the ordered set where name = &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer type
     * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a matching trailer type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerType findByName_Last(String name,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerTypeException, SystemException {
        TrailerType trailerType = fetchByName_Last(name, orderByComparator);

        if (trailerType != null) {
            return trailerType;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("name=");
        msg.append(name);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerTypeException(msg.toString());
    }

    /**
     * Returns the last trailer type in the ordered set where name = &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer type, or <code>null</code> if a matching trailer type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerType fetchByName_Last(String name,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByName(name);

        if (count == 0) {
            return null;
        }

        List<TrailerType> list = findByName(name, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the trailer types before and after the current trailer type in the ordered set where name = &#63;.
     *
     * @param trailerTypeId the primary key of the current trailer type
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next trailer type
     * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a trailer type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerType[] findByName_PrevAndNext(long trailerTypeId,
        String name, OrderByComparator orderByComparator)
        throws NoSuchTrailerTypeException, SystemException {
        TrailerType trailerType = findByPrimaryKey(trailerTypeId);

        Session session = null;

        try {
            session = openSession();

            TrailerType[] array = new TrailerTypeImpl[3];

            array[0] = getByName_PrevAndNext(session, trailerType, name,
                    orderByComparator, true);

            array[1] = trailerType;

            array[2] = getByName_PrevAndNext(session, trailerType, name,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected TrailerType getByName_PrevAndNext(Session session,
        TrailerType trailerType, String name,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TRAILERTYPE_WHERE);

        boolean bindName = false;

        if (name == null) {
            query.append(_FINDER_COLUMN_NAME_NAME_1);
        } else if (name.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_NAME_NAME_3);
        } else {
            bindName = true;

            query.append(_FINDER_COLUMN_NAME_NAME_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TrailerTypeModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindName) {
            qPos.add(name.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(trailerType);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<TrailerType> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the trailer types where name = &#63; from the database.
     *
     * @param name the name
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByName(String name) throws SystemException {
        for (TrailerType trailerType : findByName(name, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(trailerType);
        }
    }

    /**
     * Returns the number of trailer types where name = &#63;.
     *
     * @param name the name
     * @return the number of matching trailer types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByName(String name) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_NAME;

        Object[] finderArgs = new Object[] { name };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_TRAILERTYPE_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAME_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAME_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAME_NAME_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the trailer types where name LIKE &#63;.
     *
     * @param name the name
     * @return the matching trailer types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerType> findByNameLike(String name)
        throws SystemException {
        return findByNameLike(name, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the trailer types where name LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param name the name
     * @param start the lower bound of the range of trailer types
     * @param end the upper bound of the range of trailer types (not inclusive)
     * @return the range of matching trailer types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerType> findByNameLike(String name, int start, int end)
        throws SystemException {
        return findByNameLike(name, start, end, null);
    }

    /**
     * Returns an ordered range of all the trailer types where name LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param name the name
     * @param start the lower bound of the range of trailer types
     * @param end the upper bound of the range of trailer types (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching trailer types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerType> findByNameLike(String name, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NAMELIKE;
        finderArgs = new Object[] { name, start, end, orderByComparator };

        List<TrailerType> list = (List<TrailerType>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (TrailerType trailerType : list) {
                if (!StringUtil.wildcardMatches(trailerType.getName(), name,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, false)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_TRAILERTYPE_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAMELIKE_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAMELIKE_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAMELIKE_NAME_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TrailerTypeModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name.toLowerCase());
                }

                if (!pagination) {
                    list = (List<TrailerType>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<TrailerType>(list);
                } else {
                    list = (List<TrailerType>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first trailer type in the ordered set where name LIKE &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer type
     * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a matching trailer type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerType findByNameLike_First(String name,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerTypeException, SystemException {
        TrailerType trailerType = fetchByNameLike_First(name, orderByComparator);

        if (trailerType != null) {
            return trailerType;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("name=");
        msg.append(name);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerTypeException(msg.toString());
    }

    /**
     * Returns the first trailer type in the ordered set where name LIKE &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer type, or <code>null</code> if a matching trailer type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerType fetchByNameLike_First(String name,
        OrderByComparator orderByComparator) throws SystemException {
        List<TrailerType> list = findByNameLike(name, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last trailer type in the ordered set where name LIKE &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer type
     * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a matching trailer type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerType findByNameLike_Last(String name,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerTypeException, SystemException {
        TrailerType trailerType = fetchByNameLike_Last(name, orderByComparator);

        if (trailerType != null) {
            return trailerType;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("name=");
        msg.append(name);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerTypeException(msg.toString());
    }

    /**
     * Returns the last trailer type in the ordered set where name LIKE &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer type, or <code>null</code> if a matching trailer type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerType fetchByNameLike_Last(String name,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByNameLike(name);

        if (count == 0) {
            return null;
        }

        List<TrailerType> list = findByNameLike(name, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the trailer types before and after the current trailer type in the ordered set where name LIKE &#63;.
     *
     * @param trailerTypeId the primary key of the current trailer type
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next trailer type
     * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a trailer type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerType[] findByNameLike_PrevAndNext(long trailerTypeId,
        String name, OrderByComparator orderByComparator)
        throws NoSuchTrailerTypeException, SystemException {
        TrailerType trailerType = findByPrimaryKey(trailerTypeId);

        Session session = null;

        try {
            session = openSession();

            TrailerType[] array = new TrailerTypeImpl[3];

            array[0] = getByNameLike_PrevAndNext(session, trailerType, name,
                    orderByComparator, true);

            array[1] = trailerType;

            array[2] = getByNameLike_PrevAndNext(session, trailerType, name,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected TrailerType getByNameLike_PrevAndNext(Session session,
        TrailerType trailerType, String name,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TRAILERTYPE_WHERE);

        boolean bindName = false;

        if (name == null) {
            query.append(_FINDER_COLUMN_NAMELIKE_NAME_1);
        } else if (name.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_NAMELIKE_NAME_3);
        } else {
            bindName = true;

            query.append(_FINDER_COLUMN_NAMELIKE_NAME_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TrailerTypeModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindName) {
            qPos.add(name.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(trailerType);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<TrailerType> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the trailer types where name LIKE &#63; from the database.
     *
     * @param name the name
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByNameLike(String name) throws SystemException {
        for (TrailerType trailerType : findByNameLike(name, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(trailerType);
        }
    }

    /**
     * Returns the number of trailer types where name LIKE &#63;.
     *
     * @param name the name
     * @return the number of matching trailer types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByNameLike(String name) throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_NAMELIKE;

        Object[] finderArgs = new Object[] { name };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_TRAILERTYPE_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAMELIKE_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAMELIKE_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAMELIKE_NAME_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the trailer type in the entity cache if it is enabled.
     *
     * @param trailerType the trailer type
     */
    @Override
    public void cacheResult(TrailerType trailerType) {
        EntityCacheUtil.putResult(TrailerTypeModelImpl.ENTITY_CACHE_ENABLED,
            TrailerTypeImpl.class, trailerType.getPrimaryKey(), trailerType);

        trailerType.resetOriginalValues();
    }

    /**
     * Caches the trailer types in the entity cache if it is enabled.
     *
     * @param trailerTypes the trailer types
     */
    @Override
    public void cacheResult(List<TrailerType> trailerTypes) {
        for (TrailerType trailerType : trailerTypes) {
            if (EntityCacheUtil.getResult(
                        TrailerTypeModelImpl.ENTITY_CACHE_ENABLED,
                        TrailerTypeImpl.class, trailerType.getPrimaryKey()) == null) {
                cacheResult(trailerType);
            } else {
                trailerType.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all trailer types.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(TrailerTypeImpl.class.getName());
        }

        EntityCacheUtil.clearCache(TrailerTypeImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the trailer type.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(TrailerType trailerType) {
        EntityCacheUtil.removeResult(TrailerTypeModelImpl.ENTITY_CACHE_ENABLED,
            TrailerTypeImpl.class, trailerType.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<TrailerType> trailerTypes) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (TrailerType trailerType : trailerTypes) {
            EntityCacheUtil.removeResult(TrailerTypeModelImpl.ENTITY_CACHE_ENABLED,
                TrailerTypeImpl.class, trailerType.getPrimaryKey());
        }
    }

    /**
     * Creates a new trailer type with the primary key. Does not add the trailer type to the database.
     *
     * @param trailerTypeId the primary key for the new trailer type
     * @return the new trailer type
     */
    @Override
    public TrailerType create(long trailerTypeId) {
        TrailerType trailerType = new TrailerTypeImpl();

        trailerType.setNew(true);
        trailerType.setPrimaryKey(trailerTypeId);

        return trailerType;
    }

    /**
     * Removes the trailer type with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param trailerTypeId the primary key of the trailer type
     * @return the trailer type that was removed
     * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a trailer type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerType remove(long trailerTypeId)
        throws NoSuchTrailerTypeException, SystemException {
        return remove((Serializable) trailerTypeId);
    }

    /**
     * Removes the trailer type with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the trailer type
     * @return the trailer type that was removed
     * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a trailer type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerType remove(Serializable primaryKey)
        throws NoSuchTrailerTypeException, SystemException {
        Session session = null;

        try {
            session = openSession();

            TrailerType trailerType = (TrailerType) session.get(TrailerTypeImpl.class,
                    primaryKey);

            if (trailerType == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchTrailerTypeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(trailerType);
        } catch (NoSuchTrailerTypeException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected TrailerType removeImpl(TrailerType trailerType)
        throws SystemException {
        trailerType = toUnwrappedModel(trailerType);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(trailerType)) {
                trailerType = (TrailerType) session.get(TrailerTypeImpl.class,
                        trailerType.getPrimaryKeyObj());
            }

            if (trailerType != null) {
                session.delete(trailerType);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (trailerType != null) {
            clearCache(trailerType);
        }

        return trailerType;
    }

    @Override
    public TrailerType updateImpl(
        de.humance.eco.profile.model.TrailerType trailerType)
        throws SystemException {
        trailerType = toUnwrappedModel(trailerType);

        boolean isNew = trailerType.isNew();

        TrailerTypeModelImpl trailerTypeModelImpl = (TrailerTypeModelImpl) trailerType;

        Session session = null;

        try {
            session = openSession();

            if (trailerType.isNew()) {
                session.save(trailerType);

                trailerType.setNew(false);
            } else {
                session.merge(trailerType);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !TrailerTypeModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((trailerTypeModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        trailerTypeModelImpl.getOriginalName()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
                    args);

                args = new Object[] { trailerTypeModelImpl.getName() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
                    args);
            }
        }

        EntityCacheUtil.putResult(TrailerTypeModelImpl.ENTITY_CACHE_ENABLED,
            TrailerTypeImpl.class, trailerType.getPrimaryKey(), trailerType);

        return trailerType;
    }

    protected TrailerType toUnwrappedModel(TrailerType trailerType) {
        if (trailerType instanceof TrailerTypeImpl) {
            return trailerType;
        }

        TrailerTypeImpl trailerTypeImpl = new TrailerTypeImpl();

        trailerTypeImpl.setNew(trailerType.isNew());
        trailerTypeImpl.setPrimaryKey(trailerType.getPrimaryKey());

        trailerTypeImpl.setTrailerTypeId(trailerType.getTrailerTypeId());
        trailerTypeImpl.setCreatorId(trailerType.getCreatorId());
        trailerTypeImpl.setCreatorName(trailerType.getCreatorName());
        trailerTypeImpl.setCreateDate(trailerType.getCreateDate());
        trailerTypeImpl.setModifierId(trailerType.getModifierId());
        trailerTypeImpl.setModifierName(trailerType.getModifierName());
        trailerTypeImpl.setModifiedDate(trailerType.getModifiedDate());
        trailerTypeImpl.setName(trailerType.getName());
        trailerTypeImpl.setIconId(trailerType.getIconId());

        return trailerTypeImpl;
    }

    /**
     * Returns the trailer type with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the trailer type
     * @return the trailer type
     * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a trailer type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerType findByPrimaryKey(Serializable primaryKey)
        throws NoSuchTrailerTypeException, SystemException {
        TrailerType trailerType = fetchByPrimaryKey(primaryKey);

        if (trailerType == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchTrailerTypeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return trailerType;
    }

    /**
     * Returns the trailer type with the primary key or throws a {@link de.humance.eco.profile.NoSuchTrailerTypeException} if it could not be found.
     *
     * @param trailerTypeId the primary key of the trailer type
     * @return the trailer type
     * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a trailer type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerType findByPrimaryKey(long trailerTypeId)
        throws NoSuchTrailerTypeException, SystemException {
        return findByPrimaryKey((Serializable) trailerTypeId);
    }

    /**
     * Returns the trailer type with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the trailer type
     * @return the trailer type, or <code>null</code> if a trailer type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerType fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        TrailerType trailerType = (TrailerType) EntityCacheUtil.getResult(TrailerTypeModelImpl.ENTITY_CACHE_ENABLED,
                TrailerTypeImpl.class, primaryKey);

        if (trailerType == _nullTrailerType) {
            return null;
        }

        if (trailerType == null) {
            Session session = null;

            try {
                session = openSession();

                trailerType = (TrailerType) session.get(TrailerTypeImpl.class,
                        primaryKey);

                if (trailerType != null) {
                    cacheResult(trailerType);
                } else {
                    EntityCacheUtil.putResult(TrailerTypeModelImpl.ENTITY_CACHE_ENABLED,
                        TrailerTypeImpl.class, primaryKey, _nullTrailerType);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(TrailerTypeModelImpl.ENTITY_CACHE_ENABLED,
                    TrailerTypeImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return trailerType;
    }

    /**
     * Returns the trailer type with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param trailerTypeId the primary key of the trailer type
     * @return the trailer type, or <code>null</code> if a trailer type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerType fetchByPrimaryKey(long trailerTypeId)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) trailerTypeId);
    }

    /**
     * Returns all the trailer types.
     *
     * @return the trailer types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerType> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the trailer types.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of trailer types
     * @param end the upper bound of the range of trailer types (not inclusive)
     * @return the range of trailer types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerType> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the trailer types.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of trailer types
     * @param end the upper bound of the range of trailer types (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of trailer types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerType> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<TrailerType> list = (List<TrailerType>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_TRAILERTYPE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_TRAILERTYPE;

                if (pagination) {
                    sql = sql.concat(TrailerTypeModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<TrailerType>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<TrailerType>(list);
                } else {
                    list = (List<TrailerType>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the trailer types from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (TrailerType trailerType : findAll()) {
            remove(trailerType);
        }
    }

    /**
     * Returns the number of trailer types.
     *
     * @return the number of trailer types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_TRAILERTYPE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the trailer type persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.de.humance.eco.profile.model.TrailerType")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<TrailerType>> listenersList = new ArrayList<ModelListener<TrailerType>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<TrailerType>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(TrailerTypeImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
