package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.humance.eco.profile.NoSuchTrailerUserException;
import de.humance.eco.profile.model.TrailerUser;
import de.humance.eco.profile.model.impl.TrailerUserImpl;
import de.humance.eco.profile.model.impl.TrailerUserModelImpl;
import de.humance.eco.profile.service.persistence.TrailerUserPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the trailer user service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see TrailerUserPersistence
 * @see TrailerUserUtil
 * @generated
 */
public class TrailerUserPersistenceImpl extends BasePersistenceImpl<TrailerUser>
    implements TrailerUserPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link TrailerUserUtil} to access the trailer user persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = TrailerUserImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TrailerUserModelImpl.ENTITY_CACHE_ENABLED,
            TrailerUserModelImpl.FINDER_CACHE_ENABLED, TrailerUserImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TrailerUserModelImpl.ENTITY_CACHE_ENABLED,
            TrailerUserModelImpl.FINDER_CACHE_ENABLED, TrailerUserImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TrailerUserModelImpl.ENTITY_CACHE_ENABLED,
            TrailerUserModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(TrailerUserModelImpl.ENTITY_CACHE_ENABLED,
            TrailerUserModelImpl.FINDER_CACHE_ENABLED, TrailerUserImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
        new FinderPath(TrailerUserModelImpl.ENTITY_CACHE_ENABLED,
            TrailerUserModelImpl.FINDER_CACHE_ENABLED, TrailerUserImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserId",
            new String[] { Long.class.getName() },
            TrailerUserModelImpl.USERID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(TrailerUserModelImpl.ENTITY_CACHE_ENABLED,
            TrailerUserModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_USERID_USERID_2 = "trailerUser.userId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TRAILERID =
        new FinderPath(TrailerUserModelImpl.ENTITY_CACHE_ENABLED,
            TrailerUserModelImpl.FINDER_CACHE_ENABLED, TrailerUserImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTrailerId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TRAILERID =
        new FinderPath(TrailerUserModelImpl.ENTITY_CACHE_ENABLED,
            TrailerUserModelImpl.FINDER_CACHE_ENABLED, TrailerUserImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByTrailerId",
            new String[] { Long.class.getName() },
            TrailerUserModelImpl.TRAILERID_COLUMN_BITMASK |
            TrailerUserModelImpl.USERID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_TRAILERID = new FinderPath(TrailerUserModelImpl.ENTITY_CACHE_ENABLED,
            TrailerUserModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTrailerId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_TRAILERID_TRAILERID_2 = "trailerUser.trailerId = ?";
    private static final String _SQL_SELECT_TRAILERUSER = "SELECT trailerUser FROM TrailerUser trailerUser";
    private static final String _SQL_SELECT_TRAILERUSER_WHERE = "SELECT trailerUser FROM TrailerUser trailerUser WHERE ";
    private static final String _SQL_COUNT_TRAILERUSER = "SELECT COUNT(trailerUser) FROM TrailerUser trailerUser";
    private static final String _SQL_COUNT_TRAILERUSER_WHERE = "SELECT COUNT(trailerUser) FROM TrailerUser trailerUser WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "trailerUser.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No TrailerUser exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No TrailerUser exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(TrailerUserPersistenceImpl.class);
    private static TrailerUser _nullTrailerUser = new TrailerUserImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<TrailerUser> toCacheModel() {
                return _nullTrailerUserCacheModel;
            }
        };

    private static CacheModel<TrailerUser> _nullTrailerUserCacheModel = new CacheModel<TrailerUser>() {
            @Override
            public TrailerUser toEntityModel() {
                return _nullTrailerUser;
            }
        };

    public TrailerUserPersistenceImpl() {
        setModelClass(TrailerUser.class);
    }

    /**
     * Returns all the trailer users where userId = &#63;.
     *
     * @param userId the user ID
     * @return the matching trailer users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerUser> findByUserId(long userId)
        throws SystemException {
        return findByUserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the trailer users where userId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param userId the user ID
     * @param start the lower bound of the range of trailer users
     * @param end the upper bound of the range of trailer users (not inclusive)
     * @return the range of matching trailer users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerUser> findByUserId(long userId, int start, int end)
        throws SystemException {
        return findByUserId(userId, start, end, null);
    }

    /**
     * Returns an ordered range of all the trailer users where userId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param userId the user ID
     * @param start the lower bound of the range of trailer users
     * @param end the upper bound of the range of trailer users (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching trailer users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerUser> findByUserId(long userId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
            finderArgs = new Object[] { userId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
            finderArgs = new Object[] { userId, start, end, orderByComparator };
        }

        List<TrailerUser> list = (List<TrailerUser>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (TrailerUser trailerUser : list) {
                if ((userId != trailerUser.getUserId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_TRAILERUSER_WHERE);

            query.append(_FINDER_COLUMN_USERID_USERID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TrailerUserModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(userId);

                if (!pagination) {
                    list = (List<TrailerUser>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<TrailerUser>(list);
                } else {
                    list = (List<TrailerUser>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first trailer user in the ordered set where userId = &#63;.
     *
     * @param userId the user ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer user
     * @throws de.humance.eco.profile.NoSuchTrailerUserException if a matching trailer user could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerUser findByUserId_First(long userId,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerUserException, SystemException {
        TrailerUser trailerUser = fetchByUserId_First(userId, orderByComparator);

        if (trailerUser != null) {
            return trailerUser;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("userId=");
        msg.append(userId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerUserException(msg.toString());
    }

    /**
     * Returns the first trailer user in the ordered set where userId = &#63;.
     *
     * @param userId the user ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer user, or <code>null</code> if a matching trailer user could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerUser fetchByUserId_First(long userId,
        OrderByComparator orderByComparator) throws SystemException {
        List<TrailerUser> list = findByUserId(userId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last trailer user in the ordered set where userId = &#63;.
     *
     * @param userId the user ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer user
     * @throws de.humance.eco.profile.NoSuchTrailerUserException if a matching trailer user could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerUser findByUserId_Last(long userId,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerUserException, SystemException {
        TrailerUser trailerUser = fetchByUserId_Last(userId, orderByComparator);

        if (trailerUser != null) {
            return trailerUser;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("userId=");
        msg.append(userId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerUserException(msg.toString());
    }

    /**
     * Returns the last trailer user in the ordered set where userId = &#63;.
     *
     * @param userId the user ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer user, or <code>null</code> if a matching trailer user could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerUser fetchByUserId_Last(long userId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByUserId(userId);

        if (count == 0) {
            return null;
        }

        List<TrailerUser> list = findByUserId(userId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the trailer users before and after the current trailer user in the ordered set where userId = &#63;.
     *
     * @param trailerUserId the primary key of the current trailer user
     * @param userId the user ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next trailer user
     * @throws de.humance.eco.profile.NoSuchTrailerUserException if a trailer user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerUser[] findByUserId_PrevAndNext(long trailerUserId,
        long userId, OrderByComparator orderByComparator)
        throws NoSuchTrailerUserException, SystemException {
        TrailerUser trailerUser = findByPrimaryKey(trailerUserId);

        Session session = null;

        try {
            session = openSession();

            TrailerUser[] array = new TrailerUserImpl[3];

            array[0] = getByUserId_PrevAndNext(session, trailerUser, userId,
                    orderByComparator, true);

            array[1] = trailerUser;

            array[2] = getByUserId_PrevAndNext(session, trailerUser, userId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected TrailerUser getByUserId_PrevAndNext(Session session,
        TrailerUser trailerUser, long userId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TRAILERUSER_WHERE);

        query.append(_FINDER_COLUMN_USERID_USERID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TrailerUserModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(userId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(trailerUser);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<TrailerUser> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the trailer users where userId = &#63; from the database.
     *
     * @param userId the user ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByUserId(long userId) throws SystemException {
        for (TrailerUser trailerUser : findByUserId(userId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(trailerUser);
        }
    }

    /**
     * Returns the number of trailer users where userId = &#63;.
     *
     * @param userId the user ID
     * @return the number of matching trailer users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByUserId(long userId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

        Object[] finderArgs = new Object[] { userId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_TRAILERUSER_WHERE);

            query.append(_FINDER_COLUMN_USERID_USERID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(userId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the trailer users where trailerId = &#63;.
     *
     * @param trailerId the trailer ID
     * @return the matching trailer users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerUser> findByTrailerId(long trailerId)
        throws SystemException {
        return findByTrailerId(trailerId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the trailer users where trailerId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param trailerId the trailer ID
     * @param start the lower bound of the range of trailer users
     * @param end the upper bound of the range of trailer users (not inclusive)
     * @return the range of matching trailer users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerUser> findByTrailerId(long trailerId, int start, int end)
        throws SystemException {
        return findByTrailerId(trailerId, start, end, null);
    }

    /**
     * Returns an ordered range of all the trailer users where trailerId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param trailerId the trailer ID
     * @param start the lower bound of the range of trailer users
     * @param end the upper bound of the range of trailer users (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching trailer users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerUser> findByTrailerId(long trailerId, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TRAILERID;
            finderArgs = new Object[] { trailerId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TRAILERID;
            finderArgs = new Object[] { trailerId, start, end, orderByComparator };
        }

        List<TrailerUser> list = (List<TrailerUser>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (TrailerUser trailerUser : list) {
                if ((trailerId != trailerUser.getTrailerId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_TRAILERUSER_WHERE);

            query.append(_FINDER_COLUMN_TRAILERID_TRAILERID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(TrailerUserModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(trailerId);

                if (!pagination) {
                    list = (List<TrailerUser>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<TrailerUser>(list);
                } else {
                    list = (List<TrailerUser>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first trailer user in the ordered set where trailerId = &#63;.
     *
     * @param trailerId the trailer ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer user
     * @throws de.humance.eco.profile.NoSuchTrailerUserException if a matching trailer user could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerUser findByTrailerId_First(long trailerId,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerUserException, SystemException {
        TrailerUser trailerUser = fetchByTrailerId_First(trailerId,
                orderByComparator);

        if (trailerUser != null) {
            return trailerUser;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("trailerId=");
        msg.append(trailerId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerUserException(msg.toString());
    }

    /**
     * Returns the first trailer user in the ordered set where trailerId = &#63;.
     *
     * @param trailerId the trailer ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching trailer user, or <code>null</code> if a matching trailer user could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerUser fetchByTrailerId_First(long trailerId,
        OrderByComparator orderByComparator) throws SystemException {
        List<TrailerUser> list = findByTrailerId(trailerId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last trailer user in the ordered set where trailerId = &#63;.
     *
     * @param trailerId the trailer ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer user
     * @throws de.humance.eco.profile.NoSuchTrailerUserException if a matching trailer user could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerUser findByTrailerId_Last(long trailerId,
        OrderByComparator orderByComparator)
        throws NoSuchTrailerUserException, SystemException {
        TrailerUser trailerUser = fetchByTrailerId_Last(trailerId,
                orderByComparator);

        if (trailerUser != null) {
            return trailerUser;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("trailerId=");
        msg.append(trailerId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchTrailerUserException(msg.toString());
    }

    /**
     * Returns the last trailer user in the ordered set where trailerId = &#63;.
     *
     * @param trailerId the trailer ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching trailer user, or <code>null</code> if a matching trailer user could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerUser fetchByTrailerId_Last(long trailerId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByTrailerId(trailerId);

        if (count == 0) {
            return null;
        }

        List<TrailerUser> list = findByTrailerId(trailerId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the trailer users before and after the current trailer user in the ordered set where trailerId = &#63;.
     *
     * @param trailerUserId the primary key of the current trailer user
     * @param trailerId the trailer ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next trailer user
     * @throws de.humance.eco.profile.NoSuchTrailerUserException if a trailer user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerUser[] findByTrailerId_PrevAndNext(long trailerUserId,
        long trailerId, OrderByComparator orderByComparator)
        throws NoSuchTrailerUserException, SystemException {
        TrailerUser trailerUser = findByPrimaryKey(trailerUserId);

        Session session = null;

        try {
            session = openSession();

            TrailerUser[] array = new TrailerUserImpl[3];

            array[0] = getByTrailerId_PrevAndNext(session, trailerUser,
                    trailerId, orderByComparator, true);

            array[1] = trailerUser;

            array[2] = getByTrailerId_PrevAndNext(session, trailerUser,
                    trailerId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected TrailerUser getByTrailerId_PrevAndNext(Session session,
        TrailerUser trailerUser, long trailerId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_TRAILERUSER_WHERE);

        query.append(_FINDER_COLUMN_TRAILERID_TRAILERID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(TrailerUserModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(trailerId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(trailerUser);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<TrailerUser> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the trailer users where trailerId = &#63; from the database.
     *
     * @param trailerId the trailer ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByTrailerId(long trailerId) throws SystemException {
        for (TrailerUser trailerUser : findByTrailerId(trailerId,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(trailerUser);
        }
    }

    /**
     * Returns the number of trailer users where trailerId = &#63;.
     *
     * @param trailerId the trailer ID
     * @return the number of matching trailer users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByTrailerId(long trailerId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_TRAILERID;

        Object[] finderArgs = new Object[] { trailerId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_TRAILERUSER_WHERE);

            query.append(_FINDER_COLUMN_TRAILERID_TRAILERID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(trailerId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the trailer user in the entity cache if it is enabled.
     *
     * @param trailerUser the trailer user
     */
    @Override
    public void cacheResult(TrailerUser trailerUser) {
        EntityCacheUtil.putResult(TrailerUserModelImpl.ENTITY_CACHE_ENABLED,
            TrailerUserImpl.class, trailerUser.getPrimaryKey(), trailerUser);

        trailerUser.resetOriginalValues();
    }

    /**
     * Caches the trailer users in the entity cache if it is enabled.
     *
     * @param trailerUsers the trailer users
     */
    @Override
    public void cacheResult(List<TrailerUser> trailerUsers) {
        for (TrailerUser trailerUser : trailerUsers) {
            if (EntityCacheUtil.getResult(
                        TrailerUserModelImpl.ENTITY_CACHE_ENABLED,
                        TrailerUserImpl.class, trailerUser.getPrimaryKey()) == null) {
                cacheResult(trailerUser);
            } else {
                trailerUser.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all trailer users.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(TrailerUserImpl.class.getName());
        }

        EntityCacheUtil.clearCache(TrailerUserImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the trailer user.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(TrailerUser trailerUser) {
        EntityCacheUtil.removeResult(TrailerUserModelImpl.ENTITY_CACHE_ENABLED,
            TrailerUserImpl.class, trailerUser.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<TrailerUser> trailerUsers) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (TrailerUser trailerUser : trailerUsers) {
            EntityCacheUtil.removeResult(TrailerUserModelImpl.ENTITY_CACHE_ENABLED,
                TrailerUserImpl.class, trailerUser.getPrimaryKey());
        }
    }

    /**
     * Creates a new trailer user with the primary key. Does not add the trailer user to the database.
     *
     * @param trailerUserId the primary key for the new trailer user
     * @return the new trailer user
     */
    @Override
    public TrailerUser create(long trailerUserId) {
        TrailerUser trailerUser = new TrailerUserImpl();

        trailerUser.setNew(true);
        trailerUser.setPrimaryKey(trailerUserId);

        return trailerUser;
    }

    /**
     * Removes the trailer user with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param trailerUserId the primary key of the trailer user
     * @return the trailer user that was removed
     * @throws de.humance.eco.profile.NoSuchTrailerUserException if a trailer user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerUser remove(long trailerUserId)
        throws NoSuchTrailerUserException, SystemException {
        return remove((Serializable) trailerUserId);
    }

    /**
     * Removes the trailer user with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the trailer user
     * @return the trailer user that was removed
     * @throws de.humance.eco.profile.NoSuchTrailerUserException if a trailer user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerUser remove(Serializable primaryKey)
        throws NoSuchTrailerUserException, SystemException {
        Session session = null;

        try {
            session = openSession();

            TrailerUser trailerUser = (TrailerUser) session.get(TrailerUserImpl.class,
                    primaryKey);

            if (trailerUser == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchTrailerUserException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(trailerUser);
        } catch (NoSuchTrailerUserException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected TrailerUser removeImpl(TrailerUser trailerUser)
        throws SystemException {
        trailerUser = toUnwrappedModel(trailerUser);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(trailerUser)) {
                trailerUser = (TrailerUser) session.get(TrailerUserImpl.class,
                        trailerUser.getPrimaryKeyObj());
            }

            if (trailerUser != null) {
                session.delete(trailerUser);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (trailerUser != null) {
            clearCache(trailerUser);
        }

        return trailerUser;
    }

    @Override
    public TrailerUser updateImpl(
        de.humance.eco.profile.model.TrailerUser trailerUser)
        throws SystemException {
        trailerUser = toUnwrappedModel(trailerUser);

        boolean isNew = trailerUser.isNew();

        TrailerUserModelImpl trailerUserModelImpl = (TrailerUserModelImpl) trailerUser;

        Session session = null;

        try {
            session = openSession();

            if (trailerUser.isNew()) {
                session.save(trailerUser);

                trailerUser.setNew(false);
            } else {
                session.merge(trailerUser);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !TrailerUserModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((trailerUserModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        trailerUserModelImpl.getOriginalUserId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
                    args);

                args = new Object[] { trailerUserModelImpl.getUserId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
                    args);
            }

            if ((trailerUserModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TRAILERID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        trailerUserModelImpl.getOriginalTrailerId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TRAILERID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TRAILERID,
                    args);

                args = new Object[] { trailerUserModelImpl.getTrailerId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TRAILERID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TRAILERID,
                    args);
            }
        }

        EntityCacheUtil.putResult(TrailerUserModelImpl.ENTITY_CACHE_ENABLED,
            TrailerUserImpl.class, trailerUser.getPrimaryKey(), trailerUser);

        return trailerUser;
    }

    protected TrailerUser toUnwrappedModel(TrailerUser trailerUser) {
        if (trailerUser instanceof TrailerUserImpl) {
            return trailerUser;
        }

        TrailerUserImpl trailerUserImpl = new TrailerUserImpl();

        trailerUserImpl.setNew(trailerUser.isNew());
        trailerUserImpl.setPrimaryKey(trailerUser.getPrimaryKey());

        trailerUserImpl.setTrailerUserId(trailerUser.getTrailerUserId());
        trailerUserImpl.setUserId(trailerUser.getUserId());
        trailerUserImpl.setTrailerId(trailerUser.getTrailerId());

        return trailerUserImpl;
    }

    /**
     * Returns the trailer user with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the trailer user
     * @return the trailer user
     * @throws de.humance.eco.profile.NoSuchTrailerUserException if a trailer user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerUser findByPrimaryKey(Serializable primaryKey)
        throws NoSuchTrailerUserException, SystemException {
        TrailerUser trailerUser = fetchByPrimaryKey(primaryKey);

        if (trailerUser == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchTrailerUserException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return trailerUser;
    }

    /**
     * Returns the trailer user with the primary key or throws a {@link de.humance.eco.profile.NoSuchTrailerUserException} if it could not be found.
     *
     * @param trailerUserId the primary key of the trailer user
     * @return the trailer user
     * @throws de.humance.eco.profile.NoSuchTrailerUserException if a trailer user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerUser findByPrimaryKey(long trailerUserId)
        throws NoSuchTrailerUserException, SystemException {
        return findByPrimaryKey((Serializable) trailerUserId);
    }

    /**
     * Returns the trailer user with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the trailer user
     * @return the trailer user, or <code>null</code> if a trailer user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerUser fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        TrailerUser trailerUser = (TrailerUser) EntityCacheUtil.getResult(TrailerUserModelImpl.ENTITY_CACHE_ENABLED,
                TrailerUserImpl.class, primaryKey);

        if (trailerUser == _nullTrailerUser) {
            return null;
        }

        if (trailerUser == null) {
            Session session = null;

            try {
                session = openSession();

                trailerUser = (TrailerUser) session.get(TrailerUserImpl.class,
                        primaryKey);

                if (trailerUser != null) {
                    cacheResult(trailerUser);
                } else {
                    EntityCacheUtil.putResult(TrailerUserModelImpl.ENTITY_CACHE_ENABLED,
                        TrailerUserImpl.class, primaryKey, _nullTrailerUser);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(TrailerUserModelImpl.ENTITY_CACHE_ENABLED,
                    TrailerUserImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return trailerUser;
    }

    /**
     * Returns the trailer user with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param trailerUserId the primary key of the trailer user
     * @return the trailer user, or <code>null</code> if a trailer user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrailerUser fetchByPrimaryKey(long trailerUserId)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) trailerUserId);
    }

    /**
     * Returns all the trailer users.
     *
     * @return the trailer users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerUser> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the trailer users.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of trailer users
     * @param end the upper bound of the range of trailer users (not inclusive)
     * @return the range of trailer users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerUser> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the trailer users.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of trailer users
     * @param end the upper bound of the range of trailer users (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of trailer users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<TrailerUser> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<TrailerUser> list = (List<TrailerUser>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_TRAILERUSER);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_TRAILERUSER;

                if (pagination) {
                    sql = sql.concat(TrailerUserModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<TrailerUser>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<TrailerUser>(list);
                } else {
                    list = (List<TrailerUser>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the trailer users from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (TrailerUser trailerUser : findAll()) {
            remove(trailerUser);
        }
    }

    /**
     * Returns the number of trailer users.
     *
     * @return the number of trailer users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_TRAILERUSER);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the trailer user persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.de.humance.eco.profile.model.TrailerUser")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<TrailerUser>> listenersList = new ArrayList<ModelListener<TrailerUser>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<TrailerUser>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(TrailerUserImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
