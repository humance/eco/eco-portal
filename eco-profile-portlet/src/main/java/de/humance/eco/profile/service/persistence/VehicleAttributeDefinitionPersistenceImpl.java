package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException;
import de.humance.eco.profile.model.VehicleAttributeDefinition;
import de.humance.eco.profile.model.impl.VehicleAttributeDefinitionImpl;
import de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl;
import de.humance.eco.profile.service.persistence.VehicleAttributeDefinitionPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the vehicle attribute definition service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehicleAttributeDefinitionPersistence
 * @see VehicleAttributeDefinitionUtil
 * @generated
 */
public class VehicleAttributeDefinitionPersistenceImpl
    extends BasePersistenceImpl<VehicleAttributeDefinition>
    implements VehicleAttributeDefinitionPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link VehicleAttributeDefinitionUtil} to access the vehicle attribute definition persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = VehicleAttributeDefinitionImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeDefinitionImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeDefinitionImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
            new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLETYPEID =
        new FinderPath(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeDefinitionImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByVehicleTypeId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEID =
        new FinderPath(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeDefinitionImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByVehicleTypeId",
            new String[] { Long.class.getName() },
            VehicleAttributeDefinitionModelImpl.VEHICLETYPEID_COLUMN_BITMASK |
            VehicleAttributeDefinitionModelImpl.SEQUENCENUMBER_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_VEHICLETYPEID = new FinderPath(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByVehicleTypeId", new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_VEHICLETYPEID_VEHICLETYPEID_2 = "vehicleAttributeDefinition.vehicleTypeId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TITLE = new FinderPath(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeDefinitionImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTitle",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE = new FinderPath(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeDefinitionImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByTitle",
            new String[] { String.class.getName() },
            VehicleAttributeDefinitionModelImpl.TITLE_COLUMN_BITMASK |
            VehicleAttributeDefinitionModelImpl.VEHICLETYPEID_COLUMN_BITMASK |
            VehicleAttributeDefinitionModelImpl.SEQUENCENUMBER_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_TITLE = new FinderPath(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByTitle", new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_TITLE_TITLE_1 = "vehicleAttributeDefinition.title IS NULL";
    private static final String _FINDER_COLUMN_TITLE_TITLE_2 = "lower(vehicleAttributeDefinition.title) = ?";
    private static final String _FINDER_COLUMN_TITLE_TITLE_3 = "(vehicleAttributeDefinition.title IS NULL OR vehicleAttributeDefinition.title = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TITLELIKE =
        new FinderPath(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeDefinitionImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTitleLike",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_TITLELIKE =
        new FinderPath(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "countByTitleLike", new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_TITLELIKE_TITLE_1 = "vehicleAttributeDefinition.title LIKE NULL";
    private static final String _FINDER_COLUMN_TITLELIKE_TITLE_2 = "lower(vehicleAttributeDefinition.title) LIKE ?";
    private static final String _FINDER_COLUMN_TITLELIKE_TITLE_3 = "(vehicleAttributeDefinition.title IS NULL OR vehicleAttributeDefinition.title LIKE '')";
    private static final String _SQL_SELECT_VEHICLEATTRIBUTEDEFINITION = "SELECT vehicleAttributeDefinition FROM VehicleAttributeDefinition vehicleAttributeDefinition";
    private static final String _SQL_SELECT_VEHICLEATTRIBUTEDEFINITION_WHERE = "SELECT vehicleAttributeDefinition FROM VehicleAttributeDefinition vehicleAttributeDefinition WHERE ";
    private static final String _SQL_COUNT_VEHICLEATTRIBUTEDEFINITION = "SELECT COUNT(vehicleAttributeDefinition) FROM VehicleAttributeDefinition vehicleAttributeDefinition";
    private static final String _SQL_COUNT_VEHICLEATTRIBUTEDEFINITION_WHERE = "SELECT COUNT(vehicleAttributeDefinition) FROM VehicleAttributeDefinition vehicleAttributeDefinition WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "vehicleAttributeDefinition.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No VehicleAttributeDefinition exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No VehicleAttributeDefinition exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(VehicleAttributeDefinitionPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "type"
            });
    private static VehicleAttributeDefinition _nullVehicleAttributeDefinition = new VehicleAttributeDefinitionImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<VehicleAttributeDefinition> toCacheModel() {
                return _nullVehicleAttributeDefinitionCacheModel;
            }
        };

    private static CacheModel<VehicleAttributeDefinition> _nullVehicleAttributeDefinitionCacheModel =
        new CacheModel<VehicleAttributeDefinition>() {
            @Override
            public VehicleAttributeDefinition toEntityModel() {
                return _nullVehicleAttributeDefinition;
            }
        };

    public VehicleAttributeDefinitionPersistenceImpl() {
        setModelClass(VehicleAttributeDefinition.class);
    }

    /**
     * Returns all the vehicle attribute definitions where vehicleTypeId = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @return the matching vehicle attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinition> findByVehicleTypeId(
        long vehicleTypeId) throws SystemException {
        return findByVehicleTypeId(vehicleTypeId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle attribute definitions where vehicleTypeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleTypeId the vehicle type ID
     * @param start the lower bound of the range of vehicle attribute definitions
     * @param end the upper bound of the range of vehicle attribute definitions (not inclusive)
     * @return the range of matching vehicle attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinition> findByVehicleTypeId(
        long vehicleTypeId, int start, int end) throws SystemException {
        return findByVehicleTypeId(vehicleTypeId, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle attribute definitions where vehicleTypeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleTypeId the vehicle type ID
     * @param start the lower bound of the range of vehicle attribute definitions
     * @param end the upper bound of the range of vehicle attribute definitions (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinition> findByVehicleTypeId(
        long vehicleTypeId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEID;
            finderArgs = new Object[] { vehicleTypeId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLETYPEID;
            finderArgs = new Object[] {
                    vehicleTypeId,
                    
                    start, end, orderByComparator
                };
        }

        List<VehicleAttributeDefinition> list = (List<VehicleAttributeDefinition>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleAttributeDefinition vehicleAttributeDefinition : list) {
                if ((vehicleTypeId != vehicleAttributeDefinition.getVehicleTypeId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLEATTRIBUTEDEFINITION_WHERE);

            query.append(_FINDER_COLUMN_VEHICLETYPEID_VEHICLETYPEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleAttributeDefinitionModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleTypeId);

                if (!pagination) {
                    list = (List<VehicleAttributeDefinition>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleAttributeDefinition>(list);
                } else {
                    list = (List<VehicleAttributeDefinition>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle attribute definition in the ordered set where vehicleTypeId = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute definition
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a matching vehicle attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition findByVehicleTypeId_First(
        long vehicleTypeId, OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionException, SystemException {
        VehicleAttributeDefinition vehicleAttributeDefinition = fetchByVehicleTypeId_First(vehicleTypeId,
                orderByComparator);

        if (vehicleAttributeDefinition != null) {
            return vehicleAttributeDefinition;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleTypeId=");
        msg.append(vehicleTypeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeDefinitionException(msg.toString());
    }

    /**
     * Returns the first vehicle attribute definition in the ordered set where vehicleTypeId = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute definition, or <code>null</code> if a matching vehicle attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition fetchByVehicleTypeId_First(
        long vehicleTypeId, OrderByComparator orderByComparator)
        throws SystemException {
        List<VehicleAttributeDefinition> list = findByVehicleTypeId(vehicleTypeId,
                0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle attribute definition in the ordered set where vehicleTypeId = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute definition
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a matching vehicle attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition findByVehicleTypeId_Last(
        long vehicleTypeId, OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionException, SystemException {
        VehicleAttributeDefinition vehicleAttributeDefinition = fetchByVehicleTypeId_Last(vehicleTypeId,
                orderByComparator);

        if (vehicleAttributeDefinition != null) {
            return vehicleAttributeDefinition;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleTypeId=");
        msg.append(vehicleTypeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeDefinitionException(msg.toString());
    }

    /**
     * Returns the last vehicle attribute definition in the ordered set where vehicleTypeId = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute definition, or <code>null</code> if a matching vehicle attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition fetchByVehicleTypeId_Last(
        long vehicleTypeId, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByVehicleTypeId(vehicleTypeId);

        if (count == 0) {
            return null;
        }

        List<VehicleAttributeDefinition> list = findByVehicleTypeId(vehicleTypeId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle attribute definitions before and after the current vehicle attribute definition in the ordered set where vehicleTypeId = &#63;.
     *
     * @param vehicleAttributeDefinitionId the primary key of the current vehicle attribute definition
     * @param vehicleTypeId the vehicle type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle attribute definition
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a vehicle attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition[] findByVehicleTypeId_PrevAndNext(
        long vehicleAttributeDefinitionId, long vehicleTypeId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionException, SystemException {
        VehicleAttributeDefinition vehicleAttributeDefinition = findByPrimaryKey(vehicleAttributeDefinitionId);

        Session session = null;

        try {
            session = openSession();

            VehicleAttributeDefinition[] array = new VehicleAttributeDefinitionImpl[3];

            array[0] = getByVehicleTypeId_PrevAndNext(session,
                    vehicleAttributeDefinition, vehicleTypeId,
                    orderByComparator, true);

            array[1] = vehicleAttributeDefinition;

            array[2] = getByVehicleTypeId_PrevAndNext(session,
                    vehicleAttributeDefinition, vehicleTypeId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleAttributeDefinition getByVehicleTypeId_PrevAndNext(
        Session session, VehicleAttributeDefinition vehicleAttributeDefinition,
        long vehicleTypeId, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLEATTRIBUTEDEFINITION_WHERE);

        query.append(_FINDER_COLUMN_VEHICLETYPEID_VEHICLETYPEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleAttributeDefinitionModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(vehicleTypeId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleAttributeDefinition);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleAttributeDefinition> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle attribute definitions where vehicleTypeId = &#63; from the database.
     *
     * @param vehicleTypeId the vehicle type ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByVehicleTypeId(long vehicleTypeId)
        throws SystemException {
        for (VehicleAttributeDefinition vehicleAttributeDefinition : findByVehicleTypeId(
                vehicleTypeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(vehicleAttributeDefinition);
        }
    }

    /**
     * Returns the number of vehicle attribute definitions where vehicleTypeId = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @return the number of matching vehicle attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByVehicleTypeId(long vehicleTypeId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_VEHICLETYPEID;

        Object[] finderArgs = new Object[] { vehicleTypeId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLEATTRIBUTEDEFINITION_WHERE);

            query.append(_FINDER_COLUMN_VEHICLETYPEID_VEHICLETYPEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleTypeId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicle attribute definitions where title = &#63;.
     *
     * @param title the title
     * @return the matching vehicle attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinition> findByTitle(String title)
        throws SystemException {
        return findByTitle(title, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle attribute definitions where title = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param title the title
     * @param start the lower bound of the range of vehicle attribute definitions
     * @param end the upper bound of the range of vehicle attribute definitions (not inclusive)
     * @return the range of matching vehicle attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinition> findByTitle(String title,
        int start, int end) throws SystemException {
        return findByTitle(title, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle attribute definitions where title = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param title the title
     * @param start the lower bound of the range of vehicle attribute definitions
     * @param end the upper bound of the range of vehicle attribute definitions (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinition> findByTitle(String title,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE;
            finderArgs = new Object[] { title };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TITLE;
            finderArgs = new Object[] { title, start, end, orderByComparator };
        }

        List<VehicleAttributeDefinition> list = (List<VehicleAttributeDefinition>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleAttributeDefinition vehicleAttributeDefinition : list) {
                if (!Validator.equals(title,
                            vehicleAttributeDefinition.getTitle())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLEATTRIBUTEDEFINITION_WHERE);

            boolean bindTitle = false;

            if (title == null) {
                query.append(_FINDER_COLUMN_TITLE_TITLE_1);
            } else if (title.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_TITLE_TITLE_3);
            } else {
                bindTitle = true;

                query.append(_FINDER_COLUMN_TITLE_TITLE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleAttributeDefinitionModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindTitle) {
                    qPos.add(title.toLowerCase());
                }

                if (!pagination) {
                    list = (List<VehicleAttributeDefinition>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleAttributeDefinition>(list);
                } else {
                    list = (List<VehicleAttributeDefinition>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle attribute definition in the ordered set where title = &#63;.
     *
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute definition
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a matching vehicle attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition findByTitle_First(String title,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionException, SystemException {
        VehicleAttributeDefinition vehicleAttributeDefinition = fetchByTitle_First(title,
                orderByComparator);

        if (vehicleAttributeDefinition != null) {
            return vehicleAttributeDefinition;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("title=");
        msg.append(title);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeDefinitionException(msg.toString());
    }

    /**
     * Returns the first vehicle attribute definition in the ordered set where title = &#63;.
     *
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute definition, or <code>null</code> if a matching vehicle attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition fetchByTitle_First(String title,
        OrderByComparator orderByComparator) throws SystemException {
        List<VehicleAttributeDefinition> list = findByTitle(title, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle attribute definition in the ordered set where title = &#63;.
     *
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute definition
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a matching vehicle attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition findByTitle_Last(String title,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionException, SystemException {
        VehicleAttributeDefinition vehicleAttributeDefinition = fetchByTitle_Last(title,
                orderByComparator);

        if (vehicleAttributeDefinition != null) {
            return vehicleAttributeDefinition;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("title=");
        msg.append(title);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeDefinitionException(msg.toString());
    }

    /**
     * Returns the last vehicle attribute definition in the ordered set where title = &#63;.
     *
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute definition, or <code>null</code> if a matching vehicle attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition fetchByTitle_Last(String title,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByTitle(title);

        if (count == 0) {
            return null;
        }

        List<VehicleAttributeDefinition> list = findByTitle(title, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle attribute definitions before and after the current vehicle attribute definition in the ordered set where title = &#63;.
     *
     * @param vehicleAttributeDefinitionId the primary key of the current vehicle attribute definition
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle attribute definition
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a vehicle attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition[] findByTitle_PrevAndNext(
        long vehicleAttributeDefinitionId, String title,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionException, SystemException {
        VehicleAttributeDefinition vehicleAttributeDefinition = findByPrimaryKey(vehicleAttributeDefinitionId);

        Session session = null;

        try {
            session = openSession();

            VehicleAttributeDefinition[] array = new VehicleAttributeDefinitionImpl[3];

            array[0] = getByTitle_PrevAndNext(session,
                    vehicleAttributeDefinition, title, orderByComparator, true);

            array[1] = vehicleAttributeDefinition;

            array[2] = getByTitle_PrevAndNext(session,
                    vehicleAttributeDefinition, title, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleAttributeDefinition getByTitle_PrevAndNext(
        Session session, VehicleAttributeDefinition vehicleAttributeDefinition,
        String title, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLEATTRIBUTEDEFINITION_WHERE);

        boolean bindTitle = false;

        if (title == null) {
            query.append(_FINDER_COLUMN_TITLE_TITLE_1);
        } else if (title.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_TITLE_TITLE_3);
        } else {
            bindTitle = true;

            query.append(_FINDER_COLUMN_TITLE_TITLE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleAttributeDefinitionModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindTitle) {
            qPos.add(title.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleAttributeDefinition);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleAttributeDefinition> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle attribute definitions where title = &#63; from the database.
     *
     * @param title the title
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByTitle(String title) throws SystemException {
        for (VehicleAttributeDefinition vehicleAttributeDefinition : findByTitle(
                title, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(vehicleAttributeDefinition);
        }
    }

    /**
     * Returns the number of vehicle attribute definitions where title = &#63;.
     *
     * @param title the title
     * @return the number of matching vehicle attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByTitle(String title) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_TITLE;

        Object[] finderArgs = new Object[] { title };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLEATTRIBUTEDEFINITION_WHERE);

            boolean bindTitle = false;

            if (title == null) {
                query.append(_FINDER_COLUMN_TITLE_TITLE_1);
            } else if (title.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_TITLE_TITLE_3);
            } else {
                bindTitle = true;

                query.append(_FINDER_COLUMN_TITLE_TITLE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindTitle) {
                    qPos.add(title.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicle attribute definitions where title LIKE &#63;.
     *
     * @param title the title
     * @return the matching vehicle attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinition> findByTitleLike(String title)
        throws SystemException {
        return findByTitleLike(title, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle attribute definitions where title LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param title the title
     * @param start the lower bound of the range of vehicle attribute definitions
     * @param end the upper bound of the range of vehicle attribute definitions (not inclusive)
     * @return the range of matching vehicle attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinition> findByTitleLike(String title,
        int start, int end) throws SystemException {
        return findByTitleLike(title, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle attribute definitions where title LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param title the title
     * @param start the lower bound of the range of vehicle attribute definitions
     * @param end the upper bound of the range of vehicle attribute definitions (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinition> findByTitleLike(String title,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TITLELIKE;
        finderArgs = new Object[] { title, start, end, orderByComparator };

        List<VehicleAttributeDefinition> list = (List<VehicleAttributeDefinition>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleAttributeDefinition vehicleAttributeDefinition : list) {
                if (!StringUtil.wildcardMatches(
                            vehicleAttributeDefinition.getTitle(), title,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, false)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLEATTRIBUTEDEFINITION_WHERE);

            boolean bindTitle = false;

            if (title == null) {
                query.append(_FINDER_COLUMN_TITLELIKE_TITLE_1);
            } else if (title.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_TITLELIKE_TITLE_3);
            } else {
                bindTitle = true;

                query.append(_FINDER_COLUMN_TITLELIKE_TITLE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleAttributeDefinitionModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindTitle) {
                    qPos.add(title.toLowerCase());
                }

                if (!pagination) {
                    list = (List<VehicleAttributeDefinition>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleAttributeDefinition>(list);
                } else {
                    list = (List<VehicleAttributeDefinition>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle attribute definition in the ordered set where title LIKE &#63;.
     *
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute definition
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a matching vehicle attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition findByTitleLike_First(String title,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionException, SystemException {
        VehicleAttributeDefinition vehicleAttributeDefinition = fetchByTitleLike_First(title,
                orderByComparator);

        if (vehicleAttributeDefinition != null) {
            return vehicleAttributeDefinition;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("title=");
        msg.append(title);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeDefinitionException(msg.toString());
    }

    /**
     * Returns the first vehicle attribute definition in the ordered set where title LIKE &#63;.
     *
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute definition, or <code>null</code> if a matching vehicle attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition fetchByTitleLike_First(String title,
        OrderByComparator orderByComparator) throws SystemException {
        List<VehicleAttributeDefinition> list = findByTitleLike(title, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle attribute definition in the ordered set where title LIKE &#63;.
     *
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute definition
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a matching vehicle attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition findByTitleLike_Last(String title,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionException, SystemException {
        VehicleAttributeDefinition vehicleAttributeDefinition = fetchByTitleLike_Last(title,
                orderByComparator);

        if (vehicleAttributeDefinition != null) {
            return vehicleAttributeDefinition;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("title=");
        msg.append(title);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeDefinitionException(msg.toString());
    }

    /**
     * Returns the last vehicle attribute definition in the ordered set where title LIKE &#63;.
     *
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute definition, or <code>null</code> if a matching vehicle attribute definition could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition fetchByTitleLike_Last(String title,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByTitleLike(title);

        if (count == 0) {
            return null;
        }

        List<VehicleAttributeDefinition> list = findByTitleLike(title,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle attribute definitions before and after the current vehicle attribute definition in the ordered set where title LIKE &#63;.
     *
     * @param vehicleAttributeDefinitionId the primary key of the current vehicle attribute definition
     * @param title the title
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle attribute definition
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a vehicle attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition[] findByTitleLike_PrevAndNext(
        long vehicleAttributeDefinitionId, String title,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionException, SystemException {
        VehicleAttributeDefinition vehicleAttributeDefinition = findByPrimaryKey(vehicleAttributeDefinitionId);

        Session session = null;

        try {
            session = openSession();

            VehicleAttributeDefinition[] array = new VehicleAttributeDefinitionImpl[3];

            array[0] = getByTitleLike_PrevAndNext(session,
                    vehicleAttributeDefinition, title, orderByComparator, true);

            array[1] = vehicleAttributeDefinition;

            array[2] = getByTitleLike_PrevAndNext(session,
                    vehicleAttributeDefinition, title, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleAttributeDefinition getByTitleLike_PrevAndNext(
        Session session, VehicleAttributeDefinition vehicleAttributeDefinition,
        String title, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLEATTRIBUTEDEFINITION_WHERE);

        boolean bindTitle = false;

        if (title == null) {
            query.append(_FINDER_COLUMN_TITLELIKE_TITLE_1);
        } else if (title.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_TITLELIKE_TITLE_3);
        } else {
            bindTitle = true;

            query.append(_FINDER_COLUMN_TITLELIKE_TITLE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleAttributeDefinitionModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindTitle) {
            qPos.add(title.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleAttributeDefinition);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleAttributeDefinition> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle attribute definitions where title LIKE &#63; from the database.
     *
     * @param title the title
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByTitleLike(String title) throws SystemException {
        for (VehicleAttributeDefinition vehicleAttributeDefinition : findByTitleLike(
                title, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(vehicleAttributeDefinition);
        }
    }

    /**
     * Returns the number of vehicle attribute definitions where title LIKE &#63;.
     *
     * @param title the title
     * @return the number of matching vehicle attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByTitleLike(String title) throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_TITLELIKE;

        Object[] finderArgs = new Object[] { title };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLEATTRIBUTEDEFINITION_WHERE);

            boolean bindTitle = false;

            if (title == null) {
                query.append(_FINDER_COLUMN_TITLELIKE_TITLE_1);
            } else if (title.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_TITLELIKE_TITLE_3);
            } else {
                bindTitle = true;

                query.append(_FINDER_COLUMN_TITLELIKE_TITLE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindTitle) {
                    qPos.add(title.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the vehicle attribute definition in the entity cache if it is enabled.
     *
     * @param vehicleAttributeDefinition the vehicle attribute definition
     */
    @Override
    public void cacheResult(
        VehicleAttributeDefinition vehicleAttributeDefinition) {
        EntityCacheUtil.putResult(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionImpl.class,
            vehicleAttributeDefinition.getPrimaryKey(),
            vehicleAttributeDefinition);

        vehicleAttributeDefinition.resetOriginalValues();
    }

    /**
     * Caches the vehicle attribute definitions in the entity cache if it is enabled.
     *
     * @param vehicleAttributeDefinitions the vehicle attribute definitions
     */
    @Override
    public void cacheResult(
        List<VehicleAttributeDefinition> vehicleAttributeDefinitions) {
        for (VehicleAttributeDefinition vehicleAttributeDefinition : vehicleAttributeDefinitions) {
            if (EntityCacheUtil.getResult(
                        VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleAttributeDefinitionImpl.class,
                        vehicleAttributeDefinition.getPrimaryKey()) == null) {
                cacheResult(vehicleAttributeDefinition);
            } else {
                vehicleAttributeDefinition.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all vehicle attribute definitions.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(VehicleAttributeDefinitionImpl.class.getName());
        }

        EntityCacheUtil.clearCache(VehicleAttributeDefinitionImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the vehicle attribute definition.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(
        VehicleAttributeDefinition vehicleAttributeDefinition) {
        EntityCacheUtil.removeResult(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionImpl.class,
            vehicleAttributeDefinition.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(
        List<VehicleAttributeDefinition> vehicleAttributeDefinitions) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (VehicleAttributeDefinition vehicleAttributeDefinition : vehicleAttributeDefinitions) {
            EntityCacheUtil.removeResult(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
                VehicleAttributeDefinitionImpl.class,
                vehicleAttributeDefinition.getPrimaryKey());
        }
    }

    /**
     * Creates a new vehicle attribute definition with the primary key. Does not add the vehicle attribute definition to the database.
     *
     * @param vehicleAttributeDefinitionId the primary key for the new vehicle attribute definition
     * @return the new vehicle attribute definition
     */
    @Override
    public VehicleAttributeDefinition create(long vehicleAttributeDefinitionId) {
        VehicleAttributeDefinition vehicleAttributeDefinition = new VehicleAttributeDefinitionImpl();

        vehicleAttributeDefinition.setNew(true);
        vehicleAttributeDefinition.setPrimaryKey(vehicleAttributeDefinitionId);

        return vehicleAttributeDefinition;
    }

    /**
     * Removes the vehicle attribute definition with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param vehicleAttributeDefinitionId the primary key of the vehicle attribute definition
     * @return the vehicle attribute definition that was removed
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a vehicle attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition remove(long vehicleAttributeDefinitionId)
        throws NoSuchVehicleAttributeDefinitionException, SystemException {
        return remove((Serializable) vehicleAttributeDefinitionId);
    }

    /**
     * Removes the vehicle attribute definition with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the vehicle attribute definition
     * @return the vehicle attribute definition that was removed
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a vehicle attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition remove(Serializable primaryKey)
        throws NoSuchVehicleAttributeDefinitionException, SystemException {
        Session session = null;

        try {
            session = openSession();

            VehicleAttributeDefinition vehicleAttributeDefinition = (VehicleAttributeDefinition) session.get(VehicleAttributeDefinitionImpl.class,
                    primaryKey);

            if (vehicleAttributeDefinition == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchVehicleAttributeDefinitionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(vehicleAttributeDefinition);
        } catch (NoSuchVehicleAttributeDefinitionException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected VehicleAttributeDefinition removeImpl(
        VehicleAttributeDefinition vehicleAttributeDefinition)
        throws SystemException {
        vehicleAttributeDefinition = toUnwrappedModel(vehicleAttributeDefinition);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(vehicleAttributeDefinition)) {
                vehicleAttributeDefinition = (VehicleAttributeDefinition) session.get(VehicleAttributeDefinitionImpl.class,
                        vehicleAttributeDefinition.getPrimaryKeyObj());
            }

            if (vehicleAttributeDefinition != null) {
                session.delete(vehicleAttributeDefinition);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (vehicleAttributeDefinition != null) {
            clearCache(vehicleAttributeDefinition);
        }

        return vehicleAttributeDefinition;
    }

    @Override
    public VehicleAttributeDefinition updateImpl(
        de.humance.eco.profile.model.VehicleAttributeDefinition vehicleAttributeDefinition)
        throws SystemException {
        vehicleAttributeDefinition = toUnwrappedModel(vehicleAttributeDefinition);

        boolean isNew = vehicleAttributeDefinition.isNew();

        VehicleAttributeDefinitionModelImpl vehicleAttributeDefinitionModelImpl = (VehicleAttributeDefinitionModelImpl) vehicleAttributeDefinition;

        Session session = null;

        try {
            session = openSession();

            if (vehicleAttributeDefinition.isNew()) {
                session.save(vehicleAttributeDefinition);

                vehicleAttributeDefinition.setNew(false);
            } else {
                session.merge(vehicleAttributeDefinition);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew ||
                !VehicleAttributeDefinitionModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((vehicleAttributeDefinitionModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleAttributeDefinitionModelImpl.getOriginalVehicleTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLETYPEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEID,
                    args);

                args = new Object[] {
                        vehicleAttributeDefinitionModelImpl.getVehicleTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLETYPEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEID,
                    args);
            }

            if ((vehicleAttributeDefinitionModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleAttributeDefinitionModelImpl.getOriginalTitle()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TITLE, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE,
                    args);

                args = new Object[] {
                        vehicleAttributeDefinitionModelImpl.getTitle()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TITLE, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITLE,
                    args);
            }
        }

        EntityCacheUtil.putResult(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionImpl.class,
            vehicleAttributeDefinition.getPrimaryKey(),
            vehicleAttributeDefinition);

        return vehicleAttributeDefinition;
    }

    protected VehicleAttributeDefinition toUnwrappedModel(
        VehicleAttributeDefinition vehicleAttributeDefinition) {
        if (vehicleAttributeDefinition instanceof VehicleAttributeDefinitionImpl) {
            return vehicleAttributeDefinition;
        }

        VehicleAttributeDefinitionImpl vehicleAttributeDefinitionImpl = new VehicleAttributeDefinitionImpl();

        vehicleAttributeDefinitionImpl.setNew(vehicleAttributeDefinition.isNew());
        vehicleAttributeDefinitionImpl.setPrimaryKey(vehicleAttributeDefinition.getPrimaryKey());

        vehicleAttributeDefinitionImpl.setVehicleAttributeDefinitionId(vehicleAttributeDefinition.getVehicleAttributeDefinitionId());
        vehicleAttributeDefinitionImpl.setCreatorId(vehicleAttributeDefinition.getCreatorId());
        vehicleAttributeDefinitionImpl.setCreatorName(vehicleAttributeDefinition.getCreatorName());
        vehicleAttributeDefinitionImpl.setCreateDate(vehicleAttributeDefinition.getCreateDate());
        vehicleAttributeDefinitionImpl.setModifierId(vehicleAttributeDefinition.getModifierId());
        vehicleAttributeDefinitionImpl.setModifierName(vehicleAttributeDefinition.getModifierName());
        vehicleAttributeDefinitionImpl.setModifiedDate(vehicleAttributeDefinition.getModifiedDate());
        vehicleAttributeDefinitionImpl.setVehicleTypeId(vehicleAttributeDefinition.getVehicleTypeId());
        vehicleAttributeDefinitionImpl.setType(vehicleAttributeDefinition.getType());
        vehicleAttributeDefinitionImpl.setTitle(vehicleAttributeDefinition.getTitle());
        vehicleAttributeDefinitionImpl.setMinRangeValue(vehicleAttributeDefinition.getMinRangeValue());
        vehicleAttributeDefinitionImpl.setMaxRangeValue(vehicleAttributeDefinition.getMaxRangeValue());
        vehicleAttributeDefinitionImpl.setUnit(vehicleAttributeDefinition.getUnit());
        vehicleAttributeDefinitionImpl.setSequenceNumber(vehicleAttributeDefinition.getSequenceNumber());

        return vehicleAttributeDefinitionImpl;
    }

    /**
     * Returns the vehicle attribute definition with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle attribute definition
     * @return the vehicle attribute definition
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a vehicle attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition findByPrimaryKey(Serializable primaryKey)
        throws NoSuchVehicleAttributeDefinitionException, SystemException {
        VehicleAttributeDefinition vehicleAttributeDefinition = fetchByPrimaryKey(primaryKey);

        if (vehicleAttributeDefinition == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchVehicleAttributeDefinitionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return vehicleAttributeDefinition;
    }

    /**
     * Returns the vehicle attribute definition with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException} if it could not be found.
     *
     * @param vehicleAttributeDefinitionId the primary key of the vehicle attribute definition
     * @return the vehicle attribute definition
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a vehicle attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition findByPrimaryKey(
        long vehicleAttributeDefinitionId)
        throws NoSuchVehicleAttributeDefinitionException, SystemException {
        return findByPrimaryKey((Serializable) vehicleAttributeDefinitionId);
    }

    /**
     * Returns the vehicle attribute definition with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle attribute definition
     * @return the vehicle attribute definition, or <code>null</code> if a vehicle attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        VehicleAttributeDefinition vehicleAttributeDefinition = (VehicleAttributeDefinition) EntityCacheUtil.getResult(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
                VehicleAttributeDefinitionImpl.class, primaryKey);

        if (vehicleAttributeDefinition == _nullVehicleAttributeDefinition) {
            return null;
        }

        if (vehicleAttributeDefinition == null) {
            Session session = null;

            try {
                session = openSession();

                vehicleAttributeDefinition = (VehicleAttributeDefinition) session.get(VehicleAttributeDefinitionImpl.class,
                        primaryKey);

                if (vehicleAttributeDefinition != null) {
                    cacheResult(vehicleAttributeDefinition);
                } else {
                    EntityCacheUtil.putResult(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleAttributeDefinitionImpl.class, primaryKey,
                        _nullVehicleAttributeDefinition);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(VehicleAttributeDefinitionModelImpl.ENTITY_CACHE_ENABLED,
                    VehicleAttributeDefinitionImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return vehicleAttributeDefinition;
    }

    /**
     * Returns the vehicle attribute definition with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param vehicleAttributeDefinitionId the primary key of the vehicle attribute definition
     * @return the vehicle attribute definition, or <code>null</code> if a vehicle attribute definition with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinition fetchByPrimaryKey(
        long vehicleAttributeDefinitionId) throws SystemException {
        return fetchByPrimaryKey((Serializable) vehicleAttributeDefinitionId);
    }

    /**
     * Returns all the vehicle attribute definitions.
     *
     * @return the vehicle attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinition> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle attribute definitions.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicle attribute definitions
     * @param end the upper bound of the range of vehicle attribute definitions (not inclusive)
     * @return the range of vehicle attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinition> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle attribute definitions.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicle attribute definitions
     * @param end the upper bound of the range of vehicle attribute definitions (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of vehicle attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinition> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<VehicleAttributeDefinition> list = (List<VehicleAttributeDefinition>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_VEHICLEATTRIBUTEDEFINITION);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_VEHICLEATTRIBUTEDEFINITION;

                if (pagination) {
                    sql = sql.concat(VehicleAttributeDefinitionModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<VehicleAttributeDefinition>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleAttributeDefinition>(list);
                } else {
                    list = (List<VehicleAttributeDefinition>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the vehicle attribute definitions from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (VehicleAttributeDefinition vehicleAttributeDefinition : findAll()) {
            remove(vehicleAttributeDefinition);
        }
    }

    /**
     * Returns the number of vehicle attribute definitions.
     *
     * @return the number of vehicle attribute definitions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_VEHICLEATTRIBUTEDEFINITION);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the vehicle attribute definition persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.de.humance.eco.profile.model.VehicleAttributeDefinition")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<VehicleAttributeDefinition>> listenersList = new ArrayList<ModelListener<VehicleAttributeDefinition>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<VehicleAttributeDefinition>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(VehicleAttributeDefinitionImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
