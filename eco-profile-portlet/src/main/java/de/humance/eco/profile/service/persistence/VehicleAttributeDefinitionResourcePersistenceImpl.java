package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException;
import de.humance.eco.profile.model.VehicleAttributeDefinitionResource;
import de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceImpl;
import de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl;
import de.humance.eco.profile.service.persistence.VehicleAttributeDefinitionResourcePersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the vehicle attribute definition resource service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehicleAttributeDefinitionResourcePersistence
 * @see VehicleAttributeDefinitionResourceUtil
 * @generated
 */
public class VehicleAttributeDefinitionResourcePersistenceImpl
    extends BasePersistenceImpl<VehicleAttributeDefinitionResource>
    implements VehicleAttributeDefinitionResourcePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link VehicleAttributeDefinitionResourceUtil} to access the vehicle attribute definition resource persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = VehicleAttributeDefinitionResourceImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
            new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONID =
        new FinderPath(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByVehicleAttributeDefinitionId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONID =
        new FinderPath(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByVehicleAttributeDefinitionId",
            new String[] { Long.class.getName() },
            VehicleAttributeDefinitionResourceModelImpl.VEHICLEATTRIBUTEDEFINITIONID_COLUMN_BITMASK |
            VehicleAttributeDefinitionResourceModelImpl.COUNTRY_COLUMN_BITMASK |
            VehicleAttributeDefinitionResourceModelImpl.LANGUAGE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONID =
        new FinderPath(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByVehicleAttributeDefinitionId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONID_VEHICLEATTRIBUTEDEFINITIONID_2 =
        "vehicleAttributeDefinitionResource.vehicleAttributeDefinitionId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY =
        new FinderPath(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByVehicleAttributeDefinitionIdAndCountry",
            new String[] {
                Long.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY =
        new FinderPath(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByVehicleAttributeDefinitionIdAndCountry",
            new String[] { Long.class.getName(), String.class.getName() },
            VehicleAttributeDefinitionResourceModelImpl.VEHICLEATTRIBUTEDEFINITIONID_COLUMN_BITMASK |
            VehicleAttributeDefinitionResourceModelImpl.COUNTRY_COLUMN_BITMASK |
            VehicleAttributeDefinitionResourceModelImpl.LANGUAGE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY =
        new FinderPath(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByVehicleAttributeDefinitionIdAndCountry",
            new String[] { Long.class.getName(), String.class.getName() });
    private static final String _FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY_VEHICLEATTRIBUTEDEFINITIONID_2 =
        "vehicleAttributeDefinitionResource.vehicleAttributeDefinitionId = ? AND ";
    private static final String _FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY_COUNTRY_1 =
        "vehicleAttributeDefinitionResource.country IS NULL";
    private static final String _FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY_COUNTRY_2 =
        "lower(vehicleAttributeDefinitionResource.country) = ?";
    private static final String _FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY_COUNTRY_3 =
        "(vehicleAttributeDefinitionResource.country IS NULL OR vehicleAttributeDefinitionResource.country = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE =
        new FinderPath(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByVehicleAttributeDefinitionIdAndLanguage",
            new String[] {
                Long.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE =
        new FinderPath(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByVehicleAttributeDefinitionIdAndLanguage",
            new String[] { Long.class.getName(), String.class.getName() },
            VehicleAttributeDefinitionResourceModelImpl.VEHICLEATTRIBUTEDEFINITIONID_COLUMN_BITMASK |
            VehicleAttributeDefinitionResourceModelImpl.LANGUAGE_COLUMN_BITMASK |
            VehicleAttributeDefinitionResourceModelImpl.COUNTRY_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE =
        new FinderPath(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByVehicleAttributeDefinitionIdAndLanguage",
            new String[] { Long.class.getName(), String.class.getName() });
    private static final String _FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE_VEHICLEATTRIBUTEDEFINITIONID_2 =
        "vehicleAttributeDefinitionResource.vehicleAttributeDefinitionId = ? AND ";
    private static final String _FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE_LANGUAGE_1 =
        "vehicleAttributeDefinitionResource.language IS NULL";
    private static final String _FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE_LANGUAGE_2 =
        "lower(vehicleAttributeDefinitionResource.language) = ?";
    private static final String _FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE_LANGUAGE_3 =
        "(vehicleAttributeDefinitionResource.language IS NULL OR vehicleAttributeDefinitionResource.language = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE =
        new FinderPath(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByVehicleAttributeDefinitionIdAndLocale",
            new String[] {
                Long.class.getName(), String.class.getName(),
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE =
        new FinderPath(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByVehicleAttributeDefinitionIdAndLocale",
            new String[] {
                Long.class.getName(), String.class.getName(),
                String.class.getName()
            },
            VehicleAttributeDefinitionResourceModelImpl.VEHICLEATTRIBUTEDEFINITIONID_COLUMN_BITMASK |
            VehicleAttributeDefinitionResourceModelImpl.COUNTRY_COLUMN_BITMASK |
            VehicleAttributeDefinitionResourceModelImpl.LANGUAGE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE =
        new FinderPath(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceModelImpl.FINDER_CACHE_ENABLED,
            Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByVehicleAttributeDefinitionIdAndLocale",
            new String[] {
                Long.class.getName(), String.class.getName(),
                String.class.getName()
            });
    private static final String _FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_VEHICLEATTRIBUTEDEFINITIONID_2 =
        "vehicleAttributeDefinitionResource.vehicleAttributeDefinitionId = ? AND ";
    private static final String _FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_COUNTRY_1 =
        "vehicleAttributeDefinitionResource.country IS NULL AND ";
    private static final String _FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_COUNTRY_2 =
        "lower(vehicleAttributeDefinitionResource.country) = ? AND ";
    private static final String _FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_COUNTRY_3 =
        "(vehicleAttributeDefinitionResource.country IS NULL OR vehicleAttributeDefinitionResource.country = '') AND ";
    private static final String _FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_LANGUAGE_1 =
        "vehicleAttributeDefinitionResource.language IS NULL";
    private static final String _FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_LANGUAGE_2 =
        "lower(vehicleAttributeDefinitionResource.language) = ?";
    private static final String _FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_LANGUAGE_3 =
        "(vehicleAttributeDefinitionResource.language IS NULL OR vehicleAttributeDefinitionResource.language = '')";
    private static final String _SQL_SELECT_VEHICLEATTRIBUTEDEFINITIONRESOURCE = "SELECT vehicleAttributeDefinitionResource FROM VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource";
    private static final String _SQL_SELECT_VEHICLEATTRIBUTEDEFINITIONRESOURCE_WHERE =
        "SELECT vehicleAttributeDefinitionResource FROM VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource WHERE ";
    private static final String _SQL_COUNT_VEHICLEATTRIBUTEDEFINITIONRESOURCE = "SELECT COUNT(vehicleAttributeDefinitionResource) FROM VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource";
    private static final String _SQL_COUNT_VEHICLEATTRIBUTEDEFINITIONRESOURCE_WHERE =
        "SELECT COUNT(vehicleAttributeDefinitionResource) FROM VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "vehicleAttributeDefinitionResource.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No VehicleAttributeDefinitionResource exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No VehicleAttributeDefinitionResource exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(VehicleAttributeDefinitionResourcePersistenceImpl.class);
    private static VehicleAttributeDefinitionResource _nullVehicleAttributeDefinitionResource =
        new VehicleAttributeDefinitionResourceImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<VehicleAttributeDefinitionResource> toCacheModel() {
                return _nullVehicleAttributeDefinitionResourceCacheModel;
            }
        };

    private static CacheModel<VehicleAttributeDefinitionResource> _nullVehicleAttributeDefinitionResourceCacheModel =
        new CacheModel<VehicleAttributeDefinitionResource>() {
            @Override
            public VehicleAttributeDefinitionResource toEntityModel() {
                return _nullVehicleAttributeDefinitionResource;
            }
        };

    public VehicleAttributeDefinitionResourcePersistenceImpl() {
        setModelClass(VehicleAttributeDefinitionResource.class);
    }

    /**
     * Returns all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @return the matching vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId) throws SystemException {
        return findByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param start the lower bound of the range of vehicle attribute definition resources
     * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
     * @return the range of matching vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId, int start, int end)
        throws SystemException {
        return findByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId,
            start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param start the lower bound of the range of vehicle attribute definition resources
     * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONID;
            finderArgs = new Object[] { vehicleAttributeDefinitionId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONID;
            finderArgs = new Object[] {
                    vehicleAttributeDefinitionId,
                    
                    start, end, orderByComparator
                };
        }

        List<VehicleAttributeDefinitionResource> list = (List<VehicleAttributeDefinitionResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource : list) {
                if ((vehicleAttributeDefinitionId != vehicleAttributeDefinitionResource.getVehicleAttributeDefinitionId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLEATTRIBUTEDEFINITIONRESOURCE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONID_VEHICLEATTRIBUTEDEFINITIONID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleAttributeDefinitionId);

                if (!pagination) {
                    list = (List<VehicleAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleAttributeDefinitionResource>(list);
                } else {
                    list = (List<VehicleAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute definition resource
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionId_First(
        long vehicleAttributeDefinitionId, OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionResourceException,
            SystemException {
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource = fetchByVehicleAttributeDefinitionId_First(vehicleAttributeDefinitionId,
                orderByComparator);

        if (vehicleAttributeDefinitionResource != null) {
            return vehicleAttributeDefinitionResource;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleAttributeDefinitionId=");
        msg.append(vehicleAttributeDefinitionId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeDefinitionResourceException(msg.toString());
    }

    /**
     * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionId_First(
        long vehicleAttributeDefinitionId, OrderByComparator orderByComparator)
        throws SystemException {
        List<VehicleAttributeDefinitionResource> list = findByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId,
                0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute definition resource
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionId_Last(
        long vehicleAttributeDefinitionId, OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionResourceException,
            SystemException {
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource = fetchByVehicleAttributeDefinitionId_Last(vehicleAttributeDefinitionId,
                orderByComparator);

        if (vehicleAttributeDefinitionResource != null) {
            return vehicleAttributeDefinitionResource;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleAttributeDefinitionId=");
        msg.append(vehicleAttributeDefinitionId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeDefinitionResourceException(msg.toString());
    }

    /**
     * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionId_Last(
        long vehicleAttributeDefinitionId, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);

        if (count == 0) {
            return null;
        }

        List<VehicleAttributeDefinitionResource> list = findByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle attribute definition resources before and after the current vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleAttributeDefinitionResourceId the primary key of the current vehicle attribute definition resource
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle attribute definition resource
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource[] findByVehicleAttributeDefinitionId_PrevAndNext(
        long vehicleAttributeDefinitionResourceId,
        long vehicleAttributeDefinitionId, OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionResourceException,
            SystemException {
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource = findByPrimaryKey(vehicleAttributeDefinitionResourceId);

        Session session = null;

        try {
            session = openSession();

            VehicleAttributeDefinitionResource[] array = new VehicleAttributeDefinitionResourceImpl[3];

            array[0] = getByVehicleAttributeDefinitionId_PrevAndNext(session,
                    vehicleAttributeDefinitionResource,
                    vehicleAttributeDefinitionId, orderByComparator, true);

            array[1] = vehicleAttributeDefinitionResource;

            array[2] = getByVehicleAttributeDefinitionId_PrevAndNext(session,
                    vehicleAttributeDefinitionResource,
                    vehicleAttributeDefinitionId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleAttributeDefinitionResource getByVehicleAttributeDefinitionId_PrevAndNext(
        Session session,
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource,
        long vehicleAttributeDefinitionId, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLEATTRIBUTEDEFINITIONRESOURCE_WHERE);

        query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONID_VEHICLEATTRIBUTEDEFINITIONID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(vehicleAttributeDefinitionId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleAttributeDefinitionResource);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleAttributeDefinitionResource> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; from the database.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId) throws SystemException {
        for (VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource : findByVehicleAttributeDefinitionId(
                vehicleAttributeDefinitionId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(vehicleAttributeDefinitionResource);
        }
    }

    /**
     * Returns the number of vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @return the number of matching vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONID;

        Object[] finderArgs = new Object[] { vehicleAttributeDefinitionId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLEATTRIBUTEDEFINITIONRESOURCE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONID_VEHICLEATTRIBUTEDEFINITIONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleAttributeDefinitionId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @return the matching vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndCountry(
        long vehicleAttributeDefinitionId, String country)
        throws SystemException {
        return findByVehicleAttributeDefinitionIdAndCountry(vehicleAttributeDefinitionId,
            country, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @param start the lower bound of the range of vehicle attribute definition resources
     * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
     * @return the range of matching vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndCountry(
        long vehicleAttributeDefinitionId, String country, int start, int end)
        throws SystemException {
        return findByVehicleAttributeDefinitionIdAndCountry(vehicleAttributeDefinitionId,
            country, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @param start the lower bound of the range of vehicle attribute definition resources
     * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndCountry(
        long vehicleAttributeDefinitionId, String country, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY;
            finderArgs = new Object[] { vehicleAttributeDefinitionId, country };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY;
            finderArgs = new Object[] {
                    vehicleAttributeDefinitionId, country,
                    
                    start, end, orderByComparator
                };
        }

        List<VehicleAttributeDefinitionResource> list = (List<VehicleAttributeDefinitionResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource : list) {
                if ((vehicleAttributeDefinitionId != vehicleAttributeDefinitionResource.getVehicleAttributeDefinitionId()) ||
                        !Validator.equals(country,
                            vehicleAttributeDefinitionResource.getCountry())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_VEHICLEATTRIBUTEDEFINITIONRESOURCE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY_VEHICLEATTRIBUTEDEFINITIONID_2);

            boolean bindCountry = false;

            if (country == null) {
                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY_COUNTRY_1);
            } else if (country.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY_COUNTRY_3);
            } else {
                bindCountry = true;

                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY_COUNTRY_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleAttributeDefinitionId);

                if (bindCountry) {
                    qPos.add(country.toLowerCase());
                }

                if (!pagination) {
                    list = (List<VehicleAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleAttributeDefinitionResource>(list);
                } else {
                    list = (List<VehicleAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute definition resource
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndCountry_First(
        long vehicleAttributeDefinitionId, String country,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionResourceException,
            SystemException {
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource = fetchByVehicleAttributeDefinitionIdAndCountry_First(vehicleAttributeDefinitionId,
                country, orderByComparator);

        if (vehicleAttributeDefinitionResource != null) {
            return vehicleAttributeDefinitionResource;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleAttributeDefinitionId=");
        msg.append(vehicleAttributeDefinitionId);

        msg.append(", country=");
        msg.append(country);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeDefinitionResourceException(msg.toString());
    }

    /**
     * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndCountry_First(
        long vehicleAttributeDefinitionId, String country,
        OrderByComparator orderByComparator) throws SystemException {
        List<VehicleAttributeDefinitionResource> list = findByVehicleAttributeDefinitionIdAndCountry(vehicleAttributeDefinitionId,
                country, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute definition resource
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndCountry_Last(
        long vehicleAttributeDefinitionId, String country,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionResourceException,
            SystemException {
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource = fetchByVehicleAttributeDefinitionIdAndCountry_Last(vehicleAttributeDefinitionId,
                country, orderByComparator);

        if (vehicleAttributeDefinitionResource != null) {
            return vehicleAttributeDefinitionResource;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleAttributeDefinitionId=");
        msg.append(vehicleAttributeDefinitionId);

        msg.append(", country=");
        msg.append(country);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeDefinitionResourceException(msg.toString());
    }

    /**
     * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndCountry_Last(
        long vehicleAttributeDefinitionId, String country,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByVehicleAttributeDefinitionIdAndCountry(vehicleAttributeDefinitionId,
                country);

        if (count == 0) {
            return null;
        }

        List<VehicleAttributeDefinitionResource> list = findByVehicleAttributeDefinitionIdAndCountry(vehicleAttributeDefinitionId,
                country, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle attribute definition resources before and after the current vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63;.
     *
     * @param vehicleAttributeDefinitionResourceId the primary key of the current vehicle attribute definition resource
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle attribute definition resource
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource[] findByVehicleAttributeDefinitionIdAndCountry_PrevAndNext(
        long vehicleAttributeDefinitionResourceId,
        long vehicleAttributeDefinitionId, String country,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionResourceException,
            SystemException {
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource = findByPrimaryKey(vehicleAttributeDefinitionResourceId);

        Session session = null;

        try {
            session = openSession();

            VehicleAttributeDefinitionResource[] array = new VehicleAttributeDefinitionResourceImpl[3];

            array[0] = getByVehicleAttributeDefinitionIdAndCountry_PrevAndNext(session,
                    vehicleAttributeDefinitionResource,
                    vehicleAttributeDefinitionId, country, orderByComparator,
                    true);

            array[1] = vehicleAttributeDefinitionResource;

            array[2] = getByVehicleAttributeDefinitionIdAndCountry_PrevAndNext(session,
                    vehicleAttributeDefinitionResource,
                    vehicleAttributeDefinitionId, country, orderByComparator,
                    false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleAttributeDefinitionResource getByVehicleAttributeDefinitionIdAndCountry_PrevAndNext(
        Session session,
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource,
        long vehicleAttributeDefinitionId, String country,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLEATTRIBUTEDEFINITIONRESOURCE_WHERE);

        query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY_VEHICLEATTRIBUTEDEFINITIONID_2);

        boolean bindCountry = false;

        if (country == null) {
            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY_COUNTRY_1);
        } else if (country.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY_COUNTRY_3);
        } else {
            bindCountry = true;

            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY_COUNTRY_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(vehicleAttributeDefinitionId);

        if (bindCountry) {
            qPos.add(country.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleAttributeDefinitionResource);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleAttributeDefinitionResource> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; from the database.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByVehicleAttributeDefinitionIdAndCountry(
        long vehicleAttributeDefinitionId, String country)
        throws SystemException {
        for (VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource : findByVehicleAttributeDefinitionIdAndCountry(
                vehicleAttributeDefinitionId, country, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(vehicleAttributeDefinitionResource);
        }
    }

    /**
     * Returns the number of vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @return the number of matching vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByVehicleAttributeDefinitionIdAndCountry(
        long vehicleAttributeDefinitionId, String country)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY;

        Object[] finderArgs = new Object[] { vehicleAttributeDefinitionId, country };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_VEHICLEATTRIBUTEDEFINITIONRESOURCE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY_VEHICLEATTRIBUTEDEFINITIONID_2);

            boolean bindCountry = false;

            if (country == null) {
                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY_COUNTRY_1);
            } else if (country.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY_COUNTRY_3);
            } else {
                bindCountry = true;

                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY_COUNTRY_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleAttributeDefinitionId);

                if (bindCountry) {
                    qPos.add(country.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and language = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param language the language
     * @return the matching vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLanguage(
        long vehicleAttributeDefinitionId, String language)
        throws SystemException {
        return findByVehicleAttributeDefinitionIdAndLanguage(vehicleAttributeDefinitionId,
            language, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and language = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param language the language
     * @param start the lower bound of the range of vehicle attribute definition resources
     * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
     * @return the range of matching vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLanguage(
        long vehicleAttributeDefinitionId, String language, int start, int end)
        throws SystemException {
        return findByVehicleAttributeDefinitionIdAndLanguage(vehicleAttributeDefinitionId,
            language, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and language = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param language the language
     * @param start the lower bound of the range of vehicle attribute definition resources
     * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLanguage(
        long vehicleAttributeDefinitionId, String language, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE;
            finderArgs = new Object[] { vehicleAttributeDefinitionId, language };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE;
            finderArgs = new Object[] {
                    vehicleAttributeDefinitionId, language,
                    
                    start, end, orderByComparator
                };
        }

        List<VehicleAttributeDefinitionResource> list = (List<VehicleAttributeDefinitionResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource : list) {
                if ((vehicleAttributeDefinitionId != vehicleAttributeDefinitionResource.getVehicleAttributeDefinitionId()) ||
                        !Validator.equals(language,
                            vehicleAttributeDefinitionResource.getLanguage())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_VEHICLEATTRIBUTEDEFINITIONRESOURCE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE_VEHICLEATTRIBUTEDEFINITIONID_2);

            boolean bindLanguage = false;

            if (language == null) {
                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE_LANGUAGE_1);
            } else if (language.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE_LANGUAGE_3);
            } else {
                bindLanguage = true;

                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE_LANGUAGE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleAttributeDefinitionId);

                if (bindLanguage) {
                    qPos.add(language.toLowerCase());
                }

                if (!pagination) {
                    list = (List<VehicleAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleAttributeDefinitionResource>(list);
                } else {
                    list = (List<VehicleAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and language = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute definition resource
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndLanguage_First(
        long vehicleAttributeDefinitionId, String language,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionResourceException,
            SystemException {
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource = fetchByVehicleAttributeDefinitionIdAndLanguage_First(vehicleAttributeDefinitionId,
                language, orderByComparator);

        if (vehicleAttributeDefinitionResource != null) {
            return vehicleAttributeDefinitionResource;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleAttributeDefinitionId=");
        msg.append(vehicleAttributeDefinitionId);

        msg.append(", language=");
        msg.append(language);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeDefinitionResourceException(msg.toString());
    }

    /**
     * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and language = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndLanguage_First(
        long vehicleAttributeDefinitionId, String language,
        OrderByComparator orderByComparator) throws SystemException {
        List<VehicleAttributeDefinitionResource> list = findByVehicleAttributeDefinitionIdAndLanguage(vehicleAttributeDefinitionId,
                language, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and language = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute definition resource
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndLanguage_Last(
        long vehicleAttributeDefinitionId, String language,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionResourceException,
            SystemException {
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource = fetchByVehicleAttributeDefinitionIdAndLanguage_Last(vehicleAttributeDefinitionId,
                language, orderByComparator);

        if (vehicleAttributeDefinitionResource != null) {
            return vehicleAttributeDefinitionResource;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleAttributeDefinitionId=");
        msg.append(vehicleAttributeDefinitionId);

        msg.append(", language=");
        msg.append(language);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeDefinitionResourceException(msg.toString());
    }

    /**
     * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and language = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndLanguage_Last(
        long vehicleAttributeDefinitionId, String language,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByVehicleAttributeDefinitionIdAndLanguage(vehicleAttributeDefinitionId,
                language);

        if (count == 0) {
            return null;
        }

        List<VehicleAttributeDefinitionResource> list = findByVehicleAttributeDefinitionIdAndLanguage(vehicleAttributeDefinitionId,
                language, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle attribute definition resources before and after the current vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and language = &#63;.
     *
     * @param vehicleAttributeDefinitionResourceId the primary key of the current vehicle attribute definition resource
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle attribute definition resource
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource[] findByVehicleAttributeDefinitionIdAndLanguage_PrevAndNext(
        long vehicleAttributeDefinitionResourceId,
        long vehicleAttributeDefinitionId, String language,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionResourceException,
            SystemException {
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource = findByPrimaryKey(vehicleAttributeDefinitionResourceId);

        Session session = null;

        try {
            session = openSession();

            VehicleAttributeDefinitionResource[] array = new VehicleAttributeDefinitionResourceImpl[3];

            array[0] = getByVehicleAttributeDefinitionIdAndLanguage_PrevAndNext(session,
                    vehicleAttributeDefinitionResource,
                    vehicleAttributeDefinitionId, language, orderByComparator,
                    true);

            array[1] = vehicleAttributeDefinitionResource;

            array[2] = getByVehicleAttributeDefinitionIdAndLanguage_PrevAndNext(session,
                    vehicleAttributeDefinitionResource,
                    vehicleAttributeDefinitionId, language, orderByComparator,
                    false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleAttributeDefinitionResource getByVehicleAttributeDefinitionIdAndLanguage_PrevAndNext(
        Session session,
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource,
        long vehicleAttributeDefinitionId, String language,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLEATTRIBUTEDEFINITIONRESOURCE_WHERE);

        query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE_VEHICLEATTRIBUTEDEFINITIONID_2);

        boolean bindLanguage = false;

        if (language == null) {
            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE_LANGUAGE_1);
        } else if (language.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE_LANGUAGE_3);
        } else {
            bindLanguage = true;

            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE_LANGUAGE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(vehicleAttributeDefinitionId);

        if (bindLanguage) {
            qPos.add(language.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleAttributeDefinitionResource);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleAttributeDefinitionResource> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and language = &#63; from the database.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param language the language
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByVehicleAttributeDefinitionIdAndLanguage(
        long vehicleAttributeDefinitionId, String language)
        throws SystemException {
        for (VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource : findByVehicleAttributeDefinitionIdAndLanguage(
                vehicleAttributeDefinitionId, language, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(vehicleAttributeDefinitionResource);
        }
    }

    /**
     * Returns the number of vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and language = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param language the language
     * @return the number of matching vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByVehicleAttributeDefinitionIdAndLanguage(
        long vehicleAttributeDefinitionId, String language)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE;

        Object[] finderArgs = new Object[] {
                vehicleAttributeDefinitionId, language
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_VEHICLEATTRIBUTEDEFINITIONRESOURCE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE_VEHICLEATTRIBUTEDEFINITIONID_2);

            boolean bindLanguage = false;

            if (language == null) {
                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE_LANGUAGE_1);
            } else if (language.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE_LANGUAGE_3);
            } else {
                bindLanguage = true;

                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE_LANGUAGE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleAttributeDefinitionId);

                if (bindLanguage) {
                    qPos.add(language.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @param language the language
     * @return the matching vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLocale(
        long vehicleAttributeDefinitionId, String country, String language)
        throws SystemException {
        return findByVehicleAttributeDefinitionIdAndLocale(vehicleAttributeDefinitionId,
            country, language, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @param language the language
     * @param start the lower bound of the range of vehicle attribute definition resources
     * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
     * @return the range of matching vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLocale(
        long vehicleAttributeDefinitionId, String country, String language,
        int start, int end) throws SystemException {
        return findByVehicleAttributeDefinitionIdAndLocale(vehicleAttributeDefinitionId,
            country, language, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @param language the language
     * @param start the lower bound of the range of vehicle attribute definition resources
     * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLocale(
        long vehicleAttributeDefinitionId, String country, String language,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE;
            finderArgs = new Object[] {
                    vehicleAttributeDefinitionId, country, language
                };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE;
            finderArgs = new Object[] {
                    vehicleAttributeDefinitionId, country, language,
                    
                    start, end, orderByComparator
                };
        }

        List<VehicleAttributeDefinitionResource> list = (List<VehicleAttributeDefinitionResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource : list) {
                if ((vehicleAttributeDefinitionId != vehicleAttributeDefinitionResource.getVehicleAttributeDefinitionId()) ||
                        !Validator.equals(country,
                            vehicleAttributeDefinitionResource.getCountry()) ||
                        !Validator.equals(language,
                            vehicleAttributeDefinitionResource.getLanguage())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(5 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(5);
            }

            query.append(_SQL_SELECT_VEHICLEATTRIBUTEDEFINITIONRESOURCE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_VEHICLEATTRIBUTEDEFINITIONID_2);

            boolean bindCountry = false;

            if (country == null) {
                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_COUNTRY_1);
            } else if (country.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_COUNTRY_3);
            } else {
                bindCountry = true;

                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_COUNTRY_2);
            }

            boolean bindLanguage = false;

            if (language == null) {
                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_LANGUAGE_1);
            } else if (language.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_LANGUAGE_3);
            } else {
                bindLanguage = true;

                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_LANGUAGE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleAttributeDefinitionId);

                if (bindCountry) {
                    qPos.add(country.toLowerCase());
                }

                if (bindLanguage) {
                    qPos.add(language.toLowerCase());
                }

                if (!pagination) {
                    list = (List<VehicleAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleAttributeDefinitionResource>(list);
                } else {
                    list = (List<VehicleAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute definition resource
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndLocale_First(
        long vehicleAttributeDefinitionId, String country, String language,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionResourceException,
            SystemException {
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource = fetchByVehicleAttributeDefinitionIdAndLocale_First(vehicleAttributeDefinitionId,
                country, language, orderByComparator);

        if (vehicleAttributeDefinitionResource != null) {
            return vehicleAttributeDefinitionResource;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleAttributeDefinitionId=");
        msg.append(vehicleAttributeDefinitionId);

        msg.append(", country=");
        msg.append(country);

        msg.append(", language=");
        msg.append(language);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeDefinitionResourceException(msg.toString());
    }

    /**
     * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndLocale_First(
        long vehicleAttributeDefinitionId, String country, String language,
        OrderByComparator orderByComparator) throws SystemException {
        List<VehicleAttributeDefinitionResource> list = findByVehicleAttributeDefinitionIdAndLocale(vehicleAttributeDefinitionId,
                country, language, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute definition resource
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndLocale_Last(
        long vehicleAttributeDefinitionId, String country, String language,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionResourceException,
            SystemException {
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource = fetchByVehicleAttributeDefinitionIdAndLocale_Last(vehicleAttributeDefinitionId,
                country, language, orderByComparator);

        if (vehicleAttributeDefinitionResource != null) {
            return vehicleAttributeDefinitionResource;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleAttributeDefinitionId=");
        msg.append(vehicleAttributeDefinitionId);

        msg.append(", country=");
        msg.append(country);

        msg.append(", language=");
        msg.append(language);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeDefinitionResourceException(msg.toString());
    }

    /**
     * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndLocale_Last(
        long vehicleAttributeDefinitionId, String country, String language,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByVehicleAttributeDefinitionIdAndLocale(vehicleAttributeDefinitionId,
                country, language);

        if (count == 0) {
            return null;
        }

        List<VehicleAttributeDefinitionResource> list = findByVehicleAttributeDefinitionIdAndLocale(vehicleAttributeDefinitionId,
                country, language, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle attribute definition resources before and after the current vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * @param vehicleAttributeDefinitionResourceId the primary key of the current vehicle attribute definition resource
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle attribute definition resource
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource[] findByVehicleAttributeDefinitionIdAndLocale_PrevAndNext(
        long vehicleAttributeDefinitionResourceId,
        long vehicleAttributeDefinitionId, String country, String language,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeDefinitionResourceException,
            SystemException {
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource = findByPrimaryKey(vehicleAttributeDefinitionResourceId);

        Session session = null;

        try {
            session = openSession();

            VehicleAttributeDefinitionResource[] array = new VehicleAttributeDefinitionResourceImpl[3];

            array[0] = getByVehicleAttributeDefinitionIdAndLocale_PrevAndNext(session,
                    vehicleAttributeDefinitionResource,
                    vehicleAttributeDefinitionId, country, language,
                    orderByComparator, true);

            array[1] = vehicleAttributeDefinitionResource;

            array[2] = getByVehicleAttributeDefinitionIdAndLocale_PrevAndNext(session,
                    vehicleAttributeDefinitionResource,
                    vehicleAttributeDefinitionId, country, language,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleAttributeDefinitionResource getByVehicleAttributeDefinitionIdAndLocale_PrevAndNext(
        Session session,
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource,
        long vehicleAttributeDefinitionId, String country, String language,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLEATTRIBUTEDEFINITIONRESOURCE_WHERE);

        query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_VEHICLEATTRIBUTEDEFINITIONID_2);

        boolean bindCountry = false;

        if (country == null) {
            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_COUNTRY_1);
        } else if (country.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_COUNTRY_3);
        } else {
            bindCountry = true;

            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_COUNTRY_2);
        }

        boolean bindLanguage = false;

        if (language == null) {
            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_LANGUAGE_1);
        } else if (language.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_LANGUAGE_3);
        } else {
            bindLanguage = true;

            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_LANGUAGE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(vehicleAttributeDefinitionId);

        if (bindCountry) {
            qPos.add(country.toLowerCase());
        }

        if (bindLanguage) {
            qPos.add(language.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleAttributeDefinitionResource);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleAttributeDefinitionResource> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63; from the database.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @param language the language
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByVehicleAttributeDefinitionIdAndLocale(
        long vehicleAttributeDefinitionId, String country, String language)
        throws SystemException {
        for (VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource : findByVehicleAttributeDefinitionIdAndLocale(
                vehicleAttributeDefinitionId, country, language,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(vehicleAttributeDefinitionResource);
        }
    }

    /**
     * Returns the number of vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param country the country
     * @param language the language
     * @return the number of matching vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByVehicleAttributeDefinitionIdAndLocale(
        long vehicleAttributeDefinitionId, String country, String language)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE;

        Object[] finderArgs = new Object[] {
                vehicleAttributeDefinitionId, country, language
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_COUNT_VEHICLEATTRIBUTEDEFINITIONRESOURCE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_VEHICLEATTRIBUTEDEFINITIONID_2);

            boolean bindCountry = false;

            if (country == null) {
                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_COUNTRY_1);
            } else if (country.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_COUNTRY_3);
            } else {
                bindCountry = true;

                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_COUNTRY_2);
            }

            boolean bindLanguage = false;

            if (language == null) {
                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_LANGUAGE_1);
            } else if (language.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_LANGUAGE_3);
            } else {
                bindLanguage = true;

                query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE_LANGUAGE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleAttributeDefinitionId);

                if (bindCountry) {
                    qPos.add(country.toLowerCase());
                }

                if (bindLanguage) {
                    qPos.add(language.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the vehicle attribute definition resource in the entity cache if it is enabled.
     *
     * @param vehicleAttributeDefinitionResource the vehicle attribute definition resource
     */
    @Override
    public void cacheResult(
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource) {
        EntityCacheUtil.putResult(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceImpl.class,
            vehicleAttributeDefinitionResource.getPrimaryKey(),
            vehicleAttributeDefinitionResource);

        vehicleAttributeDefinitionResource.resetOriginalValues();
    }

    /**
     * Caches the vehicle attribute definition resources in the entity cache if it is enabled.
     *
     * @param vehicleAttributeDefinitionResources the vehicle attribute definition resources
     */
    @Override
    public void cacheResult(
        List<VehicleAttributeDefinitionResource> vehicleAttributeDefinitionResources) {
        for (VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource : vehicleAttributeDefinitionResources) {
            if (EntityCacheUtil.getResult(
                        VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleAttributeDefinitionResourceImpl.class,
                        vehicleAttributeDefinitionResource.getPrimaryKey()) == null) {
                cacheResult(vehicleAttributeDefinitionResource);
            } else {
                vehicleAttributeDefinitionResource.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all vehicle attribute definition resources.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(VehicleAttributeDefinitionResourceImpl.class.getName());
        }

        EntityCacheUtil.clearCache(VehicleAttributeDefinitionResourceImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the vehicle attribute definition resource.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource) {
        EntityCacheUtil.removeResult(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceImpl.class,
            vehicleAttributeDefinitionResource.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(
        List<VehicleAttributeDefinitionResource> vehicleAttributeDefinitionResources) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource : vehicleAttributeDefinitionResources) {
            EntityCacheUtil.removeResult(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
                VehicleAttributeDefinitionResourceImpl.class,
                vehicleAttributeDefinitionResource.getPrimaryKey());
        }
    }

    /**
     * Creates a new vehicle attribute definition resource with the primary key. Does not add the vehicle attribute definition resource to the database.
     *
     * @param vehicleAttributeDefinitionResourceId the primary key for the new vehicle attribute definition resource
     * @return the new vehicle attribute definition resource
     */
    @Override
    public VehicleAttributeDefinitionResource create(
        long vehicleAttributeDefinitionResourceId) {
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource = new VehicleAttributeDefinitionResourceImpl();

        vehicleAttributeDefinitionResource.setNew(true);
        vehicleAttributeDefinitionResource.setPrimaryKey(vehicleAttributeDefinitionResourceId);

        return vehicleAttributeDefinitionResource;
    }

    /**
     * Removes the vehicle attribute definition resource with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param vehicleAttributeDefinitionResourceId the primary key of the vehicle attribute definition resource
     * @return the vehicle attribute definition resource that was removed
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource remove(
        long vehicleAttributeDefinitionResourceId)
        throws NoSuchVehicleAttributeDefinitionResourceException,
            SystemException {
        return remove((Serializable) vehicleAttributeDefinitionResourceId);
    }

    /**
     * Removes the vehicle attribute definition resource with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the vehicle attribute definition resource
     * @return the vehicle attribute definition resource that was removed
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource remove(Serializable primaryKey)
        throws NoSuchVehicleAttributeDefinitionResourceException,
            SystemException {
        Session session = null;

        try {
            session = openSession();

            VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource =
                (VehicleAttributeDefinitionResource) session.get(VehicleAttributeDefinitionResourceImpl.class,
                    primaryKey);

            if (vehicleAttributeDefinitionResource == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchVehicleAttributeDefinitionResourceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(vehicleAttributeDefinitionResource);
        } catch (NoSuchVehicleAttributeDefinitionResourceException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected VehicleAttributeDefinitionResource removeImpl(
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource)
        throws SystemException {
        vehicleAttributeDefinitionResource = toUnwrappedModel(vehicleAttributeDefinitionResource);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(vehicleAttributeDefinitionResource)) {
                vehicleAttributeDefinitionResource = (VehicleAttributeDefinitionResource) session.get(VehicleAttributeDefinitionResourceImpl.class,
                        vehicleAttributeDefinitionResource.getPrimaryKeyObj());
            }

            if (vehicleAttributeDefinitionResource != null) {
                session.delete(vehicleAttributeDefinitionResource);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (vehicleAttributeDefinitionResource != null) {
            clearCache(vehicleAttributeDefinitionResource);
        }

        return vehicleAttributeDefinitionResource;
    }

    @Override
    public VehicleAttributeDefinitionResource updateImpl(
        de.humance.eco.profile.model.VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource)
        throws SystemException {
        vehicleAttributeDefinitionResource = toUnwrappedModel(vehicleAttributeDefinitionResource);

        boolean isNew = vehicleAttributeDefinitionResource.isNew();

        VehicleAttributeDefinitionResourceModelImpl vehicleAttributeDefinitionResourceModelImpl =
            (VehicleAttributeDefinitionResourceModelImpl) vehicleAttributeDefinitionResource;

        Session session = null;

        try {
            session = openSession();

            if (vehicleAttributeDefinitionResource.isNew()) {
                session.save(vehicleAttributeDefinitionResource);

                vehicleAttributeDefinitionResource.setNew(false);
            } else {
                session.merge(vehicleAttributeDefinitionResource);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew ||
                !VehicleAttributeDefinitionResourceModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((vehicleAttributeDefinitionResourceModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleAttributeDefinitionResourceModelImpl.getOriginalVehicleAttributeDefinitionId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONID,
                    args);

                args = new Object[] {
                        vehicleAttributeDefinitionResourceModelImpl.getVehicleAttributeDefinitionId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONID,
                    args);
            }

            if ((vehicleAttributeDefinitionResourceModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleAttributeDefinitionResourceModelImpl.getOriginalVehicleAttributeDefinitionId(),
                        vehicleAttributeDefinitionResourceModelImpl.getOriginalCountry()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY,
                    args);

                args = new Object[] {
                        vehicleAttributeDefinitionResourceModelImpl.getVehicleAttributeDefinitionId(),
                        vehicleAttributeDefinitionResourceModelImpl.getCountry()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDCOUNTRY,
                    args);
            }

            if ((vehicleAttributeDefinitionResourceModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleAttributeDefinitionResourceModelImpl.getOriginalVehicleAttributeDefinitionId(),
                        vehicleAttributeDefinitionResourceModelImpl.getOriginalLanguage()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE,
                    args);

                args = new Object[] {
                        vehicleAttributeDefinitionResourceModelImpl.getVehicleAttributeDefinitionId(),
                        vehicleAttributeDefinitionResourceModelImpl.getLanguage()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLANGUAGE,
                    args);
            }

            if ((vehicleAttributeDefinitionResourceModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleAttributeDefinitionResourceModelImpl.getOriginalVehicleAttributeDefinitionId(),
                        vehicleAttributeDefinitionResourceModelImpl.getOriginalCountry(),
                        vehicleAttributeDefinitionResourceModelImpl.getOriginalLanguage()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE,
                    args);

                args = new Object[] {
                        vehicleAttributeDefinitionResourceModelImpl.getVehicleAttributeDefinitionId(),
                        vehicleAttributeDefinitionResourceModelImpl.getCountry(),
                        vehicleAttributeDefinitionResourceModelImpl.getLanguage()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONIDANDLOCALE,
                    args);
            }
        }

        EntityCacheUtil.putResult(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeDefinitionResourceImpl.class,
            vehicleAttributeDefinitionResource.getPrimaryKey(),
            vehicleAttributeDefinitionResource);

        return vehicleAttributeDefinitionResource;
    }

    protected VehicleAttributeDefinitionResource toUnwrappedModel(
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource) {
        if (vehicleAttributeDefinitionResource instanceof VehicleAttributeDefinitionResourceImpl) {
            return vehicleAttributeDefinitionResource;
        }

        VehicleAttributeDefinitionResourceImpl vehicleAttributeDefinitionResourceImpl =
            new VehicleAttributeDefinitionResourceImpl();

        vehicleAttributeDefinitionResourceImpl.setNew(vehicleAttributeDefinitionResource.isNew());
        vehicleAttributeDefinitionResourceImpl.setPrimaryKey(vehicleAttributeDefinitionResource.getPrimaryKey());

        vehicleAttributeDefinitionResourceImpl.setVehicleAttributeDefinitionResourceId(vehicleAttributeDefinitionResource.getVehicleAttributeDefinitionResourceId());
        vehicleAttributeDefinitionResourceImpl.setVehicleAttributeDefinitionId(vehicleAttributeDefinitionResource.getVehicleAttributeDefinitionId());
        vehicleAttributeDefinitionResourceImpl.setCountry(vehicleAttributeDefinitionResource.getCountry());
        vehicleAttributeDefinitionResourceImpl.setLanguage(vehicleAttributeDefinitionResource.getLanguage());
        vehicleAttributeDefinitionResourceImpl.setTitle(vehicleAttributeDefinitionResource.getTitle());
        vehicleAttributeDefinitionResourceImpl.setUnit(vehicleAttributeDefinitionResource.getUnit());

        return vehicleAttributeDefinitionResourceImpl;
    }

    /**
     * Returns the vehicle attribute definition resource with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle attribute definition resource
     * @return the vehicle attribute definition resource
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource findByPrimaryKey(
        Serializable primaryKey)
        throws NoSuchVehicleAttributeDefinitionResourceException,
            SystemException {
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource = fetchByPrimaryKey(primaryKey);

        if (vehicleAttributeDefinitionResource == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchVehicleAttributeDefinitionResourceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return vehicleAttributeDefinitionResource;
    }

    /**
     * Returns the vehicle attribute definition resource with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException} if it could not be found.
     *
     * @param vehicleAttributeDefinitionResourceId the primary key of the vehicle attribute definition resource
     * @return the vehicle attribute definition resource
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource findByPrimaryKey(
        long vehicleAttributeDefinitionResourceId)
        throws NoSuchVehicleAttributeDefinitionResourceException,
            SystemException {
        return findByPrimaryKey((Serializable) vehicleAttributeDefinitionResourceId);
    }

    /**
     * Returns the vehicle attribute definition resource with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle attribute definition resource
     * @return the vehicle attribute definition resource, or <code>null</code> if a vehicle attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource fetchByPrimaryKey(
        Serializable primaryKey) throws SystemException {
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource = (VehicleAttributeDefinitionResource) EntityCacheUtil.getResult(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
                VehicleAttributeDefinitionResourceImpl.class, primaryKey);

        if (vehicleAttributeDefinitionResource == _nullVehicleAttributeDefinitionResource) {
            return null;
        }

        if (vehicleAttributeDefinitionResource == null) {
            Session session = null;

            try {
                session = openSession();

                vehicleAttributeDefinitionResource = (VehicleAttributeDefinitionResource) session.get(VehicleAttributeDefinitionResourceImpl.class,
                        primaryKey);

                if (vehicleAttributeDefinitionResource != null) {
                    cacheResult(vehicleAttributeDefinitionResource);
                } else {
                    EntityCacheUtil.putResult(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleAttributeDefinitionResourceImpl.class,
                        primaryKey, _nullVehicleAttributeDefinitionResource);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(VehicleAttributeDefinitionResourceModelImpl.ENTITY_CACHE_ENABLED,
                    VehicleAttributeDefinitionResourceImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return vehicleAttributeDefinitionResource;
    }

    /**
     * Returns the vehicle attribute definition resource with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param vehicleAttributeDefinitionResourceId the primary key of the vehicle attribute definition resource
     * @return the vehicle attribute definition resource, or <code>null</code> if a vehicle attribute definition resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttributeDefinitionResource fetchByPrimaryKey(
        long vehicleAttributeDefinitionResourceId) throws SystemException {
        return fetchByPrimaryKey((Serializable) vehicleAttributeDefinitionResourceId);
    }

    /**
     * Returns all the vehicle attribute definition resources.
     *
     * @return the vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinitionResource> findAll()
        throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle attribute definition resources.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicle attribute definition resources
     * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
     * @return the range of vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinitionResource> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle attribute definition resources.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicle attribute definition resources
     * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttributeDefinitionResource> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<VehicleAttributeDefinitionResource> list = (List<VehicleAttributeDefinitionResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_VEHICLEATTRIBUTEDEFINITIONRESOURCE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_VEHICLEATTRIBUTEDEFINITIONRESOURCE;

                if (pagination) {
                    sql = sql.concat(VehicleAttributeDefinitionResourceModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<VehicleAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleAttributeDefinitionResource>(list);
                } else {
                    list = (List<VehicleAttributeDefinitionResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the vehicle attribute definition resources from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource : findAll()) {
            remove(vehicleAttributeDefinitionResource);
        }
    }

    /**
     * Returns the number of vehicle attribute definition resources.
     *
     * @return the number of vehicle attribute definition resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_VEHICLEATTRIBUTEDEFINITIONRESOURCE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the vehicle attribute definition resource persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.de.humance.eco.profile.model.VehicleAttributeDefinitionResource")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<VehicleAttributeDefinitionResource>> listenersList =
                    new ArrayList<ModelListener<VehicleAttributeDefinitionResource>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<VehicleAttributeDefinitionResource>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(VehicleAttributeDefinitionResourceImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
