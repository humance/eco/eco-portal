package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.humance.eco.profile.NoSuchVehicleAttributeException;
import de.humance.eco.profile.model.VehicleAttribute;
import de.humance.eco.profile.model.impl.VehicleAttributeImpl;
import de.humance.eco.profile.model.impl.VehicleAttributeModelImpl;
import de.humance.eco.profile.service.persistence.VehicleAttributePersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the vehicle attribute service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehicleAttributePersistence
 * @see VehicleAttributeUtil
 * @generated
 */
public class VehicleAttributePersistenceImpl extends BasePersistenceImpl<VehicleAttribute>
    implements VehicleAttributePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link VehicleAttributeUtil} to access the vehicle attribute persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = VehicleAttributeImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLEID =
        new FinderPath(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByVehicleId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEID =
        new FinderPath(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByVehicleId",
            new String[] { Long.class.getName() },
            VehicleAttributeModelImpl.VEHICLEID_COLUMN_BITMASK |
            VehicleAttributeModelImpl.SEQUENCENUMBER_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_VEHICLEID = new FinderPath(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByVehicleId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_VEHICLEID_VEHICLEID_2 = "vehicleAttribute.vehicleId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONID =
        new FinderPath(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByVehicleAttributeDefinitionId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONID =
        new FinderPath(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByVehicleAttributeDefinitionId",
            new String[] { Long.class.getName() },
            VehicleAttributeModelImpl.VEHICLEATTRIBUTEDEFINITIONID_COLUMN_BITMASK |
            VehicleAttributeModelImpl.VEHICLEID_COLUMN_BITMASK |
            VehicleAttributeModelImpl.SEQUENCENUMBER_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONID =
        new FinderPath(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByVehicleAttributeDefinitionId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONID_VEHICLEATTRIBUTEDEFINITIONID_2 =
        "vehicleAttribute.vehicleAttributeDefinitionId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLEIDANDATTRIBUTEDEFINITIONID =
        new FinderPath(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByVehicleIdAndAttributeDefinitionId",
            new String[] {
                Long.class.getName(), Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEIDANDATTRIBUTEDEFINITIONID =
        new FinderPath(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeModelImpl.FINDER_CACHE_ENABLED,
            VehicleAttributeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByVehicleIdAndAttributeDefinitionId",
            new String[] { Long.class.getName(), Long.class.getName() },
            VehicleAttributeModelImpl.VEHICLEID_COLUMN_BITMASK |
            VehicleAttributeModelImpl.VEHICLEATTRIBUTEDEFINITIONID_COLUMN_BITMASK |
            VehicleAttributeModelImpl.SEQUENCENUMBER_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_VEHICLEIDANDATTRIBUTEDEFINITIONID =
        new FinderPath(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByVehicleIdAndAttributeDefinitionId",
            new String[] { Long.class.getName(), Long.class.getName() });
    private static final String _FINDER_COLUMN_VEHICLEIDANDATTRIBUTEDEFINITIONID_VEHICLEID_2 =
        "vehicleAttribute.vehicleId = ? AND ";
    private static final String _FINDER_COLUMN_VEHICLEIDANDATTRIBUTEDEFINITIONID_VEHICLEATTRIBUTEDEFINITIONID_2 =
        "vehicleAttribute.vehicleAttributeDefinitionId = ?";
    private static final String _SQL_SELECT_VEHICLEATTRIBUTE = "SELECT vehicleAttribute FROM VehicleAttribute vehicleAttribute";
    private static final String _SQL_SELECT_VEHICLEATTRIBUTE_WHERE = "SELECT vehicleAttribute FROM VehicleAttribute vehicleAttribute WHERE ";
    private static final String _SQL_COUNT_VEHICLEATTRIBUTE = "SELECT COUNT(vehicleAttribute) FROM VehicleAttribute vehicleAttribute";
    private static final String _SQL_COUNT_VEHICLEATTRIBUTE_WHERE = "SELECT COUNT(vehicleAttribute) FROM VehicleAttribute vehicleAttribute WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "vehicleAttribute.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No VehicleAttribute exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No VehicleAttribute exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(VehicleAttributePersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "type"
            });
    private static VehicleAttribute _nullVehicleAttribute = new VehicleAttributeImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<VehicleAttribute> toCacheModel() {
                return _nullVehicleAttributeCacheModel;
            }
        };

    private static CacheModel<VehicleAttribute> _nullVehicleAttributeCacheModel = new CacheModel<VehicleAttribute>() {
            @Override
            public VehicleAttribute toEntityModel() {
                return _nullVehicleAttribute;
            }
        };

    public VehicleAttributePersistenceImpl() {
        setModelClass(VehicleAttribute.class);
    }

    /**
     * Returns all the vehicle attributes where vehicleId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @return the matching vehicle attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttribute> findByVehicleId(long vehicleId)
        throws SystemException {
        return findByVehicleId(vehicleId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the vehicle attributes where vehicleId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleId the vehicle ID
     * @param start the lower bound of the range of vehicle attributes
     * @param end the upper bound of the range of vehicle attributes (not inclusive)
     * @return the range of matching vehicle attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttribute> findByVehicleId(long vehicleId, int start,
        int end) throws SystemException {
        return findByVehicleId(vehicleId, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle attributes where vehicleId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleId the vehicle ID
     * @param start the lower bound of the range of vehicle attributes
     * @param end the upper bound of the range of vehicle attributes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttribute> findByVehicleId(long vehicleId, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEID;
            finderArgs = new Object[] { vehicleId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLEID;
            finderArgs = new Object[] { vehicleId, start, end, orderByComparator };
        }

        List<VehicleAttribute> list = (List<VehicleAttribute>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleAttribute vehicleAttribute : list) {
                if ((vehicleId != vehicleAttribute.getVehicleId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLEATTRIBUTE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLEID_VEHICLEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleAttributeModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleId);

                if (!pagination) {
                    list = (List<VehicleAttribute>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleAttribute>(list);
                } else {
                    list = (List<VehicleAttribute>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle attribute in the ordered set where vehicleId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute findByVehicleId_First(long vehicleId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeException, SystemException {
        VehicleAttribute vehicleAttribute = fetchByVehicleId_First(vehicleId,
                orderByComparator);

        if (vehicleAttribute != null) {
            return vehicleAttribute;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleId=");
        msg.append(vehicleId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeException(msg.toString());
    }

    /**
     * Returns the first vehicle attribute in the ordered set where vehicleId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute fetchByVehicleId_First(long vehicleId,
        OrderByComparator orderByComparator) throws SystemException {
        List<VehicleAttribute> list = findByVehicleId(vehicleId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle attribute in the ordered set where vehicleId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute findByVehicleId_Last(long vehicleId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeException, SystemException {
        VehicleAttribute vehicleAttribute = fetchByVehicleId_Last(vehicleId,
                orderByComparator);

        if (vehicleAttribute != null) {
            return vehicleAttribute;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleId=");
        msg.append(vehicleId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeException(msg.toString());
    }

    /**
     * Returns the last vehicle attribute in the ordered set where vehicleId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute fetchByVehicleId_Last(long vehicleId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByVehicleId(vehicleId);

        if (count == 0) {
            return null;
        }

        List<VehicleAttribute> list = findByVehicleId(vehicleId, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle attributes before and after the current vehicle attribute in the ordered set where vehicleId = &#63;.
     *
     * @param vehicleAttributeId the primary key of the current vehicle attribute
     * @param vehicleId the vehicle ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle attribute
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a vehicle attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute[] findByVehicleId_PrevAndNext(
        long vehicleAttributeId, long vehicleId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeException, SystemException {
        VehicleAttribute vehicleAttribute = findByPrimaryKey(vehicleAttributeId);

        Session session = null;

        try {
            session = openSession();

            VehicleAttribute[] array = new VehicleAttributeImpl[3];

            array[0] = getByVehicleId_PrevAndNext(session, vehicleAttribute,
                    vehicleId, orderByComparator, true);

            array[1] = vehicleAttribute;

            array[2] = getByVehicleId_PrevAndNext(session, vehicleAttribute,
                    vehicleId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleAttribute getByVehicleId_PrevAndNext(Session session,
        VehicleAttribute vehicleAttribute, long vehicleId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLEATTRIBUTE_WHERE);

        query.append(_FINDER_COLUMN_VEHICLEID_VEHICLEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleAttributeModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(vehicleId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleAttribute);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleAttribute> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle attributes where vehicleId = &#63; from the database.
     *
     * @param vehicleId the vehicle ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByVehicleId(long vehicleId) throws SystemException {
        for (VehicleAttribute vehicleAttribute : findByVehicleId(vehicleId,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(vehicleAttribute);
        }
    }

    /**
     * Returns the number of vehicle attributes where vehicleId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @return the number of matching vehicle attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByVehicleId(long vehicleId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_VEHICLEID;

        Object[] finderArgs = new Object[] { vehicleId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLEATTRIBUTE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLEID_VEHICLEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicle attributes where vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @return the matching vehicle attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttribute> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId) throws SystemException {
        return findByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle attributes where vehicleAttributeDefinitionId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param start the lower bound of the range of vehicle attributes
     * @param end the upper bound of the range of vehicle attributes (not inclusive)
     * @return the range of matching vehicle attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttribute> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId, int start, int end)
        throws SystemException {
        return findByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId,
            start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle attributes where vehicleAttributeDefinitionId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param start the lower bound of the range of vehicle attributes
     * @param end the upper bound of the range of vehicle attributes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttribute> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONID;
            finderArgs = new Object[] { vehicleAttributeDefinitionId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONID;
            finderArgs = new Object[] {
                    vehicleAttributeDefinitionId,
                    
                    start, end, orderByComparator
                };
        }

        List<VehicleAttribute> list = (List<VehicleAttribute>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleAttribute vehicleAttribute : list) {
                if ((vehicleAttributeDefinitionId != vehicleAttribute.getVehicleAttributeDefinitionId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLEATTRIBUTE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONID_VEHICLEATTRIBUTEDEFINITIONID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleAttributeModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleAttributeDefinitionId);

                if (!pagination) {
                    list = (List<VehicleAttribute>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleAttribute>(list);
                } else {
                    list = (List<VehicleAttribute>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle attribute in the ordered set where vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute findByVehicleAttributeDefinitionId_First(
        long vehicleAttributeDefinitionId, OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeException, SystemException {
        VehicleAttribute vehicleAttribute = fetchByVehicleAttributeDefinitionId_First(vehicleAttributeDefinitionId,
                orderByComparator);

        if (vehicleAttribute != null) {
            return vehicleAttribute;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleAttributeDefinitionId=");
        msg.append(vehicleAttributeDefinitionId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeException(msg.toString());
    }

    /**
     * Returns the first vehicle attribute in the ordered set where vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute fetchByVehicleAttributeDefinitionId_First(
        long vehicleAttributeDefinitionId, OrderByComparator orderByComparator)
        throws SystemException {
        List<VehicleAttribute> list = findByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId,
                0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle attribute in the ordered set where vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute findByVehicleAttributeDefinitionId_Last(
        long vehicleAttributeDefinitionId, OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeException, SystemException {
        VehicleAttribute vehicleAttribute = fetchByVehicleAttributeDefinitionId_Last(vehicleAttributeDefinitionId,
                orderByComparator);

        if (vehicleAttribute != null) {
            return vehicleAttribute;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleAttributeDefinitionId=");
        msg.append(vehicleAttributeDefinitionId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeException(msg.toString());
    }

    /**
     * Returns the last vehicle attribute in the ordered set where vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute fetchByVehicleAttributeDefinitionId_Last(
        long vehicleAttributeDefinitionId, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);

        if (count == 0) {
            return null;
        }

        List<VehicleAttribute> list = findByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle attributes before and after the current vehicle attribute in the ordered set where vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleAttributeId the primary key of the current vehicle attribute
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle attribute
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a vehicle attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute[] findByVehicleAttributeDefinitionId_PrevAndNext(
        long vehicleAttributeId, long vehicleAttributeDefinitionId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeException, SystemException {
        VehicleAttribute vehicleAttribute = findByPrimaryKey(vehicleAttributeId);

        Session session = null;

        try {
            session = openSession();

            VehicleAttribute[] array = new VehicleAttributeImpl[3];

            array[0] = getByVehicleAttributeDefinitionId_PrevAndNext(session,
                    vehicleAttribute, vehicleAttributeDefinitionId,
                    orderByComparator, true);

            array[1] = vehicleAttribute;

            array[2] = getByVehicleAttributeDefinitionId_PrevAndNext(session,
                    vehicleAttribute, vehicleAttributeDefinitionId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleAttribute getByVehicleAttributeDefinitionId_PrevAndNext(
        Session session, VehicleAttribute vehicleAttribute,
        long vehicleAttributeDefinitionId, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLEATTRIBUTE_WHERE);

        query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONID_VEHICLEATTRIBUTEDEFINITIONID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleAttributeModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(vehicleAttributeDefinitionId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleAttribute);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleAttribute> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle attributes where vehicleAttributeDefinitionId = &#63; from the database.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId) throws SystemException {
        for (VehicleAttribute vehicleAttribute : findByVehicleAttributeDefinitionId(
                vehicleAttributeDefinitionId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(vehicleAttribute);
        }
    }

    /**
     * Returns the number of vehicle attributes where vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @return the number of matching vehicle attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONID;

        Object[] finderArgs = new Object[] { vehicleAttributeDefinitionId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLEATTRIBUTE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLEATTRIBUTEDEFINITIONID_VEHICLEATTRIBUTEDEFINITIONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleAttributeDefinitionId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicle attributes where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @return the matching vehicle attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttribute> findByVehicleIdAndAttributeDefinitionId(
        long vehicleId, long vehicleAttributeDefinitionId)
        throws SystemException {
        return findByVehicleIdAndAttributeDefinitionId(vehicleId,
            vehicleAttributeDefinitionId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the vehicle attributes where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleId the vehicle ID
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param start the lower bound of the range of vehicle attributes
     * @param end the upper bound of the range of vehicle attributes (not inclusive)
     * @return the range of matching vehicle attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttribute> findByVehicleIdAndAttributeDefinitionId(
        long vehicleId, long vehicleAttributeDefinitionId, int start, int end)
        throws SystemException {
        return findByVehicleIdAndAttributeDefinitionId(vehicleId,
            vehicleAttributeDefinitionId, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle attributes where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleId the vehicle ID
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param start the lower bound of the range of vehicle attributes
     * @param end the upper bound of the range of vehicle attributes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttribute> findByVehicleIdAndAttributeDefinitionId(
        long vehicleId, long vehicleAttributeDefinitionId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEIDANDATTRIBUTEDEFINITIONID;
            finderArgs = new Object[] { vehicleId, vehicleAttributeDefinitionId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLEIDANDATTRIBUTEDEFINITIONID;
            finderArgs = new Object[] {
                    vehicleId, vehicleAttributeDefinitionId,
                    
                    start, end, orderByComparator
                };
        }

        List<VehicleAttribute> list = (List<VehicleAttribute>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleAttribute vehicleAttribute : list) {
                if ((vehicleId != vehicleAttribute.getVehicleId()) ||
                        (vehicleAttributeDefinitionId != vehicleAttribute.getVehicleAttributeDefinitionId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_VEHICLEATTRIBUTE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLEIDANDATTRIBUTEDEFINITIONID_VEHICLEID_2);

            query.append(_FINDER_COLUMN_VEHICLEIDANDATTRIBUTEDEFINITIONID_VEHICLEATTRIBUTEDEFINITIONID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleAttributeModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleId);

                qPos.add(vehicleAttributeDefinitionId);

                if (!pagination) {
                    list = (List<VehicleAttribute>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleAttribute>(list);
                } else {
                    list = (List<VehicleAttribute>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle attribute in the ordered set where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute findByVehicleIdAndAttributeDefinitionId_First(
        long vehicleId, long vehicleAttributeDefinitionId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeException, SystemException {
        VehicleAttribute vehicleAttribute = fetchByVehicleIdAndAttributeDefinitionId_First(vehicleId,
                vehicleAttributeDefinitionId, orderByComparator);

        if (vehicleAttribute != null) {
            return vehicleAttribute;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleId=");
        msg.append(vehicleId);

        msg.append(", vehicleAttributeDefinitionId=");
        msg.append(vehicleAttributeDefinitionId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeException(msg.toString());
    }

    /**
     * Returns the first vehicle attribute in the ordered set where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute fetchByVehicleIdAndAttributeDefinitionId_First(
        long vehicleId, long vehicleAttributeDefinitionId,
        OrderByComparator orderByComparator) throws SystemException {
        List<VehicleAttribute> list = findByVehicleIdAndAttributeDefinitionId(vehicleId,
                vehicleAttributeDefinitionId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle attribute in the ordered set where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute findByVehicleIdAndAttributeDefinitionId_Last(
        long vehicleId, long vehicleAttributeDefinitionId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeException, SystemException {
        VehicleAttribute vehicleAttribute = fetchByVehicleIdAndAttributeDefinitionId_Last(vehicleId,
                vehicleAttributeDefinitionId, orderByComparator);

        if (vehicleAttribute != null) {
            return vehicleAttribute;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleId=");
        msg.append(vehicleId);

        msg.append(", vehicleAttributeDefinitionId=");
        msg.append(vehicleAttributeDefinitionId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleAttributeException(msg.toString());
    }

    /**
     * Returns the last vehicle attribute in the ordered set where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute fetchByVehicleIdAndAttributeDefinitionId_Last(
        long vehicleId, long vehicleAttributeDefinitionId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByVehicleIdAndAttributeDefinitionId(vehicleId,
                vehicleAttributeDefinitionId);

        if (count == 0) {
            return null;
        }

        List<VehicleAttribute> list = findByVehicleIdAndAttributeDefinitionId(vehicleId,
                vehicleAttributeDefinitionId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle attributes before and after the current vehicle attribute in the ordered set where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleAttributeId the primary key of the current vehicle attribute
     * @param vehicleId the vehicle ID
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle attribute
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a vehicle attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute[] findByVehicleIdAndAttributeDefinitionId_PrevAndNext(
        long vehicleAttributeId, long vehicleId,
        long vehicleAttributeDefinitionId, OrderByComparator orderByComparator)
        throws NoSuchVehicleAttributeException, SystemException {
        VehicleAttribute vehicleAttribute = findByPrimaryKey(vehicleAttributeId);

        Session session = null;

        try {
            session = openSession();

            VehicleAttribute[] array = new VehicleAttributeImpl[3];

            array[0] = getByVehicleIdAndAttributeDefinitionId_PrevAndNext(session,
                    vehicleAttribute, vehicleId, vehicleAttributeDefinitionId,
                    orderByComparator, true);

            array[1] = vehicleAttribute;

            array[2] = getByVehicleIdAndAttributeDefinitionId_PrevAndNext(session,
                    vehicleAttribute, vehicleId, vehicleAttributeDefinitionId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleAttribute getByVehicleIdAndAttributeDefinitionId_PrevAndNext(
        Session session, VehicleAttribute vehicleAttribute, long vehicleId,
        long vehicleAttributeDefinitionId, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLEATTRIBUTE_WHERE);

        query.append(_FINDER_COLUMN_VEHICLEIDANDATTRIBUTEDEFINITIONID_VEHICLEID_2);

        query.append(_FINDER_COLUMN_VEHICLEIDANDATTRIBUTEDEFINITIONID_VEHICLEATTRIBUTEDEFINITIONID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleAttributeModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(vehicleId);

        qPos.add(vehicleAttributeDefinitionId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleAttribute);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleAttribute> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle attributes where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63; from the database.
     *
     * @param vehicleId the vehicle ID
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByVehicleIdAndAttributeDefinitionId(long vehicleId,
        long vehicleAttributeDefinitionId) throws SystemException {
        for (VehicleAttribute vehicleAttribute : findByVehicleIdAndAttributeDefinitionId(
                vehicleId, vehicleAttributeDefinitionId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(vehicleAttribute);
        }
    }

    /**
     * Returns the number of vehicle attributes where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
     * @return the number of matching vehicle attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByVehicleIdAndAttributeDefinitionId(long vehicleId,
        long vehicleAttributeDefinitionId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_VEHICLEIDANDATTRIBUTEDEFINITIONID;

        Object[] finderArgs = new Object[] {
                vehicleId, vehicleAttributeDefinitionId
            };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_VEHICLEATTRIBUTE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLEIDANDATTRIBUTEDEFINITIONID_VEHICLEID_2);

            query.append(_FINDER_COLUMN_VEHICLEIDANDATTRIBUTEDEFINITIONID_VEHICLEATTRIBUTEDEFINITIONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleId);

                qPos.add(vehicleAttributeDefinitionId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the vehicle attribute in the entity cache if it is enabled.
     *
     * @param vehicleAttribute the vehicle attribute
     */
    @Override
    public void cacheResult(VehicleAttribute vehicleAttribute) {
        EntityCacheUtil.putResult(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeImpl.class, vehicleAttribute.getPrimaryKey(),
            vehicleAttribute);

        vehicleAttribute.resetOriginalValues();
    }

    /**
     * Caches the vehicle attributes in the entity cache if it is enabled.
     *
     * @param vehicleAttributes the vehicle attributes
     */
    @Override
    public void cacheResult(List<VehicleAttribute> vehicleAttributes) {
        for (VehicleAttribute vehicleAttribute : vehicleAttributes) {
            if (EntityCacheUtil.getResult(
                        VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleAttributeImpl.class,
                        vehicleAttribute.getPrimaryKey()) == null) {
                cacheResult(vehicleAttribute);
            } else {
                vehicleAttribute.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all vehicle attributes.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(VehicleAttributeImpl.class.getName());
        }

        EntityCacheUtil.clearCache(VehicleAttributeImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the vehicle attribute.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(VehicleAttribute vehicleAttribute) {
        EntityCacheUtil.removeResult(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeImpl.class, vehicleAttribute.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<VehicleAttribute> vehicleAttributes) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (VehicleAttribute vehicleAttribute : vehicleAttributes) {
            EntityCacheUtil.removeResult(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
                VehicleAttributeImpl.class, vehicleAttribute.getPrimaryKey());
        }
    }

    /**
     * Creates a new vehicle attribute with the primary key. Does not add the vehicle attribute to the database.
     *
     * @param vehicleAttributeId the primary key for the new vehicle attribute
     * @return the new vehicle attribute
     */
    @Override
    public VehicleAttribute create(long vehicleAttributeId) {
        VehicleAttribute vehicleAttribute = new VehicleAttributeImpl();

        vehicleAttribute.setNew(true);
        vehicleAttribute.setPrimaryKey(vehicleAttributeId);

        return vehicleAttribute;
    }

    /**
     * Removes the vehicle attribute with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param vehicleAttributeId the primary key of the vehicle attribute
     * @return the vehicle attribute that was removed
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a vehicle attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute remove(long vehicleAttributeId)
        throws NoSuchVehicleAttributeException, SystemException {
        return remove((Serializable) vehicleAttributeId);
    }

    /**
     * Removes the vehicle attribute with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the vehicle attribute
     * @return the vehicle attribute that was removed
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a vehicle attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute remove(Serializable primaryKey)
        throws NoSuchVehicleAttributeException, SystemException {
        Session session = null;

        try {
            session = openSession();

            VehicleAttribute vehicleAttribute = (VehicleAttribute) session.get(VehicleAttributeImpl.class,
                    primaryKey);

            if (vehicleAttribute == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchVehicleAttributeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(vehicleAttribute);
        } catch (NoSuchVehicleAttributeException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected VehicleAttribute removeImpl(VehicleAttribute vehicleAttribute)
        throws SystemException {
        vehicleAttribute = toUnwrappedModel(vehicleAttribute);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(vehicleAttribute)) {
                vehicleAttribute = (VehicleAttribute) session.get(VehicleAttributeImpl.class,
                        vehicleAttribute.getPrimaryKeyObj());
            }

            if (vehicleAttribute != null) {
                session.delete(vehicleAttribute);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (vehicleAttribute != null) {
            clearCache(vehicleAttribute);
        }

        return vehicleAttribute;
    }

    @Override
    public VehicleAttribute updateImpl(
        de.humance.eco.profile.model.VehicleAttribute vehicleAttribute)
        throws SystemException {
        vehicleAttribute = toUnwrappedModel(vehicleAttribute);

        boolean isNew = vehicleAttribute.isNew();

        VehicleAttributeModelImpl vehicleAttributeModelImpl = (VehicleAttributeModelImpl) vehicleAttribute;

        Session session = null;

        try {
            session = openSession();

            if (vehicleAttribute.isNew()) {
                session.save(vehicleAttribute);

                vehicleAttribute.setNew(false);
            } else {
                session.merge(vehicleAttribute);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !VehicleAttributeModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((vehicleAttributeModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleAttributeModelImpl.getOriginalVehicleId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEID,
                    args);

                args = new Object[] { vehicleAttributeModelImpl.getVehicleId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEID,
                    args);
            }

            if ((vehicleAttributeModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleAttributeModelImpl.getOriginalVehicleAttributeDefinitionId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONID,
                    args);

                args = new Object[] {
                        vehicleAttributeModelImpl.getVehicleAttributeDefinitionId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLEATTRIBUTEDEFINITIONID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEATTRIBUTEDEFINITIONID,
                    args);
            }

            if ((vehicleAttributeModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEIDANDATTRIBUTEDEFINITIONID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleAttributeModelImpl.getOriginalVehicleId(),
                        vehicleAttributeModelImpl.getOriginalVehicleAttributeDefinitionId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLEIDANDATTRIBUTEDEFINITIONID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEIDANDATTRIBUTEDEFINITIONID,
                    args);

                args = new Object[] {
                        vehicleAttributeModelImpl.getVehicleId(),
                        vehicleAttributeModelImpl.getVehicleAttributeDefinitionId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLEIDANDATTRIBUTEDEFINITIONID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEIDANDATTRIBUTEDEFINITIONID,
                    args);
            }
        }

        EntityCacheUtil.putResult(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleAttributeImpl.class, vehicleAttribute.getPrimaryKey(),
            vehicleAttribute);

        return vehicleAttribute;
    }

    protected VehicleAttribute toUnwrappedModel(
        VehicleAttribute vehicleAttribute) {
        if (vehicleAttribute instanceof VehicleAttributeImpl) {
            return vehicleAttribute;
        }

        VehicleAttributeImpl vehicleAttributeImpl = new VehicleAttributeImpl();

        vehicleAttributeImpl.setNew(vehicleAttribute.isNew());
        vehicleAttributeImpl.setPrimaryKey(vehicleAttribute.getPrimaryKey());

        vehicleAttributeImpl.setVehicleAttributeId(vehicleAttribute.getVehicleAttributeId());
        vehicleAttributeImpl.setVehicleId(vehicleAttribute.getVehicleId());
        vehicleAttributeImpl.setVehicleAttributeDefinitionId(vehicleAttribute.getVehicleAttributeDefinitionId());
        vehicleAttributeImpl.setType(vehicleAttribute.getType());
        vehicleAttributeImpl.setSequenceNumber(vehicleAttribute.getSequenceNumber());
        vehicleAttributeImpl.setAttributeValue(vehicleAttribute.getAttributeValue());

        return vehicleAttributeImpl;
    }

    /**
     * Returns the vehicle attribute with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle attribute
     * @return the vehicle attribute
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a vehicle attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute findByPrimaryKey(Serializable primaryKey)
        throws NoSuchVehicleAttributeException, SystemException {
        VehicleAttribute vehicleAttribute = fetchByPrimaryKey(primaryKey);

        if (vehicleAttribute == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchVehicleAttributeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return vehicleAttribute;
    }

    /**
     * Returns the vehicle attribute with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleAttributeException} if it could not be found.
     *
     * @param vehicleAttributeId the primary key of the vehicle attribute
     * @return the vehicle attribute
     * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a vehicle attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute findByPrimaryKey(long vehicleAttributeId)
        throws NoSuchVehicleAttributeException, SystemException {
        return findByPrimaryKey((Serializable) vehicleAttributeId);
    }

    /**
     * Returns the vehicle attribute with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle attribute
     * @return the vehicle attribute, or <code>null</code> if a vehicle attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        VehicleAttribute vehicleAttribute = (VehicleAttribute) EntityCacheUtil.getResult(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
                VehicleAttributeImpl.class, primaryKey);

        if (vehicleAttribute == _nullVehicleAttribute) {
            return null;
        }

        if (vehicleAttribute == null) {
            Session session = null;

            try {
                session = openSession();

                vehicleAttribute = (VehicleAttribute) session.get(VehicleAttributeImpl.class,
                        primaryKey);

                if (vehicleAttribute != null) {
                    cacheResult(vehicleAttribute);
                } else {
                    EntityCacheUtil.putResult(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleAttributeImpl.class, primaryKey,
                        _nullVehicleAttribute);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(VehicleAttributeModelImpl.ENTITY_CACHE_ENABLED,
                    VehicleAttributeImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return vehicleAttribute;
    }

    /**
     * Returns the vehicle attribute with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param vehicleAttributeId the primary key of the vehicle attribute
     * @return the vehicle attribute, or <code>null</code> if a vehicle attribute with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleAttribute fetchByPrimaryKey(long vehicleAttributeId)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) vehicleAttributeId);
    }

    /**
     * Returns all the vehicle attributes.
     *
     * @return the vehicle attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttribute> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle attributes.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicle attributes
     * @param end the upper bound of the range of vehicle attributes (not inclusive)
     * @return the range of vehicle attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttribute> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle attributes.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicle attributes
     * @param end the upper bound of the range of vehicle attributes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of vehicle attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleAttribute> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<VehicleAttribute> list = (List<VehicleAttribute>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_VEHICLEATTRIBUTE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_VEHICLEATTRIBUTE;

                if (pagination) {
                    sql = sql.concat(VehicleAttributeModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<VehicleAttribute>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleAttribute>(list);
                } else {
                    list = (List<VehicleAttribute>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the vehicle attributes from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (VehicleAttribute vehicleAttribute : findAll()) {
            remove(vehicleAttribute);
        }
    }

    /**
     * Returns the number of vehicle attributes.
     *
     * @return the number of vehicle attributes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_VEHICLEATTRIBUTE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the vehicle attribute persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.de.humance.eco.profile.model.VehicleAttribute")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<VehicleAttribute>> listenersList = new ArrayList<ModelListener<VehicleAttribute>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<VehicleAttribute>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(VehicleAttributeImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
