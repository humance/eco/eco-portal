package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.humance.eco.profile.NoSuchVehicleManufacturerException;
import de.humance.eco.profile.model.VehicleManufacturer;
import de.humance.eco.profile.model.impl.VehicleManufacturerImpl;
import de.humance.eco.profile.model.impl.VehicleManufacturerModelImpl;
import de.humance.eco.profile.service.persistence.VehicleManufacturerPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the vehicle manufacturer service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehicleManufacturerPersistence
 * @see VehicleManufacturerUtil
 * @generated
 */
public class VehicleManufacturerPersistenceImpl extends BasePersistenceImpl<VehicleManufacturer>
    implements VehicleManufacturerPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link VehicleManufacturerUtil} to access the vehicle manufacturer persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = VehicleManufacturerImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VehicleManufacturerModelImpl.ENTITY_CACHE_ENABLED,
            VehicleManufacturerModelImpl.FINDER_CACHE_ENABLED,
            VehicleManufacturerImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VehicleManufacturerModelImpl.ENTITY_CACHE_ENABLED,
            VehicleManufacturerModelImpl.FINDER_CACHE_ENABLED,
            VehicleManufacturerImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VehicleManufacturerModelImpl.ENTITY_CACHE_ENABLED,
            VehicleManufacturerModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME = new FinderPath(VehicleManufacturerModelImpl.ENTITY_CACHE_ENABLED,
            VehicleManufacturerModelImpl.FINDER_CACHE_ENABLED,
            VehicleManufacturerImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByName",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME = new FinderPath(VehicleManufacturerModelImpl.ENTITY_CACHE_ENABLED,
            VehicleManufacturerModelImpl.FINDER_CACHE_ENABLED,
            VehicleManufacturerImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByName",
            new String[] { String.class.getName() },
            VehicleManufacturerModelImpl.NAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_NAME = new FinderPath(VehicleManufacturerModelImpl.ENTITY_CACHE_ENABLED,
            VehicleManufacturerModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByName",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_NAME_NAME_1 = "vehicleManufacturer.name IS NULL";
    private static final String _FINDER_COLUMN_NAME_NAME_2 = "lower(vehicleManufacturer.name) = ?";
    private static final String _FINDER_COLUMN_NAME_NAME_3 = "(vehicleManufacturer.name IS NULL OR vehicleManufacturer.name = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NAMELIKE = new FinderPath(VehicleManufacturerModelImpl.ENTITY_CACHE_ENABLED,
            VehicleManufacturerModelImpl.FINDER_CACHE_ENABLED,
            VehicleManufacturerImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByNameLike",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_NAMELIKE =
        new FinderPath(VehicleManufacturerModelImpl.ENTITY_CACHE_ENABLED,
            VehicleManufacturerModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByNameLike",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_NAMELIKE_NAME_1 = "vehicleManufacturer.name LIKE NULL";
    private static final String _FINDER_COLUMN_NAMELIKE_NAME_2 = "lower(vehicleManufacturer.name) LIKE ?";
    private static final String _FINDER_COLUMN_NAMELIKE_NAME_3 = "(vehicleManufacturer.name IS NULL OR vehicleManufacturer.name LIKE '')";
    private static final String _SQL_SELECT_VEHICLEMANUFACTURER = "SELECT vehicleManufacturer FROM VehicleManufacturer vehicleManufacturer";
    private static final String _SQL_SELECT_VEHICLEMANUFACTURER_WHERE = "SELECT vehicleManufacturer FROM VehicleManufacturer vehicleManufacturer WHERE ";
    private static final String _SQL_COUNT_VEHICLEMANUFACTURER = "SELECT COUNT(vehicleManufacturer) FROM VehicleManufacturer vehicleManufacturer";
    private static final String _SQL_COUNT_VEHICLEMANUFACTURER_WHERE = "SELECT COUNT(vehicleManufacturer) FROM VehicleManufacturer vehicleManufacturer WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "vehicleManufacturer.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No VehicleManufacturer exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No VehicleManufacturer exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(VehicleManufacturerPersistenceImpl.class);
    private static VehicleManufacturer _nullVehicleManufacturer = new VehicleManufacturerImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<VehicleManufacturer> toCacheModel() {
                return _nullVehicleManufacturerCacheModel;
            }
        };

    private static CacheModel<VehicleManufacturer> _nullVehicleManufacturerCacheModel =
        new CacheModel<VehicleManufacturer>() {
            @Override
            public VehicleManufacturer toEntityModel() {
                return _nullVehicleManufacturer;
            }
        };

    public VehicleManufacturerPersistenceImpl() {
        setModelClass(VehicleManufacturer.class);
    }

    /**
     * Returns all the vehicle manufacturers where name = &#63;.
     *
     * @param name the name
     * @return the matching vehicle manufacturers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleManufacturer> findByName(String name)
        throws SystemException {
        return findByName(name, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle manufacturers where name = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleManufacturerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param name the name
     * @param start the lower bound of the range of vehicle manufacturers
     * @param end the upper bound of the range of vehicle manufacturers (not inclusive)
     * @return the range of matching vehicle manufacturers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleManufacturer> findByName(String name, int start, int end)
        throws SystemException {
        return findByName(name, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle manufacturers where name = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleManufacturerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param name the name
     * @param start the lower bound of the range of vehicle manufacturers
     * @param end the upper bound of the range of vehicle manufacturers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle manufacturers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleManufacturer> findByName(String name, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME;
            finderArgs = new Object[] { name };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME;
            finderArgs = new Object[] { name, start, end, orderByComparator };
        }

        List<VehicleManufacturer> list = (List<VehicleManufacturer>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleManufacturer vehicleManufacturer : list) {
                if (!Validator.equals(name, vehicleManufacturer.getName())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLEMANUFACTURER_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAME_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAME_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAME_NAME_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleManufacturerModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name.toLowerCase());
                }

                if (!pagination) {
                    list = (List<VehicleManufacturer>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleManufacturer>(list);
                } else {
                    list = (List<VehicleManufacturer>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle manufacturer in the ordered set where name = &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle manufacturer
     * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a matching vehicle manufacturer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleManufacturer findByName_First(String name,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleManufacturerException, SystemException {
        VehicleManufacturer vehicleManufacturer = fetchByName_First(name,
                orderByComparator);

        if (vehicleManufacturer != null) {
            return vehicleManufacturer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("name=");
        msg.append(name);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleManufacturerException(msg.toString());
    }

    /**
     * Returns the first vehicle manufacturer in the ordered set where name = &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle manufacturer, or <code>null</code> if a matching vehicle manufacturer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleManufacturer fetchByName_First(String name,
        OrderByComparator orderByComparator) throws SystemException {
        List<VehicleManufacturer> list = findByName(name, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle manufacturer in the ordered set where name = &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle manufacturer
     * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a matching vehicle manufacturer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleManufacturer findByName_Last(String name,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleManufacturerException, SystemException {
        VehicleManufacturer vehicleManufacturer = fetchByName_Last(name,
                orderByComparator);

        if (vehicleManufacturer != null) {
            return vehicleManufacturer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("name=");
        msg.append(name);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleManufacturerException(msg.toString());
    }

    /**
     * Returns the last vehicle manufacturer in the ordered set where name = &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle manufacturer, or <code>null</code> if a matching vehicle manufacturer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleManufacturer fetchByName_Last(String name,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByName(name);

        if (count == 0) {
            return null;
        }

        List<VehicleManufacturer> list = findByName(name, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle manufacturers before and after the current vehicle manufacturer in the ordered set where name = &#63;.
     *
     * @param vehicleManufacturerId the primary key of the current vehicle manufacturer
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle manufacturer
     * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a vehicle manufacturer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleManufacturer[] findByName_PrevAndNext(
        long vehicleManufacturerId, String name,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleManufacturerException, SystemException {
        VehicleManufacturer vehicleManufacturer = findByPrimaryKey(vehicleManufacturerId);

        Session session = null;

        try {
            session = openSession();

            VehicleManufacturer[] array = new VehicleManufacturerImpl[3];

            array[0] = getByName_PrevAndNext(session, vehicleManufacturer,
                    name, orderByComparator, true);

            array[1] = vehicleManufacturer;

            array[2] = getByName_PrevAndNext(session, vehicleManufacturer,
                    name, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleManufacturer getByName_PrevAndNext(Session session,
        VehicleManufacturer vehicleManufacturer, String name,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLEMANUFACTURER_WHERE);

        boolean bindName = false;

        if (name == null) {
            query.append(_FINDER_COLUMN_NAME_NAME_1);
        } else if (name.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_NAME_NAME_3);
        } else {
            bindName = true;

            query.append(_FINDER_COLUMN_NAME_NAME_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleManufacturerModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindName) {
            qPos.add(name.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleManufacturer);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleManufacturer> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle manufacturers where name = &#63; from the database.
     *
     * @param name the name
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByName(String name) throws SystemException {
        for (VehicleManufacturer vehicleManufacturer : findByName(name,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(vehicleManufacturer);
        }
    }

    /**
     * Returns the number of vehicle manufacturers where name = &#63;.
     *
     * @param name the name
     * @return the number of matching vehicle manufacturers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByName(String name) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_NAME;

        Object[] finderArgs = new Object[] { name };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLEMANUFACTURER_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAME_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAME_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAME_NAME_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicle manufacturers where name LIKE &#63;.
     *
     * @param name the name
     * @return the matching vehicle manufacturers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleManufacturer> findByNameLike(String name)
        throws SystemException {
        return findByNameLike(name, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle manufacturers where name LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleManufacturerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param name the name
     * @param start the lower bound of the range of vehicle manufacturers
     * @param end the upper bound of the range of vehicle manufacturers (not inclusive)
     * @return the range of matching vehicle manufacturers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleManufacturer> findByNameLike(String name, int start,
        int end) throws SystemException {
        return findByNameLike(name, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle manufacturers where name LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleManufacturerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param name the name
     * @param start the lower bound of the range of vehicle manufacturers
     * @param end the upper bound of the range of vehicle manufacturers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle manufacturers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleManufacturer> findByNameLike(String name, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NAMELIKE;
        finderArgs = new Object[] { name, start, end, orderByComparator };

        List<VehicleManufacturer> list = (List<VehicleManufacturer>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleManufacturer vehicleManufacturer : list) {
                if (!StringUtil.wildcardMatches(vehicleManufacturer.getName(),
                            name, CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, false)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLEMANUFACTURER_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAMELIKE_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAMELIKE_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAMELIKE_NAME_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleManufacturerModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name.toLowerCase());
                }

                if (!pagination) {
                    list = (List<VehicleManufacturer>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleManufacturer>(list);
                } else {
                    list = (List<VehicleManufacturer>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle manufacturer in the ordered set where name LIKE &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle manufacturer
     * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a matching vehicle manufacturer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleManufacturer findByNameLike_First(String name,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleManufacturerException, SystemException {
        VehicleManufacturer vehicleManufacturer = fetchByNameLike_First(name,
                orderByComparator);

        if (vehicleManufacturer != null) {
            return vehicleManufacturer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("name=");
        msg.append(name);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleManufacturerException(msg.toString());
    }

    /**
     * Returns the first vehicle manufacturer in the ordered set where name LIKE &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle manufacturer, or <code>null</code> if a matching vehicle manufacturer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleManufacturer fetchByNameLike_First(String name,
        OrderByComparator orderByComparator) throws SystemException {
        List<VehicleManufacturer> list = findByNameLike(name, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle manufacturer in the ordered set where name LIKE &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle manufacturer
     * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a matching vehicle manufacturer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleManufacturer findByNameLike_Last(String name,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleManufacturerException, SystemException {
        VehicleManufacturer vehicleManufacturer = fetchByNameLike_Last(name,
                orderByComparator);

        if (vehicleManufacturer != null) {
            return vehicleManufacturer;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("name=");
        msg.append(name);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleManufacturerException(msg.toString());
    }

    /**
     * Returns the last vehicle manufacturer in the ordered set where name LIKE &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle manufacturer, or <code>null</code> if a matching vehicle manufacturer could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleManufacturer fetchByNameLike_Last(String name,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByNameLike(name);

        if (count == 0) {
            return null;
        }

        List<VehicleManufacturer> list = findByNameLike(name, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle manufacturers before and after the current vehicle manufacturer in the ordered set where name LIKE &#63;.
     *
     * @param vehicleManufacturerId the primary key of the current vehicle manufacturer
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle manufacturer
     * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a vehicle manufacturer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleManufacturer[] findByNameLike_PrevAndNext(
        long vehicleManufacturerId, String name,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleManufacturerException, SystemException {
        VehicleManufacturer vehicleManufacturer = findByPrimaryKey(vehicleManufacturerId);

        Session session = null;

        try {
            session = openSession();

            VehicleManufacturer[] array = new VehicleManufacturerImpl[3];

            array[0] = getByNameLike_PrevAndNext(session, vehicleManufacturer,
                    name, orderByComparator, true);

            array[1] = vehicleManufacturer;

            array[2] = getByNameLike_PrevAndNext(session, vehicleManufacturer,
                    name, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleManufacturer getByNameLike_PrevAndNext(Session session,
        VehicleManufacturer vehicleManufacturer, String name,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLEMANUFACTURER_WHERE);

        boolean bindName = false;

        if (name == null) {
            query.append(_FINDER_COLUMN_NAMELIKE_NAME_1);
        } else if (name.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_NAMELIKE_NAME_3);
        } else {
            bindName = true;

            query.append(_FINDER_COLUMN_NAMELIKE_NAME_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleManufacturerModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindName) {
            qPos.add(name.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleManufacturer);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleManufacturer> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle manufacturers where name LIKE &#63; from the database.
     *
     * @param name the name
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByNameLike(String name) throws SystemException {
        for (VehicleManufacturer vehicleManufacturer : findByNameLike(name,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(vehicleManufacturer);
        }
    }

    /**
     * Returns the number of vehicle manufacturers where name LIKE &#63;.
     *
     * @param name the name
     * @return the number of matching vehicle manufacturers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByNameLike(String name) throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_NAMELIKE;

        Object[] finderArgs = new Object[] { name };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLEMANUFACTURER_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAMELIKE_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAMELIKE_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAMELIKE_NAME_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the vehicle manufacturer in the entity cache if it is enabled.
     *
     * @param vehicleManufacturer the vehicle manufacturer
     */
    @Override
    public void cacheResult(VehicleManufacturer vehicleManufacturer) {
        EntityCacheUtil.putResult(VehicleManufacturerModelImpl.ENTITY_CACHE_ENABLED,
            VehicleManufacturerImpl.class, vehicleManufacturer.getPrimaryKey(),
            vehicleManufacturer);

        vehicleManufacturer.resetOriginalValues();
    }

    /**
     * Caches the vehicle manufacturers in the entity cache if it is enabled.
     *
     * @param vehicleManufacturers the vehicle manufacturers
     */
    @Override
    public void cacheResult(List<VehicleManufacturer> vehicleManufacturers) {
        for (VehicleManufacturer vehicleManufacturer : vehicleManufacturers) {
            if (EntityCacheUtil.getResult(
                        VehicleManufacturerModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleManufacturerImpl.class,
                        vehicleManufacturer.getPrimaryKey()) == null) {
                cacheResult(vehicleManufacturer);
            } else {
                vehicleManufacturer.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all vehicle manufacturers.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(VehicleManufacturerImpl.class.getName());
        }

        EntityCacheUtil.clearCache(VehicleManufacturerImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the vehicle manufacturer.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(VehicleManufacturer vehicleManufacturer) {
        EntityCacheUtil.removeResult(VehicleManufacturerModelImpl.ENTITY_CACHE_ENABLED,
            VehicleManufacturerImpl.class, vehicleManufacturer.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<VehicleManufacturer> vehicleManufacturers) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (VehicleManufacturer vehicleManufacturer : vehicleManufacturers) {
            EntityCacheUtil.removeResult(VehicleManufacturerModelImpl.ENTITY_CACHE_ENABLED,
                VehicleManufacturerImpl.class,
                vehicleManufacturer.getPrimaryKey());
        }
    }

    /**
     * Creates a new vehicle manufacturer with the primary key. Does not add the vehicle manufacturer to the database.
     *
     * @param vehicleManufacturerId the primary key for the new vehicle manufacturer
     * @return the new vehicle manufacturer
     */
    @Override
    public VehicleManufacturer create(long vehicleManufacturerId) {
        VehicleManufacturer vehicleManufacturer = new VehicleManufacturerImpl();

        vehicleManufacturer.setNew(true);
        vehicleManufacturer.setPrimaryKey(vehicleManufacturerId);

        return vehicleManufacturer;
    }

    /**
     * Removes the vehicle manufacturer with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param vehicleManufacturerId the primary key of the vehicle manufacturer
     * @return the vehicle manufacturer that was removed
     * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a vehicle manufacturer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleManufacturer remove(long vehicleManufacturerId)
        throws NoSuchVehicleManufacturerException, SystemException {
        return remove((Serializable) vehicleManufacturerId);
    }

    /**
     * Removes the vehicle manufacturer with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the vehicle manufacturer
     * @return the vehicle manufacturer that was removed
     * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a vehicle manufacturer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleManufacturer remove(Serializable primaryKey)
        throws NoSuchVehicleManufacturerException, SystemException {
        Session session = null;

        try {
            session = openSession();

            VehicleManufacturer vehicleManufacturer = (VehicleManufacturer) session.get(VehicleManufacturerImpl.class,
                    primaryKey);

            if (vehicleManufacturer == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchVehicleManufacturerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(vehicleManufacturer);
        } catch (NoSuchVehicleManufacturerException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected VehicleManufacturer removeImpl(
        VehicleManufacturer vehicleManufacturer) throws SystemException {
        vehicleManufacturer = toUnwrappedModel(vehicleManufacturer);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(vehicleManufacturer)) {
                vehicleManufacturer = (VehicleManufacturer) session.get(VehicleManufacturerImpl.class,
                        vehicleManufacturer.getPrimaryKeyObj());
            }

            if (vehicleManufacturer != null) {
                session.delete(vehicleManufacturer);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (vehicleManufacturer != null) {
            clearCache(vehicleManufacturer);
        }

        return vehicleManufacturer;
    }

    @Override
    public VehicleManufacturer updateImpl(
        de.humance.eco.profile.model.VehicleManufacturer vehicleManufacturer)
        throws SystemException {
        vehicleManufacturer = toUnwrappedModel(vehicleManufacturer);

        boolean isNew = vehicleManufacturer.isNew();

        VehicleManufacturerModelImpl vehicleManufacturerModelImpl = (VehicleManufacturerModelImpl) vehicleManufacturer;

        Session session = null;

        try {
            session = openSession();

            if (vehicleManufacturer.isNew()) {
                session.save(vehicleManufacturer);

                vehicleManufacturer.setNew(false);
            } else {
                session.merge(vehicleManufacturer);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !VehicleManufacturerModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((vehicleManufacturerModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleManufacturerModelImpl.getOriginalName()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
                    args);

                args = new Object[] { vehicleManufacturerModelImpl.getName() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
                    args);
            }
        }

        EntityCacheUtil.putResult(VehicleManufacturerModelImpl.ENTITY_CACHE_ENABLED,
            VehicleManufacturerImpl.class, vehicleManufacturer.getPrimaryKey(),
            vehicleManufacturer);

        return vehicleManufacturer;
    }

    protected VehicleManufacturer toUnwrappedModel(
        VehicleManufacturer vehicleManufacturer) {
        if (vehicleManufacturer instanceof VehicleManufacturerImpl) {
            return vehicleManufacturer;
        }

        VehicleManufacturerImpl vehicleManufacturerImpl = new VehicleManufacturerImpl();

        vehicleManufacturerImpl.setNew(vehicleManufacturer.isNew());
        vehicleManufacturerImpl.setPrimaryKey(vehicleManufacturer.getPrimaryKey());

        vehicleManufacturerImpl.setVehicleManufacturerId(vehicleManufacturer.getVehicleManufacturerId());
        vehicleManufacturerImpl.setCreatorId(vehicleManufacturer.getCreatorId());
        vehicleManufacturerImpl.setCreatorName(vehicleManufacturer.getCreatorName());
        vehicleManufacturerImpl.setCreateDate(vehicleManufacturer.getCreateDate());
        vehicleManufacturerImpl.setModifierId(vehicleManufacturer.getModifierId());
        vehicleManufacturerImpl.setModifierName(vehicleManufacturer.getModifierName());
        vehicleManufacturerImpl.setModifiedDate(vehicleManufacturer.getModifiedDate());
        vehicleManufacturerImpl.setName(vehicleManufacturer.getName());
        vehicleManufacturerImpl.setIconId(vehicleManufacturer.getIconId());

        return vehicleManufacturerImpl;
    }

    /**
     * Returns the vehicle manufacturer with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle manufacturer
     * @return the vehicle manufacturer
     * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a vehicle manufacturer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleManufacturer findByPrimaryKey(Serializable primaryKey)
        throws NoSuchVehicleManufacturerException, SystemException {
        VehicleManufacturer vehicleManufacturer = fetchByPrimaryKey(primaryKey);

        if (vehicleManufacturer == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchVehicleManufacturerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return vehicleManufacturer;
    }

    /**
     * Returns the vehicle manufacturer with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleManufacturerException} if it could not be found.
     *
     * @param vehicleManufacturerId the primary key of the vehicle manufacturer
     * @return the vehicle manufacturer
     * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a vehicle manufacturer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleManufacturer findByPrimaryKey(long vehicleManufacturerId)
        throws NoSuchVehicleManufacturerException, SystemException {
        return findByPrimaryKey((Serializable) vehicleManufacturerId);
    }

    /**
     * Returns the vehicle manufacturer with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle manufacturer
     * @return the vehicle manufacturer, or <code>null</code> if a vehicle manufacturer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleManufacturer fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        VehicleManufacturer vehicleManufacturer = (VehicleManufacturer) EntityCacheUtil.getResult(VehicleManufacturerModelImpl.ENTITY_CACHE_ENABLED,
                VehicleManufacturerImpl.class, primaryKey);

        if (vehicleManufacturer == _nullVehicleManufacturer) {
            return null;
        }

        if (vehicleManufacturer == null) {
            Session session = null;

            try {
                session = openSession();

                vehicleManufacturer = (VehicleManufacturer) session.get(VehicleManufacturerImpl.class,
                        primaryKey);

                if (vehicleManufacturer != null) {
                    cacheResult(vehicleManufacturer);
                } else {
                    EntityCacheUtil.putResult(VehicleManufacturerModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleManufacturerImpl.class, primaryKey,
                        _nullVehicleManufacturer);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(VehicleManufacturerModelImpl.ENTITY_CACHE_ENABLED,
                    VehicleManufacturerImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return vehicleManufacturer;
    }

    /**
     * Returns the vehicle manufacturer with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param vehicleManufacturerId the primary key of the vehicle manufacturer
     * @return the vehicle manufacturer, or <code>null</code> if a vehicle manufacturer with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleManufacturer fetchByPrimaryKey(long vehicleManufacturerId)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) vehicleManufacturerId);
    }

    /**
     * Returns all the vehicle manufacturers.
     *
     * @return the vehicle manufacturers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleManufacturer> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle manufacturers.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleManufacturerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicle manufacturers
     * @param end the upper bound of the range of vehicle manufacturers (not inclusive)
     * @return the range of vehicle manufacturers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleManufacturer> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle manufacturers.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleManufacturerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicle manufacturers
     * @param end the upper bound of the range of vehicle manufacturers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of vehicle manufacturers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleManufacturer> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<VehicleManufacturer> list = (List<VehicleManufacturer>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_VEHICLEMANUFACTURER);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_VEHICLEMANUFACTURER;

                if (pagination) {
                    sql = sql.concat(VehicleManufacturerModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<VehicleManufacturer>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleManufacturer>(list);
                } else {
                    list = (List<VehicleManufacturer>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the vehicle manufacturers from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (VehicleManufacturer vehicleManufacturer : findAll()) {
            remove(vehicleManufacturer);
        }
    }

    /**
     * Returns the number of vehicle manufacturers.
     *
     * @return the number of vehicle manufacturers
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_VEHICLEMANUFACTURER);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the vehicle manufacturer persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.de.humance.eco.profile.model.VehicleManufacturer")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<VehicleManufacturer>> listenersList = new ArrayList<ModelListener<VehicleManufacturer>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<VehicleManufacturer>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(VehicleManufacturerImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
