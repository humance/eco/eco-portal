package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.humance.eco.profile.NoSuchVehicleException;
import de.humance.eco.profile.model.Vehicle;
import de.humance.eco.profile.model.impl.VehicleImpl;
import de.humance.eco.profile.model.impl.VehicleModelImpl;
import de.humance.eco.profile.service.persistence.VehiclePersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the vehicle service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehiclePersistence
 * @see VehicleUtil
 * @generated
 */
public class VehiclePersistenceImpl extends BasePersistenceImpl<Vehicle>
    implements VehiclePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link VehicleUtil} to access the vehicle persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = VehicleImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DRIVERID = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByDriverId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERID =
        new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByDriverId",
            new String[] { Long.class.getName() },
            VehicleModelImpl.DRIVERID_COLUMN_BITMASK |
            VehicleModelImpl.MANUFACTURERID_COLUMN_BITMASK |
            VehicleModelImpl.MODELNAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_DRIVERID = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByDriverId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_DRIVERID_DRIVERID_2 = "vehicle.driverId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DRIVERIDANDORGAID =
        new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByDriverIdAndOrgaId",
            new String[] {
                Long.class.getName(), Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDORGAID =
        new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByDriverIdAndOrgaId",
            new String[] { Long.class.getName(), Long.class.getName() },
            VehicleModelImpl.DRIVERID_COLUMN_BITMASK |
            VehicleModelImpl.ORGANIZATIONID_COLUMN_BITMASK |
            VehicleModelImpl.MANUFACTURERID_COLUMN_BITMASK |
            VehicleModelImpl.MODELNAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_DRIVERIDANDORGAID = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByDriverIdAndOrgaId",
            new String[] { Long.class.getName(), Long.class.getName() });
    private static final String _FINDER_COLUMN_DRIVERIDANDORGAID_DRIVERID_2 = "vehicle.driverId = ? AND ";
    private static final String _FINDER_COLUMN_DRIVERIDANDORGAID_ORGANIZATIONID_2 =
        "vehicle.organizationId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TYPEID = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTypeId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPEID =
        new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByTypeId",
            new String[] { Long.class.getName() },
            VehicleModelImpl.TYPEID_COLUMN_BITMASK |
            VehicleModelImpl.MANUFACTURERID_COLUMN_BITMASK |
            VehicleModelImpl.MODELNAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_TYPEID = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTypeId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_TYPEID_TYPEID_2 = "vehicle.typeId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DRIVERIDANDTYPEID =
        new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByDriverIdAndTypeId",
            new String[] {
                Long.class.getName(), Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDTYPEID =
        new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByDriverIdAndTypeId",
            new String[] { Long.class.getName(), Long.class.getName() },
            VehicleModelImpl.DRIVERID_COLUMN_BITMASK |
            VehicleModelImpl.TYPEID_COLUMN_BITMASK |
            VehicleModelImpl.MANUFACTURERID_COLUMN_BITMASK |
            VehicleModelImpl.MODELNAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_DRIVERIDANDTYPEID = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByDriverIdAndTypeId",
            new String[] { Long.class.getName(), Long.class.getName() });
    private static final String _FINDER_COLUMN_DRIVERIDANDTYPEID_DRIVERID_2 = "vehicle.driverId = ? AND ";
    private static final String _FINDER_COLUMN_DRIVERIDANDTYPEID_TYPEID_2 = "vehicle.typeId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MANUFACTURERID =
        new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByManufacturerId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MANUFACTURERID =
        new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByManufacturerId",
            new String[] { Long.class.getName() },
            VehicleModelImpl.MANUFACTURERID_COLUMN_BITMASK |
            VehicleModelImpl.MODELNAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_MANUFACTURERID = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByManufacturerId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_MANUFACTURERID_MANUFACTURERID_2 = "vehicle.manufacturerId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MODELNAME =
        new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByModelName",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODELNAME =
        new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByModelName",
            new String[] { String.class.getName() },
            VehicleModelImpl.MODELNAME_COLUMN_BITMASK |
            VehicleModelImpl.MANUFACTURERID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_MODELNAME = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByModelName",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_MODELNAME_MODELNAME_1 = "vehicle.modelName IS NULL";
    private static final String _FINDER_COLUMN_MODELNAME_MODELNAME_2 = "lower(vehicle.modelName) = ?";
    private static final String _FINDER_COLUMN_MODELNAME_MODELNAME_3 = "(vehicle.modelName IS NULL OR vehicle.modelName = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MODELNAMELIKE =
        new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByModelNameLike",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_MODELNAMELIKE =
        new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByModelNameLike",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_MODELNAMELIKE_MODELNAME_1 = "vehicle.modelName LIKE NULL";
    private static final String _FINDER_COLUMN_MODELNAMELIKE_MODELNAME_2 = "lower(vehicle.modelName) LIKE ?";
    private static final String _FINDER_COLUMN_MODELNAMELIKE_MODELNAME_3 = "(vehicle.modelName IS NULL OR vehicle.modelName LIKE '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LICENSEPLATE =
        new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByLicensePlate",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LICENSEPLATE =
        new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByLicensePlate",
            new String[] { String.class.getName() },
            VehicleModelImpl.LICENSEPLATE_COLUMN_BITMASK |
            VehicleModelImpl.MANUFACTURERID_COLUMN_BITMASK |
            VehicleModelImpl.MODELNAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_LICENSEPLATE = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLicensePlate",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_1 = "vehicle.licensePlate IS NULL";
    private static final String _FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_2 = "lower(vehicle.licensePlate) = ?";
    private static final String _FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_3 = "(vehicle.licensePlate IS NULL OR vehicle.licensePlate = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LICENSEPLATELIKE =
        new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByLicensePlateLike",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_LICENSEPLATELIKE =
        new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByLicensePlateLike",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_1 = "vehicle.licensePlate LIKE NULL";
    private static final String _FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_2 = "lower(vehicle.licensePlate) LIKE ?";
    private static final String _FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_3 = "(vehicle.licensePlate IS NULL OR vehicle.licensePlate LIKE '')";
    private static final String _SQL_SELECT_VEHICLE = "SELECT vehicle FROM Vehicle vehicle";
    private static final String _SQL_SELECT_VEHICLE_WHERE = "SELECT vehicle FROM Vehicle vehicle WHERE ";
    private static final String _SQL_COUNT_VEHICLE = "SELECT COUNT(vehicle) FROM Vehicle vehicle";
    private static final String _SQL_COUNT_VEHICLE_WHERE = "SELECT COUNT(vehicle) FROM Vehicle vehicle WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "vehicle.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Vehicle exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Vehicle exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(VehiclePersistenceImpl.class);
    private static Vehicle _nullVehicle = new VehicleImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Vehicle> toCacheModel() {
                return _nullVehicleCacheModel;
            }
        };

    private static CacheModel<Vehicle> _nullVehicleCacheModel = new CacheModel<Vehicle>() {
            @Override
            public Vehicle toEntityModel() {
                return _nullVehicle;
            }
        };

    public VehiclePersistenceImpl() {
        setModelClass(Vehicle.class);
    }

    /**
     * Returns all the vehicles where driverId = &#63;.
     *
     * @param driverId the driver ID
     * @return the matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByDriverId(long driverId)
        throws SystemException {
        return findByDriverId(driverId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the vehicles where driverId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param driverId the driver ID
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @return the range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByDriverId(long driverId, int start, int end)
        throws SystemException {
        return findByDriverId(driverId, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicles where driverId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param driverId the driver ID
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByDriverId(long driverId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERID;
            finderArgs = new Object[] { driverId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DRIVERID;
            finderArgs = new Object[] { driverId, start, end, orderByComparator };
        }

        List<Vehicle> list = (List<Vehicle>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Vehicle vehicle : list) {
                if ((driverId != vehicle.getDriverId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLE_WHERE);

            query.append(_FINDER_COLUMN_DRIVERID_DRIVERID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(driverId);

                if (!pagination) {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Vehicle>(list);
                } else {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle in the ordered set where driverId = &#63;.
     *
     * @param driverId the driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByDriverId_First(long driverId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByDriverId_First(driverId, orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("driverId=");
        msg.append(driverId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the first vehicle in the ordered set where driverId = &#63;.
     *
     * @param driverId the driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByDriverId_First(long driverId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Vehicle> list = findByDriverId(driverId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle in the ordered set where driverId = &#63;.
     *
     * @param driverId the driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByDriverId_Last(long driverId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByDriverId_Last(driverId, orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("driverId=");
        msg.append(driverId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the last vehicle in the ordered set where driverId = &#63;.
     *
     * @param driverId the driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByDriverId_Last(long driverId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByDriverId(driverId);

        if (count == 0) {
            return null;
        }

        List<Vehicle> list = findByDriverId(driverId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicles before and after the current vehicle in the ordered set where driverId = &#63;.
     *
     * @param vehicleId the primary key of the current vehicle
     * @param driverId the driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle[] findByDriverId_PrevAndNext(long vehicleId, long driverId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = findByPrimaryKey(vehicleId);

        Session session = null;

        try {
            session = openSession();

            Vehicle[] array = new VehicleImpl[3];

            array[0] = getByDriverId_PrevAndNext(session, vehicle, driverId,
                    orderByComparator, true);

            array[1] = vehicle;

            array[2] = getByDriverId_PrevAndNext(session, vehicle, driverId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Vehicle getByDriverId_PrevAndNext(Session session,
        Vehicle vehicle, long driverId, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLE_WHERE);

        query.append(_FINDER_COLUMN_DRIVERID_DRIVERID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(driverId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicle);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Vehicle> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicles where driverId = &#63; from the database.
     *
     * @param driverId the driver ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByDriverId(long driverId) throws SystemException {
        for (Vehicle vehicle : findByDriverId(driverId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(vehicle);
        }
    }

    /**
     * Returns the number of vehicles where driverId = &#63;.
     *
     * @param driverId the driver ID
     * @return the number of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByDriverId(long driverId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_DRIVERID;

        Object[] finderArgs = new Object[] { driverId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLE_WHERE);

            query.append(_FINDER_COLUMN_DRIVERID_DRIVERID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(driverId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicles where driverId = &#63; and organizationId = &#63;.
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @return the matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByDriverIdAndOrgaId(long driverId,
        long organizationId) throws SystemException {
        return findByDriverIdAndOrgaId(driverId, organizationId,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicles where driverId = &#63; and organizationId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @return the range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByDriverIdAndOrgaId(long driverId,
        long organizationId, int start, int end) throws SystemException {
        return findByDriverIdAndOrgaId(driverId, organizationId, start, end,
            null);
    }

    /**
     * Returns an ordered range of all the vehicles where driverId = &#63; and organizationId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByDriverIdAndOrgaId(long driverId,
        long organizationId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDORGAID;
            finderArgs = new Object[] { driverId, organizationId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DRIVERIDANDORGAID;
            finderArgs = new Object[] {
                    driverId, organizationId,
                    
                    start, end, orderByComparator
                };
        }

        List<Vehicle> list = (List<Vehicle>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Vehicle vehicle : list) {
                if ((driverId != vehicle.getDriverId()) ||
                        (organizationId != vehicle.getOrganizationId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_VEHICLE_WHERE);

            query.append(_FINDER_COLUMN_DRIVERIDANDORGAID_DRIVERID_2);

            query.append(_FINDER_COLUMN_DRIVERIDANDORGAID_ORGANIZATIONID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(driverId);

                qPos.add(organizationId);

                if (!pagination) {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Vehicle>(list);
                } else {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle in the ordered set where driverId = &#63; and organizationId = &#63;.
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByDriverIdAndOrgaId_First(long driverId,
        long organizationId, OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByDriverIdAndOrgaId_First(driverId,
                organizationId, orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("driverId=");
        msg.append(driverId);

        msg.append(", organizationId=");
        msg.append(organizationId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the first vehicle in the ordered set where driverId = &#63; and organizationId = &#63;.
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByDriverIdAndOrgaId_First(long driverId,
        long organizationId, OrderByComparator orderByComparator)
        throws SystemException {
        List<Vehicle> list = findByDriverIdAndOrgaId(driverId, organizationId,
                0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle in the ordered set where driverId = &#63; and organizationId = &#63;.
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByDriverIdAndOrgaId_Last(long driverId,
        long organizationId, OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByDriverIdAndOrgaId_Last(driverId,
                organizationId, orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("driverId=");
        msg.append(driverId);

        msg.append(", organizationId=");
        msg.append(organizationId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the last vehicle in the ordered set where driverId = &#63; and organizationId = &#63;.
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByDriverIdAndOrgaId_Last(long driverId,
        long organizationId, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByDriverIdAndOrgaId(driverId, organizationId);

        if (count == 0) {
            return null;
        }

        List<Vehicle> list = findByDriverIdAndOrgaId(driverId, organizationId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicles before and after the current vehicle in the ordered set where driverId = &#63; and organizationId = &#63;.
     *
     * @param vehicleId the primary key of the current vehicle
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle[] findByDriverIdAndOrgaId_PrevAndNext(long vehicleId,
        long driverId, long organizationId, OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = findByPrimaryKey(vehicleId);

        Session session = null;

        try {
            session = openSession();

            Vehicle[] array = new VehicleImpl[3];

            array[0] = getByDriverIdAndOrgaId_PrevAndNext(session, vehicle,
                    driverId, organizationId, orderByComparator, true);

            array[1] = vehicle;

            array[2] = getByDriverIdAndOrgaId_PrevAndNext(session, vehicle,
                    driverId, organizationId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Vehicle getByDriverIdAndOrgaId_PrevAndNext(Session session,
        Vehicle vehicle, long driverId, long organizationId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLE_WHERE);

        query.append(_FINDER_COLUMN_DRIVERIDANDORGAID_DRIVERID_2);

        query.append(_FINDER_COLUMN_DRIVERIDANDORGAID_ORGANIZATIONID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(driverId);

        qPos.add(organizationId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicle);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Vehicle> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicles where driverId = &#63; and organizationId = &#63; from the database.
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByDriverIdAndOrgaId(long driverId, long organizationId)
        throws SystemException {
        for (Vehicle vehicle : findByDriverIdAndOrgaId(driverId,
                organizationId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(vehicle);
        }
    }

    /**
     * Returns the number of vehicles where driverId = &#63; and organizationId = &#63;.
     *
     * @param driverId the driver ID
     * @param organizationId the organization ID
     * @return the number of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByDriverIdAndOrgaId(long driverId, long organizationId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_DRIVERIDANDORGAID;

        Object[] finderArgs = new Object[] { driverId, organizationId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_VEHICLE_WHERE);

            query.append(_FINDER_COLUMN_DRIVERIDANDORGAID_DRIVERID_2);

            query.append(_FINDER_COLUMN_DRIVERIDANDORGAID_ORGANIZATIONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(driverId);

                qPos.add(organizationId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicles where typeId = &#63;.
     *
     * @param typeId the type ID
     * @return the matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByTypeId(long typeId) throws SystemException {
        return findByTypeId(typeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicles where typeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param typeId the type ID
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @return the range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByTypeId(long typeId, int start, int end)
        throws SystemException {
        return findByTypeId(typeId, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicles where typeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param typeId the type ID
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByTypeId(long typeId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPEID;
            finderArgs = new Object[] { typeId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TYPEID;
            finderArgs = new Object[] { typeId, start, end, orderByComparator };
        }

        List<Vehicle> list = (List<Vehicle>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Vehicle vehicle : list) {
                if ((typeId != vehicle.getTypeId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLE_WHERE);

            query.append(_FINDER_COLUMN_TYPEID_TYPEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(typeId);

                if (!pagination) {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Vehicle>(list);
                } else {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle in the ordered set where typeId = &#63;.
     *
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByTypeId_First(long typeId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByTypeId_First(typeId, orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("typeId=");
        msg.append(typeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the first vehicle in the ordered set where typeId = &#63;.
     *
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByTypeId_First(long typeId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Vehicle> list = findByTypeId(typeId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle in the ordered set where typeId = &#63;.
     *
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByTypeId_Last(long typeId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByTypeId_Last(typeId, orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("typeId=");
        msg.append(typeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the last vehicle in the ordered set where typeId = &#63;.
     *
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByTypeId_Last(long typeId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByTypeId(typeId);

        if (count == 0) {
            return null;
        }

        List<Vehicle> list = findByTypeId(typeId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicles before and after the current vehicle in the ordered set where typeId = &#63;.
     *
     * @param vehicleId the primary key of the current vehicle
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle[] findByTypeId_PrevAndNext(long vehicleId, long typeId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = findByPrimaryKey(vehicleId);

        Session session = null;

        try {
            session = openSession();

            Vehicle[] array = new VehicleImpl[3];

            array[0] = getByTypeId_PrevAndNext(session, vehicle, typeId,
                    orderByComparator, true);

            array[1] = vehicle;

            array[2] = getByTypeId_PrevAndNext(session, vehicle, typeId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Vehicle getByTypeId_PrevAndNext(Session session, Vehicle vehicle,
        long typeId, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLE_WHERE);

        query.append(_FINDER_COLUMN_TYPEID_TYPEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(typeId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicle);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Vehicle> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicles where typeId = &#63; from the database.
     *
     * @param typeId the type ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByTypeId(long typeId) throws SystemException {
        for (Vehicle vehicle : findByTypeId(typeId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(vehicle);
        }
    }

    /**
     * Returns the number of vehicles where typeId = &#63;.
     *
     * @param typeId the type ID
     * @return the number of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByTypeId(long typeId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_TYPEID;

        Object[] finderArgs = new Object[] { typeId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLE_WHERE);

            query.append(_FINDER_COLUMN_TYPEID_TYPEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(typeId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicles where driverId = &#63; and typeId = &#63;.
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @return the matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByDriverIdAndTypeId(long driverId, long typeId)
        throws SystemException {
        return findByDriverIdAndTypeId(driverId, typeId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicles where driverId = &#63; and typeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @return the range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByDriverIdAndTypeId(long driverId, long typeId,
        int start, int end) throws SystemException {
        return findByDriverIdAndTypeId(driverId, typeId, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicles where driverId = &#63; and typeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByDriverIdAndTypeId(long driverId, long typeId,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDTYPEID;
            finderArgs = new Object[] { driverId, typeId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DRIVERIDANDTYPEID;
            finderArgs = new Object[] {
                    driverId, typeId,
                    
                    start, end, orderByComparator
                };
        }

        List<Vehicle> list = (List<Vehicle>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Vehicle vehicle : list) {
                if ((driverId != vehicle.getDriverId()) ||
                        (typeId != vehicle.getTypeId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_VEHICLE_WHERE);

            query.append(_FINDER_COLUMN_DRIVERIDANDTYPEID_DRIVERID_2);

            query.append(_FINDER_COLUMN_DRIVERIDANDTYPEID_TYPEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(driverId);

                qPos.add(typeId);

                if (!pagination) {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Vehicle>(list);
                } else {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle in the ordered set where driverId = &#63; and typeId = &#63;.
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByDriverIdAndTypeId_First(long driverId, long typeId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByDriverIdAndTypeId_First(driverId, typeId,
                orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("driverId=");
        msg.append(driverId);

        msg.append(", typeId=");
        msg.append(typeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the first vehicle in the ordered set where driverId = &#63; and typeId = &#63;.
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByDriverIdAndTypeId_First(long driverId, long typeId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Vehicle> list = findByDriverIdAndTypeId(driverId, typeId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle in the ordered set where driverId = &#63; and typeId = &#63;.
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByDriverIdAndTypeId_Last(long driverId, long typeId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByDriverIdAndTypeId_Last(driverId, typeId,
                orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("driverId=");
        msg.append(driverId);

        msg.append(", typeId=");
        msg.append(typeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the last vehicle in the ordered set where driverId = &#63; and typeId = &#63;.
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByDriverIdAndTypeId_Last(long driverId, long typeId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByDriverIdAndTypeId(driverId, typeId);

        if (count == 0) {
            return null;
        }

        List<Vehicle> list = findByDriverIdAndTypeId(driverId, typeId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicles before and after the current vehicle in the ordered set where driverId = &#63; and typeId = &#63;.
     *
     * @param vehicleId the primary key of the current vehicle
     * @param driverId the driver ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle[] findByDriverIdAndTypeId_PrevAndNext(long vehicleId,
        long driverId, long typeId, OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = findByPrimaryKey(vehicleId);

        Session session = null;

        try {
            session = openSession();

            Vehicle[] array = new VehicleImpl[3];

            array[0] = getByDriverIdAndTypeId_PrevAndNext(session, vehicle,
                    driverId, typeId, orderByComparator, true);

            array[1] = vehicle;

            array[2] = getByDriverIdAndTypeId_PrevAndNext(session, vehicle,
                    driverId, typeId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Vehicle getByDriverIdAndTypeId_PrevAndNext(Session session,
        Vehicle vehicle, long driverId, long typeId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLE_WHERE);

        query.append(_FINDER_COLUMN_DRIVERIDANDTYPEID_DRIVERID_2);

        query.append(_FINDER_COLUMN_DRIVERIDANDTYPEID_TYPEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(driverId);

        qPos.add(typeId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicle);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Vehicle> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicles where driverId = &#63; and typeId = &#63; from the database.
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByDriverIdAndTypeId(long driverId, long typeId)
        throws SystemException {
        for (Vehicle vehicle : findByDriverIdAndTypeId(driverId, typeId,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(vehicle);
        }
    }

    /**
     * Returns the number of vehicles where driverId = &#63; and typeId = &#63;.
     *
     * @param driverId the driver ID
     * @param typeId the type ID
     * @return the number of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByDriverIdAndTypeId(long driverId, long typeId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_DRIVERIDANDTYPEID;

        Object[] finderArgs = new Object[] { driverId, typeId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_VEHICLE_WHERE);

            query.append(_FINDER_COLUMN_DRIVERIDANDTYPEID_DRIVERID_2);

            query.append(_FINDER_COLUMN_DRIVERIDANDTYPEID_TYPEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(driverId);

                qPos.add(typeId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicles where manufacturerId = &#63;.
     *
     * @param manufacturerId the manufacturer ID
     * @return the matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByManufacturerId(long manufacturerId)
        throws SystemException {
        return findByManufacturerId(manufacturerId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicles where manufacturerId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param manufacturerId the manufacturer ID
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @return the range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByManufacturerId(long manufacturerId, int start,
        int end) throws SystemException {
        return findByManufacturerId(manufacturerId, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicles where manufacturerId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param manufacturerId the manufacturer ID
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByManufacturerId(long manufacturerId, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MANUFACTURERID;
            finderArgs = new Object[] { manufacturerId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MANUFACTURERID;
            finderArgs = new Object[] {
                    manufacturerId,
                    
                    start, end, orderByComparator
                };
        }

        List<Vehicle> list = (List<Vehicle>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Vehicle vehicle : list) {
                if ((manufacturerId != vehicle.getManufacturerId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLE_WHERE);

            query.append(_FINDER_COLUMN_MANUFACTURERID_MANUFACTURERID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(manufacturerId);

                if (!pagination) {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Vehicle>(list);
                } else {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle in the ordered set where manufacturerId = &#63;.
     *
     * @param manufacturerId the manufacturer ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByManufacturerId_First(long manufacturerId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByManufacturerId_First(manufacturerId,
                orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("manufacturerId=");
        msg.append(manufacturerId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the first vehicle in the ordered set where manufacturerId = &#63;.
     *
     * @param manufacturerId the manufacturer ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByManufacturerId_First(long manufacturerId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Vehicle> list = findByManufacturerId(manufacturerId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle in the ordered set where manufacturerId = &#63;.
     *
     * @param manufacturerId the manufacturer ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByManufacturerId_Last(long manufacturerId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByManufacturerId_Last(manufacturerId,
                orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("manufacturerId=");
        msg.append(manufacturerId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the last vehicle in the ordered set where manufacturerId = &#63;.
     *
     * @param manufacturerId the manufacturer ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByManufacturerId_Last(long manufacturerId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByManufacturerId(manufacturerId);

        if (count == 0) {
            return null;
        }

        List<Vehicle> list = findByManufacturerId(manufacturerId, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicles before and after the current vehicle in the ordered set where manufacturerId = &#63;.
     *
     * @param vehicleId the primary key of the current vehicle
     * @param manufacturerId the manufacturer ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle[] findByManufacturerId_PrevAndNext(long vehicleId,
        long manufacturerId, OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = findByPrimaryKey(vehicleId);

        Session session = null;

        try {
            session = openSession();

            Vehicle[] array = new VehicleImpl[3];

            array[0] = getByManufacturerId_PrevAndNext(session, vehicle,
                    manufacturerId, orderByComparator, true);

            array[1] = vehicle;

            array[2] = getByManufacturerId_PrevAndNext(session, vehicle,
                    manufacturerId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Vehicle getByManufacturerId_PrevAndNext(Session session,
        Vehicle vehicle, long manufacturerId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLE_WHERE);

        query.append(_FINDER_COLUMN_MANUFACTURERID_MANUFACTURERID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(manufacturerId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicle);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Vehicle> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicles where manufacturerId = &#63; from the database.
     *
     * @param manufacturerId the manufacturer ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByManufacturerId(long manufacturerId)
        throws SystemException {
        for (Vehicle vehicle : findByManufacturerId(manufacturerId,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(vehicle);
        }
    }

    /**
     * Returns the number of vehicles where manufacturerId = &#63;.
     *
     * @param manufacturerId the manufacturer ID
     * @return the number of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByManufacturerId(long manufacturerId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_MANUFACTURERID;

        Object[] finderArgs = new Object[] { manufacturerId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLE_WHERE);

            query.append(_FINDER_COLUMN_MANUFACTURERID_MANUFACTURERID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(manufacturerId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicles where modelName = &#63;.
     *
     * @param modelName the model name
     * @return the matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByModelName(String modelName)
        throws SystemException {
        return findByModelName(modelName, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the vehicles where modelName = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param modelName the model name
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @return the range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByModelName(String modelName, int start, int end)
        throws SystemException {
        return findByModelName(modelName, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicles where modelName = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param modelName the model name
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByModelName(String modelName, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODELNAME;
            finderArgs = new Object[] { modelName };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MODELNAME;
            finderArgs = new Object[] { modelName, start, end, orderByComparator };
        }

        List<Vehicle> list = (List<Vehicle>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Vehicle vehicle : list) {
                if (!Validator.equals(modelName, vehicle.getModelName())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLE_WHERE);

            boolean bindModelName = false;

            if (modelName == null) {
                query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_1);
            } else if (modelName.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_3);
            } else {
                bindModelName = true;

                query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindModelName) {
                    qPos.add(modelName.toLowerCase());
                }

                if (!pagination) {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Vehicle>(list);
                } else {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle in the ordered set where modelName = &#63;.
     *
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByModelName_First(String modelName,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByModelName_First(modelName, orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("modelName=");
        msg.append(modelName);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the first vehicle in the ordered set where modelName = &#63;.
     *
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByModelName_First(String modelName,
        OrderByComparator orderByComparator) throws SystemException {
        List<Vehicle> list = findByModelName(modelName, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle in the ordered set where modelName = &#63;.
     *
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByModelName_Last(String modelName,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByModelName_Last(modelName, orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("modelName=");
        msg.append(modelName);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the last vehicle in the ordered set where modelName = &#63;.
     *
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByModelName_Last(String modelName,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByModelName(modelName);

        if (count == 0) {
            return null;
        }

        List<Vehicle> list = findByModelName(modelName, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicles before and after the current vehicle in the ordered set where modelName = &#63;.
     *
     * @param vehicleId the primary key of the current vehicle
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle[] findByModelName_PrevAndNext(long vehicleId,
        String modelName, OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = findByPrimaryKey(vehicleId);

        Session session = null;

        try {
            session = openSession();

            Vehicle[] array = new VehicleImpl[3];

            array[0] = getByModelName_PrevAndNext(session, vehicle, modelName,
                    orderByComparator, true);

            array[1] = vehicle;

            array[2] = getByModelName_PrevAndNext(session, vehicle, modelName,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Vehicle getByModelName_PrevAndNext(Session session,
        Vehicle vehicle, String modelName, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLE_WHERE);

        boolean bindModelName = false;

        if (modelName == null) {
            query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_1);
        } else if (modelName.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_3);
        } else {
            bindModelName = true;

            query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindModelName) {
            qPos.add(modelName.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicle);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Vehicle> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicles where modelName = &#63; from the database.
     *
     * @param modelName the model name
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByModelName(String modelName) throws SystemException {
        for (Vehicle vehicle : findByModelName(modelName, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(vehicle);
        }
    }

    /**
     * Returns the number of vehicles where modelName = &#63;.
     *
     * @param modelName the model name
     * @return the number of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByModelName(String modelName) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_MODELNAME;

        Object[] finderArgs = new Object[] { modelName };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLE_WHERE);

            boolean bindModelName = false;

            if (modelName == null) {
                query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_1);
            } else if (modelName.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_3);
            } else {
                bindModelName = true;

                query.append(_FINDER_COLUMN_MODELNAME_MODELNAME_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindModelName) {
                    qPos.add(modelName.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicles where modelName LIKE &#63;.
     *
     * @param modelName the model name
     * @return the matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByModelNameLike(String modelName)
        throws SystemException {
        return findByModelNameLike(modelName, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicles where modelName LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param modelName the model name
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @return the range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByModelNameLike(String modelName, int start,
        int end) throws SystemException {
        return findByModelNameLike(modelName, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicles where modelName LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param modelName the model name
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByModelNameLike(String modelName, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MODELNAMELIKE;
        finderArgs = new Object[] { modelName, start, end, orderByComparator };

        List<Vehicle> list = (List<Vehicle>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Vehicle vehicle : list) {
                if (!StringUtil.wildcardMatches(vehicle.getModelName(),
                            modelName, CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, false)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLE_WHERE);

            boolean bindModelName = false;

            if (modelName == null) {
                query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_1);
            } else if (modelName.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_3);
            } else {
                bindModelName = true;

                query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindModelName) {
                    qPos.add(modelName.toLowerCase());
                }

                if (!pagination) {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Vehicle>(list);
                } else {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle in the ordered set where modelName LIKE &#63;.
     *
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByModelNameLike_First(String modelName,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByModelNameLike_First(modelName,
                orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("modelName=");
        msg.append(modelName);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the first vehicle in the ordered set where modelName LIKE &#63;.
     *
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByModelNameLike_First(String modelName,
        OrderByComparator orderByComparator) throws SystemException {
        List<Vehicle> list = findByModelNameLike(modelName, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle in the ordered set where modelName LIKE &#63;.
     *
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByModelNameLike_Last(String modelName,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByModelNameLike_Last(modelName, orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("modelName=");
        msg.append(modelName);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the last vehicle in the ordered set where modelName LIKE &#63;.
     *
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByModelNameLike_Last(String modelName,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByModelNameLike(modelName);

        if (count == 0) {
            return null;
        }

        List<Vehicle> list = findByModelNameLike(modelName, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicles before and after the current vehicle in the ordered set where modelName LIKE &#63;.
     *
     * @param vehicleId the primary key of the current vehicle
     * @param modelName the model name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle[] findByModelNameLike_PrevAndNext(long vehicleId,
        String modelName, OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = findByPrimaryKey(vehicleId);

        Session session = null;

        try {
            session = openSession();

            Vehicle[] array = new VehicleImpl[3];

            array[0] = getByModelNameLike_PrevAndNext(session, vehicle,
                    modelName, orderByComparator, true);

            array[1] = vehicle;

            array[2] = getByModelNameLike_PrevAndNext(session, vehicle,
                    modelName, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Vehicle getByModelNameLike_PrevAndNext(Session session,
        Vehicle vehicle, String modelName, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLE_WHERE);

        boolean bindModelName = false;

        if (modelName == null) {
            query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_1);
        } else if (modelName.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_3);
        } else {
            bindModelName = true;

            query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindModelName) {
            qPos.add(modelName.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicle);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Vehicle> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicles where modelName LIKE &#63; from the database.
     *
     * @param modelName the model name
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByModelNameLike(String modelName)
        throws SystemException {
        for (Vehicle vehicle : findByModelNameLike(modelName,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(vehicle);
        }
    }

    /**
     * Returns the number of vehicles where modelName LIKE &#63;.
     *
     * @param modelName the model name
     * @return the number of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByModelNameLike(String modelName) throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_MODELNAMELIKE;

        Object[] finderArgs = new Object[] { modelName };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLE_WHERE);

            boolean bindModelName = false;

            if (modelName == null) {
                query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_1);
            } else if (modelName.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_3);
            } else {
                bindModelName = true;

                query.append(_FINDER_COLUMN_MODELNAMELIKE_MODELNAME_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindModelName) {
                    qPos.add(modelName.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicles where licensePlate = &#63;.
     *
     * @param licensePlate the license plate
     * @return the matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByLicensePlate(String licensePlate)
        throws SystemException {
        return findByLicensePlate(licensePlate, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicles where licensePlate = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param licensePlate the license plate
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @return the range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByLicensePlate(String licensePlate, int start,
        int end) throws SystemException {
        return findByLicensePlate(licensePlate, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicles where licensePlate = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param licensePlate the license plate
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByLicensePlate(String licensePlate, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LICENSEPLATE;
            finderArgs = new Object[] { licensePlate };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LICENSEPLATE;
            finderArgs = new Object[] {
                    licensePlate,
                    
                    start, end, orderByComparator
                };
        }

        List<Vehicle> list = (List<Vehicle>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Vehicle vehicle : list) {
                if (!Validator.equals(licensePlate, vehicle.getLicensePlate())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLE_WHERE);

            boolean bindLicensePlate = false;

            if (licensePlate == null) {
                query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_1);
            } else if (licensePlate.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_3);
            } else {
                bindLicensePlate = true;

                query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindLicensePlate) {
                    qPos.add(licensePlate.toLowerCase());
                }

                if (!pagination) {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Vehicle>(list);
                } else {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle in the ordered set where licensePlate = &#63;.
     *
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByLicensePlate_First(String licensePlate,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByLicensePlate_First(licensePlate,
                orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("licensePlate=");
        msg.append(licensePlate);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the first vehicle in the ordered set where licensePlate = &#63;.
     *
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByLicensePlate_First(String licensePlate,
        OrderByComparator orderByComparator) throws SystemException {
        List<Vehicle> list = findByLicensePlate(licensePlate, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle in the ordered set where licensePlate = &#63;.
     *
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByLicensePlate_Last(String licensePlate,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByLicensePlate_Last(licensePlate,
                orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("licensePlate=");
        msg.append(licensePlate);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the last vehicle in the ordered set where licensePlate = &#63;.
     *
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByLicensePlate_Last(String licensePlate,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByLicensePlate(licensePlate);

        if (count == 0) {
            return null;
        }

        List<Vehicle> list = findByLicensePlate(licensePlate, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicles before and after the current vehicle in the ordered set where licensePlate = &#63;.
     *
     * @param vehicleId the primary key of the current vehicle
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle[] findByLicensePlate_PrevAndNext(long vehicleId,
        String licensePlate, OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = findByPrimaryKey(vehicleId);

        Session session = null;

        try {
            session = openSession();

            Vehicle[] array = new VehicleImpl[3];

            array[0] = getByLicensePlate_PrevAndNext(session, vehicle,
                    licensePlate, orderByComparator, true);

            array[1] = vehicle;

            array[2] = getByLicensePlate_PrevAndNext(session, vehicle,
                    licensePlate, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Vehicle getByLicensePlate_PrevAndNext(Session session,
        Vehicle vehicle, String licensePlate,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLE_WHERE);

        boolean bindLicensePlate = false;

        if (licensePlate == null) {
            query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_1);
        } else if (licensePlate.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_3);
        } else {
            bindLicensePlate = true;

            query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindLicensePlate) {
            qPos.add(licensePlate.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicle);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Vehicle> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicles where licensePlate = &#63; from the database.
     *
     * @param licensePlate the license plate
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByLicensePlate(String licensePlate)
        throws SystemException {
        for (Vehicle vehicle : findByLicensePlate(licensePlate,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(vehicle);
        }
    }

    /**
     * Returns the number of vehicles where licensePlate = &#63;.
     *
     * @param licensePlate the license plate
     * @return the number of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByLicensePlate(String licensePlate)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_LICENSEPLATE;

        Object[] finderArgs = new Object[] { licensePlate };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLE_WHERE);

            boolean bindLicensePlate = false;

            if (licensePlate == null) {
                query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_1);
            } else if (licensePlate.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_3);
            } else {
                bindLicensePlate = true;

                query.append(_FINDER_COLUMN_LICENSEPLATE_LICENSEPLATE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindLicensePlate) {
                    qPos.add(licensePlate.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicles where licensePlate LIKE &#63;.
     *
     * @param licensePlate the license plate
     * @return the matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByLicensePlateLike(String licensePlate)
        throws SystemException {
        return findByLicensePlateLike(licensePlate, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicles where licensePlate LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param licensePlate the license plate
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @return the range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByLicensePlateLike(String licensePlate, int start,
        int end) throws SystemException {
        return findByLicensePlateLike(licensePlate, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicles where licensePlate LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param licensePlate the license plate
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByLicensePlateLike(String licensePlate, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LICENSEPLATELIKE;
        finderArgs = new Object[] { licensePlate, start, end, orderByComparator };

        List<Vehicle> list = (List<Vehicle>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Vehicle vehicle : list) {
                if (!StringUtil.wildcardMatches(vehicle.getLicensePlate(),
                            licensePlate, CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, false)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLE_WHERE);

            boolean bindLicensePlate = false;

            if (licensePlate == null) {
                query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_1);
            } else if (licensePlate.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_3);
            } else {
                bindLicensePlate = true;

                query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindLicensePlate) {
                    qPos.add(licensePlate.toLowerCase());
                }

                if (!pagination) {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Vehicle>(list);
                } else {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle in the ordered set where licensePlate LIKE &#63;.
     *
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByLicensePlateLike_First(String licensePlate,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByLicensePlateLike_First(licensePlate,
                orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("licensePlate=");
        msg.append(licensePlate);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the first vehicle in the ordered set where licensePlate LIKE &#63;.
     *
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByLicensePlateLike_First(String licensePlate,
        OrderByComparator orderByComparator) throws SystemException {
        List<Vehicle> list = findByLicensePlateLike(licensePlate, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle in the ordered set where licensePlate LIKE &#63;.
     *
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByLicensePlateLike_Last(String licensePlate,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByLicensePlateLike_Last(licensePlate,
                orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("licensePlate=");
        msg.append(licensePlate);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the last vehicle in the ordered set where licensePlate LIKE &#63;.
     *
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByLicensePlateLike_Last(String licensePlate,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByLicensePlateLike(licensePlate);

        if (count == 0) {
            return null;
        }

        List<Vehicle> list = findByLicensePlateLike(licensePlate, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicles before and after the current vehicle in the ordered set where licensePlate LIKE &#63;.
     *
     * @param vehicleId the primary key of the current vehicle
     * @param licensePlate the license plate
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle[] findByLicensePlateLike_PrevAndNext(long vehicleId,
        String licensePlate, OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = findByPrimaryKey(vehicleId);

        Session session = null;

        try {
            session = openSession();

            Vehicle[] array = new VehicleImpl[3];

            array[0] = getByLicensePlateLike_PrevAndNext(session, vehicle,
                    licensePlate, orderByComparator, true);

            array[1] = vehicle;

            array[2] = getByLicensePlateLike_PrevAndNext(session, vehicle,
                    licensePlate, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Vehicle getByLicensePlateLike_PrevAndNext(Session session,
        Vehicle vehicle, String licensePlate,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLE_WHERE);

        boolean bindLicensePlate = false;

        if (licensePlate == null) {
            query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_1);
        } else if (licensePlate.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_3);
        } else {
            bindLicensePlate = true;

            query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindLicensePlate) {
            qPos.add(licensePlate.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicle);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Vehicle> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicles where licensePlate LIKE &#63; from the database.
     *
     * @param licensePlate the license plate
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByLicensePlateLike(String licensePlate)
        throws SystemException {
        for (Vehicle vehicle : findByLicensePlateLike(licensePlate,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(vehicle);
        }
    }

    /**
     * Returns the number of vehicles where licensePlate LIKE &#63;.
     *
     * @param licensePlate the license plate
     * @return the number of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByLicensePlateLike(String licensePlate)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_LICENSEPLATELIKE;

        Object[] finderArgs = new Object[] { licensePlate };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLE_WHERE);

            boolean bindLicensePlate = false;

            if (licensePlate == null) {
                query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_1);
            } else if (licensePlate.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_3);
            } else {
                bindLicensePlate = true;

                query.append(_FINDER_COLUMN_LICENSEPLATELIKE_LICENSEPLATE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindLicensePlate) {
                    qPos.add(licensePlate.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the vehicle in the entity cache if it is enabled.
     *
     * @param vehicle the vehicle
     */
    @Override
    public void cacheResult(Vehicle vehicle) {
        EntityCacheUtil.putResult(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleImpl.class, vehicle.getPrimaryKey(), vehicle);

        vehicle.resetOriginalValues();
    }

    /**
     * Caches the vehicles in the entity cache if it is enabled.
     *
     * @param vehicles the vehicles
     */
    @Override
    public void cacheResult(List<Vehicle> vehicles) {
        for (Vehicle vehicle : vehicles) {
            if (EntityCacheUtil.getResult(
                        VehicleModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleImpl.class, vehicle.getPrimaryKey()) == null) {
                cacheResult(vehicle);
            } else {
                vehicle.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all vehicles.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(VehicleImpl.class.getName());
        }

        EntityCacheUtil.clearCache(VehicleImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the vehicle.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Vehicle vehicle) {
        EntityCacheUtil.removeResult(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleImpl.class, vehicle.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Vehicle> vehicles) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Vehicle vehicle : vehicles) {
            EntityCacheUtil.removeResult(VehicleModelImpl.ENTITY_CACHE_ENABLED,
                VehicleImpl.class, vehicle.getPrimaryKey());
        }
    }

    /**
     * Creates a new vehicle with the primary key. Does not add the vehicle to the database.
     *
     * @param vehicleId the primary key for the new vehicle
     * @return the new vehicle
     */
    @Override
    public Vehicle create(long vehicleId) {
        Vehicle vehicle = new VehicleImpl();

        vehicle.setNew(true);
        vehicle.setPrimaryKey(vehicleId);

        return vehicle;
    }

    /**
     * Removes the vehicle with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param vehicleId the primary key of the vehicle
     * @return the vehicle that was removed
     * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle remove(long vehicleId)
        throws NoSuchVehicleException, SystemException {
        return remove((Serializable) vehicleId);
    }

    /**
     * Removes the vehicle with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the vehicle
     * @return the vehicle that was removed
     * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle remove(Serializable primaryKey)
        throws NoSuchVehicleException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Vehicle vehicle = (Vehicle) session.get(VehicleImpl.class,
                    primaryKey);

            if (vehicle == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchVehicleException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(vehicle);
        } catch (NoSuchVehicleException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Vehicle removeImpl(Vehicle vehicle) throws SystemException {
        vehicle = toUnwrappedModel(vehicle);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(vehicle)) {
                vehicle = (Vehicle) session.get(VehicleImpl.class,
                        vehicle.getPrimaryKeyObj());
            }

            if (vehicle != null) {
                session.delete(vehicle);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (vehicle != null) {
            clearCache(vehicle);
        }

        return vehicle;
    }

    @Override
    public Vehicle updateImpl(de.humance.eco.profile.model.Vehicle vehicle)
        throws SystemException {
        vehicle = toUnwrappedModel(vehicle);

        boolean isNew = vehicle.isNew();

        VehicleModelImpl vehicleModelImpl = (VehicleModelImpl) vehicle;

        Session session = null;

        try {
            session = openSession();

            if (vehicle.isNew()) {
                session.save(vehicle);

                vehicle.setNew(false);
            } else {
                session.merge(vehicle);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !VehicleModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((vehicleModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleModelImpl.getOriginalDriverId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DRIVERID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERID,
                    args);

                args = new Object[] { vehicleModelImpl.getDriverId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DRIVERID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERID,
                    args);
            }

            if ((vehicleModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDORGAID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleModelImpl.getOriginalDriverId(),
                        vehicleModelImpl.getOriginalOrganizationId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DRIVERIDANDORGAID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDORGAID,
                    args);

                args = new Object[] {
                        vehicleModelImpl.getDriverId(),
                        vehicleModelImpl.getOrganizationId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DRIVERIDANDORGAID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDORGAID,
                    args);
            }

            if ((vehicleModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPEID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleModelImpl.getOriginalTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TYPEID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPEID,
                    args);

                args = new Object[] { vehicleModelImpl.getTypeId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TYPEID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPEID,
                    args);
            }

            if ((vehicleModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDTYPEID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleModelImpl.getOriginalDriverId(),
                        vehicleModelImpl.getOriginalTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DRIVERIDANDTYPEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDTYPEID,
                    args);

                args = new Object[] {
                        vehicleModelImpl.getDriverId(),
                        vehicleModelImpl.getTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DRIVERIDANDTYPEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DRIVERIDANDTYPEID,
                    args);
            }

            if ((vehicleModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MANUFACTURERID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleModelImpl.getOriginalManufacturerId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MANUFACTURERID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MANUFACTURERID,
                    args);

                args = new Object[] { vehicleModelImpl.getManufacturerId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MANUFACTURERID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MANUFACTURERID,
                    args);
            }

            if ((vehicleModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODELNAME.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleModelImpl.getOriginalModelName()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MODELNAME,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODELNAME,
                    args);

                args = new Object[] { vehicleModelImpl.getModelName() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MODELNAME,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MODELNAME,
                    args);
            }

            if ((vehicleModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LICENSEPLATE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleModelImpl.getOriginalLicensePlate()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LICENSEPLATE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LICENSEPLATE,
                    args);

                args = new Object[] { vehicleModelImpl.getLicensePlate() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LICENSEPLATE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LICENSEPLATE,
                    args);
            }
        }

        EntityCacheUtil.putResult(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleImpl.class, vehicle.getPrimaryKey(), vehicle);

        return vehicle;
    }

    protected Vehicle toUnwrappedModel(Vehicle vehicle) {
        if (vehicle instanceof VehicleImpl) {
            return vehicle;
        }

        VehicleImpl vehicleImpl = new VehicleImpl();

        vehicleImpl.setNew(vehicle.isNew());
        vehicleImpl.setPrimaryKey(vehicle.getPrimaryKey());

        vehicleImpl.setVehicleId(vehicle.getVehicleId());
        vehicleImpl.setCreatorId(vehicle.getCreatorId());
        vehicleImpl.setCreatorName(vehicle.getCreatorName());
        vehicleImpl.setCreateDate(vehicle.getCreateDate());
        vehicleImpl.setModifierId(vehicle.getModifierId());
        vehicleImpl.setModifierName(vehicle.getModifierName());
        vehicleImpl.setModifiedDate(vehicle.getModifiedDate());
        vehicleImpl.setDriverId(vehicle.getDriverId());
        vehicleImpl.setTypeId(vehicle.getTypeId());
        vehicleImpl.setManufacturerId(vehicle.getManufacturerId());
        vehicleImpl.setOrganizationId(vehicle.getOrganizationId());
        vehicleImpl.setModelName(vehicle.getModelName());
        vehicleImpl.setDimensionHeight(vehicle.getDimensionHeight());
        vehicleImpl.setDimensionWidth(vehicle.getDimensionWidth());
        vehicleImpl.setDimensionDepth(vehicle.getDimensionDepth());
        vehicleImpl.setLicensePlate(vehicle.getLicensePlate());

        return vehicleImpl;
    }

    /**
     * Returns the vehicle with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle
     * @return the vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByPrimaryKey(Serializable primaryKey)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByPrimaryKey(primaryKey);

        if (vehicle == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchVehicleException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return vehicle;
    }

    /**
     * Returns the vehicle with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleException} if it could not be found.
     *
     * @param vehicleId the primary key of the vehicle
     * @return the vehicle
     * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByPrimaryKey(long vehicleId)
        throws NoSuchVehicleException, SystemException {
        return findByPrimaryKey((Serializable) vehicleId);
    }

    /**
     * Returns the vehicle with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle
     * @return the vehicle, or <code>null</code> if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Vehicle vehicle = (Vehicle) EntityCacheUtil.getResult(VehicleModelImpl.ENTITY_CACHE_ENABLED,
                VehicleImpl.class, primaryKey);

        if (vehicle == _nullVehicle) {
            return null;
        }

        if (vehicle == null) {
            Session session = null;

            try {
                session = openSession();

                vehicle = (Vehicle) session.get(VehicleImpl.class, primaryKey);

                if (vehicle != null) {
                    cacheResult(vehicle);
                } else {
                    EntityCacheUtil.putResult(VehicleModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleImpl.class, primaryKey, _nullVehicle);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(VehicleModelImpl.ENTITY_CACHE_ENABLED,
                    VehicleImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return vehicle;
    }

    /**
     * Returns the vehicle with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param vehicleId the primary key of the vehicle
     * @return the vehicle, or <code>null</code> if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByPrimaryKey(long vehicleId) throws SystemException {
        return fetchByPrimaryKey((Serializable) vehicleId);
    }

    /**
     * Returns all the vehicles.
     *
     * @return the vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicles.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @return the range of vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicles.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Vehicle> list = (List<Vehicle>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_VEHICLE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_VEHICLE;

                if (pagination) {
                    sql = sql.concat(VehicleModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Vehicle>(list);
                } else {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the vehicles from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Vehicle vehicle : findAll()) {
            remove(vehicle);
        }
    }

    /**
     * Returns the number of vehicles.
     *
     * @return the number of vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_VEHICLE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the vehicle persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.de.humance.eco.profile.model.Vehicle")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Vehicle>> listenersList = new ArrayList<ModelListener<Vehicle>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Vehicle>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(VehicleImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
