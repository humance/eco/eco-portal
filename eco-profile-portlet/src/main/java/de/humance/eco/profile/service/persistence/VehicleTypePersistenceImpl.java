package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.humance.eco.profile.NoSuchVehicleTypeException;
import de.humance.eco.profile.model.VehicleType;
import de.humance.eco.profile.model.impl.VehicleTypeImpl;
import de.humance.eco.profile.model.impl.VehicleTypeModelImpl;
import de.humance.eco.profile.service.persistence.VehicleTypePersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the vehicle type service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehicleTypePersistence
 * @see VehicleTypeUtil
 * @generated
 */
public class VehicleTypePersistenceImpl extends BasePersistenceImpl<VehicleType>
    implements VehicleTypePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link VehicleTypeUtil} to access the vehicle type persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = VehicleTypeImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VehicleTypeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeModelImpl.FINDER_CACHE_ENABLED, VehicleTypeImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VehicleTypeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeModelImpl.FINDER_CACHE_ENABLED, VehicleTypeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VehicleTypeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME = new FinderPath(VehicleTypeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeModelImpl.FINDER_CACHE_ENABLED, VehicleTypeImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByName",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME = new FinderPath(VehicleTypeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeModelImpl.FINDER_CACHE_ENABLED, VehicleTypeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByName",
            new String[] { String.class.getName() },
            VehicleTypeModelImpl.NAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_NAME = new FinderPath(VehicleTypeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByName",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_NAME_NAME_1 = "vehicleType.name IS NULL";
    private static final String _FINDER_COLUMN_NAME_NAME_2 = "lower(vehicleType.name) = ?";
    private static final String _FINDER_COLUMN_NAME_NAME_3 = "(vehicleType.name IS NULL OR vehicleType.name = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NAMELIKE = new FinderPath(VehicleTypeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeModelImpl.FINDER_CACHE_ENABLED, VehicleTypeImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByNameLike",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_NAMELIKE =
        new FinderPath(VehicleTypeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByNameLike",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_NAMELIKE_NAME_1 = "vehicleType.name LIKE NULL";
    private static final String _FINDER_COLUMN_NAMELIKE_NAME_2 = "lower(vehicleType.name) LIKE ?";
    private static final String _FINDER_COLUMN_NAMELIKE_NAME_3 = "(vehicleType.name IS NULL OR vehicleType.name LIKE '')";
    private static final String _SQL_SELECT_VEHICLETYPE = "SELECT vehicleType FROM VehicleType vehicleType";
    private static final String _SQL_SELECT_VEHICLETYPE_WHERE = "SELECT vehicleType FROM VehicleType vehicleType WHERE ";
    private static final String _SQL_COUNT_VEHICLETYPE = "SELECT COUNT(vehicleType) FROM VehicleType vehicleType";
    private static final String _SQL_COUNT_VEHICLETYPE_WHERE = "SELECT COUNT(vehicleType) FROM VehicleType vehicleType WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "vehicleType.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No VehicleType exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No VehicleType exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(VehicleTypePersistenceImpl.class);
    private static VehicleType _nullVehicleType = new VehicleTypeImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<VehicleType> toCacheModel() {
                return _nullVehicleTypeCacheModel;
            }
        };

    private static CacheModel<VehicleType> _nullVehicleTypeCacheModel = new CacheModel<VehicleType>() {
            @Override
            public VehicleType toEntityModel() {
                return _nullVehicleType;
            }
        };

    public VehicleTypePersistenceImpl() {
        setModelClass(VehicleType.class);
    }

    /**
     * Returns all the vehicle types where name = &#63;.
     *
     * @param name the name
     * @return the matching vehicle types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleType> findByName(String name) throws SystemException {
        return findByName(name, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle types where name = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param name the name
     * @param start the lower bound of the range of vehicle types
     * @param end the upper bound of the range of vehicle types (not inclusive)
     * @return the range of matching vehicle types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleType> findByName(String name, int start, int end)
        throws SystemException {
        return findByName(name, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle types where name = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param name the name
     * @param start the lower bound of the range of vehicle types
     * @param end the upper bound of the range of vehicle types (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleType> findByName(String name, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME;
            finderArgs = new Object[] { name };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME;
            finderArgs = new Object[] { name, start, end, orderByComparator };
        }

        List<VehicleType> list = (List<VehicleType>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleType vehicleType : list) {
                if (!Validator.equals(name, vehicleType.getName())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLETYPE_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAME_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAME_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAME_NAME_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleTypeModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name.toLowerCase());
                }

                if (!pagination) {
                    list = (List<VehicleType>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleType>(list);
                } else {
                    list = (List<VehicleType>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle type in the ordered set where name = &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle type
     * @throws de.humance.eco.profile.NoSuchVehicleTypeException if a matching vehicle type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleType findByName_First(String name,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeException, SystemException {
        VehicleType vehicleType = fetchByName_First(name, orderByComparator);

        if (vehicleType != null) {
            return vehicleType;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("name=");
        msg.append(name);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleTypeException(msg.toString());
    }

    /**
     * Returns the first vehicle type in the ordered set where name = &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle type, or <code>null</code> if a matching vehicle type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleType fetchByName_First(String name,
        OrderByComparator orderByComparator) throws SystemException {
        List<VehicleType> list = findByName(name, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle type in the ordered set where name = &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle type
     * @throws de.humance.eco.profile.NoSuchVehicleTypeException if a matching vehicle type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleType findByName_Last(String name,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeException, SystemException {
        VehicleType vehicleType = fetchByName_Last(name, orderByComparator);

        if (vehicleType != null) {
            return vehicleType;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("name=");
        msg.append(name);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleTypeException(msg.toString());
    }

    /**
     * Returns the last vehicle type in the ordered set where name = &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle type, or <code>null</code> if a matching vehicle type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleType fetchByName_Last(String name,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByName(name);

        if (count == 0) {
            return null;
        }

        List<VehicleType> list = findByName(name, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle types before and after the current vehicle type in the ordered set where name = &#63;.
     *
     * @param vehicleTypeId the primary key of the current vehicle type
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle type
     * @throws de.humance.eco.profile.NoSuchVehicleTypeException if a vehicle type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleType[] findByName_PrevAndNext(long vehicleTypeId,
        String name, OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeException, SystemException {
        VehicleType vehicleType = findByPrimaryKey(vehicleTypeId);

        Session session = null;

        try {
            session = openSession();

            VehicleType[] array = new VehicleTypeImpl[3];

            array[0] = getByName_PrevAndNext(session, vehicleType, name,
                    orderByComparator, true);

            array[1] = vehicleType;

            array[2] = getByName_PrevAndNext(session, vehicleType, name,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleType getByName_PrevAndNext(Session session,
        VehicleType vehicleType, String name,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLETYPE_WHERE);

        boolean bindName = false;

        if (name == null) {
            query.append(_FINDER_COLUMN_NAME_NAME_1);
        } else if (name.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_NAME_NAME_3);
        } else {
            bindName = true;

            query.append(_FINDER_COLUMN_NAME_NAME_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleTypeModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindName) {
            qPos.add(name.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleType);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleType> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle types where name = &#63; from the database.
     *
     * @param name the name
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByName(String name) throws SystemException {
        for (VehicleType vehicleType : findByName(name, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(vehicleType);
        }
    }

    /**
     * Returns the number of vehicle types where name = &#63;.
     *
     * @param name the name
     * @return the number of matching vehicle types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByName(String name) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_NAME;

        Object[] finderArgs = new Object[] { name };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLETYPE_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAME_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAME_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAME_NAME_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicle types where name LIKE &#63;.
     *
     * @param name the name
     * @return the matching vehicle types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleType> findByNameLike(String name)
        throws SystemException {
        return findByNameLike(name, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle types where name LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param name the name
     * @param start the lower bound of the range of vehicle types
     * @param end the upper bound of the range of vehicle types (not inclusive)
     * @return the range of matching vehicle types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleType> findByNameLike(String name, int start, int end)
        throws SystemException {
        return findByNameLike(name, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle types where name LIKE &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param name the name
     * @param start the lower bound of the range of vehicle types
     * @param end the upper bound of the range of vehicle types (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleType> findByNameLike(String name, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NAMELIKE;
        finderArgs = new Object[] { name, start, end, orderByComparator };

        List<VehicleType> list = (List<VehicleType>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleType vehicleType : list) {
                if (!StringUtil.wildcardMatches(vehicleType.getName(), name,
                            CharPool.UNDERLINE, CharPool.PERCENT,
                            CharPool.BACK_SLASH, false)) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLETYPE_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAMELIKE_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAMELIKE_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAMELIKE_NAME_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleTypeModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name.toLowerCase());
                }

                if (!pagination) {
                    list = (List<VehicleType>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleType>(list);
                } else {
                    list = (List<VehicleType>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle type in the ordered set where name LIKE &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle type
     * @throws de.humance.eco.profile.NoSuchVehicleTypeException if a matching vehicle type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleType findByNameLike_First(String name,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeException, SystemException {
        VehicleType vehicleType = fetchByNameLike_First(name, orderByComparator);

        if (vehicleType != null) {
            return vehicleType;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("name=");
        msg.append(name);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleTypeException(msg.toString());
    }

    /**
     * Returns the first vehicle type in the ordered set where name LIKE &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle type, or <code>null</code> if a matching vehicle type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleType fetchByNameLike_First(String name,
        OrderByComparator orderByComparator) throws SystemException {
        List<VehicleType> list = findByNameLike(name, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle type in the ordered set where name LIKE &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle type
     * @throws de.humance.eco.profile.NoSuchVehicleTypeException if a matching vehicle type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleType findByNameLike_Last(String name,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeException, SystemException {
        VehicleType vehicleType = fetchByNameLike_Last(name, orderByComparator);

        if (vehicleType != null) {
            return vehicleType;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("name=");
        msg.append(name);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleTypeException(msg.toString());
    }

    /**
     * Returns the last vehicle type in the ordered set where name LIKE &#63;.
     *
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle type, or <code>null</code> if a matching vehicle type could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleType fetchByNameLike_Last(String name,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByNameLike(name);

        if (count == 0) {
            return null;
        }

        List<VehicleType> list = findByNameLike(name, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle types before and after the current vehicle type in the ordered set where name LIKE &#63;.
     *
     * @param vehicleTypeId the primary key of the current vehicle type
     * @param name the name
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle type
     * @throws de.humance.eco.profile.NoSuchVehicleTypeException if a vehicle type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleType[] findByNameLike_PrevAndNext(long vehicleTypeId,
        String name, OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeException, SystemException {
        VehicleType vehicleType = findByPrimaryKey(vehicleTypeId);

        Session session = null;

        try {
            session = openSession();

            VehicleType[] array = new VehicleTypeImpl[3];

            array[0] = getByNameLike_PrevAndNext(session, vehicleType, name,
                    orderByComparator, true);

            array[1] = vehicleType;

            array[2] = getByNameLike_PrevAndNext(session, vehicleType, name,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleType getByNameLike_PrevAndNext(Session session,
        VehicleType vehicleType, String name,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLETYPE_WHERE);

        boolean bindName = false;

        if (name == null) {
            query.append(_FINDER_COLUMN_NAMELIKE_NAME_1);
        } else if (name.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_NAMELIKE_NAME_3);
        } else {
            bindName = true;

            query.append(_FINDER_COLUMN_NAMELIKE_NAME_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleTypeModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindName) {
            qPos.add(name.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleType);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleType> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle types where name LIKE &#63; from the database.
     *
     * @param name the name
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByNameLike(String name) throws SystemException {
        for (VehicleType vehicleType : findByNameLike(name, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(vehicleType);
        }
    }

    /**
     * Returns the number of vehicle types where name LIKE &#63;.
     *
     * @param name the name
     * @return the number of matching vehicle types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByNameLike(String name) throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_NAMELIKE;

        Object[] finderArgs = new Object[] { name };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLETYPE_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAMELIKE_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAMELIKE_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAMELIKE_NAME_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the vehicle type in the entity cache if it is enabled.
     *
     * @param vehicleType the vehicle type
     */
    @Override
    public void cacheResult(VehicleType vehicleType) {
        EntityCacheUtil.putResult(VehicleTypeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeImpl.class, vehicleType.getPrimaryKey(), vehicleType);

        vehicleType.resetOriginalValues();
    }

    /**
     * Caches the vehicle types in the entity cache if it is enabled.
     *
     * @param vehicleTypes the vehicle types
     */
    @Override
    public void cacheResult(List<VehicleType> vehicleTypes) {
        for (VehicleType vehicleType : vehicleTypes) {
            if (EntityCacheUtil.getResult(
                        VehicleTypeModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleTypeImpl.class, vehicleType.getPrimaryKey()) == null) {
                cacheResult(vehicleType);
            } else {
                vehicleType.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all vehicle types.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(VehicleTypeImpl.class.getName());
        }

        EntityCacheUtil.clearCache(VehicleTypeImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the vehicle type.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(VehicleType vehicleType) {
        EntityCacheUtil.removeResult(VehicleTypeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeImpl.class, vehicleType.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<VehicleType> vehicleTypes) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (VehicleType vehicleType : vehicleTypes) {
            EntityCacheUtil.removeResult(VehicleTypeModelImpl.ENTITY_CACHE_ENABLED,
                VehicleTypeImpl.class, vehicleType.getPrimaryKey());
        }
    }

    /**
     * Creates a new vehicle type with the primary key. Does not add the vehicle type to the database.
     *
     * @param vehicleTypeId the primary key for the new vehicle type
     * @return the new vehicle type
     */
    @Override
    public VehicleType create(long vehicleTypeId) {
        VehicleType vehicleType = new VehicleTypeImpl();

        vehicleType.setNew(true);
        vehicleType.setPrimaryKey(vehicleTypeId);

        return vehicleType;
    }

    /**
     * Removes the vehicle type with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param vehicleTypeId the primary key of the vehicle type
     * @return the vehicle type that was removed
     * @throws de.humance.eco.profile.NoSuchVehicleTypeException if a vehicle type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleType remove(long vehicleTypeId)
        throws NoSuchVehicleTypeException, SystemException {
        return remove((Serializable) vehicleTypeId);
    }

    /**
     * Removes the vehicle type with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the vehicle type
     * @return the vehicle type that was removed
     * @throws de.humance.eco.profile.NoSuchVehicleTypeException if a vehicle type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleType remove(Serializable primaryKey)
        throws NoSuchVehicleTypeException, SystemException {
        Session session = null;

        try {
            session = openSession();

            VehicleType vehicleType = (VehicleType) session.get(VehicleTypeImpl.class,
                    primaryKey);

            if (vehicleType == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchVehicleTypeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(vehicleType);
        } catch (NoSuchVehicleTypeException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected VehicleType removeImpl(VehicleType vehicleType)
        throws SystemException {
        vehicleType = toUnwrappedModel(vehicleType);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(vehicleType)) {
                vehicleType = (VehicleType) session.get(VehicleTypeImpl.class,
                        vehicleType.getPrimaryKeyObj());
            }

            if (vehicleType != null) {
                session.delete(vehicleType);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (vehicleType != null) {
            clearCache(vehicleType);
        }

        return vehicleType;
    }

    @Override
    public VehicleType updateImpl(
        de.humance.eco.profile.model.VehicleType vehicleType)
        throws SystemException {
        vehicleType = toUnwrappedModel(vehicleType);

        boolean isNew = vehicleType.isNew();

        VehicleTypeModelImpl vehicleTypeModelImpl = (VehicleTypeModelImpl) vehicleType;

        Session session = null;

        try {
            session = openSession();

            if (vehicleType.isNew()) {
                session.save(vehicleType);

                vehicleType.setNew(false);
            } else {
                session.merge(vehicleType);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !VehicleTypeModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((vehicleTypeModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleTypeModelImpl.getOriginalName()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
                    args);

                args = new Object[] { vehicleTypeModelImpl.getName() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
                    args);
            }
        }

        EntityCacheUtil.putResult(VehicleTypeModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeImpl.class, vehicleType.getPrimaryKey(), vehicleType);

        return vehicleType;
    }

    protected VehicleType toUnwrappedModel(VehicleType vehicleType) {
        if (vehicleType instanceof VehicleTypeImpl) {
            return vehicleType;
        }

        VehicleTypeImpl vehicleTypeImpl = new VehicleTypeImpl();

        vehicleTypeImpl.setNew(vehicleType.isNew());
        vehicleTypeImpl.setPrimaryKey(vehicleType.getPrimaryKey());

        vehicleTypeImpl.setVehicleTypeId(vehicleType.getVehicleTypeId());
        vehicleTypeImpl.setCreatorId(vehicleType.getCreatorId());
        vehicleTypeImpl.setCreatorName(vehicleType.getCreatorName());
        vehicleTypeImpl.setCreateDate(vehicleType.getCreateDate());
        vehicleTypeImpl.setModifierId(vehicleType.getModifierId());
        vehicleTypeImpl.setModifierName(vehicleType.getModifierName());
        vehicleTypeImpl.setModifiedDate(vehicleType.getModifiedDate());
        vehicleTypeImpl.setName(vehicleType.getName());
        vehicleTypeImpl.setIconId(vehicleType.getIconId());

        return vehicleTypeImpl;
    }

    /**
     * Returns the vehicle type with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle type
     * @return the vehicle type
     * @throws de.humance.eco.profile.NoSuchVehicleTypeException if a vehicle type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleType findByPrimaryKey(Serializable primaryKey)
        throws NoSuchVehicleTypeException, SystemException {
        VehicleType vehicleType = fetchByPrimaryKey(primaryKey);

        if (vehicleType == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchVehicleTypeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return vehicleType;
    }

    /**
     * Returns the vehicle type with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleTypeException} if it could not be found.
     *
     * @param vehicleTypeId the primary key of the vehicle type
     * @return the vehicle type
     * @throws de.humance.eco.profile.NoSuchVehicleTypeException if a vehicle type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleType findByPrimaryKey(long vehicleTypeId)
        throws NoSuchVehicleTypeException, SystemException {
        return findByPrimaryKey((Serializable) vehicleTypeId);
    }

    /**
     * Returns the vehicle type with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle type
     * @return the vehicle type, or <code>null</code> if a vehicle type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleType fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        VehicleType vehicleType = (VehicleType) EntityCacheUtil.getResult(VehicleTypeModelImpl.ENTITY_CACHE_ENABLED,
                VehicleTypeImpl.class, primaryKey);

        if (vehicleType == _nullVehicleType) {
            return null;
        }

        if (vehicleType == null) {
            Session session = null;

            try {
                session = openSession();

                vehicleType = (VehicleType) session.get(VehicleTypeImpl.class,
                        primaryKey);

                if (vehicleType != null) {
                    cacheResult(vehicleType);
                } else {
                    EntityCacheUtil.putResult(VehicleTypeModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleTypeImpl.class, primaryKey, _nullVehicleType);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(VehicleTypeModelImpl.ENTITY_CACHE_ENABLED,
                    VehicleTypeImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return vehicleType;
    }

    /**
     * Returns the vehicle type with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param vehicleTypeId the primary key of the vehicle type
     * @return the vehicle type, or <code>null</code> if a vehicle type with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleType fetchByPrimaryKey(long vehicleTypeId)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) vehicleTypeId);
    }

    /**
     * Returns all the vehicle types.
     *
     * @return the vehicle types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleType> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle types.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicle types
     * @param end the upper bound of the range of vehicle types (not inclusive)
     * @return the range of vehicle types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleType> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle types.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicle types
     * @param end the upper bound of the range of vehicle types (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of vehicle types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleType> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<VehicleType> list = (List<VehicleType>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_VEHICLETYPE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_VEHICLETYPE;

                if (pagination) {
                    sql = sql.concat(VehicleTypeModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<VehicleType>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleType>(list);
                } else {
                    list = (List<VehicleType>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the vehicle types from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (VehicleType vehicleType : findAll()) {
            remove(vehicleType);
        }
    }

    /**
     * Returns the number of vehicle types.
     *
     * @return the number of vehicle types
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_VEHICLETYPE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the vehicle type persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.de.humance.eco.profile.model.VehicleType")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<VehicleType>> listenersList = new ArrayList<ModelListener<VehicleType>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<VehicleType>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(VehicleTypeImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
