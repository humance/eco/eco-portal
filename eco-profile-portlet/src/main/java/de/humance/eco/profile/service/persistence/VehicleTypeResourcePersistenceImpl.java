package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.humance.eco.profile.NoSuchVehicleTypeResourceException;
import de.humance.eco.profile.model.VehicleTypeResource;
import de.humance.eco.profile.model.impl.VehicleTypeResourceImpl;
import de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl;
import de.humance.eco.profile.service.persistence.VehicleTypeResourcePersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the vehicle type resource service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehicleTypeResourcePersistence
 * @see VehicleTypeResourceUtil
 * @generated
 */
public class VehicleTypeResourcePersistenceImpl extends BasePersistenceImpl<VehicleTypeResource>
    implements VehicleTypeResourcePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link VehicleTypeResourceUtil} to access the vehicle type resource persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = VehicleTypeResourceImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLETYPEID =
        new FinderPath(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByVehicleTypeId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEID =
        new FinderPath(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByVehicleTypeId",
            new String[] { Long.class.getName() },
            VehicleTypeResourceModelImpl.VEHICLETYPEID_COLUMN_BITMASK |
            VehicleTypeResourceModelImpl.COUNTRY_COLUMN_BITMASK |
            VehicleTypeResourceModelImpl.LANGUAGE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_VEHICLETYPEID = new FinderPath(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByVehicleTypeId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_VEHICLETYPEID_VEHICLETYPEID_2 = "vehicleTypeResource.vehicleTypeId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLETYPEIDANDCOUNTRY =
        new FinderPath(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByVehicleTypeIdAndCountry",
            new String[] {
                Long.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEIDANDCOUNTRY =
        new FinderPath(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByVehicleTypeIdAndCountry",
            new String[] { Long.class.getName(), String.class.getName() },
            VehicleTypeResourceModelImpl.VEHICLETYPEID_COLUMN_BITMASK |
            VehicleTypeResourceModelImpl.COUNTRY_COLUMN_BITMASK |
            VehicleTypeResourceModelImpl.LANGUAGE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_VEHICLETYPEIDANDCOUNTRY = new FinderPath(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByVehicleTypeIdAndCountry",
            new String[] { Long.class.getName(), String.class.getName() });
    private static final String _FINDER_COLUMN_VEHICLETYPEIDANDCOUNTRY_VEHICLETYPEID_2 =
        "vehicleTypeResource.vehicleTypeId = ? AND ";
    private static final String _FINDER_COLUMN_VEHICLETYPEIDANDCOUNTRY_COUNTRY_1 =
        "vehicleTypeResource.country IS NULL";
    private static final String _FINDER_COLUMN_VEHICLETYPEIDANDCOUNTRY_COUNTRY_2 =
        "lower(vehicleTypeResource.country) = ?";
    private static final String _FINDER_COLUMN_VEHICLETYPEIDANDCOUNTRY_COUNTRY_3 =
        "(vehicleTypeResource.country IS NULL OR vehicleTypeResource.country = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLETYPEIDANDLANGUAGE =
        new FinderPath(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByVehicleTypeIdAndLanguage",
            new String[] {
                Long.class.getName(), String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEIDANDLANGUAGE =
        new FinderPath(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByVehicleTypeIdAndLanguage",
            new String[] { Long.class.getName(), String.class.getName() },
            VehicleTypeResourceModelImpl.VEHICLETYPEID_COLUMN_BITMASK |
            VehicleTypeResourceModelImpl.LANGUAGE_COLUMN_BITMASK |
            VehicleTypeResourceModelImpl.COUNTRY_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_VEHICLETYPEIDANDLANGUAGE =
        new FinderPath(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByVehicleTypeIdAndLanguage",
            new String[] { Long.class.getName(), String.class.getName() });
    private static final String _FINDER_COLUMN_VEHICLETYPEIDANDLANGUAGE_VEHICLETYPEID_2 =
        "vehicleTypeResource.vehicleTypeId = ? AND ";
    private static final String _FINDER_COLUMN_VEHICLETYPEIDANDLANGUAGE_LANGUAGE_1 =
        "vehicleTypeResource.language IS NULL";
    private static final String _FINDER_COLUMN_VEHICLETYPEIDANDLANGUAGE_LANGUAGE_2 =
        "lower(vehicleTypeResource.language) = ?";
    private static final String _FINDER_COLUMN_VEHICLETYPEIDANDLANGUAGE_LANGUAGE_3 =
        "(vehicleTypeResource.language IS NULL OR vehicleTypeResource.language = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLETYPEIDANDLOCALE =
        new FinderPath(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByVehicleTypeIdAndLocale",
            new String[] {
                Long.class.getName(), String.class.getName(),
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEIDANDLOCALE =
        new FinderPath(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceModelImpl.FINDER_CACHE_ENABLED,
            VehicleTypeResourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByVehicleTypeIdAndLocale",
            new String[] {
                Long.class.getName(), String.class.getName(),
                String.class.getName()
            },
            VehicleTypeResourceModelImpl.VEHICLETYPEID_COLUMN_BITMASK |
            VehicleTypeResourceModelImpl.COUNTRY_COLUMN_BITMASK |
            VehicleTypeResourceModelImpl.LANGUAGE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_VEHICLETYPEIDANDLOCALE = new FinderPath(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByVehicleTypeIdAndLocale",
            new String[] {
                Long.class.getName(), String.class.getName(),
                String.class.getName()
            });
    private static final String _FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_VEHICLETYPEID_2 =
        "vehicleTypeResource.vehicleTypeId = ? AND ";
    private static final String _FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_COUNTRY_1 = "vehicleTypeResource.country IS NULL AND ";
    private static final String _FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_COUNTRY_2 = "lower(vehicleTypeResource.country) = ? AND ";
    private static final String _FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_COUNTRY_3 = "(vehicleTypeResource.country IS NULL OR vehicleTypeResource.country = '') AND ";
    private static final String _FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_LANGUAGE_1 =
        "vehicleTypeResource.language IS NULL";
    private static final String _FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_LANGUAGE_2 =
        "lower(vehicleTypeResource.language) = ?";
    private static final String _FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_LANGUAGE_3 =
        "(vehicleTypeResource.language IS NULL OR vehicleTypeResource.language = '')";
    private static final String _SQL_SELECT_VEHICLETYPERESOURCE = "SELECT vehicleTypeResource FROM VehicleTypeResource vehicleTypeResource";
    private static final String _SQL_SELECT_VEHICLETYPERESOURCE_WHERE = "SELECT vehicleTypeResource FROM VehicleTypeResource vehicleTypeResource WHERE ";
    private static final String _SQL_COUNT_VEHICLETYPERESOURCE = "SELECT COUNT(vehicleTypeResource) FROM VehicleTypeResource vehicleTypeResource";
    private static final String _SQL_COUNT_VEHICLETYPERESOURCE_WHERE = "SELECT COUNT(vehicleTypeResource) FROM VehicleTypeResource vehicleTypeResource WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "vehicleTypeResource.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No VehicleTypeResource exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No VehicleTypeResource exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(VehicleTypeResourcePersistenceImpl.class);
    private static VehicleTypeResource _nullVehicleTypeResource = new VehicleTypeResourceImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<VehicleTypeResource> toCacheModel() {
                return _nullVehicleTypeResourceCacheModel;
            }
        };

    private static CacheModel<VehicleTypeResource> _nullVehicleTypeResourceCacheModel =
        new CacheModel<VehicleTypeResource>() {
            @Override
            public VehicleTypeResource toEntityModel() {
                return _nullVehicleTypeResource;
            }
        };

    public VehicleTypeResourcePersistenceImpl() {
        setModelClass(VehicleTypeResource.class);
    }

    /**
     * Returns all the vehicle type resources where vehicleTypeId = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @return the matching vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleTypeResource> findByVehicleTypeId(long vehicleTypeId)
        throws SystemException {
        return findByVehicleTypeId(vehicleTypeId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle type resources where vehicleTypeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleTypeId the vehicle type ID
     * @param start the lower bound of the range of vehicle type resources
     * @param end the upper bound of the range of vehicle type resources (not inclusive)
     * @return the range of matching vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleTypeResource> findByVehicleTypeId(long vehicleTypeId,
        int start, int end) throws SystemException {
        return findByVehicleTypeId(vehicleTypeId, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle type resources where vehicleTypeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleTypeId the vehicle type ID
     * @param start the lower bound of the range of vehicle type resources
     * @param end the upper bound of the range of vehicle type resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleTypeResource> findByVehicleTypeId(long vehicleTypeId,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEID;
            finderArgs = new Object[] { vehicleTypeId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLETYPEID;
            finderArgs = new Object[] {
                    vehicleTypeId,
                    
                    start, end, orderByComparator
                };
        }

        List<VehicleTypeResource> list = (List<VehicleTypeResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleTypeResource vehicleTypeResource : list) {
                if ((vehicleTypeId != vehicleTypeResource.getVehicleTypeId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLETYPERESOURCE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLETYPEID_VEHICLETYPEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleTypeResourceModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleTypeId);

                if (!pagination) {
                    list = (List<VehicleTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleTypeResource>(list);
                } else {
                    list = (List<VehicleTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle type resource
     * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource findByVehicleTypeId_First(long vehicleTypeId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeResourceException, SystemException {
        VehicleTypeResource vehicleTypeResource = fetchByVehicleTypeId_First(vehicleTypeId,
                orderByComparator);

        if (vehicleTypeResource != null) {
            return vehicleTypeResource;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleTypeId=");
        msg.append(vehicleTypeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleTypeResourceException(msg.toString());
    }

    /**
     * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource fetchByVehicleTypeId_First(long vehicleTypeId,
        OrderByComparator orderByComparator) throws SystemException {
        List<VehicleTypeResource> list = findByVehicleTypeId(vehicleTypeId, 0,
                1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle type resource
     * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource findByVehicleTypeId_Last(long vehicleTypeId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeResourceException, SystemException {
        VehicleTypeResource vehicleTypeResource = fetchByVehicleTypeId_Last(vehicleTypeId,
                orderByComparator);

        if (vehicleTypeResource != null) {
            return vehicleTypeResource;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleTypeId=");
        msg.append(vehicleTypeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleTypeResourceException(msg.toString());
    }

    /**
     * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource fetchByVehicleTypeId_Last(long vehicleTypeId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByVehicleTypeId(vehicleTypeId);

        if (count == 0) {
            return null;
        }

        List<VehicleTypeResource> list = findByVehicleTypeId(vehicleTypeId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle type resources before and after the current vehicle type resource in the ordered set where vehicleTypeId = &#63;.
     *
     * @param vehicleTypeResourceId the primary key of the current vehicle type resource
     * @param vehicleTypeId the vehicle type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle type resource
     * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource[] findByVehicleTypeId_PrevAndNext(
        long vehicleTypeResourceId, long vehicleTypeId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeResourceException, SystemException {
        VehicleTypeResource vehicleTypeResource = findByPrimaryKey(vehicleTypeResourceId);

        Session session = null;

        try {
            session = openSession();

            VehicleTypeResource[] array = new VehicleTypeResourceImpl[3];

            array[0] = getByVehicleTypeId_PrevAndNext(session,
                    vehicleTypeResource, vehicleTypeId, orderByComparator, true);

            array[1] = vehicleTypeResource;

            array[2] = getByVehicleTypeId_PrevAndNext(session,
                    vehicleTypeResource, vehicleTypeId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleTypeResource getByVehicleTypeId_PrevAndNext(
        Session session, VehicleTypeResource vehicleTypeResource,
        long vehicleTypeId, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLETYPERESOURCE_WHERE);

        query.append(_FINDER_COLUMN_VEHICLETYPEID_VEHICLETYPEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleTypeResourceModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(vehicleTypeId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleTypeResource);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleTypeResource> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle type resources where vehicleTypeId = &#63; from the database.
     *
     * @param vehicleTypeId the vehicle type ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByVehicleTypeId(long vehicleTypeId)
        throws SystemException {
        for (VehicleTypeResource vehicleTypeResource : findByVehicleTypeId(
                vehicleTypeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(vehicleTypeResource);
        }
    }

    /**
     * Returns the number of vehicle type resources where vehicleTypeId = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @return the number of matching vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByVehicleTypeId(long vehicleTypeId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_VEHICLETYPEID;

        Object[] finderArgs = new Object[] { vehicleTypeId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLETYPERESOURCE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLETYPEID_VEHICLETYPEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleTypeId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicle type resources where vehicleTypeId = &#63; and country = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @return the matching vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleTypeResource> findByVehicleTypeIdAndCountry(
        long vehicleTypeId, String country) throws SystemException {
        return findByVehicleTypeIdAndCountry(vehicleTypeId, country,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle type resources where vehicleTypeId = &#63; and country = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @param start the lower bound of the range of vehicle type resources
     * @param end the upper bound of the range of vehicle type resources (not inclusive)
     * @return the range of matching vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleTypeResource> findByVehicleTypeIdAndCountry(
        long vehicleTypeId, String country, int start, int end)
        throws SystemException {
        return findByVehicleTypeIdAndCountry(vehicleTypeId, country, start,
            end, null);
    }

    /**
     * Returns an ordered range of all the vehicle type resources where vehicleTypeId = &#63; and country = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @param start the lower bound of the range of vehicle type resources
     * @param end the upper bound of the range of vehicle type resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleTypeResource> findByVehicleTypeIdAndCountry(
        long vehicleTypeId, String country, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEIDANDCOUNTRY;
            finderArgs = new Object[] { vehicleTypeId, country };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLETYPEIDANDCOUNTRY;
            finderArgs = new Object[] {
                    vehicleTypeId, country,
                    
                    start, end, orderByComparator
                };
        }

        List<VehicleTypeResource> list = (List<VehicleTypeResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleTypeResource vehicleTypeResource : list) {
                if ((vehicleTypeId != vehicleTypeResource.getVehicleTypeId()) ||
                        !Validator.equals(country,
                            vehicleTypeResource.getCountry())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_VEHICLETYPERESOURCE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDCOUNTRY_VEHICLETYPEID_2);

            boolean bindCountry = false;

            if (country == null) {
                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDCOUNTRY_COUNTRY_1);
            } else if (country.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDCOUNTRY_COUNTRY_3);
            } else {
                bindCountry = true;

                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDCOUNTRY_COUNTRY_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleTypeResourceModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleTypeId);

                if (bindCountry) {
                    qPos.add(country.toLowerCase());
                }

                if (!pagination) {
                    list = (List<VehicleTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleTypeResource>(list);
                } else {
                    list = (List<VehicleTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle type resource
     * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource findByVehicleTypeIdAndCountry_First(
        long vehicleTypeId, String country, OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeResourceException, SystemException {
        VehicleTypeResource vehicleTypeResource = fetchByVehicleTypeIdAndCountry_First(vehicleTypeId,
                country, orderByComparator);

        if (vehicleTypeResource != null) {
            return vehicleTypeResource;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleTypeId=");
        msg.append(vehicleTypeId);

        msg.append(", country=");
        msg.append(country);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleTypeResourceException(msg.toString());
    }

    /**
     * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource fetchByVehicleTypeIdAndCountry_First(
        long vehicleTypeId, String country, OrderByComparator orderByComparator)
        throws SystemException {
        List<VehicleTypeResource> list = findByVehicleTypeIdAndCountry(vehicleTypeId,
                country, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle type resource
     * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource findByVehicleTypeIdAndCountry_Last(
        long vehicleTypeId, String country, OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeResourceException, SystemException {
        VehicleTypeResource vehicleTypeResource = fetchByVehicleTypeIdAndCountry_Last(vehicleTypeId,
                country, orderByComparator);

        if (vehicleTypeResource != null) {
            return vehicleTypeResource;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleTypeId=");
        msg.append(vehicleTypeId);

        msg.append(", country=");
        msg.append(country);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleTypeResourceException(msg.toString());
    }

    /**
     * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource fetchByVehicleTypeIdAndCountry_Last(
        long vehicleTypeId, String country, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByVehicleTypeIdAndCountry(vehicleTypeId, country);

        if (count == 0) {
            return null;
        }

        List<VehicleTypeResource> list = findByVehicleTypeIdAndCountry(vehicleTypeId,
                country, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle type resources before and after the current vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63;.
     *
     * @param vehicleTypeResourceId the primary key of the current vehicle type resource
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle type resource
     * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource[] findByVehicleTypeIdAndCountry_PrevAndNext(
        long vehicleTypeResourceId, long vehicleTypeId, String country,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeResourceException, SystemException {
        VehicleTypeResource vehicleTypeResource = findByPrimaryKey(vehicleTypeResourceId);

        Session session = null;

        try {
            session = openSession();

            VehicleTypeResource[] array = new VehicleTypeResourceImpl[3];

            array[0] = getByVehicleTypeIdAndCountry_PrevAndNext(session,
                    vehicleTypeResource, vehicleTypeId, country,
                    orderByComparator, true);

            array[1] = vehicleTypeResource;

            array[2] = getByVehicleTypeIdAndCountry_PrevAndNext(session,
                    vehicleTypeResource, vehicleTypeId, country,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleTypeResource getByVehicleTypeIdAndCountry_PrevAndNext(
        Session session, VehicleTypeResource vehicleTypeResource,
        long vehicleTypeId, String country,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLETYPERESOURCE_WHERE);

        query.append(_FINDER_COLUMN_VEHICLETYPEIDANDCOUNTRY_VEHICLETYPEID_2);

        boolean bindCountry = false;

        if (country == null) {
            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDCOUNTRY_COUNTRY_1);
        } else if (country.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDCOUNTRY_COUNTRY_3);
        } else {
            bindCountry = true;

            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDCOUNTRY_COUNTRY_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleTypeResourceModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(vehicleTypeId);

        if (bindCountry) {
            qPos.add(country.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleTypeResource);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleTypeResource> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle type resources where vehicleTypeId = &#63; and country = &#63; from the database.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByVehicleTypeIdAndCountry(long vehicleTypeId,
        String country) throws SystemException {
        for (VehicleTypeResource vehicleTypeResource : findByVehicleTypeIdAndCountry(
                vehicleTypeId, country, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
                null)) {
            remove(vehicleTypeResource);
        }
    }

    /**
     * Returns the number of vehicle type resources where vehicleTypeId = &#63; and country = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @return the number of matching vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByVehicleTypeIdAndCountry(long vehicleTypeId, String country)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_VEHICLETYPEIDANDCOUNTRY;

        Object[] finderArgs = new Object[] { vehicleTypeId, country };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_VEHICLETYPERESOURCE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDCOUNTRY_VEHICLETYPEID_2);

            boolean bindCountry = false;

            if (country == null) {
                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDCOUNTRY_COUNTRY_1);
            } else if (country.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDCOUNTRY_COUNTRY_3);
            } else {
                bindCountry = true;

                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDCOUNTRY_COUNTRY_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleTypeId);

                if (bindCountry) {
                    qPos.add(country.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicle type resources where vehicleTypeId = &#63; and language = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param language the language
     * @return the matching vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleTypeResource> findByVehicleTypeIdAndLanguage(
        long vehicleTypeId, String language) throws SystemException {
        return findByVehicleTypeIdAndLanguage(vehicleTypeId, language,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle type resources where vehicleTypeId = &#63; and language = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleTypeId the vehicle type ID
     * @param language the language
     * @param start the lower bound of the range of vehicle type resources
     * @param end the upper bound of the range of vehicle type resources (not inclusive)
     * @return the range of matching vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleTypeResource> findByVehicleTypeIdAndLanguage(
        long vehicleTypeId, String language, int start, int end)
        throws SystemException {
        return findByVehicleTypeIdAndLanguage(vehicleTypeId, language, start,
            end, null);
    }

    /**
     * Returns an ordered range of all the vehicle type resources where vehicleTypeId = &#63; and language = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleTypeId the vehicle type ID
     * @param language the language
     * @param start the lower bound of the range of vehicle type resources
     * @param end the upper bound of the range of vehicle type resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleTypeResource> findByVehicleTypeIdAndLanguage(
        long vehicleTypeId, String language, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEIDANDLANGUAGE;
            finderArgs = new Object[] { vehicleTypeId, language };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLETYPEIDANDLANGUAGE;
            finderArgs = new Object[] {
                    vehicleTypeId, language,
                    
                    start, end, orderByComparator
                };
        }

        List<VehicleTypeResource> list = (List<VehicleTypeResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleTypeResource vehicleTypeResource : list) {
                if ((vehicleTypeId != vehicleTypeResource.getVehicleTypeId()) ||
                        !Validator.equals(language,
                            vehicleTypeResource.getLanguage())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_VEHICLETYPERESOURCE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLANGUAGE_VEHICLETYPEID_2);

            boolean bindLanguage = false;

            if (language == null) {
                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLANGUAGE_LANGUAGE_1);
            } else if (language.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLANGUAGE_LANGUAGE_3);
            } else {
                bindLanguage = true;

                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLANGUAGE_LANGUAGE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleTypeResourceModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleTypeId);

                if (bindLanguage) {
                    qPos.add(language.toLowerCase());
                }

                if (!pagination) {
                    list = (List<VehicleTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleTypeResource>(list);
                } else {
                    list = (List<VehicleTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and language = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle type resource
     * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource findByVehicleTypeIdAndLanguage_First(
        long vehicleTypeId, String language, OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeResourceException, SystemException {
        VehicleTypeResource vehicleTypeResource = fetchByVehicleTypeIdAndLanguage_First(vehicleTypeId,
                language, orderByComparator);

        if (vehicleTypeResource != null) {
            return vehicleTypeResource;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleTypeId=");
        msg.append(vehicleTypeId);

        msg.append(", language=");
        msg.append(language);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleTypeResourceException(msg.toString());
    }

    /**
     * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and language = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource fetchByVehicleTypeIdAndLanguage_First(
        long vehicleTypeId, String language, OrderByComparator orderByComparator)
        throws SystemException {
        List<VehicleTypeResource> list = findByVehicleTypeIdAndLanguage(vehicleTypeId,
                language, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and language = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle type resource
     * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource findByVehicleTypeIdAndLanguage_Last(
        long vehicleTypeId, String language, OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeResourceException, SystemException {
        VehicleTypeResource vehicleTypeResource = fetchByVehicleTypeIdAndLanguage_Last(vehicleTypeId,
                language, orderByComparator);

        if (vehicleTypeResource != null) {
            return vehicleTypeResource;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleTypeId=");
        msg.append(vehicleTypeId);

        msg.append(", language=");
        msg.append(language);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleTypeResourceException(msg.toString());
    }

    /**
     * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and language = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource fetchByVehicleTypeIdAndLanguage_Last(
        long vehicleTypeId, String language, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByVehicleTypeIdAndLanguage(vehicleTypeId, language);

        if (count == 0) {
            return null;
        }

        List<VehicleTypeResource> list = findByVehicleTypeIdAndLanguage(vehicleTypeId,
                language, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle type resources before and after the current vehicle type resource in the ordered set where vehicleTypeId = &#63; and language = &#63;.
     *
     * @param vehicleTypeResourceId the primary key of the current vehicle type resource
     * @param vehicleTypeId the vehicle type ID
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle type resource
     * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource[] findByVehicleTypeIdAndLanguage_PrevAndNext(
        long vehicleTypeResourceId, long vehicleTypeId, String language,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeResourceException, SystemException {
        VehicleTypeResource vehicleTypeResource = findByPrimaryKey(vehicleTypeResourceId);

        Session session = null;

        try {
            session = openSession();

            VehicleTypeResource[] array = new VehicleTypeResourceImpl[3];

            array[0] = getByVehicleTypeIdAndLanguage_PrevAndNext(session,
                    vehicleTypeResource, vehicleTypeId, language,
                    orderByComparator, true);

            array[1] = vehicleTypeResource;

            array[2] = getByVehicleTypeIdAndLanguage_PrevAndNext(session,
                    vehicleTypeResource, vehicleTypeId, language,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleTypeResource getByVehicleTypeIdAndLanguage_PrevAndNext(
        Session session, VehicleTypeResource vehicleTypeResource,
        long vehicleTypeId, String language,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLETYPERESOURCE_WHERE);

        query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLANGUAGE_VEHICLETYPEID_2);

        boolean bindLanguage = false;

        if (language == null) {
            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLANGUAGE_LANGUAGE_1);
        } else if (language.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLANGUAGE_LANGUAGE_3);
        } else {
            bindLanguage = true;

            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLANGUAGE_LANGUAGE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleTypeResourceModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(vehicleTypeId);

        if (bindLanguage) {
            qPos.add(language.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleTypeResource);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleTypeResource> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle type resources where vehicleTypeId = &#63; and language = &#63; from the database.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param language the language
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByVehicleTypeIdAndLanguage(long vehicleTypeId,
        String language) throws SystemException {
        for (VehicleTypeResource vehicleTypeResource : findByVehicleTypeIdAndLanguage(
                vehicleTypeId, language, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
                null)) {
            remove(vehicleTypeResource);
        }
    }

    /**
     * Returns the number of vehicle type resources where vehicleTypeId = &#63; and language = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param language the language
     * @return the number of matching vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByVehicleTypeIdAndLanguage(long vehicleTypeId,
        String language) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_VEHICLETYPEIDANDLANGUAGE;

        Object[] finderArgs = new Object[] { vehicleTypeId, language };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_VEHICLETYPERESOURCE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLANGUAGE_VEHICLETYPEID_2);

            boolean bindLanguage = false;

            if (language == null) {
                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLANGUAGE_LANGUAGE_1);
            } else if (language.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLANGUAGE_LANGUAGE_3);
            } else {
                bindLanguage = true;

                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLANGUAGE_LANGUAGE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleTypeId);

                if (bindLanguage) {
                    qPos.add(language.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicle type resources where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @param language the language
     * @return the matching vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleTypeResource> findByVehicleTypeIdAndLocale(
        long vehicleTypeId, String country, String language)
        throws SystemException {
        return findByVehicleTypeIdAndLocale(vehicleTypeId, country, language,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle type resources where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @param language the language
     * @param start the lower bound of the range of vehicle type resources
     * @param end the upper bound of the range of vehicle type resources (not inclusive)
     * @return the range of matching vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleTypeResource> findByVehicleTypeIdAndLocale(
        long vehicleTypeId, String country, String language, int start, int end)
        throws SystemException {
        return findByVehicleTypeIdAndLocale(vehicleTypeId, country, language,
            start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle type resources where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @param language the language
     * @param start the lower bound of the range of vehicle type resources
     * @param end the upper bound of the range of vehicle type resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleTypeResource> findByVehicleTypeIdAndLocale(
        long vehicleTypeId, String country, String language, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEIDANDLOCALE;
            finderArgs = new Object[] { vehicleTypeId, country, language };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLETYPEIDANDLOCALE;
            finderArgs = new Object[] {
                    vehicleTypeId, country, language,
                    
                    start, end, orderByComparator
                };
        }

        List<VehicleTypeResource> list = (List<VehicleTypeResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleTypeResource vehicleTypeResource : list) {
                if ((vehicleTypeId != vehicleTypeResource.getVehicleTypeId()) ||
                        !Validator.equals(country,
                            vehicleTypeResource.getCountry()) ||
                        !Validator.equals(language,
                            vehicleTypeResource.getLanguage())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(5 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(5);
            }

            query.append(_SQL_SELECT_VEHICLETYPERESOURCE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_VEHICLETYPEID_2);

            boolean bindCountry = false;

            if (country == null) {
                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_COUNTRY_1);
            } else if (country.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_COUNTRY_3);
            } else {
                bindCountry = true;

                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_COUNTRY_2);
            }

            boolean bindLanguage = false;

            if (language == null) {
                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_LANGUAGE_1);
            } else if (language.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_LANGUAGE_3);
            } else {
                bindLanguage = true;

                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_LANGUAGE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleTypeResourceModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleTypeId);

                if (bindCountry) {
                    qPos.add(country.toLowerCase());
                }

                if (bindLanguage) {
                    qPos.add(language.toLowerCase());
                }

                if (!pagination) {
                    list = (List<VehicleTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleTypeResource>(list);
                } else {
                    list = (List<VehicleTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle type resource
     * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource findByVehicleTypeIdAndLocale_First(
        long vehicleTypeId, String country, String language,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeResourceException, SystemException {
        VehicleTypeResource vehicleTypeResource = fetchByVehicleTypeIdAndLocale_First(vehicleTypeId,
                country, language, orderByComparator);

        if (vehicleTypeResource != null) {
            return vehicleTypeResource;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleTypeId=");
        msg.append(vehicleTypeId);

        msg.append(", country=");
        msg.append(country);

        msg.append(", language=");
        msg.append(language);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleTypeResourceException(msg.toString());
    }

    /**
     * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource fetchByVehicleTypeIdAndLocale_First(
        long vehicleTypeId, String country, String language,
        OrderByComparator orderByComparator) throws SystemException {
        List<VehicleTypeResource> list = findByVehicleTypeIdAndLocale(vehicleTypeId,
                country, language, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle type resource
     * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource findByVehicleTypeIdAndLocale_Last(
        long vehicleTypeId, String country, String language,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeResourceException, SystemException {
        VehicleTypeResource vehicleTypeResource = fetchByVehicleTypeIdAndLocale_Last(vehicleTypeId,
                country, language, orderByComparator);

        if (vehicleTypeResource != null) {
            return vehicleTypeResource;
        }

        StringBundler msg = new StringBundler(8);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleTypeId=");
        msg.append(vehicleTypeId);

        msg.append(", country=");
        msg.append(country);

        msg.append(", language=");
        msg.append(language);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleTypeResourceException(msg.toString());
    }

    /**
     * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource fetchByVehicleTypeIdAndLocale_Last(
        long vehicleTypeId, String country, String language,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByVehicleTypeIdAndLocale(vehicleTypeId, country,
                language);

        if (count == 0) {
            return null;
        }

        List<VehicleTypeResource> list = findByVehicleTypeIdAndLocale(vehicleTypeId,
                country, language, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle type resources before and after the current vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * @param vehicleTypeResourceId the primary key of the current vehicle type resource
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @param language the language
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle type resource
     * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource[] findByVehicleTypeIdAndLocale_PrevAndNext(
        long vehicleTypeResourceId, long vehicleTypeId, String country,
        String language, OrderByComparator orderByComparator)
        throws NoSuchVehicleTypeResourceException, SystemException {
        VehicleTypeResource vehicleTypeResource = findByPrimaryKey(vehicleTypeResourceId);

        Session session = null;

        try {
            session = openSession();

            VehicleTypeResource[] array = new VehicleTypeResourceImpl[3];

            array[0] = getByVehicleTypeIdAndLocale_PrevAndNext(session,
                    vehicleTypeResource, vehicleTypeId, country, language,
                    orderByComparator, true);

            array[1] = vehicleTypeResource;

            array[2] = getByVehicleTypeIdAndLocale_PrevAndNext(session,
                    vehicleTypeResource, vehicleTypeId, country, language,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleTypeResource getByVehicleTypeIdAndLocale_PrevAndNext(
        Session session, VehicleTypeResource vehicleTypeResource,
        long vehicleTypeId, String country, String language,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLETYPERESOURCE_WHERE);

        query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_VEHICLETYPEID_2);

        boolean bindCountry = false;

        if (country == null) {
            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_COUNTRY_1);
        } else if (country.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_COUNTRY_3);
        } else {
            bindCountry = true;

            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_COUNTRY_2);
        }

        boolean bindLanguage = false;

        if (language == null) {
            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_LANGUAGE_1);
        } else if (language.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_LANGUAGE_3);
        } else {
            bindLanguage = true;

            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_LANGUAGE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleTypeResourceModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(vehicleTypeId);

        if (bindCountry) {
            qPos.add(country.toLowerCase());
        }

        if (bindLanguage) {
            qPos.add(language.toLowerCase());
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleTypeResource);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleTypeResource> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle type resources where vehicleTypeId = &#63; and country = &#63; and language = &#63; from the database.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @param language the language
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByVehicleTypeIdAndLocale(long vehicleTypeId,
        String country, String language) throws SystemException {
        for (VehicleTypeResource vehicleTypeResource : findByVehicleTypeIdAndLocale(
                vehicleTypeId, country, language, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(vehicleTypeResource);
        }
    }

    /**
     * Returns the number of vehicle type resources where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
     *
     * @param vehicleTypeId the vehicle type ID
     * @param country the country
     * @param language the language
     * @return the number of matching vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByVehicleTypeIdAndLocale(long vehicleTypeId,
        String country, String language) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_VEHICLETYPEIDANDLOCALE;

        Object[] finderArgs = new Object[] { vehicleTypeId, country, language };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_COUNT_VEHICLETYPERESOURCE_WHERE);

            query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_VEHICLETYPEID_2);

            boolean bindCountry = false;

            if (country == null) {
                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_COUNTRY_1);
            } else if (country.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_COUNTRY_3);
            } else {
                bindCountry = true;

                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_COUNTRY_2);
            }

            boolean bindLanguage = false;

            if (language == null) {
                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_LANGUAGE_1);
            } else if (language.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_LANGUAGE_3);
            } else {
                bindLanguage = true;

                query.append(_FINDER_COLUMN_VEHICLETYPEIDANDLOCALE_LANGUAGE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleTypeId);

                if (bindCountry) {
                    qPos.add(country.toLowerCase());
                }

                if (bindLanguage) {
                    qPos.add(language.toLowerCase());
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the vehicle type resource in the entity cache if it is enabled.
     *
     * @param vehicleTypeResource the vehicle type resource
     */
    @Override
    public void cacheResult(VehicleTypeResource vehicleTypeResource) {
        EntityCacheUtil.putResult(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceImpl.class, vehicleTypeResource.getPrimaryKey(),
            vehicleTypeResource);

        vehicleTypeResource.resetOriginalValues();
    }

    /**
     * Caches the vehicle type resources in the entity cache if it is enabled.
     *
     * @param vehicleTypeResources the vehicle type resources
     */
    @Override
    public void cacheResult(List<VehicleTypeResource> vehicleTypeResources) {
        for (VehicleTypeResource vehicleTypeResource : vehicleTypeResources) {
            if (EntityCacheUtil.getResult(
                        VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleTypeResourceImpl.class,
                        vehicleTypeResource.getPrimaryKey()) == null) {
                cacheResult(vehicleTypeResource);
            } else {
                vehicleTypeResource.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all vehicle type resources.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(VehicleTypeResourceImpl.class.getName());
        }

        EntityCacheUtil.clearCache(VehicleTypeResourceImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the vehicle type resource.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(VehicleTypeResource vehicleTypeResource) {
        EntityCacheUtil.removeResult(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceImpl.class, vehicleTypeResource.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<VehicleTypeResource> vehicleTypeResources) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (VehicleTypeResource vehicleTypeResource : vehicleTypeResources) {
            EntityCacheUtil.removeResult(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
                VehicleTypeResourceImpl.class,
                vehicleTypeResource.getPrimaryKey());
        }
    }

    /**
     * Creates a new vehicle type resource with the primary key. Does not add the vehicle type resource to the database.
     *
     * @param vehicleTypeResourceId the primary key for the new vehicle type resource
     * @return the new vehicle type resource
     */
    @Override
    public VehicleTypeResource create(long vehicleTypeResourceId) {
        VehicleTypeResource vehicleTypeResource = new VehicleTypeResourceImpl();

        vehicleTypeResource.setNew(true);
        vehicleTypeResource.setPrimaryKey(vehicleTypeResourceId);

        return vehicleTypeResource;
    }

    /**
     * Removes the vehicle type resource with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param vehicleTypeResourceId the primary key of the vehicle type resource
     * @return the vehicle type resource that was removed
     * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource remove(long vehicleTypeResourceId)
        throws NoSuchVehicleTypeResourceException, SystemException {
        return remove((Serializable) vehicleTypeResourceId);
    }

    /**
     * Removes the vehicle type resource with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the vehicle type resource
     * @return the vehicle type resource that was removed
     * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource remove(Serializable primaryKey)
        throws NoSuchVehicleTypeResourceException, SystemException {
        Session session = null;

        try {
            session = openSession();

            VehicleTypeResource vehicleTypeResource = (VehicleTypeResource) session.get(VehicleTypeResourceImpl.class,
                    primaryKey);

            if (vehicleTypeResource == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchVehicleTypeResourceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(vehicleTypeResource);
        } catch (NoSuchVehicleTypeResourceException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected VehicleTypeResource removeImpl(
        VehicleTypeResource vehicleTypeResource) throws SystemException {
        vehicleTypeResource = toUnwrappedModel(vehicleTypeResource);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(vehicleTypeResource)) {
                vehicleTypeResource = (VehicleTypeResource) session.get(VehicleTypeResourceImpl.class,
                        vehicleTypeResource.getPrimaryKeyObj());
            }

            if (vehicleTypeResource != null) {
                session.delete(vehicleTypeResource);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (vehicleTypeResource != null) {
            clearCache(vehicleTypeResource);
        }

        return vehicleTypeResource;
    }

    @Override
    public VehicleTypeResource updateImpl(
        de.humance.eco.profile.model.VehicleTypeResource vehicleTypeResource)
        throws SystemException {
        vehicleTypeResource = toUnwrappedModel(vehicleTypeResource);

        boolean isNew = vehicleTypeResource.isNew();

        VehicleTypeResourceModelImpl vehicleTypeResourceModelImpl = (VehicleTypeResourceModelImpl) vehicleTypeResource;

        Session session = null;

        try {
            session = openSession();

            if (vehicleTypeResource.isNew()) {
                session.save(vehicleTypeResource);

                vehicleTypeResource.setNew(false);
            } else {
                session.merge(vehicleTypeResource);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !VehicleTypeResourceModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((vehicleTypeResourceModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleTypeResourceModelImpl.getOriginalVehicleTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLETYPEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEID,
                    args);

                args = new Object[] {
                        vehicleTypeResourceModelImpl.getVehicleTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLETYPEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEID,
                    args);
            }

            if ((vehicleTypeResourceModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEIDANDCOUNTRY.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleTypeResourceModelImpl.getOriginalVehicleTypeId(),
                        vehicleTypeResourceModelImpl.getOriginalCountry()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLETYPEIDANDCOUNTRY,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEIDANDCOUNTRY,
                    args);

                args = new Object[] {
                        vehicleTypeResourceModelImpl.getVehicleTypeId(),
                        vehicleTypeResourceModelImpl.getCountry()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLETYPEIDANDCOUNTRY,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEIDANDCOUNTRY,
                    args);
            }

            if ((vehicleTypeResourceModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEIDANDLANGUAGE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleTypeResourceModelImpl.getOriginalVehicleTypeId(),
                        vehicleTypeResourceModelImpl.getOriginalLanguage()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLETYPEIDANDLANGUAGE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEIDANDLANGUAGE,
                    args);

                args = new Object[] {
                        vehicleTypeResourceModelImpl.getVehicleTypeId(),
                        vehicleTypeResourceModelImpl.getLanguage()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLETYPEIDANDLANGUAGE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEIDANDLANGUAGE,
                    args);
            }

            if ((vehicleTypeResourceModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEIDANDLOCALE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleTypeResourceModelImpl.getOriginalVehicleTypeId(),
                        vehicleTypeResourceModelImpl.getOriginalCountry(),
                        vehicleTypeResourceModelImpl.getOriginalLanguage()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLETYPEIDANDLOCALE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEIDANDLOCALE,
                    args);

                args = new Object[] {
                        vehicleTypeResourceModelImpl.getVehicleTypeId(),
                        vehicleTypeResourceModelImpl.getCountry(),
                        vehicleTypeResourceModelImpl.getLanguage()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLETYPEIDANDLOCALE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLETYPEIDANDLOCALE,
                    args);
            }
        }

        EntityCacheUtil.putResult(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
            VehicleTypeResourceImpl.class, vehicleTypeResource.getPrimaryKey(),
            vehicleTypeResource);

        return vehicleTypeResource;
    }

    protected VehicleTypeResource toUnwrappedModel(
        VehicleTypeResource vehicleTypeResource) {
        if (vehicleTypeResource instanceof VehicleTypeResourceImpl) {
            return vehicleTypeResource;
        }

        VehicleTypeResourceImpl vehicleTypeResourceImpl = new VehicleTypeResourceImpl();

        vehicleTypeResourceImpl.setNew(vehicleTypeResource.isNew());
        vehicleTypeResourceImpl.setPrimaryKey(vehicleTypeResource.getPrimaryKey());

        vehicleTypeResourceImpl.setVehicleTypeResourceId(vehicleTypeResource.getVehicleTypeResourceId());
        vehicleTypeResourceImpl.setVehicleTypeId(vehicleTypeResource.getVehicleTypeId());
        vehicleTypeResourceImpl.setCountry(vehicleTypeResource.getCountry());
        vehicleTypeResourceImpl.setLanguage(vehicleTypeResource.getLanguage());
        vehicleTypeResourceImpl.setName(vehicleTypeResource.getName());
        vehicleTypeResourceImpl.setIconId(vehicleTypeResource.getIconId());

        return vehicleTypeResourceImpl;
    }

    /**
     * Returns the vehicle type resource with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle type resource
     * @return the vehicle type resource
     * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource findByPrimaryKey(Serializable primaryKey)
        throws NoSuchVehicleTypeResourceException, SystemException {
        VehicleTypeResource vehicleTypeResource = fetchByPrimaryKey(primaryKey);

        if (vehicleTypeResource == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchVehicleTypeResourceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return vehicleTypeResource;
    }

    /**
     * Returns the vehicle type resource with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleTypeResourceException} if it could not be found.
     *
     * @param vehicleTypeResourceId the primary key of the vehicle type resource
     * @return the vehicle type resource
     * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource findByPrimaryKey(long vehicleTypeResourceId)
        throws NoSuchVehicleTypeResourceException, SystemException {
        return findByPrimaryKey((Serializable) vehicleTypeResourceId);
    }

    /**
     * Returns the vehicle type resource with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle type resource
     * @return the vehicle type resource, or <code>null</code> if a vehicle type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        VehicleTypeResource vehicleTypeResource = (VehicleTypeResource) EntityCacheUtil.getResult(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
                VehicleTypeResourceImpl.class, primaryKey);

        if (vehicleTypeResource == _nullVehicleTypeResource) {
            return null;
        }

        if (vehicleTypeResource == null) {
            Session session = null;

            try {
                session = openSession();

                vehicleTypeResource = (VehicleTypeResource) session.get(VehicleTypeResourceImpl.class,
                        primaryKey);

                if (vehicleTypeResource != null) {
                    cacheResult(vehicleTypeResource);
                } else {
                    EntityCacheUtil.putResult(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleTypeResourceImpl.class, primaryKey,
                        _nullVehicleTypeResource);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(VehicleTypeResourceModelImpl.ENTITY_CACHE_ENABLED,
                    VehicleTypeResourceImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return vehicleTypeResource;
    }

    /**
     * Returns the vehicle type resource with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param vehicleTypeResourceId the primary key of the vehicle type resource
     * @return the vehicle type resource, or <code>null</code> if a vehicle type resource with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleTypeResource fetchByPrimaryKey(long vehicleTypeResourceId)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) vehicleTypeResourceId);
    }

    /**
     * Returns all the vehicle type resources.
     *
     * @return the vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleTypeResource> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle type resources.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicle type resources
     * @param end the upper bound of the range of vehicle type resources (not inclusive)
     * @return the range of vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleTypeResource> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle type resources.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicle type resources
     * @param end the upper bound of the range of vehicle type resources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleTypeResource> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<VehicleTypeResource> list = (List<VehicleTypeResource>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_VEHICLETYPERESOURCE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_VEHICLETYPERESOURCE;

                if (pagination) {
                    sql = sql.concat(VehicleTypeResourceModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<VehicleTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleTypeResource>(list);
                } else {
                    list = (List<VehicleTypeResource>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the vehicle type resources from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (VehicleTypeResource vehicleTypeResource : findAll()) {
            remove(vehicleTypeResource);
        }
    }

    /**
     * Returns the number of vehicle type resources.
     *
     * @return the number of vehicle type resources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_VEHICLETYPERESOURCE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the vehicle type resource persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.de.humance.eco.profile.model.VehicleTypeResource")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<VehicleTypeResource>> listenersList = new ArrayList<ModelListener<VehicleTypeResource>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<VehicleTypeResource>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(VehicleTypeResourceImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
