package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import de.humance.eco.profile.NoSuchVehicleUserException;
import de.humance.eco.profile.model.VehicleUser;
import de.humance.eco.profile.model.impl.VehicleUserImpl;
import de.humance.eco.profile.model.impl.VehicleUserModelImpl;
import de.humance.eco.profile.service.persistence.VehicleUserPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the vehicle user service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehicleUserPersistence
 * @see VehicleUserUtil
 * @generated
 */
public class VehicleUserPersistenceImpl extends BasePersistenceImpl<VehicleUser>
    implements VehicleUserPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link VehicleUserUtil} to access the vehicle user persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = VehicleUserImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VehicleUserModelImpl.ENTITY_CACHE_ENABLED,
            VehicleUserModelImpl.FINDER_CACHE_ENABLED, VehicleUserImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VehicleUserModelImpl.ENTITY_CACHE_ENABLED,
            VehicleUserModelImpl.FINDER_CACHE_ENABLED, VehicleUserImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VehicleUserModelImpl.ENTITY_CACHE_ENABLED,
            VehicleUserModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(VehicleUserModelImpl.ENTITY_CACHE_ENABLED,
            VehicleUserModelImpl.FINDER_CACHE_ENABLED, VehicleUserImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
        new FinderPath(VehicleUserModelImpl.ENTITY_CACHE_ENABLED,
            VehicleUserModelImpl.FINDER_CACHE_ENABLED, VehicleUserImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserId",
            new String[] { Long.class.getName() },
            VehicleUserModelImpl.USERID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(VehicleUserModelImpl.ENTITY_CACHE_ENABLED,
            VehicleUserModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_USERID_USERID_2 = "vehicleUser.userId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLEID =
        new FinderPath(VehicleUserModelImpl.ENTITY_CACHE_ENABLED,
            VehicleUserModelImpl.FINDER_CACHE_ENABLED, VehicleUserImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByVehicleId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEID =
        new FinderPath(VehicleUserModelImpl.ENTITY_CACHE_ENABLED,
            VehicleUserModelImpl.FINDER_CACHE_ENABLED, VehicleUserImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByVehicleId",
            new String[] { Long.class.getName() },
            VehicleUserModelImpl.VEHICLEID_COLUMN_BITMASK |
            VehicleUserModelImpl.USERID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_VEHICLEID = new FinderPath(VehicleUserModelImpl.ENTITY_CACHE_ENABLED,
            VehicleUserModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByVehicleId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_VEHICLEID_VEHICLEID_2 = "vehicleUser.vehicleId = ?";
    private static final String _SQL_SELECT_VEHICLEUSER = "SELECT vehicleUser FROM VehicleUser vehicleUser";
    private static final String _SQL_SELECT_VEHICLEUSER_WHERE = "SELECT vehicleUser FROM VehicleUser vehicleUser WHERE ";
    private static final String _SQL_COUNT_VEHICLEUSER = "SELECT COUNT(vehicleUser) FROM VehicleUser vehicleUser";
    private static final String _SQL_COUNT_VEHICLEUSER_WHERE = "SELECT COUNT(vehicleUser) FROM VehicleUser vehicleUser WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "vehicleUser.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No VehicleUser exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No VehicleUser exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(VehicleUserPersistenceImpl.class);
    private static VehicleUser _nullVehicleUser = new VehicleUserImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<VehicleUser> toCacheModel() {
                return _nullVehicleUserCacheModel;
            }
        };

    private static CacheModel<VehicleUser> _nullVehicleUserCacheModel = new CacheModel<VehicleUser>() {
            @Override
            public VehicleUser toEntityModel() {
                return _nullVehicleUser;
            }
        };

    public VehicleUserPersistenceImpl() {
        setModelClass(VehicleUser.class);
    }

    /**
     * Returns all the vehicle users where userId = &#63;.
     *
     * @param userId the user ID
     * @return the matching vehicle users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleUser> findByUserId(long userId)
        throws SystemException {
        return findByUserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle users where userId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param userId the user ID
     * @param start the lower bound of the range of vehicle users
     * @param end the upper bound of the range of vehicle users (not inclusive)
     * @return the range of matching vehicle users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleUser> findByUserId(long userId, int start, int end)
        throws SystemException {
        return findByUserId(userId, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle users where userId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param userId the user ID
     * @param start the lower bound of the range of vehicle users
     * @param end the upper bound of the range of vehicle users (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleUser> findByUserId(long userId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
            finderArgs = new Object[] { userId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
            finderArgs = new Object[] { userId, start, end, orderByComparator };
        }

        List<VehicleUser> list = (List<VehicleUser>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleUser vehicleUser : list) {
                if ((userId != vehicleUser.getUserId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLEUSER_WHERE);

            query.append(_FINDER_COLUMN_USERID_USERID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleUserModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(userId);

                if (!pagination) {
                    list = (List<VehicleUser>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleUser>(list);
                } else {
                    list = (List<VehicleUser>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle user in the ordered set where userId = &#63;.
     *
     * @param userId the user ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle user
     * @throws de.humance.eco.profile.NoSuchVehicleUserException if a matching vehicle user could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleUser findByUserId_First(long userId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleUserException, SystemException {
        VehicleUser vehicleUser = fetchByUserId_First(userId, orderByComparator);

        if (vehicleUser != null) {
            return vehicleUser;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("userId=");
        msg.append(userId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleUserException(msg.toString());
    }

    /**
     * Returns the first vehicle user in the ordered set where userId = &#63;.
     *
     * @param userId the user ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle user, or <code>null</code> if a matching vehicle user could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleUser fetchByUserId_First(long userId,
        OrderByComparator orderByComparator) throws SystemException {
        List<VehicleUser> list = findByUserId(userId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle user in the ordered set where userId = &#63;.
     *
     * @param userId the user ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle user
     * @throws de.humance.eco.profile.NoSuchVehicleUserException if a matching vehicle user could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleUser findByUserId_Last(long userId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleUserException, SystemException {
        VehicleUser vehicleUser = fetchByUserId_Last(userId, orderByComparator);

        if (vehicleUser != null) {
            return vehicleUser;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("userId=");
        msg.append(userId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleUserException(msg.toString());
    }

    /**
     * Returns the last vehicle user in the ordered set where userId = &#63;.
     *
     * @param userId the user ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle user, or <code>null</code> if a matching vehicle user could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleUser fetchByUserId_Last(long userId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByUserId(userId);

        if (count == 0) {
            return null;
        }

        List<VehicleUser> list = findByUserId(userId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle users before and after the current vehicle user in the ordered set where userId = &#63;.
     *
     * @param vehicleUserId the primary key of the current vehicle user
     * @param userId the user ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle user
     * @throws de.humance.eco.profile.NoSuchVehicleUserException if a vehicle user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleUser[] findByUserId_PrevAndNext(long vehicleUserId,
        long userId, OrderByComparator orderByComparator)
        throws NoSuchVehicleUserException, SystemException {
        VehicleUser vehicleUser = findByPrimaryKey(vehicleUserId);

        Session session = null;

        try {
            session = openSession();

            VehicleUser[] array = new VehicleUserImpl[3];

            array[0] = getByUserId_PrevAndNext(session, vehicleUser, userId,
                    orderByComparator, true);

            array[1] = vehicleUser;

            array[2] = getByUserId_PrevAndNext(session, vehicleUser, userId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleUser getByUserId_PrevAndNext(Session session,
        VehicleUser vehicleUser, long userId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLEUSER_WHERE);

        query.append(_FINDER_COLUMN_USERID_USERID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleUserModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(userId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleUser);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleUser> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle users where userId = &#63; from the database.
     *
     * @param userId the user ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByUserId(long userId) throws SystemException {
        for (VehicleUser vehicleUser : findByUserId(userId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(vehicleUser);
        }
    }

    /**
     * Returns the number of vehicle users where userId = &#63;.
     *
     * @param userId the user ID
     * @return the number of matching vehicle users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByUserId(long userId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

        Object[] finderArgs = new Object[] { userId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLEUSER_WHERE);

            query.append(_FINDER_COLUMN_USERID_USERID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(userId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicle users where vehicleId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @return the matching vehicle users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleUser> findByVehicleId(long vehicleId)
        throws SystemException {
        return findByVehicleId(vehicleId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the vehicle users where vehicleId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleId the vehicle ID
     * @param start the lower bound of the range of vehicle users
     * @param end the upper bound of the range of vehicle users (not inclusive)
     * @return the range of matching vehicle users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleUser> findByVehicleId(long vehicleId, int start, int end)
        throws SystemException {
        return findByVehicleId(vehicleId, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle users where vehicleId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param vehicleId the vehicle ID
     * @param start the lower bound of the range of vehicle users
     * @param end the upper bound of the range of vehicle users (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicle users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleUser> findByVehicleId(long vehicleId, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEID;
            finderArgs = new Object[] { vehicleId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_VEHICLEID;
            finderArgs = new Object[] { vehicleId, start, end, orderByComparator };
        }

        List<VehicleUser> list = (List<VehicleUser>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (VehicleUser vehicleUser : list) {
                if ((vehicleId != vehicleUser.getVehicleId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLEUSER_WHERE);

            query.append(_FINDER_COLUMN_VEHICLEID_VEHICLEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleUserModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleId);

                if (!pagination) {
                    list = (List<VehicleUser>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleUser>(list);
                } else {
                    list = (List<VehicleUser>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle user in the ordered set where vehicleId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle user
     * @throws de.humance.eco.profile.NoSuchVehicleUserException if a matching vehicle user could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleUser findByVehicleId_First(long vehicleId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleUserException, SystemException {
        VehicleUser vehicleUser = fetchByVehicleId_First(vehicleId,
                orderByComparator);

        if (vehicleUser != null) {
            return vehicleUser;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleId=");
        msg.append(vehicleId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleUserException(msg.toString());
    }

    /**
     * Returns the first vehicle user in the ordered set where vehicleId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle user, or <code>null</code> if a matching vehicle user could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleUser fetchByVehicleId_First(long vehicleId,
        OrderByComparator orderByComparator) throws SystemException {
        List<VehicleUser> list = findByVehicleId(vehicleId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle user in the ordered set where vehicleId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle user
     * @throws de.humance.eco.profile.NoSuchVehicleUserException if a matching vehicle user could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleUser findByVehicleId_Last(long vehicleId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleUserException, SystemException {
        VehicleUser vehicleUser = fetchByVehicleId_Last(vehicleId,
                orderByComparator);

        if (vehicleUser != null) {
            return vehicleUser;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("vehicleId=");
        msg.append(vehicleId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleUserException(msg.toString());
    }

    /**
     * Returns the last vehicle user in the ordered set where vehicleId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle user, or <code>null</code> if a matching vehicle user could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleUser fetchByVehicleId_Last(long vehicleId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByVehicleId(vehicleId);

        if (count == 0) {
            return null;
        }

        List<VehicleUser> list = findByVehicleId(vehicleId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicle users before and after the current vehicle user in the ordered set where vehicleId = &#63;.
     *
     * @param vehicleUserId the primary key of the current vehicle user
     * @param vehicleId the vehicle ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle user
     * @throws de.humance.eco.profile.NoSuchVehicleUserException if a vehicle user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleUser[] findByVehicleId_PrevAndNext(long vehicleUserId,
        long vehicleId, OrderByComparator orderByComparator)
        throws NoSuchVehicleUserException, SystemException {
        VehicleUser vehicleUser = findByPrimaryKey(vehicleUserId);

        Session session = null;

        try {
            session = openSession();

            VehicleUser[] array = new VehicleUserImpl[3];

            array[0] = getByVehicleId_PrevAndNext(session, vehicleUser,
                    vehicleId, orderByComparator, true);

            array[1] = vehicleUser;

            array[2] = getByVehicleId_PrevAndNext(session, vehicleUser,
                    vehicleId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected VehicleUser getByVehicleId_PrevAndNext(Session session,
        VehicleUser vehicleUser, long vehicleId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLEUSER_WHERE);

        query.append(_FINDER_COLUMN_VEHICLEID_VEHICLEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleUserModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(vehicleId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicleUser);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<VehicleUser> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicle users where vehicleId = &#63; from the database.
     *
     * @param vehicleId the vehicle ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByVehicleId(long vehicleId) throws SystemException {
        for (VehicleUser vehicleUser : findByVehicleId(vehicleId,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(vehicleUser);
        }
    }

    /**
     * Returns the number of vehicle users where vehicleId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @return the number of matching vehicle users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByVehicleId(long vehicleId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_VEHICLEID;

        Object[] finderArgs = new Object[] { vehicleId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLEUSER_WHERE);

            query.append(_FINDER_COLUMN_VEHICLEID_VEHICLEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the vehicle user in the entity cache if it is enabled.
     *
     * @param vehicleUser the vehicle user
     */
    @Override
    public void cacheResult(VehicleUser vehicleUser) {
        EntityCacheUtil.putResult(VehicleUserModelImpl.ENTITY_CACHE_ENABLED,
            VehicleUserImpl.class, vehicleUser.getPrimaryKey(), vehicleUser);

        vehicleUser.resetOriginalValues();
    }

    /**
     * Caches the vehicle users in the entity cache if it is enabled.
     *
     * @param vehicleUsers the vehicle users
     */
    @Override
    public void cacheResult(List<VehicleUser> vehicleUsers) {
        for (VehicleUser vehicleUser : vehicleUsers) {
            if (EntityCacheUtil.getResult(
                        VehicleUserModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleUserImpl.class, vehicleUser.getPrimaryKey()) == null) {
                cacheResult(vehicleUser);
            } else {
                vehicleUser.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all vehicle users.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(VehicleUserImpl.class.getName());
        }

        EntityCacheUtil.clearCache(VehicleUserImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the vehicle user.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(VehicleUser vehicleUser) {
        EntityCacheUtil.removeResult(VehicleUserModelImpl.ENTITY_CACHE_ENABLED,
            VehicleUserImpl.class, vehicleUser.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<VehicleUser> vehicleUsers) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (VehicleUser vehicleUser : vehicleUsers) {
            EntityCacheUtil.removeResult(VehicleUserModelImpl.ENTITY_CACHE_ENABLED,
                VehicleUserImpl.class, vehicleUser.getPrimaryKey());
        }
    }

    /**
     * Creates a new vehicle user with the primary key. Does not add the vehicle user to the database.
     *
     * @param vehicleUserId the primary key for the new vehicle user
     * @return the new vehicle user
     */
    @Override
    public VehicleUser create(long vehicleUserId) {
        VehicleUser vehicleUser = new VehicleUserImpl();

        vehicleUser.setNew(true);
        vehicleUser.setPrimaryKey(vehicleUserId);

        return vehicleUser;
    }

    /**
     * Removes the vehicle user with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param vehicleUserId the primary key of the vehicle user
     * @return the vehicle user that was removed
     * @throws de.humance.eco.profile.NoSuchVehicleUserException if a vehicle user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleUser remove(long vehicleUserId)
        throws NoSuchVehicleUserException, SystemException {
        return remove((Serializable) vehicleUserId);
    }

    /**
     * Removes the vehicle user with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the vehicle user
     * @return the vehicle user that was removed
     * @throws de.humance.eco.profile.NoSuchVehicleUserException if a vehicle user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleUser remove(Serializable primaryKey)
        throws NoSuchVehicleUserException, SystemException {
        Session session = null;

        try {
            session = openSession();

            VehicleUser vehicleUser = (VehicleUser) session.get(VehicleUserImpl.class,
                    primaryKey);

            if (vehicleUser == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchVehicleUserException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(vehicleUser);
        } catch (NoSuchVehicleUserException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected VehicleUser removeImpl(VehicleUser vehicleUser)
        throws SystemException {
        vehicleUser = toUnwrappedModel(vehicleUser);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(vehicleUser)) {
                vehicleUser = (VehicleUser) session.get(VehicleUserImpl.class,
                        vehicleUser.getPrimaryKeyObj());
            }

            if (vehicleUser != null) {
                session.delete(vehicleUser);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (vehicleUser != null) {
            clearCache(vehicleUser);
        }

        return vehicleUser;
    }

    @Override
    public VehicleUser updateImpl(
        de.humance.eco.profile.model.VehicleUser vehicleUser)
        throws SystemException {
        vehicleUser = toUnwrappedModel(vehicleUser);

        boolean isNew = vehicleUser.isNew();

        VehicleUserModelImpl vehicleUserModelImpl = (VehicleUserModelImpl) vehicleUser;

        Session session = null;

        try {
            session = openSession();

            if (vehicleUser.isNew()) {
                session.save(vehicleUser);

                vehicleUser.setNew(false);
            } else {
                session.merge(vehicleUser);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !VehicleUserModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((vehicleUserModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleUserModelImpl.getOriginalUserId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
                    args);

                args = new Object[] { vehicleUserModelImpl.getUserId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
                    args);
            }

            if ((vehicleUserModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleUserModelImpl.getOriginalVehicleId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEID,
                    args);

                args = new Object[] { vehicleUserModelImpl.getVehicleId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLEID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VEHICLEID,
                    args);
            }
        }

        EntityCacheUtil.putResult(VehicleUserModelImpl.ENTITY_CACHE_ENABLED,
            VehicleUserImpl.class, vehicleUser.getPrimaryKey(), vehicleUser);

        return vehicleUser;
    }

    protected VehicleUser toUnwrappedModel(VehicleUser vehicleUser) {
        if (vehicleUser instanceof VehicleUserImpl) {
            return vehicleUser;
        }

        VehicleUserImpl vehicleUserImpl = new VehicleUserImpl();

        vehicleUserImpl.setNew(vehicleUser.isNew());
        vehicleUserImpl.setPrimaryKey(vehicleUser.getPrimaryKey());

        vehicleUserImpl.setVehicleUserId(vehicleUser.getVehicleUserId());
        vehicleUserImpl.setUserId(vehicleUser.getUserId());
        vehicleUserImpl.setVehicleId(vehicleUser.getVehicleId());

        return vehicleUserImpl;
    }

    /**
     * Returns the vehicle user with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle user
     * @return the vehicle user
     * @throws de.humance.eco.profile.NoSuchVehicleUserException if a vehicle user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleUser findByPrimaryKey(Serializable primaryKey)
        throws NoSuchVehicleUserException, SystemException {
        VehicleUser vehicleUser = fetchByPrimaryKey(primaryKey);

        if (vehicleUser == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchVehicleUserException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return vehicleUser;
    }

    /**
     * Returns the vehicle user with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleUserException} if it could not be found.
     *
     * @param vehicleUserId the primary key of the vehicle user
     * @return the vehicle user
     * @throws de.humance.eco.profile.NoSuchVehicleUserException if a vehicle user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleUser findByPrimaryKey(long vehicleUserId)
        throws NoSuchVehicleUserException, SystemException {
        return findByPrimaryKey((Serializable) vehicleUserId);
    }

    /**
     * Returns the vehicle user with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle user
     * @return the vehicle user, or <code>null</code> if a vehicle user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleUser fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        VehicleUser vehicleUser = (VehicleUser) EntityCacheUtil.getResult(VehicleUserModelImpl.ENTITY_CACHE_ENABLED,
                VehicleUserImpl.class, primaryKey);

        if (vehicleUser == _nullVehicleUser) {
            return null;
        }

        if (vehicleUser == null) {
            Session session = null;

            try {
                session = openSession();

                vehicleUser = (VehicleUser) session.get(VehicleUserImpl.class,
                        primaryKey);

                if (vehicleUser != null) {
                    cacheResult(vehicleUser);
                } else {
                    EntityCacheUtil.putResult(VehicleUserModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleUserImpl.class, primaryKey, _nullVehicleUser);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(VehicleUserModelImpl.ENTITY_CACHE_ENABLED,
                    VehicleUserImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return vehicleUser;
    }

    /**
     * Returns the vehicle user with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param vehicleUserId the primary key of the vehicle user
     * @return the vehicle user, or <code>null</code> if a vehicle user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public VehicleUser fetchByPrimaryKey(long vehicleUserId)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) vehicleUserId);
    }

    /**
     * Returns all the vehicle users.
     *
     * @return the vehicle users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleUser> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicle users.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicle users
     * @param end the upper bound of the range of vehicle users (not inclusive)
     * @return the range of vehicle users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleUser> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicle users.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicle users
     * @param end the upper bound of the range of vehicle users (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of vehicle users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<VehicleUser> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<VehicleUser> list = (List<VehicleUser>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_VEHICLEUSER);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_VEHICLEUSER;

                if (pagination) {
                    sql = sql.concat(VehicleUserModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<VehicleUser>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<VehicleUser>(list);
                } else {
                    list = (List<VehicleUser>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the vehicle users from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (VehicleUser vehicleUser : findAll()) {
            remove(vehicleUser);
        }
    }

    /**
     * Returns the number of vehicle users.
     *
     * @return the number of vehicle users
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_VEHICLEUSER);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the vehicle user persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.de.humance.eco.profile.model.VehicleUser")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<VehicleUser>> listenersList = new ArrayList<ModelListener<VehicleUser>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<VehicleUser>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(VehicleUserImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
