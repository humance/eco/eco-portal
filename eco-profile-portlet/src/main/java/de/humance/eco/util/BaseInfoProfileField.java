package de.humance.eco.util;

public enum BaseInfoProfileField {
	Street1, Street2, Street3, City, Zip, Country, PersonalPhoneNumber, MobilePhoneNumber, BusinessPhoneNumber, SkypeId, PrivateEmail, BusinessEmail, Height, Weight, EyeColor, HairColor, OverallSize, JacketSize, ShirtSize, PantsSize, ShoesSize, AccountHolderName, BankName, BicSwift, Iban, Organization, CurrentOrganization
}
