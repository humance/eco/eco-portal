package de.humance.eco.util;

public enum GeneralProfileField {
	FirstName, MiddleName, LastName, ScreenName, Email, DateOfBirth, Gender, Language, JobTitle
}
