package de.humance.eco.view;

import com.github.peholmst.i18n4vaadin.annotations.Message;
import com.github.peholmst.i18n4vaadin.annotations.Messages;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Country;
import com.liferay.portal.model.Organization;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

import de.humance.eco.model.NotificationIpcEventMessage;
import de.humance.eco.portlet.BaseInfoProfileApplication;
import de.humance.eco.presenter.BaseInfoProfileViewPresenter;
import de.humance.eco.util.BaseInfoProfileField;
import de.humance.eco.util.CommonConstants;
import de.humance.vaadin.i18n.LocaleChangedIpcEventListener;
import de.humance.vaadin.ui.content.Subheader;
import de.humance.vaadin.ui.layout.ResponsiveBlock;
import de.humance.vaadin.ui.layout.ResponsiveRow;
import de.humance.vaadin.view.BootstrapView;

public class BaseInfoProfileView extends
		BootstrapView<BaseInfoProfileViewPresenter, BaseInfoProfileApplication> {
	private static final long serialVersionUID = -5391786809809448425L;
	private Log log = LogFactoryUtil.getLog(getClass());
	private Bundle messageBundle = new Bundle();
	private boolean isPopulating = true;
	// Column 1
	private Subheader addressSubHeader = null;
	private TextField street1TextField = null;
	private TextField street2TextField = null;
	private TextField cityTextField = null;
	private TextField zipTextField = null;
	private ComboBox countryComboBox = null;
	private CountryValueChangedListener countryComboBoxListener = new CountryValueChangedListener();
	// Column 2
	private Subheader contactSubHeader = null;
	private TextField personalPhoneNumberTextField = null;
	private TextField mobilePhoneNumberTextField = null;
	private TextField businessPhoneNumberTextField = null;
	// Column 3
	private Subheader orgSubHeader = null;
	private ComboBox organizationComboBox = null;
	private OrganizationValueChangedListener organizationComboBoxListener = new OrganizationValueChangedListener();
	private Label orgAddressField = null;
	private Label orgWebsiteField = null;
	private Label orgPhoneField = null;
	private Label orgFaxField = null;

	@SuppressWarnings("serial")
	private class FieldTextChangedListener implements ValueChangeListener {
		private Component component = null;

		public FieldTextChangedListener(Component component) {
			this.component = component;
		}

		public void valueChange(ValueChangeEvent event) {
			if (isPopulating) {
				return;
			}

			String value = (String) event.getProperty().getValue();
			if (component == street1TextField) {
				viewPresenter
						.commitChanges(BaseInfoProfileField.Street1, value);
			} else if (component == street2TextField) {
				viewPresenter
						.commitChanges(BaseInfoProfileField.Street2, value);
			} else if (component == cityTextField) {
				viewPresenter.commitChanges(BaseInfoProfileField.City, value);
			} else if (component == zipTextField) {
				viewPresenter.commitChanges(BaseInfoProfileField.Zip, value);
			} else if (component == personalPhoneNumberTextField) {
				viewPresenter.commitChanges(
						BaseInfoProfileField.PersonalPhoneNumber, value);
			} else if (component == mobilePhoneNumberTextField) {
				viewPresenter.commitChanges(
						BaseInfoProfileField.MobilePhoneNumber, value);
			} else if (component == businessPhoneNumberTextField) {
				viewPresenter.commitChanges(
						BaseInfoProfileField.BusinessPhoneNumber, value);
			} else {
				log.warn("Unknown component. Text won't be saved");
			}
		}
	}

	@SuppressWarnings("serial")
	private class CountryValueChangedListener implements
			Property.ValueChangeListener {

		public void valueChange(ValueChangeEvent event) {
			if (isPopulating) {
				return;
			}

			viewPresenter.commitChanges(BaseInfoProfileField.Country, event
					.getProperty().getValue());
		}
	}

	@SuppressWarnings("serial")
	private class OrganizationValueChangedListener implements
			Property.ValueChangeListener {

		public void valueChange(ValueChangeEvent event) {
			if (isPopulating) {
				return;
			}

			viewPresenter.commitChanges(BaseInfoProfileField.Organization,
					event.getProperty().getValue());
			long orgId = 0;
			if (event.getProperty().getValue() != null) {
				orgId = (Long) event.getProperty().getValue();
			}
			updateOrganizationInfo(orgId);

			NotificationIpcEventMessage ipcNotification = new NotificationIpcEventMessage(
					CommonConstants.IPC_EVENT_NEW_CURRENT_ORG);
			application.getIpc().sendEvent(
					CommonConstants.IPC_EVENT_NEW_CURRENT_ORG,
					ipcNotification.toString());
		}
	}

	@Override
	protected BaseInfoProfileViewPresenter initViewPresenter(
			BaseInfoProfileApplication application) {
		return new BaseInfoProfileViewPresenter(this, application);
	}

	@Override
	protected boolean isFluid() {
		return true;
	}

	@Override
	protected void initComponent() {
		// Initialize the IPC
		application.getIpc().addLiferayIPCEventListener(
				CommonConstants.IPC_EVENT_LOCALE_CHANGED,
				new LocaleChangedIpcEventListener(application));
	}

	@Override
	public void initLayout() {
		// Main row
		ResponsiveRow mainRow = createRow();
		rootContainer.addComponent(mainRow);

		// Col 1
		ResponsiveBlock col1 = createBlock(4);
		mainRow.addComponent(col1);

		// Address header
		addressSubHeader = new Subheader();
		addComponentInRow(col1, addressSubHeader, 12);

		// Address row 1
		ResponsiveRow addressRow1 = createRow();
		col1.addComponent(addressRow1);

		// Street 1
		street1TextField = new TextField();
		street1TextField.addValueChangeListener(new FieldTextChangedListener(
				street1TextField));
		addressRow1.addComponent(createBlock(8, 0, street1TextField));

		// Street 2
		street2TextField = new TextField();
		street2TextField.addValueChangeListener(new FieldTextChangedListener(
				street2TextField));
		addressRow1.addComponent(createBlock(4, 0, street2TextField));

		// Address row 2
		ResponsiveRow addressRow2 = createRow();
		col1.addComponent(addressRow2);

		// Zip
		zipTextField = new TextField();
		zipTextField.addValueChangeListener(new FieldTextChangedListener(
				zipTextField));
		addressRow2.addComponent(createBlock(4, 0, zipTextField));

		// City
		cityTextField = new TextField();
		cityTextField.addValueChangeListener(new FieldTextChangedListener(
				cityTextField));
		addressRow2.addComponent(createBlock(8, 0, cityTextField));

		// Country
		countryComboBox = new ComboBox();
		countryComboBox.setNullSelectionAllowed(false);
		for (Country country : viewPresenter.getAvailableCountries()) {
			countryComboBox.addItem(country.getCountryId());
		}
		countryComboBox.addValueChangeListener(countryComboBoxListener);
		addComponentInRow(col1, countryComboBox, 12);

		// Col 2
		ResponsiveBlock col2 = createBlock(4);
		mainRow.addComponent(col2);

		// Contact header
		contactSubHeader = new Subheader();
		addComponentInRow(col2, contactSubHeader, 12);

		// Personal Phone number
		personalPhoneNumberTextField = new TextField();
		personalPhoneNumberTextField
				.addValueChangeListener(new FieldTextChangedListener(
						personalPhoneNumberTextField));
		addComponentInRow(col2, personalPhoneNumberTextField, 12);

		// Mobile Phone number
		mobilePhoneNumberTextField = new TextField();
		mobilePhoneNumberTextField
				.addValueChangeListener(new FieldTextChangedListener(
						mobilePhoneNumberTextField));
		addComponentInRow(col2, mobilePhoneNumberTextField, 12);

		// Business Phone number
		businessPhoneNumberTextField = new TextField();
		businessPhoneNumberTextField
				.addValueChangeListener(new FieldTextChangedListener(
						businessPhoneNumberTextField));
		addComponentInRow(col2, businessPhoneNumberTextField, 12);

		// Col 3
		ResponsiveBlock col3 = createBlock(4);
		mainRow.addComponent(col3);

		// Organization header
		orgSubHeader = new Subheader();
		addComponentInRow(col3, orgSubHeader, 12);

		// Organization
		organizationComboBox = new ComboBox();
		// organizationComboBox.setNullSelectionAllowed(false);
		organizationComboBox
				.addValueChangeListener(organizationComboBoxListener);
		addComponentInRow(col3, organizationComboBox, 12);

		// Organization Address
		orgAddressField = new Label();
		orgAddressField.setContentMode(ContentMode.HTML);
		addComponentInRow(col3, orgAddressField, 12);

		// Organization Website
		orgWebsiteField = new Label();
		addComponentInRow(col3, orgWebsiteField, 12);

		// Phone row
		ResponsiveRow orgPhoneRow = createRow();
		col3.addComponent(orgPhoneRow);

		// Organization Phone
		orgPhoneField = new Label();
		orgPhoneRow.addComponent(createBlock(6, 0, orgPhoneField));

		// Organization Fax
		orgFaxField = new Label();
		orgPhoneRow.addComponent(createBlock(6, 0, orgFaxField));

		// Populate the view
		populateView();
	}

	@Messages({ @Message(key = "address", value = "Address"),
			@Message(key = "street", value = "Street"),
			@Message(key = "streetNumber", value = "Nr."),
			@Message(key = "zip", value = "Zip"),
			@Message(key = "city", value = "City"),
			@Message(key = "country", value = "Country"),
			@Message(key = "contact", value = "Contact"),
			@Message(key = "phone", value = "Phone"),
			@Message(key = "mobile", value = "Mobile"),
			@Message(key = "work", value = "Work"),
			@Message(key = "organization", value = "Organization"),
			@Message(key = "website", value = "Website"),
			@Message(key = "fax", value = "Fax") })
	@Override
	public void refreshCaption() {
		addressSubHeader.setValue(messageBundle.address());
		street1TextField.setCaption(messageBundle.street());
		street2TextField.setCaption(messageBundle.streetNumber());
		zipTextField.setCaption(messageBundle.zip());
		cityTextField.setCaption(messageBundle.city());
		countryComboBox.setCaption(messageBundle.country());
		for (Country country : viewPresenter.getAvailableCountries()) {
			countryComboBox.setItemCaption(country.getCountryId(),
					country.getName(application.getUserLocale()));
		}
		contactSubHeader.setValue(messageBundle.contact());
		personalPhoneNumberTextField.setCaption(messageBundle.phone());
		mobilePhoneNumberTextField.setCaption(messageBundle.mobile());
		businessPhoneNumberTextField.setCaption(messageBundle.work());
		orgSubHeader.setValue(messageBundle.organization());
		organizationComboBox.setCaption(messageBundle.name());
		orgAddressField.setCaption(messageBundle.address());
		orgWebsiteField.setCaption(messageBundle.website());
		orgPhoneField.setCaption(messageBundle.phone());
		orgFaxField.setCaption(messageBundle.fax());
	}

	public void refreshView() {
		populateView();
	}

	protected void populateView() {
		isPopulating = true;

		street1TextField.setValue(viewPresenter.getStreet1());
		street2TextField.setValue(viewPresenter.getStreet2());
		cityTextField.setValue(viewPresenter.getCity());
		zipTextField.setValue(viewPresenter.getZip());
		countryComboBox.setValue(viewPresenter.getAddressCountryId());
		personalPhoneNumberTextField.setValue(viewPresenter
				.getPersonalPhoneNumber());
		mobilePhoneNumberTextField.setValue(viewPresenter
				.getMobilePhoneNumber());
		businessPhoneNumberTextField.setValue(viewPresenter
				.getBusinessPhoneNumber());

		organizationComboBox.removeAllItems();
		for (Organization organization : viewPresenter
				.getAvailableOrganizations()) {
			organizationComboBox.addItem(organization.getOrganizationId());
			organizationComboBox.setItemCaption(
					organization.getOrganizationId(), organization.getName());
		}
		organizationComboBox.setValue(viewPresenter.getCurrentOrganizationId());

		updateOrganizationInfo(viewPresenter.getCurrentOrganizationId());

		isPopulating = false;
	}

	private void updateOrganizationInfo(long orgId) {
		orgAddressField.setValue(viewPresenter.getOrgAddress(orgId));
		orgWebsiteField.setValue(viewPresenter.getOrgWebsite(orgId));
		orgPhoneField.setValue(viewPresenter.getOrgPhone(orgId));
		orgFaxField.setValue(viewPresenter.getOrgFax(orgId));
	}
}
