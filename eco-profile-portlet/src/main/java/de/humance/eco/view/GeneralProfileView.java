package de.humance.eco.view;

import java.util.Locale;

import com.github.peholmst.i18n4vaadin.annotations.Message;
import com.github.peholmst.i18n4vaadin.annotations.Messages;
import com.liferay.portal.DuplicateUserScreenNameException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.BaseServiceImpl;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.vaadin.addon.ipcforliferay.event.LiferayIPCEvent;
import com.vaadin.addon.ipcforliferay.event.LiferayIPCEventListener;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.Validator;
import com.vaadin.event.LayoutEvents;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

import de.humance.eco.portlet.GeneralProfileApplication;
import de.humance.eco.presenter.GeneralProfileViewPresenter;
import de.humance.eco.util.CommonConstants;
import de.humance.eco.util.GeneralProfileField;
import de.humance.vaadin.ui.content.Subheader;
import de.humance.vaadin.ui.content.media.ImageUploader;
import de.humance.vaadin.ui.content.media.ScaledImage;
import de.humance.vaadin.ui.layout.ResponsiveBlock;
import de.humance.vaadin.ui.layout.ResponsiveRow;
import de.humance.vaadin.view.BootstrapView;

public class GeneralProfileView extends
		BootstrapView<GeneralProfileViewPresenter, GeneralProfileApplication> {
	private static final long serialVersionUID = -5391786809809448425L;
	private Log log = LogFactoryUtil.getLog(getClass());
	private boolean isPopulating = true;
	private Bundle messageBundle = new Bundle();
	// Column 1
	private ImageUploader portraitUploader = null;
	private ScaledImage portraitEmbedded = null;
	// Column 2
	private TextField firstNameTextField = null;
	private TextField middleNameTextField = null;
	private TextField lastNameTextField = null;
	private TextField screenNameTextField = null;
	private TextField emailTextField = null;
	private TextField jobTitleField = null;
	private DateField dateOfBirthDateField = null;
	private ComboBox genderComboBox = null;
	private ComboBox languageComboBox = null;
	private DateOfBirthValueChangedListener dateOfBirthListener = new DateOfBirthValueChangedListener();
	private GenderValueChangedListener genderListener = new GenderValueChangedListener();
	private LanguageValueChangedListener languageListener = new LanguageValueChangedListener();
	// Column 3
	private Subheader orgHeader = null;
	private ScaledImage orgLogo = null;
	private Label orgNameField = null;

	@SuppressWarnings("serial")
	private class NewCurrentOrgIpcEventListener implements
			LiferayIPCEventListener {

		public void eventReceived(LiferayIPCEvent event) {
			updateCurrentOrg();
		}
	}

	@SuppressWarnings("serial")
	public class PortraitClickListener implements
			LayoutEvents.LayoutClickListener {

		public void layoutClick(LayoutClickEvent event) {
			portraitUploader.close();
			application.addWindow(portraitUploader);
		}
	}

	@SuppressWarnings("serial")
	private class PortraitCommitBtnClickListener implements
			Button.ClickListener {

		@Messages({
				@Message(key = "errorOccured", value = "An error occured :("),
				@Message(key = "portraitUploadFailed", value = "Your portrait couldn't be updated.\nPlease try again.") })
		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
			if (!portraitUploader.isSucceeded()) {
				application.showWarning(messageBundle.errorOccured(),
						messageBundle.portraitUploadFailed());
				return;
			}

			viewPresenter.commitPortrait(portraitUploader.getSource());
		}
	}

	@SuppressWarnings("unused")
	private class ScreenNameValidator implements Validator {
		private static final long serialVersionUID = -5444912511136038790L;
		private static final String CYRUS = "cyrus";
		private static final String POSTFIX = "postfix";

		@Override
		public void validate(Object value) throws InvalidValueException {
			if (com.liferay.portal.kernel.util.Validator.isNull((String) value)) {
				throw new InvalidValueException(
						"The screen name cannot be empty.");
			}

			String screenName = (String) value;
			if (com.liferay.portal.kernel.util.Validator
					.isEmailAddress(screenName)
					|| StringUtil.equalsIgnoreCase(screenName, CYRUS)
					|| StringUtil.equalsIgnoreCase(screenName, POSTFIX)
					|| (screenName.indexOf(CharPool.SLASH) != -1)
					|| (screenName.indexOf(CharPool.UNDERLINE) != -1)) {
				throw new InvalidValueException(
						"The screen name cannot be an email address or starts with '/' or '_'.");
			}

			if (com.liferay.portal.kernel.util.Validator.isNumber(screenName)) {
				throw new InvalidValueException(
						"The screen name cannot be a number.");
			}

			for (char c : screenName.toCharArray()) {
				if (!com.liferay.portal.kernel.util.Validator.isChar(c)
						&& !com.liferay.portal.kernel.util.Validator.isDigit(c)
						&& (c != CharPool.DASH) && (c != CharPool.PERIOD)
						&& (c != CharPool.UNDERLINE)) {
					throw new InvalidValueException(
							"The screen name can only contain the following characters: a-z, A-Z, 0-9, '-', '.' or '_'.");
				}
			}

			String[] anonymousNames = BaseServiceImpl.ANONYMOUS_NAMES;
			for (String anonymousName : anonymousNames) {
				if (StringUtil.equalsIgnoreCase(screenName, anonymousName)) {
					throw new InvalidValueException(
							"The screen name cannot be an anonymous name");
				}
			}

			try {
				User user = UserLocalServiceUtil.fetchUserByScreenName(
						application.getCompany().getCompanyId(), screenName);
				if ((user != null)
						&& (user.getUserId() != application.getUser()
								.getUserId())) {
					throw new DuplicateUserScreenNameException();
				}
			} catch (Exception e) {
				throw new InvalidValueException(
						"The screen name is already taken.");
			}
		}
	}

	@SuppressWarnings("serial")
	private class FieldTextChangedListener implements ValueChangeListener {
		private Component component = null;

		public FieldTextChangedListener(Component component) {
			this.component = component;
		}

		public void valueChange(ValueChangeEvent event) {
			if (isPopulating) {
				return;
			}

			String value = (String) event.getProperty().getValue();
			if (component == firstNameTextField) {
				viewPresenter.commitChanges(GeneralProfileField.FirstName,
						value);
			} else if (component == middleNameTextField) {
				viewPresenter.commitChanges(GeneralProfileField.MiddleName,
						value);
			} else if (component == lastNameTextField) {
				viewPresenter
						.commitChanges(GeneralProfileField.LastName, value);
			} else if (component == screenNameTextField) {
				viewPresenter.commitChanges(GeneralProfileField.ScreenName,
						value);
			} else if (component == emailTextField) {
				viewPresenter.commitChanges(GeneralProfileField.Email, value);
			} else if (component == jobTitleField) {
				viewPresenter
						.commitChanges(GeneralProfileField.JobTitle, value);
			} else {
				log.warn("Unknown component. Text won't be saved");
			}
		}
	}

	@SuppressWarnings("serial")
	private class DateOfBirthValueChangedListener implements
			Property.ValueChangeListener {

		public void valueChange(ValueChangeEvent event) {
			if (isPopulating) {
				return;
			}

			viewPresenter.commitChanges(GeneralProfileField.DateOfBirth, event
					.getProperty().getValue());
		}
	}

	@SuppressWarnings("serial")
	private class GenderValueChangedListener implements
			Property.ValueChangeListener {

		public void valueChange(ValueChangeEvent event) {
			if (isPopulating) {
				return;
			}

			viewPresenter.commitChanges(GeneralProfileField.Gender,
					viewPresenter.getGenderBoolean((String) event.getProperty()
							.getValue()));
		}
	}

	@SuppressWarnings("serial")
	private class LanguageValueChangedListener implements
			Property.ValueChangeListener {

		public void valueChange(ValueChangeEvent event) {
			if (isPopulating) {
				return;
			}

			Locale newValue = (Locale) event.getProperty().getValue();
			viewPresenter.commitChanges(GeneralProfileField.Language, newValue);

			// Propagate the change to other portlets on the same page
			application.getIpc().sendEvent(
					CommonConstants.IPC_EVENT_LOCALE_CHANGED, null);
		}
	}

	@Override
	protected GeneralProfileViewPresenter initViewPresenter(
			GeneralProfileApplication application) {
		return new GeneralProfileViewPresenter(this, application);
	}

	@Override
	protected boolean isFluid() {
		return true;
	}

	@Override
	protected void initComponent() {
		// Initialize the IPC
		application.getIpc().addLiferayIPCEventListener(
				CommonConstants.IPC_EVENT_NEW_CURRENT_ORG,
				new NewCurrentOrgIpcEventListener());
	}

	@Override
	public void initLayout() {
		// Main row
		ResponsiveRow mainRow = createRow();
		rootContainer.addComponent(mainRow);

		// Column 1 : Portrait only
		portraitEmbedded = new ScaledImage(true);
		portraitEmbedded.setHeight(300, Unit.PIXELS);
		portraitEmbedded.addLayoutClickListener(new PortraitClickListener());
		mainRow.addComponent(createBlock(3, 0, portraitEmbedded));

		// Portrait Uploader
		portraitUploader = new ImageUploader();
		portraitUploader
				.addCommitBtnClickListener(new PortraitCommitBtnClickListener());

		// Column 2
		ResponsiveBlock column2 = createBlock(6);
		mainRow.addComponent(column2);

		// First name
		firstNameTextField = new TextField();
		firstNameTextField.addValueChangeListener(new FieldTextChangedListener(
				firstNameTextField));

		// Middle name
		middleNameTextField = new TextField();
		middleNameTextField
				.addValueChangeListener(new FieldTextChangedListener(
						middleNameTextField));

		// Add to row
		addComponentToRow(column2, firstNameTextField, middleNameTextField);

		// Last name
		lastNameTextField = new TextField();
		lastNameTextField.addValueChangeListener(new FieldTextChangedListener(
				lastNameTextField));

		// Add to row
		addComponentToRow(column2, lastNameTextField, null);

		// Screen name
		screenNameTextField = new TextField();
		screenNameTextField.setEnabled(false);
		// screenNameTextField.addValidator(new ScreenNameValidator());
		screenNameTextField
				.addValueChangeListener(new FieldTextChangedListener(
						screenNameTextField));

		// Email
		emailTextField = new TextField();
		emailTextField.setEnabled(false);
		emailTextField.addValueChangeListener(new FieldTextChangedListener(
				emailTextField));

		// Add to row
		addComponentToRow(column2, screenNameTextField, emailTextField);

		// Date of Birth
		dateOfBirthDateField = new DateField();
		dateOfBirthDateField.setImmediate(true);
		dateOfBirthDateField.setResolution(Resolution.DAY);
		dateOfBirthDateField.addValueChangeListener(dateOfBirthListener);

		// Gender
		genderComboBox = new ComboBox();
		genderComboBox.setNullSelectionAllowed(false);
		genderComboBox.addValueChangeListener(genderListener);
		for (String gender : viewPresenter.getAvailableGenders()) {
			genderComboBox.addItem(gender);
		}

		// Add to row
		addComponentToRow(column2, dateOfBirthDateField, genderComboBox);

		// Job Title
		jobTitleField = new TextField();
		jobTitleField.addValueChangeListener(new FieldTextChangedListener(
				jobTitleField));

		// Language
		languageComboBox = new ComboBox();
		languageComboBox.setNullSelectionAllowed(false);
		for (Locale locale : viewPresenter.getAvailableLocales()) {
			languageComboBox.addItem(locale);
		}
		languageComboBox.addValueChangeListener(languageListener);

		// Add to row
		addComponentToRow(column2, jobTitleField, languageComboBox);

		// Column 3
		ResponsiveBlock column3 = createBlock(3);
		mainRow.addComponent(column3);

		// Current Organization header
		orgHeader = new Subheader();
		addComponentInRow(column3, orgHeader, 12);

		// Organization image
		orgLogo = new ScaledImage(true);
		orgLogo.setHeight(175, Unit.PIXELS);
		addComponentInRow(column3, orgLogo, 12);

		// Organization Name
		orgNameField = new Label();
		addComponentInRow(column3, orgNameField, 12);

		// Populate the view
		populateView();
	}

	private void addComponentToRow(AbstractLayout layout, Component c1,
			Component c2) {
		// Add a new row to the layout
		ResponsiveRow newRow = createRow();
		layout.addComponent(newRow);

		// Component 1
		newRow.addComponent(createBlock(6, 0, c1));

		// Component 2
		if (c2 != null) {
			newRow.addComponent(createBlock(6, 0, c2));
		}
	}

	@Messages({
			@Message(key = "uploadPortrait", value = "Upload portrait"),
			@Message(key = "firstname", value = "First name"),
			@Message(key = "middlename", value = "Middle name"),
			@Message(key = "lastname", value = "Last name"),
			@Message(key = "screenname", value = "Screen name"),
			@Message(key = "email", value = "Email"),
			@Message(key = "dateOfBirth", value = "Date of birth"),
			@Message(key = "gender", value = "Gender"),
			@Message(key = "jobTitle", value = "Job title"),
			@Message(key = "language", value = "Language"),
			@Message(key = "currentOrganization", value = "Current organization"),
			@Message(key = "name", value = "Name") })
	public void refreshCaption() {
		portraitUploader.setCaption(messageBundle.uploadPortrait());
		firstNameTextField.setCaption(messageBundle.firstname());
		middleNameTextField.setCaption(messageBundle.middlename());
		lastNameTextField.setCaption(messageBundle.lastname());
		screenNameTextField.setCaption(messageBundle.screenname());
		emailTextField.setCaption(messageBundle.email());
		dateOfBirthDateField.setCaption(messageBundle.dateOfBirth());
		genderComboBox.setCaption(messageBundle.gender());
		for (String gender : viewPresenter.getAvailableGenders()) {
			genderComboBox.setItemCaption(gender,
					viewPresenter.getGenderLabel(gender));
		}
		jobTitleField.setCaption(messageBundle.jobTitle());
		languageComboBox.setCaption(messageBundle.language());
		for (Locale locale : viewPresenter.getAvailableLocales()) {
			languageComboBox.setItemCaption(locale,
					viewPresenter.getLanguageLabel(locale));
		}
		orgHeader.setValue(messageBundle.currentOrganization());
		orgNameField.setCaption(messageBundle.name());
	}

	public void refreshView() {
		populateView();
	}

	protected void populateView() {
		isPopulating = true;

		portraitEmbedded.setSource(viewPresenter.getPortraitResource());
		firstNameTextField.setValue(viewPresenter.getFirstName());
		middleNameTextField.setValue(viewPresenter.getMiddleName());
		lastNameTextField.setValue(viewPresenter.getLastName());
		screenNameTextField.setValue(viewPresenter.getScreenName());
		emailTextField.setValue(viewPresenter.getEmail());
		jobTitleField.setValue(viewPresenter.getJobTitle());
		dateOfBirthDateField.setValue(viewPresenter.getDateOfBirth());
		genderComboBox.select(viewPresenter.getSelectedGender());
		languageComboBox.select(viewPresenter.getSelectedLanguage());
		updateCurrentOrg();

		isPopulating = false;
	}

	private void updateCurrentOrg() {
		orgNameField.setValue(viewPresenter.getCurrentOrgName());
		orgLogo.setSource(viewPresenter.getCurrentOrgLogoResource());
	}

	@Message(key = "portraitUpdated", value = "Your portrait was successfully updated")
	public void portraitCommited() {
		application.showNotification(messageBundle.portraitUpdated());

		portraitUploader.closePopup();
		populateView();
	}

	@Messages({
			@Message(key = "errorOccured", value = "An error occured :("),
			@Message(key = "portraitUploadFailed", value = "Your portrait couldn't be updated.\nPlease try again.") })
	public void portraitCommitFailed() {
		application.showWarning(messageBundle.errorOccured(),
				messageBundle.portraitUploadFailed());
	}
}
