package de.humance.eco.view;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.github.peholmst.i18n4vaadin.annotations.Message;
import com.github.peholmst.i18n4vaadin.annotations.Messages;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Country;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.event.Action;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.LiferayTheme;

import de.humance.common.util.AttributeDefinitionType;
import de.humance.eco.portlet.QualificationProfileApplication;
import de.humance.eco.presenter.QualificationProfileViewPresenter;
import de.humance.eco.profile.model.Qualification;
import de.humance.eco.profile.model.QualificationAttribute;
import de.humance.eco.profile.model.QualificationAttributeDefinition;
import de.humance.eco.profile.model.QualificationType;
import de.humance.eco.util.CommonConstants;
import de.humance.eco.util.ContextMenuAction;
import de.humance.vaadin.i18n.LocaleChangedIpcEventListener;
import de.humance.vaadin.ui.content.Subheader;
import de.humance.vaadin.ui.layout.Popup;
import de.humance.vaadin.ui.layout.ResponsiveBlock;
import de.humance.vaadin.ui.layout.ResponsiveContainer;
import de.humance.vaadin.ui.layout.ResponsiveRow;
import de.humance.vaadin.ui.layout.Table;
import de.humance.vaadin.ui.misc.AttributeBooleanWidget;
import de.humance.vaadin.ui.misc.AttributeDateWidget;
import de.humance.vaadin.ui.misc.AttributeDoubleWidget;
import de.humance.vaadin.ui.misc.AttributeIntegerWidget;
import de.humance.vaadin.ui.misc.AttributeStringWidget;
import de.humance.vaadin.ui.misc.BaseAttributeWidget;
import de.humance.vaadin.util.BootstrapStyle;
import de.humance.vaadin.view.BootstrapView;

public class QualificationProfileView
		extends
		BootstrapView<QualificationProfileViewPresenter, QualificationProfileApplication> {
	private static final long serialVersionUID = 4603196649466931363L;
	private static Action[] HANDLER_ACTIONS = new Action[] {
			ContextMenuAction.EDIT.getAction(),
			ContextMenuAction.DELETE.getAction() };
	private static final String TABLECOL_NAME = "name";
	private static final String TABLECOL_TYPE = "type";
	private static final String TABLECOL_CERT_NO = "certNo";
	private static final String TABLECOL_ISSUE_DATE = "issueDate";
	private static final String TABLECOL_EXPIRY_DATE = "expiryDate";
	private Bundle messageBundle = new Bundle();
	private DateFormat dateFormatter = null;
	private Subheader certSubHeader = null;
	private Button addCertBtn = null;
	private AddCertBtnClickListener addCertBtnListener = new AddCertBtnClickListener();
	private Table certTable = null;
	private QualificationTableActionHandler qualificationActionHandler = new QualificationTableActionHandler();
	// Add Certificate popup
	private Popup addCertificatePopup = null;
	private TextField nameTextField = null;
	private ComboBox typeComboBox = null;
	private TextField certNumberTextField = null;
	private DateField issueDateField = null;
	private DateField expiryDateField = null;
	private TextField issuingInstitutionTextField = null;
	private ComboBox issuingCountryComboBox = null;
	private Label noAttrTitle = null;
	private ResponsiveContainer attributesContainer = null;
	@SuppressWarnings("rawtypes")
	private List<BaseAttributeWidget> attrWidgets = null;
	private Qualification qualificationToEdit = null;

	@SuppressWarnings("serial")
	private class AddCertBtnClickListener implements Button.ClickListener {

		public void buttonClick(ClickEvent event) {
			displayPopup();
		}
	}

	@SuppressWarnings("serial")
	private class TypeComboBoxValueChangeListener implements
			Property.ValueChangeListener {

		public void valueChange(ValueChangeEvent event) {
			Long typeId = (Long) typeComboBox.getValue();
			updateAttributeContainer(
					(qualificationToEdit != null ? qualificationToEdit.getQualificationId()
							: 0), typeId);
		}
	}

	@SuppressWarnings("serial")
	private class CancelButtonClickListener implements Button.ClickListener {

		public void buttonClick(ClickEvent event) {
			preparePopup();
			closePopup();
		}
	}

	@SuppressWarnings("serial")
	private class CommitButtonClickListener implements Button.ClickListener {

		@Message(key = "mandatoryFieldsNotFilled", value = "Please fill all mandatory fields")
		@SuppressWarnings("rawtypes")
		public void buttonClick(ClickEvent event) {
			String name = nameTextField.getValue();
			Long typeId = (Long) typeComboBox.getValue();
			String certNumber = certNumberTextField.getValue();
			Date issueDate = issueDateField.getValue();
			Date expiryDate = expiryDateField.getValue();
			String issuingInstitution = issuingInstitutionTextField.getValue();
			String issuingCountry = (String) issuingCountryComboBox.getValue();

			if (Validator.isNull(typeId) || Validator.isNull(certNumber)
					|| Validator.isNull(issueDate)
					|| Validator.isNull(issuingInstitution)
					|| Validator.isNull(issuingCountry)) {
				application.showWarning(messageBundle
						.mandatoryFieldsNotFilled());
				return;
			}

			for (BaseAttributeWidget attrWidget : attrWidgets) {
				if (attrWidget instanceof AttributeBooleanWidget) {
					// Just ignore
					continue;
				}

				if (Validator.isNull(attrWidget.getValue())) {
					application.showWarning(messageBundle
							.mandatoryFieldsNotFilled());
					return;
				}
			}

			viewPresenter.commitCertificate(
					(qualificationToEdit != null ? qualificationToEdit
							.getQualificationId() : 0), typeId, name,
					certNumber, issueDate, expiryDate, issuingInstitution,
					issuingCountry, attrWidgets);
		}
	}

	@SuppressWarnings("serial")
	private class QualificationTableActionHandler implements Action.Handler {

		public Action[] getActions(Object target, Object sender) {
			return HANDLER_ACTIONS;
		}

		public void handleAction(Action action, Object sender, Object target) {
			if (target == null) {
				return;
			}

			Long rowId = (Long) target;
			if (action == ContextMenuAction.DELETE.getAction()) {
				viewPresenter.deleteQualification(rowId);

			} else if (action == ContextMenuAction.EDIT.getAction()) {
				qualificationToEdit = viewPresenter.getQualification(rowId);
				displayPopup();
			}
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected QualificationProfileViewPresenter initViewPresenter(
			QualificationProfileApplication application) {
		dateFormatter = DateFormat.getDateInstance(DateFormat.SHORT,
				application.getLocale());
		attrWidgets = new ArrayList<BaseAttributeWidget>();
		return new QualificationProfileViewPresenter(this, application);
	}

	@Override
	protected boolean isFluid() {
		return true;
	}

	@Override
	protected void initComponent() {
		// Initialize the IPC
		application.getIpc().addLiferayIPCEventListener(
				CommonConstants.IPC_EVENT_LOCALE_CHANGED,
				new LocaleChangedIpcEventListener(application));
	}

	@Override
	public void initLayout() {
		// Row 1
		ResponsiveRow row1 = createRow();
		rootContainer.addComponent(row1);

		// Cert subheader
		certSubHeader = new Subheader();
		certSubHeader.addStyleName(BootstrapStyle.SPAN10);
		row1.addComponent(certSubHeader);

		addCertBtn = new Button();
		addCertBtn.addStyleName(BootstrapStyle.SPAN2);
		addCertBtn.addClickListener(addCertBtnListener);
		row1.addComponent(addCertBtn);

		// Cert table
		certTable = new Table(false, false, false, false);
		certTable.setHeight(300, Unit.PIXELS);
		certTable.addContainerProperty(TABLECOL_NAME, Label.class, null);
		certTable.addContainerProperty(TABLECOL_TYPE, Label.class, null);
		certTable.addContainerProperty(TABLECOL_CERT_NO, Label.class, null);
		certTable.addContainerProperty(TABLECOL_ISSUE_DATE, Label.class, null);
		certTable.addContainerProperty(TABLECOL_EXPIRY_DATE, Label.class, null);
		certTable.setColumnExpandRatio(TABLECOL_NAME, 1);
		certTable.setColumnExpandRatio(TABLECOL_TYPE, 1);
		certTable.setColumnWidth(TABLECOL_CERT_NO, 180);
		certTable.setColumnWidth(TABLECOL_ISSUE_DATE, 120);
		certTable.setColumnWidth(TABLECOL_EXPIRY_DATE, 120);
		certTable.setImmediate(true);
		certTable.setSelectable(true);
		certTable.addActionHandler(qualificationActionHandler);
		addComponentInRow(rootContainer, certTable, 12);

		// Populate the view
		populateView();
	}

	@Messages({ @Message(key = "certificate", value = "Certificate"),
			@Message(key = "addCertificate", value = "add"),
			@Message(key = "name", value = "Name"),
			@Message(key = "type", value = "Type"),
			@Message(key = "certNo", value = "Cert. no."),
			@Message(key = "issueDate", value = "Issue date"),
			@Message(key = "expiryDate", value = "Expiry date") })
	@Override
	public void refreshCaption() {
		certSubHeader.setValue(messageBundle.certificate());
		addCertBtn.setCaption(messageBundle.addCertificate());
		certTable.setColumnHeader(TABLECOL_NAME, messageBundle.name());
		certTable.setColumnHeader(TABLECOL_TYPE, messageBundle.type());
		certTable.setColumnHeader(TABLECOL_CERT_NO, messageBundle.certNo());
		certTable.setColumnHeader(TABLECOL_ISSUE_DATE,
				messageBundle.issueDate());
		certTable.setColumnHeader(TABLECOL_EXPIRY_DATE,
				messageBundle.expiryDate());

		// Reset the popup
		addCertificatePopup = null;
	}

	public void refreshView() {
		populateView();
	}

	protected void populateView() {
		certTable.removeAllItems();
		for (Qualification qualif : viewPresenter.getAvailableQualifications()) {
			Label nameLabel = new Label(qualif.getName());
			nameLabel.setDescription(qualif.getName());
			String qualifType = viewPresenter.getQualificationTypeLabel(qualif
					.getTypeId());
			Label typeLabel = new Label(qualifType);
			typeLabel.setDescription(qualifType);
			Label certNoLabel = new Label(qualif.getCertNumber());
			Label issueDateLabel = new Label(dateFormatter.format(qualif
					.getDeliveryDate()));
			Label expiryDateLabel = new Label(dateFormatter.format(qualif
					.getExpirationDate()));
			certTable.addItem(new Object[] { nameLabel, typeLabel, certNoLabel,
					issueDateLabel, expiryDateLabel },
					qualif.getQualificationId());
		}
	}

	@Messages({
			@Message(key = "newCertificate", value = "New certificate"),
			@Message(key = "baseInformation", value = "Base information"),
			@Message(key = "name", value = "Name"),
			@Message(key = "type", value = "Type"),
			@Message(key = "certificateNumber", value = "Certificate number"),
			@Message(key = "issueDate", value = "Issue date"),
			@Message(key = "issuingInstitution", value = "Issuing institution"),
			@Message(key = "issuingCountry", value = "Issuing country"),
			@Message(key = "expiryDate", value = "Expiry date"),
			@Message(key = "extraProperties", value = "Extra properties"),
			@Message(key = "noExtraProperties", value = "No extra properties."),
			@Message(key = "discard", value = "discard"),
			@Message(key = "commit", value = "commit") })
	private Popup getPopup() {
		if (addCertificatePopup == null) {
			ResponsiveContainer layoutContainer = createContainer();
			addCertificatePopup = new Popup(messageBundle.newCertificate(),
					layoutContainer, true);

			// Row layout
			ResponsiveRow rowLayout = createRow();
			layoutContainer.addComponent(rowLayout);

			// Col 1
			ResponsiveBlock col1Block = createBlock(6);
			rowLayout.addComponent(col1Block);

			// Subheader
			Subheader baseInfoTitle = new Subheader(
					messageBundle.baseInformation());
			addComponentInRow(col1Block, baseInfoTitle, 12);

			// Name
			nameTextField = new TextField(messageBundle.name());
			addComponentInRow(col1Block, nameTextField, 12);

			// Type
			typeComboBox = new ComboBox(messageBundle.type());
			typeComboBox.setImmediate(true);
			typeComboBox.setNullSelectionAllowed(false);
			for (QualificationType type : viewPresenter.getQualificationTypes()) {
				typeComboBox.addItem(type.getQualificationTypeId());
				typeComboBox.setItemCaption(type.getQualificationTypeId(),
						type.getName());
			}
			typeComboBox
					.addValueChangeListener(new TypeComboBoxValueChangeListener());
			addComponentInRow(col1Block, typeComboBox, 12);

			// Cert number
			certNumberTextField = new TextField(
					messageBundle.certificateNumber());
			addComponentInRow(col1Block, certNumberTextField, 12);

			// Issue date
			issueDateField = new DateField(messageBundle.issueDate());
			issueDateField.setResolution(Resolution.DAY);
			addComponentInRow(col1Block, issueDateField, 12);

			// Issuing institution
			issuingInstitutionTextField = new TextField(
					messageBundle.issuingInstitution());
			addComponentInRow(col1Block, issuingInstitutionTextField, 12);

			// Issuing country
			issuingCountryComboBox = new ComboBox(
					messageBundle.issuingCountry());
			issuingCountryComboBox.setNullSelectionAllowed(false);
			for (Country country : viewPresenter.getAvailableCountries()) {
				issuingCountryComboBox.addItem(country.getName());
				issuingCountryComboBox.setItemCaption(country.getName(),
						country.getName(application.getUserLocale()));
			}
			addComponentInRow(col1Block, issuingCountryComboBox, 12);

			// Expiry date
			expiryDateField = new DateField(messageBundle.expiryDate());
			expiryDateField.setResolution(Resolution.DAY);
			addComponentInRow(col1Block, expiryDateField, 12);

			// Col 2
			ResponsiveBlock col2Block = createBlock(6);
			rowLayout.addComponent(col2Block);

			// Subheader
			Subheader attributesTitle = new Subheader(
					messageBundle.extraProperties());
			addComponentInRow(col2Block, attributesTitle, 12);

			// No attributes content
			noAttrTitle = new Label(messageBundle.noExtraProperties());
			noAttrTitle.setWidthUndefined();
			col2Block.addComponent(noAttrTitle);

			// Attribute container
			attributesContainer = createContainer();
			Panel attributesContainerPanel = new Panel(attributesContainer);
			attributesContainerPanel.setStyleName(LiferayTheme.PANEL_LIGHT);
			attributesContainerPanel.setHeight(330, Unit.PIXELS);
			addComponentInRow(col2Block, attributesContainerPanel, 12);

			// Button row
			ResponsiveRow btnRow = createRow();
			layoutContainer.addComponent(btnRow);

			// Discard button
			Button discardBtn = new Button(messageBundle.discard());
			discardBtn.addStyleName(BootstrapStyle.SPAN2);
			discardBtn.addClickListener(new CancelButtonClickListener());
			btnRow.addComponent(discardBtn);

			// Commit button
			Button commitBtn = new Button(messageBundle.commit());
			commitBtn.addStyleName(BootstrapStyle.SPAN2);
			commitBtn.addStyleName(BootstrapStyle.OFFSET8);
			commitBtn.addClickListener(new CommitButtonClickListener());
			btnRow.addComponent(commitBtn);
		}

		return addCertificatePopup;
	}

	public void displayPopup() {
		Popup addCertificatePopup = getPopup();
		preparePopup();
		addCertificatePopup.center();
		getUI().addWindow(addCertificatePopup);
	}

	public void closePopup() {
		getUI().removeWindow(addCertificatePopup);
		qualificationToEdit = null;
	}

	public void preparePopup() {
		if (qualificationToEdit == null) {
			nameTextField.setValue(StringPool.BLANK);
			// typeComboBox = null;
			certNumberTextField.setValue(StringPool.BLANK);
			issueDateField.setValue(new Date());
			expiryDateField.setValue(new Date());
			issuingInstitutionTextField.setValue(StringPool.BLANK);
			// issuingCountryComboBox = null;
		} else {
			nameTextField.setValue(qualificationToEdit.getName());
			typeComboBox.setValue(qualificationToEdit.getTypeId());
			certNumberTextField.setValue(qualificationToEdit.getCertNumber());
			issueDateField.setValue(qualificationToEdit.getDeliveryDate());
			expiryDateField.setValue(qualificationToEdit.getExpirationDate());
			issuingInstitutionTextField.setValue(qualificationToEdit
					.getIssuingInstitution());
			issuingCountryComboBox.setValue(qualificationToEdit
					.getIssuingCountry());
		}
		updateAttributeContainer(
				(qualificationToEdit != null ? qualificationToEdit.getQualificationId()
						: 0),
				(qualificationToEdit != null ? qualificationToEdit.getTypeId()
						: 0));
	}

	@SuppressWarnings("rawtypes")
	private void updateAttributeContainer(long qualificationId, long typeId) {
		noAttrTitle.setVisible(true);
		attributesContainer.removeAllComponents();
		attrWidgets.clear();
		if (typeId < 1) {
			return;
		}

		List<QualificationAttributeDefinition> definitions = viewPresenter
				.getAttrDefinitions(typeId);
		if (definitions.isEmpty()) {
			return;
		}

		noAttrTitle.setVisible(false);
		for (QualificationAttributeDefinition definition : definitions) {
			QualificationAttribute attribute = null;
			if (qualificationId > 0) {
				attribute = viewPresenter.getAttribute(qualificationId,
						definition.getQualificationAttributeDefinitionId());
			}
			long attributeId = (attribute != null ? attribute
					.getQualificationAttributeId() : 0);
			BaseAttributeWidget attrWidget = null;
			AttributeDefinitionType type = AttributeDefinitionType
					.valueOf(definition.getType());
			if (type == AttributeDefinitionType.STRING) {
				attrWidget = new AttributeStringWidget(attributeId,
						definition.getQualificationAttributeDefinitionId(),
						qualificationId, definition.getTitle(),
						(attribute != null ? attribute.getAttributeValue()
								: StringPool.BLANK), definition.getUnit());
			} else if (type == AttributeDefinitionType.INTEGER) {
				attrWidget = new AttributeIntegerWidget(attributeId,
						definition.getQualificationAttributeDefinitionId(),
						qualificationId, definition.getTitle(),
						(attribute != null ? Integer.parseInt(attribute
								.getAttributeValue()) : 0),
						definition.getUnit());
			} else if (type == AttributeDefinitionType.DOUBLE) {
				attrWidget = new AttributeDoubleWidget(attributeId,
						definition.getQualificationAttributeDefinitionId(),
						qualificationId, definition.getTitle(),
						(attribute != null ? Double.parseDouble(attribute
								.getAttributeValue()) : 0.0),
						definition.getUnit());
			} else if (type == AttributeDefinitionType.DATE) {
				attrWidget = new AttributeDateWidget(attributeId,
						definition.getQualificationAttributeDefinitionId(),
						qualificationId, definition.getTitle(),
						(attribute != null ? new Date(Long.parseLong(attribute
								.getAttributeValue())) : null));
			} else if (type == AttributeDefinitionType.BOOLEAN) {
				attrWidget = new AttributeBooleanWidget(attributeId,
						definition.getQualificationAttributeDefinitionId(),
						qualificationId, definition.getTitle(),
						(attribute != null ? Boolean.parseBoolean(attribute
								.getAttributeValue()) : false));
			}
			if (attrWidget == null) {
				continue;
			}
			attrWidgets.add(attrWidget);
			addComponentInRow(attributesContainer, attrWidget, 12);
		}
	}

	@Message(key = "certCommitted", value = "Certificate successfully saved")
	public void certificateCommitted() {
		application.showNotification(messageBundle.certCommitted());

		preparePopup();
		closePopup();
		populateView();
	}

	@Messages({
			@Message(key = "errorOccured", value = "An error occured :("),
			@Message(key = "certCommitFailed", value = "The certificate could not be saved.\nPlease try again.") })
	public void certificateCommitFailed(Exception e) {
		application.showWarning(messageBundle.errorOccured(),
				messageBundle.certCommitFailed());
	}

	@Message(key = "certDeleted", value = "Certificate successfully deleted")
	public void certificateDeleted() {
		application.showNotification(messageBundle.certDeleted());

		populateView();
	}

	@Messages({
			@Message(key = "errorOccured", value = "An error occured :("),
			@Message(key = "certDeleteFailed", value = "The certificate could not be deleted.\nPlease try again.") })
	public void certificateDeleteFailed(Exception e) {
		application.showWarning(messageBundle.errorOccured(),
				messageBundle.certDeleteFailed());
	}
}
