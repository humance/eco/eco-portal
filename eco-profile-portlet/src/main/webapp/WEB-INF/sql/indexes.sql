create index IX_6516EA9F on Profile_Qualification (ownerId);
create index IX_A45D87C8 on Profile_Qualification (ownerId, typeId);
create index IX_2D79FC48 on Profile_Qualification (typeId);

create index IX_A1EDA118 on Profile_QualificationAttribute (qualificationAttributeDefinitionId);
create index IX_C5CF0131 on Profile_QualificationAttribute (qualificationId);
create index IX_AFF53640 on Profile_QualificationAttribute (qualificationId, qualificationAttributeDefinitionId);

create index IX_9C1A4278 on Profile_QualificationAttributeDefinition (qualificationTypeId);
create index IX_7BFC6E8C on Profile_QualificationAttributeDefinition (title);

create index IX_7157AC39 on Profile_QualificationAttributeDefinitionResource (qualificationAttributeDefinitionId);
create index IX_2872FD2D on Profile_QualificationAttributeDefinitionResource (qualificationAttributeDefinitionId, country);
create index IX_4B3F6139 on Profile_QualificationAttributeDefinitionResource (qualificationAttributeDefinitionId, country, language);
create index IX_180DB145 on Profile_QualificationAttributeDefinitionResource (qualificationAttributeDefinitionId, language);

create index IX_2ABE3078 on Profile_QualificationType (name);

create index IX_98584B2D on Profile_QualificationTypeResource (qualificationTypeId);
create index IX_B43F0B9 on Profile_QualificationTypeResource (qualificationTypeId, country);
create index IX_6CCE55C5 on Profile_QualificationTypeResource (qualificationTypeId, country, language);
create index IX_8F5B2F39 on Profile_QualificationTypeResource (qualificationTypeId, language);

create index IX_44A842BA on Profile_Trailer (driverId);
create index IX_BA12925C on Profile_Trailer (driverId, organizationId);
create index IX_8C4E94E3 on Profile_Trailer (driverId, typeId);
create index IX_D20DC3AC on Profile_Trailer (licensePlate);
create index IX_81E75223 on Profile_Trailer (manufacturerId);
create index IX_1A783741 on Profile_Trailer (modelName);
create index IX_627DD60C on Profile_Trailer (typeId);

create index IX_2E32A93C on Profile_TrailerType (name);

create index IX_D6687590 on Profile_TrailerUser (trailerId);
create index IX_746BE1C8 on Profile_TrailerUser (userId);

create index IX_EC0F33 on Profile_Vehicle (driverId);
create index IX_D884CD5 on Profile_Vehicle (driverId, organizationId);
create index IX_F25ED85C on Profile_Vehicle (driverId, typeId);
create index IX_F634ABA5 on Profile_Vehicle (licensePlate);
create index IX_37F41FDC on Profile_Vehicle (manufacturerId);
create index IX_E6ADF9E8 on Profile_Vehicle (modelName);
create index IX_480C6CC5 on Profile_Vehicle (typeId);

create index IX_60590F5E on Profile_VehicleAttribute (vehicleAttributeDefinitionId);
create index IX_F5E7DCAB on Profile_VehicleAttribute (vehicleId);
create index IX_7E09EC9D on Profile_VehicleAttribute (vehicleId, vehicleAttributeDefinitionId);

create index IX_CD0499C9 on Profile_VehicleAttributeDefinition (title);
create index IX_B44C0E72 on Profile_VehicleAttributeDefinition (vehicleTypeId);

create index IX_4BF1347F on Profile_VehicleAttributeDefinitionResource (vehicleAttributeDefinitionId);
create index IX_FA2763A7 on Profile_VehicleAttributeDefinitionResource (vehicleAttributeDefinitionId, country);
create index IX_A0297733 on Profile_VehicleAttributeDefinitionResource (vehicleAttributeDefinitionId, country, language);
create index IX_7CE61A0B on Profile_VehicleAttributeDefinitionResource (vehicleAttributeDefinitionId, language);

create index IX_AD09AD4C on Profile_VehicleManufacturer (name);

create index IX_EA7675B5 on Profile_VehicleType (name);

create index IX_CB19A10D on Profile_VehicleTypeResource (vehicleTypeId);
create index IX_847536D9 on Profile_VehicleTypeResource (vehicleTypeId, country);
create index IX_23673E5 on Profile_VehicleTypeResource (vehicleTypeId, country, language);
create index IX_3C52AD19 on Profile_VehicleTypeResource (vehicleTypeId, language);

create index IX_2EEE7401 on Profile_VehicleUser (userId);
create index IX_362EE230 on Profile_VehicleUser (vehicleId);