create table Profile_Qualification (
	qualificationId LONG not null primary key,
	creatorId LONG,
	creatorName VARCHAR(255) null,
	createDate DATE null,
	modifierId LONG,
	modifierName VARCHAR(255) null,
	modifiedDate DATE null,
	ownerId LONG,
	typeId LONG,
	name VARCHAR(500) null,
	certNumber VARCHAR(255) null,
	deliveryDate DATE null,
	expirationDate DATE null,
	issuingCountry VARCHAR(75) null,
	issuingInstitution VARCHAR(500) null
);

create table Profile_QualificationAttribute (
	qualificationAttributeId LONG not null primary key,
	qualificationId LONG,
	qualificationAttributeDefinitionId LONG,
	type_ VARCHAR(75) null,
	sequenceNumber LONG,
	attributeValue TEXT null
);

create table Profile_QualificationAttributeDefinition (
	qualificationAttributeDefinitionId LONG not null primary key,
	creatorId LONG,
	creatorName VARCHAR(75) null,
	createDate DATE null,
	modifierId LONG,
	modifierName VARCHAR(75) null,
	modifiedDate DATE null,
	qualificationTypeId LONG,
	type_ VARCHAR(75) null,
	title VARCHAR(500) null,
	minRangeValue DOUBLE,
	maxRangeValue DOUBLE,
	unit VARCHAR(255) null,
	sequenceNumber LONG
);

create table Profile_QualificationAttributeDefinitionResource (
	qualificationAttributeDefinitionResourceId LONG not null primary key,
	qualificationAttributeDefinitionId LONG,
	country VARCHAR(75) null,
	language VARCHAR(75) null,
	title VARCHAR(500) null,
	unit VARCHAR(255) null
);

create table Profile_QualificationType (
	qualificationTypeId LONG not null primary key,
	creatorId LONG,
	creatorName VARCHAR(255) null,
	createDate DATE null,
	modifierId LONG,
	modifierName VARCHAR(255) null,
	modifiedDate DATE null,
	name VARCHAR(1000) null,
	description TEXT null,
	note VARCHAR(2000) null,
	iconId LONG
);

create table Profile_QualificationTypeResource (
	qualificationTypeResourceId LONG not null primary key,
	qualificationTypeId LONG,
	country VARCHAR(75) null,
	language VARCHAR(75) null,
	name VARCHAR(1000) null,
	description TEXT null,
	note VARCHAR(2000) null,
	iconId LONG
);

create table Profile_Trailer (
	trailerId LONG not null primary key,
	creatorId LONG,
	creatorName VARCHAR(75) null,
	createDate DATE null,
	modifierId LONG,
	modifierName VARCHAR(75) null,
	modifiedDate DATE null,
	driverId LONG,
	typeId LONG,
	manufacturerId LONG,
	organizationId LONG,
	modelName VARCHAR(75) null,
	dimensionHeight LONG,
	dimensionWidth LONG,
	dimensionDepth LONG,
	licensePlate VARCHAR(75) null
);

create table Profile_TrailerType (
	trailerTypeId LONG not null primary key,
	creatorId LONG,
	creatorName VARCHAR(75) null,
	createDate DATE null,
	modifierId LONG,
	modifierName VARCHAR(75) null,
	modifiedDate DATE null,
	name VARCHAR(75) null,
	iconId LONG
);

create table Profile_TrailerUser (
	trailerUserId LONG not null primary key,
	userId LONG,
	trailerId LONG
);

create table Profile_Vehicle (
	vehicleId LONG not null primary key,
	creatorId LONG,
	creatorName VARCHAR(255) null,
	createDate DATE null,
	modifierId LONG,
	modifierName VARCHAR(255) null,
	modifiedDate DATE null,
	driverId LONG,
	typeId LONG,
	manufacturerId LONG,
	organizationId LONG,
	modelName VARCHAR(1000) null,
	dimensionHeight LONG,
	dimensionWidth LONG,
	dimensionDepth LONG,
	licensePlate VARCHAR(75) null
);

create table Profile_VehicleAttribute (
	vehicleAttributeId LONG not null primary key,
	vehicleId LONG,
	vehicleAttributeDefinitionId LONG,
	type_ VARCHAR(75) null,
	sequenceNumber LONG,
	attributeValue TEXT null
);

create table Profile_VehicleAttributeDefinition (
	vehicleAttributeDefinitionId LONG not null primary key,
	creatorId LONG,
	creatorName VARCHAR(75) null,
	createDate DATE null,
	modifierId LONG,
	modifierName VARCHAR(75) null,
	modifiedDate DATE null,
	vehicleTypeId LONG,
	type_ VARCHAR(75) null,
	title VARCHAR(500) null,
	minRangeValue DOUBLE,
	maxRangeValue DOUBLE,
	unit VARCHAR(255) null,
	sequenceNumber LONG
);

create table Profile_VehicleAttributeDefinitionResource (
	vehicleAttributeDefinitionResourceId LONG not null primary key,
	vehicleAttributeDefinitionId LONG,
	country VARCHAR(75) null,
	language VARCHAR(75) null,
	title VARCHAR(500) null,
	unit VARCHAR(255) null
);

create table Profile_VehicleManufacturer (
	vehicleManufacturerId LONG not null primary key,
	creatorId LONG,
	creatorName VARCHAR(75) null,
	createDate DATE null,
	modifierId LONG,
	modifierName VARCHAR(75) null,
	modifiedDate DATE null,
	name VARCHAR(500) null,
	iconId LONG
);

create table Profile_VehicleType (
	vehicleTypeId LONG not null primary key,
	creatorId LONG,
	creatorName VARCHAR(75) null,
	createDate DATE null,
	modifierId LONG,
	modifierName VARCHAR(75) null,
	modifiedDate DATE null,
	name VARCHAR(500) null,
	iconId LONG
);

create table Profile_VehicleTypeResource (
	vehicleTypeResourceId LONG not null primary key,
	vehicleTypeId LONG,
	country VARCHAR(75) null,
	language VARCHAR(75) null,
	name VARCHAR(500) null,
	iconId LONG
);

create table Profile_VehicleUser (
	vehicleUserId LONG not null primary key,
	userId LONG,
	vehicleId LONG
);