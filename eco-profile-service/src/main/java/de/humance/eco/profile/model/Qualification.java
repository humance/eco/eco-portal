package de.humance.eco.profile.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Qualification service. Represents a row in the &quot;Profile_Qualification&quot; database table, with each column mapped to a property of this class.
 *
 * @author Humance
 * @see QualificationModel
 * @see de.humance.eco.profile.model.impl.QualificationImpl
 * @see de.humance.eco.profile.model.impl.QualificationModelImpl
 * @generated
 */
public interface Qualification extends QualificationModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link de.humance.eco.profile.model.impl.QualificationImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
