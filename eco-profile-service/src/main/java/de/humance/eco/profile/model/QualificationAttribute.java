package de.humance.eco.profile.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the QualificationAttribute service. Represents a row in the &quot;Profile_QualificationAttribute&quot; database table, with each column mapped to a property of this class.
 *
 * @author Humance
 * @see QualificationAttributeModel
 * @see de.humance.eco.profile.model.impl.QualificationAttributeImpl
 * @see de.humance.eco.profile.model.impl.QualificationAttributeModelImpl
 * @generated
 */
public interface QualificationAttribute extends QualificationAttributeModel,
    PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link de.humance.eco.profile.model.impl.QualificationAttributeImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
