package de.humance.eco.profile.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import de.humance.eco.profile.service.ClpSerializer;
import de.humance.eco.profile.service.QualificationAttributeLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class QualificationAttributeClp extends BaseModelImpl<QualificationAttribute>
    implements QualificationAttribute {
    private long _qualificationAttributeId;
    private long _qualificationId;
    private long _qualificationAttributeDefinitionId;
    private String _type;
    private long _sequenceNumber;
    private String _attributeValue;
    private BaseModel<?> _qualificationAttributeRemoteModel;

    public QualificationAttributeClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return QualificationAttribute.class;
    }

    @Override
    public String getModelClassName() {
        return QualificationAttribute.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _qualificationAttributeId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setQualificationAttributeId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _qualificationAttributeId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("qualificationAttributeId", getQualificationAttributeId());
        attributes.put("qualificationId", getQualificationId());
        attributes.put("qualificationAttributeDefinitionId",
            getQualificationAttributeDefinitionId());
        attributes.put("type", getType());
        attributes.put("sequenceNumber", getSequenceNumber());
        attributes.put("attributeValue", getAttributeValue());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long qualificationAttributeId = (Long) attributes.get(
                "qualificationAttributeId");

        if (qualificationAttributeId != null) {
            setQualificationAttributeId(qualificationAttributeId);
        }

        Long qualificationId = (Long) attributes.get("qualificationId");

        if (qualificationId != null) {
            setQualificationId(qualificationId);
        }

        Long qualificationAttributeDefinitionId = (Long) attributes.get(
                "qualificationAttributeDefinitionId");

        if (qualificationAttributeDefinitionId != null) {
            setQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);
        }

        String type = (String) attributes.get("type");

        if (type != null) {
            setType(type);
        }

        Long sequenceNumber = (Long) attributes.get("sequenceNumber");

        if (sequenceNumber != null) {
            setSequenceNumber(sequenceNumber);
        }

        String attributeValue = (String) attributes.get("attributeValue");

        if (attributeValue != null) {
            setAttributeValue(attributeValue);
        }
    }

    @Override
    public long getQualificationAttributeId() {
        return _qualificationAttributeId;
    }

    @Override
    public void setQualificationAttributeId(long qualificationAttributeId) {
        _qualificationAttributeId = qualificationAttributeId;

        if (_qualificationAttributeRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationAttributeRemoteModel.getClass();

                Method method = clazz.getMethod("setQualificationAttributeId",
                        long.class);

                method.invoke(_qualificationAttributeRemoteModel,
                    qualificationAttributeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getQualificationId() {
        return _qualificationId;
    }

    @Override
    public void setQualificationId(long qualificationId) {
        _qualificationId = qualificationId;

        if (_qualificationAttributeRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationAttributeRemoteModel.getClass();

                Method method = clazz.getMethod("setQualificationId", long.class);

                method.invoke(_qualificationAttributeRemoteModel,
                    qualificationId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getQualificationAttributeDefinitionId() {
        return _qualificationAttributeDefinitionId;
    }

    @Override
    public void setQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId) {
        _qualificationAttributeDefinitionId = qualificationAttributeDefinitionId;

        if (_qualificationAttributeRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationAttributeRemoteModel.getClass();

                Method method = clazz.getMethod("setQualificationAttributeDefinitionId",
                        long.class);

                method.invoke(_qualificationAttributeRemoteModel,
                    qualificationAttributeDefinitionId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getType() {
        return _type;
    }

    @Override
    public void setType(String type) {
        _type = type;

        if (_qualificationAttributeRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationAttributeRemoteModel.getClass();

                Method method = clazz.getMethod("setType", String.class);

                method.invoke(_qualificationAttributeRemoteModel, type);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getSequenceNumber() {
        return _sequenceNumber;
    }

    @Override
    public void setSequenceNumber(long sequenceNumber) {
        _sequenceNumber = sequenceNumber;

        if (_qualificationAttributeRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationAttributeRemoteModel.getClass();

                Method method = clazz.getMethod("setSequenceNumber", long.class);

                method.invoke(_qualificationAttributeRemoteModel, sequenceNumber);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getAttributeValue() {
        return _attributeValue;
    }

    @Override
    public void setAttributeValue(String attributeValue) {
        _attributeValue = attributeValue;

        if (_qualificationAttributeRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationAttributeRemoteModel.getClass();

                Method method = clazz.getMethod("setAttributeValue",
                        String.class);

                method.invoke(_qualificationAttributeRemoteModel, attributeValue);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getQualificationAttributeRemoteModel() {
        return _qualificationAttributeRemoteModel;
    }

    public void setQualificationAttributeRemoteModel(
        BaseModel<?> qualificationAttributeRemoteModel) {
        _qualificationAttributeRemoteModel = qualificationAttributeRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _qualificationAttributeRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_qualificationAttributeRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            QualificationAttributeLocalServiceUtil.addQualificationAttribute(this);
        } else {
            QualificationAttributeLocalServiceUtil.updateQualificationAttribute(this);
        }
    }

    @Override
    public QualificationAttribute toEscapedModel() {
        return (QualificationAttribute) ProxyUtil.newProxyInstance(QualificationAttribute.class.getClassLoader(),
            new Class[] { QualificationAttribute.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        QualificationAttributeClp clone = new QualificationAttributeClp();

        clone.setQualificationAttributeId(getQualificationAttributeId());
        clone.setQualificationId(getQualificationId());
        clone.setQualificationAttributeDefinitionId(getQualificationAttributeDefinitionId());
        clone.setType(getType());
        clone.setSequenceNumber(getSequenceNumber());
        clone.setAttributeValue(getAttributeValue());

        return clone;
    }

    @Override
    public int compareTo(QualificationAttribute qualificationAttribute) {
        int value = 0;

        if (getQualificationId() < qualificationAttribute.getQualificationId()) {
            value = -1;
        } else if (getQualificationId() > qualificationAttribute.getQualificationId()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        if (getSequenceNumber() < qualificationAttribute.getSequenceNumber()) {
            value = -1;
        } else if (getSequenceNumber() > qualificationAttribute.getSequenceNumber()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof QualificationAttributeClp)) {
            return false;
        }

        QualificationAttributeClp qualificationAttribute = (QualificationAttributeClp) obj;

        long primaryKey = qualificationAttribute.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{qualificationAttributeId=");
        sb.append(getQualificationAttributeId());
        sb.append(", qualificationId=");
        sb.append(getQualificationId());
        sb.append(", qualificationAttributeDefinitionId=");
        sb.append(getQualificationAttributeDefinitionId());
        sb.append(", type=");
        sb.append(getType());
        sb.append(", sequenceNumber=");
        sb.append(getSequenceNumber());
        sb.append(", attributeValue=");
        sb.append(getAttributeValue());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(22);

        sb.append("<model><model-name>");
        sb.append("de.humance.eco.profile.model.QualificationAttribute");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>qualificationAttributeId</column-name><column-value><![CDATA[");
        sb.append(getQualificationAttributeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>qualificationId</column-name><column-value><![CDATA[");
        sb.append(getQualificationId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>qualificationAttributeDefinitionId</column-name><column-value><![CDATA[");
        sb.append(getQualificationAttributeDefinitionId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>type</column-name><column-value><![CDATA[");
        sb.append(getType());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>sequenceNumber</column-name><column-value><![CDATA[");
        sb.append(getSequenceNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>attributeValue</column-name><column-value><![CDATA[");
        sb.append(getAttributeValue());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
