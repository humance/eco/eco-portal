package de.humance.eco.profile.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the QualificationAttributeDefinition service. Represents a row in the &quot;Profile_QualificationAttributeDefinition&quot; database table, with each column mapped to a property of this class.
 *
 * @author Humance
 * @see QualificationAttributeDefinitionModel
 * @see de.humance.eco.profile.model.impl.QualificationAttributeDefinitionImpl
 * @see de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl
 * @generated
 */
public interface QualificationAttributeDefinition
    extends QualificationAttributeDefinitionModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
