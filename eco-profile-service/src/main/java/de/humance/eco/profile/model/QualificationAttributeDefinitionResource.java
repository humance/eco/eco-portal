package de.humance.eco.profile.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the QualificationAttributeDefinitionResource service. Represents a row in the &quot;Profile_QualificationAttributeDefinitionResource&quot; database table, with each column mapped to a property of this class.
 *
 * @author Humance
 * @see QualificationAttributeDefinitionResourceModel
 * @see de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceImpl
 * @see de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl
 * @generated
 */
public interface QualificationAttributeDefinitionResource
    extends QualificationAttributeDefinitionResourceModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
