package de.humance.eco.profile.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import de.humance.eco.profile.service.ClpSerializer;
import de.humance.eco.profile.service.QualificationAttributeDefinitionResourceLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class QualificationAttributeDefinitionResourceClp extends BaseModelImpl<QualificationAttributeDefinitionResource>
    implements QualificationAttributeDefinitionResource {
    private long _qualificationAttributeDefinitionResourceId;
    private long _qualificationAttributeDefinitionId;
    private String _country;
    private String _language;
    private String _title;
    private String _unit;
    private BaseModel<?> _qualificationAttributeDefinitionResourceRemoteModel;

    public QualificationAttributeDefinitionResourceClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return QualificationAttributeDefinitionResource.class;
    }

    @Override
    public String getModelClassName() {
        return QualificationAttributeDefinitionResource.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _qualificationAttributeDefinitionResourceId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setQualificationAttributeDefinitionResourceId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _qualificationAttributeDefinitionResourceId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("qualificationAttributeDefinitionResourceId",
            getQualificationAttributeDefinitionResourceId());
        attributes.put("qualificationAttributeDefinitionId",
            getQualificationAttributeDefinitionId());
        attributes.put("country", getCountry());
        attributes.put("language", getLanguage());
        attributes.put("title", getTitle());
        attributes.put("unit", getUnit());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long qualificationAttributeDefinitionResourceId = (Long) attributes.get(
                "qualificationAttributeDefinitionResourceId");

        if (qualificationAttributeDefinitionResourceId != null) {
            setQualificationAttributeDefinitionResourceId(qualificationAttributeDefinitionResourceId);
        }

        Long qualificationAttributeDefinitionId = (Long) attributes.get(
                "qualificationAttributeDefinitionId");

        if (qualificationAttributeDefinitionId != null) {
            setQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);
        }

        String country = (String) attributes.get("country");

        if (country != null) {
            setCountry(country);
        }

        String language = (String) attributes.get("language");

        if (language != null) {
            setLanguage(language);
        }

        String title = (String) attributes.get("title");

        if (title != null) {
            setTitle(title);
        }

        String unit = (String) attributes.get("unit");

        if (unit != null) {
            setUnit(unit);
        }
    }

    @Override
    public long getQualificationAttributeDefinitionResourceId() {
        return _qualificationAttributeDefinitionResourceId;
    }

    @Override
    public void setQualificationAttributeDefinitionResourceId(
        long qualificationAttributeDefinitionResourceId) {
        _qualificationAttributeDefinitionResourceId = qualificationAttributeDefinitionResourceId;

        if (_qualificationAttributeDefinitionResourceRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationAttributeDefinitionResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setQualificationAttributeDefinitionResourceId",
                        long.class);

                method.invoke(_qualificationAttributeDefinitionResourceRemoteModel,
                    qualificationAttributeDefinitionResourceId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getQualificationAttributeDefinitionId() {
        return _qualificationAttributeDefinitionId;
    }

    @Override
    public void setQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId) {
        _qualificationAttributeDefinitionId = qualificationAttributeDefinitionId;

        if (_qualificationAttributeDefinitionResourceRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationAttributeDefinitionResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setQualificationAttributeDefinitionId",
                        long.class);

                method.invoke(_qualificationAttributeDefinitionResourceRemoteModel,
                    qualificationAttributeDefinitionId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCountry() {
        return _country;
    }

    @Override
    public void setCountry(String country) {
        _country = country;

        if (_qualificationAttributeDefinitionResourceRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationAttributeDefinitionResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setCountry", String.class);

                method.invoke(_qualificationAttributeDefinitionResourceRemoteModel,
                    country);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getLanguage() {
        return _language;
    }

    @Override
    public void setLanguage(String language) {
        _language = language;

        if (_qualificationAttributeDefinitionResourceRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationAttributeDefinitionResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setLanguage", String.class);

                method.invoke(_qualificationAttributeDefinitionResourceRemoteModel,
                    language);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTitle() {
        return _title;
    }

    @Override
    public void setTitle(String title) {
        _title = title;

        if (_qualificationAttributeDefinitionResourceRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationAttributeDefinitionResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setTitle", String.class);

                method.invoke(_qualificationAttributeDefinitionResourceRemoteModel,
                    title);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getUnit() {
        return _unit;
    }

    @Override
    public void setUnit(String unit) {
        _unit = unit;

        if (_qualificationAttributeDefinitionResourceRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationAttributeDefinitionResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setUnit", String.class);

                method.invoke(_qualificationAttributeDefinitionResourceRemoteModel,
                    unit);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getQualificationAttributeDefinitionResourceRemoteModel() {
        return _qualificationAttributeDefinitionResourceRemoteModel;
    }

    public void setQualificationAttributeDefinitionResourceRemoteModel(
        BaseModel<?> qualificationAttributeDefinitionResourceRemoteModel) {
        _qualificationAttributeDefinitionResourceRemoteModel = qualificationAttributeDefinitionResourceRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _qualificationAttributeDefinitionResourceRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_qualificationAttributeDefinitionResourceRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            QualificationAttributeDefinitionResourceLocalServiceUtil.addQualificationAttributeDefinitionResource(this);
        } else {
            QualificationAttributeDefinitionResourceLocalServiceUtil.updateQualificationAttributeDefinitionResource(this);
        }
    }

    @Override
    public QualificationAttributeDefinitionResource toEscapedModel() {
        return (QualificationAttributeDefinitionResource) ProxyUtil.newProxyInstance(QualificationAttributeDefinitionResource.class.getClassLoader(),
            new Class[] { QualificationAttributeDefinitionResource.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        QualificationAttributeDefinitionResourceClp clone = new QualificationAttributeDefinitionResourceClp();

        clone.setQualificationAttributeDefinitionResourceId(getQualificationAttributeDefinitionResourceId());
        clone.setQualificationAttributeDefinitionId(getQualificationAttributeDefinitionId());
        clone.setCountry(getCountry());
        clone.setLanguage(getLanguage());
        clone.setTitle(getTitle());
        clone.setUnit(getUnit());

        return clone;
    }

    @Override
    public int compareTo(
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource) {
        int value = 0;

        if (getQualificationAttributeDefinitionId() < qualificationAttributeDefinitionResource.getQualificationAttributeDefinitionId()) {
            value = -1;
        } else if (getQualificationAttributeDefinitionId() > qualificationAttributeDefinitionResource.getQualificationAttributeDefinitionId()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        value = getCountry()
                    .compareToIgnoreCase(qualificationAttributeDefinitionResource.getCountry());

        if (value != 0) {
            return value;
        }

        value = getLanguage()
                    .compareToIgnoreCase(qualificationAttributeDefinitionResource.getLanguage());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof QualificationAttributeDefinitionResourceClp)) {
            return false;
        }

        QualificationAttributeDefinitionResourceClp qualificationAttributeDefinitionResource =
            (QualificationAttributeDefinitionResourceClp) obj;

        long primaryKey = qualificationAttributeDefinitionResource.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{qualificationAttributeDefinitionResourceId=");
        sb.append(getQualificationAttributeDefinitionResourceId());
        sb.append(", qualificationAttributeDefinitionId=");
        sb.append(getQualificationAttributeDefinitionId());
        sb.append(", country=");
        sb.append(getCountry());
        sb.append(", language=");
        sb.append(getLanguage());
        sb.append(", title=");
        sb.append(getTitle());
        sb.append(", unit=");
        sb.append(getUnit());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(22);

        sb.append("<model><model-name>");
        sb.append(
            "de.humance.eco.profile.model.QualificationAttributeDefinitionResource");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>qualificationAttributeDefinitionResourceId</column-name><column-value><![CDATA[");
        sb.append(getQualificationAttributeDefinitionResourceId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>qualificationAttributeDefinitionId</column-name><column-value><![CDATA[");
        sb.append(getQualificationAttributeDefinitionId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>country</column-name><column-value><![CDATA[");
        sb.append(getCountry());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>language</column-name><column-value><![CDATA[");
        sb.append(getLanguage());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>title</column-name><column-value><![CDATA[");
        sb.append(getTitle());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>unit</column-name><column-value><![CDATA[");
        sb.append(getUnit());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
