package de.humance.eco.profile.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Humance
 * @generated
 */
public class QualificationAttributeDefinitionResourceSoap
    implements Serializable {
    private long _qualificationAttributeDefinitionResourceId;
    private long _qualificationAttributeDefinitionId;
    private String _country;
    private String _language;
    private String _title;
    private String _unit;

    public QualificationAttributeDefinitionResourceSoap() {
    }

    public static QualificationAttributeDefinitionResourceSoap toSoapModel(
        QualificationAttributeDefinitionResource model) {
        QualificationAttributeDefinitionResourceSoap soapModel = new QualificationAttributeDefinitionResourceSoap();

        soapModel.setQualificationAttributeDefinitionResourceId(model.getQualificationAttributeDefinitionResourceId());
        soapModel.setQualificationAttributeDefinitionId(model.getQualificationAttributeDefinitionId());
        soapModel.setCountry(model.getCountry());
        soapModel.setLanguage(model.getLanguage());
        soapModel.setTitle(model.getTitle());
        soapModel.setUnit(model.getUnit());

        return soapModel;
    }

    public static QualificationAttributeDefinitionResourceSoap[] toSoapModels(
        QualificationAttributeDefinitionResource[] models) {
        QualificationAttributeDefinitionResourceSoap[] soapModels = new QualificationAttributeDefinitionResourceSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static QualificationAttributeDefinitionResourceSoap[][] toSoapModels(
        QualificationAttributeDefinitionResource[][] models) {
        QualificationAttributeDefinitionResourceSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new QualificationAttributeDefinitionResourceSoap[models.length][models[0].length];
        } else {
            soapModels = new QualificationAttributeDefinitionResourceSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static QualificationAttributeDefinitionResourceSoap[] toSoapModels(
        List<QualificationAttributeDefinitionResource> models) {
        List<QualificationAttributeDefinitionResourceSoap> soapModels = new ArrayList<QualificationAttributeDefinitionResourceSoap>(models.size());

        for (QualificationAttributeDefinitionResource model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new QualificationAttributeDefinitionResourceSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _qualificationAttributeDefinitionResourceId;
    }

    public void setPrimaryKey(long pk) {
        setQualificationAttributeDefinitionResourceId(pk);
    }

    public long getQualificationAttributeDefinitionResourceId() {
        return _qualificationAttributeDefinitionResourceId;
    }

    public void setQualificationAttributeDefinitionResourceId(
        long qualificationAttributeDefinitionResourceId) {
        _qualificationAttributeDefinitionResourceId = qualificationAttributeDefinitionResourceId;
    }

    public long getQualificationAttributeDefinitionId() {
        return _qualificationAttributeDefinitionId;
    }

    public void setQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId) {
        _qualificationAttributeDefinitionId = qualificationAttributeDefinitionId;
    }

    public String getCountry() {
        return _country;
    }

    public void setCountry(String country) {
        _country = country;
    }

    public String getLanguage() {
        return _language;
    }

    public void setLanguage(String language) {
        _language = language;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        _title = title;
    }

    public String getUnit() {
        return _unit;
    }

    public void setUnit(String unit) {
        _unit = unit;
    }
}
