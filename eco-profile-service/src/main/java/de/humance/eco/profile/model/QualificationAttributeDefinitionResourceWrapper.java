package de.humance.eco.profile.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link QualificationAttributeDefinitionResource}.
 * </p>
 *
 * @author Humance
 * @see QualificationAttributeDefinitionResource
 * @generated
 */
public class QualificationAttributeDefinitionResourceWrapper
    implements QualificationAttributeDefinitionResource,
        ModelWrapper<QualificationAttributeDefinitionResource> {
    private QualificationAttributeDefinitionResource _qualificationAttributeDefinitionResource;

    public QualificationAttributeDefinitionResourceWrapper(
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource) {
        _qualificationAttributeDefinitionResource = qualificationAttributeDefinitionResource;
    }

    @Override
    public Class<?> getModelClass() {
        return QualificationAttributeDefinitionResource.class;
    }

    @Override
    public String getModelClassName() {
        return QualificationAttributeDefinitionResource.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("qualificationAttributeDefinitionResourceId",
            getQualificationAttributeDefinitionResourceId());
        attributes.put("qualificationAttributeDefinitionId",
            getQualificationAttributeDefinitionId());
        attributes.put("country", getCountry());
        attributes.put("language", getLanguage());
        attributes.put("title", getTitle());
        attributes.put("unit", getUnit());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long qualificationAttributeDefinitionResourceId = (Long) attributes.get(
                "qualificationAttributeDefinitionResourceId");

        if (qualificationAttributeDefinitionResourceId != null) {
            setQualificationAttributeDefinitionResourceId(qualificationAttributeDefinitionResourceId);
        }

        Long qualificationAttributeDefinitionId = (Long) attributes.get(
                "qualificationAttributeDefinitionId");

        if (qualificationAttributeDefinitionId != null) {
            setQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);
        }

        String country = (String) attributes.get("country");

        if (country != null) {
            setCountry(country);
        }

        String language = (String) attributes.get("language");

        if (language != null) {
            setLanguage(language);
        }

        String title = (String) attributes.get("title");

        if (title != null) {
            setTitle(title);
        }

        String unit = (String) attributes.get("unit");

        if (unit != null) {
            setUnit(unit);
        }
    }

    /**
    * Returns the primary key of this qualification attribute definition resource.
    *
    * @return the primary key of this qualification attribute definition resource
    */
    @Override
    public long getPrimaryKey() {
        return _qualificationAttributeDefinitionResource.getPrimaryKey();
    }

    /**
    * Sets the primary key of this qualification attribute definition resource.
    *
    * @param primaryKey the primary key of this qualification attribute definition resource
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _qualificationAttributeDefinitionResource.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the qualification attribute definition resource ID of this qualification attribute definition resource.
    *
    * @return the qualification attribute definition resource ID of this qualification attribute definition resource
    */
    @Override
    public long getQualificationAttributeDefinitionResourceId() {
        return _qualificationAttributeDefinitionResource.getQualificationAttributeDefinitionResourceId();
    }

    /**
    * Sets the qualification attribute definition resource ID of this qualification attribute definition resource.
    *
    * @param qualificationAttributeDefinitionResourceId the qualification attribute definition resource ID of this qualification attribute definition resource
    */
    @Override
    public void setQualificationAttributeDefinitionResourceId(
        long qualificationAttributeDefinitionResourceId) {
        _qualificationAttributeDefinitionResource.setQualificationAttributeDefinitionResourceId(qualificationAttributeDefinitionResourceId);
    }

    /**
    * Returns the qualification attribute definition ID of this qualification attribute definition resource.
    *
    * @return the qualification attribute definition ID of this qualification attribute definition resource
    */
    @Override
    public long getQualificationAttributeDefinitionId() {
        return _qualificationAttributeDefinitionResource.getQualificationAttributeDefinitionId();
    }

    /**
    * Sets the qualification attribute definition ID of this qualification attribute definition resource.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID of this qualification attribute definition resource
    */
    @Override
    public void setQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId) {
        _qualificationAttributeDefinitionResource.setQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);
    }

    /**
    * Returns the country of this qualification attribute definition resource.
    *
    * @return the country of this qualification attribute definition resource
    */
    @Override
    public java.lang.String getCountry() {
        return _qualificationAttributeDefinitionResource.getCountry();
    }

    /**
    * Sets the country of this qualification attribute definition resource.
    *
    * @param country the country of this qualification attribute definition resource
    */
    @Override
    public void setCountry(java.lang.String country) {
        _qualificationAttributeDefinitionResource.setCountry(country);
    }

    /**
    * Returns the language of this qualification attribute definition resource.
    *
    * @return the language of this qualification attribute definition resource
    */
    @Override
    public java.lang.String getLanguage() {
        return _qualificationAttributeDefinitionResource.getLanguage();
    }

    /**
    * Sets the language of this qualification attribute definition resource.
    *
    * @param language the language of this qualification attribute definition resource
    */
    @Override
    public void setLanguage(java.lang.String language) {
        _qualificationAttributeDefinitionResource.setLanguage(language);
    }

    /**
    * Returns the title of this qualification attribute definition resource.
    *
    * @return the title of this qualification attribute definition resource
    */
    @Override
    public java.lang.String getTitle() {
        return _qualificationAttributeDefinitionResource.getTitle();
    }

    /**
    * Sets the title of this qualification attribute definition resource.
    *
    * @param title the title of this qualification attribute definition resource
    */
    @Override
    public void setTitle(java.lang.String title) {
        _qualificationAttributeDefinitionResource.setTitle(title);
    }

    /**
    * Returns the unit of this qualification attribute definition resource.
    *
    * @return the unit of this qualification attribute definition resource
    */
    @Override
    public java.lang.String getUnit() {
        return _qualificationAttributeDefinitionResource.getUnit();
    }

    /**
    * Sets the unit of this qualification attribute definition resource.
    *
    * @param unit the unit of this qualification attribute definition resource
    */
    @Override
    public void setUnit(java.lang.String unit) {
        _qualificationAttributeDefinitionResource.setUnit(unit);
    }

    @Override
    public boolean isNew() {
        return _qualificationAttributeDefinitionResource.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _qualificationAttributeDefinitionResource.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _qualificationAttributeDefinitionResource.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _qualificationAttributeDefinitionResource.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _qualificationAttributeDefinitionResource.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _qualificationAttributeDefinitionResource.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _qualificationAttributeDefinitionResource.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _qualificationAttributeDefinitionResource.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _qualificationAttributeDefinitionResource.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _qualificationAttributeDefinitionResource.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _qualificationAttributeDefinitionResource.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new QualificationAttributeDefinitionResourceWrapper((QualificationAttributeDefinitionResource) _qualificationAttributeDefinitionResource.clone());
    }

    @Override
    public int compareTo(
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource) {
        return _qualificationAttributeDefinitionResource.compareTo(qualificationAttributeDefinitionResource);
    }

    @Override
    public int hashCode() {
        return _qualificationAttributeDefinitionResource.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<QualificationAttributeDefinitionResource> toCacheModel() {
        return _qualificationAttributeDefinitionResource.toCacheModel();
    }

    @Override
    public QualificationAttributeDefinitionResource toEscapedModel() {
        return new QualificationAttributeDefinitionResourceWrapper(_qualificationAttributeDefinitionResource.toEscapedModel());
    }

    @Override
    public QualificationAttributeDefinitionResource toUnescapedModel() {
        return new QualificationAttributeDefinitionResourceWrapper(_qualificationAttributeDefinitionResource.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _qualificationAttributeDefinitionResource.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _qualificationAttributeDefinitionResource.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _qualificationAttributeDefinitionResource.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof QualificationAttributeDefinitionResourceWrapper)) {
            return false;
        }

        QualificationAttributeDefinitionResourceWrapper qualificationAttributeDefinitionResourceWrapper =
            (QualificationAttributeDefinitionResourceWrapper) obj;

        if (Validator.equals(_qualificationAttributeDefinitionResource,
                    qualificationAttributeDefinitionResourceWrapper._qualificationAttributeDefinitionResource)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public QualificationAttributeDefinitionResource getWrappedQualificationAttributeDefinitionResource() {
        return _qualificationAttributeDefinitionResource;
    }

    @Override
    public QualificationAttributeDefinitionResource getWrappedModel() {
        return _qualificationAttributeDefinitionResource;
    }

    @Override
    public void resetOriginalValues() {
        _qualificationAttributeDefinitionResource.resetOriginalValues();
    }
}
