package de.humance.eco.profile.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Humance
 * @generated
 */
public class QualificationAttributeDefinitionSoap implements Serializable {
    private long _qualificationAttributeDefinitionId;
    private long _creatorId;
    private String _creatorName;
    private Date _createDate;
    private long _modifierId;
    private String _modifierName;
    private Date _modifiedDate;
    private long _qualificationTypeId;
    private String _type;
    private String _title;
    private double _minRangeValue;
    private double _maxRangeValue;
    private String _unit;
    private long _sequenceNumber;

    public QualificationAttributeDefinitionSoap() {
    }

    public static QualificationAttributeDefinitionSoap toSoapModel(
        QualificationAttributeDefinition model) {
        QualificationAttributeDefinitionSoap soapModel = new QualificationAttributeDefinitionSoap();

        soapModel.setQualificationAttributeDefinitionId(model.getQualificationAttributeDefinitionId());
        soapModel.setCreatorId(model.getCreatorId());
        soapModel.setCreatorName(model.getCreatorName());
        soapModel.setCreateDate(model.getCreateDate());
        soapModel.setModifierId(model.getModifierId());
        soapModel.setModifierName(model.getModifierName());
        soapModel.setModifiedDate(model.getModifiedDate());
        soapModel.setQualificationTypeId(model.getQualificationTypeId());
        soapModel.setType(model.getType());
        soapModel.setTitle(model.getTitle());
        soapModel.setMinRangeValue(model.getMinRangeValue());
        soapModel.setMaxRangeValue(model.getMaxRangeValue());
        soapModel.setUnit(model.getUnit());
        soapModel.setSequenceNumber(model.getSequenceNumber());

        return soapModel;
    }

    public static QualificationAttributeDefinitionSoap[] toSoapModels(
        QualificationAttributeDefinition[] models) {
        QualificationAttributeDefinitionSoap[] soapModels = new QualificationAttributeDefinitionSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static QualificationAttributeDefinitionSoap[][] toSoapModels(
        QualificationAttributeDefinition[][] models) {
        QualificationAttributeDefinitionSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new QualificationAttributeDefinitionSoap[models.length][models[0].length];
        } else {
            soapModels = new QualificationAttributeDefinitionSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static QualificationAttributeDefinitionSoap[] toSoapModels(
        List<QualificationAttributeDefinition> models) {
        List<QualificationAttributeDefinitionSoap> soapModels = new ArrayList<QualificationAttributeDefinitionSoap>(models.size());

        for (QualificationAttributeDefinition model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new QualificationAttributeDefinitionSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _qualificationAttributeDefinitionId;
    }

    public void setPrimaryKey(long pk) {
        setQualificationAttributeDefinitionId(pk);
    }

    public long getQualificationAttributeDefinitionId() {
        return _qualificationAttributeDefinitionId;
    }

    public void setQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId) {
        _qualificationAttributeDefinitionId = qualificationAttributeDefinitionId;
    }

    public long getCreatorId() {
        return _creatorId;
    }

    public void setCreatorId(long creatorId) {
        _creatorId = creatorId;
    }

    public String getCreatorName() {
        return _creatorName;
    }

    public void setCreatorName(String creatorName) {
        _creatorName = creatorName;
    }

    public Date getCreateDate() {
        return _createDate;
    }

    public void setCreateDate(Date createDate) {
        _createDate = createDate;
    }

    public long getModifierId() {
        return _modifierId;
    }

    public void setModifierId(long modifierId) {
        _modifierId = modifierId;
    }

    public String getModifierName() {
        return _modifierName;
    }

    public void setModifierName(String modifierName) {
        _modifierName = modifierName;
    }

    public Date getModifiedDate() {
        return _modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;
    }

    public long getQualificationTypeId() {
        return _qualificationTypeId;
    }

    public void setQualificationTypeId(long qualificationTypeId) {
        _qualificationTypeId = qualificationTypeId;
    }

    public String getType() {
        return _type;
    }

    public void setType(String type) {
        _type = type;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        _title = title;
    }

    public double getMinRangeValue() {
        return _minRangeValue;
    }

    public void setMinRangeValue(double minRangeValue) {
        _minRangeValue = minRangeValue;
    }

    public double getMaxRangeValue() {
        return _maxRangeValue;
    }

    public void setMaxRangeValue(double maxRangeValue) {
        _maxRangeValue = maxRangeValue;
    }

    public String getUnit() {
        return _unit;
    }

    public void setUnit(String unit) {
        _unit = unit;
    }

    public long getSequenceNumber() {
        return _sequenceNumber;
    }

    public void setSequenceNumber(long sequenceNumber) {
        _sequenceNumber = sequenceNumber;
    }
}
