package de.humance.eco.profile.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link QualificationAttributeDefinition}.
 * </p>
 *
 * @author Humance
 * @see QualificationAttributeDefinition
 * @generated
 */
public class QualificationAttributeDefinitionWrapper
    implements QualificationAttributeDefinition,
        ModelWrapper<QualificationAttributeDefinition> {
    private QualificationAttributeDefinition _qualificationAttributeDefinition;

    public QualificationAttributeDefinitionWrapper(
        QualificationAttributeDefinition qualificationAttributeDefinition) {
        _qualificationAttributeDefinition = qualificationAttributeDefinition;
    }

    @Override
    public Class<?> getModelClass() {
        return QualificationAttributeDefinition.class;
    }

    @Override
    public String getModelClassName() {
        return QualificationAttributeDefinition.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("qualificationAttributeDefinitionId",
            getQualificationAttributeDefinitionId());
        attributes.put("creatorId", getCreatorId());
        attributes.put("creatorName", getCreatorName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifierId", getModifierId());
        attributes.put("modifierName", getModifierName());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("qualificationTypeId", getQualificationTypeId());
        attributes.put("type", getType());
        attributes.put("title", getTitle());
        attributes.put("minRangeValue", getMinRangeValue());
        attributes.put("maxRangeValue", getMaxRangeValue());
        attributes.put("unit", getUnit());
        attributes.put("sequenceNumber", getSequenceNumber());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long qualificationAttributeDefinitionId = (Long) attributes.get(
                "qualificationAttributeDefinitionId");

        if (qualificationAttributeDefinitionId != null) {
            setQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);
        }

        Long creatorId = (Long) attributes.get("creatorId");

        if (creatorId != null) {
            setCreatorId(creatorId);
        }

        String creatorName = (String) attributes.get("creatorName");

        if (creatorName != null) {
            setCreatorName(creatorName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Long modifierId = (Long) attributes.get("modifierId");

        if (modifierId != null) {
            setModifierId(modifierId);
        }

        String modifierName = (String) attributes.get("modifierName");

        if (modifierName != null) {
            setModifierName(modifierName);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        Long qualificationTypeId = (Long) attributes.get("qualificationTypeId");

        if (qualificationTypeId != null) {
            setQualificationTypeId(qualificationTypeId);
        }

        String type = (String) attributes.get("type");

        if (type != null) {
            setType(type);
        }

        String title = (String) attributes.get("title");

        if (title != null) {
            setTitle(title);
        }

        Double minRangeValue = (Double) attributes.get("minRangeValue");

        if (minRangeValue != null) {
            setMinRangeValue(minRangeValue);
        }

        Double maxRangeValue = (Double) attributes.get("maxRangeValue");

        if (maxRangeValue != null) {
            setMaxRangeValue(maxRangeValue);
        }

        String unit = (String) attributes.get("unit");

        if (unit != null) {
            setUnit(unit);
        }

        Long sequenceNumber = (Long) attributes.get("sequenceNumber");

        if (sequenceNumber != null) {
            setSequenceNumber(sequenceNumber);
        }
    }

    /**
    * Returns the primary key of this qualification attribute definition.
    *
    * @return the primary key of this qualification attribute definition
    */
    @Override
    public long getPrimaryKey() {
        return _qualificationAttributeDefinition.getPrimaryKey();
    }

    /**
    * Sets the primary key of this qualification attribute definition.
    *
    * @param primaryKey the primary key of this qualification attribute definition
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _qualificationAttributeDefinition.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the qualification attribute definition ID of this qualification attribute definition.
    *
    * @return the qualification attribute definition ID of this qualification attribute definition
    */
    @Override
    public long getQualificationAttributeDefinitionId() {
        return _qualificationAttributeDefinition.getQualificationAttributeDefinitionId();
    }

    /**
    * Sets the qualification attribute definition ID of this qualification attribute definition.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID of this qualification attribute definition
    */
    @Override
    public void setQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId) {
        _qualificationAttributeDefinition.setQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);
    }

    /**
    * Returns the creator ID of this qualification attribute definition.
    *
    * @return the creator ID of this qualification attribute definition
    */
    @Override
    public long getCreatorId() {
        return _qualificationAttributeDefinition.getCreatorId();
    }

    /**
    * Sets the creator ID of this qualification attribute definition.
    *
    * @param creatorId the creator ID of this qualification attribute definition
    */
    @Override
    public void setCreatorId(long creatorId) {
        _qualificationAttributeDefinition.setCreatorId(creatorId);
    }

    /**
    * Returns the creator name of this qualification attribute definition.
    *
    * @return the creator name of this qualification attribute definition
    */
    @Override
    public java.lang.String getCreatorName() {
        return _qualificationAttributeDefinition.getCreatorName();
    }

    /**
    * Sets the creator name of this qualification attribute definition.
    *
    * @param creatorName the creator name of this qualification attribute definition
    */
    @Override
    public void setCreatorName(java.lang.String creatorName) {
        _qualificationAttributeDefinition.setCreatorName(creatorName);
    }

    /**
    * Returns the create date of this qualification attribute definition.
    *
    * @return the create date of this qualification attribute definition
    */
    @Override
    public java.util.Date getCreateDate() {
        return _qualificationAttributeDefinition.getCreateDate();
    }

    /**
    * Sets the create date of this qualification attribute definition.
    *
    * @param createDate the create date of this qualification attribute definition
    */
    @Override
    public void setCreateDate(java.util.Date createDate) {
        _qualificationAttributeDefinition.setCreateDate(createDate);
    }

    /**
    * Returns the modifier ID of this qualification attribute definition.
    *
    * @return the modifier ID of this qualification attribute definition
    */
    @Override
    public long getModifierId() {
        return _qualificationAttributeDefinition.getModifierId();
    }

    /**
    * Sets the modifier ID of this qualification attribute definition.
    *
    * @param modifierId the modifier ID of this qualification attribute definition
    */
    @Override
    public void setModifierId(long modifierId) {
        _qualificationAttributeDefinition.setModifierId(modifierId);
    }

    /**
    * Returns the modifier name of this qualification attribute definition.
    *
    * @return the modifier name of this qualification attribute definition
    */
    @Override
    public java.lang.String getModifierName() {
        return _qualificationAttributeDefinition.getModifierName();
    }

    /**
    * Sets the modifier name of this qualification attribute definition.
    *
    * @param modifierName the modifier name of this qualification attribute definition
    */
    @Override
    public void setModifierName(java.lang.String modifierName) {
        _qualificationAttributeDefinition.setModifierName(modifierName);
    }

    /**
    * Returns the modified date of this qualification attribute definition.
    *
    * @return the modified date of this qualification attribute definition
    */
    @Override
    public java.util.Date getModifiedDate() {
        return _qualificationAttributeDefinition.getModifiedDate();
    }

    /**
    * Sets the modified date of this qualification attribute definition.
    *
    * @param modifiedDate the modified date of this qualification attribute definition
    */
    @Override
    public void setModifiedDate(java.util.Date modifiedDate) {
        _qualificationAttributeDefinition.setModifiedDate(modifiedDate);
    }

    /**
    * Returns the qualification type ID of this qualification attribute definition.
    *
    * @return the qualification type ID of this qualification attribute definition
    */
    @Override
    public long getQualificationTypeId() {
        return _qualificationAttributeDefinition.getQualificationTypeId();
    }

    /**
    * Sets the qualification type ID of this qualification attribute definition.
    *
    * @param qualificationTypeId the qualification type ID of this qualification attribute definition
    */
    @Override
    public void setQualificationTypeId(long qualificationTypeId) {
        _qualificationAttributeDefinition.setQualificationTypeId(qualificationTypeId);
    }

    /**
    * Returns the type of this qualification attribute definition.
    *
    * @return the type of this qualification attribute definition
    */
    @Override
    public java.lang.String getType() {
        return _qualificationAttributeDefinition.getType();
    }

    /**
    * Sets the type of this qualification attribute definition.
    *
    * @param type the type of this qualification attribute definition
    */
    @Override
    public void setType(java.lang.String type) {
        _qualificationAttributeDefinition.setType(type);
    }

    /**
    * Returns the title of this qualification attribute definition.
    *
    * @return the title of this qualification attribute definition
    */
    @Override
    public java.lang.String getTitle() {
        return _qualificationAttributeDefinition.getTitle();
    }

    /**
    * Sets the title of this qualification attribute definition.
    *
    * @param title the title of this qualification attribute definition
    */
    @Override
    public void setTitle(java.lang.String title) {
        _qualificationAttributeDefinition.setTitle(title);
    }

    /**
    * Returns the min range value of this qualification attribute definition.
    *
    * @return the min range value of this qualification attribute definition
    */
    @Override
    public double getMinRangeValue() {
        return _qualificationAttributeDefinition.getMinRangeValue();
    }

    /**
    * Sets the min range value of this qualification attribute definition.
    *
    * @param minRangeValue the min range value of this qualification attribute definition
    */
    @Override
    public void setMinRangeValue(double minRangeValue) {
        _qualificationAttributeDefinition.setMinRangeValue(minRangeValue);
    }

    /**
    * Returns the max range value of this qualification attribute definition.
    *
    * @return the max range value of this qualification attribute definition
    */
    @Override
    public double getMaxRangeValue() {
        return _qualificationAttributeDefinition.getMaxRangeValue();
    }

    /**
    * Sets the max range value of this qualification attribute definition.
    *
    * @param maxRangeValue the max range value of this qualification attribute definition
    */
    @Override
    public void setMaxRangeValue(double maxRangeValue) {
        _qualificationAttributeDefinition.setMaxRangeValue(maxRangeValue);
    }

    /**
    * Returns the unit of this qualification attribute definition.
    *
    * @return the unit of this qualification attribute definition
    */
    @Override
    public java.lang.String getUnit() {
        return _qualificationAttributeDefinition.getUnit();
    }

    /**
    * Sets the unit of this qualification attribute definition.
    *
    * @param unit the unit of this qualification attribute definition
    */
    @Override
    public void setUnit(java.lang.String unit) {
        _qualificationAttributeDefinition.setUnit(unit);
    }

    /**
    * Returns the sequence number of this qualification attribute definition.
    *
    * @return the sequence number of this qualification attribute definition
    */
    @Override
    public long getSequenceNumber() {
        return _qualificationAttributeDefinition.getSequenceNumber();
    }

    /**
    * Sets the sequence number of this qualification attribute definition.
    *
    * @param sequenceNumber the sequence number of this qualification attribute definition
    */
    @Override
    public void setSequenceNumber(long sequenceNumber) {
        _qualificationAttributeDefinition.setSequenceNumber(sequenceNumber);
    }

    @Override
    public boolean isNew() {
        return _qualificationAttributeDefinition.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _qualificationAttributeDefinition.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _qualificationAttributeDefinition.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _qualificationAttributeDefinition.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _qualificationAttributeDefinition.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _qualificationAttributeDefinition.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _qualificationAttributeDefinition.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _qualificationAttributeDefinition.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _qualificationAttributeDefinition.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _qualificationAttributeDefinition.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _qualificationAttributeDefinition.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new QualificationAttributeDefinitionWrapper((QualificationAttributeDefinition) _qualificationAttributeDefinition.clone());
    }

    @Override
    public int compareTo(
        QualificationAttributeDefinition qualificationAttributeDefinition) {
        return _qualificationAttributeDefinition.compareTo(qualificationAttributeDefinition);
    }

    @Override
    public int hashCode() {
        return _qualificationAttributeDefinition.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<QualificationAttributeDefinition> toCacheModel() {
        return _qualificationAttributeDefinition.toCacheModel();
    }

    @Override
    public QualificationAttributeDefinition toEscapedModel() {
        return new QualificationAttributeDefinitionWrapper(_qualificationAttributeDefinition.toEscapedModel());
    }

    @Override
    public QualificationAttributeDefinition toUnescapedModel() {
        return new QualificationAttributeDefinitionWrapper(_qualificationAttributeDefinition.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _qualificationAttributeDefinition.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _qualificationAttributeDefinition.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _qualificationAttributeDefinition.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof QualificationAttributeDefinitionWrapper)) {
            return false;
        }

        QualificationAttributeDefinitionWrapper qualificationAttributeDefinitionWrapper =
            (QualificationAttributeDefinitionWrapper) obj;

        if (Validator.equals(_qualificationAttributeDefinition,
                    qualificationAttributeDefinitionWrapper._qualificationAttributeDefinition)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public QualificationAttributeDefinition getWrappedQualificationAttributeDefinition() {
        return _qualificationAttributeDefinition;
    }

    @Override
    public QualificationAttributeDefinition getWrappedModel() {
        return _qualificationAttributeDefinition;
    }

    @Override
    public void resetOriginalValues() {
        _qualificationAttributeDefinition.resetOriginalValues();
    }
}
