package de.humance.eco.profile.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Humance
 * @generated
 */
public class QualificationAttributeSoap implements Serializable {
    private long _qualificationAttributeId;
    private long _qualificationId;
    private long _qualificationAttributeDefinitionId;
    private String _type;
    private long _sequenceNumber;
    private String _attributeValue;

    public QualificationAttributeSoap() {
    }

    public static QualificationAttributeSoap toSoapModel(
        QualificationAttribute model) {
        QualificationAttributeSoap soapModel = new QualificationAttributeSoap();

        soapModel.setQualificationAttributeId(model.getQualificationAttributeId());
        soapModel.setQualificationId(model.getQualificationId());
        soapModel.setQualificationAttributeDefinitionId(model.getQualificationAttributeDefinitionId());
        soapModel.setType(model.getType());
        soapModel.setSequenceNumber(model.getSequenceNumber());
        soapModel.setAttributeValue(model.getAttributeValue());

        return soapModel;
    }

    public static QualificationAttributeSoap[] toSoapModels(
        QualificationAttribute[] models) {
        QualificationAttributeSoap[] soapModels = new QualificationAttributeSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static QualificationAttributeSoap[][] toSoapModels(
        QualificationAttribute[][] models) {
        QualificationAttributeSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new QualificationAttributeSoap[models.length][models[0].length];
        } else {
            soapModels = new QualificationAttributeSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static QualificationAttributeSoap[] toSoapModels(
        List<QualificationAttribute> models) {
        List<QualificationAttributeSoap> soapModels = new ArrayList<QualificationAttributeSoap>(models.size());

        for (QualificationAttribute model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new QualificationAttributeSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _qualificationAttributeId;
    }

    public void setPrimaryKey(long pk) {
        setQualificationAttributeId(pk);
    }

    public long getQualificationAttributeId() {
        return _qualificationAttributeId;
    }

    public void setQualificationAttributeId(long qualificationAttributeId) {
        _qualificationAttributeId = qualificationAttributeId;
    }

    public long getQualificationId() {
        return _qualificationId;
    }

    public void setQualificationId(long qualificationId) {
        _qualificationId = qualificationId;
    }

    public long getQualificationAttributeDefinitionId() {
        return _qualificationAttributeDefinitionId;
    }

    public void setQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId) {
        _qualificationAttributeDefinitionId = qualificationAttributeDefinitionId;
    }

    public String getType() {
        return _type;
    }

    public void setType(String type) {
        _type = type;
    }

    public long getSequenceNumber() {
        return _sequenceNumber;
    }

    public void setSequenceNumber(long sequenceNumber) {
        _sequenceNumber = sequenceNumber;
    }

    public String getAttributeValue() {
        return _attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        _attributeValue = attributeValue;
    }
}
