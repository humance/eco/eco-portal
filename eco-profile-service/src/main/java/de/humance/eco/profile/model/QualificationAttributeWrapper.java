package de.humance.eco.profile.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link QualificationAttribute}.
 * </p>
 *
 * @author Humance
 * @see QualificationAttribute
 * @generated
 */
public class QualificationAttributeWrapper implements QualificationAttribute,
    ModelWrapper<QualificationAttribute> {
    private QualificationAttribute _qualificationAttribute;

    public QualificationAttributeWrapper(
        QualificationAttribute qualificationAttribute) {
        _qualificationAttribute = qualificationAttribute;
    }

    @Override
    public Class<?> getModelClass() {
        return QualificationAttribute.class;
    }

    @Override
    public String getModelClassName() {
        return QualificationAttribute.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("qualificationAttributeId", getQualificationAttributeId());
        attributes.put("qualificationId", getQualificationId());
        attributes.put("qualificationAttributeDefinitionId",
            getQualificationAttributeDefinitionId());
        attributes.put("type", getType());
        attributes.put("sequenceNumber", getSequenceNumber());
        attributes.put("attributeValue", getAttributeValue());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long qualificationAttributeId = (Long) attributes.get(
                "qualificationAttributeId");

        if (qualificationAttributeId != null) {
            setQualificationAttributeId(qualificationAttributeId);
        }

        Long qualificationId = (Long) attributes.get("qualificationId");

        if (qualificationId != null) {
            setQualificationId(qualificationId);
        }

        Long qualificationAttributeDefinitionId = (Long) attributes.get(
                "qualificationAttributeDefinitionId");

        if (qualificationAttributeDefinitionId != null) {
            setQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);
        }

        String type = (String) attributes.get("type");

        if (type != null) {
            setType(type);
        }

        Long sequenceNumber = (Long) attributes.get("sequenceNumber");

        if (sequenceNumber != null) {
            setSequenceNumber(sequenceNumber);
        }

        String attributeValue = (String) attributes.get("attributeValue");

        if (attributeValue != null) {
            setAttributeValue(attributeValue);
        }
    }

    /**
    * Returns the primary key of this qualification attribute.
    *
    * @return the primary key of this qualification attribute
    */
    @Override
    public long getPrimaryKey() {
        return _qualificationAttribute.getPrimaryKey();
    }

    /**
    * Sets the primary key of this qualification attribute.
    *
    * @param primaryKey the primary key of this qualification attribute
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _qualificationAttribute.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the qualification attribute ID of this qualification attribute.
    *
    * @return the qualification attribute ID of this qualification attribute
    */
    @Override
    public long getQualificationAttributeId() {
        return _qualificationAttribute.getQualificationAttributeId();
    }

    /**
    * Sets the qualification attribute ID of this qualification attribute.
    *
    * @param qualificationAttributeId the qualification attribute ID of this qualification attribute
    */
    @Override
    public void setQualificationAttributeId(long qualificationAttributeId) {
        _qualificationAttribute.setQualificationAttributeId(qualificationAttributeId);
    }

    /**
    * Returns the qualification ID of this qualification attribute.
    *
    * @return the qualification ID of this qualification attribute
    */
    @Override
    public long getQualificationId() {
        return _qualificationAttribute.getQualificationId();
    }

    /**
    * Sets the qualification ID of this qualification attribute.
    *
    * @param qualificationId the qualification ID of this qualification attribute
    */
    @Override
    public void setQualificationId(long qualificationId) {
        _qualificationAttribute.setQualificationId(qualificationId);
    }

    /**
    * Returns the qualification attribute definition ID of this qualification attribute.
    *
    * @return the qualification attribute definition ID of this qualification attribute
    */
    @Override
    public long getQualificationAttributeDefinitionId() {
        return _qualificationAttribute.getQualificationAttributeDefinitionId();
    }

    /**
    * Sets the qualification attribute definition ID of this qualification attribute.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID of this qualification attribute
    */
    @Override
    public void setQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId) {
        _qualificationAttribute.setQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);
    }

    /**
    * Returns the type of this qualification attribute.
    *
    * @return the type of this qualification attribute
    */
    @Override
    public java.lang.String getType() {
        return _qualificationAttribute.getType();
    }

    /**
    * Sets the type of this qualification attribute.
    *
    * @param type the type of this qualification attribute
    */
    @Override
    public void setType(java.lang.String type) {
        _qualificationAttribute.setType(type);
    }

    /**
    * Returns the sequence number of this qualification attribute.
    *
    * @return the sequence number of this qualification attribute
    */
    @Override
    public long getSequenceNumber() {
        return _qualificationAttribute.getSequenceNumber();
    }

    /**
    * Sets the sequence number of this qualification attribute.
    *
    * @param sequenceNumber the sequence number of this qualification attribute
    */
    @Override
    public void setSequenceNumber(long sequenceNumber) {
        _qualificationAttribute.setSequenceNumber(sequenceNumber);
    }

    /**
    * Returns the attribute value of this qualification attribute.
    *
    * @return the attribute value of this qualification attribute
    */
    @Override
    public java.lang.String getAttributeValue() {
        return _qualificationAttribute.getAttributeValue();
    }

    /**
    * Sets the attribute value of this qualification attribute.
    *
    * @param attributeValue the attribute value of this qualification attribute
    */
    @Override
    public void setAttributeValue(java.lang.String attributeValue) {
        _qualificationAttribute.setAttributeValue(attributeValue);
    }

    @Override
    public boolean isNew() {
        return _qualificationAttribute.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _qualificationAttribute.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _qualificationAttribute.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _qualificationAttribute.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _qualificationAttribute.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _qualificationAttribute.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _qualificationAttribute.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _qualificationAttribute.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _qualificationAttribute.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _qualificationAttribute.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _qualificationAttribute.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new QualificationAttributeWrapper((QualificationAttribute) _qualificationAttribute.clone());
    }

    @Override
    public int compareTo(QualificationAttribute qualificationAttribute) {
        return _qualificationAttribute.compareTo(qualificationAttribute);
    }

    @Override
    public int hashCode() {
        return _qualificationAttribute.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<QualificationAttribute> toCacheModel() {
        return _qualificationAttribute.toCacheModel();
    }

    @Override
    public QualificationAttribute toEscapedModel() {
        return new QualificationAttributeWrapper(_qualificationAttribute.toEscapedModel());
    }

    @Override
    public QualificationAttribute toUnescapedModel() {
        return new QualificationAttributeWrapper(_qualificationAttribute.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _qualificationAttribute.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _qualificationAttribute.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _qualificationAttribute.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof QualificationAttributeWrapper)) {
            return false;
        }

        QualificationAttributeWrapper qualificationAttributeWrapper = (QualificationAttributeWrapper) obj;

        if (Validator.equals(_qualificationAttribute,
                    qualificationAttributeWrapper._qualificationAttribute)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public QualificationAttribute getWrappedQualificationAttribute() {
        return _qualificationAttribute;
    }

    @Override
    public QualificationAttribute getWrappedModel() {
        return _qualificationAttribute;
    }

    @Override
    public void resetOriginalValues() {
        _qualificationAttribute.resetOriginalValues();
    }
}
