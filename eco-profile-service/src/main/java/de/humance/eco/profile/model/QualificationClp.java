package de.humance.eco.profile.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import de.humance.eco.profile.service.ClpSerializer;
import de.humance.eco.profile.service.QualificationLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class QualificationClp extends BaseModelImpl<Qualification>
    implements Qualification {
    private long _qualificationId;
    private long _creatorId;
    private String _creatorName;
    private Date _createDate;
    private long _modifierId;
    private String _modifierName;
    private Date _modifiedDate;
    private long _ownerId;
    private long _typeId;
    private String _name;
    private String _certNumber;
    private Date _deliveryDate;
    private Date _expirationDate;
    private String _issuingCountry;
    private String _issuingInstitution;
    private BaseModel<?> _qualificationRemoteModel;

    public QualificationClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Qualification.class;
    }

    @Override
    public String getModelClassName() {
        return Qualification.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _qualificationId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setQualificationId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _qualificationId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("qualificationId", getQualificationId());
        attributes.put("creatorId", getCreatorId());
        attributes.put("creatorName", getCreatorName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifierId", getModifierId());
        attributes.put("modifierName", getModifierName());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("ownerId", getOwnerId());
        attributes.put("typeId", getTypeId());
        attributes.put("name", getName());
        attributes.put("certNumber", getCertNumber());
        attributes.put("deliveryDate", getDeliveryDate());
        attributes.put("expirationDate", getExpirationDate());
        attributes.put("issuingCountry", getIssuingCountry());
        attributes.put("issuingInstitution", getIssuingInstitution());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long qualificationId = (Long) attributes.get("qualificationId");

        if (qualificationId != null) {
            setQualificationId(qualificationId);
        }

        Long creatorId = (Long) attributes.get("creatorId");

        if (creatorId != null) {
            setCreatorId(creatorId);
        }

        String creatorName = (String) attributes.get("creatorName");

        if (creatorName != null) {
            setCreatorName(creatorName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Long modifierId = (Long) attributes.get("modifierId");

        if (modifierId != null) {
            setModifierId(modifierId);
        }

        String modifierName = (String) attributes.get("modifierName");

        if (modifierName != null) {
            setModifierName(modifierName);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        Long ownerId = (Long) attributes.get("ownerId");

        if (ownerId != null) {
            setOwnerId(ownerId);
        }

        Long typeId = (Long) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        String certNumber = (String) attributes.get("certNumber");

        if (certNumber != null) {
            setCertNumber(certNumber);
        }

        Date deliveryDate = (Date) attributes.get("deliveryDate");

        if (deliveryDate != null) {
            setDeliveryDate(deliveryDate);
        }

        Date expirationDate = (Date) attributes.get("expirationDate");

        if (expirationDate != null) {
            setExpirationDate(expirationDate);
        }

        String issuingCountry = (String) attributes.get("issuingCountry");

        if (issuingCountry != null) {
            setIssuingCountry(issuingCountry);
        }

        String issuingInstitution = (String) attributes.get(
                "issuingInstitution");

        if (issuingInstitution != null) {
            setIssuingInstitution(issuingInstitution);
        }
    }

    @Override
    public long getQualificationId() {
        return _qualificationId;
    }

    @Override
    public void setQualificationId(long qualificationId) {
        _qualificationId = qualificationId;

        if (_qualificationRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationRemoteModel.getClass();

                Method method = clazz.getMethod("setQualificationId", long.class);

                method.invoke(_qualificationRemoteModel, qualificationId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getCreatorId() {
        return _creatorId;
    }

    @Override
    public void setCreatorId(long creatorId) {
        _creatorId = creatorId;

        if (_qualificationRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationRemoteModel.getClass();

                Method method = clazz.getMethod("setCreatorId", long.class);

                method.invoke(_qualificationRemoteModel, creatorId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCreatorName() {
        return _creatorName;
    }

    @Override
    public void setCreatorName(String creatorName) {
        _creatorName = creatorName;

        if (_qualificationRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationRemoteModel.getClass();

                Method method = clazz.getMethod("setCreatorName", String.class);

                method.invoke(_qualificationRemoteModel, creatorName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getCreateDate() {
        return _createDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        _createDate = createDate;

        if (_qualificationRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationRemoteModel.getClass();

                Method method = clazz.getMethod("setCreateDate", Date.class);

                method.invoke(_qualificationRemoteModel, createDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getModifierId() {
        return _modifierId;
    }

    @Override
    public void setModifierId(long modifierId) {
        _modifierId = modifierId;

        if (_qualificationRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationRemoteModel.getClass();

                Method method = clazz.getMethod("setModifierId", long.class);

                method.invoke(_qualificationRemoteModel, modifierId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getModifierName() {
        return _modifierName;
    }

    @Override
    public void setModifierName(String modifierName) {
        _modifierName = modifierName;

        if (_qualificationRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationRemoteModel.getClass();

                Method method = clazz.getMethod("setModifierName", String.class);

                method.invoke(_qualificationRemoteModel, modifierName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getModifiedDate() {
        return _modifiedDate;
    }

    @Override
    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;

        if (_qualificationRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationRemoteModel.getClass();

                Method method = clazz.getMethod("setModifiedDate", Date.class);

                method.invoke(_qualificationRemoteModel, modifiedDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOwnerId() {
        return _ownerId;
    }

    @Override
    public void setOwnerId(long ownerId) {
        _ownerId = ownerId;

        if (_qualificationRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationRemoteModel.getClass();

                Method method = clazz.getMethod("setOwnerId", long.class);

                method.invoke(_qualificationRemoteModel, ownerId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getTypeId() {
        return _typeId;
    }

    @Override
    public void setTypeId(long typeId) {
        _typeId = typeId;

        if (_qualificationRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationRemoteModel.getClass();

                Method method = clazz.getMethod("setTypeId", long.class);

                method.invoke(_qualificationRemoteModel, typeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getName() {
        return _name;
    }

    @Override
    public void setName(String name) {
        _name = name;

        if (_qualificationRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationRemoteModel.getClass();

                Method method = clazz.getMethod("setName", String.class);

                method.invoke(_qualificationRemoteModel, name);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCertNumber() {
        return _certNumber;
    }

    @Override
    public void setCertNumber(String certNumber) {
        _certNumber = certNumber;

        if (_qualificationRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationRemoteModel.getClass();

                Method method = clazz.getMethod("setCertNumber", String.class);

                method.invoke(_qualificationRemoteModel, certNumber);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getDeliveryDate() {
        return _deliveryDate;
    }

    @Override
    public void setDeliveryDate(Date deliveryDate) {
        _deliveryDate = deliveryDate;

        if (_qualificationRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationRemoteModel.getClass();

                Method method = clazz.getMethod("setDeliveryDate", Date.class);

                method.invoke(_qualificationRemoteModel, deliveryDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getExpirationDate() {
        return _expirationDate;
    }

    @Override
    public void setExpirationDate(Date expirationDate) {
        _expirationDate = expirationDate;

        if (_qualificationRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationRemoteModel.getClass();

                Method method = clazz.getMethod("setExpirationDate", Date.class);

                method.invoke(_qualificationRemoteModel, expirationDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getIssuingCountry() {
        return _issuingCountry;
    }

    @Override
    public void setIssuingCountry(String issuingCountry) {
        _issuingCountry = issuingCountry;

        if (_qualificationRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationRemoteModel.getClass();

                Method method = clazz.getMethod("setIssuingCountry",
                        String.class);

                method.invoke(_qualificationRemoteModel, issuingCountry);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getIssuingInstitution() {
        return _issuingInstitution;
    }

    @Override
    public void setIssuingInstitution(String issuingInstitution) {
        _issuingInstitution = issuingInstitution;

        if (_qualificationRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationRemoteModel.getClass();

                Method method = clazz.getMethod("setIssuingInstitution",
                        String.class);

                method.invoke(_qualificationRemoteModel, issuingInstitution);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getQualificationRemoteModel() {
        return _qualificationRemoteModel;
    }

    public void setQualificationRemoteModel(
        BaseModel<?> qualificationRemoteModel) {
        _qualificationRemoteModel = qualificationRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _qualificationRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_qualificationRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            QualificationLocalServiceUtil.addQualification(this);
        } else {
            QualificationLocalServiceUtil.updateQualification(this);
        }
    }

    @Override
    public Qualification toEscapedModel() {
        return (Qualification) ProxyUtil.newProxyInstance(Qualification.class.getClassLoader(),
            new Class[] { Qualification.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        QualificationClp clone = new QualificationClp();

        clone.setQualificationId(getQualificationId());
        clone.setCreatorId(getCreatorId());
        clone.setCreatorName(getCreatorName());
        clone.setCreateDate(getCreateDate());
        clone.setModifierId(getModifierId());
        clone.setModifierName(getModifierName());
        clone.setModifiedDate(getModifiedDate());
        clone.setOwnerId(getOwnerId());
        clone.setTypeId(getTypeId());
        clone.setName(getName());
        clone.setCertNumber(getCertNumber());
        clone.setDeliveryDate(getDeliveryDate());
        clone.setExpirationDate(getExpirationDate());
        clone.setIssuingCountry(getIssuingCountry());
        clone.setIssuingInstitution(getIssuingInstitution());

        return clone;
    }

    @Override
    public int compareTo(Qualification qualification) {
        int value = 0;

        if (getOwnerId() < qualification.getOwnerId()) {
            value = -1;
        } else if (getOwnerId() > qualification.getOwnerId()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        if (getTypeId() < qualification.getTypeId()) {
            value = -1;
        } else if (getTypeId() > qualification.getTypeId()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        value = DateUtil.compareTo(getDeliveryDate(),
                qualification.getDeliveryDate());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof QualificationClp)) {
            return false;
        }

        QualificationClp qualification = (QualificationClp) obj;

        long primaryKey = qualification.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(31);

        sb.append("{qualificationId=");
        sb.append(getQualificationId());
        sb.append(", creatorId=");
        sb.append(getCreatorId());
        sb.append(", creatorName=");
        sb.append(getCreatorName());
        sb.append(", createDate=");
        sb.append(getCreateDate());
        sb.append(", modifierId=");
        sb.append(getModifierId());
        sb.append(", modifierName=");
        sb.append(getModifierName());
        sb.append(", modifiedDate=");
        sb.append(getModifiedDate());
        sb.append(", ownerId=");
        sb.append(getOwnerId());
        sb.append(", typeId=");
        sb.append(getTypeId());
        sb.append(", name=");
        sb.append(getName());
        sb.append(", certNumber=");
        sb.append(getCertNumber());
        sb.append(", deliveryDate=");
        sb.append(getDeliveryDate());
        sb.append(", expirationDate=");
        sb.append(getExpirationDate());
        sb.append(", issuingCountry=");
        sb.append(getIssuingCountry());
        sb.append(", issuingInstitution=");
        sb.append(getIssuingInstitution());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(49);

        sb.append("<model><model-name>");
        sb.append("de.humance.eco.profile.model.Qualification");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>qualificationId</column-name><column-value><![CDATA[");
        sb.append(getQualificationId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>creatorId</column-name><column-value><![CDATA[");
        sb.append(getCreatorId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>creatorName</column-name><column-value><![CDATA[");
        sb.append(getCreatorName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>createDate</column-name><column-value><![CDATA[");
        sb.append(getCreateDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifierId</column-name><column-value><![CDATA[");
        sb.append(getModifierId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifierName</column-name><column-value><![CDATA[");
        sb.append(getModifierName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
        sb.append(getModifiedDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>ownerId</column-name><column-value><![CDATA[");
        sb.append(getOwnerId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>typeId</column-name><column-value><![CDATA[");
        sb.append(getTypeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>name</column-name><column-value><![CDATA[");
        sb.append(getName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>certNumber</column-name><column-value><![CDATA[");
        sb.append(getCertNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>deliveryDate</column-name><column-value><![CDATA[");
        sb.append(getDeliveryDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>expirationDate</column-name><column-value><![CDATA[");
        sb.append(getExpirationDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>issuingCountry</column-name><column-value><![CDATA[");
        sb.append(getIssuingCountry());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>issuingInstitution</column-name><column-value><![CDATA[");
        sb.append(getIssuingInstitution());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
