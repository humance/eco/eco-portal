package de.humance.eco.profile.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Humance
 * @generated
 */
public class QualificationSoap implements Serializable {
    private long _qualificationId;
    private long _creatorId;
    private String _creatorName;
    private Date _createDate;
    private long _modifierId;
    private String _modifierName;
    private Date _modifiedDate;
    private long _ownerId;
    private long _typeId;
    private String _name;
    private String _certNumber;
    private Date _deliveryDate;
    private Date _expirationDate;
    private String _issuingCountry;
    private String _issuingInstitution;

    public QualificationSoap() {
    }

    public static QualificationSoap toSoapModel(Qualification model) {
        QualificationSoap soapModel = new QualificationSoap();

        soapModel.setQualificationId(model.getQualificationId());
        soapModel.setCreatorId(model.getCreatorId());
        soapModel.setCreatorName(model.getCreatorName());
        soapModel.setCreateDate(model.getCreateDate());
        soapModel.setModifierId(model.getModifierId());
        soapModel.setModifierName(model.getModifierName());
        soapModel.setModifiedDate(model.getModifiedDate());
        soapModel.setOwnerId(model.getOwnerId());
        soapModel.setTypeId(model.getTypeId());
        soapModel.setName(model.getName());
        soapModel.setCertNumber(model.getCertNumber());
        soapModel.setDeliveryDate(model.getDeliveryDate());
        soapModel.setExpirationDate(model.getExpirationDate());
        soapModel.setIssuingCountry(model.getIssuingCountry());
        soapModel.setIssuingInstitution(model.getIssuingInstitution());

        return soapModel;
    }

    public static QualificationSoap[] toSoapModels(Qualification[] models) {
        QualificationSoap[] soapModels = new QualificationSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static QualificationSoap[][] toSoapModels(Qualification[][] models) {
        QualificationSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new QualificationSoap[models.length][models[0].length];
        } else {
            soapModels = new QualificationSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static QualificationSoap[] toSoapModels(List<Qualification> models) {
        List<QualificationSoap> soapModels = new ArrayList<QualificationSoap>(models.size());

        for (Qualification model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new QualificationSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _qualificationId;
    }

    public void setPrimaryKey(long pk) {
        setQualificationId(pk);
    }

    public long getQualificationId() {
        return _qualificationId;
    }

    public void setQualificationId(long qualificationId) {
        _qualificationId = qualificationId;
    }

    public long getCreatorId() {
        return _creatorId;
    }

    public void setCreatorId(long creatorId) {
        _creatorId = creatorId;
    }

    public String getCreatorName() {
        return _creatorName;
    }

    public void setCreatorName(String creatorName) {
        _creatorName = creatorName;
    }

    public Date getCreateDate() {
        return _createDate;
    }

    public void setCreateDate(Date createDate) {
        _createDate = createDate;
    }

    public long getModifierId() {
        return _modifierId;
    }

    public void setModifierId(long modifierId) {
        _modifierId = modifierId;
    }

    public String getModifierName() {
        return _modifierName;
    }

    public void setModifierName(String modifierName) {
        _modifierName = modifierName;
    }

    public Date getModifiedDate() {
        return _modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;
    }

    public long getOwnerId() {
        return _ownerId;
    }

    public void setOwnerId(long ownerId) {
        _ownerId = ownerId;
    }

    public long getTypeId() {
        return _typeId;
    }

    public void setTypeId(long typeId) {
        _typeId = typeId;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public String getCertNumber() {
        return _certNumber;
    }

    public void setCertNumber(String certNumber) {
        _certNumber = certNumber;
    }

    public Date getDeliveryDate() {
        return _deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        _deliveryDate = deliveryDate;
    }

    public Date getExpirationDate() {
        return _expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        _expirationDate = expirationDate;
    }

    public String getIssuingCountry() {
        return _issuingCountry;
    }

    public void setIssuingCountry(String issuingCountry) {
        _issuingCountry = issuingCountry;
    }

    public String getIssuingInstitution() {
        return _issuingInstitution;
    }

    public void setIssuingInstitution(String issuingInstitution) {
        _issuingInstitution = issuingInstitution;
    }
}
