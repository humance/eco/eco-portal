package de.humance.eco.profile.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the QualificationType service. Represents a row in the &quot;Profile_QualificationType&quot; database table, with each column mapped to a property of this class.
 *
 * @author Humance
 * @see QualificationTypeModel
 * @see de.humance.eco.profile.model.impl.QualificationTypeImpl
 * @see de.humance.eco.profile.model.impl.QualificationTypeModelImpl
 * @generated
 */
public interface QualificationType extends QualificationTypeModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link de.humance.eco.profile.model.impl.QualificationTypeImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
