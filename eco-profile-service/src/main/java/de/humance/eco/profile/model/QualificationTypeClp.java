package de.humance.eco.profile.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import de.humance.eco.profile.service.ClpSerializer;
import de.humance.eco.profile.service.QualificationTypeLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class QualificationTypeClp extends BaseModelImpl<QualificationType>
    implements QualificationType {
    private long _qualificationTypeId;
    private long _creatorId;
    private String _creatorName;
    private Date _createDate;
    private long _modifierId;
    private String _modifierName;
    private Date _modifiedDate;
    private String _name;
    private String _description;
    private String _note;
    private long _iconId;
    private BaseModel<?> _qualificationTypeRemoteModel;

    public QualificationTypeClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return QualificationType.class;
    }

    @Override
    public String getModelClassName() {
        return QualificationType.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _qualificationTypeId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setQualificationTypeId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _qualificationTypeId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("qualificationTypeId", getQualificationTypeId());
        attributes.put("creatorId", getCreatorId());
        attributes.put("creatorName", getCreatorName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifierId", getModifierId());
        attributes.put("modifierName", getModifierName());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("name", getName());
        attributes.put("description", getDescription());
        attributes.put("note", getNote());
        attributes.put("iconId", getIconId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long qualificationTypeId = (Long) attributes.get("qualificationTypeId");

        if (qualificationTypeId != null) {
            setQualificationTypeId(qualificationTypeId);
        }

        Long creatorId = (Long) attributes.get("creatorId");

        if (creatorId != null) {
            setCreatorId(creatorId);
        }

        String creatorName = (String) attributes.get("creatorName");

        if (creatorName != null) {
            setCreatorName(creatorName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Long modifierId = (Long) attributes.get("modifierId");

        if (modifierId != null) {
            setModifierId(modifierId);
        }

        String modifierName = (String) attributes.get("modifierName");

        if (modifierName != null) {
            setModifierName(modifierName);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        String note = (String) attributes.get("note");

        if (note != null) {
            setNote(note);
        }

        Long iconId = (Long) attributes.get("iconId");

        if (iconId != null) {
            setIconId(iconId);
        }
    }

    @Override
    public long getQualificationTypeId() {
        return _qualificationTypeId;
    }

    @Override
    public void setQualificationTypeId(long qualificationTypeId) {
        _qualificationTypeId = qualificationTypeId;

        if (_qualificationTypeRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setQualificationTypeId",
                        long.class);

                method.invoke(_qualificationTypeRemoteModel, qualificationTypeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getCreatorId() {
        return _creatorId;
    }

    @Override
    public void setCreatorId(long creatorId) {
        _creatorId = creatorId;

        if (_qualificationTypeRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setCreatorId", long.class);

                method.invoke(_qualificationTypeRemoteModel, creatorId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCreatorName() {
        return _creatorName;
    }

    @Override
    public void setCreatorName(String creatorName) {
        _creatorName = creatorName;

        if (_qualificationTypeRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setCreatorName", String.class);

                method.invoke(_qualificationTypeRemoteModel, creatorName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getCreateDate() {
        return _createDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        _createDate = createDate;

        if (_qualificationTypeRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setCreateDate", Date.class);

                method.invoke(_qualificationTypeRemoteModel, createDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getModifierId() {
        return _modifierId;
    }

    @Override
    public void setModifierId(long modifierId) {
        _modifierId = modifierId;

        if (_qualificationTypeRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setModifierId", long.class);

                method.invoke(_qualificationTypeRemoteModel, modifierId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getModifierName() {
        return _modifierName;
    }

    @Override
    public void setModifierName(String modifierName) {
        _modifierName = modifierName;

        if (_qualificationTypeRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setModifierName", String.class);

                method.invoke(_qualificationTypeRemoteModel, modifierName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getModifiedDate() {
        return _modifiedDate;
    }

    @Override
    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;

        if (_qualificationTypeRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setModifiedDate", Date.class);

                method.invoke(_qualificationTypeRemoteModel, modifiedDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getName() {
        return _name;
    }

    @Override
    public void setName(String name) {
        _name = name;

        if (_qualificationTypeRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setName", String.class);

                method.invoke(_qualificationTypeRemoteModel, name);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getDescription() {
        return _description;
    }

    @Override
    public void setDescription(String description) {
        _description = description;

        if (_qualificationTypeRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setDescription", String.class);

                method.invoke(_qualificationTypeRemoteModel, description);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNote() {
        return _note;
    }

    @Override
    public void setNote(String note) {
        _note = note;

        if (_qualificationTypeRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setNote", String.class);

                method.invoke(_qualificationTypeRemoteModel, note);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getIconId() {
        return _iconId;
    }

    @Override
    public void setIconId(long iconId) {
        _iconId = iconId;

        if (_qualificationTypeRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setIconId", long.class);

                method.invoke(_qualificationTypeRemoteModel, iconId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getQualificationTypeRemoteModel() {
        return _qualificationTypeRemoteModel;
    }

    public void setQualificationTypeRemoteModel(
        BaseModel<?> qualificationTypeRemoteModel) {
        _qualificationTypeRemoteModel = qualificationTypeRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _qualificationTypeRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_qualificationTypeRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            QualificationTypeLocalServiceUtil.addQualificationType(this);
        } else {
            QualificationTypeLocalServiceUtil.updateQualificationType(this);
        }
    }

    @Override
    public QualificationType toEscapedModel() {
        return (QualificationType) ProxyUtil.newProxyInstance(QualificationType.class.getClassLoader(),
            new Class[] { QualificationType.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        QualificationTypeClp clone = new QualificationTypeClp();

        clone.setQualificationTypeId(getQualificationTypeId());
        clone.setCreatorId(getCreatorId());
        clone.setCreatorName(getCreatorName());
        clone.setCreateDate(getCreateDate());
        clone.setModifierId(getModifierId());
        clone.setModifierName(getModifierName());
        clone.setModifiedDate(getModifiedDate());
        clone.setName(getName());
        clone.setDescription(getDescription());
        clone.setNote(getNote());
        clone.setIconId(getIconId());

        return clone;
    }

    @Override
    public int compareTo(QualificationType qualificationType) {
        int value = 0;

        value = getName().compareToIgnoreCase(qualificationType.getName());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof QualificationTypeClp)) {
            return false;
        }

        QualificationTypeClp qualificationType = (QualificationTypeClp) obj;

        long primaryKey = qualificationType.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(23);

        sb.append("{qualificationTypeId=");
        sb.append(getQualificationTypeId());
        sb.append(", creatorId=");
        sb.append(getCreatorId());
        sb.append(", creatorName=");
        sb.append(getCreatorName());
        sb.append(", createDate=");
        sb.append(getCreateDate());
        sb.append(", modifierId=");
        sb.append(getModifierId());
        sb.append(", modifierName=");
        sb.append(getModifierName());
        sb.append(", modifiedDate=");
        sb.append(getModifiedDate());
        sb.append(", name=");
        sb.append(getName());
        sb.append(", description=");
        sb.append(getDescription());
        sb.append(", note=");
        sb.append(getNote());
        sb.append(", iconId=");
        sb.append(getIconId());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(37);

        sb.append("<model><model-name>");
        sb.append("de.humance.eco.profile.model.QualificationType");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>qualificationTypeId</column-name><column-value><![CDATA[");
        sb.append(getQualificationTypeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>creatorId</column-name><column-value><![CDATA[");
        sb.append(getCreatorId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>creatorName</column-name><column-value><![CDATA[");
        sb.append(getCreatorName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>createDate</column-name><column-value><![CDATA[");
        sb.append(getCreateDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifierId</column-name><column-value><![CDATA[");
        sb.append(getModifierId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifierName</column-name><column-value><![CDATA[");
        sb.append(getModifierName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
        sb.append(getModifiedDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>name</column-name><column-value><![CDATA[");
        sb.append(getName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>description</column-name><column-value><![CDATA[");
        sb.append(getDescription());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>note</column-name><column-value><![CDATA[");
        sb.append(getNote());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>iconId</column-name><column-value><![CDATA[");
        sb.append(getIconId());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
