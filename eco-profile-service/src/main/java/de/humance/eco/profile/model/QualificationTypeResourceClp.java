package de.humance.eco.profile.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import de.humance.eco.profile.service.ClpSerializer;
import de.humance.eco.profile.service.QualificationTypeResourceLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class QualificationTypeResourceClp extends BaseModelImpl<QualificationTypeResource>
    implements QualificationTypeResource {
    private long _qualificationTypeResourceId;
    private long _qualificationTypeId;
    private String _country;
    private String _language;
    private String _name;
    private String _description;
    private String _note;
    private long _iconId;
    private BaseModel<?> _qualificationTypeResourceRemoteModel;

    public QualificationTypeResourceClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return QualificationTypeResource.class;
    }

    @Override
    public String getModelClassName() {
        return QualificationTypeResource.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _qualificationTypeResourceId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setQualificationTypeResourceId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _qualificationTypeResourceId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("qualificationTypeResourceId",
            getQualificationTypeResourceId());
        attributes.put("qualificationTypeId", getQualificationTypeId());
        attributes.put("country", getCountry());
        attributes.put("language", getLanguage());
        attributes.put("name", getName());
        attributes.put("description", getDescription());
        attributes.put("note", getNote());
        attributes.put("iconId", getIconId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long qualificationTypeResourceId = (Long) attributes.get(
                "qualificationTypeResourceId");

        if (qualificationTypeResourceId != null) {
            setQualificationTypeResourceId(qualificationTypeResourceId);
        }

        Long qualificationTypeId = (Long) attributes.get("qualificationTypeId");

        if (qualificationTypeId != null) {
            setQualificationTypeId(qualificationTypeId);
        }

        String country = (String) attributes.get("country");

        if (country != null) {
            setCountry(country);
        }

        String language = (String) attributes.get("language");

        if (language != null) {
            setLanguage(language);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        String note = (String) attributes.get("note");

        if (note != null) {
            setNote(note);
        }

        Long iconId = (Long) attributes.get("iconId");

        if (iconId != null) {
            setIconId(iconId);
        }
    }

    @Override
    public long getQualificationTypeResourceId() {
        return _qualificationTypeResourceId;
    }

    @Override
    public void setQualificationTypeResourceId(long qualificationTypeResourceId) {
        _qualificationTypeResourceId = qualificationTypeResourceId;

        if (_qualificationTypeResourceRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setQualificationTypeResourceId",
                        long.class);

                method.invoke(_qualificationTypeResourceRemoteModel,
                    qualificationTypeResourceId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getQualificationTypeId() {
        return _qualificationTypeId;
    }

    @Override
    public void setQualificationTypeId(long qualificationTypeId) {
        _qualificationTypeId = qualificationTypeId;

        if (_qualificationTypeResourceRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setQualificationTypeId",
                        long.class);

                method.invoke(_qualificationTypeResourceRemoteModel,
                    qualificationTypeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCountry() {
        return _country;
    }

    @Override
    public void setCountry(String country) {
        _country = country;

        if (_qualificationTypeResourceRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setCountry", String.class);

                method.invoke(_qualificationTypeResourceRemoteModel, country);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getLanguage() {
        return _language;
    }

    @Override
    public void setLanguage(String language) {
        _language = language;

        if (_qualificationTypeResourceRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setLanguage", String.class);

                method.invoke(_qualificationTypeResourceRemoteModel, language);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getName() {
        return _name;
    }

    @Override
    public void setName(String name) {
        _name = name;

        if (_qualificationTypeResourceRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setName", String.class);

                method.invoke(_qualificationTypeResourceRemoteModel, name);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getDescription() {
        return _description;
    }

    @Override
    public void setDescription(String description) {
        _description = description;

        if (_qualificationTypeResourceRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setDescription", String.class);

                method.invoke(_qualificationTypeResourceRemoteModel, description);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNote() {
        return _note;
    }

    @Override
    public void setNote(String note) {
        _note = note;

        if (_qualificationTypeResourceRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setNote", String.class);

                method.invoke(_qualificationTypeResourceRemoteModel, note);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getIconId() {
        return _iconId;
    }

    @Override
    public void setIconId(long iconId) {
        _iconId = iconId;

        if (_qualificationTypeResourceRemoteModel != null) {
            try {
                Class<?> clazz = _qualificationTypeResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setIconId", long.class);

                method.invoke(_qualificationTypeResourceRemoteModel, iconId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getQualificationTypeResourceRemoteModel() {
        return _qualificationTypeResourceRemoteModel;
    }

    public void setQualificationTypeResourceRemoteModel(
        BaseModel<?> qualificationTypeResourceRemoteModel) {
        _qualificationTypeResourceRemoteModel = qualificationTypeResourceRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _qualificationTypeResourceRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_qualificationTypeResourceRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            QualificationTypeResourceLocalServiceUtil.addQualificationTypeResource(this);
        } else {
            QualificationTypeResourceLocalServiceUtil.updateQualificationTypeResource(this);
        }
    }

    @Override
    public QualificationTypeResource toEscapedModel() {
        return (QualificationTypeResource) ProxyUtil.newProxyInstance(QualificationTypeResource.class.getClassLoader(),
            new Class[] { QualificationTypeResource.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        QualificationTypeResourceClp clone = new QualificationTypeResourceClp();

        clone.setQualificationTypeResourceId(getQualificationTypeResourceId());
        clone.setQualificationTypeId(getQualificationTypeId());
        clone.setCountry(getCountry());
        clone.setLanguage(getLanguage());
        clone.setName(getName());
        clone.setDescription(getDescription());
        clone.setNote(getNote());
        clone.setIconId(getIconId());

        return clone;
    }

    @Override
    public int compareTo(QualificationTypeResource qualificationTypeResource) {
        int value = 0;

        if (getQualificationTypeId() < qualificationTypeResource.getQualificationTypeId()) {
            value = -1;
        } else if (getQualificationTypeId() > qualificationTypeResource.getQualificationTypeId()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        value = getCountry()
                    .compareToIgnoreCase(qualificationTypeResource.getCountry());

        if (value != 0) {
            return value;
        }

        value = getLanguage()
                    .compareToIgnoreCase(qualificationTypeResource.getLanguage());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof QualificationTypeResourceClp)) {
            return false;
        }

        QualificationTypeResourceClp qualificationTypeResource = (QualificationTypeResourceClp) obj;

        long primaryKey = qualificationTypeResource.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(17);

        sb.append("{qualificationTypeResourceId=");
        sb.append(getQualificationTypeResourceId());
        sb.append(", qualificationTypeId=");
        sb.append(getQualificationTypeId());
        sb.append(", country=");
        sb.append(getCountry());
        sb.append(", language=");
        sb.append(getLanguage());
        sb.append(", name=");
        sb.append(getName());
        sb.append(", description=");
        sb.append(getDescription());
        sb.append(", note=");
        sb.append(getNote());
        sb.append(", iconId=");
        sb.append(getIconId());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(28);

        sb.append("<model><model-name>");
        sb.append("de.humance.eco.profile.model.QualificationTypeResource");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>qualificationTypeResourceId</column-name><column-value><![CDATA[");
        sb.append(getQualificationTypeResourceId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>qualificationTypeId</column-name><column-value><![CDATA[");
        sb.append(getQualificationTypeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>country</column-name><column-value><![CDATA[");
        sb.append(getCountry());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>language</column-name><column-value><![CDATA[");
        sb.append(getLanguage());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>name</column-name><column-value><![CDATA[");
        sb.append(getName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>description</column-name><column-value><![CDATA[");
        sb.append(getDescription());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>note</column-name><column-value><![CDATA[");
        sb.append(getNote());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>iconId</column-name><column-value><![CDATA[");
        sb.append(getIconId());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
