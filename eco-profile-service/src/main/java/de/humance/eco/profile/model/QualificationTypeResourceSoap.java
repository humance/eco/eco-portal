package de.humance.eco.profile.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Humance
 * @generated
 */
public class QualificationTypeResourceSoap implements Serializable {
    private long _qualificationTypeResourceId;
    private long _qualificationTypeId;
    private String _country;
    private String _language;
    private String _name;
    private String _description;
    private String _note;
    private long _iconId;

    public QualificationTypeResourceSoap() {
    }

    public static QualificationTypeResourceSoap toSoapModel(
        QualificationTypeResource model) {
        QualificationTypeResourceSoap soapModel = new QualificationTypeResourceSoap();

        soapModel.setQualificationTypeResourceId(model.getQualificationTypeResourceId());
        soapModel.setQualificationTypeId(model.getQualificationTypeId());
        soapModel.setCountry(model.getCountry());
        soapModel.setLanguage(model.getLanguage());
        soapModel.setName(model.getName());
        soapModel.setDescription(model.getDescription());
        soapModel.setNote(model.getNote());
        soapModel.setIconId(model.getIconId());

        return soapModel;
    }

    public static QualificationTypeResourceSoap[] toSoapModels(
        QualificationTypeResource[] models) {
        QualificationTypeResourceSoap[] soapModels = new QualificationTypeResourceSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static QualificationTypeResourceSoap[][] toSoapModels(
        QualificationTypeResource[][] models) {
        QualificationTypeResourceSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new QualificationTypeResourceSoap[models.length][models[0].length];
        } else {
            soapModels = new QualificationTypeResourceSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static QualificationTypeResourceSoap[] toSoapModels(
        List<QualificationTypeResource> models) {
        List<QualificationTypeResourceSoap> soapModels = new ArrayList<QualificationTypeResourceSoap>(models.size());

        for (QualificationTypeResource model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new QualificationTypeResourceSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _qualificationTypeResourceId;
    }

    public void setPrimaryKey(long pk) {
        setQualificationTypeResourceId(pk);
    }

    public long getQualificationTypeResourceId() {
        return _qualificationTypeResourceId;
    }

    public void setQualificationTypeResourceId(long qualificationTypeResourceId) {
        _qualificationTypeResourceId = qualificationTypeResourceId;
    }

    public long getQualificationTypeId() {
        return _qualificationTypeId;
    }

    public void setQualificationTypeId(long qualificationTypeId) {
        _qualificationTypeId = qualificationTypeId;
    }

    public String getCountry() {
        return _country;
    }

    public void setCountry(String country) {
        _country = country;
    }

    public String getLanguage() {
        return _language;
    }

    public void setLanguage(String language) {
        _language = language;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public String getNote() {
        return _note;
    }

    public void setNote(String note) {
        _note = note;
    }

    public long getIconId() {
        return _iconId;
    }

    public void setIconId(long iconId) {
        _iconId = iconId;
    }
}
