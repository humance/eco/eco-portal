package de.humance.eco.profile.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link QualificationTypeResource}.
 * </p>
 *
 * @author Humance
 * @see QualificationTypeResource
 * @generated
 */
public class QualificationTypeResourceWrapper
    implements QualificationTypeResource,
        ModelWrapper<QualificationTypeResource> {
    private QualificationTypeResource _qualificationTypeResource;

    public QualificationTypeResourceWrapper(
        QualificationTypeResource qualificationTypeResource) {
        _qualificationTypeResource = qualificationTypeResource;
    }

    @Override
    public Class<?> getModelClass() {
        return QualificationTypeResource.class;
    }

    @Override
    public String getModelClassName() {
        return QualificationTypeResource.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("qualificationTypeResourceId",
            getQualificationTypeResourceId());
        attributes.put("qualificationTypeId", getQualificationTypeId());
        attributes.put("country", getCountry());
        attributes.put("language", getLanguage());
        attributes.put("name", getName());
        attributes.put("description", getDescription());
        attributes.put("note", getNote());
        attributes.put("iconId", getIconId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long qualificationTypeResourceId = (Long) attributes.get(
                "qualificationTypeResourceId");

        if (qualificationTypeResourceId != null) {
            setQualificationTypeResourceId(qualificationTypeResourceId);
        }

        Long qualificationTypeId = (Long) attributes.get("qualificationTypeId");

        if (qualificationTypeId != null) {
            setQualificationTypeId(qualificationTypeId);
        }

        String country = (String) attributes.get("country");

        if (country != null) {
            setCountry(country);
        }

        String language = (String) attributes.get("language");

        if (language != null) {
            setLanguage(language);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        String note = (String) attributes.get("note");

        if (note != null) {
            setNote(note);
        }

        Long iconId = (Long) attributes.get("iconId");

        if (iconId != null) {
            setIconId(iconId);
        }
    }

    /**
    * Returns the primary key of this qualification type resource.
    *
    * @return the primary key of this qualification type resource
    */
    @Override
    public long getPrimaryKey() {
        return _qualificationTypeResource.getPrimaryKey();
    }

    /**
    * Sets the primary key of this qualification type resource.
    *
    * @param primaryKey the primary key of this qualification type resource
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _qualificationTypeResource.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the qualification type resource ID of this qualification type resource.
    *
    * @return the qualification type resource ID of this qualification type resource
    */
    @Override
    public long getQualificationTypeResourceId() {
        return _qualificationTypeResource.getQualificationTypeResourceId();
    }

    /**
    * Sets the qualification type resource ID of this qualification type resource.
    *
    * @param qualificationTypeResourceId the qualification type resource ID of this qualification type resource
    */
    @Override
    public void setQualificationTypeResourceId(long qualificationTypeResourceId) {
        _qualificationTypeResource.setQualificationTypeResourceId(qualificationTypeResourceId);
    }

    /**
    * Returns the qualification type ID of this qualification type resource.
    *
    * @return the qualification type ID of this qualification type resource
    */
    @Override
    public long getQualificationTypeId() {
        return _qualificationTypeResource.getQualificationTypeId();
    }

    /**
    * Sets the qualification type ID of this qualification type resource.
    *
    * @param qualificationTypeId the qualification type ID of this qualification type resource
    */
    @Override
    public void setQualificationTypeId(long qualificationTypeId) {
        _qualificationTypeResource.setQualificationTypeId(qualificationTypeId);
    }

    /**
    * Returns the country of this qualification type resource.
    *
    * @return the country of this qualification type resource
    */
    @Override
    public java.lang.String getCountry() {
        return _qualificationTypeResource.getCountry();
    }

    /**
    * Sets the country of this qualification type resource.
    *
    * @param country the country of this qualification type resource
    */
    @Override
    public void setCountry(java.lang.String country) {
        _qualificationTypeResource.setCountry(country);
    }

    /**
    * Returns the language of this qualification type resource.
    *
    * @return the language of this qualification type resource
    */
    @Override
    public java.lang.String getLanguage() {
        return _qualificationTypeResource.getLanguage();
    }

    /**
    * Sets the language of this qualification type resource.
    *
    * @param language the language of this qualification type resource
    */
    @Override
    public void setLanguage(java.lang.String language) {
        _qualificationTypeResource.setLanguage(language);
    }

    /**
    * Returns the name of this qualification type resource.
    *
    * @return the name of this qualification type resource
    */
    @Override
    public java.lang.String getName() {
        return _qualificationTypeResource.getName();
    }

    /**
    * Sets the name of this qualification type resource.
    *
    * @param name the name of this qualification type resource
    */
    @Override
    public void setName(java.lang.String name) {
        _qualificationTypeResource.setName(name);
    }

    /**
    * Returns the description of this qualification type resource.
    *
    * @return the description of this qualification type resource
    */
    @Override
    public java.lang.String getDescription() {
        return _qualificationTypeResource.getDescription();
    }

    /**
    * Sets the description of this qualification type resource.
    *
    * @param description the description of this qualification type resource
    */
    @Override
    public void setDescription(java.lang.String description) {
        _qualificationTypeResource.setDescription(description);
    }

    /**
    * Returns the note of this qualification type resource.
    *
    * @return the note of this qualification type resource
    */
    @Override
    public java.lang.String getNote() {
        return _qualificationTypeResource.getNote();
    }

    /**
    * Sets the note of this qualification type resource.
    *
    * @param note the note of this qualification type resource
    */
    @Override
    public void setNote(java.lang.String note) {
        _qualificationTypeResource.setNote(note);
    }

    /**
    * Returns the icon ID of this qualification type resource.
    *
    * @return the icon ID of this qualification type resource
    */
    @Override
    public long getIconId() {
        return _qualificationTypeResource.getIconId();
    }

    /**
    * Sets the icon ID of this qualification type resource.
    *
    * @param iconId the icon ID of this qualification type resource
    */
    @Override
    public void setIconId(long iconId) {
        _qualificationTypeResource.setIconId(iconId);
    }

    @Override
    public boolean isNew() {
        return _qualificationTypeResource.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _qualificationTypeResource.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _qualificationTypeResource.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _qualificationTypeResource.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _qualificationTypeResource.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _qualificationTypeResource.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _qualificationTypeResource.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _qualificationTypeResource.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _qualificationTypeResource.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _qualificationTypeResource.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _qualificationTypeResource.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new QualificationTypeResourceWrapper((QualificationTypeResource) _qualificationTypeResource.clone());
    }

    @Override
    public int compareTo(QualificationTypeResource qualificationTypeResource) {
        return _qualificationTypeResource.compareTo(qualificationTypeResource);
    }

    @Override
    public int hashCode() {
        return _qualificationTypeResource.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<QualificationTypeResource> toCacheModel() {
        return _qualificationTypeResource.toCacheModel();
    }

    @Override
    public QualificationTypeResource toEscapedModel() {
        return new QualificationTypeResourceWrapper(_qualificationTypeResource.toEscapedModel());
    }

    @Override
    public QualificationTypeResource toUnescapedModel() {
        return new QualificationTypeResourceWrapper(_qualificationTypeResource.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _qualificationTypeResource.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _qualificationTypeResource.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _qualificationTypeResource.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof QualificationTypeResourceWrapper)) {
            return false;
        }

        QualificationTypeResourceWrapper qualificationTypeResourceWrapper = (QualificationTypeResourceWrapper) obj;

        if (Validator.equals(_qualificationTypeResource,
                    qualificationTypeResourceWrapper._qualificationTypeResource)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public QualificationTypeResource getWrappedQualificationTypeResource() {
        return _qualificationTypeResource;
    }

    @Override
    public QualificationTypeResource getWrappedModel() {
        return _qualificationTypeResource;
    }

    @Override
    public void resetOriginalValues() {
        _qualificationTypeResource.resetOriginalValues();
    }
}
