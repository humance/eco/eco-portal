package de.humance.eco.profile.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Humance
 * @generated
 */
public class QualificationTypeSoap implements Serializable {
    private long _qualificationTypeId;
    private long _creatorId;
    private String _creatorName;
    private Date _createDate;
    private long _modifierId;
    private String _modifierName;
    private Date _modifiedDate;
    private String _name;
    private String _description;
    private String _note;
    private long _iconId;

    public QualificationTypeSoap() {
    }

    public static QualificationTypeSoap toSoapModel(QualificationType model) {
        QualificationTypeSoap soapModel = new QualificationTypeSoap();

        soapModel.setQualificationTypeId(model.getQualificationTypeId());
        soapModel.setCreatorId(model.getCreatorId());
        soapModel.setCreatorName(model.getCreatorName());
        soapModel.setCreateDate(model.getCreateDate());
        soapModel.setModifierId(model.getModifierId());
        soapModel.setModifierName(model.getModifierName());
        soapModel.setModifiedDate(model.getModifiedDate());
        soapModel.setName(model.getName());
        soapModel.setDescription(model.getDescription());
        soapModel.setNote(model.getNote());
        soapModel.setIconId(model.getIconId());

        return soapModel;
    }

    public static QualificationTypeSoap[] toSoapModels(
        QualificationType[] models) {
        QualificationTypeSoap[] soapModels = new QualificationTypeSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static QualificationTypeSoap[][] toSoapModels(
        QualificationType[][] models) {
        QualificationTypeSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new QualificationTypeSoap[models.length][models[0].length];
        } else {
            soapModels = new QualificationTypeSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static QualificationTypeSoap[] toSoapModels(
        List<QualificationType> models) {
        List<QualificationTypeSoap> soapModels = new ArrayList<QualificationTypeSoap>(models.size());

        for (QualificationType model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new QualificationTypeSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _qualificationTypeId;
    }

    public void setPrimaryKey(long pk) {
        setQualificationTypeId(pk);
    }

    public long getQualificationTypeId() {
        return _qualificationTypeId;
    }

    public void setQualificationTypeId(long qualificationTypeId) {
        _qualificationTypeId = qualificationTypeId;
    }

    public long getCreatorId() {
        return _creatorId;
    }

    public void setCreatorId(long creatorId) {
        _creatorId = creatorId;
    }

    public String getCreatorName() {
        return _creatorName;
    }

    public void setCreatorName(String creatorName) {
        _creatorName = creatorName;
    }

    public Date getCreateDate() {
        return _createDate;
    }

    public void setCreateDate(Date createDate) {
        _createDate = createDate;
    }

    public long getModifierId() {
        return _modifierId;
    }

    public void setModifierId(long modifierId) {
        _modifierId = modifierId;
    }

    public String getModifierName() {
        return _modifierName;
    }

    public void setModifierName(String modifierName) {
        _modifierName = modifierName;
    }

    public Date getModifiedDate() {
        return _modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public String getNote() {
        return _note;
    }

    public void setNote(String note) {
        _note = note;
    }

    public long getIconId() {
        return _iconId;
    }

    public void setIconId(long iconId) {
        _iconId = iconId;
    }
}
