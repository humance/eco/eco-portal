package de.humance.eco.profile.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link QualificationType}.
 * </p>
 *
 * @author Humance
 * @see QualificationType
 * @generated
 */
public class QualificationTypeWrapper implements QualificationType,
    ModelWrapper<QualificationType> {
    private QualificationType _qualificationType;

    public QualificationTypeWrapper(QualificationType qualificationType) {
        _qualificationType = qualificationType;
    }

    @Override
    public Class<?> getModelClass() {
        return QualificationType.class;
    }

    @Override
    public String getModelClassName() {
        return QualificationType.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("qualificationTypeId", getQualificationTypeId());
        attributes.put("creatorId", getCreatorId());
        attributes.put("creatorName", getCreatorName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifierId", getModifierId());
        attributes.put("modifierName", getModifierName());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("name", getName());
        attributes.put("description", getDescription());
        attributes.put("note", getNote());
        attributes.put("iconId", getIconId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long qualificationTypeId = (Long) attributes.get("qualificationTypeId");

        if (qualificationTypeId != null) {
            setQualificationTypeId(qualificationTypeId);
        }

        Long creatorId = (Long) attributes.get("creatorId");

        if (creatorId != null) {
            setCreatorId(creatorId);
        }

        String creatorName = (String) attributes.get("creatorName");

        if (creatorName != null) {
            setCreatorName(creatorName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Long modifierId = (Long) attributes.get("modifierId");

        if (modifierId != null) {
            setModifierId(modifierId);
        }

        String modifierName = (String) attributes.get("modifierName");

        if (modifierName != null) {
            setModifierName(modifierName);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        String note = (String) attributes.get("note");

        if (note != null) {
            setNote(note);
        }

        Long iconId = (Long) attributes.get("iconId");

        if (iconId != null) {
            setIconId(iconId);
        }
    }

    /**
    * Returns the primary key of this qualification type.
    *
    * @return the primary key of this qualification type
    */
    @Override
    public long getPrimaryKey() {
        return _qualificationType.getPrimaryKey();
    }

    /**
    * Sets the primary key of this qualification type.
    *
    * @param primaryKey the primary key of this qualification type
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _qualificationType.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the qualification type ID of this qualification type.
    *
    * @return the qualification type ID of this qualification type
    */
    @Override
    public long getQualificationTypeId() {
        return _qualificationType.getQualificationTypeId();
    }

    /**
    * Sets the qualification type ID of this qualification type.
    *
    * @param qualificationTypeId the qualification type ID of this qualification type
    */
    @Override
    public void setQualificationTypeId(long qualificationTypeId) {
        _qualificationType.setQualificationTypeId(qualificationTypeId);
    }

    /**
    * Returns the creator ID of this qualification type.
    *
    * @return the creator ID of this qualification type
    */
    @Override
    public long getCreatorId() {
        return _qualificationType.getCreatorId();
    }

    /**
    * Sets the creator ID of this qualification type.
    *
    * @param creatorId the creator ID of this qualification type
    */
    @Override
    public void setCreatorId(long creatorId) {
        _qualificationType.setCreatorId(creatorId);
    }

    /**
    * Returns the creator name of this qualification type.
    *
    * @return the creator name of this qualification type
    */
    @Override
    public java.lang.String getCreatorName() {
        return _qualificationType.getCreatorName();
    }

    /**
    * Sets the creator name of this qualification type.
    *
    * @param creatorName the creator name of this qualification type
    */
    @Override
    public void setCreatorName(java.lang.String creatorName) {
        _qualificationType.setCreatorName(creatorName);
    }

    /**
    * Returns the create date of this qualification type.
    *
    * @return the create date of this qualification type
    */
    @Override
    public java.util.Date getCreateDate() {
        return _qualificationType.getCreateDate();
    }

    /**
    * Sets the create date of this qualification type.
    *
    * @param createDate the create date of this qualification type
    */
    @Override
    public void setCreateDate(java.util.Date createDate) {
        _qualificationType.setCreateDate(createDate);
    }

    /**
    * Returns the modifier ID of this qualification type.
    *
    * @return the modifier ID of this qualification type
    */
    @Override
    public long getModifierId() {
        return _qualificationType.getModifierId();
    }

    /**
    * Sets the modifier ID of this qualification type.
    *
    * @param modifierId the modifier ID of this qualification type
    */
    @Override
    public void setModifierId(long modifierId) {
        _qualificationType.setModifierId(modifierId);
    }

    /**
    * Returns the modifier name of this qualification type.
    *
    * @return the modifier name of this qualification type
    */
    @Override
    public java.lang.String getModifierName() {
        return _qualificationType.getModifierName();
    }

    /**
    * Sets the modifier name of this qualification type.
    *
    * @param modifierName the modifier name of this qualification type
    */
    @Override
    public void setModifierName(java.lang.String modifierName) {
        _qualificationType.setModifierName(modifierName);
    }

    /**
    * Returns the modified date of this qualification type.
    *
    * @return the modified date of this qualification type
    */
    @Override
    public java.util.Date getModifiedDate() {
        return _qualificationType.getModifiedDate();
    }

    /**
    * Sets the modified date of this qualification type.
    *
    * @param modifiedDate the modified date of this qualification type
    */
    @Override
    public void setModifiedDate(java.util.Date modifiedDate) {
        _qualificationType.setModifiedDate(modifiedDate);
    }

    /**
    * Returns the name of this qualification type.
    *
    * @return the name of this qualification type
    */
    @Override
    public java.lang.String getName() {
        return _qualificationType.getName();
    }

    /**
    * Sets the name of this qualification type.
    *
    * @param name the name of this qualification type
    */
    @Override
    public void setName(java.lang.String name) {
        _qualificationType.setName(name);
    }

    /**
    * Returns the description of this qualification type.
    *
    * @return the description of this qualification type
    */
    @Override
    public java.lang.String getDescription() {
        return _qualificationType.getDescription();
    }

    /**
    * Sets the description of this qualification type.
    *
    * @param description the description of this qualification type
    */
    @Override
    public void setDescription(java.lang.String description) {
        _qualificationType.setDescription(description);
    }

    /**
    * Returns the note of this qualification type.
    *
    * @return the note of this qualification type
    */
    @Override
    public java.lang.String getNote() {
        return _qualificationType.getNote();
    }

    /**
    * Sets the note of this qualification type.
    *
    * @param note the note of this qualification type
    */
    @Override
    public void setNote(java.lang.String note) {
        _qualificationType.setNote(note);
    }

    /**
    * Returns the icon ID of this qualification type.
    *
    * @return the icon ID of this qualification type
    */
    @Override
    public long getIconId() {
        return _qualificationType.getIconId();
    }

    /**
    * Sets the icon ID of this qualification type.
    *
    * @param iconId the icon ID of this qualification type
    */
    @Override
    public void setIconId(long iconId) {
        _qualificationType.setIconId(iconId);
    }

    @Override
    public boolean isNew() {
        return _qualificationType.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _qualificationType.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _qualificationType.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _qualificationType.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _qualificationType.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _qualificationType.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _qualificationType.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _qualificationType.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _qualificationType.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _qualificationType.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _qualificationType.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new QualificationTypeWrapper((QualificationType) _qualificationType.clone());
    }

    @Override
    public int compareTo(QualificationType qualificationType) {
        return _qualificationType.compareTo(qualificationType);
    }

    @Override
    public int hashCode() {
        return _qualificationType.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<QualificationType> toCacheModel() {
        return _qualificationType.toCacheModel();
    }

    @Override
    public QualificationType toEscapedModel() {
        return new QualificationTypeWrapper(_qualificationType.toEscapedModel());
    }

    @Override
    public QualificationType toUnescapedModel() {
        return new QualificationTypeWrapper(_qualificationType.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _qualificationType.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _qualificationType.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _qualificationType.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof QualificationTypeWrapper)) {
            return false;
        }

        QualificationTypeWrapper qualificationTypeWrapper = (QualificationTypeWrapper) obj;

        if (Validator.equals(_qualificationType,
                    qualificationTypeWrapper._qualificationType)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public QualificationType getWrappedQualificationType() {
        return _qualificationType;
    }

    @Override
    public QualificationType getWrappedModel() {
        return _qualificationType;
    }

    @Override
    public void resetOriginalValues() {
        _qualificationType.resetOriginalValues();
    }
}
