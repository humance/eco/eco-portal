package de.humance.eco.profile.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Qualification}.
 * </p>
 *
 * @author Humance
 * @see Qualification
 * @generated
 */
public class QualificationWrapper implements Qualification,
    ModelWrapper<Qualification> {
    private Qualification _qualification;

    public QualificationWrapper(Qualification qualification) {
        _qualification = qualification;
    }

    @Override
    public Class<?> getModelClass() {
        return Qualification.class;
    }

    @Override
    public String getModelClassName() {
        return Qualification.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("qualificationId", getQualificationId());
        attributes.put("creatorId", getCreatorId());
        attributes.put("creatorName", getCreatorName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifierId", getModifierId());
        attributes.put("modifierName", getModifierName());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("ownerId", getOwnerId());
        attributes.put("typeId", getTypeId());
        attributes.put("name", getName());
        attributes.put("certNumber", getCertNumber());
        attributes.put("deliveryDate", getDeliveryDate());
        attributes.put("expirationDate", getExpirationDate());
        attributes.put("issuingCountry", getIssuingCountry());
        attributes.put("issuingInstitution", getIssuingInstitution());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long qualificationId = (Long) attributes.get("qualificationId");

        if (qualificationId != null) {
            setQualificationId(qualificationId);
        }

        Long creatorId = (Long) attributes.get("creatorId");

        if (creatorId != null) {
            setCreatorId(creatorId);
        }

        String creatorName = (String) attributes.get("creatorName");

        if (creatorName != null) {
            setCreatorName(creatorName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Long modifierId = (Long) attributes.get("modifierId");

        if (modifierId != null) {
            setModifierId(modifierId);
        }

        String modifierName = (String) attributes.get("modifierName");

        if (modifierName != null) {
            setModifierName(modifierName);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        Long ownerId = (Long) attributes.get("ownerId");

        if (ownerId != null) {
            setOwnerId(ownerId);
        }

        Long typeId = (Long) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        String certNumber = (String) attributes.get("certNumber");

        if (certNumber != null) {
            setCertNumber(certNumber);
        }

        Date deliveryDate = (Date) attributes.get("deliveryDate");

        if (deliveryDate != null) {
            setDeliveryDate(deliveryDate);
        }

        Date expirationDate = (Date) attributes.get("expirationDate");

        if (expirationDate != null) {
            setExpirationDate(expirationDate);
        }

        String issuingCountry = (String) attributes.get("issuingCountry");

        if (issuingCountry != null) {
            setIssuingCountry(issuingCountry);
        }

        String issuingInstitution = (String) attributes.get(
                "issuingInstitution");

        if (issuingInstitution != null) {
            setIssuingInstitution(issuingInstitution);
        }
    }

    /**
    * Returns the primary key of this qualification.
    *
    * @return the primary key of this qualification
    */
    @Override
    public long getPrimaryKey() {
        return _qualification.getPrimaryKey();
    }

    /**
    * Sets the primary key of this qualification.
    *
    * @param primaryKey the primary key of this qualification
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _qualification.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the qualification ID of this qualification.
    *
    * @return the qualification ID of this qualification
    */
    @Override
    public long getQualificationId() {
        return _qualification.getQualificationId();
    }

    /**
    * Sets the qualification ID of this qualification.
    *
    * @param qualificationId the qualification ID of this qualification
    */
    @Override
    public void setQualificationId(long qualificationId) {
        _qualification.setQualificationId(qualificationId);
    }

    /**
    * Returns the creator ID of this qualification.
    *
    * @return the creator ID of this qualification
    */
    @Override
    public long getCreatorId() {
        return _qualification.getCreatorId();
    }

    /**
    * Sets the creator ID of this qualification.
    *
    * @param creatorId the creator ID of this qualification
    */
    @Override
    public void setCreatorId(long creatorId) {
        _qualification.setCreatorId(creatorId);
    }

    /**
    * Returns the creator name of this qualification.
    *
    * @return the creator name of this qualification
    */
    @Override
    public java.lang.String getCreatorName() {
        return _qualification.getCreatorName();
    }

    /**
    * Sets the creator name of this qualification.
    *
    * @param creatorName the creator name of this qualification
    */
    @Override
    public void setCreatorName(java.lang.String creatorName) {
        _qualification.setCreatorName(creatorName);
    }

    /**
    * Returns the create date of this qualification.
    *
    * @return the create date of this qualification
    */
    @Override
    public java.util.Date getCreateDate() {
        return _qualification.getCreateDate();
    }

    /**
    * Sets the create date of this qualification.
    *
    * @param createDate the create date of this qualification
    */
    @Override
    public void setCreateDate(java.util.Date createDate) {
        _qualification.setCreateDate(createDate);
    }

    /**
    * Returns the modifier ID of this qualification.
    *
    * @return the modifier ID of this qualification
    */
    @Override
    public long getModifierId() {
        return _qualification.getModifierId();
    }

    /**
    * Sets the modifier ID of this qualification.
    *
    * @param modifierId the modifier ID of this qualification
    */
    @Override
    public void setModifierId(long modifierId) {
        _qualification.setModifierId(modifierId);
    }

    /**
    * Returns the modifier name of this qualification.
    *
    * @return the modifier name of this qualification
    */
    @Override
    public java.lang.String getModifierName() {
        return _qualification.getModifierName();
    }

    /**
    * Sets the modifier name of this qualification.
    *
    * @param modifierName the modifier name of this qualification
    */
    @Override
    public void setModifierName(java.lang.String modifierName) {
        _qualification.setModifierName(modifierName);
    }

    /**
    * Returns the modified date of this qualification.
    *
    * @return the modified date of this qualification
    */
    @Override
    public java.util.Date getModifiedDate() {
        return _qualification.getModifiedDate();
    }

    /**
    * Sets the modified date of this qualification.
    *
    * @param modifiedDate the modified date of this qualification
    */
    @Override
    public void setModifiedDate(java.util.Date modifiedDate) {
        _qualification.setModifiedDate(modifiedDate);
    }

    /**
    * Returns the owner ID of this qualification.
    *
    * @return the owner ID of this qualification
    */
    @Override
    public long getOwnerId() {
        return _qualification.getOwnerId();
    }

    /**
    * Sets the owner ID of this qualification.
    *
    * @param ownerId the owner ID of this qualification
    */
    @Override
    public void setOwnerId(long ownerId) {
        _qualification.setOwnerId(ownerId);
    }

    /**
    * Returns the type ID of this qualification.
    *
    * @return the type ID of this qualification
    */
    @Override
    public long getTypeId() {
        return _qualification.getTypeId();
    }

    /**
    * Sets the type ID of this qualification.
    *
    * @param typeId the type ID of this qualification
    */
    @Override
    public void setTypeId(long typeId) {
        _qualification.setTypeId(typeId);
    }

    /**
    * Returns the name of this qualification.
    *
    * @return the name of this qualification
    */
    @Override
    public java.lang.String getName() {
        return _qualification.getName();
    }

    /**
    * Sets the name of this qualification.
    *
    * @param name the name of this qualification
    */
    @Override
    public void setName(java.lang.String name) {
        _qualification.setName(name);
    }

    /**
    * Returns the cert number of this qualification.
    *
    * @return the cert number of this qualification
    */
    @Override
    public java.lang.String getCertNumber() {
        return _qualification.getCertNumber();
    }

    /**
    * Sets the cert number of this qualification.
    *
    * @param certNumber the cert number of this qualification
    */
    @Override
    public void setCertNumber(java.lang.String certNumber) {
        _qualification.setCertNumber(certNumber);
    }

    /**
    * Returns the delivery date of this qualification.
    *
    * @return the delivery date of this qualification
    */
    @Override
    public java.util.Date getDeliveryDate() {
        return _qualification.getDeliveryDate();
    }

    /**
    * Sets the delivery date of this qualification.
    *
    * @param deliveryDate the delivery date of this qualification
    */
    @Override
    public void setDeliveryDate(java.util.Date deliveryDate) {
        _qualification.setDeliveryDate(deliveryDate);
    }

    /**
    * Returns the expiration date of this qualification.
    *
    * @return the expiration date of this qualification
    */
    @Override
    public java.util.Date getExpirationDate() {
        return _qualification.getExpirationDate();
    }

    /**
    * Sets the expiration date of this qualification.
    *
    * @param expirationDate the expiration date of this qualification
    */
    @Override
    public void setExpirationDate(java.util.Date expirationDate) {
        _qualification.setExpirationDate(expirationDate);
    }

    /**
    * Returns the issuing country of this qualification.
    *
    * @return the issuing country of this qualification
    */
    @Override
    public java.lang.String getIssuingCountry() {
        return _qualification.getIssuingCountry();
    }

    /**
    * Sets the issuing country of this qualification.
    *
    * @param issuingCountry the issuing country of this qualification
    */
    @Override
    public void setIssuingCountry(java.lang.String issuingCountry) {
        _qualification.setIssuingCountry(issuingCountry);
    }

    /**
    * Returns the issuing institution of this qualification.
    *
    * @return the issuing institution of this qualification
    */
    @Override
    public java.lang.String getIssuingInstitution() {
        return _qualification.getIssuingInstitution();
    }

    /**
    * Sets the issuing institution of this qualification.
    *
    * @param issuingInstitution the issuing institution of this qualification
    */
    @Override
    public void setIssuingInstitution(java.lang.String issuingInstitution) {
        _qualification.setIssuingInstitution(issuingInstitution);
    }

    @Override
    public boolean isNew() {
        return _qualification.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _qualification.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _qualification.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _qualification.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _qualification.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _qualification.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _qualification.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _qualification.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _qualification.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _qualification.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _qualification.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new QualificationWrapper((Qualification) _qualification.clone());
    }

    @Override
    public int compareTo(Qualification qualification) {
        return _qualification.compareTo(qualification);
    }

    @Override
    public int hashCode() {
        return _qualification.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<Qualification> toCacheModel() {
        return _qualification.toCacheModel();
    }

    @Override
    public Qualification toEscapedModel() {
        return new QualificationWrapper(_qualification.toEscapedModel());
    }

    @Override
    public Qualification toUnescapedModel() {
        return new QualificationWrapper(_qualification.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _qualification.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _qualification.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _qualification.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof QualificationWrapper)) {
            return false;
        }

        QualificationWrapper qualificationWrapper = (QualificationWrapper) obj;

        if (Validator.equals(_qualification, qualificationWrapper._qualification)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Qualification getWrappedQualification() {
        return _qualification;
    }

    @Override
    public Qualification getWrappedModel() {
        return _qualification;
    }

    @Override
    public void resetOriginalValues() {
        _qualification.resetOriginalValues();
    }
}
