package de.humance.eco.profile.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Trailer service. Represents a row in the &quot;Profile_Trailer&quot; database table, with each column mapped to a property of this class.
 *
 * @author Humance
 * @see TrailerModel
 * @see de.humance.eco.profile.model.impl.TrailerImpl
 * @see de.humance.eco.profile.model.impl.TrailerModelImpl
 * @generated
 */
public interface Trailer extends TrailerModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link de.humance.eco.profile.model.impl.TrailerImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
