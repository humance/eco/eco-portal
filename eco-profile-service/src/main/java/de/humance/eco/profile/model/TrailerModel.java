package de.humance.eco.profile.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the Trailer service. Represents a row in the &quot;Profile_Trailer&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link de.humance.eco.profile.model.impl.TrailerModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link de.humance.eco.profile.model.impl.TrailerImpl}.
 * </p>
 *
 * @author Humance
 * @see Trailer
 * @see de.humance.eco.profile.model.impl.TrailerImpl
 * @see de.humance.eco.profile.model.impl.TrailerModelImpl
 * @generated
 */
public interface TrailerModel extends BaseModel<Trailer> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. All methods that expect a trailer model instance should use the {@link Trailer} interface instead.
     */

    /**
     * Returns the primary key of this trailer.
     *
     * @return the primary key of this trailer
     */
    public long getPrimaryKey();

    /**
     * Sets the primary key of this trailer.
     *
     * @param primaryKey the primary key of this trailer
     */
    public void setPrimaryKey(long primaryKey);

    /**
     * Returns the trailer ID of this trailer.
     *
     * @return the trailer ID of this trailer
     */
    public long getTrailerId();

    /**
     * Sets the trailer ID of this trailer.
     *
     * @param trailerId the trailer ID of this trailer
     */
    public void setTrailerId(long trailerId);

    /**
     * Returns the creator ID of this trailer.
     *
     * @return the creator ID of this trailer
     */
    public long getCreatorId();

    /**
     * Sets the creator ID of this trailer.
     *
     * @param creatorId the creator ID of this trailer
     */
    public void setCreatorId(long creatorId);

    /**
     * Returns the creator name of this trailer.
     *
     * @return the creator name of this trailer
     */
    @AutoEscape
    public String getCreatorName();

    /**
     * Sets the creator name of this trailer.
     *
     * @param creatorName the creator name of this trailer
     */
    public void setCreatorName(String creatorName);

    /**
     * Returns the create date of this trailer.
     *
     * @return the create date of this trailer
     */
    public Date getCreateDate();

    /**
     * Sets the create date of this trailer.
     *
     * @param createDate the create date of this trailer
     */
    public void setCreateDate(Date createDate);

    /**
     * Returns the modifier ID of this trailer.
     *
     * @return the modifier ID of this trailer
     */
    public long getModifierId();

    /**
     * Sets the modifier ID of this trailer.
     *
     * @param modifierId the modifier ID of this trailer
     */
    public void setModifierId(long modifierId);

    /**
     * Returns the modifier name of this trailer.
     *
     * @return the modifier name of this trailer
     */
    @AutoEscape
    public String getModifierName();

    /**
     * Sets the modifier name of this trailer.
     *
     * @param modifierName the modifier name of this trailer
     */
    public void setModifierName(String modifierName);

    /**
     * Returns the modified date of this trailer.
     *
     * @return the modified date of this trailer
     */
    public Date getModifiedDate();

    /**
     * Sets the modified date of this trailer.
     *
     * @param modifiedDate the modified date of this trailer
     */
    public void setModifiedDate(Date modifiedDate);

    /**
     * Returns the driver ID of this trailer.
     *
     * @return the driver ID of this trailer
     */
    public long getDriverId();

    /**
     * Sets the driver ID of this trailer.
     *
     * @param driverId the driver ID of this trailer
     */
    public void setDriverId(long driverId);

    /**
     * Returns the type ID of this trailer.
     *
     * @return the type ID of this trailer
     */
    public long getTypeId();

    /**
     * Sets the type ID of this trailer.
     *
     * @param typeId the type ID of this trailer
     */
    public void setTypeId(long typeId);

    /**
     * Returns the manufacturer ID of this trailer.
     *
     * @return the manufacturer ID of this trailer
     */
    public long getManufacturerId();

    /**
     * Sets the manufacturer ID of this trailer.
     *
     * @param manufacturerId the manufacturer ID of this trailer
     */
    public void setManufacturerId(long manufacturerId);

    /**
     * Returns the organization ID of this trailer.
     *
     * @return the organization ID of this trailer
     */
    public long getOrganizationId();

    /**
     * Sets the organization ID of this trailer.
     *
     * @param organizationId the organization ID of this trailer
     */
    public void setOrganizationId(long organizationId);

    /**
     * Returns the model name of this trailer.
     *
     * @return the model name of this trailer
     */
    @AutoEscape
    public String getModelName();

    /**
     * Sets the model name of this trailer.
     *
     * @param modelName the model name of this trailer
     */
    public void setModelName(String modelName);

    /**
     * Returns the dimension height of this trailer.
     *
     * @return the dimension height of this trailer
     */
    public long getDimensionHeight();

    /**
     * Sets the dimension height of this trailer.
     *
     * @param dimensionHeight the dimension height of this trailer
     */
    public void setDimensionHeight(long dimensionHeight);

    /**
     * Returns the dimension width of this trailer.
     *
     * @return the dimension width of this trailer
     */
    public long getDimensionWidth();

    /**
     * Sets the dimension width of this trailer.
     *
     * @param dimensionWidth the dimension width of this trailer
     */
    public void setDimensionWidth(long dimensionWidth);

    /**
     * Returns the dimension depth of this trailer.
     *
     * @return the dimension depth of this trailer
     */
    public long getDimensionDepth();

    /**
     * Sets the dimension depth of this trailer.
     *
     * @param dimensionDepth the dimension depth of this trailer
     */
    public void setDimensionDepth(long dimensionDepth);

    /**
     * Returns the license plate of this trailer.
     *
     * @return the license plate of this trailer
     */
    @AutoEscape
    public String getLicensePlate();

    /**
     * Sets the license plate of this trailer.
     *
     * @param licensePlate the license plate of this trailer
     */
    public void setLicensePlate(String licensePlate);

    @Override
    public boolean isNew();

    @Override
    public void setNew(boolean n);

    @Override
    public boolean isCachedModel();

    @Override
    public void setCachedModel(boolean cachedModel);

    @Override
    public boolean isEscapedModel();

    @Override
    public Serializable getPrimaryKeyObj();

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj);

    @Override
    public ExpandoBridge getExpandoBridge();

    @Override
    public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

    @Override
    public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

    @Override
    public void setExpandoBridgeAttributes(ServiceContext serviceContext);

    @Override
    public Object clone();

    @Override
    public int compareTo(Trailer trailer);

    @Override
    public int hashCode();

    @Override
    public CacheModel<Trailer> toCacheModel();

    @Override
    public Trailer toEscapedModel();

    @Override
    public Trailer toUnescapedModel();

    @Override
    public String toString();

    @Override
    public String toXmlString();
}
