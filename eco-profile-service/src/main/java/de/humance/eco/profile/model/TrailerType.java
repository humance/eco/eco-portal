package de.humance.eco.profile.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the TrailerType service. Represents a row in the &quot;Profile_TrailerType&quot; database table, with each column mapped to a property of this class.
 *
 * @author Humance
 * @see TrailerTypeModel
 * @see de.humance.eco.profile.model.impl.TrailerTypeImpl
 * @see de.humance.eco.profile.model.impl.TrailerTypeModelImpl
 * @generated
 */
public interface TrailerType extends TrailerTypeModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link de.humance.eco.profile.model.impl.TrailerTypeImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
