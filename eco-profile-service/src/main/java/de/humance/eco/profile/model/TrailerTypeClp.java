package de.humance.eco.profile.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import de.humance.eco.profile.service.ClpSerializer;
import de.humance.eco.profile.service.TrailerTypeLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class TrailerTypeClp extends BaseModelImpl<TrailerType>
    implements TrailerType {
    private long _trailerTypeId;
    private long _creatorId;
    private String _creatorName;
    private Date _createDate;
    private long _modifierId;
    private String _modifierName;
    private Date _modifiedDate;
    private String _name;
    private long _iconId;
    private BaseModel<?> _trailerTypeRemoteModel;

    public TrailerTypeClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return TrailerType.class;
    }

    @Override
    public String getModelClassName() {
        return TrailerType.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _trailerTypeId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setTrailerTypeId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _trailerTypeId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("trailerTypeId", getTrailerTypeId());
        attributes.put("creatorId", getCreatorId());
        attributes.put("creatorName", getCreatorName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifierId", getModifierId());
        attributes.put("modifierName", getModifierName());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("name", getName());
        attributes.put("iconId", getIconId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long trailerTypeId = (Long) attributes.get("trailerTypeId");

        if (trailerTypeId != null) {
            setTrailerTypeId(trailerTypeId);
        }

        Long creatorId = (Long) attributes.get("creatorId");

        if (creatorId != null) {
            setCreatorId(creatorId);
        }

        String creatorName = (String) attributes.get("creatorName");

        if (creatorName != null) {
            setCreatorName(creatorName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Long modifierId = (Long) attributes.get("modifierId");

        if (modifierId != null) {
            setModifierId(modifierId);
        }

        String modifierName = (String) attributes.get("modifierName");

        if (modifierName != null) {
            setModifierName(modifierName);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        Long iconId = (Long) attributes.get("iconId");

        if (iconId != null) {
            setIconId(iconId);
        }
    }

    @Override
    public long getTrailerTypeId() {
        return _trailerTypeId;
    }

    @Override
    public void setTrailerTypeId(long trailerTypeId) {
        _trailerTypeId = trailerTypeId;

        if (_trailerTypeRemoteModel != null) {
            try {
                Class<?> clazz = _trailerTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setTrailerTypeId", long.class);

                method.invoke(_trailerTypeRemoteModel, trailerTypeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getCreatorId() {
        return _creatorId;
    }

    @Override
    public void setCreatorId(long creatorId) {
        _creatorId = creatorId;

        if (_trailerTypeRemoteModel != null) {
            try {
                Class<?> clazz = _trailerTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setCreatorId", long.class);

                method.invoke(_trailerTypeRemoteModel, creatorId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCreatorName() {
        return _creatorName;
    }

    @Override
    public void setCreatorName(String creatorName) {
        _creatorName = creatorName;

        if (_trailerTypeRemoteModel != null) {
            try {
                Class<?> clazz = _trailerTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setCreatorName", String.class);

                method.invoke(_trailerTypeRemoteModel, creatorName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getCreateDate() {
        return _createDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        _createDate = createDate;

        if (_trailerTypeRemoteModel != null) {
            try {
                Class<?> clazz = _trailerTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setCreateDate", Date.class);

                method.invoke(_trailerTypeRemoteModel, createDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getModifierId() {
        return _modifierId;
    }

    @Override
    public void setModifierId(long modifierId) {
        _modifierId = modifierId;

        if (_trailerTypeRemoteModel != null) {
            try {
                Class<?> clazz = _trailerTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setModifierId", long.class);

                method.invoke(_trailerTypeRemoteModel, modifierId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getModifierName() {
        return _modifierName;
    }

    @Override
    public void setModifierName(String modifierName) {
        _modifierName = modifierName;

        if (_trailerTypeRemoteModel != null) {
            try {
                Class<?> clazz = _trailerTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setModifierName", String.class);

                method.invoke(_trailerTypeRemoteModel, modifierName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getModifiedDate() {
        return _modifiedDate;
    }

    @Override
    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;

        if (_trailerTypeRemoteModel != null) {
            try {
                Class<?> clazz = _trailerTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setModifiedDate", Date.class);

                method.invoke(_trailerTypeRemoteModel, modifiedDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getName() {
        return _name;
    }

    @Override
    public void setName(String name) {
        _name = name;

        if (_trailerTypeRemoteModel != null) {
            try {
                Class<?> clazz = _trailerTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setName", String.class);

                method.invoke(_trailerTypeRemoteModel, name);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getIconId() {
        return _iconId;
    }

    @Override
    public void setIconId(long iconId) {
        _iconId = iconId;

        if (_trailerTypeRemoteModel != null) {
            try {
                Class<?> clazz = _trailerTypeRemoteModel.getClass();

                Method method = clazz.getMethod("setIconId", long.class);

                method.invoke(_trailerTypeRemoteModel, iconId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getTrailerTypeRemoteModel() {
        return _trailerTypeRemoteModel;
    }

    public void setTrailerTypeRemoteModel(BaseModel<?> trailerTypeRemoteModel) {
        _trailerTypeRemoteModel = trailerTypeRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _trailerTypeRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_trailerTypeRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            TrailerTypeLocalServiceUtil.addTrailerType(this);
        } else {
            TrailerTypeLocalServiceUtil.updateTrailerType(this);
        }
    }

    @Override
    public TrailerType toEscapedModel() {
        return (TrailerType) ProxyUtil.newProxyInstance(TrailerType.class.getClassLoader(),
            new Class[] { TrailerType.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        TrailerTypeClp clone = new TrailerTypeClp();

        clone.setTrailerTypeId(getTrailerTypeId());
        clone.setCreatorId(getCreatorId());
        clone.setCreatorName(getCreatorName());
        clone.setCreateDate(getCreateDate());
        clone.setModifierId(getModifierId());
        clone.setModifierName(getModifierName());
        clone.setModifiedDate(getModifiedDate());
        clone.setName(getName());
        clone.setIconId(getIconId());

        return clone;
    }

    @Override
    public int compareTo(TrailerType trailerType) {
        int value = 0;

        value = getName().compareToIgnoreCase(trailerType.getName());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TrailerTypeClp)) {
            return false;
        }

        TrailerTypeClp trailerType = (TrailerTypeClp) obj;

        long primaryKey = trailerType.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(19);

        sb.append("{trailerTypeId=");
        sb.append(getTrailerTypeId());
        sb.append(", creatorId=");
        sb.append(getCreatorId());
        sb.append(", creatorName=");
        sb.append(getCreatorName());
        sb.append(", createDate=");
        sb.append(getCreateDate());
        sb.append(", modifierId=");
        sb.append(getModifierId());
        sb.append(", modifierName=");
        sb.append(getModifierName());
        sb.append(", modifiedDate=");
        sb.append(getModifiedDate());
        sb.append(", name=");
        sb.append(getName());
        sb.append(", iconId=");
        sb.append(getIconId());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(31);

        sb.append("<model><model-name>");
        sb.append("de.humance.eco.profile.model.TrailerType");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>trailerTypeId</column-name><column-value><![CDATA[");
        sb.append(getTrailerTypeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>creatorId</column-name><column-value><![CDATA[");
        sb.append(getCreatorId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>creatorName</column-name><column-value><![CDATA[");
        sb.append(getCreatorName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>createDate</column-name><column-value><![CDATA[");
        sb.append(getCreateDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifierId</column-name><column-value><![CDATA[");
        sb.append(getModifierId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifierName</column-name><column-value><![CDATA[");
        sb.append(getModifierName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
        sb.append(getModifiedDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>name</column-name><column-value><![CDATA[");
        sb.append(getName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>iconId</column-name><column-value><![CDATA[");
        sb.append(getIconId());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
