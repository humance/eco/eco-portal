package de.humance.eco.profile.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link TrailerType}.
 * </p>
 *
 * @author Humance
 * @see TrailerType
 * @generated
 */
public class TrailerTypeWrapper implements TrailerType,
    ModelWrapper<TrailerType> {
    private TrailerType _trailerType;

    public TrailerTypeWrapper(TrailerType trailerType) {
        _trailerType = trailerType;
    }

    @Override
    public Class<?> getModelClass() {
        return TrailerType.class;
    }

    @Override
    public String getModelClassName() {
        return TrailerType.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("trailerTypeId", getTrailerTypeId());
        attributes.put("creatorId", getCreatorId());
        attributes.put("creatorName", getCreatorName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifierId", getModifierId());
        attributes.put("modifierName", getModifierName());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("name", getName());
        attributes.put("iconId", getIconId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long trailerTypeId = (Long) attributes.get("trailerTypeId");

        if (trailerTypeId != null) {
            setTrailerTypeId(trailerTypeId);
        }

        Long creatorId = (Long) attributes.get("creatorId");

        if (creatorId != null) {
            setCreatorId(creatorId);
        }

        String creatorName = (String) attributes.get("creatorName");

        if (creatorName != null) {
            setCreatorName(creatorName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Long modifierId = (Long) attributes.get("modifierId");

        if (modifierId != null) {
            setModifierId(modifierId);
        }

        String modifierName = (String) attributes.get("modifierName");

        if (modifierName != null) {
            setModifierName(modifierName);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        Long iconId = (Long) attributes.get("iconId");

        if (iconId != null) {
            setIconId(iconId);
        }
    }

    /**
    * Returns the primary key of this trailer type.
    *
    * @return the primary key of this trailer type
    */
    @Override
    public long getPrimaryKey() {
        return _trailerType.getPrimaryKey();
    }

    /**
    * Sets the primary key of this trailer type.
    *
    * @param primaryKey the primary key of this trailer type
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _trailerType.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the trailer type ID of this trailer type.
    *
    * @return the trailer type ID of this trailer type
    */
    @Override
    public long getTrailerTypeId() {
        return _trailerType.getTrailerTypeId();
    }

    /**
    * Sets the trailer type ID of this trailer type.
    *
    * @param trailerTypeId the trailer type ID of this trailer type
    */
    @Override
    public void setTrailerTypeId(long trailerTypeId) {
        _trailerType.setTrailerTypeId(trailerTypeId);
    }

    /**
    * Returns the creator ID of this trailer type.
    *
    * @return the creator ID of this trailer type
    */
    @Override
    public long getCreatorId() {
        return _trailerType.getCreatorId();
    }

    /**
    * Sets the creator ID of this trailer type.
    *
    * @param creatorId the creator ID of this trailer type
    */
    @Override
    public void setCreatorId(long creatorId) {
        _trailerType.setCreatorId(creatorId);
    }

    /**
    * Returns the creator name of this trailer type.
    *
    * @return the creator name of this trailer type
    */
    @Override
    public java.lang.String getCreatorName() {
        return _trailerType.getCreatorName();
    }

    /**
    * Sets the creator name of this trailer type.
    *
    * @param creatorName the creator name of this trailer type
    */
    @Override
    public void setCreatorName(java.lang.String creatorName) {
        _trailerType.setCreatorName(creatorName);
    }

    /**
    * Returns the create date of this trailer type.
    *
    * @return the create date of this trailer type
    */
    @Override
    public java.util.Date getCreateDate() {
        return _trailerType.getCreateDate();
    }

    /**
    * Sets the create date of this trailer type.
    *
    * @param createDate the create date of this trailer type
    */
    @Override
    public void setCreateDate(java.util.Date createDate) {
        _trailerType.setCreateDate(createDate);
    }

    /**
    * Returns the modifier ID of this trailer type.
    *
    * @return the modifier ID of this trailer type
    */
    @Override
    public long getModifierId() {
        return _trailerType.getModifierId();
    }

    /**
    * Sets the modifier ID of this trailer type.
    *
    * @param modifierId the modifier ID of this trailer type
    */
    @Override
    public void setModifierId(long modifierId) {
        _trailerType.setModifierId(modifierId);
    }

    /**
    * Returns the modifier name of this trailer type.
    *
    * @return the modifier name of this trailer type
    */
    @Override
    public java.lang.String getModifierName() {
        return _trailerType.getModifierName();
    }

    /**
    * Sets the modifier name of this trailer type.
    *
    * @param modifierName the modifier name of this trailer type
    */
    @Override
    public void setModifierName(java.lang.String modifierName) {
        _trailerType.setModifierName(modifierName);
    }

    /**
    * Returns the modified date of this trailer type.
    *
    * @return the modified date of this trailer type
    */
    @Override
    public java.util.Date getModifiedDate() {
        return _trailerType.getModifiedDate();
    }

    /**
    * Sets the modified date of this trailer type.
    *
    * @param modifiedDate the modified date of this trailer type
    */
    @Override
    public void setModifiedDate(java.util.Date modifiedDate) {
        _trailerType.setModifiedDate(modifiedDate);
    }

    /**
    * Returns the name of this trailer type.
    *
    * @return the name of this trailer type
    */
    @Override
    public java.lang.String getName() {
        return _trailerType.getName();
    }

    /**
    * Sets the name of this trailer type.
    *
    * @param name the name of this trailer type
    */
    @Override
    public void setName(java.lang.String name) {
        _trailerType.setName(name);
    }

    /**
    * Returns the icon ID of this trailer type.
    *
    * @return the icon ID of this trailer type
    */
    @Override
    public long getIconId() {
        return _trailerType.getIconId();
    }

    /**
    * Sets the icon ID of this trailer type.
    *
    * @param iconId the icon ID of this trailer type
    */
    @Override
    public void setIconId(long iconId) {
        _trailerType.setIconId(iconId);
    }

    @Override
    public boolean isNew() {
        return _trailerType.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _trailerType.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _trailerType.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _trailerType.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _trailerType.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _trailerType.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _trailerType.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _trailerType.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _trailerType.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _trailerType.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _trailerType.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new TrailerTypeWrapper((TrailerType) _trailerType.clone());
    }

    @Override
    public int compareTo(TrailerType trailerType) {
        return _trailerType.compareTo(trailerType);
    }

    @Override
    public int hashCode() {
        return _trailerType.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<TrailerType> toCacheModel() {
        return _trailerType.toCacheModel();
    }

    @Override
    public TrailerType toEscapedModel() {
        return new TrailerTypeWrapper(_trailerType.toEscapedModel());
    }

    @Override
    public TrailerType toUnescapedModel() {
        return new TrailerTypeWrapper(_trailerType.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _trailerType.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _trailerType.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _trailerType.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TrailerTypeWrapper)) {
            return false;
        }

        TrailerTypeWrapper trailerTypeWrapper = (TrailerTypeWrapper) obj;

        if (Validator.equals(_trailerType, trailerTypeWrapper._trailerType)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public TrailerType getWrappedTrailerType() {
        return _trailerType;
    }

    @Override
    public TrailerType getWrappedModel() {
        return _trailerType;
    }

    @Override
    public void resetOriginalValues() {
        _trailerType.resetOriginalValues();
    }
}
