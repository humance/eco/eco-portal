package de.humance.eco.profile.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the TrailerUser service. Represents a row in the &quot;Profile_TrailerUser&quot; database table, with each column mapped to a property of this class.
 *
 * @author Humance
 * @see TrailerUserModel
 * @see de.humance.eco.profile.model.impl.TrailerUserImpl
 * @see de.humance.eco.profile.model.impl.TrailerUserModelImpl
 * @generated
 */
public interface TrailerUser extends TrailerUserModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link de.humance.eco.profile.model.impl.TrailerUserImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
