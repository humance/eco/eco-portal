package de.humance.eco.profile.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import de.humance.eco.profile.service.ClpSerializer;
import de.humance.eco.profile.service.TrailerUserLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class TrailerUserClp extends BaseModelImpl<TrailerUser>
    implements TrailerUser {
    private long _trailerUserId;
    private String _trailerUserUuid;
    private long _userId;
    private String _userUuid;
    private long _trailerId;
    private BaseModel<?> _trailerUserRemoteModel;

    public TrailerUserClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return TrailerUser.class;
    }

    @Override
    public String getModelClassName() {
        return TrailerUser.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _trailerUserId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setTrailerUserId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _trailerUserId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("trailerUserId", getTrailerUserId());
        attributes.put("userId", getUserId());
        attributes.put("trailerId", getTrailerId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long trailerUserId = (Long) attributes.get("trailerUserId");

        if (trailerUserId != null) {
            setTrailerUserId(trailerUserId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        Long trailerId = (Long) attributes.get("trailerId");

        if (trailerId != null) {
            setTrailerId(trailerId);
        }
    }

    @Override
    public long getTrailerUserId() {
        return _trailerUserId;
    }

    @Override
    public void setTrailerUserId(long trailerUserId) {
        _trailerUserId = trailerUserId;

        if (_trailerUserRemoteModel != null) {
            try {
                Class<?> clazz = _trailerUserRemoteModel.getClass();

                Method method = clazz.getMethod("setTrailerUserId", long.class);

                method.invoke(_trailerUserRemoteModel, trailerUserId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTrailerUserUuid() throws SystemException {
        return PortalUtil.getUserValue(getTrailerUserId(), "uuid",
            _trailerUserUuid);
    }

    @Override
    public void setTrailerUserUuid(String trailerUserUuid) {
        _trailerUserUuid = trailerUserUuid;
    }

    @Override
    public long getUserId() {
        return _userId;
    }

    @Override
    public void setUserId(long userId) {
        _userId = userId;

        if (_trailerUserRemoteModel != null) {
            try {
                Class<?> clazz = _trailerUserRemoteModel.getClass();

                Method method = clazz.getMethod("setUserId", long.class);

                method.invoke(_trailerUserRemoteModel, userId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getUserUuid() throws SystemException {
        return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
    }

    @Override
    public void setUserUuid(String userUuid) {
        _userUuid = userUuid;
    }

    @Override
    public long getTrailerId() {
        return _trailerId;
    }

    @Override
    public void setTrailerId(long trailerId) {
        _trailerId = trailerId;

        if (_trailerUserRemoteModel != null) {
            try {
                Class<?> clazz = _trailerUserRemoteModel.getClass();

                Method method = clazz.getMethod("setTrailerId", long.class);

                method.invoke(_trailerUserRemoteModel, trailerId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getTrailerUserRemoteModel() {
        return _trailerUserRemoteModel;
    }

    public void setTrailerUserRemoteModel(BaseModel<?> trailerUserRemoteModel) {
        _trailerUserRemoteModel = trailerUserRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _trailerUserRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_trailerUserRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            TrailerUserLocalServiceUtil.addTrailerUser(this);
        } else {
            TrailerUserLocalServiceUtil.updateTrailerUser(this);
        }
    }

    @Override
    public TrailerUser toEscapedModel() {
        return (TrailerUser) ProxyUtil.newProxyInstance(TrailerUser.class.getClassLoader(),
            new Class[] { TrailerUser.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        TrailerUserClp clone = new TrailerUserClp();

        clone.setTrailerUserId(getTrailerUserId());
        clone.setUserId(getUserId());
        clone.setTrailerId(getTrailerId());

        return clone;
    }

    @Override
    public int compareTo(TrailerUser trailerUser) {
        int value = 0;

        if (getUserId() < trailerUser.getUserId()) {
            value = -1;
        } else if (getUserId() > trailerUser.getUserId()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TrailerUserClp)) {
            return false;
        }

        TrailerUserClp trailerUser = (TrailerUserClp) obj;

        long primaryKey = trailerUser.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{trailerUserId=");
        sb.append(getTrailerUserId());
        sb.append(", userId=");
        sb.append(getUserId());
        sb.append(", trailerId=");
        sb.append(getTrailerId());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(13);

        sb.append("<model><model-name>");
        sb.append("de.humance.eco.profile.model.TrailerUser");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>trailerUserId</column-name><column-value><![CDATA[");
        sb.append(getTrailerUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userId</column-name><column-value><![CDATA[");
        sb.append(getUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>trailerId</column-name><column-value><![CDATA[");
        sb.append(getTrailerId());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
