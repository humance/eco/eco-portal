package de.humance.eco.profile.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Humance
 * @generated
 */
public class TrailerUserSoap implements Serializable {
    private long _trailerUserId;
    private long _userId;
    private long _trailerId;

    public TrailerUserSoap() {
    }

    public static TrailerUserSoap toSoapModel(TrailerUser model) {
        TrailerUserSoap soapModel = new TrailerUserSoap();

        soapModel.setTrailerUserId(model.getTrailerUserId());
        soapModel.setUserId(model.getUserId());
        soapModel.setTrailerId(model.getTrailerId());

        return soapModel;
    }

    public static TrailerUserSoap[] toSoapModels(TrailerUser[] models) {
        TrailerUserSoap[] soapModels = new TrailerUserSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static TrailerUserSoap[][] toSoapModels(TrailerUser[][] models) {
        TrailerUserSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new TrailerUserSoap[models.length][models[0].length];
        } else {
            soapModels = new TrailerUserSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static TrailerUserSoap[] toSoapModels(List<TrailerUser> models) {
        List<TrailerUserSoap> soapModels = new ArrayList<TrailerUserSoap>(models.size());

        for (TrailerUser model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new TrailerUserSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _trailerUserId;
    }

    public void setPrimaryKey(long pk) {
        setTrailerUserId(pk);
    }

    public long getTrailerUserId() {
        return _trailerUserId;
    }

    public void setTrailerUserId(long trailerUserId) {
        _trailerUserId = trailerUserId;
    }

    public long getUserId() {
        return _userId;
    }

    public void setUserId(long userId) {
        _userId = userId;
    }

    public long getTrailerId() {
        return _trailerId;
    }

    public void setTrailerId(long trailerId) {
        _trailerId = trailerId;
    }
}
