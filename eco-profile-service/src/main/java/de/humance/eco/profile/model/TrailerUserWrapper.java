package de.humance.eco.profile.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link TrailerUser}.
 * </p>
 *
 * @author Humance
 * @see TrailerUser
 * @generated
 */
public class TrailerUserWrapper implements TrailerUser,
    ModelWrapper<TrailerUser> {
    private TrailerUser _trailerUser;

    public TrailerUserWrapper(TrailerUser trailerUser) {
        _trailerUser = trailerUser;
    }

    @Override
    public Class<?> getModelClass() {
        return TrailerUser.class;
    }

    @Override
    public String getModelClassName() {
        return TrailerUser.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("trailerUserId", getTrailerUserId());
        attributes.put("userId", getUserId());
        attributes.put("trailerId", getTrailerId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long trailerUserId = (Long) attributes.get("trailerUserId");

        if (trailerUserId != null) {
            setTrailerUserId(trailerUserId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        Long trailerId = (Long) attributes.get("trailerId");

        if (trailerId != null) {
            setTrailerId(trailerId);
        }
    }

    /**
    * Returns the primary key of this trailer user.
    *
    * @return the primary key of this trailer user
    */
    @Override
    public long getPrimaryKey() {
        return _trailerUser.getPrimaryKey();
    }

    /**
    * Sets the primary key of this trailer user.
    *
    * @param primaryKey the primary key of this trailer user
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _trailerUser.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the trailer user ID of this trailer user.
    *
    * @return the trailer user ID of this trailer user
    */
    @Override
    public long getTrailerUserId() {
        return _trailerUser.getTrailerUserId();
    }

    /**
    * Sets the trailer user ID of this trailer user.
    *
    * @param trailerUserId the trailer user ID of this trailer user
    */
    @Override
    public void setTrailerUserId(long trailerUserId) {
        _trailerUser.setTrailerUserId(trailerUserId);
    }

    /**
    * Returns the trailer user uuid of this trailer user.
    *
    * @return the trailer user uuid of this trailer user
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.lang.String getTrailerUserUuid()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerUser.getTrailerUserUuid();
    }

    /**
    * Sets the trailer user uuid of this trailer user.
    *
    * @param trailerUserUuid the trailer user uuid of this trailer user
    */
    @Override
    public void setTrailerUserUuid(java.lang.String trailerUserUuid) {
        _trailerUser.setTrailerUserUuid(trailerUserUuid);
    }

    /**
    * Returns the user ID of this trailer user.
    *
    * @return the user ID of this trailer user
    */
    @Override
    public long getUserId() {
        return _trailerUser.getUserId();
    }

    /**
    * Sets the user ID of this trailer user.
    *
    * @param userId the user ID of this trailer user
    */
    @Override
    public void setUserId(long userId) {
        _trailerUser.setUserId(userId);
    }

    /**
    * Returns the user uuid of this trailer user.
    *
    * @return the user uuid of this trailer user
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.lang.String getUserUuid()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerUser.getUserUuid();
    }

    /**
    * Sets the user uuid of this trailer user.
    *
    * @param userUuid the user uuid of this trailer user
    */
    @Override
    public void setUserUuid(java.lang.String userUuid) {
        _trailerUser.setUserUuid(userUuid);
    }

    /**
    * Returns the trailer ID of this trailer user.
    *
    * @return the trailer ID of this trailer user
    */
    @Override
    public long getTrailerId() {
        return _trailerUser.getTrailerId();
    }

    /**
    * Sets the trailer ID of this trailer user.
    *
    * @param trailerId the trailer ID of this trailer user
    */
    @Override
    public void setTrailerId(long trailerId) {
        _trailerUser.setTrailerId(trailerId);
    }

    @Override
    public boolean isNew() {
        return _trailerUser.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _trailerUser.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _trailerUser.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _trailerUser.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _trailerUser.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _trailerUser.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _trailerUser.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _trailerUser.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _trailerUser.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _trailerUser.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _trailerUser.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new TrailerUserWrapper((TrailerUser) _trailerUser.clone());
    }

    @Override
    public int compareTo(TrailerUser trailerUser) {
        return _trailerUser.compareTo(trailerUser);
    }

    @Override
    public int hashCode() {
        return _trailerUser.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<TrailerUser> toCacheModel() {
        return _trailerUser.toCacheModel();
    }

    @Override
    public TrailerUser toEscapedModel() {
        return new TrailerUserWrapper(_trailerUser.toEscapedModel());
    }

    @Override
    public TrailerUser toUnescapedModel() {
        return new TrailerUserWrapper(_trailerUser.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _trailerUser.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _trailerUser.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _trailerUser.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TrailerUserWrapper)) {
            return false;
        }

        TrailerUserWrapper trailerUserWrapper = (TrailerUserWrapper) obj;

        if (Validator.equals(_trailerUser, trailerUserWrapper._trailerUser)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public TrailerUser getWrappedTrailerUser() {
        return _trailerUser;
    }

    @Override
    public TrailerUser getWrappedModel() {
        return _trailerUser;
    }

    @Override
    public void resetOriginalValues() {
        _trailerUser.resetOriginalValues();
    }
}
