package de.humance.eco.profile.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Trailer}.
 * </p>
 *
 * @author Humance
 * @see Trailer
 * @generated
 */
public class TrailerWrapper implements Trailer, ModelWrapper<Trailer> {
    private Trailer _trailer;

    public TrailerWrapper(Trailer trailer) {
        _trailer = trailer;
    }

    @Override
    public Class<?> getModelClass() {
        return Trailer.class;
    }

    @Override
    public String getModelClassName() {
        return Trailer.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("trailerId", getTrailerId());
        attributes.put("creatorId", getCreatorId());
        attributes.put("creatorName", getCreatorName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifierId", getModifierId());
        attributes.put("modifierName", getModifierName());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("driverId", getDriverId());
        attributes.put("typeId", getTypeId());
        attributes.put("manufacturerId", getManufacturerId());
        attributes.put("organizationId", getOrganizationId());
        attributes.put("modelName", getModelName());
        attributes.put("dimensionHeight", getDimensionHeight());
        attributes.put("dimensionWidth", getDimensionWidth());
        attributes.put("dimensionDepth", getDimensionDepth());
        attributes.put("licensePlate", getLicensePlate());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long trailerId = (Long) attributes.get("trailerId");

        if (trailerId != null) {
            setTrailerId(trailerId);
        }

        Long creatorId = (Long) attributes.get("creatorId");

        if (creatorId != null) {
            setCreatorId(creatorId);
        }

        String creatorName = (String) attributes.get("creatorName");

        if (creatorName != null) {
            setCreatorName(creatorName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Long modifierId = (Long) attributes.get("modifierId");

        if (modifierId != null) {
            setModifierId(modifierId);
        }

        String modifierName = (String) attributes.get("modifierName");

        if (modifierName != null) {
            setModifierName(modifierName);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        Long driverId = (Long) attributes.get("driverId");

        if (driverId != null) {
            setDriverId(driverId);
        }

        Long typeId = (Long) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        Long manufacturerId = (Long) attributes.get("manufacturerId");

        if (manufacturerId != null) {
            setManufacturerId(manufacturerId);
        }

        Long organizationId = (Long) attributes.get("organizationId");

        if (organizationId != null) {
            setOrganizationId(organizationId);
        }

        String modelName = (String) attributes.get("modelName");

        if (modelName != null) {
            setModelName(modelName);
        }

        Long dimensionHeight = (Long) attributes.get("dimensionHeight");

        if (dimensionHeight != null) {
            setDimensionHeight(dimensionHeight);
        }

        Long dimensionWidth = (Long) attributes.get("dimensionWidth");

        if (dimensionWidth != null) {
            setDimensionWidth(dimensionWidth);
        }

        Long dimensionDepth = (Long) attributes.get("dimensionDepth");

        if (dimensionDepth != null) {
            setDimensionDepth(dimensionDepth);
        }

        String licensePlate = (String) attributes.get("licensePlate");

        if (licensePlate != null) {
            setLicensePlate(licensePlate);
        }
    }

    /**
    * Returns the primary key of this trailer.
    *
    * @return the primary key of this trailer
    */
    @Override
    public long getPrimaryKey() {
        return _trailer.getPrimaryKey();
    }

    /**
    * Sets the primary key of this trailer.
    *
    * @param primaryKey the primary key of this trailer
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _trailer.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the trailer ID of this trailer.
    *
    * @return the trailer ID of this trailer
    */
    @Override
    public long getTrailerId() {
        return _trailer.getTrailerId();
    }

    /**
    * Sets the trailer ID of this trailer.
    *
    * @param trailerId the trailer ID of this trailer
    */
    @Override
    public void setTrailerId(long trailerId) {
        _trailer.setTrailerId(trailerId);
    }

    /**
    * Returns the creator ID of this trailer.
    *
    * @return the creator ID of this trailer
    */
    @Override
    public long getCreatorId() {
        return _trailer.getCreatorId();
    }

    /**
    * Sets the creator ID of this trailer.
    *
    * @param creatorId the creator ID of this trailer
    */
    @Override
    public void setCreatorId(long creatorId) {
        _trailer.setCreatorId(creatorId);
    }

    /**
    * Returns the creator name of this trailer.
    *
    * @return the creator name of this trailer
    */
    @Override
    public java.lang.String getCreatorName() {
        return _trailer.getCreatorName();
    }

    /**
    * Sets the creator name of this trailer.
    *
    * @param creatorName the creator name of this trailer
    */
    @Override
    public void setCreatorName(java.lang.String creatorName) {
        _trailer.setCreatorName(creatorName);
    }

    /**
    * Returns the create date of this trailer.
    *
    * @return the create date of this trailer
    */
    @Override
    public java.util.Date getCreateDate() {
        return _trailer.getCreateDate();
    }

    /**
    * Sets the create date of this trailer.
    *
    * @param createDate the create date of this trailer
    */
    @Override
    public void setCreateDate(java.util.Date createDate) {
        _trailer.setCreateDate(createDate);
    }

    /**
    * Returns the modifier ID of this trailer.
    *
    * @return the modifier ID of this trailer
    */
    @Override
    public long getModifierId() {
        return _trailer.getModifierId();
    }

    /**
    * Sets the modifier ID of this trailer.
    *
    * @param modifierId the modifier ID of this trailer
    */
    @Override
    public void setModifierId(long modifierId) {
        _trailer.setModifierId(modifierId);
    }

    /**
    * Returns the modifier name of this trailer.
    *
    * @return the modifier name of this trailer
    */
    @Override
    public java.lang.String getModifierName() {
        return _trailer.getModifierName();
    }

    /**
    * Sets the modifier name of this trailer.
    *
    * @param modifierName the modifier name of this trailer
    */
    @Override
    public void setModifierName(java.lang.String modifierName) {
        _trailer.setModifierName(modifierName);
    }

    /**
    * Returns the modified date of this trailer.
    *
    * @return the modified date of this trailer
    */
    @Override
    public java.util.Date getModifiedDate() {
        return _trailer.getModifiedDate();
    }

    /**
    * Sets the modified date of this trailer.
    *
    * @param modifiedDate the modified date of this trailer
    */
    @Override
    public void setModifiedDate(java.util.Date modifiedDate) {
        _trailer.setModifiedDate(modifiedDate);
    }

    /**
    * Returns the driver ID of this trailer.
    *
    * @return the driver ID of this trailer
    */
    @Override
    public long getDriverId() {
        return _trailer.getDriverId();
    }

    /**
    * Sets the driver ID of this trailer.
    *
    * @param driverId the driver ID of this trailer
    */
    @Override
    public void setDriverId(long driverId) {
        _trailer.setDriverId(driverId);
    }

    /**
    * Returns the type ID of this trailer.
    *
    * @return the type ID of this trailer
    */
    @Override
    public long getTypeId() {
        return _trailer.getTypeId();
    }

    /**
    * Sets the type ID of this trailer.
    *
    * @param typeId the type ID of this trailer
    */
    @Override
    public void setTypeId(long typeId) {
        _trailer.setTypeId(typeId);
    }

    /**
    * Returns the manufacturer ID of this trailer.
    *
    * @return the manufacturer ID of this trailer
    */
    @Override
    public long getManufacturerId() {
        return _trailer.getManufacturerId();
    }

    /**
    * Sets the manufacturer ID of this trailer.
    *
    * @param manufacturerId the manufacturer ID of this trailer
    */
    @Override
    public void setManufacturerId(long manufacturerId) {
        _trailer.setManufacturerId(manufacturerId);
    }

    /**
    * Returns the organization ID of this trailer.
    *
    * @return the organization ID of this trailer
    */
    @Override
    public long getOrganizationId() {
        return _trailer.getOrganizationId();
    }

    /**
    * Sets the organization ID of this trailer.
    *
    * @param organizationId the organization ID of this trailer
    */
    @Override
    public void setOrganizationId(long organizationId) {
        _trailer.setOrganizationId(organizationId);
    }

    /**
    * Returns the model name of this trailer.
    *
    * @return the model name of this trailer
    */
    @Override
    public java.lang.String getModelName() {
        return _trailer.getModelName();
    }

    /**
    * Sets the model name of this trailer.
    *
    * @param modelName the model name of this trailer
    */
    @Override
    public void setModelName(java.lang.String modelName) {
        _trailer.setModelName(modelName);
    }

    /**
    * Returns the dimension height of this trailer.
    *
    * @return the dimension height of this trailer
    */
    @Override
    public long getDimensionHeight() {
        return _trailer.getDimensionHeight();
    }

    /**
    * Sets the dimension height of this trailer.
    *
    * @param dimensionHeight the dimension height of this trailer
    */
    @Override
    public void setDimensionHeight(long dimensionHeight) {
        _trailer.setDimensionHeight(dimensionHeight);
    }

    /**
    * Returns the dimension width of this trailer.
    *
    * @return the dimension width of this trailer
    */
    @Override
    public long getDimensionWidth() {
        return _trailer.getDimensionWidth();
    }

    /**
    * Sets the dimension width of this trailer.
    *
    * @param dimensionWidth the dimension width of this trailer
    */
    @Override
    public void setDimensionWidth(long dimensionWidth) {
        _trailer.setDimensionWidth(dimensionWidth);
    }

    /**
    * Returns the dimension depth of this trailer.
    *
    * @return the dimension depth of this trailer
    */
    @Override
    public long getDimensionDepth() {
        return _trailer.getDimensionDepth();
    }

    /**
    * Sets the dimension depth of this trailer.
    *
    * @param dimensionDepth the dimension depth of this trailer
    */
    @Override
    public void setDimensionDepth(long dimensionDepth) {
        _trailer.setDimensionDepth(dimensionDepth);
    }

    /**
    * Returns the license plate of this trailer.
    *
    * @return the license plate of this trailer
    */
    @Override
    public java.lang.String getLicensePlate() {
        return _trailer.getLicensePlate();
    }

    /**
    * Sets the license plate of this trailer.
    *
    * @param licensePlate the license plate of this trailer
    */
    @Override
    public void setLicensePlate(java.lang.String licensePlate) {
        _trailer.setLicensePlate(licensePlate);
    }

    @Override
    public boolean isNew() {
        return _trailer.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _trailer.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _trailer.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _trailer.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _trailer.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _trailer.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _trailer.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _trailer.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _trailer.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _trailer.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _trailer.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new TrailerWrapper((Trailer) _trailer.clone());
    }

    @Override
    public int compareTo(Trailer trailer) {
        return _trailer.compareTo(trailer);
    }

    @Override
    public int hashCode() {
        return _trailer.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<Trailer> toCacheModel() {
        return _trailer.toCacheModel();
    }

    @Override
    public Trailer toEscapedModel() {
        return new TrailerWrapper(_trailer.toEscapedModel());
    }

    @Override
    public Trailer toUnescapedModel() {
        return new TrailerWrapper(_trailer.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _trailer.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _trailer.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _trailer.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TrailerWrapper)) {
            return false;
        }

        TrailerWrapper trailerWrapper = (TrailerWrapper) obj;

        if (Validator.equals(_trailer, trailerWrapper._trailer)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Trailer getWrappedTrailer() {
        return _trailer;
    }

    @Override
    public Trailer getWrappedModel() {
        return _trailer;
    }

    @Override
    public void resetOriginalValues() {
        _trailer.resetOriginalValues();
    }
}
