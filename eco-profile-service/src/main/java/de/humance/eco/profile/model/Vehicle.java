package de.humance.eco.profile.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Vehicle service. Represents a row in the &quot;Profile_Vehicle&quot; database table, with each column mapped to a property of this class.
 *
 * @author Humance
 * @see VehicleModel
 * @see de.humance.eco.profile.model.impl.VehicleImpl
 * @see de.humance.eco.profile.model.impl.VehicleModelImpl
 * @generated
 */
public interface Vehicle extends VehicleModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link de.humance.eco.profile.model.impl.VehicleImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
