package de.humance.eco.profile.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the VehicleAttribute service. Represents a row in the &quot;Profile_VehicleAttribute&quot; database table, with each column mapped to a property of this class.
 *
 * @author Humance
 * @see VehicleAttributeModel
 * @see de.humance.eco.profile.model.impl.VehicleAttributeImpl
 * @see de.humance.eco.profile.model.impl.VehicleAttributeModelImpl
 * @generated
 */
public interface VehicleAttribute extends VehicleAttributeModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link de.humance.eco.profile.model.impl.VehicleAttributeImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
