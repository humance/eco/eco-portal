package de.humance.eco.profile.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import de.humance.eco.profile.service.ClpSerializer;
import de.humance.eco.profile.service.VehicleAttributeLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class VehicleAttributeClp extends BaseModelImpl<VehicleAttribute>
    implements VehicleAttribute {
    private long _vehicleAttributeId;
    private long _vehicleId;
    private long _vehicleAttributeDefinitionId;
    private String _type;
    private long _sequenceNumber;
    private String _attributeValue;
    private BaseModel<?> _vehicleAttributeRemoteModel;

    public VehicleAttributeClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return VehicleAttribute.class;
    }

    @Override
    public String getModelClassName() {
        return VehicleAttribute.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _vehicleAttributeId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setVehicleAttributeId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _vehicleAttributeId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("vehicleAttributeId", getVehicleAttributeId());
        attributes.put("vehicleId", getVehicleId());
        attributes.put("vehicleAttributeDefinitionId",
            getVehicleAttributeDefinitionId());
        attributes.put("type", getType());
        attributes.put("sequenceNumber", getSequenceNumber());
        attributes.put("attributeValue", getAttributeValue());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long vehicleAttributeId = (Long) attributes.get("vehicleAttributeId");

        if (vehicleAttributeId != null) {
            setVehicleAttributeId(vehicleAttributeId);
        }

        Long vehicleId = (Long) attributes.get("vehicleId");

        if (vehicleId != null) {
            setVehicleId(vehicleId);
        }

        Long vehicleAttributeDefinitionId = (Long) attributes.get(
                "vehicleAttributeDefinitionId");

        if (vehicleAttributeDefinitionId != null) {
            setVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
        }

        String type = (String) attributes.get("type");

        if (type != null) {
            setType(type);
        }

        Long sequenceNumber = (Long) attributes.get("sequenceNumber");

        if (sequenceNumber != null) {
            setSequenceNumber(sequenceNumber);
        }

        String attributeValue = (String) attributes.get("attributeValue");

        if (attributeValue != null) {
            setAttributeValue(attributeValue);
        }
    }

    @Override
    public long getVehicleAttributeId() {
        return _vehicleAttributeId;
    }

    @Override
    public void setVehicleAttributeId(long vehicleAttributeId) {
        _vehicleAttributeId = vehicleAttributeId;

        if (_vehicleAttributeRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeRemoteModel.getClass();

                Method method = clazz.getMethod("setVehicleAttributeId",
                        long.class);

                method.invoke(_vehicleAttributeRemoteModel, vehicleAttributeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getVehicleId() {
        return _vehicleId;
    }

    @Override
    public void setVehicleId(long vehicleId) {
        _vehicleId = vehicleId;

        if (_vehicleAttributeRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeRemoteModel.getClass();

                Method method = clazz.getMethod("setVehicleId", long.class);

                method.invoke(_vehicleAttributeRemoteModel, vehicleId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getVehicleAttributeDefinitionId() {
        return _vehicleAttributeDefinitionId;
    }

    @Override
    public void setVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId) {
        _vehicleAttributeDefinitionId = vehicleAttributeDefinitionId;

        if (_vehicleAttributeRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeRemoteModel.getClass();

                Method method = clazz.getMethod("setVehicleAttributeDefinitionId",
                        long.class);

                method.invoke(_vehicleAttributeRemoteModel,
                    vehicleAttributeDefinitionId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getType() {
        return _type;
    }

    @Override
    public void setType(String type) {
        _type = type;

        if (_vehicleAttributeRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeRemoteModel.getClass();

                Method method = clazz.getMethod("setType", String.class);

                method.invoke(_vehicleAttributeRemoteModel, type);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getSequenceNumber() {
        return _sequenceNumber;
    }

    @Override
    public void setSequenceNumber(long sequenceNumber) {
        _sequenceNumber = sequenceNumber;

        if (_vehicleAttributeRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeRemoteModel.getClass();

                Method method = clazz.getMethod("setSequenceNumber", long.class);

                method.invoke(_vehicleAttributeRemoteModel, sequenceNumber);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getAttributeValue() {
        return _attributeValue;
    }

    @Override
    public void setAttributeValue(String attributeValue) {
        _attributeValue = attributeValue;

        if (_vehicleAttributeRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeRemoteModel.getClass();

                Method method = clazz.getMethod("setAttributeValue",
                        String.class);

                method.invoke(_vehicleAttributeRemoteModel, attributeValue);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getVehicleAttributeRemoteModel() {
        return _vehicleAttributeRemoteModel;
    }

    public void setVehicleAttributeRemoteModel(
        BaseModel<?> vehicleAttributeRemoteModel) {
        _vehicleAttributeRemoteModel = vehicleAttributeRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _vehicleAttributeRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_vehicleAttributeRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            VehicleAttributeLocalServiceUtil.addVehicleAttribute(this);
        } else {
            VehicleAttributeLocalServiceUtil.updateVehicleAttribute(this);
        }
    }

    @Override
    public VehicleAttribute toEscapedModel() {
        return (VehicleAttribute) ProxyUtil.newProxyInstance(VehicleAttribute.class.getClassLoader(),
            new Class[] { VehicleAttribute.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        VehicleAttributeClp clone = new VehicleAttributeClp();

        clone.setVehicleAttributeId(getVehicleAttributeId());
        clone.setVehicleId(getVehicleId());
        clone.setVehicleAttributeDefinitionId(getVehicleAttributeDefinitionId());
        clone.setType(getType());
        clone.setSequenceNumber(getSequenceNumber());
        clone.setAttributeValue(getAttributeValue());

        return clone;
    }

    @Override
    public int compareTo(VehicleAttribute vehicleAttribute) {
        int value = 0;

        if (getVehicleId() < vehicleAttribute.getVehicleId()) {
            value = -1;
        } else if (getVehicleId() > vehicleAttribute.getVehicleId()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        if (getSequenceNumber() < vehicleAttribute.getSequenceNumber()) {
            value = -1;
        } else if (getSequenceNumber() > vehicleAttribute.getSequenceNumber()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VehicleAttributeClp)) {
            return false;
        }

        VehicleAttributeClp vehicleAttribute = (VehicleAttributeClp) obj;

        long primaryKey = vehicleAttribute.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{vehicleAttributeId=");
        sb.append(getVehicleAttributeId());
        sb.append(", vehicleId=");
        sb.append(getVehicleId());
        sb.append(", vehicleAttributeDefinitionId=");
        sb.append(getVehicleAttributeDefinitionId());
        sb.append(", type=");
        sb.append(getType());
        sb.append(", sequenceNumber=");
        sb.append(getSequenceNumber());
        sb.append(", attributeValue=");
        sb.append(getAttributeValue());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(22);

        sb.append("<model><model-name>");
        sb.append("de.humance.eco.profile.model.VehicleAttribute");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>vehicleAttributeId</column-name><column-value><![CDATA[");
        sb.append(getVehicleAttributeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>vehicleId</column-name><column-value><![CDATA[");
        sb.append(getVehicleId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>vehicleAttributeDefinitionId</column-name><column-value><![CDATA[");
        sb.append(getVehicleAttributeDefinitionId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>type</column-name><column-value><![CDATA[");
        sb.append(getType());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>sequenceNumber</column-name><column-value><![CDATA[");
        sb.append(getSequenceNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>attributeValue</column-name><column-value><![CDATA[");
        sb.append(getAttributeValue());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
