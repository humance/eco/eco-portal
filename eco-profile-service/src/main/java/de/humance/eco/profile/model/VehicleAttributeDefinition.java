package de.humance.eco.profile.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the VehicleAttributeDefinition service. Represents a row in the &quot;Profile_VehicleAttributeDefinition&quot; database table, with each column mapped to a property of this class.
 *
 * @author Humance
 * @see VehicleAttributeDefinitionModel
 * @see de.humance.eco.profile.model.impl.VehicleAttributeDefinitionImpl
 * @see de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl
 * @generated
 */
public interface VehicleAttributeDefinition
    extends VehicleAttributeDefinitionModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
