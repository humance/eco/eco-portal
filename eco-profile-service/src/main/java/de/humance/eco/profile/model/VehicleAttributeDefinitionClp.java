package de.humance.eco.profile.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import de.humance.eco.profile.service.ClpSerializer;
import de.humance.eco.profile.service.VehicleAttributeDefinitionLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class VehicleAttributeDefinitionClp extends BaseModelImpl<VehicleAttributeDefinition>
    implements VehicleAttributeDefinition {
    private long _vehicleAttributeDefinitionId;
    private long _creatorId;
    private String _creatorName;
    private Date _createDate;
    private long _modifierId;
    private String _modifierName;
    private Date _modifiedDate;
    private long _vehicleTypeId;
    private String _type;
    private String _title;
    private double _minRangeValue;
    private double _maxRangeValue;
    private String _unit;
    private long _sequenceNumber;
    private BaseModel<?> _vehicleAttributeDefinitionRemoteModel;

    public VehicleAttributeDefinitionClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return VehicleAttributeDefinition.class;
    }

    @Override
    public String getModelClassName() {
        return VehicleAttributeDefinition.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _vehicleAttributeDefinitionId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setVehicleAttributeDefinitionId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _vehicleAttributeDefinitionId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("vehicleAttributeDefinitionId",
            getVehicleAttributeDefinitionId());
        attributes.put("creatorId", getCreatorId());
        attributes.put("creatorName", getCreatorName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifierId", getModifierId());
        attributes.put("modifierName", getModifierName());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("vehicleTypeId", getVehicleTypeId());
        attributes.put("type", getType());
        attributes.put("title", getTitle());
        attributes.put("minRangeValue", getMinRangeValue());
        attributes.put("maxRangeValue", getMaxRangeValue());
        attributes.put("unit", getUnit());
        attributes.put("sequenceNumber", getSequenceNumber());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long vehicleAttributeDefinitionId = (Long) attributes.get(
                "vehicleAttributeDefinitionId");

        if (vehicleAttributeDefinitionId != null) {
            setVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
        }

        Long creatorId = (Long) attributes.get("creatorId");

        if (creatorId != null) {
            setCreatorId(creatorId);
        }

        String creatorName = (String) attributes.get("creatorName");

        if (creatorName != null) {
            setCreatorName(creatorName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Long modifierId = (Long) attributes.get("modifierId");

        if (modifierId != null) {
            setModifierId(modifierId);
        }

        String modifierName = (String) attributes.get("modifierName");

        if (modifierName != null) {
            setModifierName(modifierName);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        Long vehicleTypeId = (Long) attributes.get("vehicleTypeId");

        if (vehicleTypeId != null) {
            setVehicleTypeId(vehicleTypeId);
        }

        String type = (String) attributes.get("type");

        if (type != null) {
            setType(type);
        }

        String title = (String) attributes.get("title");

        if (title != null) {
            setTitle(title);
        }

        Double minRangeValue = (Double) attributes.get("minRangeValue");

        if (minRangeValue != null) {
            setMinRangeValue(minRangeValue);
        }

        Double maxRangeValue = (Double) attributes.get("maxRangeValue");

        if (maxRangeValue != null) {
            setMaxRangeValue(maxRangeValue);
        }

        String unit = (String) attributes.get("unit");

        if (unit != null) {
            setUnit(unit);
        }

        Long sequenceNumber = (Long) attributes.get("sequenceNumber");

        if (sequenceNumber != null) {
            setSequenceNumber(sequenceNumber);
        }
    }

    @Override
    public long getVehicleAttributeDefinitionId() {
        return _vehicleAttributeDefinitionId;
    }

    @Override
    public void setVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId) {
        _vehicleAttributeDefinitionId = vehicleAttributeDefinitionId;

        if (_vehicleAttributeDefinitionRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionRemoteModel.getClass();

                Method method = clazz.getMethod("setVehicleAttributeDefinitionId",
                        long.class);

                method.invoke(_vehicleAttributeDefinitionRemoteModel,
                    vehicleAttributeDefinitionId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getCreatorId() {
        return _creatorId;
    }

    @Override
    public void setCreatorId(long creatorId) {
        _creatorId = creatorId;

        if (_vehicleAttributeDefinitionRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionRemoteModel.getClass();

                Method method = clazz.getMethod("setCreatorId", long.class);

                method.invoke(_vehicleAttributeDefinitionRemoteModel, creatorId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCreatorName() {
        return _creatorName;
    }

    @Override
    public void setCreatorName(String creatorName) {
        _creatorName = creatorName;

        if (_vehicleAttributeDefinitionRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionRemoteModel.getClass();

                Method method = clazz.getMethod("setCreatorName", String.class);

                method.invoke(_vehicleAttributeDefinitionRemoteModel,
                    creatorName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getCreateDate() {
        return _createDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        _createDate = createDate;

        if (_vehicleAttributeDefinitionRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionRemoteModel.getClass();

                Method method = clazz.getMethod("setCreateDate", Date.class);

                method.invoke(_vehicleAttributeDefinitionRemoteModel, createDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getModifierId() {
        return _modifierId;
    }

    @Override
    public void setModifierId(long modifierId) {
        _modifierId = modifierId;

        if (_vehicleAttributeDefinitionRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionRemoteModel.getClass();

                Method method = clazz.getMethod("setModifierId", long.class);

                method.invoke(_vehicleAttributeDefinitionRemoteModel, modifierId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getModifierName() {
        return _modifierName;
    }

    @Override
    public void setModifierName(String modifierName) {
        _modifierName = modifierName;

        if (_vehicleAttributeDefinitionRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionRemoteModel.getClass();

                Method method = clazz.getMethod("setModifierName", String.class);

                method.invoke(_vehicleAttributeDefinitionRemoteModel,
                    modifierName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getModifiedDate() {
        return _modifiedDate;
    }

    @Override
    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;

        if (_vehicleAttributeDefinitionRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionRemoteModel.getClass();

                Method method = clazz.getMethod("setModifiedDate", Date.class);

                method.invoke(_vehicleAttributeDefinitionRemoteModel,
                    modifiedDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getVehicleTypeId() {
        return _vehicleTypeId;
    }

    @Override
    public void setVehicleTypeId(long vehicleTypeId) {
        _vehicleTypeId = vehicleTypeId;

        if (_vehicleAttributeDefinitionRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionRemoteModel.getClass();

                Method method = clazz.getMethod("setVehicleTypeId", long.class);

                method.invoke(_vehicleAttributeDefinitionRemoteModel,
                    vehicleTypeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getType() {
        return _type;
    }

    @Override
    public void setType(String type) {
        _type = type;

        if (_vehicleAttributeDefinitionRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionRemoteModel.getClass();

                Method method = clazz.getMethod("setType", String.class);

                method.invoke(_vehicleAttributeDefinitionRemoteModel, type);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTitle() {
        return _title;
    }

    @Override
    public void setTitle(String title) {
        _title = title;

        if (_vehicleAttributeDefinitionRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionRemoteModel.getClass();

                Method method = clazz.getMethod("setTitle", String.class);

                method.invoke(_vehicleAttributeDefinitionRemoteModel, title);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getMinRangeValue() {
        return _minRangeValue;
    }

    @Override
    public void setMinRangeValue(double minRangeValue) {
        _minRangeValue = minRangeValue;

        if (_vehicleAttributeDefinitionRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionRemoteModel.getClass();

                Method method = clazz.getMethod("setMinRangeValue", double.class);

                method.invoke(_vehicleAttributeDefinitionRemoteModel,
                    minRangeValue);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getMaxRangeValue() {
        return _maxRangeValue;
    }

    @Override
    public void setMaxRangeValue(double maxRangeValue) {
        _maxRangeValue = maxRangeValue;

        if (_vehicleAttributeDefinitionRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionRemoteModel.getClass();

                Method method = clazz.getMethod("setMaxRangeValue", double.class);

                method.invoke(_vehicleAttributeDefinitionRemoteModel,
                    maxRangeValue);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getUnit() {
        return _unit;
    }

    @Override
    public void setUnit(String unit) {
        _unit = unit;

        if (_vehicleAttributeDefinitionRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionRemoteModel.getClass();

                Method method = clazz.getMethod("setUnit", String.class);

                method.invoke(_vehicleAttributeDefinitionRemoteModel, unit);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getSequenceNumber() {
        return _sequenceNumber;
    }

    @Override
    public void setSequenceNumber(long sequenceNumber) {
        _sequenceNumber = sequenceNumber;

        if (_vehicleAttributeDefinitionRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionRemoteModel.getClass();

                Method method = clazz.getMethod("setSequenceNumber", long.class);

                method.invoke(_vehicleAttributeDefinitionRemoteModel,
                    sequenceNumber);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getVehicleAttributeDefinitionRemoteModel() {
        return _vehicleAttributeDefinitionRemoteModel;
    }

    public void setVehicleAttributeDefinitionRemoteModel(
        BaseModel<?> vehicleAttributeDefinitionRemoteModel) {
        _vehicleAttributeDefinitionRemoteModel = vehicleAttributeDefinitionRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _vehicleAttributeDefinitionRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_vehicleAttributeDefinitionRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            VehicleAttributeDefinitionLocalServiceUtil.addVehicleAttributeDefinition(this);
        } else {
            VehicleAttributeDefinitionLocalServiceUtil.updateVehicleAttributeDefinition(this);
        }
    }

    @Override
    public VehicleAttributeDefinition toEscapedModel() {
        return (VehicleAttributeDefinition) ProxyUtil.newProxyInstance(VehicleAttributeDefinition.class.getClassLoader(),
            new Class[] { VehicleAttributeDefinition.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        VehicleAttributeDefinitionClp clone = new VehicleAttributeDefinitionClp();

        clone.setVehicleAttributeDefinitionId(getVehicleAttributeDefinitionId());
        clone.setCreatorId(getCreatorId());
        clone.setCreatorName(getCreatorName());
        clone.setCreateDate(getCreateDate());
        clone.setModifierId(getModifierId());
        clone.setModifierName(getModifierName());
        clone.setModifiedDate(getModifiedDate());
        clone.setVehicleTypeId(getVehicleTypeId());
        clone.setType(getType());
        clone.setTitle(getTitle());
        clone.setMinRangeValue(getMinRangeValue());
        clone.setMaxRangeValue(getMaxRangeValue());
        clone.setUnit(getUnit());
        clone.setSequenceNumber(getSequenceNumber());

        return clone;
    }

    @Override
    public int compareTo(VehicleAttributeDefinition vehicleAttributeDefinition) {
        int value = 0;

        if (getVehicleTypeId() < vehicleAttributeDefinition.getVehicleTypeId()) {
            value = -1;
        } else if (getVehicleTypeId() > vehicleAttributeDefinition.getVehicleTypeId()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        if (getSequenceNumber() < vehicleAttributeDefinition.getSequenceNumber()) {
            value = -1;
        } else if (getSequenceNumber() > vehicleAttributeDefinition.getSequenceNumber()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VehicleAttributeDefinitionClp)) {
            return false;
        }

        VehicleAttributeDefinitionClp vehicleAttributeDefinition = (VehicleAttributeDefinitionClp) obj;

        long primaryKey = vehicleAttributeDefinition.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(29);

        sb.append("{vehicleAttributeDefinitionId=");
        sb.append(getVehicleAttributeDefinitionId());
        sb.append(", creatorId=");
        sb.append(getCreatorId());
        sb.append(", creatorName=");
        sb.append(getCreatorName());
        sb.append(", createDate=");
        sb.append(getCreateDate());
        sb.append(", modifierId=");
        sb.append(getModifierId());
        sb.append(", modifierName=");
        sb.append(getModifierName());
        sb.append(", modifiedDate=");
        sb.append(getModifiedDate());
        sb.append(", vehicleTypeId=");
        sb.append(getVehicleTypeId());
        sb.append(", type=");
        sb.append(getType());
        sb.append(", title=");
        sb.append(getTitle());
        sb.append(", minRangeValue=");
        sb.append(getMinRangeValue());
        sb.append(", maxRangeValue=");
        sb.append(getMaxRangeValue());
        sb.append(", unit=");
        sb.append(getUnit());
        sb.append(", sequenceNumber=");
        sb.append(getSequenceNumber());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(46);

        sb.append("<model><model-name>");
        sb.append("de.humance.eco.profile.model.VehicleAttributeDefinition");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>vehicleAttributeDefinitionId</column-name><column-value><![CDATA[");
        sb.append(getVehicleAttributeDefinitionId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>creatorId</column-name><column-value><![CDATA[");
        sb.append(getCreatorId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>creatorName</column-name><column-value><![CDATA[");
        sb.append(getCreatorName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>createDate</column-name><column-value><![CDATA[");
        sb.append(getCreateDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifierId</column-name><column-value><![CDATA[");
        sb.append(getModifierId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifierName</column-name><column-value><![CDATA[");
        sb.append(getModifierName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
        sb.append(getModifiedDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>vehicleTypeId</column-name><column-value><![CDATA[");
        sb.append(getVehicleTypeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>type</column-name><column-value><![CDATA[");
        sb.append(getType());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>title</column-name><column-value><![CDATA[");
        sb.append(getTitle());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>minRangeValue</column-name><column-value><![CDATA[");
        sb.append(getMinRangeValue());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>maxRangeValue</column-name><column-value><![CDATA[");
        sb.append(getMaxRangeValue());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>unit</column-name><column-value><![CDATA[");
        sb.append(getUnit());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>sequenceNumber</column-name><column-value><![CDATA[");
        sb.append(getSequenceNumber());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
