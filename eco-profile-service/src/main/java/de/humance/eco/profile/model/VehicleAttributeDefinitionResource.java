package de.humance.eco.profile.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the VehicleAttributeDefinitionResource service. Represents a row in the &quot;Profile_VehicleAttributeDefinitionResource&quot; database table, with each column mapped to a property of this class.
 *
 * @author Humance
 * @see VehicleAttributeDefinitionResourceModel
 * @see de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceImpl
 * @see de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl
 * @generated
 */
public interface VehicleAttributeDefinitionResource
    extends VehicleAttributeDefinitionResourceModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
