package de.humance.eco.profile.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import de.humance.eco.profile.service.ClpSerializer;
import de.humance.eco.profile.service.VehicleAttributeDefinitionResourceLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class VehicleAttributeDefinitionResourceClp extends BaseModelImpl<VehicleAttributeDefinitionResource>
    implements VehicleAttributeDefinitionResource {
    private long _vehicleAttributeDefinitionResourceId;
    private long _vehicleAttributeDefinitionId;
    private String _country;
    private String _language;
    private String _title;
    private String _unit;
    private BaseModel<?> _vehicleAttributeDefinitionResourceRemoteModel;

    public VehicleAttributeDefinitionResourceClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return VehicleAttributeDefinitionResource.class;
    }

    @Override
    public String getModelClassName() {
        return VehicleAttributeDefinitionResource.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _vehicleAttributeDefinitionResourceId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setVehicleAttributeDefinitionResourceId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _vehicleAttributeDefinitionResourceId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("vehicleAttributeDefinitionResourceId",
            getVehicleAttributeDefinitionResourceId());
        attributes.put("vehicleAttributeDefinitionId",
            getVehicleAttributeDefinitionId());
        attributes.put("country", getCountry());
        attributes.put("language", getLanguage());
        attributes.put("title", getTitle());
        attributes.put("unit", getUnit());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long vehicleAttributeDefinitionResourceId = (Long) attributes.get(
                "vehicleAttributeDefinitionResourceId");

        if (vehicleAttributeDefinitionResourceId != null) {
            setVehicleAttributeDefinitionResourceId(vehicleAttributeDefinitionResourceId);
        }

        Long vehicleAttributeDefinitionId = (Long) attributes.get(
                "vehicleAttributeDefinitionId");

        if (vehicleAttributeDefinitionId != null) {
            setVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
        }

        String country = (String) attributes.get("country");

        if (country != null) {
            setCountry(country);
        }

        String language = (String) attributes.get("language");

        if (language != null) {
            setLanguage(language);
        }

        String title = (String) attributes.get("title");

        if (title != null) {
            setTitle(title);
        }

        String unit = (String) attributes.get("unit");

        if (unit != null) {
            setUnit(unit);
        }
    }

    @Override
    public long getVehicleAttributeDefinitionResourceId() {
        return _vehicleAttributeDefinitionResourceId;
    }

    @Override
    public void setVehicleAttributeDefinitionResourceId(
        long vehicleAttributeDefinitionResourceId) {
        _vehicleAttributeDefinitionResourceId = vehicleAttributeDefinitionResourceId;

        if (_vehicleAttributeDefinitionResourceRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setVehicleAttributeDefinitionResourceId",
                        long.class);

                method.invoke(_vehicleAttributeDefinitionResourceRemoteModel,
                    vehicleAttributeDefinitionResourceId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getVehicleAttributeDefinitionId() {
        return _vehicleAttributeDefinitionId;
    }

    @Override
    public void setVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId) {
        _vehicleAttributeDefinitionId = vehicleAttributeDefinitionId;

        if (_vehicleAttributeDefinitionResourceRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setVehicleAttributeDefinitionId",
                        long.class);

                method.invoke(_vehicleAttributeDefinitionResourceRemoteModel,
                    vehicleAttributeDefinitionId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCountry() {
        return _country;
    }

    @Override
    public void setCountry(String country) {
        _country = country;

        if (_vehicleAttributeDefinitionResourceRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setCountry", String.class);

                method.invoke(_vehicleAttributeDefinitionResourceRemoteModel,
                    country);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getLanguage() {
        return _language;
    }

    @Override
    public void setLanguage(String language) {
        _language = language;

        if (_vehicleAttributeDefinitionResourceRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setLanguage", String.class);

                method.invoke(_vehicleAttributeDefinitionResourceRemoteModel,
                    language);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTitle() {
        return _title;
    }

    @Override
    public void setTitle(String title) {
        _title = title;

        if (_vehicleAttributeDefinitionResourceRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setTitle", String.class);

                method.invoke(_vehicleAttributeDefinitionResourceRemoteModel,
                    title);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getUnit() {
        return _unit;
    }

    @Override
    public void setUnit(String unit) {
        _unit = unit;

        if (_vehicleAttributeDefinitionResourceRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleAttributeDefinitionResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setUnit", String.class);

                method.invoke(_vehicleAttributeDefinitionResourceRemoteModel,
                    unit);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getVehicleAttributeDefinitionResourceRemoteModel() {
        return _vehicleAttributeDefinitionResourceRemoteModel;
    }

    public void setVehicleAttributeDefinitionResourceRemoteModel(
        BaseModel<?> vehicleAttributeDefinitionResourceRemoteModel) {
        _vehicleAttributeDefinitionResourceRemoteModel = vehicleAttributeDefinitionResourceRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _vehicleAttributeDefinitionResourceRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_vehicleAttributeDefinitionResourceRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            VehicleAttributeDefinitionResourceLocalServiceUtil.addVehicleAttributeDefinitionResource(this);
        } else {
            VehicleAttributeDefinitionResourceLocalServiceUtil.updateVehicleAttributeDefinitionResource(this);
        }
    }

    @Override
    public VehicleAttributeDefinitionResource toEscapedModel() {
        return (VehicleAttributeDefinitionResource) ProxyUtil.newProxyInstance(VehicleAttributeDefinitionResource.class.getClassLoader(),
            new Class[] { VehicleAttributeDefinitionResource.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        VehicleAttributeDefinitionResourceClp clone = new VehicleAttributeDefinitionResourceClp();

        clone.setVehicleAttributeDefinitionResourceId(getVehicleAttributeDefinitionResourceId());
        clone.setVehicleAttributeDefinitionId(getVehicleAttributeDefinitionId());
        clone.setCountry(getCountry());
        clone.setLanguage(getLanguage());
        clone.setTitle(getTitle());
        clone.setUnit(getUnit());

        return clone;
    }

    @Override
    public int compareTo(
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource) {
        int value = 0;

        if (getVehicleAttributeDefinitionId() < vehicleAttributeDefinitionResource.getVehicleAttributeDefinitionId()) {
            value = -1;
        } else if (getVehicleAttributeDefinitionId() > vehicleAttributeDefinitionResource.getVehicleAttributeDefinitionId()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        value = getCountry()
                    .compareToIgnoreCase(vehicleAttributeDefinitionResource.getCountry());

        if (value != 0) {
            return value;
        }

        value = getLanguage()
                    .compareToIgnoreCase(vehicleAttributeDefinitionResource.getLanguage());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VehicleAttributeDefinitionResourceClp)) {
            return false;
        }

        VehicleAttributeDefinitionResourceClp vehicleAttributeDefinitionResource =
            (VehicleAttributeDefinitionResourceClp) obj;

        long primaryKey = vehicleAttributeDefinitionResource.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{vehicleAttributeDefinitionResourceId=");
        sb.append(getVehicleAttributeDefinitionResourceId());
        sb.append(", vehicleAttributeDefinitionId=");
        sb.append(getVehicleAttributeDefinitionId());
        sb.append(", country=");
        sb.append(getCountry());
        sb.append(", language=");
        sb.append(getLanguage());
        sb.append(", title=");
        sb.append(getTitle());
        sb.append(", unit=");
        sb.append(getUnit());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(22);

        sb.append("<model><model-name>");
        sb.append(
            "de.humance.eco.profile.model.VehicleAttributeDefinitionResource");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>vehicleAttributeDefinitionResourceId</column-name><column-value><![CDATA[");
        sb.append(getVehicleAttributeDefinitionResourceId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>vehicleAttributeDefinitionId</column-name><column-value><![CDATA[");
        sb.append(getVehicleAttributeDefinitionId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>country</column-name><column-value><![CDATA[");
        sb.append(getCountry());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>language</column-name><column-value><![CDATA[");
        sb.append(getLanguage());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>title</column-name><column-value><![CDATA[");
        sb.append(getTitle());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>unit</column-name><column-value><![CDATA[");
        sb.append(getUnit());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
