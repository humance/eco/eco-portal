package de.humance.eco.profile.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Humance
 * @generated
 */
public class VehicleAttributeDefinitionResourceSoap implements Serializable {
    private long _vehicleAttributeDefinitionResourceId;
    private long _vehicleAttributeDefinitionId;
    private String _country;
    private String _language;
    private String _title;
    private String _unit;

    public VehicleAttributeDefinitionResourceSoap() {
    }

    public static VehicleAttributeDefinitionResourceSoap toSoapModel(
        VehicleAttributeDefinitionResource model) {
        VehicleAttributeDefinitionResourceSoap soapModel = new VehicleAttributeDefinitionResourceSoap();

        soapModel.setVehicleAttributeDefinitionResourceId(model.getVehicleAttributeDefinitionResourceId());
        soapModel.setVehicleAttributeDefinitionId(model.getVehicleAttributeDefinitionId());
        soapModel.setCountry(model.getCountry());
        soapModel.setLanguage(model.getLanguage());
        soapModel.setTitle(model.getTitle());
        soapModel.setUnit(model.getUnit());

        return soapModel;
    }

    public static VehicleAttributeDefinitionResourceSoap[] toSoapModels(
        VehicleAttributeDefinitionResource[] models) {
        VehicleAttributeDefinitionResourceSoap[] soapModels = new VehicleAttributeDefinitionResourceSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static VehicleAttributeDefinitionResourceSoap[][] toSoapModels(
        VehicleAttributeDefinitionResource[][] models) {
        VehicleAttributeDefinitionResourceSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new VehicleAttributeDefinitionResourceSoap[models.length][models[0].length];
        } else {
            soapModels = new VehicleAttributeDefinitionResourceSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static VehicleAttributeDefinitionResourceSoap[] toSoapModels(
        List<VehicleAttributeDefinitionResource> models) {
        List<VehicleAttributeDefinitionResourceSoap> soapModels = new ArrayList<VehicleAttributeDefinitionResourceSoap>(models.size());

        for (VehicleAttributeDefinitionResource model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new VehicleAttributeDefinitionResourceSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _vehicleAttributeDefinitionResourceId;
    }

    public void setPrimaryKey(long pk) {
        setVehicleAttributeDefinitionResourceId(pk);
    }

    public long getVehicleAttributeDefinitionResourceId() {
        return _vehicleAttributeDefinitionResourceId;
    }

    public void setVehicleAttributeDefinitionResourceId(
        long vehicleAttributeDefinitionResourceId) {
        _vehicleAttributeDefinitionResourceId = vehicleAttributeDefinitionResourceId;
    }

    public long getVehicleAttributeDefinitionId() {
        return _vehicleAttributeDefinitionId;
    }

    public void setVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId) {
        _vehicleAttributeDefinitionId = vehicleAttributeDefinitionId;
    }

    public String getCountry() {
        return _country;
    }

    public void setCountry(String country) {
        _country = country;
    }

    public String getLanguage() {
        return _language;
    }

    public void setLanguage(String language) {
        _language = language;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        _title = title;
    }

    public String getUnit() {
        return _unit;
    }

    public void setUnit(String unit) {
        _unit = unit;
    }
}
