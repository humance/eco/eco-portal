package de.humance.eco.profile.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link VehicleAttributeDefinitionResource}.
 * </p>
 *
 * @author Humance
 * @see VehicleAttributeDefinitionResource
 * @generated
 */
public class VehicleAttributeDefinitionResourceWrapper
    implements VehicleAttributeDefinitionResource,
        ModelWrapper<VehicleAttributeDefinitionResource> {
    private VehicleAttributeDefinitionResource _vehicleAttributeDefinitionResource;

    public VehicleAttributeDefinitionResourceWrapper(
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource) {
        _vehicleAttributeDefinitionResource = vehicleAttributeDefinitionResource;
    }

    @Override
    public Class<?> getModelClass() {
        return VehicleAttributeDefinitionResource.class;
    }

    @Override
    public String getModelClassName() {
        return VehicleAttributeDefinitionResource.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("vehicleAttributeDefinitionResourceId",
            getVehicleAttributeDefinitionResourceId());
        attributes.put("vehicleAttributeDefinitionId",
            getVehicleAttributeDefinitionId());
        attributes.put("country", getCountry());
        attributes.put("language", getLanguage());
        attributes.put("title", getTitle());
        attributes.put("unit", getUnit());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long vehicleAttributeDefinitionResourceId = (Long) attributes.get(
                "vehicleAttributeDefinitionResourceId");

        if (vehicleAttributeDefinitionResourceId != null) {
            setVehicleAttributeDefinitionResourceId(vehicleAttributeDefinitionResourceId);
        }

        Long vehicleAttributeDefinitionId = (Long) attributes.get(
                "vehicleAttributeDefinitionId");

        if (vehicleAttributeDefinitionId != null) {
            setVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
        }

        String country = (String) attributes.get("country");

        if (country != null) {
            setCountry(country);
        }

        String language = (String) attributes.get("language");

        if (language != null) {
            setLanguage(language);
        }

        String title = (String) attributes.get("title");

        if (title != null) {
            setTitle(title);
        }

        String unit = (String) attributes.get("unit");

        if (unit != null) {
            setUnit(unit);
        }
    }

    /**
    * Returns the primary key of this vehicle attribute definition resource.
    *
    * @return the primary key of this vehicle attribute definition resource
    */
    @Override
    public long getPrimaryKey() {
        return _vehicleAttributeDefinitionResource.getPrimaryKey();
    }

    /**
    * Sets the primary key of this vehicle attribute definition resource.
    *
    * @param primaryKey the primary key of this vehicle attribute definition resource
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _vehicleAttributeDefinitionResource.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the vehicle attribute definition resource ID of this vehicle attribute definition resource.
    *
    * @return the vehicle attribute definition resource ID of this vehicle attribute definition resource
    */
    @Override
    public long getVehicleAttributeDefinitionResourceId() {
        return _vehicleAttributeDefinitionResource.getVehicleAttributeDefinitionResourceId();
    }

    /**
    * Sets the vehicle attribute definition resource ID of this vehicle attribute definition resource.
    *
    * @param vehicleAttributeDefinitionResourceId the vehicle attribute definition resource ID of this vehicle attribute definition resource
    */
    @Override
    public void setVehicleAttributeDefinitionResourceId(
        long vehicleAttributeDefinitionResourceId) {
        _vehicleAttributeDefinitionResource.setVehicleAttributeDefinitionResourceId(vehicleAttributeDefinitionResourceId);
    }

    /**
    * Returns the vehicle attribute definition ID of this vehicle attribute definition resource.
    *
    * @return the vehicle attribute definition ID of this vehicle attribute definition resource
    */
    @Override
    public long getVehicleAttributeDefinitionId() {
        return _vehicleAttributeDefinitionResource.getVehicleAttributeDefinitionId();
    }

    /**
    * Sets the vehicle attribute definition ID of this vehicle attribute definition resource.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID of this vehicle attribute definition resource
    */
    @Override
    public void setVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId) {
        _vehicleAttributeDefinitionResource.setVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
    }

    /**
    * Returns the country of this vehicle attribute definition resource.
    *
    * @return the country of this vehicle attribute definition resource
    */
    @Override
    public java.lang.String getCountry() {
        return _vehicleAttributeDefinitionResource.getCountry();
    }

    /**
    * Sets the country of this vehicle attribute definition resource.
    *
    * @param country the country of this vehicle attribute definition resource
    */
    @Override
    public void setCountry(java.lang.String country) {
        _vehicleAttributeDefinitionResource.setCountry(country);
    }

    /**
    * Returns the language of this vehicle attribute definition resource.
    *
    * @return the language of this vehicle attribute definition resource
    */
    @Override
    public java.lang.String getLanguage() {
        return _vehicleAttributeDefinitionResource.getLanguage();
    }

    /**
    * Sets the language of this vehicle attribute definition resource.
    *
    * @param language the language of this vehicle attribute definition resource
    */
    @Override
    public void setLanguage(java.lang.String language) {
        _vehicleAttributeDefinitionResource.setLanguage(language);
    }

    /**
    * Returns the title of this vehicle attribute definition resource.
    *
    * @return the title of this vehicle attribute definition resource
    */
    @Override
    public java.lang.String getTitle() {
        return _vehicleAttributeDefinitionResource.getTitle();
    }

    /**
    * Sets the title of this vehicle attribute definition resource.
    *
    * @param title the title of this vehicle attribute definition resource
    */
    @Override
    public void setTitle(java.lang.String title) {
        _vehicleAttributeDefinitionResource.setTitle(title);
    }

    /**
    * Returns the unit of this vehicle attribute definition resource.
    *
    * @return the unit of this vehicle attribute definition resource
    */
    @Override
    public java.lang.String getUnit() {
        return _vehicleAttributeDefinitionResource.getUnit();
    }

    /**
    * Sets the unit of this vehicle attribute definition resource.
    *
    * @param unit the unit of this vehicle attribute definition resource
    */
    @Override
    public void setUnit(java.lang.String unit) {
        _vehicleAttributeDefinitionResource.setUnit(unit);
    }

    @Override
    public boolean isNew() {
        return _vehicleAttributeDefinitionResource.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _vehicleAttributeDefinitionResource.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _vehicleAttributeDefinitionResource.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _vehicleAttributeDefinitionResource.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _vehicleAttributeDefinitionResource.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _vehicleAttributeDefinitionResource.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _vehicleAttributeDefinitionResource.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _vehicleAttributeDefinitionResource.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _vehicleAttributeDefinitionResource.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _vehicleAttributeDefinitionResource.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _vehicleAttributeDefinitionResource.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new VehicleAttributeDefinitionResourceWrapper((VehicleAttributeDefinitionResource) _vehicleAttributeDefinitionResource.clone());
    }

    @Override
    public int compareTo(
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource) {
        return _vehicleAttributeDefinitionResource.compareTo(vehicleAttributeDefinitionResource);
    }

    @Override
    public int hashCode() {
        return _vehicleAttributeDefinitionResource.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<VehicleAttributeDefinitionResource> toCacheModel() {
        return _vehicleAttributeDefinitionResource.toCacheModel();
    }

    @Override
    public VehicleAttributeDefinitionResource toEscapedModel() {
        return new VehicleAttributeDefinitionResourceWrapper(_vehicleAttributeDefinitionResource.toEscapedModel());
    }

    @Override
    public VehicleAttributeDefinitionResource toUnescapedModel() {
        return new VehicleAttributeDefinitionResourceWrapper(_vehicleAttributeDefinitionResource.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _vehicleAttributeDefinitionResource.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _vehicleAttributeDefinitionResource.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _vehicleAttributeDefinitionResource.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VehicleAttributeDefinitionResourceWrapper)) {
            return false;
        }

        VehicleAttributeDefinitionResourceWrapper vehicleAttributeDefinitionResourceWrapper =
            (VehicleAttributeDefinitionResourceWrapper) obj;

        if (Validator.equals(_vehicleAttributeDefinitionResource,
                    vehicleAttributeDefinitionResourceWrapper._vehicleAttributeDefinitionResource)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public VehicleAttributeDefinitionResource getWrappedVehicleAttributeDefinitionResource() {
        return _vehicleAttributeDefinitionResource;
    }

    @Override
    public VehicleAttributeDefinitionResource getWrappedModel() {
        return _vehicleAttributeDefinitionResource;
    }

    @Override
    public void resetOriginalValues() {
        _vehicleAttributeDefinitionResource.resetOriginalValues();
    }
}
