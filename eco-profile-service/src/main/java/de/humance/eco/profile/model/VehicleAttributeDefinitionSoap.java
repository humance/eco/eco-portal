package de.humance.eco.profile.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Humance
 * @generated
 */
public class VehicleAttributeDefinitionSoap implements Serializable {
    private long _vehicleAttributeDefinitionId;
    private long _creatorId;
    private String _creatorName;
    private Date _createDate;
    private long _modifierId;
    private String _modifierName;
    private Date _modifiedDate;
    private long _vehicleTypeId;
    private String _type;
    private String _title;
    private double _minRangeValue;
    private double _maxRangeValue;
    private String _unit;
    private long _sequenceNumber;

    public VehicleAttributeDefinitionSoap() {
    }

    public static VehicleAttributeDefinitionSoap toSoapModel(
        VehicleAttributeDefinition model) {
        VehicleAttributeDefinitionSoap soapModel = new VehicleAttributeDefinitionSoap();

        soapModel.setVehicleAttributeDefinitionId(model.getVehicleAttributeDefinitionId());
        soapModel.setCreatorId(model.getCreatorId());
        soapModel.setCreatorName(model.getCreatorName());
        soapModel.setCreateDate(model.getCreateDate());
        soapModel.setModifierId(model.getModifierId());
        soapModel.setModifierName(model.getModifierName());
        soapModel.setModifiedDate(model.getModifiedDate());
        soapModel.setVehicleTypeId(model.getVehicleTypeId());
        soapModel.setType(model.getType());
        soapModel.setTitle(model.getTitle());
        soapModel.setMinRangeValue(model.getMinRangeValue());
        soapModel.setMaxRangeValue(model.getMaxRangeValue());
        soapModel.setUnit(model.getUnit());
        soapModel.setSequenceNumber(model.getSequenceNumber());

        return soapModel;
    }

    public static VehicleAttributeDefinitionSoap[] toSoapModels(
        VehicleAttributeDefinition[] models) {
        VehicleAttributeDefinitionSoap[] soapModels = new VehicleAttributeDefinitionSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static VehicleAttributeDefinitionSoap[][] toSoapModels(
        VehicleAttributeDefinition[][] models) {
        VehicleAttributeDefinitionSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new VehicleAttributeDefinitionSoap[models.length][models[0].length];
        } else {
            soapModels = new VehicleAttributeDefinitionSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static VehicleAttributeDefinitionSoap[] toSoapModels(
        List<VehicleAttributeDefinition> models) {
        List<VehicleAttributeDefinitionSoap> soapModels = new ArrayList<VehicleAttributeDefinitionSoap>(models.size());

        for (VehicleAttributeDefinition model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new VehicleAttributeDefinitionSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _vehicleAttributeDefinitionId;
    }

    public void setPrimaryKey(long pk) {
        setVehicleAttributeDefinitionId(pk);
    }

    public long getVehicleAttributeDefinitionId() {
        return _vehicleAttributeDefinitionId;
    }

    public void setVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId) {
        _vehicleAttributeDefinitionId = vehicleAttributeDefinitionId;
    }

    public long getCreatorId() {
        return _creatorId;
    }

    public void setCreatorId(long creatorId) {
        _creatorId = creatorId;
    }

    public String getCreatorName() {
        return _creatorName;
    }

    public void setCreatorName(String creatorName) {
        _creatorName = creatorName;
    }

    public Date getCreateDate() {
        return _createDate;
    }

    public void setCreateDate(Date createDate) {
        _createDate = createDate;
    }

    public long getModifierId() {
        return _modifierId;
    }

    public void setModifierId(long modifierId) {
        _modifierId = modifierId;
    }

    public String getModifierName() {
        return _modifierName;
    }

    public void setModifierName(String modifierName) {
        _modifierName = modifierName;
    }

    public Date getModifiedDate() {
        return _modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;
    }

    public long getVehicleTypeId() {
        return _vehicleTypeId;
    }

    public void setVehicleTypeId(long vehicleTypeId) {
        _vehicleTypeId = vehicleTypeId;
    }

    public String getType() {
        return _type;
    }

    public void setType(String type) {
        _type = type;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        _title = title;
    }

    public double getMinRangeValue() {
        return _minRangeValue;
    }

    public void setMinRangeValue(double minRangeValue) {
        _minRangeValue = minRangeValue;
    }

    public double getMaxRangeValue() {
        return _maxRangeValue;
    }

    public void setMaxRangeValue(double maxRangeValue) {
        _maxRangeValue = maxRangeValue;
    }

    public String getUnit() {
        return _unit;
    }

    public void setUnit(String unit) {
        _unit = unit;
    }

    public long getSequenceNumber() {
        return _sequenceNumber;
    }

    public void setSequenceNumber(long sequenceNumber) {
        _sequenceNumber = sequenceNumber;
    }
}
