package de.humance.eco.profile.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link VehicleAttributeDefinition}.
 * </p>
 *
 * @author Humance
 * @see VehicleAttributeDefinition
 * @generated
 */
public class VehicleAttributeDefinitionWrapper
    implements VehicleAttributeDefinition,
        ModelWrapper<VehicleAttributeDefinition> {
    private VehicleAttributeDefinition _vehicleAttributeDefinition;

    public VehicleAttributeDefinitionWrapper(
        VehicleAttributeDefinition vehicleAttributeDefinition) {
        _vehicleAttributeDefinition = vehicleAttributeDefinition;
    }

    @Override
    public Class<?> getModelClass() {
        return VehicleAttributeDefinition.class;
    }

    @Override
    public String getModelClassName() {
        return VehicleAttributeDefinition.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("vehicleAttributeDefinitionId",
            getVehicleAttributeDefinitionId());
        attributes.put("creatorId", getCreatorId());
        attributes.put("creatorName", getCreatorName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifierId", getModifierId());
        attributes.put("modifierName", getModifierName());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("vehicleTypeId", getVehicleTypeId());
        attributes.put("type", getType());
        attributes.put("title", getTitle());
        attributes.put("minRangeValue", getMinRangeValue());
        attributes.put("maxRangeValue", getMaxRangeValue());
        attributes.put("unit", getUnit());
        attributes.put("sequenceNumber", getSequenceNumber());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long vehicleAttributeDefinitionId = (Long) attributes.get(
                "vehicleAttributeDefinitionId");

        if (vehicleAttributeDefinitionId != null) {
            setVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
        }

        Long creatorId = (Long) attributes.get("creatorId");

        if (creatorId != null) {
            setCreatorId(creatorId);
        }

        String creatorName = (String) attributes.get("creatorName");

        if (creatorName != null) {
            setCreatorName(creatorName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Long modifierId = (Long) attributes.get("modifierId");

        if (modifierId != null) {
            setModifierId(modifierId);
        }

        String modifierName = (String) attributes.get("modifierName");

        if (modifierName != null) {
            setModifierName(modifierName);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        Long vehicleTypeId = (Long) attributes.get("vehicleTypeId");

        if (vehicleTypeId != null) {
            setVehicleTypeId(vehicleTypeId);
        }

        String type = (String) attributes.get("type");

        if (type != null) {
            setType(type);
        }

        String title = (String) attributes.get("title");

        if (title != null) {
            setTitle(title);
        }

        Double minRangeValue = (Double) attributes.get("minRangeValue");

        if (minRangeValue != null) {
            setMinRangeValue(minRangeValue);
        }

        Double maxRangeValue = (Double) attributes.get("maxRangeValue");

        if (maxRangeValue != null) {
            setMaxRangeValue(maxRangeValue);
        }

        String unit = (String) attributes.get("unit");

        if (unit != null) {
            setUnit(unit);
        }

        Long sequenceNumber = (Long) attributes.get("sequenceNumber");

        if (sequenceNumber != null) {
            setSequenceNumber(sequenceNumber);
        }
    }

    /**
    * Returns the primary key of this vehicle attribute definition.
    *
    * @return the primary key of this vehicle attribute definition
    */
    @Override
    public long getPrimaryKey() {
        return _vehicleAttributeDefinition.getPrimaryKey();
    }

    /**
    * Sets the primary key of this vehicle attribute definition.
    *
    * @param primaryKey the primary key of this vehicle attribute definition
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _vehicleAttributeDefinition.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the vehicle attribute definition ID of this vehicle attribute definition.
    *
    * @return the vehicle attribute definition ID of this vehicle attribute definition
    */
    @Override
    public long getVehicleAttributeDefinitionId() {
        return _vehicleAttributeDefinition.getVehicleAttributeDefinitionId();
    }

    /**
    * Sets the vehicle attribute definition ID of this vehicle attribute definition.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID of this vehicle attribute definition
    */
    @Override
    public void setVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId) {
        _vehicleAttributeDefinition.setVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
    }

    /**
    * Returns the creator ID of this vehicle attribute definition.
    *
    * @return the creator ID of this vehicle attribute definition
    */
    @Override
    public long getCreatorId() {
        return _vehicleAttributeDefinition.getCreatorId();
    }

    /**
    * Sets the creator ID of this vehicle attribute definition.
    *
    * @param creatorId the creator ID of this vehicle attribute definition
    */
    @Override
    public void setCreatorId(long creatorId) {
        _vehicleAttributeDefinition.setCreatorId(creatorId);
    }

    /**
    * Returns the creator name of this vehicle attribute definition.
    *
    * @return the creator name of this vehicle attribute definition
    */
    @Override
    public java.lang.String getCreatorName() {
        return _vehicleAttributeDefinition.getCreatorName();
    }

    /**
    * Sets the creator name of this vehicle attribute definition.
    *
    * @param creatorName the creator name of this vehicle attribute definition
    */
    @Override
    public void setCreatorName(java.lang.String creatorName) {
        _vehicleAttributeDefinition.setCreatorName(creatorName);
    }

    /**
    * Returns the create date of this vehicle attribute definition.
    *
    * @return the create date of this vehicle attribute definition
    */
    @Override
    public java.util.Date getCreateDate() {
        return _vehicleAttributeDefinition.getCreateDate();
    }

    /**
    * Sets the create date of this vehicle attribute definition.
    *
    * @param createDate the create date of this vehicle attribute definition
    */
    @Override
    public void setCreateDate(java.util.Date createDate) {
        _vehicleAttributeDefinition.setCreateDate(createDate);
    }

    /**
    * Returns the modifier ID of this vehicle attribute definition.
    *
    * @return the modifier ID of this vehicle attribute definition
    */
    @Override
    public long getModifierId() {
        return _vehicleAttributeDefinition.getModifierId();
    }

    /**
    * Sets the modifier ID of this vehicle attribute definition.
    *
    * @param modifierId the modifier ID of this vehicle attribute definition
    */
    @Override
    public void setModifierId(long modifierId) {
        _vehicleAttributeDefinition.setModifierId(modifierId);
    }

    /**
    * Returns the modifier name of this vehicle attribute definition.
    *
    * @return the modifier name of this vehicle attribute definition
    */
    @Override
    public java.lang.String getModifierName() {
        return _vehicleAttributeDefinition.getModifierName();
    }

    /**
    * Sets the modifier name of this vehicle attribute definition.
    *
    * @param modifierName the modifier name of this vehicle attribute definition
    */
    @Override
    public void setModifierName(java.lang.String modifierName) {
        _vehicleAttributeDefinition.setModifierName(modifierName);
    }

    /**
    * Returns the modified date of this vehicle attribute definition.
    *
    * @return the modified date of this vehicle attribute definition
    */
    @Override
    public java.util.Date getModifiedDate() {
        return _vehicleAttributeDefinition.getModifiedDate();
    }

    /**
    * Sets the modified date of this vehicle attribute definition.
    *
    * @param modifiedDate the modified date of this vehicle attribute definition
    */
    @Override
    public void setModifiedDate(java.util.Date modifiedDate) {
        _vehicleAttributeDefinition.setModifiedDate(modifiedDate);
    }

    /**
    * Returns the vehicle type ID of this vehicle attribute definition.
    *
    * @return the vehicle type ID of this vehicle attribute definition
    */
    @Override
    public long getVehicleTypeId() {
        return _vehicleAttributeDefinition.getVehicleTypeId();
    }

    /**
    * Sets the vehicle type ID of this vehicle attribute definition.
    *
    * @param vehicleTypeId the vehicle type ID of this vehicle attribute definition
    */
    @Override
    public void setVehicleTypeId(long vehicleTypeId) {
        _vehicleAttributeDefinition.setVehicleTypeId(vehicleTypeId);
    }

    /**
    * Returns the type of this vehicle attribute definition.
    *
    * @return the type of this vehicle attribute definition
    */
    @Override
    public java.lang.String getType() {
        return _vehicleAttributeDefinition.getType();
    }

    /**
    * Sets the type of this vehicle attribute definition.
    *
    * @param type the type of this vehicle attribute definition
    */
    @Override
    public void setType(java.lang.String type) {
        _vehicleAttributeDefinition.setType(type);
    }

    /**
    * Returns the title of this vehicle attribute definition.
    *
    * @return the title of this vehicle attribute definition
    */
    @Override
    public java.lang.String getTitle() {
        return _vehicleAttributeDefinition.getTitle();
    }

    /**
    * Sets the title of this vehicle attribute definition.
    *
    * @param title the title of this vehicle attribute definition
    */
    @Override
    public void setTitle(java.lang.String title) {
        _vehicleAttributeDefinition.setTitle(title);
    }

    /**
    * Returns the min range value of this vehicle attribute definition.
    *
    * @return the min range value of this vehicle attribute definition
    */
    @Override
    public double getMinRangeValue() {
        return _vehicleAttributeDefinition.getMinRangeValue();
    }

    /**
    * Sets the min range value of this vehicle attribute definition.
    *
    * @param minRangeValue the min range value of this vehicle attribute definition
    */
    @Override
    public void setMinRangeValue(double minRangeValue) {
        _vehicleAttributeDefinition.setMinRangeValue(minRangeValue);
    }

    /**
    * Returns the max range value of this vehicle attribute definition.
    *
    * @return the max range value of this vehicle attribute definition
    */
    @Override
    public double getMaxRangeValue() {
        return _vehicleAttributeDefinition.getMaxRangeValue();
    }

    /**
    * Sets the max range value of this vehicle attribute definition.
    *
    * @param maxRangeValue the max range value of this vehicle attribute definition
    */
    @Override
    public void setMaxRangeValue(double maxRangeValue) {
        _vehicleAttributeDefinition.setMaxRangeValue(maxRangeValue);
    }

    /**
    * Returns the unit of this vehicle attribute definition.
    *
    * @return the unit of this vehicle attribute definition
    */
    @Override
    public java.lang.String getUnit() {
        return _vehicleAttributeDefinition.getUnit();
    }

    /**
    * Sets the unit of this vehicle attribute definition.
    *
    * @param unit the unit of this vehicle attribute definition
    */
    @Override
    public void setUnit(java.lang.String unit) {
        _vehicleAttributeDefinition.setUnit(unit);
    }

    /**
    * Returns the sequence number of this vehicle attribute definition.
    *
    * @return the sequence number of this vehicle attribute definition
    */
    @Override
    public long getSequenceNumber() {
        return _vehicleAttributeDefinition.getSequenceNumber();
    }

    /**
    * Sets the sequence number of this vehicle attribute definition.
    *
    * @param sequenceNumber the sequence number of this vehicle attribute definition
    */
    @Override
    public void setSequenceNumber(long sequenceNumber) {
        _vehicleAttributeDefinition.setSequenceNumber(sequenceNumber);
    }

    @Override
    public boolean isNew() {
        return _vehicleAttributeDefinition.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _vehicleAttributeDefinition.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _vehicleAttributeDefinition.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _vehicleAttributeDefinition.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _vehicleAttributeDefinition.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _vehicleAttributeDefinition.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _vehicleAttributeDefinition.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _vehicleAttributeDefinition.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _vehicleAttributeDefinition.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _vehicleAttributeDefinition.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _vehicleAttributeDefinition.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new VehicleAttributeDefinitionWrapper((VehicleAttributeDefinition) _vehicleAttributeDefinition.clone());
    }

    @Override
    public int compareTo(VehicleAttributeDefinition vehicleAttributeDefinition) {
        return _vehicleAttributeDefinition.compareTo(vehicleAttributeDefinition);
    }

    @Override
    public int hashCode() {
        return _vehicleAttributeDefinition.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<VehicleAttributeDefinition> toCacheModel() {
        return _vehicleAttributeDefinition.toCacheModel();
    }

    @Override
    public VehicleAttributeDefinition toEscapedModel() {
        return new VehicleAttributeDefinitionWrapper(_vehicleAttributeDefinition.toEscapedModel());
    }

    @Override
    public VehicleAttributeDefinition toUnescapedModel() {
        return new VehicleAttributeDefinitionWrapper(_vehicleAttributeDefinition.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _vehicleAttributeDefinition.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _vehicleAttributeDefinition.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _vehicleAttributeDefinition.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VehicleAttributeDefinitionWrapper)) {
            return false;
        }

        VehicleAttributeDefinitionWrapper vehicleAttributeDefinitionWrapper = (VehicleAttributeDefinitionWrapper) obj;

        if (Validator.equals(_vehicleAttributeDefinition,
                    vehicleAttributeDefinitionWrapper._vehicleAttributeDefinition)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public VehicleAttributeDefinition getWrappedVehicleAttributeDefinition() {
        return _vehicleAttributeDefinition;
    }

    @Override
    public VehicleAttributeDefinition getWrappedModel() {
        return _vehicleAttributeDefinition;
    }

    @Override
    public void resetOriginalValues() {
        _vehicleAttributeDefinition.resetOriginalValues();
    }
}
