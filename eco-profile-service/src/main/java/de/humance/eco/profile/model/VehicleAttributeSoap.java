package de.humance.eco.profile.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Humance
 * @generated
 */
public class VehicleAttributeSoap implements Serializable {
    private long _vehicleAttributeId;
    private long _vehicleId;
    private long _vehicleAttributeDefinitionId;
    private String _type;
    private long _sequenceNumber;
    private String _attributeValue;

    public VehicleAttributeSoap() {
    }

    public static VehicleAttributeSoap toSoapModel(VehicleAttribute model) {
        VehicleAttributeSoap soapModel = new VehicleAttributeSoap();

        soapModel.setVehicleAttributeId(model.getVehicleAttributeId());
        soapModel.setVehicleId(model.getVehicleId());
        soapModel.setVehicleAttributeDefinitionId(model.getVehicleAttributeDefinitionId());
        soapModel.setType(model.getType());
        soapModel.setSequenceNumber(model.getSequenceNumber());
        soapModel.setAttributeValue(model.getAttributeValue());

        return soapModel;
    }

    public static VehicleAttributeSoap[] toSoapModels(VehicleAttribute[] models) {
        VehicleAttributeSoap[] soapModels = new VehicleAttributeSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static VehicleAttributeSoap[][] toSoapModels(
        VehicleAttribute[][] models) {
        VehicleAttributeSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new VehicleAttributeSoap[models.length][models[0].length];
        } else {
            soapModels = new VehicleAttributeSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static VehicleAttributeSoap[] toSoapModels(
        List<VehicleAttribute> models) {
        List<VehicleAttributeSoap> soapModels = new ArrayList<VehicleAttributeSoap>(models.size());

        for (VehicleAttribute model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new VehicleAttributeSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _vehicleAttributeId;
    }

    public void setPrimaryKey(long pk) {
        setVehicleAttributeId(pk);
    }

    public long getVehicleAttributeId() {
        return _vehicleAttributeId;
    }

    public void setVehicleAttributeId(long vehicleAttributeId) {
        _vehicleAttributeId = vehicleAttributeId;
    }

    public long getVehicleId() {
        return _vehicleId;
    }

    public void setVehicleId(long vehicleId) {
        _vehicleId = vehicleId;
    }

    public long getVehicleAttributeDefinitionId() {
        return _vehicleAttributeDefinitionId;
    }

    public void setVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId) {
        _vehicleAttributeDefinitionId = vehicleAttributeDefinitionId;
    }

    public String getType() {
        return _type;
    }

    public void setType(String type) {
        _type = type;
    }

    public long getSequenceNumber() {
        return _sequenceNumber;
    }

    public void setSequenceNumber(long sequenceNumber) {
        _sequenceNumber = sequenceNumber;
    }

    public String getAttributeValue() {
        return _attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        _attributeValue = attributeValue;
    }
}
