package de.humance.eco.profile.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link VehicleAttribute}.
 * </p>
 *
 * @author Humance
 * @see VehicleAttribute
 * @generated
 */
public class VehicleAttributeWrapper implements VehicleAttribute,
    ModelWrapper<VehicleAttribute> {
    private VehicleAttribute _vehicleAttribute;

    public VehicleAttributeWrapper(VehicleAttribute vehicleAttribute) {
        _vehicleAttribute = vehicleAttribute;
    }

    @Override
    public Class<?> getModelClass() {
        return VehicleAttribute.class;
    }

    @Override
    public String getModelClassName() {
        return VehicleAttribute.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("vehicleAttributeId", getVehicleAttributeId());
        attributes.put("vehicleId", getVehicleId());
        attributes.put("vehicleAttributeDefinitionId",
            getVehicleAttributeDefinitionId());
        attributes.put("type", getType());
        attributes.put("sequenceNumber", getSequenceNumber());
        attributes.put("attributeValue", getAttributeValue());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long vehicleAttributeId = (Long) attributes.get("vehicleAttributeId");

        if (vehicleAttributeId != null) {
            setVehicleAttributeId(vehicleAttributeId);
        }

        Long vehicleId = (Long) attributes.get("vehicleId");

        if (vehicleId != null) {
            setVehicleId(vehicleId);
        }

        Long vehicleAttributeDefinitionId = (Long) attributes.get(
                "vehicleAttributeDefinitionId");

        if (vehicleAttributeDefinitionId != null) {
            setVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
        }

        String type = (String) attributes.get("type");

        if (type != null) {
            setType(type);
        }

        Long sequenceNumber = (Long) attributes.get("sequenceNumber");

        if (sequenceNumber != null) {
            setSequenceNumber(sequenceNumber);
        }

        String attributeValue = (String) attributes.get("attributeValue");

        if (attributeValue != null) {
            setAttributeValue(attributeValue);
        }
    }

    /**
    * Returns the primary key of this vehicle attribute.
    *
    * @return the primary key of this vehicle attribute
    */
    @Override
    public long getPrimaryKey() {
        return _vehicleAttribute.getPrimaryKey();
    }

    /**
    * Sets the primary key of this vehicle attribute.
    *
    * @param primaryKey the primary key of this vehicle attribute
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _vehicleAttribute.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the vehicle attribute ID of this vehicle attribute.
    *
    * @return the vehicle attribute ID of this vehicle attribute
    */
    @Override
    public long getVehicleAttributeId() {
        return _vehicleAttribute.getVehicleAttributeId();
    }

    /**
    * Sets the vehicle attribute ID of this vehicle attribute.
    *
    * @param vehicleAttributeId the vehicle attribute ID of this vehicle attribute
    */
    @Override
    public void setVehicleAttributeId(long vehicleAttributeId) {
        _vehicleAttribute.setVehicleAttributeId(vehicleAttributeId);
    }

    /**
    * Returns the vehicle ID of this vehicle attribute.
    *
    * @return the vehicle ID of this vehicle attribute
    */
    @Override
    public long getVehicleId() {
        return _vehicleAttribute.getVehicleId();
    }

    /**
    * Sets the vehicle ID of this vehicle attribute.
    *
    * @param vehicleId the vehicle ID of this vehicle attribute
    */
    @Override
    public void setVehicleId(long vehicleId) {
        _vehicleAttribute.setVehicleId(vehicleId);
    }

    /**
    * Returns the vehicle attribute definition ID of this vehicle attribute.
    *
    * @return the vehicle attribute definition ID of this vehicle attribute
    */
    @Override
    public long getVehicleAttributeDefinitionId() {
        return _vehicleAttribute.getVehicleAttributeDefinitionId();
    }

    /**
    * Sets the vehicle attribute definition ID of this vehicle attribute.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID of this vehicle attribute
    */
    @Override
    public void setVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId) {
        _vehicleAttribute.setVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
    }

    /**
    * Returns the type of this vehicle attribute.
    *
    * @return the type of this vehicle attribute
    */
    @Override
    public java.lang.String getType() {
        return _vehicleAttribute.getType();
    }

    /**
    * Sets the type of this vehicle attribute.
    *
    * @param type the type of this vehicle attribute
    */
    @Override
    public void setType(java.lang.String type) {
        _vehicleAttribute.setType(type);
    }

    /**
    * Returns the sequence number of this vehicle attribute.
    *
    * @return the sequence number of this vehicle attribute
    */
    @Override
    public long getSequenceNumber() {
        return _vehicleAttribute.getSequenceNumber();
    }

    /**
    * Sets the sequence number of this vehicle attribute.
    *
    * @param sequenceNumber the sequence number of this vehicle attribute
    */
    @Override
    public void setSequenceNumber(long sequenceNumber) {
        _vehicleAttribute.setSequenceNumber(sequenceNumber);
    }

    /**
    * Returns the attribute value of this vehicle attribute.
    *
    * @return the attribute value of this vehicle attribute
    */
    @Override
    public java.lang.String getAttributeValue() {
        return _vehicleAttribute.getAttributeValue();
    }

    /**
    * Sets the attribute value of this vehicle attribute.
    *
    * @param attributeValue the attribute value of this vehicle attribute
    */
    @Override
    public void setAttributeValue(java.lang.String attributeValue) {
        _vehicleAttribute.setAttributeValue(attributeValue);
    }

    @Override
    public boolean isNew() {
        return _vehicleAttribute.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _vehicleAttribute.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _vehicleAttribute.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _vehicleAttribute.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _vehicleAttribute.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _vehicleAttribute.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _vehicleAttribute.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _vehicleAttribute.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _vehicleAttribute.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _vehicleAttribute.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _vehicleAttribute.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new VehicleAttributeWrapper((VehicleAttribute) _vehicleAttribute.clone());
    }

    @Override
    public int compareTo(VehicleAttribute vehicleAttribute) {
        return _vehicleAttribute.compareTo(vehicleAttribute);
    }

    @Override
    public int hashCode() {
        return _vehicleAttribute.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<VehicleAttribute> toCacheModel() {
        return _vehicleAttribute.toCacheModel();
    }

    @Override
    public VehicleAttribute toEscapedModel() {
        return new VehicleAttributeWrapper(_vehicleAttribute.toEscapedModel());
    }

    @Override
    public VehicleAttribute toUnescapedModel() {
        return new VehicleAttributeWrapper(_vehicleAttribute.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _vehicleAttribute.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _vehicleAttribute.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _vehicleAttribute.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VehicleAttributeWrapper)) {
            return false;
        }

        VehicleAttributeWrapper vehicleAttributeWrapper = (VehicleAttributeWrapper) obj;

        if (Validator.equals(_vehicleAttribute,
                    vehicleAttributeWrapper._vehicleAttribute)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public VehicleAttribute getWrappedVehicleAttribute() {
        return _vehicleAttribute;
    }

    @Override
    public VehicleAttribute getWrappedModel() {
        return _vehicleAttribute;
    }

    @Override
    public void resetOriginalValues() {
        _vehicleAttribute.resetOriginalValues();
    }
}
