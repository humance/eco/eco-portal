package de.humance.eco.profile.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import de.humance.eco.profile.service.ClpSerializer;
import de.humance.eco.profile.service.VehicleLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class VehicleClp extends BaseModelImpl<Vehicle> implements Vehicle {
    private long _vehicleId;
    private long _creatorId;
    private String _creatorName;
    private Date _createDate;
    private long _modifierId;
    private String _modifierName;
    private Date _modifiedDate;
    private long _driverId;
    private long _typeId;
    private long _manufacturerId;
    private long _organizationId;
    private String _modelName;
    private long _dimensionHeight;
    private long _dimensionWidth;
    private long _dimensionDepth;
    private String _licensePlate;
    private BaseModel<?> _vehicleRemoteModel;

    public VehicleClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Vehicle.class;
    }

    @Override
    public String getModelClassName() {
        return Vehicle.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _vehicleId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setVehicleId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _vehicleId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("vehicleId", getVehicleId());
        attributes.put("creatorId", getCreatorId());
        attributes.put("creatorName", getCreatorName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifierId", getModifierId());
        attributes.put("modifierName", getModifierName());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("driverId", getDriverId());
        attributes.put("typeId", getTypeId());
        attributes.put("manufacturerId", getManufacturerId());
        attributes.put("organizationId", getOrganizationId());
        attributes.put("modelName", getModelName());
        attributes.put("dimensionHeight", getDimensionHeight());
        attributes.put("dimensionWidth", getDimensionWidth());
        attributes.put("dimensionDepth", getDimensionDepth());
        attributes.put("licensePlate", getLicensePlate());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long vehicleId = (Long) attributes.get("vehicleId");

        if (vehicleId != null) {
            setVehicleId(vehicleId);
        }

        Long creatorId = (Long) attributes.get("creatorId");

        if (creatorId != null) {
            setCreatorId(creatorId);
        }

        String creatorName = (String) attributes.get("creatorName");

        if (creatorName != null) {
            setCreatorName(creatorName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Long modifierId = (Long) attributes.get("modifierId");

        if (modifierId != null) {
            setModifierId(modifierId);
        }

        String modifierName = (String) attributes.get("modifierName");

        if (modifierName != null) {
            setModifierName(modifierName);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        Long driverId = (Long) attributes.get("driverId");

        if (driverId != null) {
            setDriverId(driverId);
        }

        Long typeId = (Long) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        Long manufacturerId = (Long) attributes.get("manufacturerId");

        if (manufacturerId != null) {
            setManufacturerId(manufacturerId);
        }

        Long organizationId = (Long) attributes.get("organizationId");

        if (organizationId != null) {
            setOrganizationId(organizationId);
        }

        String modelName = (String) attributes.get("modelName");

        if (modelName != null) {
            setModelName(modelName);
        }

        Long dimensionHeight = (Long) attributes.get("dimensionHeight");

        if (dimensionHeight != null) {
            setDimensionHeight(dimensionHeight);
        }

        Long dimensionWidth = (Long) attributes.get("dimensionWidth");

        if (dimensionWidth != null) {
            setDimensionWidth(dimensionWidth);
        }

        Long dimensionDepth = (Long) attributes.get("dimensionDepth");

        if (dimensionDepth != null) {
            setDimensionDepth(dimensionDepth);
        }

        String licensePlate = (String) attributes.get("licensePlate");

        if (licensePlate != null) {
            setLicensePlate(licensePlate);
        }
    }

    @Override
    public long getVehicleId() {
        return _vehicleId;
    }

    @Override
    public void setVehicleId(long vehicleId) {
        _vehicleId = vehicleId;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setVehicleId", long.class);

                method.invoke(_vehicleRemoteModel, vehicleId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getCreatorId() {
        return _creatorId;
    }

    @Override
    public void setCreatorId(long creatorId) {
        _creatorId = creatorId;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setCreatorId", long.class);

                method.invoke(_vehicleRemoteModel, creatorId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCreatorName() {
        return _creatorName;
    }

    @Override
    public void setCreatorName(String creatorName) {
        _creatorName = creatorName;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setCreatorName", String.class);

                method.invoke(_vehicleRemoteModel, creatorName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getCreateDate() {
        return _createDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        _createDate = createDate;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setCreateDate", Date.class);

                method.invoke(_vehicleRemoteModel, createDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getModifierId() {
        return _modifierId;
    }

    @Override
    public void setModifierId(long modifierId) {
        _modifierId = modifierId;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setModifierId", long.class);

                method.invoke(_vehicleRemoteModel, modifierId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getModifierName() {
        return _modifierName;
    }

    @Override
    public void setModifierName(String modifierName) {
        _modifierName = modifierName;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setModifierName", String.class);

                method.invoke(_vehicleRemoteModel, modifierName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getModifiedDate() {
        return _modifiedDate;
    }

    @Override
    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setModifiedDate", Date.class);

                method.invoke(_vehicleRemoteModel, modifiedDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getDriverId() {
        return _driverId;
    }

    @Override
    public void setDriverId(long driverId) {
        _driverId = driverId;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setDriverId", long.class);

                method.invoke(_vehicleRemoteModel, driverId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getTypeId() {
        return _typeId;
    }

    @Override
    public void setTypeId(long typeId) {
        _typeId = typeId;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setTypeId", long.class);

                method.invoke(_vehicleRemoteModel, typeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getManufacturerId() {
        return _manufacturerId;
    }

    @Override
    public void setManufacturerId(long manufacturerId) {
        _manufacturerId = manufacturerId;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setManufacturerId", long.class);

                method.invoke(_vehicleRemoteModel, manufacturerId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOrganizationId() {
        return _organizationId;
    }

    @Override
    public void setOrganizationId(long organizationId) {
        _organizationId = organizationId;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setOrganizationId", long.class);

                method.invoke(_vehicleRemoteModel, organizationId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getModelName() {
        return _modelName;
    }

    @Override
    public void setModelName(String modelName) {
        _modelName = modelName;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setModelName", String.class);

                method.invoke(_vehicleRemoteModel, modelName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getDimensionHeight() {
        return _dimensionHeight;
    }

    @Override
    public void setDimensionHeight(long dimensionHeight) {
        _dimensionHeight = dimensionHeight;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setDimensionHeight", long.class);

                method.invoke(_vehicleRemoteModel, dimensionHeight);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getDimensionWidth() {
        return _dimensionWidth;
    }

    @Override
    public void setDimensionWidth(long dimensionWidth) {
        _dimensionWidth = dimensionWidth;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setDimensionWidth", long.class);

                method.invoke(_vehicleRemoteModel, dimensionWidth);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getDimensionDepth() {
        return _dimensionDepth;
    }

    @Override
    public void setDimensionDepth(long dimensionDepth) {
        _dimensionDepth = dimensionDepth;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setDimensionDepth", long.class);

                method.invoke(_vehicleRemoteModel, dimensionDepth);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getLicensePlate() {
        return _licensePlate;
    }

    @Override
    public void setLicensePlate(String licensePlate) {
        _licensePlate = licensePlate;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setLicensePlate", String.class);

                method.invoke(_vehicleRemoteModel, licensePlate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getVehicleRemoteModel() {
        return _vehicleRemoteModel;
    }

    public void setVehicleRemoteModel(BaseModel<?> vehicleRemoteModel) {
        _vehicleRemoteModel = vehicleRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _vehicleRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_vehicleRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            VehicleLocalServiceUtil.addVehicle(this);
        } else {
            VehicleLocalServiceUtil.updateVehicle(this);
        }
    }

    @Override
    public Vehicle toEscapedModel() {
        return (Vehicle) ProxyUtil.newProxyInstance(Vehicle.class.getClassLoader(),
            new Class[] { Vehicle.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        VehicleClp clone = new VehicleClp();

        clone.setVehicleId(getVehicleId());
        clone.setCreatorId(getCreatorId());
        clone.setCreatorName(getCreatorName());
        clone.setCreateDate(getCreateDate());
        clone.setModifierId(getModifierId());
        clone.setModifierName(getModifierName());
        clone.setModifiedDate(getModifiedDate());
        clone.setDriverId(getDriverId());
        clone.setTypeId(getTypeId());
        clone.setManufacturerId(getManufacturerId());
        clone.setOrganizationId(getOrganizationId());
        clone.setModelName(getModelName());
        clone.setDimensionHeight(getDimensionHeight());
        clone.setDimensionWidth(getDimensionWidth());
        clone.setDimensionDepth(getDimensionDepth());
        clone.setLicensePlate(getLicensePlate());

        return clone;
    }

    @Override
    public int compareTo(Vehicle vehicle) {
        int value = 0;

        if (getManufacturerId() < vehicle.getManufacturerId()) {
            value = -1;
        } else if (getManufacturerId() > vehicle.getManufacturerId()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        value = getModelName().compareToIgnoreCase(vehicle.getModelName());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VehicleClp)) {
            return false;
        }

        VehicleClp vehicle = (VehicleClp) obj;

        long primaryKey = vehicle.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(33);

        sb.append("{vehicleId=");
        sb.append(getVehicleId());
        sb.append(", creatorId=");
        sb.append(getCreatorId());
        sb.append(", creatorName=");
        sb.append(getCreatorName());
        sb.append(", createDate=");
        sb.append(getCreateDate());
        sb.append(", modifierId=");
        sb.append(getModifierId());
        sb.append(", modifierName=");
        sb.append(getModifierName());
        sb.append(", modifiedDate=");
        sb.append(getModifiedDate());
        sb.append(", driverId=");
        sb.append(getDriverId());
        sb.append(", typeId=");
        sb.append(getTypeId());
        sb.append(", manufacturerId=");
        sb.append(getManufacturerId());
        sb.append(", organizationId=");
        sb.append(getOrganizationId());
        sb.append(", modelName=");
        sb.append(getModelName());
        sb.append(", dimensionHeight=");
        sb.append(getDimensionHeight());
        sb.append(", dimensionWidth=");
        sb.append(getDimensionWidth());
        sb.append(", dimensionDepth=");
        sb.append(getDimensionDepth());
        sb.append(", licensePlate=");
        sb.append(getLicensePlate());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(52);

        sb.append("<model><model-name>");
        sb.append("de.humance.eco.profile.model.Vehicle");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>vehicleId</column-name><column-value><![CDATA[");
        sb.append(getVehicleId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>creatorId</column-name><column-value><![CDATA[");
        sb.append(getCreatorId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>creatorName</column-name><column-value><![CDATA[");
        sb.append(getCreatorName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>createDate</column-name><column-value><![CDATA[");
        sb.append(getCreateDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifierId</column-name><column-value><![CDATA[");
        sb.append(getModifierId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifierName</column-name><column-value><![CDATA[");
        sb.append(getModifierName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
        sb.append(getModifiedDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>driverId</column-name><column-value><![CDATA[");
        sb.append(getDriverId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>typeId</column-name><column-value><![CDATA[");
        sb.append(getTypeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>manufacturerId</column-name><column-value><![CDATA[");
        sb.append(getManufacturerId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>organizationId</column-name><column-value><![CDATA[");
        sb.append(getOrganizationId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modelName</column-name><column-value><![CDATA[");
        sb.append(getModelName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>dimensionHeight</column-name><column-value><![CDATA[");
        sb.append(getDimensionHeight());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>dimensionWidth</column-name><column-value><![CDATA[");
        sb.append(getDimensionWidth());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>dimensionDepth</column-name><column-value><![CDATA[");
        sb.append(getDimensionDepth());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>licensePlate</column-name><column-value><![CDATA[");
        sb.append(getLicensePlate());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
