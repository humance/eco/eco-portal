package de.humance.eco.profile.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the VehicleManufacturer service. Represents a row in the &quot;Profile_VehicleManufacturer&quot; database table, with each column mapped to a property of this class.
 *
 * @author Humance
 * @see VehicleManufacturerModel
 * @see de.humance.eco.profile.model.impl.VehicleManufacturerImpl
 * @see de.humance.eco.profile.model.impl.VehicleManufacturerModelImpl
 * @generated
 */
public interface VehicleManufacturer extends VehicleManufacturerModel,
    PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link de.humance.eco.profile.model.impl.VehicleManufacturerImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
