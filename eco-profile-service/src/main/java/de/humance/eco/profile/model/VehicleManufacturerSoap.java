package de.humance.eco.profile.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Humance
 * @generated
 */
public class VehicleManufacturerSoap implements Serializable {
    private long _vehicleManufacturerId;
    private long _creatorId;
    private String _creatorName;
    private Date _createDate;
    private long _modifierId;
    private String _modifierName;
    private Date _modifiedDate;
    private String _name;
    private long _iconId;

    public VehicleManufacturerSoap() {
    }

    public static VehicleManufacturerSoap toSoapModel(VehicleManufacturer model) {
        VehicleManufacturerSoap soapModel = new VehicleManufacturerSoap();

        soapModel.setVehicleManufacturerId(model.getVehicleManufacturerId());
        soapModel.setCreatorId(model.getCreatorId());
        soapModel.setCreatorName(model.getCreatorName());
        soapModel.setCreateDate(model.getCreateDate());
        soapModel.setModifierId(model.getModifierId());
        soapModel.setModifierName(model.getModifierName());
        soapModel.setModifiedDate(model.getModifiedDate());
        soapModel.setName(model.getName());
        soapModel.setIconId(model.getIconId());

        return soapModel;
    }

    public static VehicleManufacturerSoap[] toSoapModels(
        VehicleManufacturer[] models) {
        VehicleManufacturerSoap[] soapModels = new VehicleManufacturerSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static VehicleManufacturerSoap[][] toSoapModels(
        VehicleManufacturer[][] models) {
        VehicleManufacturerSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new VehicleManufacturerSoap[models.length][models[0].length];
        } else {
            soapModels = new VehicleManufacturerSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static VehicleManufacturerSoap[] toSoapModels(
        List<VehicleManufacturer> models) {
        List<VehicleManufacturerSoap> soapModels = new ArrayList<VehicleManufacturerSoap>(models.size());

        for (VehicleManufacturer model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new VehicleManufacturerSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _vehicleManufacturerId;
    }

    public void setPrimaryKey(long pk) {
        setVehicleManufacturerId(pk);
    }

    public long getVehicleManufacturerId() {
        return _vehicleManufacturerId;
    }

    public void setVehicleManufacturerId(long vehicleManufacturerId) {
        _vehicleManufacturerId = vehicleManufacturerId;
    }

    public long getCreatorId() {
        return _creatorId;
    }

    public void setCreatorId(long creatorId) {
        _creatorId = creatorId;
    }

    public String getCreatorName() {
        return _creatorName;
    }

    public void setCreatorName(String creatorName) {
        _creatorName = creatorName;
    }

    public Date getCreateDate() {
        return _createDate;
    }

    public void setCreateDate(Date createDate) {
        _createDate = createDate;
    }

    public long getModifierId() {
        return _modifierId;
    }

    public void setModifierId(long modifierId) {
        _modifierId = modifierId;
    }

    public String getModifierName() {
        return _modifierName;
    }

    public void setModifierName(String modifierName) {
        _modifierName = modifierName;
    }

    public Date getModifiedDate() {
        return _modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public long getIconId() {
        return _iconId;
    }

    public void setIconId(long iconId) {
        _iconId = iconId;
    }
}
