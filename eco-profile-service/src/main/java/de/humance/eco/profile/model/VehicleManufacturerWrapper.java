package de.humance.eco.profile.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link VehicleManufacturer}.
 * </p>
 *
 * @author Humance
 * @see VehicleManufacturer
 * @generated
 */
public class VehicleManufacturerWrapper implements VehicleManufacturer,
    ModelWrapper<VehicleManufacturer> {
    private VehicleManufacturer _vehicleManufacturer;

    public VehicleManufacturerWrapper(VehicleManufacturer vehicleManufacturer) {
        _vehicleManufacturer = vehicleManufacturer;
    }

    @Override
    public Class<?> getModelClass() {
        return VehicleManufacturer.class;
    }

    @Override
    public String getModelClassName() {
        return VehicleManufacturer.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("vehicleManufacturerId", getVehicleManufacturerId());
        attributes.put("creatorId", getCreatorId());
        attributes.put("creatorName", getCreatorName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifierId", getModifierId());
        attributes.put("modifierName", getModifierName());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("name", getName());
        attributes.put("iconId", getIconId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long vehicleManufacturerId = (Long) attributes.get(
                "vehicleManufacturerId");

        if (vehicleManufacturerId != null) {
            setVehicleManufacturerId(vehicleManufacturerId);
        }

        Long creatorId = (Long) attributes.get("creatorId");

        if (creatorId != null) {
            setCreatorId(creatorId);
        }

        String creatorName = (String) attributes.get("creatorName");

        if (creatorName != null) {
            setCreatorName(creatorName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Long modifierId = (Long) attributes.get("modifierId");

        if (modifierId != null) {
            setModifierId(modifierId);
        }

        String modifierName = (String) attributes.get("modifierName");

        if (modifierName != null) {
            setModifierName(modifierName);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        Long iconId = (Long) attributes.get("iconId");

        if (iconId != null) {
            setIconId(iconId);
        }
    }

    /**
    * Returns the primary key of this vehicle manufacturer.
    *
    * @return the primary key of this vehicle manufacturer
    */
    @Override
    public long getPrimaryKey() {
        return _vehicleManufacturer.getPrimaryKey();
    }

    /**
    * Sets the primary key of this vehicle manufacturer.
    *
    * @param primaryKey the primary key of this vehicle manufacturer
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _vehicleManufacturer.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the vehicle manufacturer ID of this vehicle manufacturer.
    *
    * @return the vehicle manufacturer ID of this vehicle manufacturer
    */
    @Override
    public long getVehicleManufacturerId() {
        return _vehicleManufacturer.getVehicleManufacturerId();
    }

    /**
    * Sets the vehicle manufacturer ID of this vehicle manufacturer.
    *
    * @param vehicleManufacturerId the vehicle manufacturer ID of this vehicle manufacturer
    */
    @Override
    public void setVehicleManufacturerId(long vehicleManufacturerId) {
        _vehicleManufacturer.setVehicleManufacturerId(vehicleManufacturerId);
    }

    /**
    * Returns the creator ID of this vehicle manufacturer.
    *
    * @return the creator ID of this vehicle manufacturer
    */
    @Override
    public long getCreatorId() {
        return _vehicleManufacturer.getCreatorId();
    }

    /**
    * Sets the creator ID of this vehicle manufacturer.
    *
    * @param creatorId the creator ID of this vehicle manufacturer
    */
    @Override
    public void setCreatorId(long creatorId) {
        _vehicleManufacturer.setCreatorId(creatorId);
    }

    /**
    * Returns the creator name of this vehicle manufacturer.
    *
    * @return the creator name of this vehicle manufacturer
    */
    @Override
    public java.lang.String getCreatorName() {
        return _vehicleManufacturer.getCreatorName();
    }

    /**
    * Sets the creator name of this vehicle manufacturer.
    *
    * @param creatorName the creator name of this vehicle manufacturer
    */
    @Override
    public void setCreatorName(java.lang.String creatorName) {
        _vehicleManufacturer.setCreatorName(creatorName);
    }

    /**
    * Returns the create date of this vehicle manufacturer.
    *
    * @return the create date of this vehicle manufacturer
    */
    @Override
    public java.util.Date getCreateDate() {
        return _vehicleManufacturer.getCreateDate();
    }

    /**
    * Sets the create date of this vehicle manufacturer.
    *
    * @param createDate the create date of this vehicle manufacturer
    */
    @Override
    public void setCreateDate(java.util.Date createDate) {
        _vehicleManufacturer.setCreateDate(createDate);
    }

    /**
    * Returns the modifier ID of this vehicle manufacturer.
    *
    * @return the modifier ID of this vehicle manufacturer
    */
    @Override
    public long getModifierId() {
        return _vehicleManufacturer.getModifierId();
    }

    /**
    * Sets the modifier ID of this vehicle manufacturer.
    *
    * @param modifierId the modifier ID of this vehicle manufacturer
    */
    @Override
    public void setModifierId(long modifierId) {
        _vehicleManufacturer.setModifierId(modifierId);
    }

    /**
    * Returns the modifier name of this vehicle manufacturer.
    *
    * @return the modifier name of this vehicle manufacturer
    */
    @Override
    public java.lang.String getModifierName() {
        return _vehicleManufacturer.getModifierName();
    }

    /**
    * Sets the modifier name of this vehicle manufacturer.
    *
    * @param modifierName the modifier name of this vehicle manufacturer
    */
    @Override
    public void setModifierName(java.lang.String modifierName) {
        _vehicleManufacturer.setModifierName(modifierName);
    }

    /**
    * Returns the modified date of this vehicle manufacturer.
    *
    * @return the modified date of this vehicle manufacturer
    */
    @Override
    public java.util.Date getModifiedDate() {
        return _vehicleManufacturer.getModifiedDate();
    }

    /**
    * Sets the modified date of this vehicle manufacturer.
    *
    * @param modifiedDate the modified date of this vehicle manufacturer
    */
    @Override
    public void setModifiedDate(java.util.Date modifiedDate) {
        _vehicleManufacturer.setModifiedDate(modifiedDate);
    }

    /**
    * Returns the name of this vehicle manufacturer.
    *
    * @return the name of this vehicle manufacturer
    */
    @Override
    public java.lang.String getName() {
        return _vehicleManufacturer.getName();
    }

    /**
    * Sets the name of this vehicle manufacturer.
    *
    * @param name the name of this vehicle manufacturer
    */
    @Override
    public void setName(java.lang.String name) {
        _vehicleManufacturer.setName(name);
    }

    /**
    * Returns the icon ID of this vehicle manufacturer.
    *
    * @return the icon ID of this vehicle manufacturer
    */
    @Override
    public long getIconId() {
        return _vehicleManufacturer.getIconId();
    }

    /**
    * Sets the icon ID of this vehicle manufacturer.
    *
    * @param iconId the icon ID of this vehicle manufacturer
    */
    @Override
    public void setIconId(long iconId) {
        _vehicleManufacturer.setIconId(iconId);
    }

    @Override
    public boolean isNew() {
        return _vehicleManufacturer.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _vehicleManufacturer.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _vehicleManufacturer.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _vehicleManufacturer.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _vehicleManufacturer.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _vehicleManufacturer.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _vehicleManufacturer.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _vehicleManufacturer.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _vehicleManufacturer.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _vehicleManufacturer.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _vehicleManufacturer.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new VehicleManufacturerWrapper((VehicleManufacturer) _vehicleManufacturer.clone());
    }

    @Override
    public int compareTo(VehicleManufacturer vehicleManufacturer) {
        return _vehicleManufacturer.compareTo(vehicleManufacturer);
    }

    @Override
    public int hashCode() {
        return _vehicleManufacturer.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<VehicleManufacturer> toCacheModel() {
        return _vehicleManufacturer.toCacheModel();
    }

    @Override
    public VehicleManufacturer toEscapedModel() {
        return new VehicleManufacturerWrapper(_vehicleManufacturer.toEscapedModel());
    }

    @Override
    public VehicleManufacturer toUnescapedModel() {
        return new VehicleManufacturerWrapper(_vehicleManufacturer.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _vehicleManufacturer.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _vehicleManufacturer.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _vehicleManufacturer.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VehicleManufacturerWrapper)) {
            return false;
        }

        VehicleManufacturerWrapper vehicleManufacturerWrapper = (VehicleManufacturerWrapper) obj;

        if (Validator.equals(_vehicleManufacturer,
                    vehicleManufacturerWrapper._vehicleManufacturer)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public VehicleManufacturer getWrappedVehicleManufacturer() {
        return _vehicleManufacturer;
    }

    @Override
    public VehicleManufacturer getWrappedModel() {
        return _vehicleManufacturer;
    }

    @Override
    public void resetOriginalValues() {
        _vehicleManufacturer.resetOriginalValues();
    }
}
