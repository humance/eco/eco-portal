package de.humance.eco.profile.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Humance
 * @generated
 */
public class VehicleSoap implements Serializable {
    private long _vehicleId;
    private long _creatorId;
    private String _creatorName;
    private Date _createDate;
    private long _modifierId;
    private String _modifierName;
    private Date _modifiedDate;
    private long _driverId;
    private long _typeId;
    private long _manufacturerId;
    private long _organizationId;
    private String _modelName;
    private long _dimensionHeight;
    private long _dimensionWidth;
    private long _dimensionDepth;
    private String _licensePlate;

    public VehicleSoap() {
    }

    public static VehicleSoap toSoapModel(Vehicle model) {
        VehicleSoap soapModel = new VehicleSoap();

        soapModel.setVehicleId(model.getVehicleId());
        soapModel.setCreatorId(model.getCreatorId());
        soapModel.setCreatorName(model.getCreatorName());
        soapModel.setCreateDate(model.getCreateDate());
        soapModel.setModifierId(model.getModifierId());
        soapModel.setModifierName(model.getModifierName());
        soapModel.setModifiedDate(model.getModifiedDate());
        soapModel.setDriverId(model.getDriverId());
        soapModel.setTypeId(model.getTypeId());
        soapModel.setManufacturerId(model.getManufacturerId());
        soapModel.setOrganizationId(model.getOrganizationId());
        soapModel.setModelName(model.getModelName());
        soapModel.setDimensionHeight(model.getDimensionHeight());
        soapModel.setDimensionWidth(model.getDimensionWidth());
        soapModel.setDimensionDepth(model.getDimensionDepth());
        soapModel.setLicensePlate(model.getLicensePlate());

        return soapModel;
    }

    public static VehicleSoap[] toSoapModels(Vehicle[] models) {
        VehicleSoap[] soapModels = new VehicleSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static VehicleSoap[][] toSoapModels(Vehicle[][] models) {
        VehicleSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new VehicleSoap[models.length][models[0].length];
        } else {
            soapModels = new VehicleSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static VehicleSoap[] toSoapModels(List<Vehicle> models) {
        List<VehicleSoap> soapModels = new ArrayList<VehicleSoap>(models.size());

        for (Vehicle model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new VehicleSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _vehicleId;
    }

    public void setPrimaryKey(long pk) {
        setVehicleId(pk);
    }

    public long getVehicleId() {
        return _vehicleId;
    }

    public void setVehicleId(long vehicleId) {
        _vehicleId = vehicleId;
    }

    public long getCreatorId() {
        return _creatorId;
    }

    public void setCreatorId(long creatorId) {
        _creatorId = creatorId;
    }

    public String getCreatorName() {
        return _creatorName;
    }

    public void setCreatorName(String creatorName) {
        _creatorName = creatorName;
    }

    public Date getCreateDate() {
        return _createDate;
    }

    public void setCreateDate(Date createDate) {
        _createDate = createDate;
    }

    public long getModifierId() {
        return _modifierId;
    }

    public void setModifierId(long modifierId) {
        _modifierId = modifierId;
    }

    public String getModifierName() {
        return _modifierName;
    }

    public void setModifierName(String modifierName) {
        _modifierName = modifierName;
    }

    public Date getModifiedDate() {
        return _modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;
    }

    public long getDriverId() {
        return _driverId;
    }

    public void setDriverId(long driverId) {
        _driverId = driverId;
    }

    public long getTypeId() {
        return _typeId;
    }

    public void setTypeId(long typeId) {
        _typeId = typeId;
    }

    public long getManufacturerId() {
        return _manufacturerId;
    }

    public void setManufacturerId(long manufacturerId) {
        _manufacturerId = manufacturerId;
    }

    public long getOrganizationId() {
        return _organizationId;
    }

    public void setOrganizationId(long organizationId) {
        _organizationId = organizationId;
    }

    public String getModelName() {
        return _modelName;
    }

    public void setModelName(String modelName) {
        _modelName = modelName;
    }

    public long getDimensionHeight() {
        return _dimensionHeight;
    }

    public void setDimensionHeight(long dimensionHeight) {
        _dimensionHeight = dimensionHeight;
    }

    public long getDimensionWidth() {
        return _dimensionWidth;
    }

    public void setDimensionWidth(long dimensionWidth) {
        _dimensionWidth = dimensionWidth;
    }

    public long getDimensionDepth() {
        return _dimensionDepth;
    }

    public void setDimensionDepth(long dimensionDepth) {
        _dimensionDepth = dimensionDepth;
    }

    public String getLicensePlate() {
        return _licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        _licensePlate = licensePlate;
    }
}
