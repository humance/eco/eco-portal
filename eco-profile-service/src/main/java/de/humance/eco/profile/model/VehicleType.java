package de.humance.eco.profile.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the VehicleType service. Represents a row in the &quot;Profile_VehicleType&quot; database table, with each column mapped to a property of this class.
 *
 * @author Humance
 * @see VehicleTypeModel
 * @see de.humance.eco.profile.model.impl.VehicleTypeImpl
 * @see de.humance.eco.profile.model.impl.VehicleTypeModelImpl
 * @generated
 */
public interface VehicleType extends VehicleTypeModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link de.humance.eco.profile.model.impl.VehicleTypeImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
