package de.humance.eco.profile.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the VehicleTypeResource service. Represents a row in the &quot;Profile_VehicleTypeResource&quot; database table, with each column mapped to a property of this class.
 *
 * @author Humance
 * @see VehicleTypeResourceModel
 * @see de.humance.eco.profile.model.impl.VehicleTypeResourceImpl
 * @see de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl
 * @generated
 */
public interface VehicleTypeResource extends VehicleTypeResourceModel,
    PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link de.humance.eco.profile.model.impl.VehicleTypeResourceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
