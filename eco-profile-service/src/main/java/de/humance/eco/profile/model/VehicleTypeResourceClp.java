package de.humance.eco.profile.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import de.humance.eco.profile.service.ClpSerializer;
import de.humance.eco.profile.service.VehicleTypeResourceLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class VehicleTypeResourceClp extends BaseModelImpl<VehicleTypeResource>
    implements VehicleTypeResource {
    private long _vehicleTypeResourceId;
    private long _vehicleTypeId;
    private String _country;
    private String _language;
    private String _name;
    private long _iconId;
    private BaseModel<?> _vehicleTypeResourceRemoteModel;

    public VehicleTypeResourceClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return VehicleTypeResource.class;
    }

    @Override
    public String getModelClassName() {
        return VehicleTypeResource.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _vehicleTypeResourceId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setVehicleTypeResourceId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _vehicleTypeResourceId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("vehicleTypeResourceId", getVehicleTypeResourceId());
        attributes.put("vehicleTypeId", getVehicleTypeId());
        attributes.put("country", getCountry());
        attributes.put("language", getLanguage());
        attributes.put("name", getName());
        attributes.put("iconId", getIconId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long vehicleTypeResourceId = (Long) attributes.get(
                "vehicleTypeResourceId");

        if (vehicleTypeResourceId != null) {
            setVehicleTypeResourceId(vehicleTypeResourceId);
        }

        Long vehicleTypeId = (Long) attributes.get("vehicleTypeId");

        if (vehicleTypeId != null) {
            setVehicleTypeId(vehicleTypeId);
        }

        String country = (String) attributes.get("country");

        if (country != null) {
            setCountry(country);
        }

        String language = (String) attributes.get("language");

        if (language != null) {
            setLanguage(language);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        Long iconId = (Long) attributes.get("iconId");

        if (iconId != null) {
            setIconId(iconId);
        }
    }

    @Override
    public long getVehicleTypeResourceId() {
        return _vehicleTypeResourceId;
    }

    @Override
    public void setVehicleTypeResourceId(long vehicleTypeResourceId) {
        _vehicleTypeResourceId = vehicleTypeResourceId;

        if (_vehicleTypeResourceRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleTypeResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setVehicleTypeResourceId",
                        long.class);

                method.invoke(_vehicleTypeResourceRemoteModel,
                    vehicleTypeResourceId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getVehicleTypeId() {
        return _vehicleTypeId;
    }

    @Override
    public void setVehicleTypeId(long vehicleTypeId) {
        _vehicleTypeId = vehicleTypeId;

        if (_vehicleTypeResourceRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleTypeResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setVehicleTypeId", long.class);

                method.invoke(_vehicleTypeResourceRemoteModel, vehicleTypeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCountry() {
        return _country;
    }

    @Override
    public void setCountry(String country) {
        _country = country;

        if (_vehicleTypeResourceRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleTypeResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setCountry", String.class);

                method.invoke(_vehicleTypeResourceRemoteModel, country);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getLanguage() {
        return _language;
    }

    @Override
    public void setLanguage(String language) {
        _language = language;

        if (_vehicleTypeResourceRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleTypeResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setLanguage", String.class);

                method.invoke(_vehicleTypeResourceRemoteModel, language);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getName() {
        return _name;
    }

    @Override
    public void setName(String name) {
        _name = name;

        if (_vehicleTypeResourceRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleTypeResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setName", String.class);

                method.invoke(_vehicleTypeResourceRemoteModel, name);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getIconId() {
        return _iconId;
    }

    @Override
    public void setIconId(long iconId) {
        _iconId = iconId;

        if (_vehicleTypeResourceRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleTypeResourceRemoteModel.getClass();

                Method method = clazz.getMethod("setIconId", long.class);

                method.invoke(_vehicleTypeResourceRemoteModel, iconId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getVehicleTypeResourceRemoteModel() {
        return _vehicleTypeResourceRemoteModel;
    }

    public void setVehicleTypeResourceRemoteModel(
        BaseModel<?> vehicleTypeResourceRemoteModel) {
        _vehicleTypeResourceRemoteModel = vehicleTypeResourceRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _vehicleTypeResourceRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_vehicleTypeResourceRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            VehicleTypeResourceLocalServiceUtil.addVehicleTypeResource(this);
        } else {
            VehicleTypeResourceLocalServiceUtil.updateVehicleTypeResource(this);
        }
    }

    @Override
    public VehicleTypeResource toEscapedModel() {
        return (VehicleTypeResource) ProxyUtil.newProxyInstance(VehicleTypeResource.class.getClassLoader(),
            new Class[] { VehicleTypeResource.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        VehicleTypeResourceClp clone = new VehicleTypeResourceClp();

        clone.setVehicleTypeResourceId(getVehicleTypeResourceId());
        clone.setVehicleTypeId(getVehicleTypeId());
        clone.setCountry(getCountry());
        clone.setLanguage(getLanguage());
        clone.setName(getName());
        clone.setIconId(getIconId());

        return clone;
    }

    @Override
    public int compareTo(VehicleTypeResource vehicleTypeResource) {
        int value = 0;

        if (getVehicleTypeId() < vehicleTypeResource.getVehicleTypeId()) {
            value = -1;
        } else if (getVehicleTypeId() > vehicleTypeResource.getVehicleTypeId()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        value = getCountry()
                    .compareToIgnoreCase(vehicleTypeResource.getCountry());

        if (value != 0) {
            return value;
        }

        value = getLanguage()
                    .compareToIgnoreCase(vehicleTypeResource.getLanguage());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VehicleTypeResourceClp)) {
            return false;
        }

        VehicleTypeResourceClp vehicleTypeResource = (VehicleTypeResourceClp) obj;

        long primaryKey = vehicleTypeResource.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{vehicleTypeResourceId=");
        sb.append(getVehicleTypeResourceId());
        sb.append(", vehicleTypeId=");
        sb.append(getVehicleTypeId());
        sb.append(", country=");
        sb.append(getCountry());
        sb.append(", language=");
        sb.append(getLanguage());
        sb.append(", name=");
        sb.append(getName());
        sb.append(", iconId=");
        sb.append(getIconId());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(22);

        sb.append("<model><model-name>");
        sb.append("de.humance.eco.profile.model.VehicleTypeResource");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>vehicleTypeResourceId</column-name><column-value><![CDATA[");
        sb.append(getVehicleTypeResourceId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>vehicleTypeId</column-name><column-value><![CDATA[");
        sb.append(getVehicleTypeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>country</column-name><column-value><![CDATA[");
        sb.append(getCountry());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>language</column-name><column-value><![CDATA[");
        sb.append(getLanguage());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>name</column-name><column-value><![CDATA[");
        sb.append(getName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>iconId</column-name><column-value><![CDATA[");
        sb.append(getIconId());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
