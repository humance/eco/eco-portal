package de.humance.eco.profile.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Humance
 * @generated
 */
public class VehicleTypeResourceSoap implements Serializable {
    private long _vehicleTypeResourceId;
    private long _vehicleTypeId;
    private String _country;
    private String _language;
    private String _name;
    private long _iconId;

    public VehicleTypeResourceSoap() {
    }

    public static VehicleTypeResourceSoap toSoapModel(VehicleTypeResource model) {
        VehicleTypeResourceSoap soapModel = new VehicleTypeResourceSoap();

        soapModel.setVehicleTypeResourceId(model.getVehicleTypeResourceId());
        soapModel.setVehicleTypeId(model.getVehicleTypeId());
        soapModel.setCountry(model.getCountry());
        soapModel.setLanguage(model.getLanguage());
        soapModel.setName(model.getName());
        soapModel.setIconId(model.getIconId());

        return soapModel;
    }

    public static VehicleTypeResourceSoap[] toSoapModels(
        VehicleTypeResource[] models) {
        VehicleTypeResourceSoap[] soapModels = new VehicleTypeResourceSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static VehicleTypeResourceSoap[][] toSoapModels(
        VehicleTypeResource[][] models) {
        VehicleTypeResourceSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new VehicleTypeResourceSoap[models.length][models[0].length];
        } else {
            soapModels = new VehicleTypeResourceSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static VehicleTypeResourceSoap[] toSoapModels(
        List<VehicleTypeResource> models) {
        List<VehicleTypeResourceSoap> soapModels = new ArrayList<VehicleTypeResourceSoap>(models.size());

        for (VehicleTypeResource model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new VehicleTypeResourceSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _vehicleTypeResourceId;
    }

    public void setPrimaryKey(long pk) {
        setVehicleTypeResourceId(pk);
    }

    public long getVehicleTypeResourceId() {
        return _vehicleTypeResourceId;
    }

    public void setVehicleTypeResourceId(long vehicleTypeResourceId) {
        _vehicleTypeResourceId = vehicleTypeResourceId;
    }

    public long getVehicleTypeId() {
        return _vehicleTypeId;
    }

    public void setVehicleTypeId(long vehicleTypeId) {
        _vehicleTypeId = vehicleTypeId;
    }

    public String getCountry() {
        return _country;
    }

    public void setCountry(String country) {
        _country = country;
    }

    public String getLanguage() {
        return _language;
    }

    public void setLanguage(String language) {
        _language = language;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public long getIconId() {
        return _iconId;
    }

    public void setIconId(long iconId) {
        _iconId = iconId;
    }
}
