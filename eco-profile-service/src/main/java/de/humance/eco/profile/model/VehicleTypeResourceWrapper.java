package de.humance.eco.profile.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link VehicleTypeResource}.
 * </p>
 *
 * @author Humance
 * @see VehicleTypeResource
 * @generated
 */
public class VehicleTypeResourceWrapper implements VehicleTypeResource,
    ModelWrapper<VehicleTypeResource> {
    private VehicleTypeResource _vehicleTypeResource;

    public VehicleTypeResourceWrapper(VehicleTypeResource vehicleTypeResource) {
        _vehicleTypeResource = vehicleTypeResource;
    }

    @Override
    public Class<?> getModelClass() {
        return VehicleTypeResource.class;
    }

    @Override
    public String getModelClassName() {
        return VehicleTypeResource.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("vehicleTypeResourceId", getVehicleTypeResourceId());
        attributes.put("vehicleTypeId", getVehicleTypeId());
        attributes.put("country", getCountry());
        attributes.put("language", getLanguage());
        attributes.put("name", getName());
        attributes.put("iconId", getIconId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long vehicleTypeResourceId = (Long) attributes.get(
                "vehicleTypeResourceId");

        if (vehicleTypeResourceId != null) {
            setVehicleTypeResourceId(vehicleTypeResourceId);
        }

        Long vehicleTypeId = (Long) attributes.get("vehicleTypeId");

        if (vehicleTypeId != null) {
            setVehicleTypeId(vehicleTypeId);
        }

        String country = (String) attributes.get("country");

        if (country != null) {
            setCountry(country);
        }

        String language = (String) attributes.get("language");

        if (language != null) {
            setLanguage(language);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        Long iconId = (Long) attributes.get("iconId");

        if (iconId != null) {
            setIconId(iconId);
        }
    }

    /**
    * Returns the primary key of this vehicle type resource.
    *
    * @return the primary key of this vehicle type resource
    */
    @Override
    public long getPrimaryKey() {
        return _vehicleTypeResource.getPrimaryKey();
    }

    /**
    * Sets the primary key of this vehicle type resource.
    *
    * @param primaryKey the primary key of this vehicle type resource
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _vehicleTypeResource.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the vehicle type resource ID of this vehicle type resource.
    *
    * @return the vehicle type resource ID of this vehicle type resource
    */
    @Override
    public long getVehicleTypeResourceId() {
        return _vehicleTypeResource.getVehicleTypeResourceId();
    }

    /**
    * Sets the vehicle type resource ID of this vehicle type resource.
    *
    * @param vehicleTypeResourceId the vehicle type resource ID of this vehicle type resource
    */
    @Override
    public void setVehicleTypeResourceId(long vehicleTypeResourceId) {
        _vehicleTypeResource.setVehicleTypeResourceId(vehicleTypeResourceId);
    }

    /**
    * Returns the vehicle type ID of this vehicle type resource.
    *
    * @return the vehicle type ID of this vehicle type resource
    */
    @Override
    public long getVehicleTypeId() {
        return _vehicleTypeResource.getVehicleTypeId();
    }

    /**
    * Sets the vehicle type ID of this vehicle type resource.
    *
    * @param vehicleTypeId the vehicle type ID of this vehicle type resource
    */
    @Override
    public void setVehicleTypeId(long vehicleTypeId) {
        _vehicleTypeResource.setVehicleTypeId(vehicleTypeId);
    }

    /**
    * Returns the country of this vehicle type resource.
    *
    * @return the country of this vehicle type resource
    */
    @Override
    public java.lang.String getCountry() {
        return _vehicleTypeResource.getCountry();
    }

    /**
    * Sets the country of this vehicle type resource.
    *
    * @param country the country of this vehicle type resource
    */
    @Override
    public void setCountry(java.lang.String country) {
        _vehicleTypeResource.setCountry(country);
    }

    /**
    * Returns the language of this vehicle type resource.
    *
    * @return the language of this vehicle type resource
    */
    @Override
    public java.lang.String getLanguage() {
        return _vehicleTypeResource.getLanguage();
    }

    /**
    * Sets the language of this vehicle type resource.
    *
    * @param language the language of this vehicle type resource
    */
    @Override
    public void setLanguage(java.lang.String language) {
        _vehicleTypeResource.setLanguage(language);
    }

    /**
    * Returns the name of this vehicle type resource.
    *
    * @return the name of this vehicle type resource
    */
    @Override
    public java.lang.String getName() {
        return _vehicleTypeResource.getName();
    }

    /**
    * Sets the name of this vehicle type resource.
    *
    * @param name the name of this vehicle type resource
    */
    @Override
    public void setName(java.lang.String name) {
        _vehicleTypeResource.setName(name);
    }

    /**
    * Returns the icon ID of this vehicle type resource.
    *
    * @return the icon ID of this vehicle type resource
    */
    @Override
    public long getIconId() {
        return _vehicleTypeResource.getIconId();
    }

    /**
    * Sets the icon ID of this vehicle type resource.
    *
    * @param iconId the icon ID of this vehicle type resource
    */
    @Override
    public void setIconId(long iconId) {
        _vehicleTypeResource.setIconId(iconId);
    }

    @Override
    public boolean isNew() {
        return _vehicleTypeResource.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _vehicleTypeResource.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _vehicleTypeResource.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _vehicleTypeResource.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _vehicleTypeResource.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _vehicleTypeResource.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _vehicleTypeResource.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _vehicleTypeResource.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _vehicleTypeResource.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _vehicleTypeResource.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _vehicleTypeResource.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new VehicleTypeResourceWrapper((VehicleTypeResource) _vehicleTypeResource.clone());
    }

    @Override
    public int compareTo(VehicleTypeResource vehicleTypeResource) {
        return _vehicleTypeResource.compareTo(vehicleTypeResource);
    }

    @Override
    public int hashCode() {
        return _vehicleTypeResource.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<VehicleTypeResource> toCacheModel() {
        return _vehicleTypeResource.toCacheModel();
    }

    @Override
    public VehicleTypeResource toEscapedModel() {
        return new VehicleTypeResourceWrapper(_vehicleTypeResource.toEscapedModel());
    }

    @Override
    public VehicleTypeResource toUnescapedModel() {
        return new VehicleTypeResourceWrapper(_vehicleTypeResource.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _vehicleTypeResource.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _vehicleTypeResource.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _vehicleTypeResource.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VehicleTypeResourceWrapper)) {
            return false;
        }

        VehicleTypeResourceWrapper vehicleTypeResourceWrapper = (VehicleTypeResourceWrapper) obj;

        if (Validator.equals(_vehicleTypeResource,
                    vehicleTypeResourceWrapper._vehicleTypeResource)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public VehicleTypeResource getWrappedVehicleTypeResource() {
        return _vehicleTypeResource;
    }

    @Override
    public VehicleTypeResource getWrappedModel() {
        return _vehicleTypeResource;
    }

    @Override
    public void resetOriginalValues() {
        _vehicleTypeResource.resetOriginalValues();
    }
}
