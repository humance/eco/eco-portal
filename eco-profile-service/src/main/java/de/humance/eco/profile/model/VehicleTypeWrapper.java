package de.humance.eco.profile.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link VehicleType}.
 * </p>
 *
 * @author Humance
 * @see VehicleType
 * @generated
 */
public class VehicleTypeWrapper implements VehicleType,
    ModelWrapper<VehicleType> {
    private VehicleType _vehicleType;

    public VehicleTypeWrapper(VehicleType vehicleType) {
        _vehicleType = vehicleType;
    }

    @Override
    public Class<?> getModelClass() {
        return VehicleType.class;
    }

    @Override
    public String getModelClassName() {
        return VehicleType.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("vehicleTypeId", getVehicleTypeId());
        attributes.put("creatorId", getCreatorId());
        attributes.put("creatorName", getCreatorName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifierId", getModifierId());
        attributes.put("modifierName", getModifierName());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("name", getName());
        attributes.put("iconId", getIconId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long vehicleTypeId = (Long) attributes.get("vehicleTypeId");

        if (vehicleTypeId != null) {
            setVehicleTypeId(vehicleTypeId);
        }

        Long creatorId = (Long) attributes.get("creatorId");

        if (creatorId != null) {
            setCreatorId(creatorId);
        }

        String creatorName = (String) attributes.get("creatorName");

        if (creatorName != null) {
            setCreatorName(creatorName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Long modifierId = (Long) attributes.get("modifierId");

        if (modifierId != null) {
            setModifierId(modifierId);
        }

        String modifierName = (String) attributes.get("modifierName");

        if (modifierName != null) {
            setModifierName(modifierName);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        Long iconId = (Long) attributes.get("iconId");

        if (iconId != null) {
            setIconId(iconId);
        }
    }

    /**
    * Returns the primary key of this vehicle type.
    *
    * @return the primary key of this vehicle type
    */
    @Override
    public long getPrimaryKey() {
        return _vehicleType.getPrimaryKey();
    }

    /**
    * Sets the primary key of this vehicle type.
    *
    * @param primaryKey the primary key of this vehicle type
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _vehicleType.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the vehicle type ID of this vehicle type.
    *
    * @return the vehicle type ID of this vehicle type
    */
    @Override
    public long getVehicleTypeId() {
        return _vehicleType.getVehicleTypeId();
    }

    /**
    * Sets the vehicle type ID of this vehicle type.
    *
    * @param vehicleTypeId the vehicle type ID of this vehicle type
    */
    @Override
    public void setVehicleTypeId(long vehicleTypeId) {
        _vehicleType.setVehicleTypeId(vehicleTypeId);
    }

    /**
    * Returns the creator ID of this vehicle type.
    *
    * @return the creator ID of this vehicle type
    */
    @Override
    public long getCreatorId() {
        return _vehicleType.getCreatorId();
    }

    /**
    * Sets the creator ID of this vehicle type.
    *
    * @param creatorId the creator ID of this vehicle type
    */
    @Override
    public void setCreatorId(long creatorId) {
        _vehicleType.setCreatorId(creatorId);
    }

    /**
    * Returns the creator name of this vehicle type.
    *
    * @return the creator name of this vehicle type
    */
    @Override
    public java.lang.String getCreatorName() {
        return _vehicleType.getCreatorName();
    }

    /**
    * Sets the creator name of this vehicle type.
    *
    * @param creatorName the creator name of this vehicle type
    */
    @Override
    public void setCreatorName(java.lang.String creatorName) {
        _vehicleType.setCreatorName(creatorName);
    }

    /**
    * Returns the create date of this vehicle type.
    *
    * @return the create date of this vehicle type
    */
    @Override
    public java.util.Date getCreateDate() {
        return _vehicleType.getCreateDate();
    }

    /**
    * Sets the create date of this vehicle type.
    *
    * @param createDate the create date of this vehicle type
    */
    @Override
    public void setCreateDate(java.util.Date createDate) {
        _vehicleType.setCreateDate(createDate);
    }

    /**
    * Returns the modifier ID of this vehicle type.
    *
    * @return the modifier ID of this vehicle type
    */
    @Override
    public long getModifierId() {
        return _vehicleType.getModifierId();
    }

    /**
    * Sets the modifier ID of this vehicle type.
    *
    * @param modifierId the modifier ID of this vehicle type
    */
    @Override
    public void setModifierId(long modifierId) {
        _vehicleType.setModifierId(modifierId);
    }

    /**
    * Returns the modifier name of this vehicle type.
    *
    * @return the modifier name of this vehicle type
    */
    @Override
    public java.lang.String getModifierName() {
        return _vehicleType.getModifierName();
    }

    /**
    * Sets the modifier name of this vehicle type.
    *
    * @param modifierName the modifier name of this vehicle type
    */
    @Override
    public void setModifierName(java.lang.String modifierName) {
        _vehicleType.setModifierName(modifierName);
    }

    /**
    * Returns the modified date of this vehicle type.
    *
    * @return the modified date of this vehicle type
    */
    @Override
    public java.util.Date getModifiedDate() {
        return _vehicleType.getModifiedDate();
    }

    /**
    * Sets the modified date of this vehicle type.
    *
    * @param modifiedDate the modified date of this vehicle type
    */
    @Override
    public void setModifiedDate(java.util.Date modifiedDate) {
        _vehicleType.setModifiedDate(modifiedDate);
    }

    /**
    * Returns the name of this vehicle type.
    *
    * @return the name of this vehicle type
    */
    @Override
    public java.lang.String getName() {
        return _vehicleType.getName();
    }

    /**
    * Sets the name of this vehicle type.
    *
    * @param name the name of this vehicle type
    */
    @Override
    public void setName(java.lang.String name) {
        _vehicleType.setName(name);
    }

    /**
    * Returns the icon ID of this vehicle type.
    *
    * @return the icon ID of this vehicle type
    */
    @Override
    public long getIconId() {
        return _vehicleType.getIconId();
    }

    /**
    * Sets the icon ID of this vehicle type.
    *
    * @param iconId the icon ID of this vehicle type
    */
    @Override
    public void setIconId(long iconId) {
        _vehicleType.setIconId(iconId);
    }

    @Override
    public boolean isNew() {
        return _vehicleType.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _vehicleType.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _vehicleType.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _vehicleType.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _vehicleType.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _vehicleType.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _vehicleType.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _vehicleType.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _vehicleType.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _vehicleType.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _vehicleType.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new VehicleTypeWrapper((VehicleType) _vehicleType.clone());
    }

    @Override
    public int compareTo(VehicleType vehicleType) {
        return _vehicleType.compareTo(vehicleType);
    }

    @Override
    public int hashCode() {
        return _vehicleType.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<VehicleType> toCacheModel() {
        return _vehicleType.toCacheModel();
    }

    @Override
    public VehicleType toEscapedModel() {
        return new VehicleTypeWrapper(_vehicleType.toEscapedModel());
    }

    @Override
    public VehicleType toUnescapedModel() {
        return new VehicleTypeWrapper(_vehicleType.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _vehicleType.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _vehicleType.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _vehicleType.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VehicleTypeWrapper)) {
            return false;
        }

        VehicleTypeWrapper vehicleTypeWrapper = (VehicleTypeWrapper) obj;

        if (Validator.equals(_vehicleType, vehicleTypeWrapper._vehicleType)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public VehicleType getWrappedVehicleType() {
        return _vehicleType;
    }

    @Override
    public VehicleType getWrappedModel() {
        return _vehicleType;
    }

    @Override
    public void resetOriginalValues() {
        _vehicleType.resetOriginalValues();
    }
}
