package de.humance.eco.profile.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the VehicleUser service. Represents a row in the &quot;Profile_VehicleUser&quot; database table, with each column mapped to a property of this class.
 *
 * @author Humance
 * @see VehicleUserModel
 * @see de.humance.eco.profile.model.impl.VehicleUserImpl
 * @see de.humance.eco.profile.model.impl.VehicleUserModelImpl
 * @generated
 */
public interface VehicleUser extends VehicleUserModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link de.humance.eco.profile.model.impl.VehicleUserImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
