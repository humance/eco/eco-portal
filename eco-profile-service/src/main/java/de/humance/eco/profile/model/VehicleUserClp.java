package de.humance.eco.profile.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import de.humance.eco.profile.service.ClpSerializer;
import de.humance.eco.profile.service.VehicleUserLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class VehicleUserClp extends BaseModelImpl<VehicleUser>
    implements VehicleUser {
    private long _vehicleUserId;
    private String _vehicleUserUuid;
    private long _userId;
    private String _userUuid;
    private long _vehicleId;
    private BaseModel<?> _vehicleUserRemoteModel;

    public VehicleUserClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return VehicleUser.class;
    }

    @Override
    public String getModelClassName() {
        return VehicleUser.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _vehicleUserId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setVehicleUserId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _vehicleUserId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("vehicleUserId", getVehicleUserId());
        attributes.put("userId", getUserId());
        attributes.put("vehicleId", getVehicleId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long vehicleUserId = (Long) attributes.get("vehicleUserId");

        if (vehicleUserId != null) {
            setVehicleUserId(vehicleUserId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        Long vehicleId = (Long) attributes.get("vehicleId");

        if (vehicleId != null) {
            setVehicleId(vehicleId);
        }
    }

    @Override
    public long getVehicleUserId() {
        return _vehicleUserId;
    }

    @Override
    public void setVehicleUserId(long vehicleUserId) {
        _vehicleUserId = vehicleUserId;

        if (_vehicleUserRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleUserRemoteModel.getClass();

                Method method = clazz.getMethod("setVehicleUserId", long.class);

                method.invoke(_vehicleUserRemoteModel, vehicleUserId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getVehicleUserUuid() throws SystemException {
        return PortalUtil.getUserValue(getVehicleUserId(), "uuid",
            _vehicleUserUuid);
    }

    @Override
    public void setVehicleUserUuid(String vehicleUserUuid) {
        _vehicleUserUuid = vehicleUserUuid;
    }

    @Override
    public long getUserId() {
        return _userId;
    }

    @Override
    public void setUserId(long userId) {
        _userId = userId;

        if (_vehicleUserRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleUserRemoteModel.getClass();

                Method method = clazz.getMethod("setUserId", long.class);

                method.invoke(_vehicleUserRemoteModel, userId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getUserUuid() throws SystemException {
        return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
    }

    @Override
    public void setUserUuid(String userUuid) {
        _userUuid = userUuid;
    }

    @Override
    public long getVehicleId() {
        return _vehicleId;
    }

    @Override
    public void setVehicleId(long vehicleId) {
        _vehicleId = vehicleId;

        if (_vehicleUserRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleUserRemoteModel.getClass();

                Method method = clazz.getMethod("setVehicleId", long.class);

                method.invoke(_vehicleUserRemoteModel, vehicleId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getVehicleUserRemoteModel() {
        return _vehicleUserRemoteModel;
    }

    public void setVehicleUserRemoteModel(BaseModel<?> vehicleUserRemoteModel) {
        _vehicleUserRemoteModel = vehicleUserRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _vehicleUserRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_vehicleUserRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            VehicleUserLocalServiceUtil.addVehicleUser(this);
        } else {
            VehicleUserLocalServiceUtil.updateVehicleUser(this);
        }
    }

    @Override
    public VehicleUser toEscapedModel() {
        return (VehicleUser) ProxyUtil.newProxyInstance(VehicleUser.class.getClassLoader(),
            new Class[] { VehicleUser.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        VehicleUserClp clone = new VehicleUserClp();

        clone.setVehicleUserId(getVehicleUserId());
        clone.setUserId(getUserId());
        clone.setVehicleId(getVehicleId());

        return clone;
    }

    @Override
    public int compareTo(VehicleUser vehicleUser) {
        int value = 0;

        if (getUserId() < vehicleUser.getUserId()) {
            value = -1;
        } else if (getUserId() > vehicleUser.getUserId()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VehicleUserClp)) {
            return false;
        }

        VehicleUserClp vehicleUser = (VehicleUserClp) obj;

        long primaryKey = vehicleUser.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{vehicleUserId=");
        sb.append(getVehicleUserId());
        sb.append(", userId=");
        sb.append(getUserId());
        sb.append(", vehicleId=");
        sb.append(getVehicleId());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(13);

        sb.append("<model><model-name>");
        sb.append("de.humance.eco.profile.model.VehicleUser");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>vehicleUserId</column-name><column-value><![CDATA[");
        sb.append(getVehicleUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userId</column-name><column-value><![CDATA[");
        sb.append(getUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>vehicleId</column-name><column-value><![CDATA[");
        sb.append(getVehicleId());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
