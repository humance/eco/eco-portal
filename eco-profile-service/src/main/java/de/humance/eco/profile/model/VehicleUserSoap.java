package de.humance.eco.profile.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Humance
 * @generated
 */
public class VehicleUserSoap implements Serializable {
    private long _vehicleUserId;
    private long _userId;
    private long _vehicleId;

    public VehicleUserSoap() {
    }

    public static VehicleUserSoap toSoapModel(VehicleUser model) {
        VehicleUserSoap soapModel = new VehicleUserSoap();

        soapModel.setVehicleUserId(model.getVehicleUserId());
        soapModel.setUserId(model.getUserId());
        soapModel.setVehicleId(model.getVehicleId());

        return soapModel;
    }

    public static VehicleUserSoap[] toSoapModels(VehicleUser[] models) {
        VehicleUserSoap[] soapModels = new VehicleUserSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static VehicleUserSoap[][] toSoapModels(VehicleUser[][] models) {
        VehicleUserSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new VehicleUserSoap[models.length][models[0].length];
        } else {
            soapModels = new VehicleUserSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static VehicleUserSoap[] toSoapModels(List<VehicleUser> models) {
        List<VehicleUserSoap> soapModels = new ArrayList<VehicleUserSoap>(models.size());

        for (VehicleUser model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new VehicleUserSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _vehicleUserId;
    }

    public void setPrimaryKey(long pk) {
        setVehicleUserId(pk);
    }

    public long getVehicleUserId() {
        return _vehicleUserId;
    }

    public void setVehicleUserId(long vehicleUserId) {
        _vehicleUserId = vehicleUserId;
    }

    public long getUserId() {
        return _userId;
    }

    public void setUserId(long userId) {
        _userId = userId;
    }

    public long getVehicleId() {
        return _vehicleId;
    }

    public void setVehicleId(long vehicleId) {
        _vehicleId = vehicleId;
    }
}
