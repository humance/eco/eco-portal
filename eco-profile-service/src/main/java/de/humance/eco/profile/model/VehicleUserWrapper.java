package de.humance.eco.profile.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link VehicleUser}.
 * </p>
 *
 * @author Humance
 * @see VehicleUser
 * @generated
 */
public class VehicleUserWrapper implements VehicleUser,
    ModelWrapper<VehicleUser> {
    private VehicleUser _vehicleUser;

    public VehicleUserWrapper(VehicleUser vehicleUser) {
        _vehicleUser = vehicleUser;
    }

    @Override
    public Class<?> getModelClass() {
        return VehicleUser.class;
    }

    @Override
    public String getModelClassName() {
        return VehicleUser.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("vehicleUserId", getVehicleUserId());
        attributes.put("userId", getUserId());
        attributes.put("vehicleId", getVehicleId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long vehicleUserId = (Long) attributes.get("vehicleUserId");

        if (vehicleUserId != null) {
            setVehicleUserId(vehicleUserId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        Long vehicleId = (Long) attributes.get("vehicleId");

        if (vehicleId != null) {
            setVehicleId(vehicleId);
        }
    }

    /**
    * Returns the primary key of this vehicle user.
    *
    * @return the primary key of this vehicle user
    */
    @Override
    public long getPrimaryKey() {
        return _vehicleUser.getPrimaryKey();
    }

    /**
    * Sets the primary key of this vehicle user.
    *
    * @param primaryKey the primary key of this vehicle user
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _vehicleUser.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the vehicle user ID of this vehicle user.
    *
    * @return the vehicle user ID of this vehicle user
    */
    @Override
    public long getVehicleUserId() {
        return _vehicleUser.getVehicleUserId();
    }

    /**
    * Sets the vehicle user ID of this vehicle user.
    *
    * @param vehicleUserId the vehicle user ID of this vehicle user
    */
    @Override
    public void setVehicleUserId(long vehicleUserId) {
        _vehicleUser.setVehicleUserId(vehicleUserId);
    }

    /**
    * Returns the vehicle user uuid of this vehicle user.
    *
    * @return the vehicle user uuid of this vehicle user
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.lang.String getVehicleUserUuid()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUser.getVehicleUserUuid();
    }

    /**
    * Sets the vehicle user uuid of this vehicle user.
    *
    * @param vehicleUserUuid the vehicle user uuid of this vehicle user
    */
    @Override
    public void setVehicleUserUuid(java.lang.String vehicleUserUuid) {
        _vehicleUser.setVehicleUserUuid(vehicleUserUuid);
    }

    /**
    * Returns the user ID of this vehicle user.
    *
    * @return the user ID of this vehicle user
    */
    @Override
    public long getUserId() {
        return _vehicleUser.getUserId();
    }

    /**
    * Sets the user ID of this vehicle user.
    *
    * @param userId the user ID of this vehicle user
    */
    @Override
    public void setUserId(long userId) {
        _vehicleUser.setUserId(userId);
    }

    /**
    * Returns the user uuid of this vehicle user.
    *
    * @return the user uuid of this vehicle user
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.lang.String getUserUuid()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUser.getUserUuid();
    }

    /**
    * Sets the user uuid of this vehicle user.
    *
    * @param userUuid the user uuid of this vehicle user
    */
    @Override
    public void setUserUuid(java.lang.String userUuid) {
        _vehicleUser.setUserUuid(userUuid);
    }

    /**
    * Returns the vehicle ID of this vehicle user.
    *
    * @return the vehicle ID of this vehicle user
    */
    @Override
    public long getVehicleId() {
        return _vehicleUser.getVehicleId();
    }

    /**
    * Sets the vehicle ID of this vehicle user.
    *
    * @param vehicleId the vehicle ID of this vehicle user
    */
    @Override
    public void setVehicleId(long vehicleId) {
        _vehicleUser.setVehicleId(vehicleId);
    }

    @Override
    public boolean isNew() {
        return _vehicleUser.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _vehicleUser.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _vehicleUser.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _vehicleUser.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _vehicleUser.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _vehicleUser.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _vehicleUser.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _vehicleUser.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _vehicleUser.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _vehicleUser.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _vehicleUser.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new VehicleUserWrapper((VehicleUser) _vehicleUser.clone());
    }

    @Override
    public int compareTo(VehicleUser vehicleUser) {
        return _vehicleUser.compareTo(vehicleUser);
    }

    @Override
    public int hashCode() {
        return _vehicleUser.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<VehicleUser> toCacheModel() {
        return _vehicleUser.toCacheModel();
    }

    @Override
    public VehicleUser toEscapedModel() {
        return new VehicleUserWrapper(_vehicleUser.toEscapedModel());
    }

    @Override
    public VehicleUser toUnescapedModel() {
        return new VehicleUserWrapper(_vehicleUser.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _vehicleUser.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _vehicleUser.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _vehicleUser.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VehicleUserWrapper)) {
            return false;
        }

        VehicleUserWrapper vehicleUserWrapper = (VehicleUserWrapper) obj;

        if (Validator.equals(_vehicleUser, vehicleUserWrapper._vehicleUser)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public VehicleUser getWrappedVehicleUser() {
        return _vehicleUser;
    }

    @Override
    public VehicleUser getWrappedModel() {
        return _vehicleUser;
    }

    @Override
    public void resetOriginalValues() {
        _vehicleUser.resetOriginalValues();
    }
}
