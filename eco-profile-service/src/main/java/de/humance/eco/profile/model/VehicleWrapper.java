package de.humance.eco.profile.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Vehicle}.
 * </p>
 *
 * @author Humance
 * @see Vehicle
 * @generated
 */
public class VehicleWrapper implements Vehicle, ModelWrapper<Vehicle> {
    private Vehicle _vehicle;

    public VehicleWrapper(Vehicle vehicle) {
        _vehicle = vehicle;
    }

    @Override
    public Class<?> getModelClass() {
        return Vehicle.class;
    }

    @Override
    public String getModelClassName() {
        return Vehicle.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("vehicleId", getVehicleId());
        attributes.put("creatorId", getCreatorId());
        attributes.put("creatorName", getCreatorName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifierId", getModifierId());
        attributes.put("modifierName", getModifierName());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("driverId", getDriverId());
        attributes.put("typeId", getTypeId());
        attributes.put("manufacturerId", getManufacturerId());
        attributes.put("organizationId", getOrganizationId());
        attributes.put("modelName", getModelName());
        attributes.put("dimensionHeight", getDimensionHeight());
        attributes.put("dimensionWidth", getDimensionWidth());
        attributes.put("dimensionDepth", getDimensionDepth());
        attributes.put("licensePlate", getLicensePlate());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long vehicleId = (Long) attributes.get("vehicleId");

        if (vehicleId != null) {
            setVehicleId(vehicleId);
        }

        Long creatorId = (Long) attributes.get("creatorId");

        if (creatorId != null) {
            setCreatorId(creatorId);
        }

        String creatorName = (String) attributes.get("creatorName");

        if (creatorName != null) {
            setCreatorName(creatorName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Long modifierId = (Long) attributes.get("modifierId");

        if (modifierId != null) {
            setModifierId(modifierId);
        }

        String modifierName = (String) attributes.get("modifierName");

        if (modifierName != null) {
            setModifierName(modifierName);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        Long driverId = (Long) attributes.get("driverId");

        if (driverId != null) {
            setDriverId(driverId);
        }

        Long typeId = (Long) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        Long manufacturerId = (Long) attributes.get("manufacturerId");

        if (manufacturerId != null) {
            setManufacturerId(manufacturerId);
        }

        Long organizationId = (Long) attributes.get("organizationId");

        if (organizationId != null) {
            setOrganizationId(organizationId);
        }

        String modelName = (String) attributes.get("modelName");

        if (modelName != null) {
            setModelName(modelName);
        }

        Long dimensionHeight = (Long) attributes.get("dimensionHeight");

        if (dimensionHeight != null) {
            setDimensionHeight(dimensionHeight);
        }

        Long dimensionWidth = (Long) attributes.get("dimensionWidth");

        if (dimensionWidth != null) {
            setDimensionWidth(dimensionWidth);
        }

        Long dimensionDepth = (Long) attributes.get("dimensionDepth");

        if (dimensionDepth != null) {
            setDimensionDepth(dimensionDepth);
        }

        String licensePlate = (String) attributes.get("licensePlate");

        if (licensePlate != null) {
            setLicensePlate(licensePlate);
        }
    }

    /**
    * Returns the primary key of this vehicle.
    *
    * @return the primary key of this vehicle
    */
    @Override
    public long getPrimaryKey() {
        return _vehicle.getPrimaryKey();
    }

    /**
    * Sets the primary key of this vehicle.
    *
    * @param primaryKey the primary key of this vehicle
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _vehicle.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the vehicle ID of this vehicle.
    *
    * @return the vehicle ID of this vehicle
    */
    @Override
    public long getVehicleId() {
        return _vehicle.getVehicleId();
    }

    /**
    * Sets the vehicle ID of this vehicle.
    *
    * @param vehicleId the vehicle ID of this vehicle
    */
    @Override
    public void setVehicleId(long vehicleId) {
        _vehicle.setVehicleId(vehicleId);
    }

    /**
    * Returns the creator ID of this vehicle.
    *
    * @return the creator ID of this vehicle
    */
    @Override
    public long getCreatorId() {
        return _vehicle.getCreatorId();
    }

    /**
    * Sets the creator ID of this vehicle.
    *
    * @param creatorId the creator ID of this vehicle
    */
    @Override
    public void setCreatorId(long creatorId) {
        _vehicle.setCreatorId(creatorId);
    }

    /**
    * Returns the creator name of this vehicle.
    *
    * @return the creator name of this vehicle
    */
    @Override
    public java.lang.String getCreatorName() {
        return _vehicle.getCreatorName();
    }

    /**
    * Sets the creator name of this vehicle.
    *
    * @param creatorName the creator name of this vehicle
    */
    @Override
    public void setCreatorName(java.lang.String creatorName) {
        _vehicle.setCreatorName(creatorName);
    }

    /**
    * Returns the create date of this vehicle.
    *
    * @return the create date of this vehicle
    */
    @Override
    public java.util.Date getCreateDate() {
        return _vehicle.getCreateDate();
    }

    /**
    * Sets the create date of this vehicle.
    *
    * @param createDate the create date of this vehicle
    */
    @Override
    public void setCreateDate(java.util.Date createDate) {
        _vehicle.setCreateDate(createDate);
    }

    /**
    * Returns the modifier ID of this vehicle.
    *
    * @return the modifier ID of this vehicle
    */
    @Override
    public long getModifierId() {
        return _vehicle.getModifierId();
    }

    /**
    * Sets the modifier ID of this vehicle.
    *
    * @param modifierId the modifier ID of this vehicle
    */
    @Override
    public void setModifierId(long modifierId) {
        _vehicle.setModifierId(modifierId);
    }

    /**
    * Returns the modifier name of this vehicle.
    *
    * @return the modifier name of this vehicle
    */
    @Override
    public java.lang.String getModifierName() {
        return _vehicle.getModifierName();
    }

    /**
    * Sets the modifier name of this vehicle.
    *
    * @param modifierName the modifier name of this vehicle
    */
    @Override
    public void setModifierName(java.lang.String modifierName) {
        _vehicle.setModifierName(modifierName);
    }

    /**
    * Returns the modified date of this vehicle.
    *
    * @return the modified date of this vehicle
    */
    @Override
    public java.util.Date getModifiedDate() {
        return _vehicle.getModifiedDate();
    }

    /**
    * Sets the modified date of this vehicle.
    *
    * @param modifiedDate the modified date of this vehicle
    */
    @Override
    public void setModifiedDate(java.util.Date modifiedDate) {
        _vehicle.setModifiedDate(modifiedDate);
    }

    /**
    * Returns the driver ID of this vehicle.
    *
    * @return the driver ID of this vehicle
    */
    @Override
    public long getDriverId() {
        return _vehicle.getDriverId();
    }

    /**
    * Sets the driver ID of this vehicle.
    *
    * @param driverId the driver ID of this vehicle
    */
    @Override
    public void setDriverId(long driverId) {
        _vehicle.setDriverId(driverId);
    }

    /**
    * Returns the type ID of this vehicle.
    *
    * @return the type ID of this vehicle
    */
    @Override
    public long getTypeId() {
        return _vehicle.getTypeId();
    }

    /**
    * Sets the type ID of this vehicle.
    *
    * @param typeId the type ID of this vehicle
    */
    @Override
    public void setTypeId(long typeId) {
        _vehicle.setTypeId(typeId);
    }

    /**
    * Returns the manufacturer ID of this vehicle.
    *
    * @return the manufacturer ID of this vehicle
    */
    @Override
    public long getManufacturerId() {
        return _vehicle.getManufacturerId();
    }

    /**
    * Sets the manufacturer ID of this vehicle.
    *
    * @param manufacturerId the manufacturer ID of this vehicle
    */
    @Override
    public void setManufacturerId(long manufacturerId) {
        _vehicle.setManufacturerId(manufacturerId);
    }

    /**
    * Returns the organization ID of this vehicle.
    *
    * @return the organization ID of this vehicle
    */
    @Override
    public long getOrganizationId() {
        return _vehicle.getOrganizationId();
    }

    /**
    * Sets the organization ID of this vehicle.
    *
    * @param organizationId the organization ID of this vehicle
    */
    @Override
    public void setOrganizationId(long organizationId) {
        _vehicle.setOrganizationId(organizationId);
    }

    /**
    * Returns the model name of this vehicle.
    *
    * @return the model name of this vehicle
    */
    @Override
    public java.lang.String getModelName() {
        return _vehicle.getModelName();
    }

    /**
    * Sets the model name of this vehicle.
    *
    * @param modelName the model name of this vehicle
    */
    @Override
    public void setModelName(java.lang.String modelName) {
        _vehicle.setModelName(modelName);
    }

    /**
    * Returns the dimension height of this vehicle.
    *
    * @return the dimension height of this vehicle
    */
    @Override
    public long getDimensionHeight() {
        return _vehicle.getDimensionHeight();
    }

    /**
    * Sets the dimension height of this vehicle.
    *
    * @param dimensionHeight the dimension height of this vehicle
    */
    @Override
    public void setDimensionHeight(long dimensionHeight) {
        _vehicle.setDimensionHeight(dimensionHeight);
    }

    /**
    * Returns the dimension width of this vehicle.
    *
    * @return the dimension width of this vehicle
    */
    @Override
    public long getDimensionWidth() {
        return _vehicle.getDimensionWidth();
    }

    /**
    * Sets the dimension width of this vehicle.
    *
    * @param dimensionWidth the dimension width of this vehicle
    */
    @Override
    public void setDimensionWidth(long dimensionWidth) {
        _vehicle.setDimensionWidth(dimensionWidth);
    }

    /**
    * Returns the dimension depth of this vehicle.
    *
    * @return the dimension depth of this vehicle
    */
    @Override
    public long getDimensionDepth() {
        return _vehicle.getDimensionDepth();
    }

    /**
    * Sets the dimension depth of this vehicle.
    *
    * @param dimensionDepth the dimension depth of this vehicle
    */
    @Override
    public void setDimensionDepth(long dimensionDepth) {
        _vehicle.setDimensionDepth(dimensionDepth);
    }

    /**
    * Returns the license plate of this vehicle.
    *
    * @return the license plate of this vehicle
    */
    @Override
    public java.lang.String getLicensePlate() {
        return _vehicle.getLicensePlate();
    }

    /**
    * Sets the license plate of this vehicle.
    *
    * @param licensePlate the license plate of this vehicle
    */
    @Override
    public void setLicensePlate(java.lang.String licensePlate) {
        _vehicle.setLicensePlate(licensePlate);
    }

    @Override
    public boolean isNew() {
        return _vehicle.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _vehicle.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _vehicle.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _vehicle.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _vehicle.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _vehicle.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _vehicle.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _vehicle.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _vehicle.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _vehicle.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _vehicle.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new VehicleWrapper((Vehicle) _vehicle.clone());
    }

    @Override
    public int compareTo(Vehicle vehicle) {
        return _vehicle.compareTo(vehicle);
    }

    @Override
    public int hashCode() {
        return _vehicle.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<Vehicle> toCacheModel() {
        return _vehicle.toCacheModel();
    }

    @Override
    public Vehicle toEscapedModel() {
        return new VehicleWrapper(_vehicle.toEscapedModel());
    }

    @Override
    public Vehicle toUnescapedModel() {
        return new VehicleWrapper(_vehicle.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _vehicle.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _vehicle.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _vehicle.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VehicleWrapper)) {
            return false;
        }

        VehicleWrapper vehicleWrapper = (VehicleWrapper) obj;

        if (Validator.equals(_vehicle, vehicleWrapper._vehicle)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Vehicle getWrappedVehicle() {
        return _vehicle;
    }

    @Override
    public Vehicle getWrappedModel() {
        return _vehicle;
    }

    @Override
    public void resetOriginalValues() {
        _vehicle.resetOriginalValues();
    }
}
