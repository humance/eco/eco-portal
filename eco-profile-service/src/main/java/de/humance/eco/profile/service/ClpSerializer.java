package de.humance.eco.profile.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import de.humance.eco.profile.model.QualificationAttributeClp;
import de.humance.eco.profile.model.QualificationAttributeDefinitionClp;
import de.humance.eco.profile.model.QualificationAttributeDefinitionResourceClp;
import de.humance.eco.profile.model.QualificationClp;
import de.humance.eco.profile.model.QualificationTypeClp;
import de.humance.eco.profile.model.QualificationTypeResourceClp;
import de.humance.eco.profile.model.TrailerClp;
import de.humance.eco.profile.model.TrailerTypeClp;
import de.humance.eco.profile.model.TrailerUserClp;
import de.humance.eco.profile.model.VehicleAttributeClp;
import de.humance.eco.profile.model.VehicleAttributeDefinitionClp;
import de.humance.eco.profile.model.VehicleAttributeDefinitionResourceClp;
import de.humance.eco.profile.model.VehicleClp;
import de.humance.eco.profile.model.VehicleManufacturerClp;
import de.humance.eco.profile.model.VehicleTypeClp;
import de.humance.eco.profile.model.VehicleTypeResourceClp;
import de.humance.eco.profile.model.VehicleUserClp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;


public class ClpSerializer {
    private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
    private static String _servletContextName;
    private static boolean _useReflectionToTranslateThrowable = true;

    public static String getServletContextName() {
        if (Validator.isNotNull(_servletContextName)) {
            return _servletContextName;
        }

        synchronized (ClpSerializer.class) {
            if (Validator.isNotNull(_servletContextName)) {
                return _servletContextName;
            }

            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Class<?> portletPropsClass = classLoader.loadClass(
                        "com.liferay.util.portlet.PortletProps");

                Method getMethod = portletPropsClass.getMethod("get",
                        new Class<?>[] { String.class });

                String portletPropsServletContextName = (String) getMethod.invoke(null,
                        "eco-profile-portlet-deployment-context");

                if (Validator.isNotNull(portletPropsServletContextName)) {
                    _servletContextName = portletPropsServletContextName;
                }
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info(
                        "Unable to locate deployment context from portlet properties");
                }
            }

            if (Validator.isNull(_servletContextName)) {
                try {
                    String propsUtilServletContextName = PropsUtil.get(
                            "eco-profile-portlet-deployment-context");

                    if (Validator.isNotNull(propsUtilServletContextName)) {
                        _servletContextName = propsUtilServletContextName;
                    }
                } catch (Throwable t) {
                    if (_log.isInfoEnabled()) {
                        _log.info(
                            "Unable to locate deployment context from portal properties");
                    }
                }
            }

            if (Validator.isNull(_servletContextName)) {
                _servletContextName = "eco-profile-portlet";
            }

            return _servletContextName;
        }
    }

    public static Object translateInput(BaseModel<?> oldModel) {
        Class<?> oldModelClass = oldModel.getClass();

        String oldModelClassName = oldModelClass.getName();

        if (oldModelClassName.equals(QualificationClp.class.getName())) {
            return translateInputQualification(oldModel);
        }

        if (oldModelClassName.equals(QualificationAttributeClp.class.getName())) {
            return translateInputQualificationAttribute(oldModel);
        }

        if (oldModelClassName.equals(
                    QualificationAttributeDefinitionClp.class.getName())) {
            return translateInputQualificationAttributeDefinition(oldModel);
        }

        if (oldModelClassName.equals(
                    QualificationAttributeDefinitionResourceClp.class.getName())) {
            return translateInputQualificationAttributeDefinitionResource(oldModel);
        }

        if (oldModelClassName.equals(QualificationTypeClp.class.getName())) {
            return translateInputQualificationType(oldModel);
        }

        if (oldModelClassName.equals(
                    QualificationTypeResourceClp.class.getName())) {
            return translateInputQualificationTypeResource(oldModel);
        }

        if (oldModelClassName.equals(TrailerClp.class.getName())) {
            return translateInputTrailer(oldModel);
        }

        if (oldModelClassName.equals(TrailerTypeClp.class.getName())) {
            return translateInputTrailerType(oldModel);
        }

        if (oldModelClassName.equals(TrailerUserClp.class.getName())) {
            return translateInputTrailerUser(oldModel);
        }

        if (oldModelClassName.equals(VehicleClp.class.getName())) {
            return translateInputVehicle(oldModel);
        }

        if (oldModelClassName.equals(VehicleAttributeClp.class.getName())) {
            return translateInputVehicleAttribute(oldModel);
        }

        if (oldModelClassName.equals(
                    VehicleAttributeDefinitionClp.class.getName())) {
            return translateInputVehicleAttributeDefinition(oldModel);
        }

        if (oldModelClassName.equals(
                    VehicleAttributeDefinitionResourceClp.class.getName())) {
            return translateInputVehicleAttributeDefinitionResource(oldModel);
        }

        if (oldModelClassName.equals(VehicleManufacturerClp.class.getName())) {
            return translateInputVehicleManufacturer(oldModel);
        }

        if (oldModelClassName.equals(VehicleTypeClp.class.getName())) {
            return translateInputVehicleType(oldModel);
        }

        if (oldModelClassName.equals(VehicleTypeResourceClp.class.getName())) {
            return translateInputVehicleTypeResource(oldModel);
        }

        if (oldModelClassName.equals(VehicleUserClp.class.getName())) {
            return translateInputVehicleUser(oldModel);
        }

        return oldModel;
    }

    public static Object translateInput(List<Object> oldList) {
        List<Object> newList = new ArrayList<Object>(oldList.size());

        for (int i = 0; i < oldList.size(); i++) {
            Object curObj = oldList.get(i);

            newList.add(translateInput(curObj));
        }

        return newList;
    }

    public static Object translateInputQualification(BaseModel<?> oldModel) {
        QualificationClp oldClpModel = (QualificationClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getQualificationRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputQualificationAttribute(
        BaseModel<?> oldModel) {
        QualificationAttributeClp oldClpModel = (QualificationAttributeClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getQualificationAttributeRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputQualificationAttributeDefinition(
        BaseModel<?> oldModel) {
        QualificationAttributeDefinitionClp oldClpModel = (QualificationAttributeDefinitionClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getQualificationAttributeDefinitionRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputQualificationAttributeDefinitionResource(
        BaseModel<?> oldModel) {
        QualificationAttributeDefinitionResourceClp oldClpModel = (QualificationAttributeDefinitionResourceClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getQualificationAttributeDefinitionResourceRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputQualificationType(BaseModel<?> oldModel) {
        QualificationTypeClp oldClpModel = (QualificationTypeClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getQualificationTypeRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputQualificationTypeResource(
        BaseModel<?> oldModel) {
        QualificationTypeResourceClp oldClpModel = (QualificationTypeResourceClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getQualificationTypeResourceRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputTrailer(BaseModel<?> oldModel) {
        TrailerClp oldClpModel = (TrailerClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getTrailerRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputTrailerType(BaseModel<?> oldModel) {
        TrailerTypeClp oldClpModel = (TrailerTypeClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getTrailerTypeRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputTrailerUser(BaseModel<?> oldModel) {
        TrailerUserClp oldClpModel = (TrailerUserClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getTrailerUserRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputVehicle(BaseModel<?> oldModel) {
        VehicleClp oldClpModel = (VehicleClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getVehicleRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputVehicleAttribute(BaseModel<?> oldModel) {
        VehicleAttributeClp oldClpModel = (VehicleAttributeClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getVehicleAttributeRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputVehicleAttributeDefinition(
        BaseModel<?> oldModel) {
        VehicleAttributeDefinitionClp oldClpModel = (VehicleAttributeDefinitionClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getVehicleAttributeDefinitionRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputVehicleAttributeDefinitionResource(
        BaseModel<?> oldModel) {
        VehicleAttributeDefinitionResourceClp oldClpModel = (VehicleAttributeDefinitionResourceClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getVehicleAttributeDefinitionResourceRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputVehicleManufacturer(
        BaseModel<?> oldModel) {
        VehicleManufacturerClp oldClpModel = (VehicleManufacturerClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getVehicleManufacturerRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputVehicleType(BaseModel<?> oldModel) {
        VehicleTypeClp oldClpModel = (VehicleTypeClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getVehicleTypeRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputVehicleTypeResource(
        BaseModel<?> oldModel) {
        VehicleTypeResourceClp oldClpModel = (VehicleTypeResourceClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getVehicleTypeResourceRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputVehicleUser(BaseModel<?> oldModel) {
        VehicleUserClp oldClpModel = (VehicleUserClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getVehicleUserRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInput(Object obj) {
        if (obj instanceof BaseModel<?>) {
            return translateInput((BaseModel<?>) obj);
        } else if (obj instanceof List<?>) {
            return translateInput((List<Object>) obj);
        } else {
            return obj;
        }
    }

    public static Object translateOutput(BaseModel<?> oldModel) {
        Class<?> oldModelClass = oldModel.getClass();

        String oldModelClassName = oldModelClass.getName();

        if (oldModelClassName.equals(
                    "de.humance.eco.profile.model.impl.QualificationImpl")) {
            return translateOutputQualification(oldModel);
        }

        if (oldModelClassName.equals(
                    "de.humance.eco.profile.model.impl.QualificationAttributeImpl")) {
            return translateOutputQualificationAttribute(oldModel);
        }

        if (oldModelClassName.equals(
                    "de.humance.eco.profile.model.impl.QualificationAttributeDefinitionImpl")) {
            return translateOutputQualificationAttributeDefinition(oldModel);
        }

        if (oldModelClassName.equals(
                    "de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceImpl")) {
            return translateOutputQualificationAttributeDefinitionResource(oldModel);
        }

        if (oldModelClassName.equals(
                    "de.humance.eco.profile.model.impl.QualificationTypeImpl")) {
            return translateOutputQualificationType(oldModel);
        }

        if (oldModelClassName.equals(
                    "de.humance.eco.profile.model.impl.QualificationTypeResourceImpl")) {
            return translateOutputQualificationTypeResource(oldModel);
        }

        if (oldModelClassName.equals(
                    "de.humance.eco.profile.model.impl.TrailerImpl")) {
            return translateOutputTrailer(oldModel);
        }

        if (oldModelClassName.equals(
                    "de.humance.eco.profile.model.impl.TrailerTypeImpl")) {
            return translateOutputTrailerType(oldModel);
        }

        if (oldModelClassName.equals(
                    "de.humance.eco.profile.model.impl.TrailerUserImpl")) {
            return translateOutputTrailerUser(oldModel);
        }

        if (oldModelClassName.equals(
                    "de.humance.eco.profile.model.impl.VehicleImpl")) {
            return translateOutputVehicle(oldModel);
        }

        if (oldModelClassName.equals(
                    "de.humance.eco.profile.model.impl.VehicleAttributeImpl")) {
            return translateOutputVehicleAttribute(oldModel);
        }

        if (oldModelClassName.equals(
                    "de.humance.eco.profile.model.impl.VehicleAttributeDefinitionImpl")) {
            return translateOutputVehicleAttributeDefinition(oldModel);
        }

        if (oldModelClassName.equals(
                    "de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceImpl")) {
            return translateOutputVehicleAttributeDefinitionResource(oldModel);
        }

        if (oldModelClassName.equals(
                    "de.humance.eco.profile.model.impl.VehicleManufacturerImpl")) {
            return translateOutputVehicleManufacturer(oldModel);
        }

        if (oldModelClassName.equals(
                    "de.humance.eco.profile.model.impl.VehicleTypeImpl")) {
            return translateOutputVehicleType(oldModel);
        }

        if (oldModelClassName.equals(
                    "de.humance.eco.profile.model.impl.VehicleTypeResourceImpl")) {
            return translateOutputVehicleTypeResource(oldModel);
        }

        if (oldModelClassName.equals(
                    "de.humance.eco.profile.model.impl.VehicleUserImpl")) {
            return translateOutputVehicleUser(oldModel);
        }

        return oldModel;
    }

    public static Object translateOutput(List<Object> oldList) {
        List<Object> newList = new ArrayList<Object>(oldList.size());

        for (int i = 0; i < oldList.size(); i++) {
            Object curObj = oldList.get(i);

            newList.add(translateOutput(curObj));
        }

        return newList;
    }

    public static Object translateOutput(Object obj) {
        if (obj instanceof BaseModel<?>) {
            return translateOutput((BaseModel<?>) obj);
        } else if (obj instanceof List<?>) {
            return translateOutput((List<Object>) obj);
        } else {
            return obj;
        }
    }

    public static Throwable translateThrowable(Throwable throwable) {
        if (_useReflectionToTranslateThrowable) {
            try {
                UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

                objectOutputStream.writeObject(throwable);

                objectOutputStream.flush();
                objectOutputStream.close();

                UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
                        0, unsyncByteArrayOutputStream.size());

                Thread currentThread = Thread.currentThread();

                ClassLoader contextClassLoader = currentThread.getContextClassLoader();

                ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
                        contextClassLoader);

                throwable = (Throwable) objectInputStream.readObject();

                objectInputStream.close();

                return throwable;
            } catch (SecurityException se) {
                if (_log.isInfoEnabled()) {
                    _log.info("Do not use reflection to translate throwable");
                }

                _useReflectionToTranslateThrowable = false;
            } catch (Throwable throwable2) {
                _log.error(throwable2, throwable2);

                return throwable2;
            }
        }

        Class<?> clazz = throwable.getClass();

        String className = clazz.getName();

        if (className.equals(PortalException.class.getName())) {
            return new PortalException();
        }

        if (className.equals(SystemException.class.getName())) {
            return new SystemException();
        }

        if (className.equals(
                    "de.humance.eco.profile.NoSuchQualificationException")) {
            return new de.humance.eco.profile.NoSuchQualificationException();
        }

        if (className.equals(
                    "de.humance.eco.profile.NoSuchQualificationAttributeException")) {
            return new de.humance.eco.profile.NoSuchQualificationAttributeException();
        }

        if (className.equals(
                    "de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException")) {
            return new de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException();
        }

        if (className.equals(
                    "de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException")) {
            return new de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException();
        }

        if (className.equals(
                    "de.humance.eco.profile.NoSuchQualificationTypeException")) {
            return new de.humance.eco.profile.NoSuchQualificationTypeException();
        }

        if (className.equals(
                    "de.humance.eco.profile.NoSuchQualificationTypeResourceException")) {
            return new de.humance.eco.profile.NoSuchQualificationTypeResourceException();
        }

        if (className.equals("de.humance.eco.profile.NoSuchTrailerException")) {
            return new de.humance.eco.profile.NoSuchTrailerException();
        }

        if (className.equals(
                    "de.humance.eco.profile.NoSuchTrailerTypeException")) {
            return new de.humance.eco.profile.NoSuchTrailerTypeException();
        }

        if (className.equals(
                    "de.humance.eco.profile.NoSuchTrailerUserException")) {
            return new de.humance.eco.profile.NoSuchTrailerUserException();
        }

        if (className.equals("de.humance.eco.profile.NoSuchVehicleException")) {
            return new de.humance.eco.profile.NoSuchVehicleException();
        }

        if (className.equals(
                    "de.humance.eco.profile.NoSuchVehicleAttributeException")) {
            return new de.humance.eco.profile.NoSuchVehicleAttributeException();
        }

        if (className.equals(
                    "de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException")) {
            return new de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException();
        }

        if (className.equals(
                    "de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException")) {
            return new de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException();
        }

        if (className.equals(
                    "de.humance.eco.profile.NoSuchVehicleManufacturerException")) {
            return new de.humance.eco.profile.NoSuchVehicleManufacturerException();
        }

        if (className.equals(
                    "de.humance.eco.profile.NoSuchVehicleTypeException")) {
            return new de.humance.eco.profile.NoSuchVehicleTypeException();
        }

        if (className.equals(
                    "de.humance.eco.profile.NoSuchVehicleTypeResourceException")) {
            return new de.humance.eco.profile.NoSuchVehicleTypeResourceException();
        }

        if (className.equals(
                    "de.humance.eco.profile.NoSuchVehicleUserException")) {
            return new de.humance.eco.profile.NoSuchVehicleUserException();
        }

        return throwable;
    }

    public static Object translateOutputQualification(BaseModel<?> oldModel) {
        QualificationClp newModel = new QualificationClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setQualificationRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputQualificationAttribute(
        BaseModel<?> oldModel) {
        QualificationAttributeClp newModel = new QualificationAttributeClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setQualificationAttributeRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputQualificationAttributeDefinition(
        BaseModel<?> oldModel) {
        QualificationAttributeDefinitionClp newModel = new QualificationAttributeDefinitionClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setQualificationAttributeDefinitionRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputQualificationAttributeDefinitionResource(
        BaseModel<?> oldModel) {
        QualificationAttributeDefinitionResourceClp newModel = new QualificationAttributeDefinitionResourceClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setQualificationAttributeDefinitionResourceRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputQualificationType(BaseModel<?> oldModel) {
        QualificationTypeClp newModel = new QualificationTypeClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setQualificationTypeRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputQualificationTypeResource(
        BaseModel<?> oldModel) {
        QualificationTypeResourceClp newModel = new QualificationTypeResourceClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setQualificationTypeResourceRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputTrailer(BaseModel<?> oldModel) {
        TrailerClp newModel = new TrailerClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setTrailerRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputTrailerType(BaseModel<?> oldModel) {
        TrailerTypeClp newModel = new TrailerTypeClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setTrailerTypeRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputTrailerUser(BaseModel<?> oldModel) {
        TrailerUserClp newModel = new TrailerUserClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setTrailerUserRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputVehicle(BaseModel<?> oldModel) {
        VehicleClp newModel = new VehicleClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setVehicleRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputVehicleAttribute(BaseModel<?> oldModel) {
        VehicleAttributeClp newModel = new VehicleAttributeClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setVehicleAttributeRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputVehicleAttributeDefinition(
        BaseModel<?> oldModel) {
        VehicleAttributeDefinitionClp newModel = new VehicleAttributeDefinitionClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setVehicleAttributeDefinitionRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputVehicleAttributeDefinitionResource(
        BaseModel<?> oldModel) {
        VehicleAttributeDefinitionResourceClp newModel = new VehicleAttributeDefinitionResourceClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setVehicleAttributeDefinitionResourceRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputVehicleManufacturer(
        BaseModel<?> oldModel) {
        VehicleManufacturerClp newModel = new VehicleManufacturerClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setVehicleManufacturerRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputVehicleType(BaseModel<?> oldModel) {
        VehicleTypeClp newModel = new VehicleTypeClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setVehicleTypeRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputVehicleTypeResource(
        BaseModel<?> oldModel) {
        VehicleTypeResourceClp newModel = new VehicleTypeResourceClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setVehicleTypeResourceRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputVehicleUser(BaseModel<?> oldModel) {
        VehicleUserClp newModel = new VehicleUserClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setVehicleUserRemoteModel(oldModel);

        return newModel;
    }
}
