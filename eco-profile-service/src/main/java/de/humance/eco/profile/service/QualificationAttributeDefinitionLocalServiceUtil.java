package de.humance.eco.profile.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for QualificationAttributeDefinition. This utility wraps
 * {@link de.humance.eco.profile.service.impl.QualificationAttributeDefinitionLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Humance
 * @see QualificationAttributeDefinitionLocalService
 * @see de.humance.eco.profile.service.base.QualificationAttributeDefinitionLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.impl.QualificationAttributeDefinitionLocalServiceImpl
 * @generated
 */
public class QualificationAttributeDefinitionLocalServiceUtil {
    private static QualificationAttributeDefinitionLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link de.humance.eco.profile.service.impl.QualificationAttributeDefinitionLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the qualification attribute definition to the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinition the qualification attribute definition
    * @return the qualification attribute definition that was added
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinition addQualificationAttributeDefinition(
        de.humance.eco.profile.model.QualificationAttributeDefinition qualificationAttributeDefinition)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addQualificationAttributeDefinition(qualificationAttributeDefinition);
    }

    /**
    * Creates a new qualification attribute definition with the primary key. Does not add the qualification attribute definition to the database.
    *
    * @param qualificationAttributeDefinitionId the primary key for the new qualification attribute definition
    * @return the new qualification attribute definition
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinition createQualificationAttributeDefinition(
        long qualificationAttributeDefinitionId) {
        return getService()
                   .createQualificationAttributeDefinition(qualificationAttributeDefinitionId);
    }

    /**
    * Deletes the qualification attribute definition with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinitionId the primary key of the qualification attribute definition
    * @return the qualification attribute definition that was removed
    * @throws PortalException if a qualification attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinition deleteQualificationAttributeDefinition(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .deleteQualificationAttributeDefinition(qualificationAttributeDefinitionId);
    }

    /**
    * Deletes the qualification attribute definition from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinition the qualification attribute definition
    * @return the qualification attribute definition that was removed
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinition deleteQualificationAttributeDefinition(
        de.humance.eco.profile.model.QualificationAttributeDefinition qualificationAttributeDefinition)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .deleteQualificationAttributeDefinition(qualificationAttributeDefinition);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static de.humance.eco.profile.model.QualificationAttributeDefinition fetchQualificationAttributeDefinition(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .fetchQualificationAttributeDefinition(qualificationAttributeDefinitionId);
    }

    /**
    * Returns the qualification attribute definition with the primary key.
    *
    * @param qualificationAttributeDefinitionId the primary key of the qualification attribute definition
    * @return the qualification attribute definition
    * @throws PortalException if a qualification attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinition getQualificationAttributeDefinition(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .getQualificationAttributeDefinition(qualificationAttributeDefinitionId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the qualification attribute definitions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification attribute definitions
    * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
    * @return the range of qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> getQualificationAttributeDefinitions(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getQualificationAttributeDefinitions(start, end);
    }

    /**
    * Returns the number of qualification attribute definitions.
    *
    * @return the number of qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static int getQualificationAttributeDefinitionsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getQualificationAttributeDefinitionsCount();
    }

    /**
    * Updates the qualification attribute definition in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinition the qualification attribute definition
    * @return the qualification attribute definition that was updated
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinition updateQualificationAttributeDefinition(
        de.humance.eco.profile.model.QualificationAttributeDefinition qualificationAttributeDefinition)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateQualificationAttributeDefinition(qualificationAttributeDefinition);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findByQualificationTypeId(
        long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByQualificationTypeId(qualificationTypeId);
    }

    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findByTitle(
        java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByTitle(title);
    }

    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findByTitleLike(
        java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByTitleLike(title);
    }

    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findByModifiedDate(
        java.util.Date modifiedDate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByModifiedDate(modifiedDate);
    }

    public static de.humance.eco.profile.model.QualificationAttributeDefinition addQualificationAttributeDefinition(
        long creatorId, long qualificationTypeId, java.lang.String type,
        java.lang.String title, double minRangeValue, double maxRangeValue,
        java.lang.String unit, long sequenceNumber)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addQualificationAttributeDefinition(creatorId,
            qualificationTypeId, type, title, minRangeValue, maxRangeValue,
            unit, sequenceNumber);
    }

    public static de.humance.eco.profile.model.QualificationAttributeDefinition updateQualificationAttributeDefinition(
        long modifierId, long qualificationAttributeDefinitionId,
        long qualificationTypeId, java.lang.String type,
        java.lang.String title, double minRangeValue, double maxRangeValue,
        java.lang.String unit, long sequenceNumber)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateQualificationAttributeDefinition(modifierId,
            qualificationAttributeDefinitionId, qualificationTypeId, type,
            title, minRangeValue, maxRangeValue, unit, sequenceNumber);
    }

    public static void clearService() {
        _service = null;
    }

    public static QualificationAttributeDefinitionLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    QualificationAttributeDefinitionLocalService.class.getName());

            if (invokableLocalService instanceof QualificationAttributeDefinitionLocalService) {
                _service = (QualificationAttributeDefinitionLocalService) invokableLocalService;
            } else {
                _service = new QualificationAttributeDefinitionLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(QualificationAttributeDefinitionLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(QualificationAttributeDefinitionLocalService service) {
    }
}
