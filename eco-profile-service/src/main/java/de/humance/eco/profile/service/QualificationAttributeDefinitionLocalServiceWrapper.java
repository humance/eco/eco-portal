package de.humance.eco.profile.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link QualificationAttributeDefinitionLocalService}.
 *
 * @author Humance
 * @see QualificationAttributeDefinitionLocalService
 * @generated
 */
public class QualificationAttributeDefinitionLocalServiceWrapper
    implements QualificationAttributeDefinitionLocalService,
        ServiceWrapper<QualificationAttributeDefinitionLocalService> {
    private QualificationAttributeDefinitionLocalService _qualificationAttributeDefinitionLocalService;

    public QualificationAttributeDefinitionLocalServiceWrapper(
        QualificationAttributeDefinitionLocalService qualificationAttributeDefinitionLocalService) {
        _qualificationAttributeDefinitionLocalService = qualificationAttributeDefinitionLocalService;
    }

    /**
    * Adds the qualification attribute definition to the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinition the qualification attribute definition
    * @return the qualification attribute definition that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinition addQualificationAttributeDefinition(
        de.humance.eco.profile.model.QualificationAttributeDefinition qualificationAttributeDefinition)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.addQualificationAttributeDefinition(qualificationAttributeDefinition);
    }

    /**
    * Creates a new qualification attribute definition with the primary key. Does not add the qualification attribute definition to the database.
    *
    * @param qualificationAttributeDefinitionId the primary key for the new qualification attribute definition
    * @return the new qualification attribute definition
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinition createQualificationAttributeDefinition(
        long qualificationAttributeDefinitionId) {
        return _qualificationAttributeDefinitionLocalService.createQualificationAttributeDefinition(qualificationAttributeDefinitionId);
    }

    /**
    * Deletes the qualification attribute definition with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinitionId the primary key of the qualification attribute definition
    * @return the qualification attribute definition that was removed
    * @throws PortalException if a qualification attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinition deleteQualificationAttributeDefinition(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.deleteQualificationAttributeDefinition(qualificationAttributeDefinitionId);
    }

    /**
    * Deletes the qualification attribute definition from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinition the qualification attribute definition
    * @return the qualification attribute definition that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinition deleteQualificationAttributeDefinition(
        de.humance.eco.profile.model.QualificationAttributeDefinition qualificationAttributeDefinition)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.deleteQualificationAttributeDefinition(qualificationAttributeDefinition);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _qualificationAttributeDefinitionLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.dynamicQuery(dynamicQuery,
            start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.dynamicQuery(dynamicQuery,
            start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinition fetchQualificationAttributeDefinition(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.fetchQualificationAttributeDefinition(qualificationAttributeDefinitionId);
    }

    /**
    * Returns the qualification attribute definition with the primary key.
    *
    * @param qualificationAttributeDefinitionId the primary key of the qualification attribute definition
    * @return the qualification attribute definition
    * @throws PortalException if a qualification attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinition getQualificationAttributeDefinition(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.getQualificationAttributeDefinition(qualificationAttributeDefinitionId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the qualification attribute definitions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification attribute definitions
    * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
    * @return the range of qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> getQualificationAttributeDefinitions(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.getQualificationAttributeDefinitions(start,
            end);
    }

    /**
    * Returns the number of qualification attribute definitions.
    *
    * @return the number of qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getQualificationAttributeDefinitionsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.getQualificationAttributeDefinitionsCount();
    }

    /**
    * Updates the qualification attribute definition in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinition the qualification attribute definition
    * @return the qualification attribute definition that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinition updateQualificationAttributeDefinition(
        de.humance.eco.profile.model.QualificationAttributeDefinition qualificationAttributeDefinition)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.updateQualificationAttributeDefinition(qualificationAttributeDefinition);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _qualificationAttributeDefinitionLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _qualificationAttributeDefinitionLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _qualificationAttributeDefinitionLocalService.invokeMethod(name,
            parameterTypes, arguments);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findByQualificationTypeId(
        long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.findByQualificationTypeId(qualificationTypeId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findByTitle(
        java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.findByTitle(title);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findByTitleLike(
        java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.findByTitleLike(title);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findByModifiedDate(
        java.util.Date modifiedDate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.findByModifiedDate(modifiedDate);
    }

    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinition addQualificationAttributeDefinition(
        long creatorId, long qualificationTypeId, java.lang.String type,
        java.lang.String title, double minRangeValue, double maxRangeValue,
        java.lang.String unit, long sequenceNumber)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.addQualificationAttributeDefinition(creatorId,
            qualificationTypeId, type, title, minRangeValue, maxRangeValue,
            unit, sequenceNumber);
    }

    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinition updateQualificationAttributeDefinition(
        long modifierId, long qualificationAttributeDefinitionId,
        long qualificationTypeId, java.lang.String type,
        java.lang.String title, double minRangeValue, double maxRangeValue,
        java.lang.String unit, long sequenceNumber)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionLocalService.updateQualificationAttributeDefinition(modifierId,
            qualificationAttributeDefinitionId, qualificationTypeId, type,
            title, minRangeValue, maxRangeValue, unit, sequenceNumber);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public QualificationAttributeDefinitionLocalService getWrappedQualificationAttributeDefinitionLocalService() {
        return _qualificationAttributeDefinitionLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedQualificationAttributeDefinitionLocalService(
        QualificationAttributeDefinitionLocalService qualificationAttributeDefinitionLocalService) {
        _qualificationAttributeDefinitionLocalService = qualificationAttributeDefinitionLocalService;
    }

    @Override
    public QualificationAttributeDefinitionLocalService getWrappedService() {
        return _qualificationAttributeDefinitionLocalService;
    }

    @Override
    public void setWrappedService(
        QualificationAttributeDefinitionLocalService qualificationAttributeDefinitionLocalService) {
        _qualificationAttributeDefinitionLocalService = qualificationAttributeDefinitionLocalService;
    }
}
