package de.humance.eco.profile.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for QualificationAttributeDefinitionResource. This utility wraps
 * {@link de.humance.eco.profile.service.impl.QualificationAttributeDefinitionResourceLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Humance
 * @see QualificationAttributeDefinitionResourceLocalService
 * @see de.humance.eco.profile.service.base.QualificationAttributeDefinitionResourceLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.impl.QualificationAttributeDefinitionResourceLocalServiceImpl
 * @generated
 */
public class QualificationAttributeDefinitionResourceLocalServiceUtil {
    private static QualificationAttributeDefinitionResourceLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link de.humance.eco.profile.service.impl.QualificationAttributeDefinitionResourceLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the qualification attribute definition resource to the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinitionResource the qualification attribute definition resource
    * @return the qualification attribute definition resource that was added
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource addQualificationAttributeDefinitionResource(
        de.humance.eco.profile.model.QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addQualificationAttributeDefinitionResource(qualificationAttributeDefinitionResource);
    }

    /**
    * Creates a new qualification attribute definition resource with the primary key. Does not add the qualification attribute definition resource to the database.
    *
    * @param qualificationAttributeDefinitionResourceId the primary key for the new qualification attribute definition resource
    * @return the new qualification attribute definition resource
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource createQualificationAttributeDefinitionResource(
        long qualificationAttributeDefinitionResourceId) {
        return getService()
                   .createQualificationAttributeDefinitionResource(qualificationAttributeDefinitionResourceId);
    }

    /**
    * Deletes the qualification attribute definition resource with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinitionResourceId the primary key of the qualification attribute definition resource
    * @return the qualification attribute definition resource that was removed
    * @throws PortalException if a qualification attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource deleteQualificationAttributeDefinitionResource(
        long qualificationAttributeDefinitionResourceId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .deleteQualificationAttributeDefinitionResource(qualificationAttributeDefinitionResourceId);
    }

    /**
    * Deletes the qualification attribute definition resource from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinitionResource the qualification attribute definition resource
    * @return the qualification attribute definition resource that was removed
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource deleteQualificationAttributeDefinitionResource(
        de.humance.eco.profile.model.QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .deleteQualificationAttributeDefinitionResource(qualificationAttributeDefinitionResource);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource fetchQualificationAttributeDefinitionResource(
        long qualificationAttributeDefinitionResourceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .fetchQualificationAttributeDefinitionResource(qualificationAttributeDefinitionResourceId);
    }

    /**
    * Returns the qualification attribute definition resource with the primary key.
    *
    * @param qualificationAttributeDefinitionResourceId the primary key of the qualification attribute definition resource
    * @return the qualification attribute definition resource
    * @throws PortalException if a qualification attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource getQualificationAttributeDefinitionResource(
        long qualificationAttributeDefinitionResourceId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .getQualificationAttributeDefinitionResource(qualificationAttributeDefinitionResourceId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the qualification attribute definition resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification attribute definition resources
    * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
    * @return the range of qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> getQualificationAttributeDefinitionResources(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .getQualificationAttributeDefinitionResources(start, end);
    }

    /**
    * Returns the number of qualification attribute definition resources.
    *
    * @return the number of qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static int getQualificationAttributeDefinitionResourcesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getQualificationAttributeDefinitionResourcesCount();
    }

    /**
    * Updates the qualification attribute definition resource in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinitionResource the qualification attribute definition resource
    * @return the qualification attribute definition resource that was updated
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource updateQualificationAttributeDefinitionResource(
        de.humance.eco.profile.model.QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateQualificationAttributeDefinitionResource(qualificationAttributeDefinitionResource);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByDefinitionId(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .findByDefinitionId(qualificationAttributeDefinitionId);
    }

    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByDefinitionIdAndCoutry(
        long qualificationAttributeDefinitionId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .findByDefinitionIdAndCoutry(qualificationAttributeDefinitionId,
            country);
    }

    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByDefinitionIdAndLanguage(
        long qualificationAttributeDefinitionId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .findByDefinitionIdAndLanguage(qualificationAttributeDefinitionId,
            language);
    }

    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByDefinitionIdAndLocale(
        long qualificationAttributeDefinitionId, java.lang.String country,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .findByDefinitionIdAndLocale(qualificationAttributeDefinitionId,
            country, language);
    }

    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource addQualificationAttributeDefinitionResource(
        long qualificationAttributeDefinitionId, java.lang.String country,
        java.lang.String language, java.lang.String title, java.lang.String unit)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addQualificationAttributeDefinitionResource(qualificationAttributeDefinitionId,
            country, language, title, unit);
    }

    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource updateQualificationAttributeDefinitionResource(
        long qualificationAttributeDefinitionResourceId,
        long qualificationAttributeDefinitionId, java.lang.String country,
        java.lang.String language, java.lang.String title, java.lang.String unit)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateQualificationAttributeDefinitionResource(qualificationAttributeDefinitionResourceId,
            qualificationAttributeDefinitionId, country, language, title, unit);
    }

    public static void clearService() {
        _service = null;
    }

    public static QualificationAttributeDefinitionResourceLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    QualificationAttributeDefinitionResourceLocalService.class.getName());

            if (invokableLocalService instanceof QualificationAttributeDefinitionResourceLocalService) {
                _service = (QualificationAttributeDefinitionResourceLocalService) invokableLocalService;
            } else {
                _service = new QualificationAttributeDefinitionResourceLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(QualificationAttributeDefinitionResourceLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(
        QualificationAttributeDefinitionResourceLocalService service) {
    }
}
