package de.humance.eco.profile.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link QualificationAttributeDefinitionResourceLocalService}.
 *
 * @author Humance
 * @see QualificationAttributeDefinitionResourceLocalService
 * @generated
 */
public class QualificationAttributeDefinitionResourceLocalServiceWrapper
    implements QualificationAttributeDefinitionResourceLocalService,
        ServiceWrapper<QualificationAttributeDefinitionResourceLocalService> {
    private QualificationAttributeDefinitionResourceLocalService _qualificationAttributeDefinitionResourceLocalService;

    public QualificationAttributeDefinitionResourceLocalServiceWrapper(
        QualificationAttributeDefinitionResourceLocalService qualificationAttributeDefinitionResourceLocalService) {
        _qualificationAttributeDefinitionResourceLocalService = qualificationAttributeDefinitionResourceLocalService;
    }

    /**
    * Adds the qualification attribute definition resource to the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinitionResource the qualification attribute definition resource
    * @return the qualification attribute definition resource that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinitionResource addQualificationAttributeDefinitionResource(
        de.humance.eco.profile.model.QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.addQualificationAttributeDefinitionResource(qualificationAttributeDefinitionResource);
    }

    /**
    * Creates a new qualification attribute definition resource with the primary key. Does not add the qualification attribute definition resource to the database.
    *
    * @param qualificationAttributeDefinitionResourceId the primary key for the new qualification attribute definition resource
    * @return the new qualification attribute definition resource
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinitionResource createQualificationAttributeDefinitionResource(
        long qualificationAttributeDefinitionResourceId) {
        return _qualificationAttributeDefinitionResourceLocalService.createQualificationAttributeDefinitionResource(qualificationAttributeDefinitionResourceId);
    }

    /**
    * Deletes the qualification attribute definition resource with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinitionResourceId the primary key of the qualification attribute definition resource
    * @return the qualification attribute definition resource that was removed
    * @throws PortalException if a qualification attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinitionResource deleteQualificationAttributeDefinitionResource(
        long qualificationAttributeDefinitionResourceId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.deleteQualificationAttributeDefinitionResource(qualificationAttributeDefinitionResourceId);
    }

    /**
    * Deletes the qualification attribute definition resource from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinitionResource the qualification attribute definition resource
    * @return the qualification attribute definition resource that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinitionResource deleteQualificationAttributeDefinitionResource(
        de.humance.eco.profile.model.QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.deleteQualificationAttributeDefinitionResource(qualificationAttributeDefinitionResource);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _qualificationAttributeDefinitionResourceLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.dynamicQuery(dynamicQuery,
            start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.dynamicQuery(dynamicQuery,
            start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinitionResource fetchQualificationAttributeDefinitionResource(
        long qualificationAttributeDefinitionResourceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.fetchQualificationAttributeDefinitionResource(qualificationAttributeDefinitionResourceId);
    }

    /**
    * Returns the qualification attribute definition resource with the primary key.
    *
    * @param qualificationAttributeDefinitionResourceId the primary key of the qualification attribute definition resource
    * @return the qualification attribute definition resource
    * @throws PortalException if a qualification attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinitionResource getQualificationAttributeDefinitionResource(
        long qualificationAttributeDefinitionResourceId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.getQualificationAttributeDefinitionResource(qualificationAttributeDefinitionResourceId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the qualification attribute definition resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification attribute definition resources
    * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
    * @return the range of qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> getQualificationAttributeDefinitionResources(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.getQualificationAttributeDefinitionResources(start,
            end);
    }

    /**
    * Returns the number of qualification attribute definition resources.
    *
    * @return the number of qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getQualificationAttributeDefinitionResourcesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.getQualificationAttributeDefinitionResourcesCount();
    }

    /**
    * Updates the qualification attribute definition resource in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinitionResource the qualification attribute definition resource
    * @return the qualification attribute definition resource that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinitionResource updateQualificationAttributeDefinitionResource(
        de.humance.eco.profile.model.QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.updateQualificationAttributeDefinitionResource(qualificationAttributeDefinitionResource);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _qualificationAttributeDefinitionResourceLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _qualificationAttributeDefinitionResourceLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _qualificationAttributeDefinitionResourceLocalService.invokeMethod(name,
            parameterTypes, arguments);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByDefinitionId(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.findByDefinitionId(qualificationAttributeDefinitionId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByDefinitionIdAndCoutry(
        long qualificationAttributeDefinitionId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.findByDefinitionIdAndCoutry(qualificationAttributeDefinitionId,
            country);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByDefinitionIdAndLanguage(
        long qualificationAttributeDefinitionId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.findByDefinitionIdAndLanguage(qualificationAttributeDefinitionId,
            language);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByDefinitionIdAndLocale(
        long qualificationAttributeDefinitionId, java.lang.String country,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.findByDefinitionIdAndLocale(qualificationAttributeDefinitionId,
            country, language);
    }

    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinitionResource addQualificationAttributeDefinitionResource(
        long qualificationAttributeDefinitionId, java.lang.String country,
        java.lang.String language, java.lang.String title, java.lang.String unit)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.addQualificationAttributeDefinitionResource(qualificationAttributeDefinitionId,
            country, language, title, unit);
    }

    @Override
    public de.humance.eco.profile.model.QualificationAttributeDefinitionResource updateQualificationAttributeDefinitionResource(
        long qualificationAttributeDefinitionResourceId,
        long qualificationAttributeDefinitionId, java.lang.String country,
        java.lang.String language, java.lang.String title, java.lang.String unit)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeDefinitionResourceLocalService.updateQualificationAttributeDefinitionResource(qualificationAttributeDefinitionResourceId,
            qualificationAttributeDefinitionId, country, language, title, unit);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public QualificationAttributeDefinitionResourceLocalService getWrappedQualificationAttributeDefinitionResourceLocalService() {
        return _qualificationAttributeDefinitionResourceLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedQualificationAttributeDefinitionResourceLocalService(
        QualificationAttributeDefinitionResourceLocalService qualificationAttributeDefinitionResourceLocalService) {
        _qualificationAttributeDefinitionResourceLocalService = qualificationAttributeDefinitionResourceLocalService;
    }

    @Override
    public QualificationAttributeDefinitionResourceLocalService getWrappedService() {
        return _qualificationAttributeDefinitionResourceLocalService;
    }

    @Override
    public void setWrappedService(
        QualificationAttributeDefinitionResourceLocalService qualificationAttributeDefinitionResourceLocalService) {
        _qualificationAttributeDefinitionResourceLocalService = qualificationAttributeDefinitionResourceLocalService;
    }
}
