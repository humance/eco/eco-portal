package de.humance.eco.profile.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link QualificationAttributeLocalService}.
 *
 * @author Humance
 * @see QualificationAttributeLocalService
 * @generated
 */
public class QualificationAttributeLocalServiceWrapper
    implements QualificationAttributeLocalService,
        ServiceWrapper<QualificationAttributeLocalService> {
    private QualificationAttributeLocalService _qualificationAttributeLocalService;

    public QualificationAttributeLocalServiceWrapper(
        QualificationAttributeLocalService qualificationAttributeLocalService) {
        _qualificationAttributeLocalService = qualificationAttributeLocalService;
    }

    /**
    * Adds the qualification attribute to the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttribute the qualification attribute
    * @return the qualification attribute that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttribute addQualificationAttribute(
        de.humance.eco.profile.model.QualificationAttribute qualificationAttribute)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.addQualificationAttribute(qualificationAttribute);
    }

    /**
    * Creates a new qualification attribute with the primary key. Does not add the qualification attribute to the database.
    *
    * @param qualificationAttributeId the primary key for the new qualification attribute
    * @return the new qualification attribute
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttribute createQualificationAttribute(
        long qualificationAttributeId) {
        return _qualificationAttributeLocalService.createQualificationAttribute(qualificationAttributeId);
    }

    /**
    * Deletes the qualification attribute with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeId the primary key of the qualification attribute
    * @return the qualification attribute that was removed
    * @throws PortalException if a qualification attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttribute deleteQualificationAttribute(
        long qualificationAttributeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.deleteQualificationAttribute(qualificationAttributeId);
    }

    /**
    * Deletes the qualification attribute from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttribute the qualification attribute
    * @return the qualification attribute that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttribute deleteQualificationAttribute(
        de.humance.eco.profile.model.QualificationAttribute qualificationAttribute)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.deleteQualificationAttribute(qualificationAttribute);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _qualificationAttributeLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.dynamicQuery(dynamicQuery,
            start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.dynamicQuery(dynamicQuery,
            start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public de.humance.eco.profile.model.QualificationAttribute fetchQualificationAttribute(
        long qualificationAttributeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.fetchQualificationAttribute(qualificationAttributeId);
    }

    /**
    * Returns the qualification attribute with the primary key.
    *
    * @param qualificationAttributeId the primary key of the qualification attribute
    * @return the qualification attribute
    * @throws PortalException if a qualification attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttribute getQualificationAttribute(
        long qualificationAttributeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.getQualificationAttribute(qualificationAttributeId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the qualification attributes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification attributes
    * @param end the upper bound of the range of qualification attributes (not inclusive)
    * @return the range of qualification attributes
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationAttribute> getQualificationAttributes(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.getQualificationAttributes(start,
            end);
    }

    /**
    * Returns the number of qualification attributes.
    *
    * @return the number of qualification attributes
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getQualificationAttributesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.getQualificationAttributesCount();
    }

    /**
    * Updates the qualification attribute in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttribute the qualification attribute
    * @return the qualification attribute that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationAttribute updateQualificationAttribute(
        de.humance.eco.profile.model.QualificationAttribute qualificationAttribute)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.updateQualificationAttribute(qualificationAttribute);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _qualificationAttributeLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _qualificationAttributeLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _qualificationAttributeLocalService.invokeMethod(name,
            parameterTypes, arguments);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByAttributeDefinitionId(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.findByAttributeDefinitionId(qualificationAttributeDefinitionId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationId(
        long qualificationId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.findByQualificationId(qualificationId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationIdAndAttributeDefinitionId(
        long qualificationId, long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.findByQualificationIdAndAttributeDefinitionId(qualificationId,
            qualificationAttributeDefinitionId);
    }

    @Override
    public de.humance.eco.profile.model.QualificationAttribute addQualificationAttribute(
        long qualificationId, long qualificationAttributeDefinitionId,
        java.lang.String type, long sequenceNumber,
        java.lang.String attributeValue)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.addQualificationAttribute(qualificationId,
            qualificationAttributeDefinitionId, type, sequenceNumber,
            attributeValue);
    }

    @Override
    public de.humance.eco.profile.model.QualificationAttribute updateQualificationAttribute(
        long qualificationAttributeId, long qualificationId,
        long qualificationAttributeDefinitionId, java.lang.String type,
        long sequenceNumber, java.lang.String attributeValue)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationAttributeLocalService.updateQualificationAttribute(qualificationAttributeId,
            qualificationId, qualificationAttributeDefinitionId, type,
            sequenceNumber, attributeValue);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public QualificationAttributeLocalService getWrappedQualificationAttributeLocalService() {
        return _qualificationAttributeLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedQualificationAttributeLocalService(
        QualificationAttributeLocalService qualificationAttributeLocalService) {
        _qualificationAttributeLocalService = qualificationAttributeLocalService;
    }

    @Override
    public QualificationAttributeLocalService getWrappedService() {
        return _qualificationAttributeLocalService;
    }

    @Override
    public void setWrappedService(
        QualificationAttributeLocalService qualificationAttributeLocalService) {
        _qualificationAttributeLocalService = qualificationAttributeLocalService;
    }
}
