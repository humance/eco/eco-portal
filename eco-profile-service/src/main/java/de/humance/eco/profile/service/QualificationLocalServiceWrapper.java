package de.humance.eco.profile.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link QualificationLocalService}.
 *
 * @author Humance
 * @see QualificationLocalService
 * @generated
 */
public class QualificationLocalServiceWrapper
    implements QualificationLocalService,
        ServiceWrapper<QualificationLocalService> {
    private QualificationLocalService _qualificationLocalService;

    public QualificationLocalServiceWrapper(
        QualificationLocalService qualificationLocalService) {
        _qualificationLocalService = qualificationLocalService;
    }

    /**
    * Adds the qualification to the database. Also notifies the appropriate model listeners.
    *
    * @param qualification the qualification
    * @return the qualification that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.Qualification addQualification(
        de.humance.eco.profile.model.Qualification qualification)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.addQualification(qualification);
    }

    /**
    * Creates a new qualification with the primary key. Does not add the qualification to the database.
    *
    * @param qualificationId the primary key for the new qualification
    * @return the new qualification
    */
    @Override
    public de.humance.eco.profile.model.Qualification createQualification(
        long qualificationId) {
        return _qualificationLocalService.createQualification(qualificationId);
    }

    /**
    * Deletes the qualification with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationId the primary key of the qualification
    * @return the qualification that was removed
    * @throws PortalException if a qualification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.Qualification deleteQualification(
        long qualificationId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.deleteQualification(qualificationId);
    }

    /**
    * Deletes the qualification from the database. Also notifies the appropriate model listeners.
    *
    * @param qualification the qualification
    * @return the qualification that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.Qualification deleteQualification(
        de.humance.eco.profile.model.Qualification qualification)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.deleteQualification(qualification);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _qualificationLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public de.humance.eco.profile.model.Qualification fetchQualification(
        long qualificationId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.fetchQualification(qualificationId);
    }

    /**
    * Returns the qualification with the primary key.
    *
    * @param qualificationId the primary key of the qualification
    * @return the qualification
    * @throws PortalException if a qualification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.Qualification getQualification(
        long qualificationId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.getQualification(qualificationId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the qualifications.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualifications
    * @param end the upper bound of the range of qualifications (not inclusive)
    * @return the range of qualifications
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<de.humance.eco.profile.model.Qualification> getQualifications(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.getQualifications(start, end);
    }

    /**
    * Returns the number of qualifications.
    *
    * @return the number of qualifications
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getQualificationsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.getQualificationsCount();
    }

    /**
    * Updates the qualification in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param qualification the qualification
    * @return the qualification that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.Qualification updateQualification(
        de.humance.eco.profile.model.Qualification qualification)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.updateQualification(qualification);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _qualificationLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _qualificationLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _qualificationLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Qualification> findByOwnerId(
        long ownerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.findByOwnerId(ownerId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Qualification> findByTypeId(
        long typeId) throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.findByTypeId(typeId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Qualification> findByOwnerIdAndTypeId(
        long ownerId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.findByOwnerIdAndTypeId(ownerId, typeId);
    }

    @Override
    public de.humance.eco.profile.model.Qualification addQualification(
        long creatorId, java.lang.String creatorName, long ownerId,
        long typeId, java.lang.String name, java.lang.String certNumber,
        java.util.Date deliveryDate, java.util.Date expirationDate,
        java.lang.String issuingCountry, java.lang.String issuingInstitution)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.addQualification(creatorId,
            creatorName, ownerId, typeId, name, certNumber, deliveryDate,
            expirationDate, issuingCountry, issuingInstitution);
    }

    @Override
    public de.humance.eco.profile.model.Qualification updateQualification(
        long qualificationId, long modifierId, java.lang.String modifierName,
        long ownerId, long typeId, java.lang.String name,
        java.lang.String certNumber, java.util.Date deliveryDate,
        java.util.Date expirationDate, java.lang.String issuingCountry,
        java.lang.String issuingInstitution)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationLocalService.updateQualification(qualificationId,
            modifierId, modifierName, ownerId, typeId, name, certNumber,
            deliveryDate, expirationDate, issuingCountry, issuingInstitution);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public QualificationLocalService getWrappedQualificationLocalService() {
        return _qualificationLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedQualificationLocalService(
        QualificationLocalService qualificationLocalService) {
        _qualificationLocalService = qualificationLocalService;
    }

    @Override
    public QualificationLocalService getWrappedService() {
        return _qualificationLocalService;
    }

    @Override
    public void setWrappedService(
        QualificationLocalService qualificationLocalService) {
        _qualificationLocalService = qualificationLocalService;
    }
}
