package de.humance.eco.profile.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link QualificationTypeLocalService}.
 *
 * @author Humance
 * @see QualificationTypeLocalService
 * @generated
 */
public class QualificationTypeLocalServiceWrapper
    implements QualificationTypeLocalService,
        ServiceWrapper<QualificationTypeLocalService> {
    private QualificationTypeLocalService _qualificationTypeLocalService;

    public QualificationTypeLocalServiceWrapper(
        QualificationTypeLocalService qualificationTypeLocalService) {
        _qualificationTypeLocalService = qualificationTypeLocalService;
    }

    /**
    * Adds the qualification type to the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationType the qualification type
    * @return the qualification type that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationType addQualificationType(
        de.humance.eco.profile.model.QualificationType qualificationType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.addQualificationType(qualificationType);
    }

    /**
    * Creates a new qualification type with the primary key. Does not add the qualification type to the database.
    *
    * @param qualificationTypeId the primary key for the new qualification type
    * @return the new qualification type
    */
    @Override
    public de.humance.eco.profile.model.QualificationType createQualificationType(
        long qualificationTypeId) {
        return _qualificationTypeLocalService.createQualificationType(qualificationTypeId);
    }

    /**
    * Deletes the qualification type with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationTypeId the primary key of the qualification type
    * @return the qualification type that was removed
    * @throws PortalException if a qualification type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationType deleteQualificationType(
        long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.deleteQualificationType(qualificationTypeId);
    }

    /**
    * Deletes the qualification type from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationType the qualification type
    * @return the qualification type that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationType deleteQualificationType(
        de.humance.eco.profile.model.QualificationType qualificationType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.deleteQualificationType(qualificationType);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _qualificationTypeLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.dynamicQuery(dynamicQuery, start,
            end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public de.humance.eco.profile.model.QualificationType fetchQualificationType(
        long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.fetchQualificationType(qualificationTypeId);
    }

    /**
    * Returns the qualification type with the primary key.
    *
    * @param qualificationTypeId the primary key of the qualification type
    * @return the qualification type
    * @throws PortalException if a qualification type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationType getQualificationType(
        long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.getQualificationType(qualificationTypeId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the qualification types.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification types
    * @param end the upper bound of the range of qualification types (not inclusive)
    * @return the range of qualification types
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationType> getQualificationTypes(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.getQualificationTypes(start, end);
    }

    /**
    * Returns the number of qualification types.
    *
    * @return the number of qualification types
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getQualificationTypesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.getQualificationTypesCount();
    }

    /**
    * Updates the qualification type in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param qualificationType the qualification type
    * @return the qualification type that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationType updateQualificationType(
        de.humance.eco.profile.model.QualificationType qualificationType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.updateQualificationType(qualificationType);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _qualificationTypeLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _qualificationTypeLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _qualificationTypeLocalService.invokeMethod(name,
            parameterTypes, arguments);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationType> findByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.findByName(name);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationType> findByNameLike(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.findByNameLike(name);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationType> findByModifiedDate(
        java.util.Date modifiedDate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.findByModifiedDate(modifiedDate);
    }

    @Override
    public de.humance.eco.profile.model.QualificationType addQualificationType(
        long creatorId, java.lang.String creatorName, java.lang.String name,
        java.lang.String description, java.lang.String note, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.addQualificationType(creatorId,
            creatorName, name, description, note, iconId);
    }

    @Override
    public de.humance.eco.profile.model.QualificationType updateQualificationType(
        long qualificationTypeId, long modifierId,
        java.lang.String modifierName, java.lang.String name,
        java.lang.String description, java.lang.String note, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeLocalService.updateQualificationType(qualificationTypeId,
            modifierId, modifierName, name, description, note, iconId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public QualificationTypeLocalService getWrappedQualificationTypeLocalService() {
        return _qualificationTypeLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedQualificationTypeLocalService(
        QualificationTypeLocalService qualificationTypeLocalService) {
        _qualificationTypeLocalService = qualificationTypeLocalService;
    }

    @Override
    public QualificationTypeLocalService getWrappedService() {
        return _qualificationTypeLocalService;
    }

    @Override
    public void setWrappedService(
        QualificationTypeLocalService qualificationTypeLocalService) {
        _qualificationTypeLocalService = qualificationTypeLocalService;
    }
}
