package de.humance.eco.profile.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link QualificationTypeResourceLocalService}.
 *
 * @author Humance
 * @see QualificationTypeResourceLocalService
 * @generated
 */
public class QualificationTypeResourceLocalServiceWrapper
    implements QualificationTypeResourceLocalService,
        ServiceWrapper<QualificationTypeResourceLocalService> {
    private QualificationTypeResourceLocalService _qualificationTypeResourceLocalService;

    public QualificationTypeResourceLocalServiceWrapper(
        QualificationTypeResourceLocalService qualificationTypeResourceLocalService) {
        _qualificationTypeResourceLocalService = qualificationTypeResourceLocalService;
    }

    /**
    * Adds the qualification type resource to the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationTypeResource the qualification type resource
    * @return the qualification type resource that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationTypeResource addQualificationTypeResource(
        de.humance.eco.profile.model.QualificationTypeResource qualificationTypeResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.addQualificationTypeResource(qualificationTypeResource);
    }

    /**
    * Creates a new qualification type resource with the primary key. Does not add the qualification type resource to the database.
    *
    * @param qualificationTypeResourceId the primary key for the new qualification type resource
    * @return the new qualification type resource
    */
    @Override
    public de.humance.eco.profile.model.QualificationTypeResource createQualificationTypeResource(
        long qualificationTypeResourceId) {
        return _qualificationTypeResourceLocalService.createQualificationTypeResource(qualificationTypeResourceId);
    }

    /**
    * Deletes the qualification type resource with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationTypeResourceId the primary key of the qualification type resource
    * @return the qualification type resource that was removed
    * @throws PortalException if a qualification type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationTypeResource deleteQualificationTypeResource(
        long qualificationTypeResourceId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.deleteQualificationTypeResource(qualificationTypeResourceId);
    }

    /**
    * Deletes the qualification type resource from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationTypeResource the qualification type resource
    * @return the qualification type resource that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationTypeResource deleteQualificationTypeResource(
        de.humance.eco.profile.model.QualificationTypeResource qualificationTypeResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.deleteQualificationTypeResource(qualificationTypeResource);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _qualificationTypeResourceLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.dynamicQuery(dynamicQuery,
            start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.dynamicQuery(dynamicQuery,
            start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public de.humance.eco.profile.model.QualificationTypeResource fetchQualificationTypeResource(
        long qualificationTypeResourceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.fetchQualificationTypeResource(qualificationTypeResourceId);
    }

    /**
    * Returns the qualification type resource with the primary key.
    *
    * @param qualificationTypeResourceId the primary key of the qualification type resource
    * @return the qualification type resource
    * @throws PortalException if a qualification type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationTypeResource getQualificationTypeResource(
        long qualificationTypeResourceId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.getQualificationTypeResource(qualificationTypeResourceId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the qualification type resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @return the range of qualification type resources
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> getQualificationTypeResources(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.getQualificationTypeResources(start,
            end);
    }

    /**
    * Returns the number of qualification type resources.
    *
    * @return the number of qualification type resources
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getQualificationTypeResourcesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.getQualificationTypeResourcesCount();
    }

    /**
    * Updates the qualification type resource in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param qualificationTypeResource the qualification type resource
    * @return the qualification type resource that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.QualificationTypeResource updateQualificationTypeResource(
        de.humance.eco.profile.model.QualificationTypeResource qualificationTypeResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.updateQualificationTypeResource(qualificationTypeResource);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _qualificationTypeResourceLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _qualificationTypeResourceLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _qualificationTypeResourceLocalService.invokeMethod(name,
            parameterTypes, arguments);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByTypeId(
        long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.findByTypeId(qualificationTypeId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByTypeIdAndCountry(
        long qualificationTypeId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.findByTypeIdAndCountry(qualificationTypeId,
            country);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByTypeIdAndLanguage(
        long qualificationTypeId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.findByTypeIdAndLanguage(qualificationTypeId,
            language);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByTypeIdAndLocale(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.findByTypeIdAndLocale(qualificationTypeId,
            country, language);
    }

    @Override
    public de.humance.eco.profile.model.QualificationTypeResource addQualificationTypeResource(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language, java.lang.String name, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.addQualificationTypeResource(qualificationTypeId,
            country, language, name, iconId);
    }

    @Override
    public de.humance.eco.profile.model.QualificationTypeResource updateQualificationTypeResource(
        long qualificationTypeResourceId, long qualificationTypeId,
        java.lang.String country, java.lang.String language,
        java.lang.String name, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _qualificationTypeResourceLocalService.updateQualificationTypeResource(qualificationTypeResourceId,
            qualificationTypeId, country, language, name, iconId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public QualificationTypeResourceLocalService getWrappedQualificationTypeResourceLocalService() {
        return _qualificationTypeResourceLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedQualificationTypeResourceLocalService(
        QualificationTypeResourceLocalService qualificationTypeResourceLocalService) {
        _qualificationTypeResourceLocalService = qualificationTypeResourceLocalService;
    }

    @Override
    public QualificationTypeResourceLocalService getWrappedService() {
        return _qualificationTypeResourceLocalService;
    }

    @Override
    public void setWrappedService(
        QualificationTypeResourceLocalService qualificationTypeResourceLocalService) {
        _qualificationTypeResourceLocalService = qualificationTypeResourceLocalService;
    }
}
