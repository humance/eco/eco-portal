package de.humance.eco.profile.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Trailer. This utility wraps
 * {@link de.humance.eco.profile.service.impl.TrailerLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Humance
 * @see TrailerLocalService
 * @see de.humance.eco.profile.service.base.TrailerLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.impl.TrailerLocalServiceImpl
 * @generated
 */
public class TrailerLocalServiceUtil {
    private static TrailerLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link de.humance.eco.profile.service.impl.TrailerLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the trailer to the database. Also notifies the appropriate model listeners.
    *
    * @param trailer the trailer
    * @return the trailer that was added
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer addTrailer(
        de.humance.eco.profile.model.Trailer trailer)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addTrailer(trailer);
    }

    /**
    * Creates a new trailer with the primary key. Does not add the trailer to the database.
    *
    * @param trailerId the primary key for the new trailer
    * @return the new trailer
    */
    public static de.humance.eco.profile.model.Trailer createTrailer(
        long trailerId) {
        return getService().createTrailer(trailerId);
    }

    /**
    * Deletes the trailer with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param trailerId the primary key of the trailer
    * @return the trailer that was removed
    * @throws PortalException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer deleteTrailer(
        long trailerId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteTrailer(trailerId);
    }

    /**
    * Deletes the trailer from the database. Also notifies the appropriate model listeners.
    *
    * @param trailer the trailer
    * @return the trailer that was removed
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer deleteTrailer(
        de.humance.eco.profile.model.Trailer trailer)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteTrailer(trailer);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static de.humance.eco.profile.model.Trailer fetchTrailer(
        long trailerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchTrailer(trailerId);
    }

    /**
    * Returns the trailer with the primary key.
    *
    * @param trailerId the primary key of the trailer
    * @return the trailer
    * @throws PortalException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer getTrailer(
        long trailerId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getTrailer(trailerId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the trailers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> getTrailers(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getTrailers(start, end);
    }

    /**
    * Returns the number of trailers.
    *
    * @return the number of trailers
    * @throws SystemException if a system exception occurred
    */
    public static int getTrailersCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getTrailersCount();
    }

    /**
    * Updates the trailer in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param trailer the trailer
    * @return the trailer that was updated
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer updateTrailer(
        de.humance.eco.profile.model.Trailer trailer)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateTrailer(trailer);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<de.humance.eco.profile.model.Trailer> findByDriverId(
        long driverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByDriverId(driverId);
    }

    public static java.util.List<de.humance.eco.profile.model.Trailer> findByDriverIdAndOrgaId(
        long driverId, long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByDriverIdAndOrgaId(driverId, organizationId);
    }

    public static java.util.List<de.humance.eco.profile.model.Trailer> findByManufacturerId(
        long manufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByManufacturerId(manufacturerId);
    }

    public static java.util.List<de.humance.eco.profile.model.Trailer> findByModelName(
        java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByModelName(modelName);
    }

    public static java.util.List<de.humance.eco.profile.model.Trailer> findByModelNameLike(
        java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByModelNameLike(modelName);
    }

    public static java.util.List<de.humance.eco.profile.model.Trailer> findByTypeId(
        long typeId) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByTypeId(typeId);
    }

    public static java.util.List<de.humance.eco.profile.model.Trailer> findByDriverIdAndTypeId(
        long driverId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByDriverIdAndTypeId(driverId, typeId);
    }

    public static java.util.List<de.humance.eco.profile.model.Trailer> findByLicensePlate(
        java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByLicensePlate(licensePlate);
    }

    public static de.humance.eco.profile.model.Trailer addTrailer(
        long creatorId, java.lang.String creatorName, long driverId,
        long typeId, long manufacturerId, java.lang.String modelName,
        java.lang.String licensePlate, long dimensionHeight,
        long dimensionWidth, long dimensionDepth, long organizationId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addTrailer(creatorId, creatorName, driverId, typeId,
            manufacturerId, modelName, licensePlate, dimensionHeight,
            dimensionWidth, dimensionDepth, organizationId);
    }

    public static de.humance.eco.profile.model.Trailer updateTrailer(
        long trailerId, long modifierId, java.lang.String modifierName,
        long driverId, long typeId, long manufacturerId,
        java.lang.String modelName, java.lang.String licensePlate,
        long dimensionHeight, long dimensionWidth, long dimensionDepth,
        long organizationId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateTrailer(trailerId, modifierId, modifierName,
            driverId, typeId, manufacturerId, modelName, licensePlate,
            dimensionHeight, dimensionWidth, dimensionDepth, organizationId);
    }

    public static void clearService() {
        _service = null;
    }

    public static TrailerLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    TrailerLocalService.class.getName());

            if (invokableLocalService instanceof TrailerLocalService) {
                _service = (TrailerLocalService) invokableLocalService;
            } else {
                _service = new TrailerLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(TrailerLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(TrailerLocalService service) {
    }
}
