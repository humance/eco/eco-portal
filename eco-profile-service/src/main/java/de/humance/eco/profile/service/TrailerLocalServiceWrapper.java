package de.humance.eco.profile.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link TrailerLocalService}.
 *
 * @author Humance
 * @see TrailerLocalService
 * @generated
 */
public class TrailerLocalServiceWrapper implements TrailerLocalService,
    ServiceWrapper<TrailerLocalService> {
    private TrailerLocalService _trailerLocalService;

    public TrailerLocalServiceWrapper(TrailerLocalService trailerLocalService) {
        _trailerLocalService = trailerLocalService;
    }

    /**
    * Adds the trailer to the database. Also notifies the appropriate model listeners.
    *
    * @param trailer the trailer
    * @return the trailer that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.Trailer addTrailer(
        de.humance.eco.profile.model.Trailer trailer)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.addTrailer(trailer);
    }

    /**
    * Creates a new trailer with the primary key. Does not add the trailer to the database.
    *
    * @param trailerId the primary key for the new trailer
    * @return the new trailer
    */
    @Override
    public de.humance.eco.profile.model.Trailer createTrailer(long trailerId) {
        return _trailerLocalService.createTrailer(trailerId);
    }

    /**
    * Deletes the trailer with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param trailerId the primary key of the trailer
    * @return the trailer that was removed
    * @throws PortalException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.Trailer deleteTrailer(long trailerId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.deleteTrailer(trailerId);
    }

    /**
    * Deletes the trailer from the database. Also notifies the appropriate model listeners.
    *
    * @param trailer the trailer
    * @return the trailer that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.Trailer deleteTrailer(
        de.humance.eco.profile.model.Trailer trailer)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.deleteTrailer(trailer);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _trailerLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public de.humance.eco.profile.model.Trailer fetchTrailer(long trailerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.fetchTrailer(trailerId);
    }

    /**
    * Returns the trailer with the primary key.
    *
    * @param trailerId the primary key of the trailer
    * @return the trailer
    * @throws PortalException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.Trailer getTrailer(long trailerId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.getTrailer(trailerId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the trailers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of trailers
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<de.humance.eco.profile.model.Trailer> getTrailers(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.getTrailers(start, end);
    }

    /**
    * Returns the number of trailers.
    *
    * @return the number of trailers
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getTrailersCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.getTrailersCount();
    }

    /**
    * Updates the trailer in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param trailer the trailer
    * @return the trailer that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.Trailer updateTrailer(
        de.humance.eco.profile.model.Trailer trailer)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.updateTrailer(trailer);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _trailerLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _trailerLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _trailerLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Trailer> findByDriverId(
        long driverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.findByDriverId(driverId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Trailer> findByDriverIdAndOrgaId(
        long driverId, long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.findByDriverIdAndOrgaId(driverId,
            organizationId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Trailer> findByManufacturerId(
        long manufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.findByManufacturerId(manufacturerId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Trailer> findByModelName(
        java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.findByModelName(modelName);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Trailer> findByModelNameLike(
        java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.findByModelNameLike(modelName);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Trailer> findByTypeId(
        long typeId) throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.findByTypeId(typeId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Trailer> findByDriverIdAndTypeId(
        long driverId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.findByDriverIdAndTypeId(driverId, typeId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Trailer> findByLicensePlate(
        java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.findByLicensePlate(licensePlate);
    }

    @Override
    public de.humance.eco.profile.model.Trailer addTrailer(long creatorId,
        java.lang.String creatorName, long driverId, long typeId,
        long manufacturerId, java.lang.String modelName,
        java.lang.String licensePlate, long dimensionHeight,
        long dimensionWidth, long dimensionDepth, long organizationId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.addTrailer(creatorId, creatorName,
            driverId, typeId, manufacturerId, modelName, licensePlate,
            dimensionHeight, dimensionWidth, dimensionDepth, organizationId);
    }

    @Override
    public de.humance.eco.profile.model.Trailer updateTrailer(long trailerId,
        long modifierId, java.lang.String modifierName, long driverId,
        long typeId, long manufacturerId, java.lang.String modelName,
        java.lang.String licensePlate, long dimensionHeight,
        long dimensionWidth, long dimensionDepth, long organizationId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _trailerLocalService.updateTrailer(trailerId, modifierId,
            modifierName, driverId, typeId, manufacturerId, modelName,
            licensePlate, dimensionHeight, dimensionWidth, dimensionDepth,
            organizationId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public TrailerLocalService getWrappedTrailerLocalService() {
        return _trailerLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedTrailerLocalService(
        TrailerLocalService trailerLocalService) {
        _trailerLocalService = trailerLocalService;
    }

    @Override
    public TrailerLocalService getWrappedService() {
        return _trailerLocalService;
    }

    @Override
    public void setWrappedService(TrailerLocalService trailerLocalService) {
        _trailerLocalService = trailerLocalService;
    }
}
