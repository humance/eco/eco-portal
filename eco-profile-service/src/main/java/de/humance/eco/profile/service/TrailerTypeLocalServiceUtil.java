package de.humance.eco.profile.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for TrailerType. This utility wraps
 * {@link de.humance.eco.profile.service.impl.TrailerTypeLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Humance
 * @see TrailerTypeLocalService
 * @see de.humance.eco.profile.service.base.TrailerTypeLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.impl.TrailerTypeLocalServiceImpl
 * @generated
 */
public class TrailerTypeLocalServiceUtil {
    private static TrailerTypeLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link de.humance.eco.profile.service.impl.TrailerTypeLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the trailer type to the database. Also notifies the appropriate model listeners.
    *
    * @param trailerType the trailer type
    * @return the trailer type that was added
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType addTrailerType(
        de.humance.eco.profile.model.TrailerType trailerType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addTrailerType(trailerType);
    }

    /**
    * Creates a new trailer type with the primary key. Does not add the trailer type to the database.
    *
    * @param trailerTypeId the primary key for the new trailer type
    * @return the new trailer type
    */
    public static de.humance.eco.profile.model.TrailerType createTrailerType(
        long trailerTypeId) {
        return getService().createTrailerType(trailerTypeId);
    }

    /**
    * Deletes the trailer type with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param trailerTypeId the primary key of the trailer type
    * @return the trailer type that was removed
    * @throws PortalException if a trailer type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType deleteTrailerType(
        long trailerTypeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteTrailerType(trailerTypeId);
    }

    /**
    * Deletes the trailer type from the database. Also notifies the appropriate model listeners.
    *
    * @param trailerType the trailer type
    * @return the trailer type that was removed
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType deleteTrailerType(
        de.humance.eco.profile.model.TrailerType trailerType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteTrailerType(trailerType);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static de.humance.eco.profile.model.TrailerType fetchTrailerType(
        long trailerTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchTrailerType(trailerTypeId);
    }

    /**
    * Returns the trailer type with the primary key.
    *
    * @param trailerTypeId the primary key of the trailer type
    * @return the trailer type
    * @throws PortalException if a trailer type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType getTrailerType(
        long trailerTypeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getTrailerType(trailerTypeId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the trailer types.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of trailer types
    * @param end the upper bound of the range of trailer types (not inclusive)
    * @return the range of trailer types
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerType> getTrailerTypes(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getTrailerTypes(start, end);
    }

    /**
    * Returns the number of trailer types.
    *
    * @return the number of trailer types
    * @throws SystemException if a system exception occurred
    */
    public static int getTrailerTypesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getTrailerTypesCount();
    }

    /**
    * Updates the trailer type in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param trailerType the trailer type
    * @return the trailer type that was updated
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType updateTrailerType(
        de.humance.eco.profile.model.TrailerType trailerType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateTrailerType(trailerType);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<de.humance.eco.profile.model.TrailerType> findByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByName(name);
    }

    public static java.util.List<de.humance.eco.profile.model.TrailerType> findByNameLike(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByNameLike(name);
    }

    public static java.util.List<de.humance.eco.profile.model.TrailerType> findByModifiedDate(
        java.util.Date modifiedDate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByModifiedDate(modifiedDate);
    }

    public static de.humance.eco.profile.model.TrailerType addTrailerType(
        long creatorId, java.lang.String name, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().addTrailerType(creatorId, name, iconId);
    }

    public static de.humance.eco.profile.model.TrailerType updateTrailerType(
        long modifierId, long trailerTypeId, java.lang.String name, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateTrailerType(modifierId, trailerTypeId, name, iconId);
    }

    public static void clearService() {
        _service = null;
    }

    public static TrailerTypeLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    TrailerTypeLocalService.class.getName());

            if (invokableLocalService instanceof TrailerTypeLocalService) {
                _service = (TrailerTypeLocalService) invokableLocalService;
            } else {
                _service = new TrailerTypeLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(TrailerTypeLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(TrailerTypeLocalService service) {
    }
}
