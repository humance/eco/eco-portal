package de.humance.eco.profile.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link TrailerTypeLocalService}.
 *
 * @author Humance
 * @see TrailerTypeLocalService
 * @generated
 */
public class TrailerTypeLocalServiceWrapper implements TrailerTypeLocalService,
    ServiceWrapper<TrailerTypeLocalService> {
    private TrailerTypeLocalService _trailerTypeLocalService;

    public TrailerTypeLocalServiceWrapper(
        TrailerTypeLocalService trailerTypeLocalService) {
        _trailerTypeLocalService = trailerTypeLocalService;
    }

    /**
    * Adds the trailer type to the database. Also notifies the appropriate model listeners.
    *
    * @param trailerType the trailer type
    * @return the trailer type that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.TrailerType addTrailerType(
        de.humance.eco.profile.model.TrailerType trailerType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.addTrailerType(trailerType);
    }

    /**
    * Creates a new trailer type with the primary key. Does not add the trailer type to the database.
    *
    * @param trailerTypeId the primary key for the new trailer type
    * @return the new trailer type
    */
    @Override
    public de.humance.eco.profile.model.TrailerType createTrailerType(
        long trailerTypeId) {
        return _trailerTypeLocalService.createTrailerType(trailerTypeId);
    }

    /**
    * Deletes the trailer type with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param trailerTypeId the primary key of the trailer type
    * @return the trailer type that was removed
    * @throws PortalException if a trailer type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.TrailerType deleteTrailerType(
        long trailerTypeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.deleteTrailerType(trailerTypeId);
    }

    /**
    * Deletes the trailer type from the database. Also notifies the appropriate model listeners.
    *
    * @param trailerType the trailer type
    * @return the trailer type that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.TrailerType deleteTrailerType(
        de.humance.eco.profile.model.TrailerType trailerType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.deleteTrailerType(trailerType);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _trailerTypeLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public de.humance.eco.profile.model.TrailerType fetchTrailerType(
        long trailerTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.fetchTrailerType(trailerTypeId);
    }

    /**
    * Returns the trailer type with the primary key.
    *
    * @param trailerTypeId the primary key of the trailer type
    * @return the trailer type
    * @throws PortalException if a trailer type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.TrailerType getTrailerType(
        long trailerTypeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.getTrailerType(trailerTypeId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the trailer types.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of trailer types
    * @param end the upper bound of the range of trailer types (not inclusive)
    * @return the range of trailer types
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<de.humance.eco.profile.model.TrailerType> getTrailerTypes(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.getTrailerTypes(start, end);
    }

    /**
    * Returns the number of trailer types.
    *
    * @return the number of trailer types
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getTrailerTypesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.getTrailerTypesCount();
    }

    /**
    * Updates the trailer type in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param trailerType the trailer type
    * @return the trailer type that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.TrailerType updateTrailerType(
        de.humance.eco.profile.model.TrailerType trailerType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.updateTrailerType(trailerType);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _trailerTypeLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _trailerTypeLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _trailerTypeLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.TrailerType> findByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.findByName(name);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.TrailerType> findByNameLike(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.findByNameLike(name);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.TrailerType> findByModifiedDate(
        java.util.Date modifiedDate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.findByModifiedDate(modifiedDate);
    }

    @Override
    public de.humance.eco.profile.model.TrailerType addTrailerType(
        long creatorId, java.lang.String name, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.addTrailerType(creatorId, name, iconId);
    }

    @Override
    public de.humance.eco.profile.model.TrailerType updateTrailerType(
        long modifierId, long trailerTypeId, java.lang.String name, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _trailerTypeLocalService.updateTrailerType(modifierId,
            trailerTypeId, name, iconId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public TrailerTypeLocalService getWrappedTrailerTypeLocalService() {
        return _trailerTypeLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedTrailerTypeLocalService(
        TrailerTypeLocalService trailerTypeLocalService) {
        _trailerTypeLocalService = trailerTypeLocalService;
    }

    @Override
    public TrailerTypeLocalService getWrappedService() {
        return _trailerTypeLocalService;
    }

    @Override
    public void setWrappedService(
        TrailerTypeLocalService trailerTypeLocalService) {
        _trailerTypeLocalService = trailerTypeLocalService;
    }
}
