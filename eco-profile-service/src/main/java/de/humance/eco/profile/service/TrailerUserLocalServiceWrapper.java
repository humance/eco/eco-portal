package de.humance.eco.profile.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link TrailerUserLocalService}.
 *
 * @author Humance
 * @see TrailerUserLocalService
 * @generated
 */
public class TrailerUserLocalServiceWrapper implements TrailerUserLocalService,
    ServiceWrapper<TrailerUserLocalService> {
    private TrailerUserLocalService _trailerUserLocalService;

    public TrailerUserLocalServiceWrapper(
        TrailerUserLocalService trailerUserLocalService) {
        _trailerUserLocalService = trailerUserLocalService;
    }

    /**
    * Adds the trailer user to the database. Also notifies the appropriate model listeners.
    *
    * @param trailerUser the trailer user
    * @return the trailer user that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.TrailerUser addTrailerUser(
        de.humance.eco.profile.model.TrailerUser trailerUser)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerUserLocalService.addTrailerUser(trailerUser);
    }

    /**
    * Creates a new trailer user with the primary key. Does not add the trailer user to the database.
    *
    * @param trailerUserId the primary key for the new trailer user
    * @return the new trailer user
    */
    @Override
    public de.humance.eco.profile.model.TrailerUser createTrailerUser(
        long trailerUserId) {
        return _trailerUserLocalService.createTrailerUser(trailerUserId);
    }

    /**
    * Deletes the trailer user with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param trailerUserId the primary key of the trailer user
    * @return the trailer user that was removed
    * @throws PortalException if a trailer user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.TrailerUser deleteTrailerUser(
        long trailerUserId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _trailerUserLocalService.deleteTrailerUser(trailerUserId);
    }

    /**
    * Deletes the trailer user from the database. Also notifies the appropriate model listeners.
    *
    * @param trailerUser the trailer user
    * @return the trailer user that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.TrailerUser deleteTrailerUser(
        de.humance.eco.profile.model.TrailerUser trailerUser)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerUserLocalService.deleteTrailerUser(trailerUser);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _trailerUserLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerUserLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerUserLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerUserLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerUserLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerUserLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public de.humance.eco.profile.model.TrailerUser fetchTrailerUser(
        long trailerUserId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerUserLocalService.fetchTrailerUser(trailerUserId);
    }

    /**
    * Returns the trailer user with the primary key.
    *
    * @param trailerUserId the primary key of the trailer user
    * @return the trailer user
    * @throws PortalException if a trailer user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.TrailerUser getTrailerUser(
        long trailerUserId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _trailerUserLocalService.getTrailerUser(trailerUserId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _trailerUserLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the trailer users.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of trailer users
    * @param end the upper bound of the range of trailer users (not inclusive)
    * @return the range of trailer users
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<de.humance.eco.profile.model.TrailerUser> getTrailerUsers(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerUserLocalService.getTrailerUsers(start, end);
    }

    /**
    * Returns the number of trailer users.
    *
    * @return the number of trailer users
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getTrailerUsersCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerUserLocalService.getTrailerUsersCount();
    }

    /**
    * Updates the trailer user in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param trailerUser the trailer user
    * @return the trailer user that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.TrailerUser updateTrailerUser(
        de.humance.eco.profile.model.TrailerUser trailerUser)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerUserLocalService.updateTrailerUser(trailerUser);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _trailerUserLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _trailerUserLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _trailerUserLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.TrailerUser> findByUserId(
        long userId) throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerUserLocalService.findByUserId(userId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.TrailerUser> findByTrailerId(
        long trailerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trailerUserLocalService.findByTrailerId(trailerId);
    }

    @Override
    public de.humance.eco.profile.model.TrailerUser addTrailerUser(
        long userId, long trailerId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _trailerUserLocalService.addTrailerUser(userId, trailerId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public TrailerUserLocalService getWrappedTrailerUserLocalService() {
        return _trailerUserLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedTrailerUserLocalService(
        TrailerUserLocalService trailerUserLocalService) {
        _trailerUserLocalService = trailerUserLocalService;
    }

    @Override
    public TrailerUserLocalService getWrappedService() {
        return _trailerUserLocalService;
    }

    @Override
    public void setWrappedService(
        TrailerUserLocalService trailerUserLocalService) {
        _trailerUserLocalService = trailerUserLocalService;
    }
}
