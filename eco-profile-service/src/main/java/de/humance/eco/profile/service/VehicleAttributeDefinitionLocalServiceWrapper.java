package de.humance.eco.profile.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VehicleAttributeDefinitionLocalService}.
 *
 * @author Humance
 * @see VehicleAttributeDefinitionLocalService
 * @generated
 */
public class VehicleAttributeDefinitionLocalServiceWrapper
    implements VehicleAttributeDefinitionLocalService,
        ServiceWrapper<VehicleAttributeDefinitionLocalService> {
    private VehicleAttributeDefinitionLocalService _vehicleAttributeDefinitionLocalService;

    public VehicleAttributeDefinitionLocalServiceWrapper(
        VehicleAttributeDefinitionLocalService vehicleAttributeDefinitionLocalService) {
        _vehicleAttributeDefinitionLocalService = vehicleAttributeDefinitionLocalService;
    }

    /**
    * Adds the vehicle attribute definition to the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleAttributeDefinition the vehicle attribute definition
    * @return the vehicle attribute definition that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleAttributeDefinition addVehicleAttributeDefinition(
        de.humance.eco.profile.model.VehicleAttributeDefinition vehicleAttributeDefinition)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.addVehicleAttributeDefinition(vehicleAttributeDefinition);
    }

    /**
    * Creates a new vehicle attribute definition with the primary key. Does not add the vehicle attribute definition to the database.
    *
    * @param vehicleAttributeDefinitionId the primary key for the new vehicle attribute definition
    * @return the new vehicle attribute definition
    */
    @Override
    public de.humance.eco.profile.model.VehicleAttributeDefinition createVehicleAttributeDefinition(
        long vehicleAttributeDefinitionId) {
        return _vehicleAttributeDefinitionLocalService.createVehicleAttributeDefinition(vehicleAttributeDefinitionId);
    }

    /**
    * Deletes the vehicle attribute definition with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleAttributeDefinitionId the primary key of the vehicle attribute definition
    * @return the vehicle attribute definition that was removed
    * @throws PortalException if a vehicle attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleAttributeDefinition deleteVehicleAttributeDefinition(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.deleteVehicleAttributeDefinition(vehicleAttributeDefinitionId);
    }

    /**
    * Deletes the vehicle attribute definition from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleAttributeDefinition the vehicle attribute definition
    * @return the vehicle attribute definition that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleAttributeDefinition deleteVehicleAttributeDefinition(
        de.humance.eco.profile.model.VehicleAttributeDefinition vehicleAttributeDefinition)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.deleteVehicleAttributeDefinition(vehicleAttributeDefinition);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _vehicleAttributeDefinitionLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.dynamicQuery(dynamicQuery,
            start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.dynamicQuery(dynamicQuery,
            start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public de.humance.eco.profile.model.VehicleAttributeDefinition fetchVehicleAttributeDefinition(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.fetchVehicleAttributeDefinition(vehicleAttributeDefinitionId);
    }

    /**
    * Returns the vehicle attribute definition with the primary key.
    *
    * @param vehicleAttributeDefinitionId the primary key of the vehicle attribute definition
    * @return the vehicle attribute definition
    * @throws PortalException if a vehicle attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleAttributeDefinition getVehicleAttributeDefinition(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.getVehicleAttributeDefinition(vehicleAttributeDefinitionId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the vehicle attribute definitions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle attribute definitions
    * @param end the upper bound of the range of vehicle attribute definitions (not inclusive)
    * @return the range of vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> getVehicleAttributeDefinitions(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.getVehicleAttributeDefinitions(start,
            end);
    }

    /**
    * Returns the number of vehicle attribute definitions.
    *
    * @return the number of vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getVehicleAttributeDefinitionsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.getVehicleAttributeDefinitionsCount();
    }

    /**
    * Updates the vehicle attribute definition in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param vehicleAttributeDefinition the vehicle attribute definition
    * @return the vehicle attribute definition that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleAttributeDefinition updateVehicleAttributeDefinition(
        de.humance.eco.profile.model.VehicleAttributeDefinition vehicleAttributeDefinition)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.updateVehicleAttributeDefinition(vehicleAttributeDefinition);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _vehicleAttributeDefinitionLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _vehicleAttributeDefinitionLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _vehicleAttributeDefinitionLocalService.invokeMethod(name,
            parameterTypes, arguments);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> findByTitle(
        java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.findByTitle(title);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> findByTitleLike(
        java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.findByTitleLike(title);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> findByVehicleTypeId(
        long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.findByVehicleTypeId(vehicleTypeId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> findByModifiedDate(
        java.util.Date modifiedDate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.findByModifiedDate(modifiedDate);
    }

    @Override
    public de.humance.eco.profile.model.VehicleAttributeDefinition addVehicleAttributeDefinition(
        long creatorId, long vehicleTypeId, java.lang.String type,
        java.lang.String title, double minRangeValue, double maxRangeValue,
        java.lang.String unit, long sequenceNumber)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.addVehicleAttributeDefinition(creatorId,
            vehicleTypeId, type, title, minRangeValue, maxRangeValue, unit,
            sequenceNumber);
    }

    @Override
    public de.humance.eco.profile.model.VehicleAttributeDefinition updateVehicleAttributeDefinition(
        long modifierId, long vehicleAttributeDefinitionId, long vehicleTypeId,
        java.lang.String type, java.lang.String title, double minRangeValue,
        double maxRangeValue, java.lang.String unit, long sequenceNumber)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeDefinitionLocalService.updateVehicleAttributeDefinition(modifierId,
            vehicleAttributeDefinitionId, vehicleTypeId, type, title,
            minRangeValue, maxRangeValue, unit, sequenceNumber);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public VehicleAttributeDefinitionLocalService getWrappedVehicleAttributeDefinitionLocalService() {
        return _vehicleAttributeDefinitionLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedVehicleAttributeDefinitionLocalService(
        VehicleAttributeDefinitionLocalService vehicleAttributeDefinitionLocalService) {
        _vehicleAttributeDefinitionLocalService = vehicleAttributeDefinitionLocalService;
    }

    @Override
    public VehicleAttributeDefinitionLocalService getWrappedService() {
        return _vehicleAttributeDefinitionLocalService;
    }

    @Override
    public void setWrappedService(
        VehicleAttributeDefinitionLocalService vehicleAttributeDefinitionLocalService) {
        _vehicleAttributeDefinitionLocalService = vehicleAttributeDefinitionLocalService;
    }
}
