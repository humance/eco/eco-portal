package de.humance.eco.profile.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for VehicleAttribute. This utility wraps
 * {@link de.humance.eco.profile.service.impl.VehicleAttributeLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Humance
 * @see VehicleAttributeLocalService
 * @see de.humance.eco.profile.service.base.VehicleAttributeLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.impl.VehicleAttributeLocalServiceImpl
 * @generated
 */
public class VehicleAttributeLocalServiceUtil {
    private static VehicleAttributeLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link de.humance.eco.profile.service.impl.VehicleAttributeLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the vehicle attribute to the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleAttribute the vehicle attribute
    * @return the vehicle attribute that was added
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute addVehicleAttribute(
        de.humance.eco.profile.model.VehicleAttribute vehicleAttribute)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addVehicleAttribute(vehicleAttribute);
    }

    /**
    * Creates a new vehicle attribute with the primary key. Does not add the vehicle attribute to the database.
    *
    * @param vehicleAttributeId the primary key for the new vehicle attribute
    * @return the new vehicle attribute
    */
    public static de.humance.eco.profile.model.VehicleAttribute createVehicleAttribute(
        long vehicleAttributeId) {
        return getService().createVehicleAttribute(vehicleAttributeId);
    }

    /**
    * Deletes the vehicle attribute with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleAttributeId the primary key of the vehicle attribute
    * @return the vehicle attribute that was removed
    * @throws PortalException if a vehicle attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute deleteVehicleAttribute(
        long vehicleAttributeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteVehicleAttribute(vehicleAttributeId);
    }

    /**
    * Deletes the vehicle attribute from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleAttribute the vehicle attribute
    * @return the vehicle attribute that was removed
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute deleteVehicleAttribute(
        de.humance.eco.profile.model.VehicleAttribute vehicleAttribute)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteVehicleAttribute(vehicleAttribute);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static de.humance.eco.profile.model.VehicleAttribute fetchVehicleAttribute(
        long vehicleAttributeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchVehicleAttribute(vehicleAttributeId);
    }

    /**
    * Returns the vehicle attribute with the primary key.
    *
    * @param vehicleAttributeId the primary key of the vehicle attribute
    * @return the vehicle attribute
    * @throws PortalException if a vehicle attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute getVehicleAttribute(
        long vehicleAttributeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicleAttribute(vehicleAttributeId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the vehicle attributes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @return the range of vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttribute> getVehicleAttributes(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicleAttributes(start, end);
    }

    /**
    * Returns the number of vehicle attributes.
    *
    * @return the number of vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static int getVehicleAttributesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicleAttributesCount();
    }

    /**
    * Updates the vehicle attribute in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param vehicleAttribute the vehicle attribute
    * @return the vehicle attribute that was updated
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute updateVehicleAttribute(
        de.humance.eco.profile.model.VehicleAttribute vehicleAttribute)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateVehicleAttribute(vehicleAttribute);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleIdAndAttributeDefinitionId(
        long vehicleId, long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .findByVehicleIdAndAttributeDefinitionId(vehicleId,
            vehicleAttributeDefinitionId);
    }

    public static java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByAttributeDefinitionId(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .findByAttributeDefinitionId(vehicleAttributeDefinitionId);
    }

    public static java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleId(
        long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByVehicleId(vehicleId);
    }

    public static de.humance.eco.profile.model.VehicleAttribute addVehicleAttribute(
        long vehicleId, long vehicleAttributeDefinitionId,
        java.lang.String type, long sequenceNumber,
        java.lang.String attributeValue)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addVehicleAttribute(vehicleId,
            vehicleAttributeDefinitionId, type, sequenceNumber, attributeValue);
    }

    public static de.humance.eco.profile.model.VehicleAttribute updateVehicleAttribute(
        long vehicleAttributeId, long vehicleId,
        long vehicleAttributeDefinitionId, java.lang.String type,
        long sequenceNumber, java.lang.String attributeValue)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateVehicleAttribute(vehicleAttributeId, vehicleId,
            vehicleAttributeDefinitionId, type, sequenceNumber, attributeValue);
    }

    public static void clearService() {
        _service = null;
    }

    public static VehicleAttributeLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    VehicleAttributeLocalService.class.getName());

            if (invokableLocalService instanceof VehicleAttributeLocalService) {
                _service = (VehicleAttributeLocalService) invokableLocalService;
            } else {
                _service = new VehicleAttributeLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(VehicleAttributeLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(VehicleAttributeLocalService service) {
    }
}
