package de.humance.eco.profile.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VehicleAttributeLocalService}.
 *
 * @author Humance
 * @see VehicleAttributeLocalService
 * @generated
 */
public class VehicleAttributeLocalServiceWrapper
    implements VehicleAttributeLocalService,
        ServiceWrapper<VehicleAttributeLocalService> {
    private VehicleAttributeLocalService _vehicleAttributeLocalService;

    public VehicleAttributeLocalServiceWrapper(
        VehicleAttributeLocalService vehicleAttributeLocalService) {
        _vehicleAttributeLocalService = vehicleAttributeLocalService;
    }

    /**
    * Adds the vehicle attribute to the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleAttribute the vehicle attribute
    * @return the vehicle attribute that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleAttribute addVehicleAttribute(
        de.humance.eco.profile.model.VehicleAttribute vehicleAttribute)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.addVehicleAttribute(vehicleAttribute);
    }

    /**
    * Creates a new vehicle attribute with the primary key. Does not add the vehicle attribute to the database.
    *
    * @param vehicleAttributeId the primary key for the new vehicle attribute
    * @return the new vehicle attribute
    */
    @Override
    public de.humance.eco.profile.model.VehicleAttribute createVehicleAttribute(
        long vehicleAttributeId) {
        return _vehicleAttributeLocalService.createVehicleAttribute(vehicleAttributeId);
    }

    /**
    * Deletes the vehicle attribute with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleAttributeId the primary key of the vehicle attribute
    * @return the vehicle attribute that was removed
    * @throws PortalException if a vehicle attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleAttribute deleteVehicleAttribute(
        long vehicleAttributeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.deleteVehicleAttribute(vehicleAttributeId);
    }

    /**
    * Deletes the vehicle attribute from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleAttribute the vehicle attribute
    * @return the vehicle attribute that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleAttribute deleteVehicleAttribute(
        de.humance.eco.profile.model.VehicleAttribute vehicleAttribute)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.deleteVehicleAttribute(vehicleAttribute);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _vehicleAttributeLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.dynamicQuery(dynamicQuery, start,
            end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public de.humance.eco.profile.model.VehicleAttribute fetchVehicleAttribute(
        long vehicleAttributeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.fetchVehicleAttribute(vehicleAttributeId);
    }

    /**
    * Returns the vehicle attribute with the primary key.
    *
    * @param vehicleAttributeId the primary key of the vehicle attribute
    * @return the vehicle attribute
    * @throws PortalException if a vehicle attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleAttribute getVehicleAttribute(
        long vehicleAttributeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.getVehicleAttribute(vehicleAttributeId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the vehicle attributes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @return the range of vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleAttribute> getVehicleAttributes(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.getVehicleAttributes(start, end);
    }

    /**
    * Returns the number of vehicle attributes.
    *
    * @return the number of vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getVehicleAttributesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.getVehicleAttributesCount();
    }

    /**
    * Updates the vehicle attribute in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param vehicleAttribute the vehicle attribute
    * @return the vehicle attribute that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleAttribute updateVehicleAttribute(
        de.humance.eco.profile.model.VehicleAttribute vehicleAttribute)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.updateVehicleAttribute(vehicleAttribute);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _vehicleAttributeLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _vehicleAttributeLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _vehicleAttributeLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleIdAndAttributeDefinitionId(
        long vehicleId, long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.findByVehicleIdAndAttributeDefinitionId(vehicleId,
            vehicleAttributeDefinitionId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByAttributeDefinitionId(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.findByAttributeDefinitionId(vehicleAttributeDefinitionId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleId(
        long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.findByVehicleId(vehicleId);
    }

    @Override
    public de.humance.eco.profile.model.VehicleAttribute addVehicleAttribute(
        long vehicleId, long vehicleAttributeDefinitionId,
        java.lang.String type, long sequenceNumber,
        java.lang.String attributeValue)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.addVehicleAttribute(vehicleId,
            vehicleAttributeDefinitionId, type, sequenceNumber, attributeValue);
    }

    @Override
    public de.humance.eco.profile.model.VehicleAttribute updateVehicleAttribute(
        long vehicleAttributeId, long vehicleId,
        long vehicleAttributeDefinitionId, java.lang.String type,
        long sequenceNumber, java.lang.String attributeValue)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleAttributeLocalService.updateVehicleAttribute(vehicleAttributeId,
            vehicleId, vehicleAttributeDefinitionId, type, sequenceNumber,
            attributeValue);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public VehicleAttributeLocalService getWrappedVehicleAttributeLocalService() {
        return _vehicleAttributeLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedVehicleAttributeLocalService(
        VehicleAttributeLocalService vehicleAttributeLocalService) {
        _vehicleAttributeLocalService = vehicleAttributeLocalService;
    }

    @Override
    public VehicleAttributeLocalService getWrappedService() {
        return _vehicleAttributeLocalService;
    }

    @Override
    public void setWrappedService(
        VehicleAttributeLocalService vehicleAttributeLocalService) {
        _vehicleAttributeLocalService = vehicleAttributeLocalService;
    }
}
