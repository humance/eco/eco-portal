package de.humance.eco.profile.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VehicleLocalService}.
 *
 * @author Humance
 * @see VehicleLocalService
 * @generated
 */
public class VehicleLocalServiceWrapper implements VehicleLocalService,
    ServiceWrapper<VehicleLocalService> {
    private VehicleLocalService _vehicleLocalService;

    public VehicleLocalServiceWrapper(VehicleLocalService vehicleLocalService) {
        _vehicleLocalService = vehicleLocalService;
    }

    /**
    * Adds the vehicle to the database. Also notifies the appropriate model listeners.
    *
    * @param vehicle the vehicle
    * @return the vehicle that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.Vehicle addVehicle(
        de.humance.eco.profile.model.Vehicle vehicle)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.addVehicle(vehicle);
    }

    /**
    * Creates a new vehicle with the primary key. Does not add the vehicle to the database.
    *
    * @param vehicleId the primary key for the new vehicle
    * @return the new vehicle
    */
    @Override
    public de.humance.eco.profile.model.Vehicle createVehicle(long vehicleId) {
        return _vehicleLocalService.createVehicle(vehicleId);
    }

    /**
    * Deletes the vehicle with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleId the primary key of the vehicle
    * @return the vehicle that was removed
    * @throws PortalException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.Vehicle deleteVehicle(long vehicleId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.deleteVehicle(vehicleId);
    }

    /**
    * Deletes the vehicle from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicle the vehicle
    * @return the vehicle that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.Vehicle deleteVehicle(
        de.humance.eco.profile.model.Vehicle vehicle)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.deleteVehicle(vehicle);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _vehicleLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public de.humance.eco.profile.model.Vehicle fetchVehicle(long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.fetchVehicle(vehicleId);
    }

    /**
    * Returns the vehicle with the primary key.
    *
    * @param vehicleId the primary key of the vehicle
    * @return the vehicle
    * @throws PortalException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.Vehicle getVehicle(long vehicleId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.getVehicle(vehicleId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the vehicles.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of vehicles
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<de.humance.eco.profile.model.Vehicle> getVehicles(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.getVehicles(start, end);
    }

    /**
    * Returns the number of vehicles.
    *
    * @return the number of vehicles
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getVehiclesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.getVehiclesCount();
    }

    /**
    * Updates the vehicle in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param vehicle the vehicle
    * @return the vehicle that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.Vehicle updateVehicle(
        de.humance.eco.profile.model.Vehicle vehicle)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.updateVehicle(vehicle);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _vehicleLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _vehicleLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _vehicleLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverId(
        long driverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.findByDriverId(driverId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverIdAndOrgaId(
        long driverId, long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.findByDriverIdAndOrgaId(driverId,
            organizationId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByManufacturerId(
        long manufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.findByManufacturerId(manufacturerId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByModelName(
        java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.findByModelName(modelName);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByModelNameLike(
        java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.findByModelNameLike(modelName);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByTypeId(
        long typeId) throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.findByTypeId(typeId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverIdAndTypeId(
        long driverId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.findByDriverIdAndTypeId(driverId, typeId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByLicensePlate(
        java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.findByLicensePlate(licensePlate);
    }

    @Override
    public de.humance.eco.profile.model.Vehicle addVehicle(long creatorId,
        java.lang.String creatorName, long driverId, long typeId,
        long manufacturerId, java.lang.String modelName,
        java.lang.String licensePlate, long dimensionHeight,
        long dimensionWidth, long dimensionDepth, long organizationId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.addVehicle(creatorId, creatorName,
            driverId, typeId, manufacturerId, modelName, licensePlate,
            dimensionHeight, dimensionWidth, dimensionDepth, organizationId);
    }

    @Override
    public de.humance.eco.profile.model.Vehicle updateVehicle(long vehicleId,
        long modifierId, java.lang.String modifierName, long driverId,
        long typeId, long manufacturerId, java.lang.String modelName,
        java.lang.String licensePlate, long dimensionHeight,
        long dimensionWidth, long dimensionDepth, long organizationId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.updateVehicle(vehicleId, modifierId,
            modifierName, driverId, typeId, manufacturerId, modelName,
            licensePlate, dimensionHeight, dimensionWidth, dimensionDepth,
            organizationId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public VehicleLocalService getWrappedVehicleLocalService() {
        return _vehicleLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedVehicleLocalService(
        VehicleLocalService vehicleLocalService) {
        _vehicleLocalService = vehicleLocalService;
    }

    @Override
    public VehicleLocalService getWrappedService() {
        return _vehicleLocalService;
    }

    @Override
    public void setWrappedService(VehicleLocalService vehicleLocalService) {
        _vehicleLocalService = vehicleLocalService;
    }
}
