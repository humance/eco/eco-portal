package de.humance.eco.profile.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VehicleManufacturerLocalService}.
 *
 * @author Humance
 * @see VehicleManufacturerLocalService
 * @generated
 */
public class VehicleManufacturerLocalServiceWrapper
    implements VehicleManufacturerLocalService,
        ServiceWrapper<VehicleManufacturerLocalService> {
    private VehicleManufacturerLocalService _vehicleManufacturerLocalService;

    public VehicleManufacturerLocalServiceWrapper(
        VehicleManufacturerLocalService vehicleManufacturerLocalService) {
        _vehicleManufacturerLocalService = vehicleManufacturerLocalService;
    }

    /**
    * Adds the vehicle manufacturer to the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleManufacturer the vehicle manufacturer
    * @return the vehicle manufacturer that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleManufacturer addVehicleManufacturer(
        de.humance.eco.profile.model.VehicleManufacturer vehicleManufacturer)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.addVehicleManufacturer(vehicleManufacturer);
    }

    /**
    * Creates a new vehicle manufacturer with the primary key. Does not add the vehicle manufacturer to the database.
    *
    * @param vehicleManufacturerId the primary key for the new vehicle manufacturer
    * @return the new vehicle manufacturer
    */
    @Override
    public de.humance.eco.profile.model.VehicleManufacturer createVehicleManufacturer(
        long vehicleManufacturerId) {
        return _vehicleManufacturerLocalService.createVehicleManufacturer(vehicleManufacturerId);
    }

    /**
    * Deletes the vehicle manufacturer with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleManufacturerId the primary key of the vehicle manufacturer
    * @return the vehicle manufacturer that was removed
    * @throws PortalException if a vehicle manufacturer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleManufacturer deleteVehicleManufacturer(
        long vehicleManufacturerId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.deleteVehicleManufacturer(vehicleManufacturerId);
    }

    /**
    * Deletes the vehicle manufacturer from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleManufacturer the vehicle manufacturer
    * @return the vehicle manufacturer that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleManufacturer deleteVehicleManufacturer(
        de.humance.eco.profile.model.VehicleManufacturer vehicleManufacturer)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.deleteVehicleManufacturer(vehicleManufacturer);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _vehicleManufacturerLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleManufacturerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.dynamicQuery(dynamicQuery,
            start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleManufacturerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.dynamicQuery(dynamicQuery,
            start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public de.humance.eco.profile.model.VehicleManufacturer fetchVehicleManufacturer(
        long vehicleManufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.fetchVehicleManufacturer(vehicleManufacturerId);
    }

    /**
    * Returns the vehicle manufacturer with the primary key.
    *
    * @param vehicleManufacturerId the primary key of the vehicle manufacturer
    * @return the vehicle manufacturer
    * @throws PortalException if a vehicle manufacturer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleManufacturer getVehicleManufacturer(
        long vehicleManufacturerId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.getVehicleManufacturer(vehicleManufacturerId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the vehicle manufacturers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleManufacturerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle manufacturers
    * @param end the upper bound of the range of vehicle manufacturers (not inclusive)
    * @return the range of vehicle manufacturers
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleManufacturer> getVehicleManufacturers(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.getVehicleManufacturers(start,
            end);
    }

    /**
    * Returns the number of vehicle manufacturers.
    *
    * @return the number of vehicle manufacturers
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getVehicleManufacturersCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.getVehicleManufacturersCount();
    }

    /**
    * Updates the vehicle manufacturer in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param vehicleManufacturer the vehicle manufacturer
    * @return the vehicle manufacturer that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleManufacturer updateVehicleManufacturer(
        de.humance.eco.profile.model.VehicleManufacturer vehicleManufacturer)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.updateVehicleManufacturer(vehicleManufacturer);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _vehicleManufacturerLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _vehicleManufacturerLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _vehicleManufacturerLocalService.invokeMethod(name,
            parameterTypes, arguments);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleManufacturer> findByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.findByName(name);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleManufacturer> findByNameLike(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.findByNameLike(name);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleManufacturer> findByModifiedDate(
        java.util.Date modifiedDate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.findByModifiedDate(modifiedDate);
    }

    @Override
    public de.humance.eco.profile.model.VehicleManufacturer addVehicleManufacturer(
        long creatorId, java.lang.String name, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.addVehicleManufacturer(creatorId,
            name, iconId);
    }

    @Override
    public de.humance.eco.profile.model.VehicleManufacturer updateVehicleManufacturer(
        long modifierId, long vehicleManufacturerId, java.lang.String name,
        long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleManufacturerLocalService.updateVehicleManufacturer(modifierId,
            vehicleManufacturerId, name, iconId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public VehicleManufacturerLocalService getWrappedVehicleManufacturerLocalService() {
        return _vehicleManufacturerLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedVehicleManufacturerLocalService(
        VehicleManufacturerLocalService vehicleManufacturerLocalService) {
        _vehicleManufacturerLocalService = vehicleManufacturerLocalService;
    }

    @Override
    public VehicleManufacturerLocalService getWrappedService() {
        return _vehicleManufacturerLocalService;
    }

    @Override
    public void setWrappedService(
        VehicleManufacturerLocalService vehicleManufacturerLocalService) {
        _vehicleManufacturerLocalService = vehicleManufacturerLocalService;
    }
}
