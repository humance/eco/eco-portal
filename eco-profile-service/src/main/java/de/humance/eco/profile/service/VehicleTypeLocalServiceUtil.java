package de.humance.eco.profile.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for VehicleType. This utility wraps
 * {@link de.humance.eco.profile.service.impl.VehicleTypeLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Humance
 * @see VehicleTypeLocalService
 * @see de.humance.eco.profile.service.base.VehicleTypeLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.impl.VehicleTypeLocalServiceImpl
 * @generated
 */
public class VehicleTypeLocalServiceUtil {
    private static VehicleTypeLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link de.humance.eco.profile.service.impl.VehicleTypeLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the vehicle type to the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleType the vehicle type
    * @return the vehicle type that was added
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleType addVehicleType(
        de.humance.eco.profile.model.VehicleType vehicleType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addVehicleType(vehicleType);
    }

    /**
    * Creates a new vehicle type with the primary key. Does not add the vehicle type to the database.
    *
    * @param vehicleTypeId the primary key for the new vehicle type
    * @return the new vehicle type
    */
    public static de.humance.eco.profile.model.VehicleType createVehicleType(
        long vehicleTypeId) {
        return getService().createVehicleType(vehicleTypeId);
    }

    /**
    * Deletes the vehicle type with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleTypeId the primary key of the vehicle type
    * @return the vehicle type that was removed
    * @throws PortalException if a vehicle type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleType deleteVehicleType(
        long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteVehicleType(vehicleTypeId);
    }

    /**
    * Deletes the vehicle type from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleType the vehicle type
    * @return the vehicle type that was removed
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleType deleteVehicleType(
        de.humance.eco.profile.model.VehicleType vehicleType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteVehicleType(vehicleType);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static de.humance.eco.profile.model.VehicleType fetchVehicleType(
        long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchVehicleType(vehicleTypeId);
    }

    /**
    * Returns the vehicle type with the primary key.
    *
    * @param vehicleTypeId the primary key of the vehicle type
    * @return the vehicle type
    * @throws PortalException if a vehicle type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleType getVehicleType(
        long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicleType(vehicleTypeId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the vehicle types.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle types
    * @param end the upper bound of the range of vehicle types (not inclusive)
    * @return the range of vehicle types
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleType> getVehicleTypes(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicleTypes(start, end);
    }

    /**
    * Returns the number of vehicle types.
    *
    * @return the number of vehicle types
    * @throws SystemException if a system exception occurred
    */
    public static int getVehicleTypesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicleTypesCount();
    }

    /**
    * Updates the vehicle type in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param vehicleType the vehicle type
    * @return the vehicle type that was updated
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleType updateVehicleType(
        de.humance.eco.profile.model.VehicleType vehicleType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateVehicleType(vehicleType);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<de.humance.eco.profile.model.VehicleType> findByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByName(name);
    }

    public static java.util.List<de.humance.eco.profile.model.VehicleType> findByNameLike(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByNameLike(name);
    }

    public static java.util.List<de.humance.eco.profile.model.VehicleType> findByModifiedDate(
        java.util.Date modifiedDate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByModifiedDate(modifiedDate);
    }

    public static de.humance.eco.profile.model.VehicleType addVehicleType(
        long creatorId, java.lang.String name, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().addVehicleType(creatorId, name, iconId);
    }

    public static de.humance.eco.profile.model.VehicleType updateVehicleType(
        long modifierId, long vehicleTypeId, java.lang.String name, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateVehicleType(modifierId, vehicleTypeId, name, iconId);
    }

    public static void clearService() {
        _service = null;
    }

    public static VehicleTypeLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    VehicleTypeLocalService.class.getName());

            if (invokableLocalService instanceof VehicleTypeLocalService) {
                _service = (VehicleTypeLocalService) invokableLocalService;
            } else {
                _service = new VehicleTypeLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(VehicleTypeLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(VehicleTypeLocalService service) {
    }
}
