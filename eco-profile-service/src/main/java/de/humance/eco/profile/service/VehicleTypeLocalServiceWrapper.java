package de.humance.eco.profile.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VehicleTypeLocalService}.
 *
 * @author Humance
 * @see VehicleTypeLocalService
 * @generated
 */
public class VehicleTypeLocalServiceWrapper implements VehicleTypeLocalService,
    ServiceWrapper<VehicleTypeLocalService> {
    private VehicleTypeLocalService _vehicleTypeLocalService;

    public VehicleTypeLocalServiceWrapper(
        VehicleTypeLocalService vehicleTypeLocalService) {
        _vehicleTypeLocalService = vehicleTypeLocalService;
    }

    /**
    * Adds the vehicle type to the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleType the vehicle type
    * @return the vehicle type that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleType addVehicleType(
        de.humance.eco.profile.model.VehicleType vehicleType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.addVehicleType(vehicleType);
    }

    /**
    * Creates a new vehicle type with the primary key. Does not add the vehicle type to the database.
    *
    * @param vehicleTypeId the primary key for the new vehicle type
    * @return the new vehicle type
    */
    @Override
    public de.humance.eco.profile.model.VehicleType createVehicleType(
        long vehicleTypeId) {
        return _vehicleTypeLocalService.createVehicleType(vehicleTypeId);
    }

    /**
    * Deletes the vehicle type with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleTypeId the primary key of the vehicle type
    * @return the vehicle type that was removed
    * @throws PortalException if a vehicle type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleType deleteVehicleType(
        long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.deleteVehicleType(vehicleTypeId);
    }

    /**
    * Deletes the vehicle type from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleType the vehicle type
    * @return the vehicle type that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleType deleteVehicleType(
        de.humance.eco.profile.model.VehicleType vehicleType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.deleteVehicleType(vehicleType);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _vehicleTypeLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public de.humance.eco.profile.model.VehicleType fetchVehicleType(
        long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.fetchVehicleType(vehicleTypeId);
    }

    /**
    * Returns the vehicle type with the primary key.
    *
    * @param vehicleTypeId the primary key of the vehicle type
    * @return the vehicle type
    * @throws PortalException if a vehicle type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleType getVehicleType(
        long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.getVehicleType(vehicleTypeId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the vehicle types.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle types
    * @param end the upper bound of the range of vehicle types (not inclusive)
    * @return the range of vehicle types
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleType> getVehicleTypes(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.getVehicleTypes(start, end);
    }

    /**
    * Returns the number of vehicle types.
    *
    * @return the number of vehicle types
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getVehicleTypesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.getVehicleTypesCount();
    }

    /**
    * Updates the vehicle type in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param vehicleType the vehicle type
    * @return the vehicle type that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleType updateVehicleType(
        de.humance.eco.profile.model.VehicleType vehicleType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.updateVehicleType(vehicleType);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _vehicleTypeLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _vehicleTypeLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _vehicleTypeLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleType> findByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.findByName(name);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleType> findByNameLike(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.findByNameLike(name);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleType> findByModifiedDate(
        java.util.Date modifiedDate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.findByModifiedDate(modifiedDate);
    }

    @Override
    public de.humance.eco.profile.model.VehicleType addVehicleType(
        long creatorId, java.lang.String name, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.addVehicleType(creatorId, name, iconId);
    }

    @Override
    public de.humance.eco.profile.model.VehicleType updateVehicleType(
        long modifierId, long vehicleTypeId, java.lang.String name, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeLocalService.updateVehicleType(modifierId,
            vehicleTypeId, name, iconId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public VehicleTypeLocalService getWrappedVehicleTypeLocalService() {
        return _vehicleTypeLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedVehicleTypeLocalService(
        VehicleTypeLocalService vehicleTypeLocalService) {
        _vehicleTypeLocalService = vehicleTypeLocalService;
    }

    @Override
    public VehicleTypeLocalService getWrappedService() {
        return _vehicleTypeLocalService;
    }

    @Override
    public void setWrappedService(
        VehicleTypeLocalService vehicleTypeLocalService) {
        _vehicleTypeLocalService = vehicleTypeLocalService;
    }
}
