package de.humance.eco.profile.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.BaseLocalService;
import com.liferay.portal.service.InvokableLocalService;
import com.liferay.portal.service.PersistedModelLocalService;

/**
 * Provides the local service interface for VehicleTypeResource. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Humance
 * @see VehicleTypeResourceLocalServiceUtil
 * @see de.humance.eco.profile.service.base.VehicleTypeResourceLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.impl.VehicleTypeResourceLocalServiceImpl
 * @generated
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
    PortalException.class, SystemException.class}
)
public interface VehicleTypeResourceLocalService extends BaseLocalService,
    InvokableLocalService, PersistedModelLocalService {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link VehicleTypeResourceLocalServiceUtil} to access the vehicle type resource local service. Add custom service methods to {@link de.humance.eco.profile.service.impl.VehicleTypeResourceLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */

    /**
    * Adds the vehicle type resource to the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleTypeResource the vehicle type resource
    * @return the vehicle type resource that was added
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource addVehicleTypeResource(
        de.humance.eco.profile.model.VehicleTypeResource vehicleTypeResource)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Creates a new vehicle type resource with the primary key. Does not add the vehicle type resource to the database.
    *
    * @param vehicleTypeResourceId the primary key for the new vehicle type resource
    * @return the new vehicle type resource
    */
    public de.humance.eco.profile.model.VehicleTypeResource createVehicleTypeResource(
        long vehicleTypeResourceId);

    /**
    * Deletes the vehicle type resource with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleTypeResourceId the primary key of the vehicle type resource
    * @return the vehicle type resource that was removed
    * @throws PortalException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource deleteVehicleTypeResource(
        long vehicleTypeResourceId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Deletes the vehicle type resource from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleTypeResource the vehicle type resource
    * @return the vehicle type resource that was removed
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource deleteVehicleTypeResource(
        de.humance.eco.profile.model.VehicleTypeResource vehicleTypeResource)
        throws com.liferay.portal.kernel.exception.SystemException;

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery();

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public de.humance.eco.profile.model.VehicleTypeResource fetchVehicleTypeResource(
        long vehicleTypeResourceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle type resource with the primary key.
    *
    * @param vehicleTypeResourceId the primary key of the vehicle type resource
    * @return the vehicle type resource
    * @throws PortalException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public de.humance.eco.profile.model.VehicleTypeResource getVehicleTypeResource(
        long vehicleTypeResourceId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle type resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @return the range of vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> getVehicleTypeResources(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle type resources.
    *
    * @return the number of vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getVehicleTypeResourcesCount()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Updates the vehicle type resource in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param vehicleTypeResource the vehicle type resource
    * @return the vehicle type resource that was updated
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource updateVehicleTypeResource(
        de.humance.eco.profile.model.VehicleTypeResource vehicleTypeResource)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier();

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier);

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable;

    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByTypeId(
        long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByTypeIdAndCountry(
        long vehicleTypeId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByTypeIdAndLanguage(
        long vehicleTypeId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByTypeIdAndLocale(
        long vehicleTypeId, java.lang.String country, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    public de.humance.eco.profile.model.VehicleTypeResource addVehicleTypeResource(
        long vehicleTypeId, java.lang.String country,
        java.lang.String language, java.lang.String name, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public de.humance.eco.profile.model.VehicleTypeResource updateVehicleTypeResource(
        long vehicleTypeResourceId, long vehicleTypeId,
        java.lang.String country, java.lang.String language,
        java.lang.String name, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;
}
