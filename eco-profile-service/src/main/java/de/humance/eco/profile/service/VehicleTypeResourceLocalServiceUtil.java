package de.humance.eco.profile.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for VehicleTypeResource. This utility wraps
 * {@link de.humance.eco.profile.service.impl.VehicleTypeResourceLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Humance
 * @see VehicleTypeResourceLocalService
 * @see de.humance.eco.profile.service.base.VehicleTypeResourceLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.impl.VehicleTypeResourceLocalServiceImpl
 * @generated
 */
public class VehicleTypeResourceLocalServiceUtil {
    private static VehicleTypeResourceLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link de.humance.eco.profile.service.impl.VehicleTypeResourceLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the vehicle type resource to the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleTypeResource the vehicle type resource
    * @return the vehicle type resource that was added
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource addVehicleTypeResource(
        de.humance.eco.profile.model.VehicleTypeResource vehicleTypeResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addVehicleTypeResource(vehicleTypeResource);
    }

    /**
    * Creates a new vehicle type resource with the primary key. Does not add the vehicle type resource to the database.
    *
    * @param vehicleTypeResourceId the primary key for the new vehicle type resource
    * @return the new vehicle type resource
    */
    public static de.humance.eco.profile.model.VehicleTypeResource createVehicleTypeResource(
        long vehicleTypeResourceId) {
        return getService().createVehicleTypeResource(vehicleTypeResourceId);
    }

    /**
    * Deletes the vehicle type resource with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleTypeResourceId the primary key of the vehicle type resource
    * @return the vehicle type resource that was removed
    * @throws PortalException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource deleteVehicleTypeResource(
        long vehicleTypeResourceId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteVehicleTypeResource(vehicleTypeResourceId);
    }

    /**
    * Deletes the vehicle type resource from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleTypeResource the vehicle type resource
    * @return the vehicle type resource that was removed
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource deleteVehicleTypeResource(
        de.humance.eco.profile.model.VehicleTypeResource vehicleTypeResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteVehicleTypeResource(vehicleTypeResource);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static de.humance.eco.profile.model.VehicleTypeResource fetchVehicleTypeResource(
        long vehicleTypeResourceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchVehicleTypeResource(vehicleTypeResourceId);
    }

    /**
    * Returns the vehicle type resource with the primary key.
    *
    * @param vehicleTypeResourceId the primary key of the vehicle type resource
    * @return the vehicle type resource
    * @throws PortalException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource getVehicleTypeResource(
        long vehicleTypeResourceId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicleTypeResource(vehicleTypeResourceId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the vehicle type resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @return the range of vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> getVehicleTypeResources(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicleTypeResources(start, end);
    }

    /**
    * Returns the number of vehicle type resources.
    *
    * @return the number of vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static int getVehicleTypeResourcesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicleTypeResourcesCount();
    }

    /**
    * Updates the vehicle type resource in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param vehicleTypeResource the vehicle type resource
    * @return the vehicle type resource that was updated
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource updateVehicleTypeResource(
        de.humance.eco.profile.model.VehicleTypeResource vehicleTypeResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateVehicleTypeResource(vehicleTypeResource);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByTypeId(
        long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByTypeId(vehicleTypeId);
    }

    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByTypeIdAndCountry(
        long vehicleTypeId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByTypeIdAndCountry(vehicleTypeId, country);
    }

    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByTypeIdAndLanguage(
        long vehicleTypeId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByTypeIdAndLanguage(vehicleTypeId, language);
    }

    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByTypeIdAndLocale(
        long vehicleTypeId, java.lang.String country, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .findByTypeIdAndLocale(vehicleTypeId, country, language);
    }

    public static de.humance.eco.profile.model.VehicleTypeResource addVehicleTypeResource(
        long vehicleTypeId, java.lang.String country,
        java.lang.String language, java.lang.String name, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addVehicleTypeResource(vehicleTypeId, country, language,
            name, iconId);
    }

    public static de.humance.eco.profile.model.VehicleTypeResource updateVehicleTypeResource(
        long vehicleTypeResourceId, long vehicleTypeId,
        java.lang.String country, java.lang.String language,
        java.lang.String name, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateVehicleTypeResource(vehicleTypeResourceId,
            vehicleTypeId, country, language, name, iconId);
    }

    public static void clearService() {
        _service = null;
    }

    public static VehicleTypeResourceLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    VehicleTypeResourceLocalService.class.getName());

            if (invokableLocalService instanceof VehicleTypeResourceLocalService) {
                _service = (VehicleTypeResourceLocalService) invokableLocalService;
            } else {
                _service = new VehicleTypeResourceLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(VehicleTypeResourceLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(VehicleTypeResourceLocalService service) {
    }
}
