package de.humance.eco.profile.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VehicleTypeResourceLocalService}.
 *
 * @author Humance
 * @see VehicleTypeResourceLocalService
 * @generated
 */
public class VehicleTypeResourceLocalServiceWrapper
    implements VehicleTypeResourceLocalService,
        ServiceWrapper<VehicleTypeResourceLocalService> {
    private VehicleTypeResourceLocalService _vehicleTypeResourceLocalService;

    public VehicleTypeResourceLocalServiceWrapper(
        VehicleTypeResourceLocalService vehicleTypeResourceLocalService) {
        _vehicleTypeResourceLocalService = vehicleTypeResourceLocalService;
    }

    /**
    * Adds the vehicle type resource to the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleTypeResource the vehicle type resource
    * @return the vehicle type resource that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleTypeResource addVehicleTypeResource(
        de.humance.eco.profile.model.VehicleTypeResource vehicleTypeResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.addVehicleTypeResource(vehicleTypeResource);
    }

    /**
    * Creates a new vehicle type resource with the primary key. Does not add the vehicle type resource to the database.
    *
    * @param vehicleTypeResourceId the primary key for the new vehicle type resource
    * @return the new vehicle type resource
    */
    @Override
    public de.humance.eco.profile.model.VehicleTypeResource createVehicleTypeResource(
        long vehicleTypeResourceId) {
        return _vehicleTypeResourceLocalService.createVehicleTypeResource(vehicleTypeResourceId);
    }

    /**
    * Deletes the vehicle type resource with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleTypeResourceId the primary key of the vehicle type resource
    * @return the vehicle type resource that was removed
    * @throws PortalException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleTypeResource deleteVehicleTypeResource(
        long vehicleTypeResourceId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.deleteVehicleTypeResource(vehicleTypeResourceId);
    }

    /**
    * Deletes the vehicle type resource from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleTypeResource the vehicle type resource
    * @return the vehicle type resource that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleTypeResource deleteVehicleTypeResource(
        de.humance.eco.profile.model.VehicleTypeResource vehicleTypeResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.deleteVehicleTypeResource(vehicleTypeResource);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _vehicleTypeResourceLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.dynamicQuery(dynamicQuery,
            start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.dynamicQuery(dynamicQuery,
            start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public de.humance.eco.profile.model.VehicleTypeResource fetchVehicleTypeResource(
        long vehicleTypeResourceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.fetchVehicleTypeResource(vehicleTypeResourceId);
    }

    /**
    * Returns the vehicle type resource with the primary key.
    *
    * @param vehicleTypeResourceId the primary key of the vehicle type resource
    * @return the vehicle type resource
    * @throws PortalException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleTypeResource getVehicleTypeResource(
        long vehicleTypeResourceId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.getVehicleTypeResource(vehicleTypeResourceId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the vehicle type resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @return the range of vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> getVehicleTypeResources(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.getVehicleTypeResources(start,
            end);
    }

    /**
    * Returns the number of vehicle type resources.
    *
    * @return the number of vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getVehicleTypeResourcesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.getVehicleTypeResourcesCount();
    }

    /**
    * Updates the vehicle type resource in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param vehicleTypeResource the vehicle type resource
    * @return the vehicle type resource that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleTypeResource updateVehicleTypeResource(
        de.humance.eco.profile.model.VehicleTypeResource vehicleTypeResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.updateVehicleTypeResource(vehicleTypeResource);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _vehicleTypeResourceLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _vehicleTypeResourceLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _vehicleTypeResourceLocalService.invokeMethod(name,
            parameterTypes, arguments);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByTypeId(
        long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.findByTypeId(vehicleTypeId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByTypeIdAndCountry(
        long vehicleTypeId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.findByTypeIdAndCountry(vehicleTypeId,
            country);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByTypeIdAndLanguage(
        long vehicleTypeId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.findByTypeIdAndLanguage(vehicleTypeId,
            language);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByTypeIdAndLocale(
        long vehicleTypeId, java.lang.String country, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.findByTypeIdAndLocale(vehicleTypeId,
            country, language);
    }

    @Override
    public de.humance.eco.profile.model.VehicleTypeResource addVehicleTypeResource(
        long vehicleTypeId, java.lang.String country,
        java.lang.String language, java.lang.String name, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.addVehicleTypeResource(vehicleTypeId,
            country, language, name, iconId);
    }

    @Override
    public de.humance.eco.profile.model.VehicleTypeResource updateVehicleTypeResource(
        long vehicleTypeResourceId, long vehicleTypeId,
        java.lang.String country, java.lang.String language,
        java.lang.String name, long iconId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleTypeResourceLocalService.updateVehicleTypeResource(vehicleTypeResourceId,
            vehicleTypeId, country, language, name, iconId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public VehicleTypeResourceLocalService getWrappedVehicleTypeResourceLocalService() {
        return _vehicleTypeResourceLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedVehicleTypeResourceLocalService(
        VehicleTypeResourceLocalService vehicleTypeResourceLocalService) {
        _vehicleTypeResourceLocalService = vehicleTypeResourceLocalService;
    }

    @Override
    public VehicleTypeResourceLocalService getWrappedService() {
        return _vehicleTypeResourceLocalService;
    }

    @Override
    public void setWrappedService(
        VehicleTypeResourceLocalService vehicleTypeResourceLocalService) {
        _vehicleTypeResourceLocalService = vehicleTypeResourceLocalService;
    }
}
