package de.humance.eco.profile.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for VehicleUser. This utility wraps
 * {@link de.humance.eco.profile.service.impl.VehicleUserLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Humance
 * @see VehicleUserLocalService
 * @see de.humance.eco.profile.service.base.VehicleUserLocalServiceBaseImpl
 * @see de.humance.eco.profile.service.impl.VehicleUserLocalServiceImpl
 * @generated
 */
public class VehicleUserLocalServiceUtil {
    private static VehicleUserLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link de.humance.eco.profile.service.impl.VehicleUserLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the vehicle user to the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleUser the vehicle user
    * @return the vehicle user that was added
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleUser addVehicleUser(
        de.humance.eco.profile.model.VehicleUser vehicleUser)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addVehicleUser(vehicleUser);
    }

    /**
    * Creates a new vehicle user with the primary key. Does not add the vehicle user to the database.
    *
    * @param vehicleUserId the primary key for the new vehicle user
    * @return the new vehicle user
    */
    public static de.humance.eco.profile.model.VehicleUser createVehicleUser(
        long vehicleUserId) {
        return getService().createVehicleUser(vehicleUserId);
    }

    /**
    * Deletes the vehicle user with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleUserId the primary key of the vehicle user
    * @return the vehicle user that was removed
    * @throws PortalException if a vehicle user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleUser deleteVehicleUser(
        long vehicleUserId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteVehicleUser(vehicleUserId);
    }

    /**
    * Deletes the vehicle user from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleUser the vehicle user
    * @return the vehicle user that was removed
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleUser deleteVehicleUser(
        de.humance.eco.profile.model.VehicleUser vehicleUser)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteVehicleUser(vehicleUser);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static de.humance.eco.profile.model.VehicleUser fetchVehicleUser(
        long vehicleUserId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchVehicleUser(vehicleUserId);
    }

    /**
    * Returns the vehicle user with the primary key.
    *
    * @param vehicleUserId the primary key of the vehicle user
    * @return the vehicle user
    * @throws PortalException if a vehicle user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleUser getVehicleUser(
        long vehicleUserId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicleUser(vehicleUserId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the vehicle users.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle users
    * @param end the upper bound of the range of vehicle users (not inclusive)
    * @return the range of vehicle users
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleUser> getVehicleUsers(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicleUsers(start, end);
    }

    /**
    * Returns the number of vehicle users.
    *
    * @return the number of vehicle users
    * @throws SystemException if a system exception occurred
    */
    public static int getVehicleUsersCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicleUsersCount();
    }

    /**
    * Updates the vehicle user in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param vehicleUser the vehicle user
    * @return the vehicle user that was updated
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleUser updateVehicleUser(
        de.humance.eco.profile.model.VehicleUser vehicleUser)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateVehicleUser(vehicleUser);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<de.humance.eco.profile.model.VehicleUser> findByUserId(
        long userId) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByUserId(userId);
    }

    public static java.util.List<de.humance.eco.profile.model.VehicleUser> findByVehicleId(
        long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findByVehicleId(vehicleId);
    }

    public static de.humance.eco.profile.model.VehicleUser addVehicleUser(
        long userId, long vehicleId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().addVehicleUser(userId, vehicleId);
    }

    public static void clearService() {
        _service = null;
    }

    public static VehicleUserLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    VehicleUserLocalService.class.getName());

            if (invokableLocalService instanceof VehicleUserLocalService) {
                _service = (VehicleUserLocalService) invokableLocalService;
            } else {
                _service = new VehicleUserLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(VehicleUserLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(VehicleUserLocalService service) {
    }
}
