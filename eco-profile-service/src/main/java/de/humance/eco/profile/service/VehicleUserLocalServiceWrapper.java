package de.humance.eco.profile.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VehicleUserLocalService}.
 *
 * @author Humance
 * @see VehicleUserLocalService
 * @generated
 */
public class VehicleUserLocalServiceWrapper implements VehicleUserLocalService,
    ServiceWrapper<VehicleUserLocalService> {
    private VehicleUserLocalService _vehicleUserLocalService;

    public VehicleUserLocalServiceWrapper(
        VehicleUserLocalService vehicleUserLocalService) {
        _vehicleUserLocalService = vehicleUserLocalService;
    }

    /**
    * Adds the vehicle user to the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleUser the vehicle user
    * @return the vehicle user that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleUser addVehicleUser(
        de.humance.eco.profile.model.VehicleUser vehicleUser)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUserLocalService.addVehicleUser(vehicleUser);
    }

    /**
    * Creates a new vehicle user with the primary key. Does not add the vehicle user to the database.
    *
    * @param vehicleUserId the primary key for the new vehicle user
    * @return the new vehicle user
    */
    @Override
    public de.humance.eco.profile.model.VehicleUser createVehicleUser(
        long vehicleUserId) {
        return _vehicleUserLocalService.createVehicleUser(vehicleUserId);
    }

    /**
    * Deletes the vehicle user with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleUserId the primary key of the vehicle user
    * @return the vehicle user that was removed
    * @throws PortalException if a vehicle user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleUser deleteVehicleUser(
        long vehicleUserId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUserLocalService.deleteVehicleUser(vehicleUserId);
    }

    /**
    * Deletes the vehicle user from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleUser the vehicle user
    * @return the vehicle user that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleUser deleteVehicleUser(
        de.humance.eco.profile.model.VehicleUser vehicleUser)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUserLocalService.deleteVehicleUser(vehicleUser);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _vehicleUserLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUserLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUserLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUserLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUserLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUserLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public de.humance.eco.profile.model.VehicleUser fetchVehicleUser(
        long vehicleUserId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUserLocalService.fetchVehicleUser(vehicleUserId);
    }

    /**
    * Returns the vehicle user with the primary key.
    *
    * @param vehicleUserId the primary key of the vehicle user
    * @return the vehicle user
    * @throws PortalException if a vehicle user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleUser getVehicleUser(
        long vehicleUserId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUserLocalService.getVehicleUser(vehicleUserId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUserLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the vehicle users.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle users
    * @param end the upper bound of the range of vehicle users (not inclusive)
    * @return the range of vehicle users
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleUser> getVehicleUsers(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUserLocalService.getVehicleUsers(start, end);
    }

    /**
    * Returns the number of vehicle users.
    *
    * @return the number of vehicle users
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getVehicleUsersCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUserLocalService.getVehicleUsersCount();
    }

    /**
    * Updates the vehicle user in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param vehicleUser the vehicle user
    * @return the vehicle user that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public de.humance.eco.profile.model.VehicleUser updateVehicleUser(
        de.humance.eco.profile.model.VehicleUser vehicleUser)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUserLocalService.updateVehicleUser(vehicleUser);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _vehicleUserLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _vehicleUserLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _vehicleUserLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleUser> findByUserId(
        long userId) throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUserLocalService.findByUserId(userId);
    }

    @Override
    public java.util.List<de.humance.eco.profile.model.VehicleUser> findByVehicleId(
        long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUserLocalService.findByVehicleId(vehicleId);
    }

    @Override
    public de.humance.eco.profile.model.VehicleUser addVehicleUser(
        long userId, long vehicleId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleUserLocalService.addVehicleUser(userId, vehicleId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public VehicleUserLocalService getWrappedVehicleUserLocalService() {
        return _vehicleUserLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedVehicleUserLocalService(
        VehicleUserLocalService vehicleUserLocalService) {
        _vehicleUserLocalService = vehicleUserLocalService;
    }

    @Override
    public VehicleUserLocalService getWrappedService() {
        return _vehicleUserLocalService;
    }

    @Override
    public void setWrappedService(
        VehicleUserLocalService vehicleUserLocalService) {
        _vehicleUserLocalService = vehicleUserLocalService;
    }
}
