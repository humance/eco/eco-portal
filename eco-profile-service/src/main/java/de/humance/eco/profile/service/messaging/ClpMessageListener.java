package de.humance.eco.profile.service.messaging;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;

import de.humance.eco.profile.service.ClpSerializer;
import de.humance.eco.profile.service.QualificationAttributeDefinitionLocalServiceUtil;
import de.humance.eco.profile.service.QualificationAttributeDefinitionResourceLocalServiceUtil;
import de.humance.eco.profile.service.QualificationAttributeLocalServiceUtil;
import de.humance.eco.profile.service.QualificationLocalServiceUtil;
import de.humance.eco.profile.service.QualificationTypeLocalServiceUtil;
import de.humance.eco.profile.service.QualificationTypeResourceLocalServiceUtil;
import de.humance.eco.profile.service.TrailerLocalServiceUtil;
import de.humance.eco.profile.service.TrailerTypeLocalServiceUtil;
import de.humance.eco.profile.service.TrailerUserLocalServiceUtil;
import de.humance.eco.profile.service.VehicleAttributeDefinitionLocalServiceUtil;
import de.humance.eco.profile.service.VehicleAttributeDefinitionResourceLocalServiceUtil;
import de.humance.eco.profile.service.VehicleAttributeLocalServiceUtil;
import de.humance.eco.profile.service.VehicleLocalServiceUtil;
import de.humance.eco.profile.service.VehicleManufacturerLocalServiceUtil;
import de.humance.eco.profile.service.VehicleTypeLocalServiceUtil;
import de.humance.eco.profile.service.VehicleTypeResourceLocalServiceUtil;
import de.humance.eco.profile.service.VehicleUserLocalServiceUtil;


public class ClpMessageListener extends BaseMessageListener {
    public static String getServletContextName() {
        return ClpSerializer.getServletContextName();
    }

    @Override
    protected void doReceive(Message message) throws Exception {
        String command = message.getString("command");
        String servletContextName = message.getString("servletContextName");

        if (command.equals("undeploy") &&
                servletContextName.equals(getServletContextName())) {
            QualificationLocalServiceUtil.clearService();

            QualificationAttributeLocalServiceUtil.clearService();

            QualificationAttributeDefinitionLocalServiceUtil.clearService();

            QualificationAttributeDefinitionResourceLocalServiceUtil.clearService();

            QualificationTypeLocalServiceUtil.clearService();

            QualificationTypeResourceLocalServiceUtil.clearService();

            TrailerLocalServiceUtil.clearService();

            TrailerTypeLocalServiceUtil.clearService();

            TrailerUserLocalServiceUtil.clearService();

            VehicleLocalServiceUtil.clearService();

            VehicleAttributeLocalServiceUtil.clearService();

            VehicleAttributeDefinitionLocalServiceUtil.clearService();

            VehicleAttributeDefinitionResourceLocalServiceUtil.clearService();

            VehicleManufacturerLocalServiceUtil.clearService();

            VehicleTypeLocalServiceUtil.clearService();

            VehicleTypeResourceLocalServiceUtil.clearService();

            VehicleUserLocalServiceUtil.clearService();
        }
    }
}
