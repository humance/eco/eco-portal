package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.Qualification;
import de.humance.eco.profile.service.QualificationLocalServiceUtil;

/**
 * @author Humance
 * @generated
 */
public abstract class QualificationActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public QualificationActionableDynamicQuery() throws SystemException {
        setBaseLocalService(QualificationLocalServiceUtil.getService());
        setClass(Qualification.class);

        setClassLoader(de.humance.eco.profile.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("qualificationId");
    }
}
