package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.QualificationAttribute;
import de.humance.eco.profile.service.QualificationAttributeLocalServiceUtil;

/**
 * @author Humance
 * @generated
 */
public abstract class QualificationAttributeActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public QualificationAttributeActionableDynamicQuery()
        throws SystemException {
        setBaseLocalService(QualificationAttributeLocalServiceUtil.getService());
        setClass(QualificationAttribute.class);

        setClassLoader(de.humance.eco.profile.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("qualificationAttributeId");
    }
}
