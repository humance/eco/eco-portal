package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.QualificationAttributeDefinition;
import de.humance.eco.profile.service.QualificationAttributeDefinitionLocalServiceUtil;

/**
 * @author Humance
 * @generated
 */
public abstract class QualificationAttributeDefinitionActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public QualificationAttributeDefinitionActionableDynamicQuery()
        throws SystemException {
        setBaseLocalService(QualificationAttributeDefinitionLocalServiceUtil.getService());
        setClass(QualificationAttributeDefinition.class);

        setClassLoader(de.humance.eco.profile.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("qualificationAttributeDefinitionId");
    }
}
