package de.humance.eco.profile.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import de.humance.eco.profile.model.QualificationAttributeDefinition;

/**
 * The persistence interface for the qualification attribute definition service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see QualificationAttributeDefinitionPersistenceImpl
 * @see QualificationAttributeDefinitionUtil
 * @generated
 */
public interface QualificationAttributeDefinitionPersistence
    extends BasePersistence<QualificationAttributeDefinition> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link QualificationAttributeDefinitionUtil} to access the qualification attribute definition persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the qualification attribute definitions where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @return the matching qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findByQualificationTypeId(
        long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualification attribute definitions where qualificationTypeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param start the lower bound of the range of qualification attribute definitions
    * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
    * @return the range of matching qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findByQualificationTypeId(
        long qualificationTypeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualification attribute definitions where qualificationTypeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param start the lower bound of the range of qualification attribute definitions
    * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findByQualificationTypeId(
        long qualificationTypeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first qualification attribute definition in the ordered set where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute definition
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a matching qualification attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition findByQualificationTypeId_First(
        long qualificationTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException;

    /**
    * Returns the first qualification attribute definition in the ordered set where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute definition, or <code>null</code> if a matching qualification attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition fetchByQualificationTypeId_First(
        long qualificationTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last qualification attribute definition in the ordered set where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute definition
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a matching qualification attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition findByQualificationTypeId_Last(
        long qualificationTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException;

    /**
    * Returns the last qualification attribute definition in the ordered set where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute definition, or <code>null</code> if a matching qualification attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition fetchByQualificationTypeId_Last(
        long qualificationTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualification attribute definitions before and after the current qualification attribute definition in the ordered set where qualificationTypeId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the primary key of the current qualification attribute definition
    * @param qualificationTypeId the qualification type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification attribute definition
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a qualification attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition[] findByQualificationTypeId_PrevAndNext(
        long qualificationAttributeDefinitionId, long qualificationTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException;

    /**
    * Removes all the qualification attribute definitions where qualificationTypeId = &#63; from the database.
    *
    * @param qualificationTypeId the qualification type ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByQualificationTypeId(long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualification attribute definitions where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @return the number of matching qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public int countByQualificationTypeId(long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the qualification attribute definitions where title = &#63;.
    *
    * @param title the title
    * @return the matching qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findByTitle(
        java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualification attribute definitions where title = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param title the title
    * @param start the lower bound of the range of qualification attribute definitions
    * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
    * @return the range of matching qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findByTitle(
        java.lang.String title, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualification attribute definitions where title = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param title the title
    * @param start the lower bound of the range of qualification attribute definitions
    * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findByTitle(
        java.lang.String title, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first qualification attribute definition in the ordered set where title = &#63;.
    *
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute definition
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a matching qualification attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition findByTitle_First(
        java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException;

    /**
    * Returns the first qualification attribute definition in the ordered set where title = &#63;.
    *
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute definition, or <code>null</code> if a matching qualification attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition fetchByTitle_First(
        java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last qualification attribute definition in the ordered set where title = &#63;.
    *
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute definition
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a matching qualification attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition findByTitle_Last(
        java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException;

    /**
    * Returns the last qualification attribute definition in the ordered set where title = &#63;.
    *
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute definition, or <code>null</code> if a matching qualification attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition fetchByTitle_Last(
        java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualification attribute definitions before and after the current qualification attribute definition in the ordered set where title = &#63;.
    *
    * @param qualificationAttributeDefinitionId the primary key of the current qualification attribute definition
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification attribute definition
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a qualification attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition[] findByTitle_PrevAndNext(
        long qualificationAttributeDefinitionId, java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException;

    /**
    * Removes all the qualification attribute definitions where title = &#63; from the database.
    *
    * @param title the title
    * @throws SystemException if a system exception occurred
    */
    public void removeByTitle(java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualification attribute definitions where title = &#63;.
    *
    * @param title the title
    * @return the number of matching qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public int countByTitle(java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the qualification attribute definitions where title LIKE &#63;.
    *
    * @param title the title
    * @return the matching qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findByTitleLike(
        java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualification attribute definitions where title LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param title the title
    * @param start the lower bound of the range of qualification attribute definitions
    * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
    * @return the range of matching qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findByTitleLike(
        java.lang.String title, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualification attribute definitions where title LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param title the title
    * @param start the lower bound of the range of qualification attribute definitions
    * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findByTitleLike(
        java.lang.String title, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first qualification attribute definition in the ordered set where title LIKE &#63;.
    *
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute definition
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a matching qualification attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition findByTitleLike_First(
        java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException;

    /**
    * Returns the first qualification attribute definition in the ordered set where title LIKE &#63;.
    *
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute definition, or <code>null</code> if a matching qualification attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition fetchByTitleLike_First(
        java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last qualification attribute definition in the ordered set where title LIKE &#63;.
    *
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute definition
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a matching qualification attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition findByTitleLike_Last(
        java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException;

    /**
    * Returns the last qualification attribute definition in the ordered set where title LIKE &#63;.
    *
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute definition, or <code>null</code> if a matching qualification attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition fetchByTitleLike_Last(
        java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualification attribute definitions before and after the current qualification attribute definition in the ordered set where title LIKE &#63;.
    *
    * @param qualificationAttributeDefinitionId the primary key of the current qualification attribute definition
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification attribute definition
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a qualification attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition[] findByTitleLike_PrevAndNext(
        long qualificationAttributeDefinitionId, java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException;

    /**
    * Removes all the qualification attribute definitions where title LIKE &#63; from the database.
    *
    * @param title the title
    * @throws SystemException if a system exception occurred
    */
    public void removeByTitleLike(java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualification attribute definitions where title LIKE &#63;.
    *
    * @param title the title
    * @return the number of matching qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public int countByTitleLike(java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the qualification attribute definition in the entity cache if it is enabled.
    *
    * @param qualificationAttributeDefinition the qualification attribute definition
    */
    public void cacheResult(
        de.humance.eco.profile.model.QualificationAttributeDefinition qualificationAttributeDefinition);

    /**
    * Caches the qualification attribute definitions in the entity cache if it is enabled.
    *
    * @param qualificationAttributeDefinitions the qualification attribute definitions
    */
    public void cacheResult(
        java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> qualificationAttributeDefinitions);

    /**
    * Creates a new qualification attribute definition with the primary key. Does not add the qualification attribute definition to the database.
    *
    * @param qualificationAttributeDefinitionId the primary key for the new qualification attribute definition
    * @return the new qualification attribute definition
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition create(
        long qualificationAttributeDefinitionId);

    /**
    * Removes the qualification attribute definition with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinitionId the primary key of the qualification attribute definition
    * @return the qualification attribute definition that was removed
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a qualification attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition remove(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException;

    public de.humance.eco.profile.model.QualificationAttributeDefinition updateImpl(
        de.humance.eco.profile.model.QualificationAttributeDefinition qualificationAttributeDefinition)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualification attribute definition with the primary key or throws a {@link de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException} if it could not be found.
    *
    * @param qualificationAttributeDefinitionId the primary key of the qualification attribute definition
    * @return the qualification attribute definition
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException if a qualification attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition findByPrimaryKey(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionException;

    /**
    * Returns the qualification attribute definition with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param qualificationAttributeDefinitionId the primary key of the qualification attribute definition
    * @return the qualification attribute definition, or <code>null</code> if a qualification attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttributeDefinition fetchByPrimaryKey(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the qualification attribute definitions.
    *
    * @return the qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualification attribute definitions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification attribute definitions
    * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
    * @return the range of qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualification attribute definitions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification attribute definitions
    * @param end the upper bound of the range of qualification attribute definitions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinition> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the qualification attribute definitions from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualification attribute definitions.
    *
    * @return the number of qualification attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
