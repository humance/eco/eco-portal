package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.QualificationAttributeDefinitionResource;
import de.humance.eco.profile.service.QualificationAttributeDefinitionResourceLocalServiceUtil;

/**
 * @author Humance
 * @generated
 */
public abstract class QualificationAttributeDefinitionResourceActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public QualificationAttributeDefinitionResourceActionableDynamicQuery()
        throws SystemException {
        setBaseLocalService(QualificationAttributeDefinitionResourceLocalServiceUtil.getService());
        setClass(QualificationAttributeDefinitionResource.class);

        setClassLoader(de.humance.eco.profile.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("qualificationAttributeDefinitionResourceId");
    }
}
