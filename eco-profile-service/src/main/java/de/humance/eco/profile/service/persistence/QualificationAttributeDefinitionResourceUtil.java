package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import de.humance.eco.profile.model.QualificationAttributeDefinitionResource;

import java.util.List;

/**
 * The persistence utility for the qualification attribute definition resource service. This utility wraps {@link QualificationAttributeDefinitionResourcePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see QualificationAttributeDefinitionResourcePersistence
 * @see QualificationAttributeDefinitionResourcePersistenceImpl
 * @generated
 */
public class QualificationAttributeDefinitionResourceUtil {
    private static QualificationAttributeDefinitionResourcePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource) {
        getPersistence().clearCache(qualificationAttributeDefinitionResource);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<QualificationAttributeDefinitionResource> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<QualificationAttributeDefinitionResource> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<QualificationAttributeDefinitionResource> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static QualificationAttributeDefinitionResource update(
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource)
        throws SystemException {
        return getPersistence().update(qualificationAttributeDefinitionResource);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static QualificationAttributeDefinitionResource update(
        QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence()
                   .update(qualificationAttributeDefinitionResource,
            serviceContext);
    }

    /**
    * Returns all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @return the matching qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByQualificationAttribDefId(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationAttribDefId(qualificationAttributeDefinitionId);
    }

    /**
    * Returns a range of all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param start the lower bound of the range of qualification attribute definition resources
    * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
    * @return the range of matching qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByQualificationAttribDefId(
        long qualificationAttributeDefinitionId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationAttribDefId(qualificationAttributeDefinitionId,
            start, end);
    }

    /**
    * Returns an ordered range of all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param start the lower bound of the range of qualification attribute definition resources
    * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByQualificationAttribDefId(
        long qualificationAttributeDefinitionId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationAttribDefId(qualificationAttributeDefinitionId,
            start, end, orderByComparator);
    }

    /**
    * Returns the first qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute definition resource
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a matching qualification attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource findByQualificationAttribDefId_First(
        long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException {
        return getPersistence()
                   .findByQualificationAttribDefId_First(qualificationAttributeDefinitionId,
            orderByComparator);
    }

    /**
    * Returns the first qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute definition resource, or <code>null</code> if a matching qualification attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource fetchByQualificationAttribDefId_First(
        long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationAttribDefId_First(qualificationAttributeDefinitionId,
            orderByComparator);
    }

    /**
    * Returns the last qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute definition resource
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a matching qualification attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource findByQualificationAttribDefId_Last(
        long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException {
        return getPersistence()
                   .findByQualificationAttribDefId_Last(qualificationAttributeDefinitionId,
            orderByComparator);
    }

    /**
    * Returns the last qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute definition resource, or <code>null</code> if a matching qualification attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource fetchByQualificationAttribDefId_Last(
        long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationAttribDefId_Last(qualificationAttributeDefinitionId,
            orderByComparator);
    }

    /**
    * Returns the qualification attribute definition resources before and after the current qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionResourceId the primary key of the current qualification attribute definition resource
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification attribute definition resource
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a qualification attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource[] findByQualificationAttribDefId_PrevAndNext(
        long qualificationAttributeDefinitionResourceId,
        long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException {
        return getPersistence()
                   .findByQualificationAttribDefId_PrevAndNext(qualificationAttributeDefinitionResourceId,
            qualificationAttributeDefinitionId, orderByComparator);
    }

    /**
    * Removes all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; from the database.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByQualificationAttribDefId(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByQualificationAttribDefId(qualificationAttributeDefinitionId);
    }

    /**
    * Returns the number of qualification attribute definition resources where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @return the number of matching qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static int countByQualificationAttribDefId(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByQualificationAttribDefId(qualificationAttributeDefinitionId);
    }

    /**
    * Returns all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @return the matching qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndCountry(
        long qualificationAttributeDefinitionId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndCountry(qualificationAttributeDefinitionId,
            country);
    }

    /**
    * Returns a range of all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @param start the lower bound of the range of qualification attribute definition resources
    * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
    * @return the range of matching qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndCountry(
        long qualificationAttributeDefinitionId, java.lang.String country,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndCountry(qualificationAttributeDefinitionId,
            country, start, end);
    }

    /**
    * Returns an ordered range of all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @param start the lower bound of the range of qualification attribute definition resources
    * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndCountry(
        long qualificationAttributeDefinitionId, java.lang.String country,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndCountry(qualificationAttributeDefinitionId,
            country, start, end, orderByComparator);
    }

    /**
    * Returns the first qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute definition resource
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a matching qualification attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource findByQualificationAttribDefIdAndCountry_First(
        long qualificationAttributeDefinitionId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndCountry_First(qualificationAttributeDefinitionId,
            country, orderByComparator);
    }

    /**
    * Returns the first qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute definition resource, or <code>null</code> if a matching qualification attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource fetchByQualificationAttribDefIdAndCountry_First(
        long qualificationAttributeDefinitionId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationAttribDefIdAndCountry_First(qualificationAttributeDefinitionId,
            country, orderByComparator);
    }

    /**
    * Returns the last qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute definition resource
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a matching qualification attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource findByQualificationAttribDefIdAndCountry_Last(
        long qualificationAttributeDefinitionId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndCountry_Last(qualificationAttributeDefinitionId,
            country, orderByComparator);
    }

    /**
    * Returns the last qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute definition resource, or <code>null</code> if a matching qualification attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource fetchByQualificationAttribDefIdAndCountry_Last(
        long qualificationAttributeDefinitionId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationAttribDefIdAndCountry_Last(qualificationAttributeDefinitionId,
            country, orderByComparator);
    }

    /**
    * Returns the qualification attribute definition resources before and after the current qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param qualificationAttributeDefinitionResourceId the primary key of the current qualification attribute definition resource
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification attribute definition resource
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a qualification attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource[] findByQualificationAttribDefIdAndCountry_PrevAndNext(
        long qualificationAttributeDefinitionResourceId,
        long qualificationAttributeDefinitionId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndCountry_PrevAndNext(qualificationAttributeDefinitionResourceId,
            qualificationAttributeDefinitionId, country, orderByComparator);
    }

    /**
    * Removes all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63; from the database.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @throws SystemException if a system exception occurred
    */
    public static void removeByQualificationAttribDefIdAndCountry(
        long qualificationAttributeDefinitionId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByQualificationAttribDefIdAndCountry(qualificationAttributeDefinitionId,
            country);
    }

    /**
    * Returns the number of qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @return the number of matching qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static int countByQualificationAttribDefIdAndCountry(
        long qualificationAttributeDefinitionId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByQualificationAttribDefIdAndCountry(qualificationAttributeDefinitionId,
            country);
    }

    /**
    * Returns all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param language the language
    * @return the matching qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndLanguage(
        long qualificationAttributeDefinitionId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndLanguage(qualificationAttributeDefinitionId,
            language);
    }

    /**
    * Returns a range of all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param language the language
    * @param start the lower bound of the range of qualification attribute definition resources
    * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
    * @return the range of matching qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndLanguage(
        long qualificationAttributeDefinitionId, java.lang.String language,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndLanguage(qualificationAttributeDefinitionId,
            language, start, end);
    }

    /**
    * Returns an ordered range of all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param language the language
    * @param start the lower bound of the range of qualification attribute definition resources
    * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndLanguage(
        long qualificationAttributeDefinitionId, java.lang.String language,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndLanguage(qualificationAttributeDefinitionId,
            language, start, end, orderByComparator);
    }

    /**
    * Returns the first qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute definition resource
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a matching qualification attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource findByQualificationAttribDefIdAndLanguage_First(
        long qualificationAttributeDefinitionId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndLanguage_First(qualificationAttributeDefinitionId,
            language, orderByComparator);
    }

    /**
    * Returns the first qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute definition resource, or <code>null</code> if a matching qualification attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource fetchByQualificationAttribDefIdAndLanguage_First(
        long qualificationAttributeDefinitionId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationAttribDefIdAndLanguage_First(qualificationAttributeDefinitionId,
            language, orderByComparator);
    }

    /**
    * Returns the last qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute definition resource
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a matching qualification attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource findByQualificationAttribDefIdAndLanguage_Last(
        long qualificationAttributeDefinitionId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndLanguage_Last(qualificationAttributeDefinitionId,
            language, orderByComparator);
    }

    /**
    * Returns the last qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute definition resource, or <code>null</code> if a matching qualification attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource fetchByQualificationAttribDefIdAndLanguage_Last(
        long qualificationAttributeDefinitionId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationAttribDefIdAndLanguage_Last(qualificationAttributeDefinitionId,
            language, orderByComparator);
    }

    /**
    * Returns the qualification attribute definition resources before and after the current qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param qualificationAttributeDefinitionResourceId the primary key of the current qualification attribute definition resource
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification attribute definition resource
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a qualification attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource[] findByQualificationAttribDefIdAndLanguage_PrevAndNext(
        long qualificationAttributeDefinitionResourceId,
        long qualificationAttributeDefinitionId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndLanguage_PrevAndNext(qualificationAttributeDefinitionResourceId,
            qualificationAttributeDefinitionId, language, orderByComparator);
    }

    /**
    * Removes all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and language = &#63; from the database.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param language the language
    * @throws SystemException if a system exception occurred
    */
    public static void removeByQualificationAttribDefIdAndLanguage(
        long qualificationAttributeDefinitionId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByQualificationAttribDefIdAndLanguage(qualificationAttributeDefinitionId,
            language);
    }

    /**
    * Returns the number of qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param language the language
    * @return the number of matching qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static int countByQualificationAttribDefIdAndLanguage(
        long qualificationAttributeDefinitionId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByQualificationAttribDefIdAndLanguage(qualificationAttributeDefinitionId,
            language);
    }

    /**
    * Returns all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @param language the language
    * @return the matching qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndLocale(
        long qualificationAttributeDefinitionId, java.lang.String country,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndLocale(qualificationAttributeDefinitionId,
            country, language);
    }

    /**
    * Returns a range of all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @param language the language
    * @param start the lower bound of the range of qualification attribute definition resources
    * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
    * @return the range of matching qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndLocale(
        long qualificationAttributeDefinitionId, java.lang.String country,
        java.lang.String language, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndLocale(qualificationAttributeDefinitionId,
            country, language, start, end);
    }

    /**
    * Returns an ordered range of all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @param language the language
    * @param start the lower bound of the range of qualification attribute definition resources
    * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findByQualificationAttribDefIdAndLocale(
        long qualificationAttributeDefinitionId, java.lang.String country,
        java.lang.String language, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndLocale(qualificationAttributeDefinitionId,
            country, language, start, end, orderByComparator);
    }

    /**
    * Returns the first qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute definition resource
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a matching qualification attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource findByQualificationAttribDefIdAndLocale_First(
        long qualificationAttributeDefinitionId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndLocale_First(qualificationAttributeDefinitionId,
            country, language, orderByComparator);
    }

    /**
    * Returns the first qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute definition resource, or <code>null</code> if a matching qualification attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource fetchByQualificationAttribDefIdAndLocale_First(
        long qualificationAttributeDefinitionId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationAttribDefIdAndLocale_First(qualificationAttributeDefinitionId,
            country, language, orderByComparator);
    }

    /**
    * Returns the last qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute definition resource
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a matching qualification attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource findByQualificationAttribDefIdAndLocale_Last(
        long qualificationAttributeDefinitionId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndLocale_Last(qualificationAttributeDefinitionId,
            country, language, orderByComparator);
    }

    /**
    * Returns the last qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute definition resource, or <code>null</code> if a matching qualification attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource fetchByQualificationAttribDefIdAndLocale_Last(
        long qualificationAttributeDefinitionId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationAttribDefIdAndLocale_Last(qualificationAttributeDefinitionId,
            country, language, orderByComparator);
    }

    /**
    * Returns the qualification attribute definition resources before and after the current qualification attribute definition resource in the ordered set where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationAttributeDefinitionResourceId the primary key of the current qualification attribute definition resource
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification attribute definition resource
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a qualification attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource[] findByQualificationAttribDefIdAndLocale_PrevAndNext(
        long qualificationAttributeDefinitionResourceId,
        long qualificationAttributeDefinitionId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException {
        return getPersistence()
                   .findByQualificationAttribDefIdAndLocale_PrevAndNext(qualificationAttributeDefinitionResourceId,
            qualificationAttributeDefinitionId, country, language,
            orderByComparator);
    }

    /**
    * Removes all the qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63; from the database.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @param language the language
    * @throws SystemException if a system exception occurred
    */
    public static void removeByQualificationAttribDefIdAndLocale(
        long qualificationAttributeDefinitionId, java.lang.String country,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByQualificationAttribDefIdAndLocale(qualificationAttributeDefinitionId,
            country, language);
    }

    /**
    * Returns the number of qualification attribute definition resources where qualificationAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param country the country
    * @param language the language
    * @return the number of matching qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static int countByQualificationAttribDefIdAndLocale(
        long qualificationAttributeDefinitionId, java.lang.String country,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByQualificationAttribDefIdAndLocale(qualificationAttributeDefinitionId,
            country, language);
    }

    /**
    * Caches the qualification attribute definition resource in the entity cache if it is enabled.
    *
    * @param qualificationAttributeDefinitionResource the qualification attribute definition resource
    */
    public static void cacheResult(
        de.humance.eco.profile.model.QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource) {
        getPersistence().cacheResult(qualificationAttributeDefinitionResource);
    }

    /**
    * Caches the qualification attribute definition resources in the entity cache if it is enabled.
    *
    * @param qualificationAttributeDefinitionResources the qualification attribute definition resources
    */
    public static void cacheResult(
        java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> qualificationAttributeDefinitionResources) {
        getPersistence().cacheResult(qualificationAttributeDefinitionResources);
    }

    /**
    * Creates a new qualification attribute definition resource with the primary key. Does not add the qualification attribute definition resource to the database.
    *
    * @param qualificationAttributeDefinitionResourceId the primary key for the new qualification attribute definition resource
    * @return the new qualification attribute definition resource
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource create(
        long qualificationAttributeDefinitionResourceId) {
        return getPersistence()
                   .create(qualificationAttributeDefinitionResourceId);
    }

    /**
    * Removes the qualification attribute definition resource with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeDefinitionResourceId the primary key of the qualification attribute definition resource
    * @return the qualification attribute definition resource that was removed
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a qualification attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource remove(
        long qualificationAttributeDefinitionResourceId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException {
        return getPersistence()
                   .remove(qualificationAttributeDefinitionResourceId);
    }

    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource updateImpl(
        de.humance.eco.profile.model.QualificationAttributeDefinitionResource qualificationAttributeDefinitionResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .updateImpl(qualificationAttributeDefinitionResource);
    }

    /**
    * Returns the qualification attribute definition resource with the primary key or throws a {@link de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException} if it could not be found.
    *
    * @param qualificationAttributeDefinitionResourceId the primary key of the qualification attribute definition resource
    * @return the qualification attribute definition resource
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException if a qualification attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource findByPrimaryKey(
        long qualificationAttributeDefinitionResourceId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeDefinitionResourceException {
        return getPersistence()
                   .findByPrimaryKey(qualificationAttributeDefinitionResourceId);
    }

    /**
    * Returns the qualification attribute definition resource with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param qualificationAttributeDefinitionResourceId the primary key of the qualification attribute definition resource
    * @return the qualification attribute definition resource, or <code>null</code> if a qualification attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttributeDefinitionResource fetchByPrimaryKey(
        long qualificationAttributeDefinitionResourceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByPrimaryKey(qualificationAttributeDefinitionResourceId);
    }

    /**
    * Returns all the qualification attribute definition resources.
    *
    * @return the qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the qualification attribute definition resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification attribute definition resources
    * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
    * @return the range of qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the qualification attribute definition resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification attribute definition resources
    * @param end the upper bound of the range of qualification attribute definition resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttributeDefinitionResource> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the qualification attribute definition resources from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of qualification attribute definition resources.
    *
    * @return the number of qualification attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static QualificationAttributeDefinitionResourcePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (QualificationAttributeDefinitionResourcePersistence) PortletBeanLocatorUtil.locate(de.humance.eco.profile.service.ClpSerializer.getServletContextName(),
                    QualificationAttributeDefinitionResourcePersistence.class.getName());

            ReferenceRegistry.registerReference(QualificationAttributeDefinitionResourceUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(
        QualificationAttributeDefinitionResourcePersistence persistence) {
    }
}
