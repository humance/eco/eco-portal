package de.humance.eco.profile.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import de.humance.eco.profile.model.QualificationAttribute;

/**
 * The persistence interface for the qualification attribute service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see QualificationAttributePersistenceImpl
 * @see QualificationAttributeUtil
 * @generated
 */
public interface QualificationAttributePersistence extends BasePersistence<QualificationAttribute> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link QualificationAttributeUtil} to access the qualification attribute persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the qualification attributes where qualificationId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @return the matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationId(
        long qualificationId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualification attributes where qualificationId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationId the qualification ID
    * @param start the lower bound of the range of qualification attributes
    * @param end the upper bound of the range of qualification attributes (not inclusive)
    * @return the range of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationId(
        long qualificationId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualification attributes where qualificationId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationId the qualification ID
    * @param start the lower bound of the range of qualification attributes
    * @param end the upper bound of the range of qualification attributes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationId(
        long qualificationId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first qualification attribute in the ordered set where qualificationId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute findByQualificationId_First(
        long qualificationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException;

    /**
    * Returns the first qualification attribute in the ordered set where qualificationId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute fetchByQualificationId_First(
        long qualificationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last qualification attribute in the ordered set where qualificationId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute findByQualificationId_Last(
        long qualificationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException;

    /**
    * Returns the last qualification attribute in the ordered set where qualificationId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute fetchByQualificationId_Last(
        long qualificationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualification attributes before and after the current qualification attribute in the ordered set where qualificationId = &#63;.
    *
    * @param qualificationAttributeId the primary key of the current qualification attribute
    * @param qualificationId the qualification ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a qualification attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute[] findByQualificationId_PrevAndNext(
        long qualificationAttributeId, long qualificationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException;

    /**
    * Removes all the qualification attributes where qualificationId = &#63; from the database.
    *
    * @param qualificationId the qualification ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByQualificationId(long qualificationId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualification attributes where qualificationId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @return the number of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public int countByQualificationId(long qualificationId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the qualification attributes where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @return the matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationIdAndAttributeDefinitionId(
        long qualificationId, long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualification attributes where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param start the lower bound of the range of qualification attributes
    * @param end the upper bound of the range of qualification attributes (not inclusive)
    * @return the range of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationIdAndAttributeDefinitionId(
        long qualificationId, long qualificationAttributeDefinitionId,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualification attributes where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param start the lower bound of the range of qualification attributes
    * @param end the upper bound of the range of qualification attributes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationIdAndAttributeDefinitionId(
        long qualificationId, long qualificationAttributeDefinitionId,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first qualification attribute in the ordered set where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute findByQualificationIdAndAttributeDefinitionId_First(
        long qualificationId, long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException;

    /**
    * Returns the first qualification attribute in the ordered set where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute fetchByQualificationIdAndAttributeDefinitionId_First(
        long qualificationId, long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last qualification attribute in the ordered set where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute findByQualificationIdAndAttributeDefinitionId_Last(
        long qualificationId, long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException;

    /**
    * Returns the last qualification attribute in the ordered set where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute fetchByQualificationIdAndAttributeDefinitionId_Last(
        long qualificationId, long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualification attributes before and after the current qualification attribute in the ordered set where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeId the primary key of the current qualification attribute
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a qualification attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute[] findByQualificationIdAndAttributeDefinitionId_PrevAndNext(
        long qualificationAttributeId, long qualificationId,
        long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException;

    /**
    * Removes all the qualification attributes where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63; from the database.
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByQualificationIdAndAttributeDefinitionId(
        long qualificationId, long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualification attributes where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @return the number of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public int countByQualificationIdAndAttributeDefinitionId(
        long qualificationId, long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the qualification attributes where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @return the matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualification attributes where qualificationAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param start the lower bound of the range of qualification attributes
    * @param end the upper bound of the range of qualification attributes (not inclusive)
    * @return the range of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualification attributes where qualificationAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param start the lower bound of the range of qualification attributes
    * @param end the upper bound of the range of qualification attributes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first qualification attribute in the ordered set where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute findByQualificationAttributeDefinitionId_First(
        long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException;

    /**
    * Returns the first qualification attribute in the ordered set where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute fetchByQualificationAttributeDefinitionId_First(
        long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last qualification attribute in the ordered set where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute findByQualificationAttributeDefinitionId_Last(
        long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException;

    /**
    * Returns the last qualification attribute in the ordered set where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute fetchByQualificationAttributeDefinitionId_Last(
        long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualification attributes before and after the current qualification attribute in the ordered set where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeId the primary key of the current qualification attribute
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a qualification attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute[] findByQualificationAttributeDefinitionId_PrevAndNext(
        long qualificationAttributeId, long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException;

    /**
    * Removes all the qualification attributes where qualificationAttributeDefinitionId = &#63; from the database.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualification attributes where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @return the number of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public int countByQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the qualification attribute in the entity cache if it is enabled.
    *
    * @param qualificationAttribute the qualification attribute
    */
    public void cacheResult(
        de.humance.eco.profile.model.QualificationAttribute qualificationAttribute);

    /**
    * Caches the qualification attributes in the entity cache if it is enabled.
    *
    * @param qualificationAttributes the qualification attributes
    */
    public void cacheResult(
        java.util.List<de.humance.eco.profile.model.QualificationAttribute> qualificationAttributes);

    /**
    * Creates a new qualification attribute with the primary key. Does not add the qualification attribute to the database.
    *
    * @param qualificationAttributeId the primary key for the new qualification attribute
    * @return the new qualification attribute
    */
    public de.humance.eco.profile.model.QualificationAttribute create(
        long qualificationAttributeId);

    /**
    * Removes the qualification attribute with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeId the primary key of the qualification attribute
    * @return the qualification attribute that was removed
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a qualification attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute remove(
        long qualificationAttributeId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException;

    public de.humance.eco.profile.model.QualificationAttribute updateImpl(
        de.humance.eco.profile.model.QualificationAttribute qualificationAttribute)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualification attribute with the primary key or throws a {@link de.humance.eco.profile.NoSuchQualificationAttributeException} if it could not be found.
    *
    * @param qualificationAttributeId the primary key of the qualification attribute
    * @return the qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a qualification attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute findByPrimaryKey(
        long qualificationAttributeId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException;

    /**
    * Returns the qualification attribute with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param qualificationAttributeId the primary key of the qualification attribute
    * @return the qualification attribute, or <code>null</code> if a qualification attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationAttribute fetchByPrimaryKey(
        long qualificationAttributeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the qualification attributes.
    *
    * @return the qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttribute> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualification attributes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification attributes
    * @param end the upper bound of the range of qualification attributes (not inclusive)
    * @return the range of qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttribute> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualification attributes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification attributes
    * @param end the upper bound of the range of qualification attributes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationAttribute> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the qualification attributes from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualification attributes.
    *
    * @return the number of qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
