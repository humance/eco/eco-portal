package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import de.humance.eco.profile.model.QualificationAttribute;

import java.util.List;

/**
 * The persistence utility for the qualification attribute service. This utility wraps {@link QualificationAttributePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see QualificationAttributePersistence
 * @see QualificationAttributePersistenceImpl
 * @generated
 */
public class QualificationAttributeUtil {
    private static QualificationAttributePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(QualificationAttribute qualificationAttribute) {
        getPersistence().clearCache(qualificationAttribute);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<QualificationAttribute> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<QualificationAttribute> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<QualificationAttribute> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static QualificationAttribute update(
        QualificationAttribute qualificationAttribute)
        throws SystemException {
        return getPersistence().update(qualificationAttribute);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static QualificationAttribute update(
        QualificationAttribute qualificationAttribute,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(qualificationAttribute, serviceContext);
    }

    /**
    * Returns all the qualification attributes where qualificationId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @return the matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationId(
        long qualificationId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByQualificationId(qualificationId);
    }

    /**
    * Returns a range of all the qualification attributes where qualificationId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationId the qualification ID
    * @param start the lower bound of the range of qualification attributes
    * @param end the upper bound of the range of qualification attributes (not inclusive)
    * @return the range of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationId(
        long qualificationId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationId(qualificationId, start, end);
    }

    /**
    * Returns an ordered range of all the qualification attributes where qualificationId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationId the qualification ID
    * @param start the lower bound of the range of qualification attributes
    * @param end the upper bound of the range of qualification attributes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationId(
        long qualificationId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationId(qualificationId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first qualification attribute in the ordered set where qualificationId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute findByQualificationId_First(
        long qualificationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException {
        return getPersistence()
                   .findByQualificationId_First(qualificationId,
            orderByComparator);
    }

    /**
    * Returns the first qualification attribute in the ordered set where qualificationId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute fetchByQualificationId_First(
        long qualificationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationId_First(qualificationId,
            orderByComparator);
    }

    /**
    * Returns the last qualification attribute in the ordered set where qualificationId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute findByQualificationId_Last(
        long qualificationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException {
        return getPersistence()
                   .findByQualificationId_Last(qualificationId,
            orderByComparator);
    }

    /**
    * Returns the last qualification attribute in the ordered set where qualificationId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute fetchByQualificationId_Last(
        long qualificationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationId_Last(qualificationId,
            orderByComparator);
    }

    /**
    * Returns the qualification attributes before and after the current qualification attribute in the ordered set where qualificationId = &#63;.
    *
    * @param qualificationAttributeId the primary key of the current qualification attribute
    * @param qualificationId the qualification ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a qualification attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute[] findByQualificationId_PrevAndNext(
        long qualificationAttributeId, long qualificationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException {
        return getPersistence()
                   .findByQualificationId_PrevAndNext(qualificationAttributeId,
            qualificationId, orderByComparator);
    }

    /**
    * Removes all the qualification attributes where qualificationId = &#63; from the database.
    *
    * @param qualificationId the qualification ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByQualificationId(long qualificationId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByQualificationId(qualificationId);
    }

    /**
    * Returns the number of qualification attributes where qualificationId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @return the number of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public static int countByQualificationId(long qualificationId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByQualificationId(qualificationId);
    }

    /**
    * Returns all the qualification attributes where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @return the matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationIdAndAttributeDefinitionId(
        long qualificationId, long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationIdAndAttributeDefinitionId(qualificationId,
            qualificationAttributeDefinitionId);
    }

    /**
    * Returns a range of all the qualification attributes where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param start the lower bound of the range of qualification attributes
    * @param end the upper bound of the range of qualification attributes (not inclusive)
    * @return the range of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationIdAndAttributeDefinitionId(
        long qualificationId, long qualificationAttributeDefinitionId,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationIdAndAttributeDefinitionId(qualificationId,
            qualificationAttributeDefinitionId, start, end);
    }

    /**
    * Returns an ordered range of all the qualification attributes where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param start the lower bound of the range of qualification attributes
    * @param end the upper bound of the range of qualification attributes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationIdAndAttributeDefinitionId(
        long qualificationId, long qualificationAttributeDefinitionId,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationIdAndAttributeDefinitionId(qualificationId,
            qualificationAttributeDefinitionId, start, end, orderByComparator);
    }

    /**
    * Returns the first qualification attribute in the ordered set where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute findByQualificationIdAndAttributeDefinitionId_First(
        long qualificationId, long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException {
        return getPersistence()
                   .findByQualificationIdAndAttributeDefinitionId_First(qualificationId,
            qualificationAttributeDefinitionId, orderByComparator);
    }

    /**
    * Returns the first qualification attribute in the ordered set where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute fetchByQualificationIdAndAttributeDefinitionId_First(
        long qualificationId, long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationIdAndAttributeDefinitionId_First(qualificationId,
            qualificationAttributeDefinitionId, orderByComparator);
    }

    /**
    * Returns the last qualification attribute in the ordered set where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute findByQualificationIdAndAttributeDefinitionId_Last(
        long qualificationId, long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException {
        return getPersistence()
                   .findByQualificationIdAndAttributeDefinitionId_Last(qualificationId,
            qualificationAttributeDefinitionId, orderByComparator);
    }

    /**
    * Returns the last qualification attribute in the ordered set where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute fetchByQualificationIdAndAttributeDefinitionId_Last(
        long qualificationId, long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationIdAndAttributeDefinitionId_Last(qualificationId,
            qualificationAttributeDefinitionId, orderByComparator);
    }

    /**
    * Returns the qualification attributes before and after the current qualification attribute in the ordered set where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeId the primary key of the current qualification attribute
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a qualification attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute[] findByQualificationIdAndAttributeDefinitionId_PrevAndNext(
        long qualificationAttributeId, long qualificationId,
        long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException {
        return getPersistence()
                   .findByQualificationIdAndAttributeDefinitionId_PrevAndNext(qualificationAttributeId,
            qualificationId, qualificationAttributeDefinitionId,
            orderByComparator);
    }

    /**
    * Removes all the qualification attributes where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63; from the database.
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByQualificationIdAndAttributeDefinitionId(
        long qualificationId, long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByQualificationIdAndAttributeDefinitionId(qualificationId,
            qualificationAttributeDefinitionId);
    }

    /**
    * Returns the number of qualification attributes where qualificationId = &#63; and qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationId the qualification ID
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @return the number of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public static int countByQualificationIdAndAttributeDefinitionId(
        long qualificationId, long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByQualificationIdAndAttributeDefinitionId(qualificationId,
            qualificationAttributeDefinitionId);
    }

    /**
    * Returns all the qualification attributes where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @return the matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);
    }

    /**
    * Returns a range of all the qualification attributes where qualificationAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param start the lower bound of the range of qualification attributes
    * @param end the upper bound of the range of qualification attributes (not inclusive)
    * @return the range of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationAttributeDefinitionId(qualificationAttributeDefinitionId,
            start, end);
    }

    /**
    * Returns an ordered range of all the qualification attributes where qualificationAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param start the lower bound of the range of qualification attributes
    * @param end the upper bound of the range of qualification attributes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttribute> findByQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationAttributeDefinitionId(qualificationAttributeDefinitionId,
            start, end, orderByComparator);
    }

    /**
    * Returns the first qualification attribute in the ordered set where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute findByQualificationAttributeDefinitionId_First(
        long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException {
        return getPersistence()
                   .findByQualificationAttributeDefinitionId_First(qualificationAttributeDefinitionId,
            orderByComparator);
    }

    /**
    * Returns the first qualification attribute in the ordered set where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute fetchByQualificationAttributeDefinitionId_First(
        long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationAttributeDefinitionId_First(qualificationAttributeDefinitionId,
            orderByComparator);
    }

    /**
    * Returns the last qualification attribute in the ordered set where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute findByQualificationAttributeDefinitionId_Last(
        long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException {
        return getPersistence()
                   .findByQualificationAttributeDefinitionId_Last(qualificationAttributeDefinitionId,
            orderByComparator);
    }

    /**
    * Returns the last qualification attribute in the ordered set where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification attribute, or <code>null</code> if a matching qualification attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute fetchByQualificationAttributeDefinitionId_Last(
        long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationAttributeDefinitionId_Last(qualificationAttributeDefinitionId,
            orderByComparator);
    }

    /**
    * Returns the qualification attributes before and after the current qualification attribute in the ordered set where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeId the primary key of the current qualification attribute
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a qualification attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute[] findByQualificationAttributeDefinitionId_PrevAndNext(
        long qualificationAttributeId, long qualificationAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException {
        return getPersistence()
                   .findByQualificationAttributeDefinitionId_PrevAndNext(qualificationAttributeId,
            qualificationAttributeDefinitionId, orderByComparator);
    }

    /**
    * Removes all the qualification attributes where qualificationAttributeDefinitionId = &#63; from the database.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);
    }

    /**
    * Returns the number of qualification attributes where qualificationAttributeDefinitionId = &#63;.
    *
    * @param qualificationAttributeDefinitionId the qualification attribute definition ID
    * @return the number of matching qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public static int countByQualificationAttributeDefinitionId(
        long qualificationAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByQualificationAttributeDefinitionId(qualificationAttributeDefinitionId);
    }

    /**
    * Caches the qualification attribute in the entity cache if it is enabled.
    *
    * @param qualificationAttribute the qualification attribute
    */
    public static void cacheResult(
        de.humance.eco.profile.model.QualificationAttribute qualificationAttribute) {
        getPersistence().cacheResult(qualificationAttribute);
    }

    /**
    * Caches the qualification attributes in the entity cache if it is enabled.
    *
    * @param qualificationAttributes the qualification attributes
    */
    public static void cacheResult(
        java.util.List<de.humance.eco.profile.model.QualificationAttribute> qualificationAttributes) {
        getPersistence().cacheResult(qualificationAttributes);
    }

    /**
    * Creates a new qualification attribute with the primary key. Does not add the qualification attribute to the database.
    *
    * @param qualificationAttributeId the primary key for the new qualification attribute
    * @return the new qualification attribute
    */
    public static de.humance.eco.profile.model.QualificationAttribute create(
        long qualificationAttributeId) {
        return getPersistence().create(qualificationAttributeId);
    }

    /**
    * Removes the qualification attribute with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationAttributeId the primary key of the qualification attribute
    * @return the qualification attribute that was removed
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a qualification attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute remove(
        long qualificationAttributeId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException {
        return getPersistence().remove(qualificationAttributeId);
    }

    public static de.humance.eco.profile.model.QualificationAttribute updateImpl(
        de.humance.eco.profile.model.QualificationAttribute qualificationAttribute)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(qualificationAttribute);
    }

    /**
    * Returns the qualification attribute with the primary key or throws a {@link de.humance.eco.profile.NoSuchQualificationAttributeException} if it could not be found.
    *
    * @param qualificationAttributeId the primary key of the qualification attribute
    * @return the qualification attribute
    * @throws de.humance.eco.profile.NoSuchQualificationAttributeException if a qualification attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute findByPrimaryKey(
        long qualificationAttributeId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationAttributeException {
        return getPersistence().findByPrimaryKey(qualificationAttributeId);
    }

    /**
    * Returns the qualification attribute with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param qualificationAttributeId the primary key of the qualification attribute
    * @return the qualification attribute, or <code>null</code> if a qualification attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationAttribute fetchByPrimaryKey(
        long qualificationAttributeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(qualificationAttributeId);
    }

    /**
    * Returns all the qualification attributes.
    *
    * @return the qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttribute> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the qualification attributes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification attributes
    * @param end the upper bound of the range of qualification attributes (not inclusive)
    * @return the range of qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttribute> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the qualification attributes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification attributes
    * @param end the upper bound of the range of qualification attributes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationAttribute> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the qualification attributes from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of qualification attributes.
    *
    * @return the number of qualification attributes
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static QualificationAttributePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (QualificationAttributePersistence) PortletBeanLocatorUtil.locate(de.humance.eco.profile.service.ClpSerializer.getServletContextName(),
                    QualificationAttributePersistence.class.getName());

            ReferenceRegistry.registerReference(QualificationAttributeUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(QualificationAttributePersistence persistence) {
    }
}
