package de.humance.eco.profile.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import de.humance.eco.profile.model.Qualification;

/**
 * The persistence interface for the qualification service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see QualificationPersistenceImpl
 * @see QualificationUtil
 * @generated
 */
public interface QualificationPersistence extends BasePersistence<Qualification> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link QualificationUtil} to access the qualification persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the qualifications where ownerId = &#63;.
    *
    * @param ownerId the owner ID
    * @return the matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Qualification> findByOwnerId(
        long ownerId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualifications where ownerId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param ownerId the owner ID
    * @param start the lower bound of the range of qualifications
    * @param end the upper bound of the range of qualifications (not inclusive)
    * @return the range of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Qualification> findByOwnerId(
        long ownerId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualifications where ownerId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param ownerId the owner ID
    * @param start the lower bound of the range of qualifications
    * @param end the upper bound of the range of qualifications (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Qualification> findByOwnerId(
        long ownerId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first qualification in the ordered set where ownerId = &#63;.
    *
    * @param ownerId the owner ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification findByOwnerId_First(
        long ownerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException;

    /**
    * Returns the first qualification in the ordered set where ownerId = &#63;.
    *
    * @param ownerId the owner ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification, or <code>null</code> if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification fetchByOwnerId_First(
        long ownerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last qualification in the ordered set where ownerId = &#63;.
    *
    * @param ownerId the owner ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification findByOwnerId_Last(
        long ownerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException;

    /**
    * Returns the last qualification in the ordered set where ownerId = &#63;.
    *
    * @param ownerId the owner ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification, or <code>null</code> if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification fetchByOwnerId_Last(
        long ownerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualifications before and after the current qualification in the ordered set where ownerId = &#63;.
    *
    * @param qualificationId the primary key of the current qualification
    * @param ownerId the owner ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a qualification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification[] findByOwnerId_PrevAndNext(
        long qualificationId, long ownerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException;

    /**
    * Removes all the qualifications where ownerId = &#63; from the database.
    *
    * @param ownerId the owner ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByOwnerId(long ownerId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualifications where ownerId = &#63;.
    *
    * @param ownerId the owner ID
    * @return the number of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public int countByOwnerId(long ownerId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the qualifications where typeId = &#63;.
    *
    * @param typeId the type ID
    * @return the matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Qualification> findByTypeId(
        long typeId) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualifications where typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param typeId the type ID
    * @param start the lower bound of the range of qualifications
    * @param end the upper bound of the range of qualifications (not inclusive)
    * @return the range of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Qualification> findByTypeId(
        long typeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualifications where typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param typeId the type ID
    * @param start the lower bound of the range of qualifications
    * @param end the upper bound of the range of qualifications (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Qualification> findByTypeId(
        long typeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first qualification in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification findByTypeId_First(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException;

    /**
    * Returns the first qualification in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification, or <code>null</code> if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification fetchByTypeId_First(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last qualification in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification findByTypeId_Last(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException;

    /**
    * Returns the last qualification in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification, or <code>null</code> if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification fetchByTypeId_Last(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualifications before and after the current qualification in the ordered set where typeId = &#63;.
    *
    * @param qualificationId the primary key of the current qualification
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a qualification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification[] findByTypeId_PrevAndNext(
        long qualificationId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException;

    /**
    * Removes all the qualifications where typeId = &#63; from the database.
    *
    * @param typeId the type ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByTypeId(long typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualifications where typeId = &#63;.
    *
    * @param typeId the type ID
    * @return the number of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public int countByTypeId(long typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the qualifications where ownerId = &#63; and typeId = &#63;.
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @return the matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Qualification> findByOwnerIdAndTypeId(
        long ownerId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualifications where ownerId = &#63; and typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @param start the lower bound of the range of qualifications
    * @param end the upper bound of the range of qualifications (not inclusive)
    * @return the range of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Qualification> findByOwnerIdAndTypeId(
        long ownerId, long typeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualifications where ownerId = &#63; and typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @param start the lower bound of the range of qualifications
    * @param end the upper bound of the range of qualifications (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Qualification> findByOwnerIdAndTypeId(
        long ownerId, long typeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first qualification in the ordered set where ownerId = &#63; and typeId = &#63;.
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification findByOwnerIdAndTypeId_First(
        long ownerId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException;

    /**
    * Returns the first qualification in the ordered set where ownerId = &#63; and typeId = &#63;.
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification, or <code>null</code> if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification fetchByOwnerIdAndTypeId_First(
        long ownerId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last qualification in the ordered set where ownerId = &#63; and typeId = &#63;.
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification findByOwnerIdAndTypeId_Last(
        long ownerId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException;

    /**
    * Returns the last qualification in the ordered set where ownerId = &#63; and typeId = &#63;.
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification, or <code>null</code> if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification fetchByOwnerIdAndTypeId_Last(
        long ownerId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualifications before and after the current qualification in the ordered set where ownerId = &#63; and typeId = &#63;.
    *
    * @param qualificationId the primary key of the current qualification
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a qualification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification[] findByOwnerIdAndTypeId_PrevAndNext(
        long qualificationId, long ownerId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException;

    /**
    * Removes all the qualifications where ownerId = &#63; and typeId = &#63; from the database.
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByOwnerIdAndTypeId(long ownerId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualifications where ownerId = &#63; and typeId = &#63;.
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @return the number of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public int countByOwnerIdAndTypeId(long ownerId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the qualification in the entity cache if it is enabled.
    *
    * @param qualification the qualification
    */
    public void cacheResult(
        de.humance.eco.profile.model.Qualification qualification);

    /**
    * Caches the qualifications in the entity cache if it is enabled.
    *
    * @param qualifications the qualifications
    */
    public void cacheResult(
        java.util.List<de.humance.eco.profile.model.Qualification> qualifications);

    /**
    * Creates a new qualification with the primary key. Does not add the qualification to the database.
    *
    * @param qualificationId the primary key for the new qualification
    * @return the new qualification
    */
    public de.humance.eco.profile.model.Qualification create(
        long qualificationId);

    /**
    * Removes the qualification with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationId the primary key of the qualification
    * @return the qualification that was removed
    * @throws de.humance.eco.profile.NoSuchQualificationException if a qualification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification remove(
        long qualificationId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException;

    public de.humance.eco.profile.model.Qualification updateImpl(
        de.humance.eco.profile.model.Qualification qualification)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualification with the primary key or throws a {@link de.humance.eco.profile.NoSuchQualificationException} if it could not be found.
    *
    * @param qualificationId the primary key of the qualification
    * @return the qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a qualification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification findByPrimaryKey(
        long qualificationId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException;

    /**
    * Returns the qualification with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param qualificationId the primary key of the qualification
    * @return the qualification, or <code>null</code> if a qualification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Qualification fetchByPrimaryKey(
        long qualificationId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the qualifications.
    *
    * @return the qualifications
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Qualification> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualifications.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualifications
    * @param end the upper bound of the range of qualifications (not inclusive)
    * @return the range of qualifications
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Qualification> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualifications.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualifications
    * @param end the upper bound of the range of qualifications (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of qualifications
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Qualification> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the qualifications from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualifications.
    *
    * @return the number of qualifications
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
