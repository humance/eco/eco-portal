package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.QualificationType;
import de.humance.eco.profile.service.QualificationTypeLocalServiceUtil;

/**
 * @author Humance
 * @generated
 */
public abstract class QualificationTypeActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public QualificationTypeActionableDynamicQuery() throws SystemException {
        setBaseLocalService(QualificationTypeLocalServiceUtil.getService());
        setClass(QualificationType.class);

        setClassLoader(de.humance.eco.profile.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("qualificationTypeId");
    }
}
