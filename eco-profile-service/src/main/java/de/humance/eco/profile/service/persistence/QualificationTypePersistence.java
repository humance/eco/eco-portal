package de.humance.eco.profile.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import de.humance.eco.profile.model.QualificationType;

/**
 * The persistence interface for the qualification type service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see QualificationTypePersistenceImpl
 * @see QualificationTypeUtil
 * @generated
 */
public interface QualificationTypePersistence extends BasePersistence<QualificationType> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link QualificationTypeUtil} to access the qualification type persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the qualification types where name = &#63;.
    *
    * @param name the name
    * @return the matching qualification types
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationType> findByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualification types where name = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param name the name
    * @param start the lower bound of the range of qualification types
    * @param end the upper bound of the range of qualification types (not inclusive)
    * @return the range of matching qualification types
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationType> findByName(
        java.lang.String name, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualification types where name = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param name the name
    * @param start the lower bound of the range of qualification types
    * @param end the upper bound of the range of qualification types (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification types
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationType> findByName(
        java.lang.String name, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first qualification type in the ordered set where name = &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type
    * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a matching qualification type could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationType findByName_First(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeException;

    /**
    * Returns the first qualification type in the ordered set where name = &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type, or <code>null</code> if a matching qualification type could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationType fetchByName_First(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last qualification type in the ordered set where name = &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type
    * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a matching qualification type could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationType findByName_Last(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeException;

    /**
    * Returns the last qualification type in the ordered set where name = &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type, or <code>null</code> if a matching qualification type could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationType fetchByName_Last(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualification types before and after the current qualification type in the ordered set where name = &#63;.
    *
    * @param qualificationTypeId the primary key of the current qualification type
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification type
    * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a qualification type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationType[] findByName_PrevAndNext(
        long qualificationTypeId, java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeException;

    /**
    * Removes all the qualification types where name = &#63; from the database.
    *
    * @param name the name
    * @throws SystemException if a system exception occurred
    */
    public void removeByName(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualification types where name = &#63;.
    *
    * @param name the name
    * @return the number of matching qualification types
    * @throws SystemException if a system exception occurred
    */
    public int countByName(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the qualification types where name LIKE &#63;.
    *
    * @param name the name
    * @return the matching qualification types
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationType> findByNameLike(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualification types where name LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param name the name
    * @param start the lower bound of the range of qualification types
    * @param end the upper bound of the range of qualification types (not inclusive)
    * @return the range of matching qualification types
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationType> findByNameLike(
        java.lang.String name, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualification types where name LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param name the name
    * @param start the lower bound of the range of qualification types
    * @param end the upper bound of the range of qualification types (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification types
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationType> findByNameLike(
        java.lang.String name, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first qualification type in the ordered set where name LIKE &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type
    * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a matching qualification type could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationType findByNameLike_First(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeException;

    /**
    * Returns the first qualification type in the ordered set where name LIKE &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type, or <code>null</code> if a matching qualification type could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationType fetchByNameLike_First(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last qualification type in the ordered set where name LIKE &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type
    * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a matching qualification type could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationType findByNameLike_Last(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeException;

    /**
    * Returns the last qualification type in the ordered set where name LIKE &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type, or <code>null</code> if a matching qualification type could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationType fetchByNameLike_Last(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualification types before and after the current qualification type in the ordered set where name LIKE &#63;.
    *
    * @param qualificationTypeId the primary key of the current qualification type
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification type
    * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a qualification type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationType[] findByNameLike_PrevAndNext(
        long qualificationTypeId, java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeException;

    /**
    * Removes all the qualification types where name LIKE &#63; from the database.
    *
    * @param name the name
    * @throws SystemException if a system exception occurred
    */
    public void removeByNameLike(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualification types where name LIKE &#63;.
    *
    * @param name the name
    * @return the number of matching qualification types
    * @throws SystemException if a system exception occurred
    */
    public int countByNameLike(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the qualification type in the entity cache if it is enabled.
    *
    * @param qualificationType the qualification type
    */
    public void cacheResult(
        de.humance.eco.profile.model.QualificationType qualificationType);

    /**
    * Caches the qualification types in the entity cache if it is enabled.
    *
    * @param qualificationTypes the qualification types
    */
    public void cacheResult(
        java.util.List<de.humance.eco.profile.model.QualificationType> qualificationTypes);

    /**
    * Creates a new qualification type with the primary key. Does not add the qualification type to the database.
    *
    * @param qualificationTypeId the primary key for the new qualification type
    * @return the new qualification type
    */
    public de.humance.eco.profile.model.QualificationType create(
        long qualificationTypeId);

    /**
    * Removes the qualification type with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationTypeId the primary key of the qualification type
    * @return the qualification type that was removed
    * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a qualification type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationType remove(
        long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeException;

    public de.humance.eco.profile.model.QualificationType updateImpl(
        de.humance.eco.profile.model.QualificationType qualificationType)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualification type with the primary key or throws a {@link de.humance.eco.profile.NoSuchQualificationTypeException} if it could not be found.
    *
    * @param qualificationTypeId the primary key of the qualification type
    * @return the qualification type
    * @throws de.humance.eco.profile.NoSuchQualificationTypeException if a qualification type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationType findByPrimaryKey(
        long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeException;

    /**
    * Returns the qualification type with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param qualificationTypeId the primary key of the qualification type
    * @return the qualification type, or <code>null</code> if a qualification type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationType fetchByPrimaryKey(
        long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the qualification types.
    *
    * @return the qualification types
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationType> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualification types.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification types
    * @param end the upper bound of the range of qualification types (not inclusive)
    * @return the range of qualification types
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationType> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualification types.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification types
    * @param end the upper bound of the range of qualification types (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of qualification types
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationType> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the qualification types from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualification types.
    *
    * @return the number of qualification types
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
