package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.QualificationTypeResource;
import de.humance.eco.profile.service.QualificationTypeResourceLocalServiceUtil;

/**
 * @author Humance
 * @generated
 */
public abstract class QualificationTypeResourceActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public QualificationTypeResourceActionableDynamicQuery()
        throws SystemException {
        setBaseLocalService(QualificationTypeResourceLocalServiceUtil.getService());
        setClass(QualificationTypeResource.class);

        setClassLoader(de.humance.eco.profile.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("qualificationTypeResourceId");
    }
}
