package de.humance.eco.profile.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import de.humance.eco.profile.model.QualificationTypeResource;

/**
 * The persistence interface for the qualification type resource service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see QualificationTypeResourcePersistenceImpl
 * @see QualificationTypeResourceUtil
 * @generated
 */
public interface QualificationTypeResourcePersistence extends BasePersistence<QualificationTypeResource> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link QualificationTypeResourceUtil} to access the qualification type resource persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the qualification type resources where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @return the matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeId(
        long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualification type resources where qualificationTypeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @return the range of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeId(
        long qualificationTypeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualification type resources where qualificationTypeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeId(
        long qualificationTypeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource findByQualificationTypeId_First(
        long qualificationTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException;

    /**
    * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource fetchByQualificationTypeId_First(
        long qualificationTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource findByQualificationTypeId_Last(
        long qualificationTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException;

    /**
    * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource fetchByQualificationTypeId_Last(
        long qualificationTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualification type resources before and after the current qualification type resource in the ordered set where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeResourceId the primary key of the current qualification type resource
    * @param qualificationTypeId the qualification type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource[] findByQualificationTypeId_PrevAndNext(
        long qualificationTypeResourceId, long qualificationTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException;

    /**
    * Removes all the qualification type resources where qualificationTypeId = &#63; from the database.
    *
    * @param qualificationTypeId the qualification type ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByQualificationTypeId(long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualification type resources where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @return the number of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public int countByQualificationTypeId(long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the qualification type resources where qualificationTypeId = &#63; and country = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @return the matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndCountry(
        long qualificationTypeId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualification type resources where qualificationTypeId = &#63; and country = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @return the range of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndCountry(
        long qualificationTypeId, java.lang.String country, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualification type resources where qualificationTypeId = &#63; and country = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndCountry(
        long qualificationTypeId, java.lang.String country, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource findByQualificationTypeIdAndCountry_First(
        long qualificationTypeId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException;

    /**
    * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource fetchByQualificationTypeIdAndCountry_First(
        long qualificationTypeId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource findByQualificationTypeIdAndCountry_Last(
        long qualificationTypeId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException;

    /**
    * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource fetchByQualificationTypeIdAndCountry_Last(
        long qualificationTypeId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualification type resources before and after the current qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63;.
    *
    * @param qualificationTypeResourceId the primary key of the current qualification type resource
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource[] findByQualificationTypeIdAndCountry_PrevAndNext(
        long qualificationTypeResourceId, long qualificationTypeId,
        java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException;

    /**
    * Removes all the qualification type resources where qualificationTypeId = &#63; and country = &#63; from the database.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @throws SystemException if a system exception occurred
    */
    public void removeByQualificationTypeIdAndCountry(
        long qualificationTypeId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualification type resources where qualificationTypeId = &#63; and country = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @return the number of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public int countByQualificationTypeIdAndCountry(long qualificationTypeId,
        java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the qualification type resources where qualificationTypeId = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @return the matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndLanguage(
        long qualificationTypeId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualification type resources where qualificationTypeId = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @return the range of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndLanguage(
        long qualificationTypeId, java.lang.String language, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualification type resources where qualificationTypeId = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndLanguage(
        long qualificationTypeId, java.lang.String language, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource findByQualificationTypeIdAndLanguage_First(
        long qualificationTypeId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException;

    /**
    * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource fetchByQualificationTypeIdAndLanguage_First(
        long qualificationTypeId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource findByQualificationTypeIdAndLanguage_Last(
        long qualificationTypeId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException;

    /**
    * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource fetchByQualificationTypeIdAndLanguage_Last(
        long qualificationTypeId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualification type resources before and after the current qualification type resource in the ordered set where qualificationTypeId = &#63; and language = &#63;.
    *
    * @param qualificationTypeResourceId the primary key of the current qualification type resource
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource[] findByQualificationTypeIdAndLanguage_PrevAndNext(
        long qualificationTypeResourceId, long qualificationTypeId,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException;

    /**
    * Removes all the qualification type resources where qualificationTypeId = &#63; and language = &#63; from the database.
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @throws SystemException if a system exception occurred
    */
    public void removeByQualificationTypeIdAndLanguage(
        long qualificationTypeId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualification type resources where qualificationTypeId = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @return the number of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public int countByQualificationTypeIdAndLanguage(long qualificationTypeId,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the qualification type resources where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @return the matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndLocale(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualification type resources where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @return the range of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndLocale(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualification type resources where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndLocale(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource findByQualificationTypeIdAndLocale_First(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException;

    /**
    * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource fetchByQualificationTypeIdAndLocale_First(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource findByQualificationTypeIdAndLocale_Last(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException;

    /**
    * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource fetchByQualificationTypeIdAndLocale_Last(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualification type resources before and after the current qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationTypeResourceId the primary key of the current qualification type resource
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource[] findByQualificationTypeIdAndLocale_PrevAndNext(
        long qualificationTypeResourceId, long qualificationTypeId,
        java.lang.String country, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException;

    /**
    * Removes all the qualification type resources where qualificationTypeId = &#63; and country = &#63; and language = &#63; from the database.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @throws SystemException if a system exception occurred
    */
    public void removeByQualificationTypeIdAndLocale(long qualificationTypeId,
        java.lang.String country, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualification type resources where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @return the number of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public int countByQualificationTypeIdAndLocale(long qualificationTypeId,
        java.lang.String country, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the qualification type resource in the entity cache if it is enabled.
    *
    * @param qualificationTypeResource the qualification type resource
    */
    public void cacheResult(
        de.humance.eco.profile.model.QualificationTypeResource qualificationTypeResource);

    /**
    * Caches the qualification type resources in the entity cache if it is enabled.
    *
    * @param qualificationTypeResources the qualification type resources
    */
    public void cacheResult(
        java.util.List<de.humance.eco.profile.model.QualificationTypeResource> qualificationTypeResources);

    /**
    * Creates a new qualification type resource with the primary key. Does not add the qualification type resource to the database.
    *
    * @param qualificationTypeResourceId the primary key for the new qualification type resource
    * @return the new qualification type resource
    */
    public de.humance.eco.profile.model.QualificationTypeResource create(
        long qualificationTypeResourceId);

    /**
    * Removes the qualification type resource with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationTypeResourceId the primary key of the qualification type resource
    * @return the qualification type resource that was removed
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource remove(
        long qualificationTypeResourceId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException;

    public de.humance.eco.profile.model.QualificationTypeResource updateImpl(
        de.humance.eco.profile.model.QualificationTypeResource qualificationTypeResource)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the qualification type resource with the primary key or throws a {@link de.humance.eco.profile.NoSuchQualificationTypeResourceException} if it could not be found.
    *
    * @param qualificationTypeResourceId the primary key of the qualification type resource
    * @return the qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource findByPrimaryKey(
        long qualificationTypeResourceId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException;

    /**
    * Returns the qualification type resource with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param qualificationTypeResourceId the primary key of the qualification type resource
    * @return the qualification type resource, or <code>null</code> if a qualification type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.QualificationTypeResource fetchByPrimaryKey(
        long qualificationTypeResourceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the qualification type resources.
    *
    * @return the qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the qualification type resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @return the range of qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the qualification type resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the qualification type resources from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of qualification type resources.
    *
    * @return the number of qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
