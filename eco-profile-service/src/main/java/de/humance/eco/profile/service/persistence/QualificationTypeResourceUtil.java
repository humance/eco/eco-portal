package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import de.humance.eco.profile.model.QualificationTypeResource;

import java.util.List;

/**
 * The persistence utility for the qualification type resource service. This utility wraps {@link QualificationTypeResourcePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see QualificationTypeResourcePersistence
 * @see QualificationTypeResourcePersistenceImpl
 * @generated
 */
public class QualificationTypeResourceUtil {
    private static QualificationTypeResourcePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(
        QualificationTypeResource qualificationTypeResource) {
        getPersistence().clearCache(qualificationTypeResource);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<QualificationTypeResource> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<QualificationTypeResource> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<QualificationTypeResource> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static QualificationTypeResource update(
        QualificationTypeResource qualificationTypeResource)
        throws SystemException {
        return getPersistence().update(qualificationTypeResource);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static QualificationTypeResource update(
        QualificationTypeResource qualificationTypeResource,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(qualificationTypeResource, serviceContext);
    }

    /**
    * Returns all the qualification type resources where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @return the matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeId(
        long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByQualificationTypeId(qualificationTypeId);
    }

    /**
    * Returns a range of all the qualification type resources where qualificationTypeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @return the range of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeId(
        long qualificationTypeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationTypeId(qualificationTypeId, start, end);
    }

    /**
    * Returns an ordered range of all the qualification type resources where qualificationTypeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeId(
        long qualificationTypeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationTypeId(qualificationTypeId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource findByQualificationTypeId_First(
        long qualificationTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException {
        return getPersistence()
                   .findByQualificationTypeId_First(qualificationTypeId,
            orderByComparator);
    }

    /**
    * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource fetchByQualificationTypeId_First(
        long qualificationTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationTypeId_First(qualificationTypeId,
            orderByComparator);
    }

    /**
    * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource findByQualificationTypeId_Last(
        long qualificationTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException {
        return getPersistence()
                   .findByQualificationTypeId_Last(qualificationTypeId,
            orderByComparator);
    }

    /**
    * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource fetchByQualificationTypeId_Last(
        long qualificationTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationTypeId_Last(qualificationTypeId,
            orderByComparator);
    }

    /**
    * Returns the qualification type resources before and after the current qualification type resource in the ordered set where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeResourceId the primary key of the current qualification type resource
    * @param qualificationTypeId the qualification type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource[] findByQualificationTypeId_PrevAndNext(
        long qualificationTypeResourceId, long qualificationTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException {
        return getPersistence()
                   .findByQualificationTypeId_PrevAndNext(qualificationTypeResourceId,
            qualificationTypeId, orderByComparator);
    }

    /**
    * Removes all the qualification type resources where qualificationTypeId = &#63; from the database.
    *
    * @param qualificationTypeId the qualification type ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByQualificationTypeId(long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByQualificationTypeId(qualificationTypeId);
    }

    /**
    * Returns the number of qualification type resources where qualificationTypeId = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @return the number of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static int countByQualificationTypeId(long qualificationTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByQualificationTypeId(qualificationTypeId);
    }

    /**
    * Returns all the qualification type resources where qualificationTypeId = &#63; and country = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @return the matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndCountry(
        long qualificationTypeId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationTypeIdAndCountry(qualificationTypeId,
            country);
    }

    /**
    * Returns a range of all the qualification type resources where qualificationTypeId = &#63; and country = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @return the range of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndCountry(
        long qualificationTypeId, java.lang.String country, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationTypeIdAndCountry(qualificationTypeId,
            country, start, end);
    }

    /**
    * Returns an ordered range of all the qualification type resources where qualificationTypeId = &#63; and country = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndCountry(
        long qualificationTypeId, java.lang.String country, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationTypeIdAndCountry(qualificationTypeId,
            country, start, end, orderByComparator);
    }

    /**
    * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource findByQualificationTypeIdAndCountry_First(
        long qualificationTypeId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException {
        return getPersistence()
                   .findByQualificationTypeIdAndCountry_First(qualificationTypeId,
            country, orderByComparator);
    }

    /**
    * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource fetchByQualificationTypeIdAndCountry_First(
        long qualificationTypeId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationTypeIdAndCountry_First(qualificationTypeId,
            country, orderByComparator);
    }

    /**
    * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource findByQualificationTypeIdAndCountry_Last(
        long qualificationTypeId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException {
        return getPersistence()
                   .findByQualificationTypeIdAndCountry_Last(qualificationTypeId,
            country, orderByComparator);
    }

    /**
    * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource fetchByQualificationTypeIdAndCountry_Last(
        long qualificationTypeId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationTypeIdAndCountry_Last(qualificationTypeId,
            country, orderByComparator);
    }

    /**
    * Returns the qualification type resources before and after the current qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63;.
    *
    * @param qualificationTypeResourceId the primary key of the current qualification type resource
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource[] findByQualificationTypeIdAndCountry_PrevAndNext(
        long qualificationTypeResourceId, long qualificationTypeId,
        java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException {
        return getPersistence()
                   .findByQualificationTypeIdAndCountry_PrevAndNext(qualificationTypeResourceId,
            qualificationTypeId, country, orderByComparator);
    }

    /**
    * Removes all the qualification type resources where qualificationTypeId = &#63; and country = &#63; from the database.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @throws SystemException if a system exception occurred
    */
    public static void removeByQualificationTypeIdAndCountry(
        long qualificationTypeId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByQualificationTypeIdAndCountry(qualificationTypeId, country);
    }

    /**
    * Returns the number of qualification type resources where qualificationTypeId = &#63; and country = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @return the number of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static int countByQualificationTypeIdAndCountry(
        long qualificationTypeId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByQualificationTypeIdAndCountry(qualificationTypeId,
            country);
    }

    /**
    * Returns all the qualification type resources where qualificationTypeId = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @return the matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndLanguage(
        long qualificationTypeId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationTypeIdAndLanguage(qualificationTypeId,
            language);
    }

    /**
    * Returns a range of all the qualification type resources where qualificationTypeId = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @return the range of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndLanguage(
        long qualificationTypeId, java.lang.String language, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationTypeIdAndLanguage(qualificationTypeId,
            language, start, end);
    }

    /**
    * Returns an ordered range of all the qualification type resources where qualificationTypeId = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndLanguage(
        long qualificationTypeId, java.lang.String language, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationTypeIdAndLanguage(qualificationTypeId,
            language, start, end, orderByComparator);
    }

    /**
    * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource findByQualificationTypeIdAndLanguage_First(
        long qualificationTypeId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException {
        return getPersistence()
                   .findByQualificationTypeIdAndLanguage_First(qualificationTypeId,
            language, orderByComparator);
    }

    /**
    * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource fetchByQualificationTypeIdAndLanguage_First(
        long qualificationTypeId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationTypeIdAndLanguage_First(qualificationTypeId,
            language, orderByComparator);
    }

    /**
    * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource findByQualificationTypeIdAndLanguage_Last(
        long qualificationTypeId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException {
        return getPersistence()
                   .findByQualificationTypeIdAndLanguage_Last(qualificationTypeId,
            language, orderByComparator);
    }

    /**
    * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource fetchByQualificationTypeIdAndLanguage_Last(
        long qualificationTypeId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationTypeIdAndLanguage_Last(qualificationTypeId,
            language, orderByComparator);
    }

    /**
    * Returns the qualification type resources before and after the current qualification type resource in the ordered set where qualificationTypeId = &#63; and language = &#63;.
    *
    * @param qualificationTypeResourceId the primary key of the current qualification type resource
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource[] findByQualificationTypeIdAndLanguage_PrevAndNext(
        long qualificationTypeResourceId, long qualificationTypeId,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException {
        return getPersistence()
                   .findByQualificationTypeIdAndLanguage_PrevAndNext(qualificationTypeResourceId,
            qualificationTypeId, language, orderByComparator);
    }

    /**
    * Removes all the qualification type resources where qualificationTypeId = &#63; and language = &#63; from the database.
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @throws SystemException if a system exception occurred
    */
    public static void removeByQualificationTypeIdAndLanguage(
        long qualificationTypeId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByQualificationTypeIdAndLanguage(qualificationTypeId,
            language);
    }

    /**
    * Returns the number of qualification type resources where qualificationTypeId = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param language the language
    * @return the number of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static int countByQualificationTypeIdAndLanguage(
        long qualificationTypeId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByQualificationTypeIdAndLanguage(qualificationTypeId,
            language);
    }

    /**
    * Returns all the qualification type resources where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @return the matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndLocale(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationTypeIdAndLocale(qualificationTypeId,
            country, language);
    }

    /**
    * Returns a range of all the qualification type resources where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @return the range of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndLocale(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationTypeIdAndLocale(qualificationTypeId,
            country, language, start, end);
    }

    /**
    * Returns an ordered range of all the qualification type resources where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findByQualificationTypeIdAndLocale(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByQualificationTypeIdAndLocale(qualificationTypeId,
            country, language, start, end, orderByComparator);
    }

    /**
    * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource findByQualificationTypeIdAndLocale_First(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException {
        return getPersistence()
                   .findByQualificationTypeIdAndLocale_First(qualificationTypeId,
            country, language, orderByComparator);
    }

    /**
    * Returns the first qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource fetchByQualificationTypeIdAndLocale_First(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationTypeIdAndLocale_First(qualificationTypeId,
            country, language, orderByComparator);
    }

    /**
    * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource findByQualificationTypeIdAndLocale_Last(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException {
        return getPersistence()
                   .findByQualificationTypeIdAndLocale_Last(qualificationTypeId,
            country, language, orderByComparator);
    }

    /**
    * Returns the last qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification type resource, or <code>null</code> if a matching qualification type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource fetchByQualificationTypeIdAndLocale_Last(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByQualificationTypeIdAndLocale_Last(qualificationTypeId,
            country, language, orderByComparator);
    }

    /**
    * Returns the qualification type resources before and after the current qualification type resource in the ordered set where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationTypeResourceId the primary key of the current qualification type resource
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource[] findByQualificationTypeIdAndLocale_PrevAndNext(
        long qualificationTypeResourceId, long qualificationTypeId,
        java.lang.String country, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException {
        return getPersistence()
                   .findByQualificationTypeIdAndLocale_PrevAndNext(qualificationTypeResourceId,
            qualificationTypeId, country, language, orderByComparator);
    }

    /**
    * Removes all the qualification type resources where qualificationTypeId = &#63; and country = &#63; and language = &#63; from the database.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @throws SystemException if a system exception occurred
    */
    public static void removeByQualificationTypeIdAndLocale(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByQualificationTypeIdAndLocale(qualificationTypeId, country,
            language);
    }

    /**
    * Returns the number of qualification type resources where qualificationTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param qualificationTypeId the qualification type ID
    * @param country the country
    * @param language the language
    * @return the number of matching qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static int countByQualificationTypeIdAndLocale(
        long qualificationTypeId, java.lang.String country,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByQualificationTypeIdAndLocale(qualificationTypeId,
            country, language);
    }

    /**
    * Caches the qualification type resource in the entity cache if it is enabled.
    *
    * @param qualificationTypeResource the qualification type resource
    */
    public static void cacheResult(
        de.humance.eco.profile.model.QualificationTypeResource qualificationTypeResource) {
        getPersistence().cacheResult(qualificationTypeResource);
    }

    /**
    * Caches the qualification type resources in the entity cache if it is enabled.
    *
    * @param qualificationTypeResources the qualification type resources
    */
    public static void cacheResult(
        java.util.List<de.humance.eco.profile.model.QualificationTypeResource> qualificationTypeResources) {
        getPersistence().cacheResult(qualificationTypeResources);
    }

    /**
    * Creates a new qualification type resource with the primary key. Does not add the qualification type resource to the database.
    *
    * @param qualificationTypeResourceId the primary key for the new qualification type resource
    * @return the new qualification type resource
    */
    public static de.humance.eco.profile.model.QualificationTypeResource create(
        long qualificationTypeResourceId) {
        return getPersistence().create(qualificationTypeResourceId);
    }

    /**
    * Removes the qualification type resource with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationTypeResourceId the primary key of the qualification type resource
    * @return the qualification type resource that was removed
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource remove(
        long qualificationTypeResourceId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException {
        return getPersistence().remove(qualificationTypeResourceId);
    }

    public static de.humance.eco.profile.model.QualificationTypeResource updateImpl(
        de.humance.eco.profile.model.QualificationTypeResource qualificationTypeResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(qualificationTypeResource);
    }

    /**
    * Returns the qualification type resource with the primary key or throws a {@link de.humance.eco.profile.NoSuchQualificationTypeResourceException} if it could not be found.
    *
    * @param qualificationTypeResourceId the primary key of the qualification type resource
    * @return the qualification type resource
    * @throws de.humance.eco.profile.NoSuchQualificationTypeResourceException if a qualification type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource findByPrimaryKey(
        long qualificationTypeResourceId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationTypeResourceException {
        return getPersistence().findByPrimaryKey(qualificationTypeResourceId);
    }

    /**
    * Returns the qualification type resource with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param qualificationTypeResourceId the primary key of the qualification type resource
    * @return the qualification type resource, or <code>null</code> if a qualification type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.QualificationTypeResource fetchByPrimaryKey(
        long qualificationTypeResourceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(qualificationTypeResourceId);
    }

    /**
    * Returns all the qualification type resources.
    *
    * @return the qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the qualification type resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @return the range of qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the qualification type resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualification type resources
    * @param end the upper bound of the range of qualification type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.QualificationTypeResource> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the qualification type resources from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of qualification type resources.
    *
    * @return the number of qualification type resources
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static QualificationTypeResourcePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (QualificationTypeResourcePersistence) PortletBeanLocatorUtil.locate(de.humance.eco.profile.service.ClpSerializer.getServletContextName(),
                    QualificationTypeResourcePersistence.class.getName());

            ReferenceRegistry.registerReference(QualificationTypeResourceUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(QualificationTypeResourcePersistence persistence) {
    }
}
