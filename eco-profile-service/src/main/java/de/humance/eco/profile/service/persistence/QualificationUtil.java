package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import de.humance.eco.profile.model.Qualification;

import java.util.List;

/**
 * The persistence utility for the qualification service. This utility wraps {@link QualificationPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see QualificationPersistence
 * @see QualificationPersistenceImpl
 * @generated
 */
public class QualificationUtil {
    private static QualificationPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Qualification qualification) {
        getPersistence().clearCache(qualification);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Qualification> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Qualification> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Qualification> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Qualification update(Qualification qualification)
        throws SystemException {
        return getPersistence().update(qualification);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Qualification update(Qualification qualification,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(qualification, serviceContext);
    }

    /**
    * Returns all the qualifications where ownerId = &#63;.
    *
    * @param ownerId the owner ID
    * @return the matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Qualification> findByOwnerId(
        long ownerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByOwnerId(ownerId);
    }

    /**
    * Returns a range of all the qualifications where ownerId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param ownerId the owner ID
    * @param start the lower bound of the range of qualifications
    * @param end the upper bound of the range of qualifications (not inclusive)
    * @return the range of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Qualification> findByOwnerId(
        long ownerId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByOwnerId(ownerId, start, end);
    }

    /**
    * Returns an ordered range of all the qualifications where ownerId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param ownerId the owner ID
    * @param start the lower bound of the range of qualifications
    * @param end the upper bound of the range of qualifications (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Qualification> findByOwnerId(
        long ownerId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByOwnerId(ownerId, start, end, orderByComparator);
    }

    /**
    * Returns the first qualification in the ordered set where ownerId = &#63;.
    *
    * @param ownerId the owner ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification findByOwnerId_First(
        long ownerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException {
        return getPersistence().findByOwnerId_First(ownerId, orderByComparator);
    }

    /**
    * Returns the first qualification in the ordered set where ownerId = &#63;.
    *
    * @param ownerId the owner ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification, or <code>null</code> if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification fetchByOwnerId_First(
        long ownerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByOwnerId_First(ownerId, orderByComparator);
    }

    /**
    * Returns the last qualification in the ordered set where ownerId = &#63;.
    *
    * @param ownerId the owner ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification findByOwnerId_Last(
        long ownerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException {
        return getPersistence().findByOwnerId_Last(ownerId, orderByComparator);
    }

    /**
    * Returns the last qualification in the ordered set where ownerId = &#63;.
    *
    * @param ownerId the owner ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification, or <code>null</code> if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification fetchByOwnerId_Last(
        long ownerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByOwnerId_Last(ownerId, orderByComparator);
    }

    /**
    * Returns the qualifications before and after the current qualification in the ordered set where ownerId = &#63;.
    *
    * @param qualificationId the primary key of the current qualification
    * @param ownerId the owner ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a qualification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification[] findByOwnerId_PrevAndNext(
        long qualificationId, long ownerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException {
        return getPersistence()
                   .findByOwnerId_PrevAndNext(qualificationId, ownerId,
            orderByComparator);
    }

    /**
    * Removes all the qualifications where ownerId = &#63; from the database.
    *
    * @param ownerId the owner ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByOwnerId(long ownerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByOwnerId(ownerId);
    }

    /**
    * Returns the number of qualifications where ownerId = &#63;.
    *
    * @param ownerId the owner ID
    * @return the number of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public static int countByOwnerId(long ownerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByOwnerId(ownerId);
    }

    /**
    * Returns all the qualifications where typeId = &#63;.
    *
    * @param typeId the type ID
    * @return the matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Qualification> findByTypeId(
        long typeId) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTypeId(typeId);
    }

    /**
    * Returns a range of all the qualifications where typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param typeId the type ID
    * @param start the lower bound of the range of qualifications
    * @param end the upper bound of the range of qualifications (not inclusive)
    * @return the range of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Qualification> findByTypeId(
        long typeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTypeId(typeId, start, end);
    }

    /**
    * Returns an ordered range of all the qualifications where typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param typeId the type ID
    * @param start the lower bound of the range of qualifications
    * @param end the upper bound of the range of qualifications (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Qualification> findByTypeId(
        long typeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByTypeId(typeId, start, end, orderByComparator);
    }

    /**
    * Returns the first qualification in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification findByTypeId_First(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException {
        return getPersistence().findByTypeId_First(typeId, orderByComparator);
    }

    /**
    * Returns the first qualification in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification, or <code>null</code> if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification fetchByTypeId_First(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByTypeId_First(typeId, orderByComparator);
    }

    /**
    * Returns the last qualification in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification findByTypeId_Last(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException {
        return getPersistence().findByTypeId_Last(typeId, orderByComparator);
    }

    /**
    * Returns the last qualification in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification, or <code>null</code> if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification fetchByTypeId_Last(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByTypeId_Last(typeId, orderByComparator);
    }

    /**
    * Returns the qualifications before and after the current qualification in the ordered set where typeId = &#63;.
    *
    * @param qualificationId the primary key of the current qualification
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a qualification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification[] findByTypeId_PrevAndNext(
        long qualificationId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException {
        return getPersistence()
                   .findByTypeId_PrevAndNext(qualificationId, typeId,
            orderByComparator);
    }

    /**
    * Removes all the qualifications where typeId = &#63; from the database.
    *
    * @param typeId the type ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByTypeId(long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByTypeId(typeId);
    }

    /**
    * Returns the number of qualifications where typeId = &#63;.
    *
    * @param typeId the type ID
    * @return the number of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public static int countByTypeId(long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByTypeId(typeId);
    }

    /**
    * Returns all the qualifications where ownerId = &#63; and typeId = &#63;.
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @return the matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Qualification> findByOwnerIdAndTypeId(
        long ownerId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByOwnerIdAndTypeId(ownerId, typeId);
    }

    /**
    * Returns a range of all the qualifications where ownerId = &#63; and typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @param start the lower bound of the range of qualifications
    * @param end the upper bound of the range of qualifications (not inclusive)
    * @return the range of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Qualification> findByOwnerIdAndTypeId(
        long ownerId, long typeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByOwnerIdAndTypeId(ownerId, typeId, start, end);
    }

    /**
    * Returns an ordered range of all the qualifications where ownerId = &#63; and typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @param start the lower bound of the range of qualifications
    * @param end the upper bound of the range of qualifications (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Qualification> findByOwnerIdAndTypeId(
        long ownerId, long typeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByOwnerIdAndTypeId(ownerId, typeId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first qualification in the ordered set where ownerId = &#63; and typeId = &#63;.
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification findByOwnerIdAndTypeId_First(
        long ownerId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException {
        return getPersistence()
                   .findByOwnerIdAndTypeId_First(ownerId, typeId,
            orderByComparator);
    }

    /**
    * Returns the first qualification in the ordered set where ownerId = &#63; and typeId = &#63;.
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching qualification, or <code>null</code> if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification fetchByOwnerIdAndTypeId_First(
        long ownerId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByOwnerIdAndTypeId_First(ownerId, typeId,
            orderByComparator);
    }

    /**
    * Returns the last qualification in the ordered set where ownerId = &#63; and typeId = &#63;.
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification findByOwnerIdAndTypeId_Last(
        long ownerId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException {
        return getPersistence()
                   .findByOwnerIdAndTypeId_Last(ownerId, typeId,
            orderByComparator);
    }

    /**
    * Returns the last qualification in the ordered set where ownerId = &#63; and typeId = &#63;.
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching qualification, or <code>null</code> if a matching qualification could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification fetchByOwnerIdAndTypeId_Last(
        long ownerId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByOwnerIdAndTypeId_Last(ownerId, typeId,
            orderByComparator);
    }

    /**
    * Returns the qualifications before and after the current qualification in the ordered set where ownerId = &#63; and typeId = &#63;.
    *
    * @param qualificationId the primary key of the current qualification
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a qualification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification[] findByOwnerIdAndTypeId_PrevAndNext(
        long qualificationId, long ownerId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException {
        return getPersistence()
                   .findByOwnerIdAndTypeId_PrevAndNext(qualificationId,
            ownerId, typeId, orderByComparator);
    }

    /**
    * Removes all the qualifications where ownerId = &#63; and typeId = &#63; from the database.
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByOwnerIdAndTypeId(long ownerId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByOwnerIdAndTypeId(ownerId, typeId);
    }

    /**
    * Returns the number of qualifications where ownerId = &#63; and typeId = &#63;.
    *
    * @param ownerId the owner ID
    * @param typeId the type ID
    * @return the number of matching qualifications
    * @throws SystemException if a system exception occurred
    */
    public static int countByOwnerIdAndTypeId(long ownerId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByOwnerIdAndTypeId(ownerId, typeId);
    }

    /**
    * Caches the qualification in the entity cache if it is enabled.
    *
    * @param qualification the qualification
    */
    public static void cacheResult(
        de.humance.eco.profile.model.Qualification qualification) {
        getPersistence().cacheResult(qualification);
    }

    /**
    * Caches the qualifications in the entity cache if it is enabled.
    *
    * @param qualifications the qualifications
    */
    public static void cacheResult(
        java.util.List<de.humance.eco.profile.model.Qualification> qualifications) {
        getPersistence().cacheResult(qualifications);
    }

    /**
    * Creates a new qualification with the primary key. Does not add the qualification to the database.
    *
    * @param qualificationId the primary key for the new qualification
    * @return the new qualification
    */
    public static de.humance.eco.profile.model.Qualification create(
        long qualificationId) {
        return getPersistence().create(qualificationId);
    }

    /**
    * Removes the qualification with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param qualificationId the primary key of the qualification
    * @return the qualification that was removed
    * @throws de.humance.eco.profile.NoSuchQualificationException if a qualification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification remove(
        long qualificationId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException {
        return getPersistence().remove(qualificationId);
    }

    public static de.humance.eco.profile.model.Qualification updateImpl(
        de.humance.eco.profile.model.Qualification qualification)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(qualification);
    }

    /**
    * Returns the qualification with the primary key or throws a {@link de.humance.eco.profile.NoSuchQualificationException} if it could not be found.
    *
    * @param qualificationId the primary key of the qualification
    * @return the qualification
    * @throws de.humance.eco.profile.NoSuchQualificationException if a qualification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification findByPrimaryKey(
        long qualificationId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchQualificationException {
        return getPersistence().findByPrimaryKey(qualificationId);
    }

    /**
    * Returns the qualification with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param qualificationId the primary key of the qualification
    * @return the qualification, or <code>null</code> if a qualification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Qualification fetchByPrimaryKey(
        long qualificationId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(qualificationId);
    }

    /**
    * Returns all the qualifications.
    *
    * @return the qualifications
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Qualification> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the qualifications.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualifications
    * @param end the upper bound of the range of qualifications (not inclusive)
    * @return the range of qualifications
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Qualification> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the qualifications.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.QualificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of qualifications
    * @param end the upper bound of the range of qualifications (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of qualifications
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Qualification> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the qualifications from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of qualifications.
    *
    * @return the number of qualifications
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static QualificationPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (QualificationPersistence) PortletBeanLocatorUtil.locate(de.humance.eco.profile.service.ClpSerializer.getServletContextName(),
                    QualificationPersistence.class.getName());

            ReferenceRegistry.registerReference(QualificationUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(QualificationPersistence persistence) {
    }
}
