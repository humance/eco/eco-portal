package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.Trailer;
import de.humance.eco.profile.service.TrailerLocalServiceUtil;

/**
 * @author Humance
 * @generated
 */
public abstract class TrailerActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public TrailerActionableDynamicQuery() throws SystemException {
        setBaseLocalService(TrailerLocalServiceUtil.getService());
        setClass(Trailer.class);

        setClassLoader(de.humance.eco.profile.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("trailerId");
    }
}
