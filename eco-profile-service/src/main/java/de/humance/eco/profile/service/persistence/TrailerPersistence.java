package de.humance.eco.profile.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import de.humance.eco.profile.model.Trailer;

/**
 * The persistence interface for the trailer service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see TrailerPersistenceImpl
 * @see TrailerUtil
 * @generated
 */
public interface TrailerPersistence extends BasePersistence<Trailer> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link TrailerUtil} to access the trailer persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the trailers where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByDriverId(
        long driverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the trailers where driverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByDriverId(
        long driverId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the trailers where driverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByDriverId(
        long driverId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first trailer in the ordered set where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByDriverId_First(
        long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the first trailer in the ordered set where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByDriverId_First(
        long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last trailer in the ordered set where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByDriverId_Last(
        long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the last trailer in the ordered set where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByDriverId_Last(
        long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the trailers before and after the current trailer in the ordered set where driverId = &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer[] findByDriverId_PrevAndNext(
        long trailerId, long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Removes all the trailers where driverId = &#63; from the database.
    *
    * @param driverId the driver ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByDriverId(long driverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of trailers where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public int countByDriverId(long driverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the trailers where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByDriverIdAndOrgaId(
        long driverId, long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the trailers where driverId = &#63; and organizationId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByDriverIdAndOrgaId(
        long driverId, long organizationId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the trailers where driverId = &#63; and organizationId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByDriverIdAndOrgaId(
        long driverId, long organizationId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first trailer in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByDriverIdAndOrgaId_First(
        long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the first trailer in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByDriverIdAndOrgaId_First(
        long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last trailer in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByDriverIdAndOrgaId_Last(
        long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the last trailer in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByDriverIdAndOrgaId_Last(
        long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the trailers before and after the current trailer in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer[] findByDriverIdAndOrgaId_PrevAndNext(
        long trailerId, long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Removes all the trailers where driverId = &#63; and organizationId = &#63; from the database.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByDriverIdAndOrgaId(long driverId, long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of trailers where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public int countByDriverIdAndOrgaId(long driverId, long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the trailers where typeId = &#63;.
    *
    * @param typeId the type ID
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByTypeId(
        long typeId) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the trailers where typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param typeId the type ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByTypeId(
        long typeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the trailers where typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param typeId the type ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByTypeId(
        long typeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first trailer in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByTypeId_First(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the first trailer in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByTypeId_First(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last trailer in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByTypeId_Last(long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the last trailer in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByTypeId_Last(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the trailers before and after the current trailer in the ordered set where typeId = &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer[] findByTypeId_PrevAndNext(
        long trailerId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Removes all the trailers where typeId = &#63; from the database.
    *
    * @param typeId the type ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByTypeId(long typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of trailers where typeId = &#63;.
    *
    * @param typeId the type ID
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public int countByTypeId(long typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the trailers where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByDriverIdAndTypeId(
        long driverId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the trailers where driverId = &#63; and typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByDriverIdAndTypeId(
        long driverId, long typeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the trailers where driverId = &#63; and typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByDriverIdAndTypeId(
        long driverId, long typeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first trailer in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByDriverIdAndTypeId_First(
        long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the first trailer in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByDriverIdAndTypeId_First(
        long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last trailer in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByDriverIdAndTypeId_Last(
        long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the last trailer in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByDriverIdAndTypeId_Last(
        long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the trailers before and after the current trailer in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer[] findByDriverIdAndTypeId_PrevAndNext(
        long trailerId, long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Removes all the trailers where driverId = &#63; and typeId = &#63; from the database.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByDriverIdAndTypeId(long driverId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of trailers where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public int countByDriverIdAndTypeId(long driverId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the trailers where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByManufacturerId(
        long manufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the trailers where manufacturerId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param manufacturerId the manufacturer ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByManufacturerId(
        long manufacturerId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the trailers where manufacturerId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param manufacturerId the manufacturer ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByManufacturerId(
        long manufacturerId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first trailer in the ordered set where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByManufacturerId_First(
        long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the first trailer in the ordered set where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByManufacturerId_First(
        long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last trailer in the ordered set where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByManufacturerId_Last(
        long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the last trailer in the ordered set where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByManufacturerId_Last(
        long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the trailers before and after the current trailer in the ordered set where manufacturerId = &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer[] findByManufacturerId_PrevAndNext(
        long trailerId, long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Removes all the trailers where manufacturerId = &#63; from the database.
    *
    * @param manufacturerId the manufacturer ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByManufacturerId(long manufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of trailers where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public int countByManufacturerId(long manufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the trailers where modelName = &#63;.
    *
    * @param modelName the model name
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByModelName(
        java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the trailers where modelName = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param modelName the model name
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByModelName(
        java.lang.String modelName, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the trailers where modelName = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param modelName the model name
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByModelName(
        java.lang.String modelName, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first trailer in the ordered set where modelName = &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByModelName_First(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the first trailer in the ordered set where modelName = &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByModelName_First(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last trailer in the ordered set where modelName = &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByModelName_Last(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the last trailer in the ordered set where modelName = &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByModelName_Last(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the trailers before and after the current trailer in the ordered set where modelName = &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer[] findByModelName_PrevAndNext(
        long trailerId, java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Removes all the trailers where modelName = &#63; from the database.
    *
    * @param modelName the model name
    * @throws SystemException if a system exception occurred
    */
    public void removeByModelName(java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of trailers where modelName = &#63;.
    *
    * @param modelName the model name
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public int countByModelName(java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the trailers where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByModelNameLike(
        java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the trailers where modelName LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param modelName the model name
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByModelNameLike(
        java.lang.String modelName, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the trailers where modelName LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param modelName the model name
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByModelNameLike(
        java.lang.String modelName, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first trailer in the ordered set where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByModelNameLike_First(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the first trailer in the ordered set where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByModelNameLike_First(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last trailer in the ordered set where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByModelNameLike_Last(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the last trailer in the ordered set where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByModelNameLike_Last(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the trailers before and after the current trailer in the ordered set where modelName LIKE &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer[] findByModelNameLike_PrevAndNext(
        long trailerId, java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Removes all the trailers where modelName LIKE &#63; from the database.
    *
    * @param modelName the model name
    * @throws SystemException if a system exception occurred
    */
    public void removeByModelNameLike(java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of trailers where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public int countByModelNameLike(java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the trailers where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByLicensePlate(
        java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the trailers where licensePlate = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param licensePlate the license plate
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByLicensePlate(
        java.lang.String licensePlate, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the trailers where licensePlate = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param licensePlate the license plate
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByLicensePlate(
        java.lang.String licensePlate, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first trailer in the ordered set where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByLicensePlate_First(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the first trailer in the ordered set where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByLicensePlate_First(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last trailer in the ordered set where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByLicensePlate_Last(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the last trailer in the ordered set where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByLicensePlate_Last(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the trailers before and after the current trailer in the ordered set where licensePlate = &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer[] findByLicensePlate_PrevAndNext(
        long trailerId, java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Removes all the trailers where licensePlate = &#63; from the database.
    *
    * @param licensePlate the license plate
    * @throws SystemException if a system exception occurred
    */
    public void removeByLicensePlate(java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of trailers where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public int countByLicensePlate(java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the trailers where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByLicensePlateLike(
        java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the trailers where licensePlate LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param licensePlate the license plate
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByLicensePlateLike(
        java.lang.String licensePlate, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the trailers where licensePlate LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param licensePlate the license plate
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findByLicensePlateLike(
        java.lang.String licensePlate, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first trailer in the ordered set where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByLicensePlateLike_First(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the first trailer in the ordered set where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByLicensePlateLike_First(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last trailer in the ordered set where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByLicensePlateLike_Last(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the last trailer in the ordered set where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByLicensePlateLike_Last(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the trailers before and after the current trailer in the ordered set where licensePlate LIKE &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer[] findByLicensePlateLike_PrevAndNext(
        long trailerId, java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Removes all the trailers where licensePlate LIKE &#63; from the database.
    *
    * @param licensePlate the license plate
    * @throws SystemException if a system exception occurred
    */
    public void removeByLicensePlateLike(java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of trailers where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public int countByLicensePlateLike(java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the trailer in the entity cache if it is enabled.
    *
    * @param trailer the trailer
    */
    public void cacheResult(de.humance.eco.profile.model.Trailer trailer);

    /**
    * Caches the trailers in the entity cache if it is enabled.
    *
    * @param trailers the trailers
    */
    public void cacheResult(
        java.util.List<de.humance.eco.profile.model.Trailer> trailers);

    /**
    * Creates a new trailer with the primary key. Does not add the trailer to the database.
    *
    * @param trailerId the primary key for the new trailer
    * @return the new trailer
    */
    public de.humance.eco.profile.model.Trailer create(long trailerId);

    /**
    * Removes the trailer with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param trailerId the primary key of the trailer
    * @return the trailer that was removed
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer remove(long trailerId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    public de.humance.eco.profile.model.Trailer updateImpl(
        de.humance.eco.profile.model.Trailer trailer)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the trailer with the primary key or throws a {@link de.humance.eco.profile.NoSuchTrailerException} if it could not be found.
    *
    * @param trailerId the primary key of the trailer
    * @return the trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer findByPrimaryKey(long trailerId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException;

    /**
    * Returns the trailer with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param trailerId the primary key of the trailer
    * @return the trailer, or <code>null</code> if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Trailer fetchByPrimaryKey(
        long trailerId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the trailers.
    *
    * @return the trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the trailers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the trailers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of trailers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Trailer> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the trailers from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of trailers.
    *
    * @return the number of trailers
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
