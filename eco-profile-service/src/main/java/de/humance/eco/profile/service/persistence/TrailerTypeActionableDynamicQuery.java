package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.TrailerType;
import de.humance.eco.profile.service.TrailerTypeLocalServiceUtil;

/**
 * @author Humance
 * @generated
 */
public abstract class TrailerTypeActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public TrailerTypeActionableDynamicQuery() throws SystemException {
        setBaseLocalService(TrailerTypeLocalServiceUtil.getService());
        setClass(TrailerType.class);

        setClassLoader(de.humance.eco.profile.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("trailerTypeId");
    }
}
