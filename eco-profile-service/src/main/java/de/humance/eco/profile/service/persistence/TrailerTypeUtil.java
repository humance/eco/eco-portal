package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import de.humance.eco.profile.model.TrailerType;

import java.util.List;

/**
 * The persistence utility for the trailer type service. This utility wraps {@link TrailerTypePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see TrailerTypePersistence
 * @see TrailerTypePersistenceImpl
 * @generated
 */
public class TrailerTypeUtil {
    private static TrailerTypePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(TrailerType trailerType) {
        getPersistence().clearCache(trailerType);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<TrailerType> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<TrailerType> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<TrailerType> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static TrailerType update(TrailerType trailerType)
        throws SystemException {
        return getPersistence().update(trailerType);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static TrailerType update(TrailerType trailerType,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(trailerType, serviceContext);
    }

    /**
    * Returns all the trailer types where name = &#63;.
    *
    * @param name the name
    * @return the matching trailer types
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerType> findByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByName(name);
    }

    /**
    * Returns a range of all the trailer types where name = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param name the name
    * @param start the lower bound of the range of trailer types
    * @param end the upper bound of the range of trailer types (not inclusive)
    * @return the range of matching trailer types
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerType> findByName(
        java.lang.String name, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByName(name, start, end);
    }

    /**
    * Returns an ordered range of all the trailer types where name = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param name the name
    * @param start the lower bound of the range of trailer types
    * @param end the upper bound of the range of trailer types (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailer types
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerType> findByName(
        java.lang.String name, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByName(name, start, end, orderByComparator);
    }

    /**
    * Returns the first trailer type in the ordered set where name = &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer type
    * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a matching trailer type could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType findByName_First(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerTypeException {
        return getPersistence().findByName_First(name, orderByComparator);
    }

    /**
    * Returns the first trailer type in the ordered set where name = &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer type, or <code>null</code> if a matching trailer type could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType fetchByName_First(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByName_First(name, orderByComparator);
    }

    /**
    * Returns the last trailer type in the ordered set where name = &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer type
    * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a matching trailer type could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType findByName_Last(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerTypeException {
        return getPersistence().findByName_Last(name, orderByComparator);
    }

    /**
    * Returns the last trailer type in the ordered set where name = &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer type, or <code>null</code> if a matching trailer type could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType fetchByName_Last(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByName_Last(name, orderByComparator);
    }

    /**
    * Returns the trailer types before and after the current trailer type in the ordered set where name = &#63;.
    *
    * @param trailerTypeId the primary key of the current trailer type
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer type
    * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a trailer type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType[] findByName_PrevAndNext(
        long trailerTypeId, java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerTypeException {
        return getPersistence()
                   .findByName_PrevAndNext(trailerTypeId, name,
            orderByComparator);
    }

    /**
    * Removes all the trailer types where name = &#63; from the database.
    *
    * @param name the name
    * @throws SystemException if a system exception occurred
    */
    public static void removeByName(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByName(name);
    }

    /**
    * Returns the number of trailer types where name = &#63;.
    *
    * @param name the name
    * @return the number of matching trailer types
    * @throws SystemException if a system exception occurred
    */
    public static int countByName(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByName(name);
    }

    /**
    * Returns all the trailer types where name LIKE &#63;.
    *
    * @param name the name
    * @return the matching trailer types
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerType> findByNameLike(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByNameLike(name);
    }

    /**
    * Returns a range of all the trailer types where name LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param name the name
    * @param start the lower bound of the range of trailer types
    * @param end the upper bound of the range of trailer types (not inclusive)
    * @return the range of matching trailer types
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerType> findByNameLike(
        java.lang.String name, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByNameLike(name, start, end);
    }

    /**
    * Returns an ordered range of all the trailer types where name LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param name the name
    * @param start the lower bound of the range of trailer types
    * @param end the upper bound of the range of trailer types (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailer types
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerType> findByNameLike(
        java.lang.String name, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByNameLike(name, start, end, orderByComparator);
    }

    /**
    * Returns the first trailer type in the ordered set where name LIKE &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer type
    * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a matching trailer type could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType findByNameLike_First(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerTypeException {
        return getPersistence().findByNameLike_First(name, orderByComparator);
    }

    /**
    * Returns the first trailer type in the ordered set where name LIKE &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer type, or <code>null</code> if a matching trailer type could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType fetchByNameLike_First(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByNameLike_First(name, orderByComparator);
    }

    /**
    * Returns the last trailer type in the ordered set where name LIKE &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer type
    * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a matching trailer type could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType findByNameLike_Last(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerTypeException {
        return getPersistence().findByNameLike_Last(name, orderByComparator);
    }

    /**
    * Returns the last trailer type in the ordered set where name LIKE &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer type, or <code>null</code> if a matching trailer type could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType fetchByNameLike_Last(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByNameLike_Last(name, orderByComparator);
    }

    /**
    * Returns the trailer types before and after the current trailer type in the ordered set where name LIKE &#63;.
    *
    * @param trailerTypeId the primary key of the current trailer type
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer type
    * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a trailer type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType[] findByNameLike_PrevAndNext(
        long trailerTypeId, java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerTypeException {
        return getPersistence()
                   .findByNameLike_PrevAndNext(trailerTypeId, name,
            orderByComparator);
    }

    /**
    * Removes all the trailer types where name LIKE &#63; from the database.
    *
    * @param name the name
    * @throws SystemException if a system exception occurred
    */
    public static void removeByNameLike(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByNameLike(name);
    }

    /**
    * Returns the number of trailer types where name LIKE &#63;.
    *
    * @param name the name
    * @return the number of matching trailer types
    * @throws SystemException if a system exception occurred
    */
    public static int countByNameLike(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByNameLike(name);
    }

    /**
    * Caches the trailer type in the entity cache if it is enabled.
    *
    * @param trailerType the trailer type
    */
    public static void cacheResult(
        de.humance.eco.profile.model.TrailerType trailerType) {
        getPersistence().cacheResult(trailerType);
    }

    /**
    * Caches the trailer types in the entity cache if it is enabled.
    *
    * @param trailerTypes the trailer types
    */
    public static void cacheResult(
        java.util.List<de.humance.eco.profile.model.TrailerType> trailerTypes) {
        getPersistence().cacheResult(trailerTypes);
    }

    /**
    * Creates a new trailer type with the primary key. Does not add the trailer type to the database.
    *
    * @param trailerTypeId the primary key for the new trailer type
    * @return the new trailer type
    */
    public static de.humance.eco.profile.model.TrailerType create(
        long trailerTypeId) {
        return getPersistence().create(trailerTypeId);
    }

    /**
    * Removes the trailer type with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param trailerTypeId the primary key of the trailer type
    * @return the trailer type that was removed
    * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a trailer type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType remove(
        long trailerTypeId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerTypeException {
        return getPersistence().remove(trailerTypeId);
    }

    public static de.humance.eco.profile.model.TrailerType updateImpl(
        de.humance.eco.profile.model.TrailerType trailerType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(trailerType);
    }

    /**
    * Returns the trailer type with the primary key or throws a {@link de.humance.eco.profile.NoSuchTrailerTypeException} if it could not be found.
    *
    * @param trailerTypeId the primary key of the trailer type
    * @return the trailer type
    * @throws de.humance.eco.profile.NoSuchTrailerTypeException if a trailer type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType findByPrimaryKey(
        long trailerTypeId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerTypeException {
        return getPersistence().findByPrimaryKey(trailerTypeId);
    }

    /**
    * Returns the trailer type with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param trailerTypeId the primary key of the trailer type
    * @return the trailer type, or <code>null</code> if a trailer type with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerType fetchByPrimaryKey(
        long trailerTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(trailerTypeId);
    }

    /**
    * Returns all the trailer types.
    *
    * @return the trailer types
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerType> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the trailer types.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of trailer types
    * @param end the upper bound of the range of trailer types (not inclusive)
    * @return the range of trailer types
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerType> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the trailer types.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of trailer types
    * @param end the upper bound of the range of trailer types (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of trailer types
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerType> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the trailer types from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of trailer types.
    *
    * @return the number of trailer types
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static TrailerTypePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (TrailerTypePersistence) PortletBeanLocatorUtil.locate(de.humance.eco.profile.service.ClpSerializer.getServletContextName(),
                    TrailerTypePersistence.class.getName());

            ReferenceRegistry.registerReference(TrailerTypeUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(TrailerTypePersistence persistence) {
    }
}
