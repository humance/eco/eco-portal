package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.TrailerUser;
import de.humance.eco.profile.service.TrailerUserLocalServiceUtil;

/**
 * @author Humance
 * @generated
 */
public abstract class TrailerUserActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public TrailerUserActionableDynamicQuery() throws SystemException {
        setBaseLocalService(TrailerUserLocalServiceUtil.getService());
        setClass(TrailerUser.class);

        setClassLoader(de.humance.eco.profile.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("trailerUserId");
    }
}
