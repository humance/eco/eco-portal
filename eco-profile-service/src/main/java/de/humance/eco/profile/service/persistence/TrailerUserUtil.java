package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import de.humance.eco.profile.model.TrailerUser;

import java.util.List;

/**
 * The persistence utility for the trailer user service. This utility wraps {@link TrailerUserPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see TrailerUserPersistence
 * @see TrailerUserPersistenceImpl
 * @generated
 */
public class TrailerUserUtil {
    private static TrailerUserPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(TrailerUser trailerUser) {
        getPersistence().clearCache(trailerUser);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<TrailerUser> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<TrailerUser> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<TrailerUser> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static TrailerUser update(TrailerUser trailerUser)
        throws SystemException {
        return getPersistence().update(trailerUser);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static TrailerUser update(TrailerUser trailerUser,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(trailerUser, serviceContext);
    }

    /**
    * Returns all the trailer users where userId = &#63;.
    *
    * @param userId the user ID
    * @return the matching trailer users
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerUser> findByUserId(
        long userId) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByUserId(userId);
    }

    /**
    * Returns a range of all the trailer users where userId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param userId the user ID
    * @param start the lower bound of the range of trailer users
    * @param end the upper bound of the range of trailer users (not inclusive)
    * @return the range of matching trailer users
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerUser> findByUserId(
        long userId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByUserId(userId, start, end);
    }

    /**
    * Returns an ordered range of all the trailer users where userId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param userId the user ID
    * @param start the lower bound of the range of trailer users
    * @param end the upper bound of the range of trailer users (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailer users
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerUser> findByUserId(
        long userId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByUserId(userId, start, end, orderByComparator);
    }

    /**
    * Returns the first trailer user in the ordered set where userId = &#63;.
    *
    * @param userId the user ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer user
    * @throws de.humance.eco.profile.NoSuchTrailerUserException if a matching trailer user could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerUser findByUserId_First(
        long userId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerUserException {
        return getPersistence().findByUserId_First(userId, orderByComparator);
    }

    /**
    * Returns the first trailer user in the ordered set where userId = &#63;.
    *
    * @param userId the user ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer user, or <code>null</code> if a matching trailer user could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerUser fetchByUserId_First(
        long userId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByUserId_First(userId, orderByComparator);
    }

    /**
    * Returns the last trailer user in the ordered set where userId = &#63;.
    *
    * @param userId the user ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer user
    * @throws de.humance.eco.profile.NoSuchTrailerUserException if a matching trailer user could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerUser findByUserId_Last(
        long userId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerUserException {
        return getPersistence().findByUserId_Last(userId, orderByComparator);
    }

    /**
    * Returns the last trailer user in the ordered set where userId = &#63;.
    *
    * @param userId the user ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer user, or <code>null</code> if a matching trailer user could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerUser fetchByUserId_Last(
        long userId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByUserId_Last(userId, orderByComparator);
    }

    /**
    * Returns the trailer users before and after the current trailer user in the ordered set where userId = &#63;.
    *
    * @param trailerUserId the primary key of the current trailer user
    * @param userId the user ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer user
    * @throws de.humance.eco.profile.NoSuchTrailerUserException if a trailer user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerUser[] findByUserId_PrevAndNext(
        long trailerUserId, long userId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerUserException {
        return getPersistence()
                   .findByUserId_PrevAndNext(trailerUserId, userId,
            orderByComparator);
    }

    /**
    * Removes all the trailer users where userId = &#63; from the database.
    *
    * @param userId the user ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByUserId(long userId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByUserId(userId);
    }

    /**
    * Returns the number of trailer users where userId = &#63;.
    *
    * @param userId the user ID
    * @return the number of matching trailer users
    * @throws SystemException if a system exception occurred
    */
    public static int countByUserId(long userId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByUserId(userId);
    }

    /**
    * Returns all the trailer users where trailerId = &#63;.
    *
    * @param trailerId the trailer ID
    * @return the matching trailer users
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerUser> findByTrailerId(
        long trailerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTrailerId(trailerId);
    }

    /**
    * Returns a range of all the trailer users where trailerId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param trailerId the trailer ID
    * @param start the lower bound of the range of trailer users
    * @param end the upper bound of the range of trailer users (not inclusive)
    * @return the range of matching trailer users
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerUser> findByTrailerId(
        long trailerId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTrailerId(trailerId, start, end);
    }

    /**
    * Returns an ordered range of all the trailer users where trailerId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param trailerId the trailer ID
    * @param start the lower bound of the range of trailer users
    * @param end the upper bound of the range of trailer users (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailer users
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerUser> findByTrailerId(
        long trailerId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByTrailerId(trailerId, start, end, orderByComparator);
    }

    /**
    * Returns the first trailer user in the ordered set where trailerId = &#63;.
    *
    * @param trailerId the trailer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer user
    * @throws de.humance.eco.profile.NoSuchTrailerUserException if a matching trailer user could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerUser findByTrailerId_First(
        long trailerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerUserException {
        return getPersistence()
                   .findByTrailerId_First(trailerId, orderByComparator);
    }

    /**
    * Returns the first trailer user in the ordered set where trailerId = &#63;.
    *
    * @param trailerId the trailer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer user, or <code>null</code> if a matching trailer user could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerUser fetchByTrailerId_First(
        long trailerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByTrailerId_First(trailerId, orderByComparator);
    }

    /**
    * Returns the last trailer user in the ordered set where trailerId = &#63;.
    *
    * @param trailerId the trailer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer user
    * @throws de.humance.eco.profile.NoSuchTrailerUserException if a matching trailer user could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerUser findByTrailerId_Last(
        long trailerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerUserException {
        return getPersistence()
                   .findByTrailerId_Last(trailerId, orderByComparator);
    }

    /**
    * Returns the last trailer user in the ordered set where trailerId = &#63;.
    *
    * @param trailerId the trailer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer user, or <code>null</code> if a matching trailer user could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerUser fetchByTrailerId_Last(
        long trailerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByTrailerId_Last(trailerId, orderByComparator);
    }

    /**
    * Returns the trailer users before and after the current trailer user in the ordered set where trailerId = &#63;.
    *
    * @param trailerUserId the primary key of the current trailer user
    * @param trailerId the trailer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer user
    * @throws de.humance.eco.profile.NoSuchTrailerUserException if a trailer user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerUser[] findByTrailerId_PrevAndNext(
        long trailerUserId, long trailerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerUserException {
        return getPersistence()
                   .findByTrailerId_PrevAndNext(trailerUserId, trailerId,
            orderByComparator);
    }

    /**
    * Removes all the trailer users where trailerId = &#63; from the database.
    *
    * @param trailerId the trailer ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByTrailerId(long trailerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByTrailerId(trailerId);
    }

    /**
    * Returns the number of trailer users where trailerId = &#63;.
    *
    * @param trailerId the trailer ID
    * @return the number of matching trailer users
    * @throws SystemException if a system exception occurred
    */
    public static int countByTrailerId(long trailerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByTrailerId(trailerId);
    }

    /**
    * Caches the trailer user in the entity cache if it is enabled.
    *
    * @param trailerUser the trailer user
    */
    public static void cacheResult(
        de.humance.eco.profile.model.TrailerUser trailerUser) {
        getPersistence().cacheResult(trailerUser);
    }

    /**
    * Caches the trailer users in the entity cache if it is enabled.
    *
    * @param trailerUsers the trailer users
    */
    public static void cacheResult(
        java.util.List<de.humance.eco.profile.model.TrailerUser> trailerUsers) {
        getPersistence().cacheResult(trailerUsers);
    }

    /**
    * Creates a new trailer user with the primary key. Does not add the trailer user to the database.
    *
    * @param trailerUserId the primary key for the new trailer user
    * @return the new trailer user
    */
    public static de.humance.eco.profile.model.TrailerUser create(
        long trailerUserId) {
        return getPersistence().create(trailerUserId);
    }

    /**
    * Removes the trailer user with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param trailerUserId the primary key of the trailer user
    * @return the trailer user that was removed
    * @throws de.humance.eco.profile.NoSuchTrailerUserException if a trailer user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerUser remove(
        long trailerUserId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerUserException {
        return getPersistence().remove(trailerUserId);
    }

    public static de.humance.eco.profile.model.TrailerUser updateImpl(
        de.humance.eco.profile.model.TrailerUser trailerUser)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(trailerUser);
    }

    /**
    * Returns the trailer user with the primary key or throws a {@link de.humance.eco.profile.NoSuchTrailerUserException} if it could not be found.
    *
    * @param trailerUserId the primary key of the trailer user
    * @return the trailer user
    * @throws de.humance.eco.profile.NoSuchTrailerUserException if a trailer user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerUser findByPrimaryKey(
        long trailerUserId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerUserException {
        return getPersistence().findByPrimaryKey(trailerUserId);
    }

    /**
    * Returns the trailer user with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param trailerUserId the primary key of the trailer user
    * @return the trailer user, or <code>null</code> if a trailer user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.TrailerUser fetchByPrimaryKey(
        long trailerUserId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(trailerUserId);
    }

    /**
    * Returns all the trailer users.
    *
    * @return the trailer users
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerUser> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the trailer users.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of trailer users
    * @param end the upper bound of the range of trailer users (not inclusive)
    * @return the range of trailer users
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerUser> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the trailer users.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of trailer users
    * @param end the upper bound of the range of trailer users (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of trailer users
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.TrailerUser> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the trailer users from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of trailer users.
    *
    * @return the number of trailer users
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static TrailerUserPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (TrailerUserPersistence) PortletBeanLocatorUtil.locate(de.humance.eco.profile.service.ClpSerializer.getServletContextName(),
                    TrailerUserPersistence.class.getName());

            ReferenceRegistry.registerReference(TrailerUserUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(TrailerUserPersistence persistence) {
    }
}
