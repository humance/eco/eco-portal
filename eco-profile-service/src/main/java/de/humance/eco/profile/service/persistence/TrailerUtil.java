package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import de.humance.eco.profile.model.Trailer;

import java.util.List;

/**
 * The persistence utility for the trailer service. This utility wraps {@link TrailerPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see TrailerPersistence
 * @see TrailerPersistenceImpl
 * @generated
 */
public class TrailerUtil {
    private static TrailerPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Trailer trailer) {
        getPersistence().clearCache(trailer);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Trailer> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Trailer> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Trailer> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Trailer update(Trailer trailer) throws SystemException {
        return getPersistence().update(trailer);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Trailer update(Trailer trailer, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(trailer, serviceContext);
    }

    /**
    * Returns all the trailers where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByDriverId(
        long driverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDriverId(driverId);
    }

    /**
    * Returns a range of all the trailers where driverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByDriverId(
        long driverId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDriverId(driverId, start, end);
    }

    /**
    * Returns an ordered range of all the trailers where driverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByDriverId(
        long driverId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDriverId(driverId, start, end, orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByDriverId_First(
        long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence().findByDriverId_First(driverId, orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByDriverId_First(
        long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByDriverId_First(driverId, orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByDriverId_Last(
        long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence().findByDriverId_Last(driverId, orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByDriverId_Last(
        long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByDriverId_Last(driverId, orderByComparator);
    }

    /**
    * Returns the trailers before and after the current trailer in the ordered set where driverId = &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer[] findByDriverId_PrevAndNext(
        long trailerId, long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByDriverId_PrevAndNext(trailerId, driverId,
            orderByComparator);
    }

    /**
    * Removes all the trailers where driverId = &#63; from the database.
    *
    * @param driverId the driver ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByDriverId(long driverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByDriverId(driverId);
    }

    /**
    * Returns the number of trailers where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static int countByDriverId(long driverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByDriverId(driverId);
    }

    /**
    * Returns all the trailers where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByDriverIdAndOrgaId(
        long driverId, long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDriverIdAndOrgaId(driverId, organizationId);
    }

    /**
    * Returns a range of all the trailers where driverId = &#63; and organizationId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByDriverIdAndOrgaId(
        long driverId, long organizationId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDriverIdAndOrgaId(driverId, organizationId, start, end);
    }

    /**
    * Returns an ordered range of all the trailers where driverId = &#63; and organizationId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByDriverIdAndOrgaId(
        long driverId, long organizationId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDriverIdAndOrgaId(driverId, organizationId, start,
            end, orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByDriverIdAndOrgaId_First(
        long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByDriverIdAndOrgaId_First(driverId, organizationId,
            orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByDriverIdAndOrgaId_First(
        long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByDriverIdAndOrgaId_First(driverId, organizationId,
            orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByDriverIdAndOrgaId_Last(
        long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByDriverIdAndOrgaId_Last(driverId, organizationId,
            orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByDriverIdAndOrgaId_Last(
        long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByDriverIdAndOrgaId_Last(driverId, organizationId,
            orderByComparator);
    }

    /**
    * Returns the trailers before and after the current trailer in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer[] findByDriverIdAndOrgaId_PrevAndNext(
        long trailerId, long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByDriverIdAndOrgaId_PrevAndNext(trailerId, driverId,
            organizationId, orderByComparator);
    }

    /**
    * Removes all the trailers where driverId = &#63; and organizationId = &#63; from the database.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByDriverIdAndOrgaId(long driverId,
        long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByDriverIdAndOrgaId(driverId, organizationId);
    }

    /**
    * Returns the number of trailers where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static int countByDriverIdAndOrgaId(long driverId,
        long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByDriverIdAndOrgaId(driverId, organizationId);
    }

    /**
    * Returns all the trailers where typeId = &#63;.
    *
    * @param typeId the type ID
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByTypeId(
        long typeId) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTypeId(typeId);
    }

    /**
    * Returns a range of all the trailers where typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param typeId the type ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByTypeId(
        long typeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTypeId(typeId, start, end);
    }

    /**
    * Returns an ordered range of all the trailers where typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param typeId the type ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByTypeId(
        long typeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByTypeId(typeId, start, end, orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByTypeId_First(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence().findByTypeId_First(typeId, orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByTypeId_First(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByTypeId_First(typeId, orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByTypeId_Last(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence().findByTypeId_Last(typeId, orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByTypeId_Last(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByTypeId_Last(typeId, orderByComparator);
    }

    /**
    * Returns the trailers before and after the current trailer in the ordered set where typeId = &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer[] findByTypeId_PrevAndNext(
        long trailerId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByTypeId_PrevAndNext(trailerId, typeId,
            orderByComparator);
    }

    /**
    * Removes all the trailers where typeId = &#63; from the database.
    *
    * @param typeId the type ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByTypeId(long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByTypeId(typeId);
    }

    /**
    * Returns the number of trailers where typeId = &#63;.
    *
    * @param typeId the type ID
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static int countByTypeId(long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByTypeId(typeId);
    }

    /**
    * Returns all the trailers where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByDriverIdAndTypeId(
        long driverId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDriverIdAndTypeId(driverId, typeId);
    }

    /**
    * Returns a range of all the trailers where driverId = &#63; and typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByDriverIdAndTypeId(
        long driverId, long typeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDriverIdAndTypeId(driverId, typeId, start, end);
    }

    /**
    * Returns an ordered range of all the trailers where driverId = &#63; and typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByDriverIdAndTypeId(
        long driverId, long typeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDriverIdAndTypeId(driverId, typeId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByDriverIdAndTypeId_First(
        long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByDriverIdAndTypeId_First(driverId, typeId,
            orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByDriverIdAndTypeId_First(
        long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByDriverIdAndTypeId_First(driverId, typeId,
            orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByDriverIdAndTypeId_Last(
        long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByDriverIdAndTypeId_Last(driverId, typeId,
            orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByDriverIdAndTypeId_Last(
        long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByDriverIdAndTypeId_Last(driverId, typeId,
            orderByComparator);
    }

    /**
    * Returns the trailers before and after the current trailer in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer[] findByDriverIdAndTypeId_PrevAndNext(
        long trailerId, long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByDriverIdAndTypeId_PrevAndNext(trailerId, driverId,
            typeId, orderByComparator);
    }

    /**
    * Removes all the trailers where driverId = &#63; and typeId = &#63; from the database.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByDriverIdAndTypeId(long driverId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByDriverIdAndTypeId(driverId, typeId);
    }

    /**
    * Returns the number of trailers where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static int countByDriverIdAndTypeId(long driverId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByDriverIdAndTypeId(driverId, typeId);
    }

    /**
    * Returns all the trailers where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByManufacturerId(
        long manufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByManufacturerId(manufacturerId);
    }

    /**
    * Returns a range of all the trailers where manufacturerId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param manufacturerId the manufacturer ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByManufacturerId(
        long manufacturerId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByManufacturerId(manufacturerId, start, end);
    }

    /**
    * Returns an ordered range of all the trailers where manufacturerId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param manufacturerId the manufacturer ID
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByManufacturerId(
        long manufacturerId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByManufacturerId(manufacturerId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByManufacturerId_First(
        long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByManufacturerId_First(manufacturerId, orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByManufacturerId_First(
        long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByManufacturerId_First(manufacturerId,
            orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByManufacturerId_Last(
        long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByManufacturerId_Last(manufacturerId, orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByManufacturerId_Last(
        long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByManufacturerId_Last(manufacturerId, orderByComparator);
    }

    /**
    * Returns the trailers before and after the current trailer in the ordered set where manufacturerId = &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer[] findByManufacturerId_PrevAndNext(
        long trailerId, long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByManufacturerId_PrevAndNext(trailerId, manufacturerId,
            orderByComparator);
    }

    /**
    * Removes all the trailers where manufacturerId = &#63; from the database.
    *
    * @param manufacturerId the manufacturer ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByManufacturerId(long manufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByManufacturerId(manufacturerId);
    }

    /**
    * Returns the number of trailers where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static int countByManufacturerId(long manufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByManufacturerId(manufacturerId);
    }

    /**
    * Returns all the trailers where modelName = &#63;.
    *
    * @param modelName the model name
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByModelName(
        java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByModelName(modelName);
    }

    /**
    * Returns a range of all the trailers where modelName = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param modelName the model name
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByModelName(
        java.lang.String modelName, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByModelName(modelName, start, end);
    }

    /**
    * Returns an ordered range of all the trailers where modelName = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param modelName the model name
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByModelName(
        java.lang.String modelName, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByModelName(modelName, start, end, orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where modelName = &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByModelName_First(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByModelName_First(modelName, orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where modelName = &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByModelName_First(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByModelName_First(modelName, orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where modelName = &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByModelName_Last(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByModelName_Last(modelName, orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where modelName = &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByModelName_Last(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByModelName_Last(modelName, orderByComparator);
    }

    /**
    * Returns the trailers before and after the current trailer in the ordered set where modelName = &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer[] findByModelName_PrevAndNext(
        long trailerId, java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByModelName_PrevAndNext(trailerId, modelName,
            orderByComparator);
    }

    /**
    * Removes all the trailers where modelName = &#63; from the database.
    *
    * @param modelName the model name
    * @throws SystemException if a system exception occurred
    */
    public static void removeByModelName(java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByModelName(modelName);
    }

    /**
    * Returns the number of trailers where modelName = &#63;.
    *
    * @param modelName the model name
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static int countByModelName(java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByModelName(modelName);
    }

    /**
    * Returns all the trailers where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByModelNameLike(
        java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByModelNameLike(modelName);
    }

    /**
    * Returns a range of all the trailers where modelName LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param modelName the model name
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByModelNameLike(
        java.lang.String modelName, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByModelNameLike(modelName, start, end);
    }

    /**
    * Returns an ordered range of all the trailers where modelName LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param modelName the model name
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByModelNameLike(
        java.lang.String modelName, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByModelNameLike(modelName, start, end, orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByModelNameLike_First(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByModelNameLike_First(modelName, orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByModelNameLike_First(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByModelNameLike_First(modelName, orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByModelNameLike_Last(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByModelNameLike_Last(modelName, orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByModelNameLike_Last(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByModelNameLike_Last(modelName, orderByComparator);
    }

    /**
    * Returns the trailers before and after the current trailer in the ordered set where modelName LIKE &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer[] findByModelNameLike_PrevAndNext(
        long trailerId, java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByModelNameLike_PrevAndNext(trailerId, modelName,
            orderByComparator);
    }

    /**
    * Removes all the trailers where modelName LIKE &#63; from the database.
    *
    * @param modelName the model name
    * @throws SystemException if a system exception occurred
    */
    public static void removeByModelNameLike(java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByModelNameLike(modelName);
    }

    /**
    * Returns the number of trailers where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static int countByModelNameLike(java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByModelNameLike(modelName);
    }

    /**
    * Returns all the trailers where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByLicensePlate(
        java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByLicensePlate(licensePlate);
    }

    /**
    * Returns a range of all the trailers where licensePlate = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param licensePlate the license plate
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByLicensePlate(
        java.lang.String licensePlate, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByLicensePlate(licensePlate, start, end);
    }

    /**
    * Returns an ordered range of all the trailers where licensePlate = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param licensePlate the license plate
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByLicensePlate(
        java.lang.String licensePlate, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByLicensePlate(licensePlate, start, end,
            orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByLicensePlate_First(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByLicensePlate_First(licensePlate, orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByLicensePlate_First(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByLicensePlate_First(licensePlate, orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByLicensePlate_Last(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByLicensePlate_Last(licensePlate, orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByLicensePlate_Last(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByLicensePlate_Last(licensePlate, orderByComparator);
    }

    /**
    * Returns the trailers before and after the current trailer in the ordered set where licensePlate = &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer[] findByLicensePlate_PrevAndNext(
        long trailerId, java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByLicensePlate_PrevAndNext(trailerId, licensePlate,
            orderByComparator);
    }

    /**
    * Removes all the trailers where licensePlate = &#63; from the database.
    *
    * @param licensePlate the license plate
    * @throws SystemException if a system exception occurred
    */
    public static void removeByLicensePlate(java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByLicensePlate(licensePlate);
    }

    /**
    * Returns the number of trailers where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static int countByLicensePlate(java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByLicensePlate(licensePlate);
    }

    /**
    * Returns all the trailers where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @return the matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByLicensePlateLike(
        java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByLicensePlateLike(licensePlate);
    }

    /**
    * Returns a range of all the trailers where licensePlate LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param licensePlate the license plate
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByLicensePlateLike(
        java.lang.String licensePlate, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByLicensePlateLike(licensePlate, start, end);
    }

    /**
    * Returns an ordered range of all the trailers where licensePlate LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param licensePlate the license plate
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findByLicensePlateLike(
        java.lang.String licensePlate, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByLicensePlateLike(licensePlate, start, end,
            orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByLicensePlateLike_First(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByLicensePlateLike_First(licensePlate, orderByComparator);
    }

    /**
    * Returns the first trailer in the ordered set where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByLicensePlateLike_First(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByLicensePlateLike_First(licensePlate,
            orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByLicensePlateLike_Last(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByLicensePlateLike_Last(licensePlate, orderByComparator);
    }

    /**
    * Returns the last trailer in the ordered set where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching trailer, or <code>null</code> if a matching trailer could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByLicensePlateLike_Last(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByLicensePlateLike_Last(licensePlate, orderByComparator);
    }

    /**
    * Returns the trailers before and after the current trailer in the ordered set where licensePlate LIKE &#63;.
    *
    * @param trailerId the primary key of the current trailer
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer[] findByLicensePlateLike_PrevAndNext(
        long trailerId, java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence()
                   .findByLicensePlateLike_PrevAndNext(trailerId, licensePlate,
            orderByComparator);
    }

    /**
    * Removes all the trailers where licensePlate LIKE &#63; from the database.
    *
    * @param licensePlate the license plate
    * @throws SystemException if a system exception occurred
    */
    public static void removeByLicensePlateLike(java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByLicensePlateLike(licensePlate);
    }

    /**
    * Returns the number of trailers where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @return the number of matching trailers
    * @throws SystemException if a system exception occurred
    */
    public static int countByLicensePlateLike(java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByLicensePlateLike(licensePlate);
    }

    /**
    * Caches the trailer in the entity cache if it is enabled.
    *
    * @param trailer the trailer
    */
    public static void cacheResult(de.humance.eco.profile.model.Trailer trailer) {
        getPersistence().cacheResult(trailer);
    }

    /**
    * Caches the trailers in the entity cache if it is enabled.
    *
    * @param trailers the trailers
    */
    public static void cacheResult(
        java.util.List<de.humance.eco.profile.model.Trailer> trailers) {
        getPersistence().cacheResult(trailers);
    }

    /**
    * Creates a new trailer with the primary key. Does not add the trailer to the database.
    *
    * @param trailerId the primary key for the new trailer
    * @return the new trailer
    */
    public static de.humance.eco.profile.model.Trailer create(long trailerId) {
        return getPersistence().create(trailerId);
    }

    /**
    * Removes the trailer with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param trailerId the primary key of the trailer
    * @return the trailer that was removed
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer remove(long trailerId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence().remove(trailerId);
    }

    public static de.humance.eco.profile.model.Trailer updateImpl(
        de.humance.eco.profile.model.Trailer trailer)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(trailer);
    }

    /**
    * Returns the trailer with the primary key or throws a {@link de.humance.eco.profile.NoSuchTrailerException} if it could not be found.
    *
    * @param trailerId the primary key of the trailer
    * @return the trailer
    * @throws de.humance.eco.profile.NoSuchTrailerException if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer findByPrimaryKey(
        long trailerId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchTrailerException {
        return getPersistence().findByPrimaryKey(trailerId);
    }

    /**
    * Returns the trailer with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param trailerId the primary key of the trailer
    * @return the trailer, or <code>null</code> if a trailer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Trailer fetchByPrimaryKey(
        long trailerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(trailerId);
    }

    /**
    * Returns all the trailers.
    *
    * @return the trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the trailers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @return the range of trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the trailers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.TrailerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of trailers
    * @param end the upper bound of the range of trailers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of trailers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Trailer> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the trailers from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of trailers.
    *
    * @return the number of trailers
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static TrailerPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (TrailerPersistence) PortletBeanLocatorUtil.locate(de.humance.eco.profile.service.ClpSerializer.getServletContextName(),
                    TrailerPersistence.class.getName());

            ReferenceRegistry.registerReference(TrailerUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(TrailerPersistence persistence) {
    }
}
