package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.Vehicle;
import de.humance.eco.profile.service.VehicleLocalServiceUtil;

/**
 * @author Humance
 * @generated
 */
public abstract class VehicleActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public VehicleActionableDynamicQuery() throws SystemException {
        setBaseLocalService(VehicleLocalServiceUtil.getService());
        setClass(Vehicle.class);

        setClassLoader(de.humance.eco.profile.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("vehicleId");
    }
}
