package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.VehicleAttributeDefinition;
import de.humance.eco.profile.service.VehicleAttributeDefinitionLocalServiceUtil;

/**
 * @author Humance
 * @generated
 */
public abstract class VehicleAttributeDefinitionActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public VehicleAttributeDefinitionActionableDynamicQuery()
        throws SystemException {
        setBaseLocalService(VehicleAttributeDefinitionLocalServiceUtil.getService());
        setClass(VehicleAttributeDefinition.class);

        setClassLoader(de.humance.eco.profile.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("vehicleAttributeDefinitionId");
    }
}
