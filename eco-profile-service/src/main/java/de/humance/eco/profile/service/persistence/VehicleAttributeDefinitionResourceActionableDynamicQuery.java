package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.VehicleAttributeDefinitionResource;
import de.humance.eco.profile.service.VehicleAttributeDefinitionResourceLocalServiceUtil;

/**
 * @author Humance
 * @generated
 */
public abstract class VehicleAttributeDefinitionResourceActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public VehicleAttributeDefinitionResourceActionableDynamicQuery()
        throws SystemException {
        setBaseLocalService(VehicleAttributeDefinitionResourceLocalServiceUtil.getService());
        setClass(VehicleAttributeDefinitionResource.class);

        setClassLoader(de.humance.eco.profile.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("vehicleAttributeDefinitionResourceId");
    }
}
