package de.humance.eco.profile.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import de.humance.eco.profile.model.VehicleAttributeDefinitionResource;

/**
 * The persistence interface for the vehicle attribute definition resource service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehicleAttributeDefinitionResourcePersistenceImpl
 * @see VehicleAttributeDefinitionResourceUtil
 * @generated
 */
public interface VehicleAttributeDefinitionResourcePersistence
    extends BasePersistence<VehicleAttributeDefinitionResource> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link VehicleAttributeDefinitionResourceUtil} to access the vehicle attribute definition resource persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @return the matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @return the range of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionId_First(
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException;

    /**
    * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionId_First(
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionId_Last(
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException;

    /**
    * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionId_Last(
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle attribute definition resources before and after the current vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionResourceId the primary key of the current vehicle attribute definition resource
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource[] findByVehicleAttributeDefinitionId_PrevAndNext(
        long vehicleAttributeDefinitionResourceId,
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException;

    /**
    * Removes all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; from the database.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @return the number of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public int countByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @return the matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndCountry(
        long vehicleAttributeDefinitionId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @return the range of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndCountry(
        long vehicleAttributeDefinitionId, java.lang.String country, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndCountry(
        long vehicleAttributeDefinitionId, java.lang.String country, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndCountry_First(
        long vehicleAttributeDefinitionId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException;

    /**
    * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndCountry_First(
        long vehicleAttributeDefinitionId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndCountry_Last(
        long vehicleAttributeDefinitionId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException;

    /**
    * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndCountry_Last(
        long vehicleAttributeDefinitionId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle attribute definition resources before and after the current vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param vehicleAttributeDefinitionResourceId the primary key of the current vehicle attribute definition resource
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource[] findByVehicleAttributeDefinitionIdAndCountry_PrevAndNext(
        long vehicleAttributeDefinitionResourceId,
        long vehicleAttributeDefinitionId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException;

    /**
    * Removes all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; from the database.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @throws SystemException if a system exception occurred
    */
    public void removeByVehicleAttributeDefinitionIdAndCountry(
        long vehicleAttributeDefinitionId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @return the number of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public int countByVehicleAttributeDefinitionIdAndCountry(
        long vehicleAttributeDefinitionId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @return the matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLanguage(
        long vehicleAttributeDefinitionId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @return the range of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLanguage(
        long vehicleAttributeDefinitionId, java.lang.String language,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLanguage(
        long vehicleAttributeDefinitionId, java.lang.String language,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndLanguage_First(
        long vehicleAttributeDefinitionId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException;

    /**
    * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndLanguage_First(
        long vehicleAttributeDefinitionId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndLanguage_Last(
        long vehicleAttributeDefinitionId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException;

    /**
    * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndLanguage_Last(
        long vehicleAttributeDefinitionId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle attribute definition resources before and after the current vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionResourceId the primary key of the current vehicle attribute definition resource
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource[] findByVehicleAttributeDefinitionIdAndLanguage_PrevAndNext(
        long vehicleAttributeDefinitionResourceId,
        long vehicleAttributeDefinitionId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException;

    /**
    * Removes all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and language = &#63; from the database.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @throws SystemException if a system exception occurred
    */
    public void removeByVehicleAttributeDefinitionIdAndLanguage(
        long vehicleAttributeDefinitionId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @return the number of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public int countByVehicleAttributeDefinitionIdAndLanguage(
        long vehicleAttributeDefinitionId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @return the matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLocale(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @return the range of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLocale(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLocale(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndLocale_First(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException;

    /**
    * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndLocale_First(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndLocale_Last(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException;

    /**
    * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndLocale_Last(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle attribute definition resources before and after the current vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionResourceId the primary key of the current vehicle attribute definition resource
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource[] findByVehicleAttributeDefinitionIdAndLocale_PrevAndNext(
        long vehicleAttributeDefinitionResourceId,
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException;

    /**
    * Removes all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63; from the database.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @throws SystemException if a system exception occurred
    */
    public void removeByVehicleAttributeDefinitionIdAndLocale(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @return the number of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public int countByVehicleAttributeDefinitionIdAndLocale(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the vehicle attribute definition resource in the entity cache if it is enabled.
    *
    * @param vehicleAttributeDefinitionResource the vehicle attribute definition resource
    */
    public void cacheResult(
        de.humance.eco.profile.model.VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource);

    /**
    * Caches the vehicle attribute definition resources in the entity cache if it is enabled.
    *
    * @param vehicleAttributeDefinitionResources the vehicle attribute definition resources
    */
    public void cacheResult(
        java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> vehicleAttributeDefinitionResources);

    /**
    * Creates a new vehicle attribute definition resource with the primary key. Does not add the vehicle attribute definition resource to the database.
    *
    * @param vehicleAttributeDefinitionResourceId the primary key for the new vehicle attribute definition resource
    * @return the new vehicle attribute definition resource
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource create(
        long vehicleAttributeDefinitionResourceId);

    /**
    * Removes the vehicle attribute definition resource with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleAttributeDefinitionResourceId the primary key of the vehicle attribute definition resource
    * @return the vehicle attribute definition resource that was removed
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource remove(
        long vehicleAttributeDefinitionResourceId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException;

    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource updateImpl(
        de.humance.eco.profile.model.VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle attribute definition resource with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException} if it could not be found.
    *
    * @param vehicleAttributeDefinitionResourceId the primary key of the vehicle attribute definition resource
    * @return the vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByPrimaryKey(
        long vehicleAttributeDefinitionResourceId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException;

    /**
    * Returns the vehicle attribute definition resource with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param vehicleAttributeDefinitionResourceId the primary key of the vehicle attribute definition resource
    * @return the vehicle attribute definition resource, or <code>null</code> if a vehicle attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByPrimaryKey(
        long vehicleAttributeDefinitionResourceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicle attribute definition resources.
    *
    * @return the vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle attribute definition resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @return the range of vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicle attribute definition resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the vehicle attribute definition resources from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle attribute definition resources.
    *
    * @return the number of vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
