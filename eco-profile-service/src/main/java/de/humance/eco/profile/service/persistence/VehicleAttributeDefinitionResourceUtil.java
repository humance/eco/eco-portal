package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import de.humance.eco.profile.model.VehicleAttributeDefinitionResource;

import java.util.List;

/**
 * The persistence utility for the vehicle attribute definition resource service. This utility wraps {@link VehicleAttributeDefinitionResourcePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehicleAttributeDefinitionResourcePersistence
 * @see VehicleAttributeDefinitionResourcePersistenceImpl
 * @generated
 */
public class VehicleAttributeDefinitionResourceUtil {
    private static VehicleAttributeDefinitionResourcePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource) {
        getPersistence().clearCache(vehicleAttributeDefinitionResource);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<VehicleAttributeDefinitionResource> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<VehicleAttributeDefinitionResource> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<VehicleAttributeDefinitionResource> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static VehicleAttributeDefinitionResource update(
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource)
        throws SystemException {
        return getPersistence().update(vehicleAttributeDefinitionResource);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static VehicleAttributeDefinitionResource update(
        VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence()
                   .update(vehicleAttributeDefinitionResource, serviceContext);
    }

    /**
    * Returns all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @return the matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
    }

    /**
    * Returns a range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @return the range of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId,
            start, end);
    }

    /**
    * Returns an ordered range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId,
            start, end, orderByComparator);
    }

    /**
    * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionId_First(
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionId_First(vehicleAttributeDefinitionId,
            orderByComparator);
    }

    /**
    * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionId_First(
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleAttributeDefinitionId_First(vehicleAttributeDefinitionId,
            orderByComparator);
    }

    /**
    * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionId_Last(
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionId_Last(vehicleAttributeDefinitionId,
            orderByComparator);
    }

    /**
    * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionId_Last(
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleAttributeDefinitionId_Last(vehicleAttributeDefinitionId,
            orderByComparator);
    }

    /**
    * Returns the vehicle attribute definition resources before and after the current vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionResourceId the primary key of the current vehicle attribute definition resource
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource[] findByVehicleAttributeDefinitionId_PrevAndNext(
        long vehicleAttributeDefinitionResourceId,
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionId_PrevAndNext(vehicleAttributeDefinitionResourceId,
            vehicleAttributeDefinitionId, orderByComparator);
    }

    /**
    * Removes all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; from the database.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
    }

    /**
    * Returns the number of vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @return the number of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static int countByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
    }

    /**
    * Returns all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @return the matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndCountry(
        long vehicleAttributeDefinitionId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndCountry(vehicleAttributeDefinitionId,
            country);
    }

    /**
    * Returns a range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @return the range of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndCountry(
        long vehicleAttributeDefinitionId, java.lang.String country, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndCountry(vehicleAttributeDefinitionId,
            country, start, end);
    }

    /**
    * Returns an ordered range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndCountry(
        long vehicleAttributeDefinitionId, java.lang.String country, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndCountry(vehicleAttributeDefinitionId,
            country, start, end, orderByComparator);
    }

    /**
    * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndCountry_First(
        long vehicleAttributeDefinitionId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndCountry_First(vehicleAttributeDefinitionId,
            country, orderByComparator);
    }

    /**
    * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndCountry_First(
        long vehicleAttributeDefinitionId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleAttributeDefinitionIdAndCountry_First(vehicleAttributeDefinitionId,
            country, orderByComparator);
    }

    /**
    * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndCountry_Last(
        long vehicleAttributeDefinitionId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndCountry_Last(vehicleAttributeDefinitionId,
            country, orderByComparator);
    }

    /**
    * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndCountry_Last(
        long vehicleAttributeDefinitionId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleAttributeDefinitionIdAndCountry_Last(vehicleAttributeDefinitionId,
            country, orderByComparator);
    }

    /**
    * Returns the vehicle attribute definition resources before and after the current vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param vehicleAttributeDefinitionResourceId the primary key of the current vehicle attribute definition resource
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource[] findByVehicleAttributeDefinitionIdAndCountry_PrevAndNext(
        long vehicleAttributeDefinitionResourceId,
        long vehicleAttributeDefinitionId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndCountry_PrevAndNext(vehicleAttributeDefinitionResourceId,
            vehicleAttributeDefinitionId, country, orderByComparator);
    }

    /**
    * Removes all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; from the database.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @throws SystemException if a system exception occurred
    */
    public static void removeByVehicleAttributeDefinitionIdAndCountry(
        long vehicleAttributeDefinitionId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByVehicleAttributeDefinitionIdAndCountry(vehicleAttributeDefinitionId,
            country);
    }

    /**
    * Returns the number of vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @return the number of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static int countByVehicleAttributeDefinitionIdAndCountry(
        long vehicleAttributeDefinitionId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByVehicleAttributeDefinitionIdAndCountry(vehicleAttributeDefinitionId,
            country);
    }

    /**
    * Returns all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @return the matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLanguage(
        long vehicleAttributeDefinitionId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndLanguage(vehicleAttributeDefinitionId,
            language);
    }

    /**
    * Returns a range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @return the range of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLanguage(
        long vehicleAttributeDefinitionId, java.lang.String language,
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndLanguage(vehicleAttributeDefinitionId,
            language, start, end);
    }

    /**
    * Returns an ordered range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLanguage(
        long vehicleAttributeDefinitionId, java.lang.String language,
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndLanguage(vehicleAttributeDefinitionId,
            language, start, end, orderByComparator);
    }

    /**
    * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndLanguage_First(
        long vehicleAttributeDefinitionId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndLanguage_First(vehicleAttributeDefinitionId,
            language, orderByComparator);
    }

    /**
    * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndLanguage_First(
        long vehicleAttributeDefinitionId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleAttributeDefinitionIdAndLanguage_First(vehicleAttributeDefinitionId,
            language, orderByComparator);
    }

    /**
    * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndLanguage_Last(
        long vehicleAttributeDefinitionId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndLanguage_Last(vehicleAttributeDefinitionId,
            language, orderByComparator);
    }

    /**
    * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndLanguage_Last(
        long vehicleAttributeDefinitionId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleAttributeDefinitionIdAndLanguage_Last(vehicleAttributeDefinitionId,
            language, orderByComparator);
    }

    /**
    * Returns the vehicle attribute definition resources before and after the current vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionResourceId the primary key of the current vehicle attribute definition resource
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource[] findByVehicleAttributeDefinitionIdAndLanguage_PrevAndNext(
        long vehicleAttributeDefinitionResourceId,
        long vehicleAttributeDefinitionId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndLanguage_PrevAndNext(vehicleAttributeDefinitionResourceId,
            vehicleAttributeDefinitionId, language, orderByComparator);
    }

    /**
    * Removes all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and language = &#63; from the database.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @throws SystemException if a system exception occurred
    */
    public static void removeByVehicleAttributeDefinitionIdAndLanguage(
        long vehicleAttributeDefinitionId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByVehicleAttributeDefinitionIdAndLanguage(vehicleAttributeDefinitionId,
            language);
    }

    /**
    * Returns the number of vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param language the language
    * @return the number of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static int countByVehicleAttributeDefinitionIdAndLanguage(
        long vehicleAttributeDefinitionId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByVehicleAttributeDefinitionIdAndLanguage(vehicleAttributeDefinitionId,
            language);
    }

    /**
    * Returns all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @return the matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLocale(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndLocale(vehicleAttributeDefinitionId,
            country, language);
    }

    /**
    * Returns a range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @return the range of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLocale(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndLocale(vehicleAttributeDefinitionId,
            country, language, start, end);
    }

    /**
    * Returns an ordered range of all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findByVehicleAttributeDefinitionIdAndLocale(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndLocale(vehicleAttributeDefinitionId,
            country, language, start, end, orderByComparator);
    }

    /**
    * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndLocale_First(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndLocale_First(vehicleAttributeDefinitionId,
            country, language, orderByComparator);
    }

    /**
    * Returns the first vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndLocale_First(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleAttributeDefinitionIdAndLocale_First(vehicleAttributeDefinitionId,
            country, language, orderByComparator);
    }

    /**
    * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByVehicleAttributeDefinitionIdAndLocale_Last(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndLocale_Last(vehicleAttributeDefinitionId,
            country, language, orderByComparator);
    }

    /**
    * Returns the last vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition resource, or <code>null</code> if a matching vehicle attribute definition resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByVehicleAttributeDefinitionIdAndLocale_Last(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleAttributeDefinitionIdAndLocale_Last(vehicleAttributeDefinitionId,
            country, language, orderByComparator);
    }

    /**
    * Returns the vehicle attribute definition resources before and after the current vehicle attribute definition resource in the ordered set where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionResourceId the primary key of the current vehicle attribute definition resource
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource[] findByVehicleAttributeDefinitionIdAndLocale_PrevAndNext(
        long vehicleAttributeDefinitionResourceId,
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionIdAndLocale_PrevAndNext(vehicleAttributeDefinitionResourceId,
            vehicleAttributeDefinitionId, country, language, orderByComparator);
    }

    /**
    * Removes all the vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63; from the database.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @throws SystemException if a system exception occurred
    */
    public static void removeByVehicleAttributeDefinitionIdAndLocale(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByVehicleAttributeDefinitionIdAndLocale(vehicleAttributeDefinitionId,
            country, language);
    }

    /**
    * Returns the number of vehicle attribute definition resources where vehicleAttributeDefinitionId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param country the country
    * @param language the language
    * @return the number of matching vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static int countByVehicleAttributeDefinitionIdAndLocale(
        long vehicleAttributeDefinitionId, java.lang.String country,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByVehicleAttributeDefinitionIdAndLocale(vehicleAttributeDefinitionId,
            country, language);
    }

    /**
    * Caches the vehicle attribute definition resource in the entity cache if it is enabled.
    *
    * @param vehicleAttributeDefinitionResource the vehicle attribute definition resource
    */
    public static void cacheResult(
        de.humance.eco.profile.model.VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource) {
        getPersistence().cacheResult(vehicleAttributeDefinitionResource);
    }

    /**
    * Caches the vehicle attribute definition resources in the entity cache if it is enabled.
    *
    * @param vehicleAttributeDefinitionResources the vehicle attribute definition resources
    */
    public static void cacheResult(
        java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> vehicleAttributeDefinitionResources) {
        getPersistence().cacheResult(vehicleAttributeDefinitionResources);
    }

    /**
    * Creates a new vehicle attribute definition resource with the primary key. Does not add the vehicle attribute definition resource to the database.
    *
    * @param vehicleAttributeDefinitionResourceId the primary key for the new vehicle attribute definition resource
    * @return the new vehicle attribute definition resource
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource create(
        long vehicleAttributeDefinitionResourceId) {
        return getPersistence().create(vehicleAttributeDefinitionResourceId);
    }

    /**
    * Removes the vehicle attribute definition resource with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleAttributeDefinitionResourceId the primary key of the vehicle attribute definition resource
    * @return the vehicle attribute definition resource that was removed
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource remove(
        long vehicleAttributeDefinitionResourceId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException {
        return getPersistence().remove(vehicleAttributeDefinitionResourceId);
    }

    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource updateImpl(
        de.humance.eco.profile.model.VehicleAttributeDefinitionResource vehicleAttributeDefinitionResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(vehicleAttributeDefinitionResource);
    }

    /**
    * Returns the vehicle attribute definition resource with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException} if it could not be found.
    *
    * @param vehicleAttributeDefinitionResourceId the primary key of the vehicle attribute definition resource
    * @return the vehicle attribute definition resource
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException if a vehicle attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource findByPrimaryKey(
        long vehicleAttributeDefinitionResourceId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionResourceException {
        return getPersistence()
                   .findByPrimaryKey(vehicleAttributeDefinitionResourceId);
    }

    /**
    * Returns the vehicle attribute definition resource with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param vehicleAttributeDefinitionResourceId the primary key of the vehicle attribute definition resource
    * @return the vehicle attribute definition resource, or <code>null</code> if a vehicle attribute definition resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinitionResource fetchByPrimaryKey(
        long vehicleAttributeDefinitionResourceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByPrimaryKey(vehicleAttributeDefinitionResourceId);
    }

    /**
    * Returns all the vehicle attribute definition resources.
    *
    * @return the vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the vehicle attribute definition resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @return the range of vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the vehicle attribute definition resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle attribute definition resources
    * @param end the upper bound of the range of vehicle attribute definition resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinitionResource> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the vehicle attribute definition resources from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of vehicle attribute definition resources.
    *
    * @return the number of vehicle attribute definition resources
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static VehicleAttributeDefinitionResourcePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (VehicleAttributeDefinitionResourcePersistence) PortletBeanLocatorUtil.locate(de.humance.eco.profile.service.ClpSerializer.getServletContextName(),
                    VehicleAttributeDefinitionResourcePersistence.class.getName());

            ReferenceRegistry.registerReference(VehicleAttributeDefinitionResourceUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(
        VehicleAttributeDefinitionResourcePersistence persistence) {
    }
}
