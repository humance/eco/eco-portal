package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import de.humance.eco.profile.model.VehicleAttributeDefinition;

import java.util.List;

/**
 * The persistence utility for the vehicle attribute definition service. This utility wraps {@link VehicleAttributeDefinitionPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehicleAttributeDefinitionPersistence
 * @see VehicleAttributeDefinitionPersistenceImpl
 * @generated
 */
public class VehicleAttributeDefinitionUtil {
    private static VehicleAttributeDefinitionPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(
        VehicleAttributeDefinition vehicleAttributeDefinition) {
        getPersistence().clearCache(vehicleAttributeDefinition);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<VehicleAttributeDefinition> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<VehicleAttributeDefinition> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<VehicleAttributeDefinition> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static VehicleAttributeDefinition update(
        VehicleAttributeDefinition vehicleAttributeDefinition)
        throws SystemException {
        return getPersistence().update(vehicleAttributeDefinition);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static VehicleAttributeDefinition update(
        VehicleAttributeDefinition vehicleAttributeDefinition,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence()
                   .update(vehicleAttributeDefinition, serviceContext);
    }

    /**
    * Returns all the vehicle attribute definitions where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @return the matching vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> findByVehicleTypeId(
        long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByVehicleTypeId(vehicleTypeId);
    }

    /**
    * Returns a range of all the vehicle attribute definitions where vehicleTypeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param start the lower bound of the range of vehicle attribute definitions
    * @param end the upper bound of the range of vehicle attribute definitions (not inclusive)
    * @return the range of matching vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> findByVehicleTypeId(
        long vehicleTypeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByVehicleTypeId(vehicleTypeId, start, end);
    }

    /**
    * Returns an ordered range of all the vehicle attribute definitions where vehicleTypeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param start the lower bound of the range of vehicle attribute definitions
    * @param end the upper bound of the range of vehicle attribute definitions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> findByVehicleTypeId(
        long vehicleTypeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleTypeId(vehicleTypeId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first vehicle attribute definition in the ordered set where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a matching vehicle attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition findByVehicleTypeId_First(
        long vehicleTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException {
        return getPersistence()
                   .findByVehicleTypeId_First(vehicleTypeId, orderByComparator);
    }

    /**
    * Returns the first vehicle attribute definition in the ordered set where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition, or <code>null</code> if a matching vehicle attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition fetchByVehicleTypeId_First(
        long vehicleTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleTypeId_First(vehicleTypeId, orderByComparator);
    }

    /**
    * Returns the last vehicle attribute definition in the ordered set where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a matching vehicle attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition findByVehicleTypeId_Last(
        long vehicleTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException {
        return getPersistence()
                   .findByVehicleTypeId_Last(vehicleTypeId, orderByComparator);
    }

    /**
    * Returns the last vehicle attribute definition in the ordered set where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition, or <code>null</code> if a matching vehicle attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition fetchByVehicleTypeId_Last(
        long vehicleTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleTypeId_Last(vehicleTypeId, orderByComparator);
    }

    /**
    * Returns the vehicle attribute definitions before and after the current vehicle attribute definition in the ordered set where vehicleTypeId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the primary key of the current vehicle attribute definition
    * @param vehicleTypeId the vehicle type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle attribute definition
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a vehicle attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition[] findByVehicleTypeId_PrevAndNext(
        long vehicleAttributeDefinitionId, long vehicleTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException {
        return getPersistence()
                   .findByVehicleTypeId_PrevAndNext(vehicleAttributeDefinitionId,
            vehicleTypeId, orderByComparator);
    }

    /**
    * Removes all the vehicle attribute definitions where vehicleTypeId = &#63; from the database.
    *
    * @param vehicleTypeId the vehicle type ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByVehicleTypeId(long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByVehicleTypeId(vehicleTypeId);
    }

    /**
    * Returns the number of vehicle attribute definitions where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @return the number of matching vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static int countByVehicleTypeId(long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByVehicleTypeId(vehicleTypeId);
    }

    /**
    * Returns all the vehicle attribute definitions where title = &#63;.
    *
    * @param title the title
    * @return the matching vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> findByTitle(
        java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTitle(title);
    }

    /**
    * Returns a range of all the vehicle attribute definitions where title = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param title the title
    * @param start the lower bound of the range of vehicle attribute definitions
    * @param end the upper bound of the range of vehicle attribute definitions (not inclusive)
    * @return the range of matching vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> findByTitle(
        java.lang.String title, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTitle(title, start, end);
    }

    /**
    * Returns an ordered range of all the vehicle attribute definitions where title = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param title the title
    * @param start the lower bound of the range of vehicle attribute definitions
    * @param end the upper bound of the range of vehicle attribute definitions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> findByTitle(
        java.lang.String title, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTitle(title, start, end, orderByComparator);
    }

    /**
    * Returns the first vehicle attribute definition in the ordered set where title = &#63;.
    *
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a matching vehicle attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition findByTitle_First(
        java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException {
        return getPersistence().findByTitle_First(title, orderByComparator);
    }

    /**
    * Returns the first vehicle attribute definition in the ordered set where title = &#63;.
    *
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition, or <code>null</code> if a matching vehicle attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition fetchByTitle_First(
        java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByTitle_First(title, orderByComparator);
    }

    /**
    * Returns the last vehicle attribute definition in the ordered set where title = &#63;.
    *
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a matching vehicle attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition findByTitle_Last(
        java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException {
        return getPersistence().findByTitle_Last(title, orderByComparator);
    }

    /**
    * Returns the last vehicle attribute definition in the ordered set where title = &#63;.
    *
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition, or <code>null</code> if a matching vehicle attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition fetchByTitle_Last(
        java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByTitle_Last(title, orderByComparator);
    }

    /**
    * Returns the vehicle attribute definitions before and after the current vehicle attribute definition in the ordered set where title = &#63;.
    *
    * @param vehicleAttributeDefinitionId the primary key of the current vehicle attribute definition
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle attribute definition
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a vehicle attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition[] findByTitle_PrevAndNext(
        long vehicleAttributeDefinitionId, java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException {
        return getPersistence()
                   .findByTitle_PrevAndNext(vehicleAttributeDefinitionId,
            title, orderByComparator);
    }

    /**
    * Removes all the vehicle attribute definitions where title = &#63; from the database.
    *
    * @param title the title
    * @throws SystemException if a system exception occurred
    */
    public static void removeByTitle(java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByTitle(title);
    }

    /**
    * Returns the number of vehicle attribute definitions where title = &#63;.
    *
    * @param title the title
    * @return the number of matching vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static int countByTitle(java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByTitle(title);
    }

    /**
    * Returns all the vehicle attribute definitions where title LIKE &#63;.
    *
    * @param title the title
    * @return the matching vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> findByTitleLike(
        java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTitleLike(title);
    }

    /**
    * Returns a range of all the vehicle attribute definitions where title LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param title the title
    * @param start the lower bound of the range of vehicle attribute definitions
    * @param end the upper bound of the range of vehicle attribute definitions (not inclusive)
    * @return the range of matching vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> findByTitleLike(
        java.lang.String title, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTitleLike(title, start, end);
    }

    /**
    * Returns an ordered range of all the vehicle attribute definitions where title LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param title the title
    * @param start the lower bound of the range of vehicle attribute definitions
    * @param end the upper bound of the range of vehicle attribute definitions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> findByTitleLike(
        java.lang.String title, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByTitleLike(title, start, end, orderByComparator);
    }

    /**
    * Returns the first vehicle attribute definition in the ordered set where title LIKE &#63;.
    *
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a matching vehicle attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition findByTitleLike_First(
        java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException {
        return getPersistence().findByTitleLike_First(title, orderByComparator);
    }

    /**
    * Returns the first vehicle attribute definition in the ordered set where title LIKE &#63;.
    *
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute definition, or <code>null</code> if a matching vehicle attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition fetchByTitleLike_First(
        java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByTitleLike_First(title, orderByComparator);
    }

    /**
    * Returns the last vehicle attribute definition in the ordered set where title LIKE &#63;.
    *
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a matching vehicle attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition findByTitleLike_Last(
        java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException {
        return getPersistence().findByTitleLike_Last(title, orderByComparator);
    }

    /**
    * Returns the last vehicle attribute definition in the ordered set where title LIKE &#63;.
    *
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute definition, or <code>null</code> if a matching vehicle attribute definition could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition fetchByTitleLike_Last(
        java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByTitleLike_Last(title, orderByComparator);
    }

    /**
    * Returns the vehicle attribute definitions before and after the current vehicle attribute definition in the ordered set where title LIKE &#63;.
    *
    * @param vehicleAttributeDefinitionId the primary key of the current vehicle attribute definition
    * @param title the title
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle attribute definition
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a vehicle attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition[] findByTitleLike_PrevAndNext(
        long vehicleAttributeDefinitionId, java.lang.String title,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException {
        return getPersistence()
                   .findByTitleLike_PrevAndNext(vehicleAttributeDefinitionId,
            title, orderByComparator);
    }

    /**
    * Removes all the vehicle attribute definitions where title LIKE &#63; from the database.
    *
    * @param title the title
    * @throws SystemException if a system exception occurred
    */
    public static void removeByTitleLike(java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByTitleLike(title);
    }

    /**
    * Returns the number of vehicle attribute definitions where title LIKE &#63;.
    *
    * @param title the title
    * @return the number of matching vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static int countByTitleLike(java.lang.String title)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByTitleLike(title);
    }

    /**
    * Caches the vehicle attribute definition in the entity cache if it is enabled.
    *
    * @param vehicleAttributeDefinition the vehicle attribute definition
    */
    public static void cacheResult(
        de.humance.eco.profile.model.VehicleAttributeDefinition vehicleAttributeDefinition) {
        getPersistence().cacheResult(vehicleAttributeDefinition);
    }

    /**
    * Caches the vehicle attribute definitions in the entity cache if it is enabled.
    *
    * @param vehicleAttributeDefinitions the vehicle attribute definitions
    */
    public static void cacheResult(
        java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> vehicleAttributeDefinitions) {
        getPersistence().cacheResult(vehicleAttributeDefinitions);
    }

    /**
    * Creates a new vehicle attribute definition with the primary key. Does not add the vehicle attribute definition to the database.
    *
    * @param vehicleAttributeDefinitionId the primary key for the new vehicle attribute definition
    * @return the new vehicle attribute definition
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition create(
        long vehicleAttributeDefinitionId) {
        return getPersistence().create(vehicleAttributeDefinitionId);
    }

    /**
    * Removes the vehicle attribute definition with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleAttributeDefinitionId the primary key of the vehicle attribute definition
    * @return the vehicle attribute definition that was removed
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a vehicle attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition remove(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException {
        return getPersistence().remove(vehicleAttributeDefinitionId);
    }

    public static de.humance.eco.profile.model.VehicleAttributeDefinition updateImpl(
        de.humance.eco.profile.model.VehicleAttributeDefinition vehicleAttributeDefinition)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(vehicleAttributeDefinition);
    }

    /**
    * Returns the vehicle attribute definition with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException} if it could not be found.
    *
    * @param vehicleAttributeDefinitionId the primary key of the vehicle attribute definition
    * @return the vehicle attribute definition
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException if a vehicle attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition findByPrimaryKey(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeDefinitionException {
        return getPersistence().findByPrimaryKey(vehicleAttributeDefinitionId);
    }

    /**
    * Returns the vehicle attribute definition with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param vehicleAttributeDefinitionId the primary key of the vehicle attribute definition
    * @return the vehicle attribute definition, or <code>null</code> if a vehicle attribute definition with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttributeDefinition fetchByPrimaryKey(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(vehicleAttributeDefinitionId);
    }

    /**
    * Returns all the vehicle attribute definitions.
    *
    * @return the vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the vehicle attribute definitions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle attribute definitions
    * @param end the upper bound of the range of vehicle attribute definitions (not inclusive)
    * @return the range of vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the vehicle attribute definitions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeDefinitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle attribute definitions
    * @param end the upper bound of the range of vehicle attribute definitions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttributeDefinition> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the vehicle attribute definitions from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of vehicle attribute definitions.
    *
    * @return the number of vehicle attribute definitions
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static VehicleAttributeDefinitionPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (VehicleAttributeDefinitionPersistence) PortletBeanLocatorUtil.locate(de.humance.eco.profile.service.ClpSerializer.getServletContextName(),
                    VehicleAttributeDefinitionPersistence.class.getName());

            ReferenceRegistry.registerReference(VehicleAttributeDefinitionUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(
        VehicleAttributeDefinitionPersistence persistence) {
    }
}
