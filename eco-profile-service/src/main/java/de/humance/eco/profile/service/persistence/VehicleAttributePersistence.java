package de.humance.eco.profile.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import de.humance.eco.profile.model.VehicleAttribute;

/**
 * The persistence interface for the vehicle attribute service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehicleAttributePersistenceImpl
 * @see VehicleAttributeUtil
 * @generated
 */
public interface VehicleAttributePersistence extends BasePersistence<VehicleAttribute> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link VehicleAttributeUtil} to access the vehicle attribute persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the vehicle attributes where vehicleId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @return the matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleId(
        long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle attributes where vehicleId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleId the vehicle ID
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @return the range of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleId(
        long vehicleId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicle attributes where vehicleId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleId the vehicle ID
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleId(
        long vehicleId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle attribute in the ordered set where vehicleId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute findByVehicleId_First(
        long vehicleId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException;

    /**
    * Returns the first vehicle attribute in the ordered set where vehicleId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute fetchByVehicleId_First(
        long vehicleId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle attribute in the ordered set where vehicleId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute findByVehicleId_Last(
        long vehicleId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException;

    /**
    * Returns the last vehicle attribute in the ordered set where vehicleId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute fetchByVehicleId_Last(
        long vehicleId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle attributes before and after the current vehicle attribute in the ordered set where vehicleId = &#63;.
    *
    * @param vehicleAttributeId the primary key of the current vehicle attribute
    * @param vehicleId the vehicle ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a vehicle attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute[] findByVehicleId_PrevAndNext(
        long vehicleAttributeId, long vehicleId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException;

    /**
    * Removes all the vehicle attributes where vehicleId = &#63; from the database.
    *
    * @param vehicleId the vehicle ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByVehicleId(long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle attributes where vehicleId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @return the number of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public int countByVehicleId(long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicle attributes where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @return the matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle attributes where vehicleAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @return the range of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicle attributes where vehicleAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle attribute in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute findByVehicleAttributeDefinitionId_First(
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException;

    /**
    * Returns the first vehicle attribute in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute fetchByVehicleAttributeDefinitionId_First(
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle attribute in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute findByVehicleAttributeDefinitionId_Last(
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException;

    /**
    * Returns the last vehicle attribute in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute fetchByVehicleAttributeDefinitionId_Last(
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle attributes before and after the current vehicle attribute in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeId the primary key of the current vehicle attribute
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a vehicle attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute[] findByVehicleAttributeDefinitionId_PrevAndNext(
        long vehicleAttributeId, long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException;

    /**
    * Removes all the vehicle attributes where vehicleAttributeDefinitionId = &#63; from the database.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle attributes where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @return the number of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public int countByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicle attributes where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @return the matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleIdAndAttributeDefinitionId(
        long vehicleId, long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle attributes where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @return the range of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleIdAndAttributeDefinitionId(
        long vehicleId, long vehicleAttributeDefinitionId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicle attributes where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleIdAndAttributeDefinitionId(
        long vehicleId, long vehicleAttributeDefinitionId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle attribute in the ordered set where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute findByVehicleIdAndAttributeDefinitionId_First(
        long vehicleId, long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException;

    /**
    * Returns the first vehicle attribute in the ordered set where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute fetchByVehicleIdAndAttributeDefinitionId_First(
        long vehicleId, long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle attribute in the ordered set where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute findByVehicleIdAndAttributeDefinitionId_Last(
        long vehicleId, long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException;

    /**
    * Returns the last vehicle attribute in the ordered set where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute fetchByVehicleIdAndAttributeDefinitionId_Last(
        long vehicleId, long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle attributes before and after the current vehicle attribute in the ordered set where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeId the primary key of the current vehicle attribute
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a vehicle attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute[] findByVehicleIdAndAttributeDefinitionId_PrevAndNext(
        long vehicleAttributeId, long vehicleId,
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException;

    /**
    * Removes all the vehicle attributes where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63; from the database.
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByVehicleIdAndAttributeDefinitionId(long vehicleId,
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle attributes where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @return the number of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public int countByVehicleIdAndAttributeDefinitionId(long vehicleId,
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the vehicle attribute in the entity cache if it is enabled.
    *
    * @param vehicleAttribute the vehicle attribute
    */
    public void cacheResult(
        de.humance.eco.profile.model.VehicleAttribute vehicleAttribute);

    /**
    * Caches the vehicle attributes in the entity cache if it is enabled.
    *
    * @param vehicleAttributes the vehicle attributes
    */
    public void cacheResult(
        java.util.List<de.humance.eco.profile.model.VehicleAttribute> vehicleAttributes);

    /**
    * Creates a new vehicle attribute with the primary key. Does not add the vehicle attribute to the database.
    *
    * @param vehicleAttributeId the primary key for the new vehicle attribute
    * @return the new vehicle attribute
    */
    public de.humance.eco.profile.model.VehicleAttribute create(
        long vehicleAttributeId);

    /**
    * Removes the vehicle attribute with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleAttributeId the primary key of the vehicle attribute
    * @return the vehicle attribute that was removed
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a vehicle attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute remove(
        long vehicleAttributeId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException;

    public de.humance.eco.profile.model.VehicleAttribute updateImpl(
        de.humance.eco.profile.model.VehicleAttribute vehicleAttribute)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle attribute with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleAttributeException} if it could not be found.
    *
    * @param vehicleAttributeId the primary key of the vehicle attribute
    * @return the vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a vehicle attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute findByPrimaryKey(
        long vehicleAttributeId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException;

    /**
    * Returns the vehicle attribute with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param vehicleAttributeId the primary key of the vehicle attribute
    * @return the vehicle attribute, or <code>null</code> if a vehicle attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleAttribute fetchByPrimaryKey(
        long vehicleAttributeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicle attributes.
    *
    * @return the vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttribute> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle attributes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @return the range of vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttribute> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicle attributes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleAttribute> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the vehicle attributes from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle attributes.
    *
    * @return the number of vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
