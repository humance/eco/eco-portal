package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import de.humance.eco.profile.model.VehicleAttribute;

import java.util.List;

/**
 * The persistence utility for the vehicle attribute service. This utility wraps {@link VehicleAttributePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehicleAttributePersistence
 * @see VehicleAttributePersistenceImpl
 * @generated
 */
public class VehicleAttributeUtil {
    private static VehicleAttributePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(VehicleAttribute vehicleAttribute) {
        getPersistence().clearCache(vehicleAttribute);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<VehicleAttribute> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<VehicleAttribute> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<VehicleAttribute> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static VehicleAttribute update(VehicleAttribute vehicleAttribute)
        throws SystemException {
        return getPersistence().update(vehicleAttribute);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static VehicleAttribute update(VehicleAttribute vehicleAttribute,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(vehicleAttribute, serviceContext);
    }

    /**
    * Returns all the vehicle attributes where vehicleId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @return the matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleId(
        long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByVehicleId(vehicleId);
    }

    /**
    * Returns a range of all the vehicle attributes where vehicleId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleId the vehicle ID
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @return the range of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleId(
        long vehicleId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByVehicleId(vehicleId, start, end);
    }

    /**
    * Returns an ordered range of all the vehicle attributes where vehicleId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleId the vehicle ID
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleId(
        long vehicleId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleId(vehicleId, start, end, orderByComparator);
    }

    /**
    * Returns the first vehicle attribute in the ordered set where vehicleId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute findByVehicleId_First(
        long vehicleId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException {
        return getPersistence()
                   .findByVehicleId_First(vehicleId, orderByComparator);
    }

    /**
    * Returns the first vehicle attribute in the ordered set where vehicleId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute fetchByVehicleId_First(
        long vehicleId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleId_First(vehicleId, orderByComparator);
    }

    /**
    * Returns the last vehicle attribute in the ordered set where vehicleId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute findByVehicleId_Last(
        long vehicleId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException {
        return getPersistence()
                   .findByVehicleId_Last(vehicleId, orderByComparator);
    }

    /**
    * Returns the last vehicle attribute in the ordered set where vehicleId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute fetchByVehicleId_Last(
        long vehicleId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleId_Last(vehicleId, orderByComparator);
    }

    /**
    * Returns the vehicle attributes before and after the current vehicle attribute in the ordered set where vehicleId = &#63;.
    *
    * @param vehicleAttributeId the primary key of the current vehicle attribute
    * @param vehicleId the vehicle ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a vehicle attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute[] findByVehicleId_PrevAndNext(
        long vehicleAttributeId, long vehicleId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException {
        return getPersistence()
                   .findByVehicleId_PrevAndNext(vehicleAttributeId, vehicleId,
            orderByComparator);
    }

    /**
    * Removes all the vehicle attributes where vehicleId = &#63; from the database.
    *
    * @param vehicleId the vehicle ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByVehicleId(long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByVehicleId(vehicleId);
    }

    /**
    * Returns the number of vehicle attributes where vehicleId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @return the number of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static int countByVehicleId(long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByVehicleId(vehicleId);
    }

    /**
    * Returns all the vehicle attributes where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @return the matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
    }

    /**
    * Returns a range of all the vehicle attributes where vehicleAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @return the range of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId,
            start, end);
    }

    /**
    * Returns an ordered range of all the vehicle attributes where vehicleAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId,
            start, end, orderByComparator);
    }

    /**
    * Returns the first vehicle attribute in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute findByVehicleAttributeDefinitionId_First(
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionId_First(vehicleAttributeDefinitionId,
            orderByComparator);
    }

    /**
    * Returns the first vehicle attribute in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute fetchByVehicleAttributeDefinitionId_First(
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleAttributeDefinitionId_First(vehicleAttributeDefinitionId,
            orderByComparator);
    }

    /**
    * Returns the last vehicle attribute in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute findByVehicleAttributeDefinitionId_Last(
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionId_Last(vehicleAttributeDefinitionId,
            orderByComparator);
    }

    /**
    * Returns the last vehicle attribute in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute fetchByVehicleAttributeDefinitionId_Last(
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleAttributeDefinitionId_Last(vehicleAttributeDefinitionId,
            orderByComparator);
    }

    /**
    * Returns the vehicle attributes before and after the current vehicle attribute in the ordered set where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeId the primary key of the current vehicle attribute
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a vehicle attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute[] findByVehicleAttributeDefinitionId_PrevAndNext(
        long vehicleAttributeId, long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException {
        return getPersistence()
                   .findByVehicleAttributeDefinitionId_PrevAndNext(vehicleAttributeId,
            vehicleAttributeDefinitionId, orderByComparator);
    }

    /**
    * Removes all the vehicle attributes where vehicleAttributeDefinitionId = &#63; from the database.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
    }

    /**
    * Returns the number of vehicle attributes where vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @return the number of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static int countByVehicleAttributeDefinitionId(
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByVehicleAttributeDefinitionId(vehicleAttributeDefinitionId);
    }

    /**
    * Returns all the vehicle attributes where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @return the matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleIdAndAttributeDefinitionId(
        long vehicleId, long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleIdAndAttributeDefinitionId(vehicleId,
            vehicleAttributeDefinitionId);
    }

    /**
    * Returns a range of all the vehicle attributes where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @return the range of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleIdAndAttributeDefinitionId(
        long vehicleId, long vehicleAttributeDefinitionId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleIdAndAttributeDefinitionId(vehicleId,
            vehicleAttributeDefinitionId, start, end);
    }

    /**
    * Returns an ordered range of all the vehicle attributes where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttribute> findByVehicleIdAndAttributeDefinitionId(
        long vehicleId, long vehicleAttributeDefinitionId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleIdAndAttributeDefinitionId(vehicleId,
            vehicleAttributeDefinitionId, start, end, orderByComparator);
    }

    /**
    * Returns the first vehicle attribute in the ordered set where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute findByVehicleIdAndAttributeDefinitionId_First(
        long vehicleId, long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException {
        return getPersistence()
                   .findByVehicleIdAndAttributeDefinitionId_First(vehicleId,
            vehicleAttributeDefinitionId, orderByComparator);
    }

    /**
    * Returns the first vehicle attribute in the ordered set where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute fetchByVehicleIdAndAttributeDefinitionId_First(
        long vehicleId, long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleIdAndAttributeDefinitionId_First(vehicleId,
            vehicleAttributeDefinitionId, orderByComparator);
    }

    /**
    * Returns the last vehicle attribute in the ordered set where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute findByVehicleIdAndAttributeDefinitionId_Last(
        long vehicleId, long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException {
        return getPersistence()
                   .findByVehicleIdAndAttributeDefinitionId_Last(vehicleId,
            vehicleAttributeDefinitionId, orderByComparator);
    }

    /**
    * Returns the last vehicle attribute in the ordered set where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle attribute, or <code>null</code> if a matching vehicle attribute could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute fetchByVehicleIdAndAttributeDefinitionId_Last(
        long vehicleId, long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleIdAndAttributeDefinitionId_Last(vehicleId,
            vehicleAttributeDefinitionId, orderByComparator);
    }

    /**
    * Returns the vehicle attributes before and after the current vehicle attribute in the ordered set where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleAttributeId the primary key of the current vehicle attribute
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a vehicle attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute[] findByVehicleIdAndAttributeDefinitionId_PrevAndNext(
        long vehicleAttributeId, long vehicleId,
        long vehicleAttributeDefinitionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException {
        return getPersistence()
                   .findByVehicleIdAndAttributeDefinitionId_PrevAndNext(vehicleAttributeId,
            vehicleId, vehicleAttributeDefinitionId, orderByComparator);
    }

    /**
    * Removes all the vehicle attributes where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63; from the database.
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByVehicleIdAndAttributeDefinitionId(
        long vehicleId, long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByVehicleIdAndAttributeDefinitionId(vehicleId,
            vehicleAttributeDefinitionId);
    }

    /**
    * Returns the number of vehicle attributes where vehicleId = &#63; and vehicleAttributeDefinitionId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @param vehicleAttributeDefinitionId the vehicle attribute definition ID
    * @return the number of matching vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static int countByVehicleIdAndAttributeDefinitionId(long vehicleId,
        long vehicleAttributeDefinitionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByVehicleIdAndAttributeDefinitionId(vehicleId,
            vehicleAttributeDefinitionId);
    }

    /**
    * Caches the vehicle attribute in the entity cache if it is enabled.
    *
    * @param vehicleAttribute the vehicle attribute
    */
    public static void cacheResult(
        de.humance.eco.profile.model.VehicleAttribute vehicleAttribute) {
        getPersistence().cacheResult(vehicleAttribute);
    }

    /**
    * Caches the vehicle attributes in the entity cache if it is enabled.
    *
    * @param vehicleAttributes the vehicle attributes
    */
    public static void cacheResult(
        java.util.List<de.humance.eco.profile.model.VehicleAttribute> vehicleAttributes) {
        getPersistence().cacheResult(vehicleAttributes);
    }

    /**
    * Creates a new vehicle attribute with the primary key. Does not add the vehicle attribute to the database.
    *
    * @param vehicleAttributeId the primary key for the new vehicle attribute
    * @return the new vehicle attribute
    */
    public static de.humance.eco.profile.model.VehicleAttribute create(
        long vehicleAttributeId) {
        return getPersistence().create(vehicleAttributeId);
    }

    /**
    * Removes the vehicle attribute with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleAttributeId the primary key of the vehicle attribute
    * @return the vehicle attribute that was removed
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a vehicle attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute remove(
        long vehicleAttributeId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException {
        return getPersistence().remove(vehicleAttributeId);
    }

    public static de.humance.eco.profile.model.VehicleAttribute updateImpl(
        de.humance.eco.profile.model.VehicleAttribute vehicleAttribute)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(vehicleAttribute);
    }

    /**
    * Returns the vehicle attribute with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleAttributeException} if it could not be found.
    *
    * @param vehicleAttributeId the primary key of the vehicle attribute
    * @return the vehicle attribute
    * @throws de.humance.eco.profile.NoSuchVehicleAttributeException if a vehicle attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute findByPrimaryKey(
        long vehicleAttributeId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleAttributeException {
        return getPersistence().findByPrimaryKey(vehicleAttributeId);
    }

    /**
    * Returns the vehicle attribute with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param vehicleAttributeId the primary key of the vehicle attribute
    * @return the vehicle attribute, or <code>null</code> if a vehicle attribute with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleAttribute fetchByPrimaryKey(
        long vehicleAttributeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(vehicleAttributeId);
    }

    /**
    * Returns all the vehicle attributes.
    *
    * @return the vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttribute> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the vehicle attributes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @return the range of vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttribute> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the vehicle attributes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleAttributeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle attributes
    * @param end the upper bound of the range of vehicle attributes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleAttribute> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the vehicle attributes from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of vehicle attributes.
    *
    * @return the number of vehicle attributes
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static VehicleAttributePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (VehicleAttributePersistence) PortletBeanLocatorUtil.locate(de.humance.eco.profile.service.ClpSerializer.getServletContextName(),
                    VehicleAttributePersistence.class.getName());

            ReferenceRegistry.registerReference(VehicleAttributeUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(VehicleAttributePersistence persistence) {
    }
}
