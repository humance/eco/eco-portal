package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.VehicleManufacturer;
import de.humance.eco.profile.service.VehicleManufacturerLocalServiceUtil;

/**
 * @author Humance
 * @generated
 */
public abstract class VehicleManufacturerActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public VehicleManufacturerActionableDynamicQuery()
        throws SystemException {
        setBaseLocalService(VehicleManufacturerLocalServiceUtil.getService());
        setClass(VehicleManufacturer.class);

        setClassLoader(de.humance.eco.profile.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("vehicleManufacturerId");
    }
}
