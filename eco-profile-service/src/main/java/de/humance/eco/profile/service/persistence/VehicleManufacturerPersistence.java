package de.humance.eco.profile.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import de.humance.eco.profile.model.VehicleManufacturer;

/**
 * The persistence interface for the vehicle manufacturer service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehicleManufacturerPersistenceImpl
 * @see VehicleManufacturerUtil
 * @generated
 */
public interface VehicleManufacturerPersistence extends BasePersistence<VehicleManufacturer> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link VehicleManufacturerUtil} to access the vehicle manufacturer persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the vehicle manufacturers where name = &#63;.
    *
    * @param name the name
    * @return the matching vehicle manufacturers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleManufacturer> findByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle manufacturers where name = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleManufacturerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param name the name
    * @param start the lower bound of the range of vehicle manufacturers
    * @param end the upper bound of the range of vehicle manufacturers (not inclusive)
    * @return the range of matching vehicle manufacturers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleManufacturer> findByName(
        java.lang.String name, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicle manufacturers where name = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleManufacturerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param name the name
    * @param start the lower bound of the range of vehicle manufacturers
    * @param end the upper bound of the range of vehicle manufacturers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle manufacturers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleManufacturer> findByName(
        java.lang.String name, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle manufacturer in the ordered set where name = &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle manufacturer
    * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a matching vehicle manufacturer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleManufacturer findByName_First(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleManufacturerException;

    /**
    * Returns the first vehicle manufacturer in the ordered set where name = &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle manufacturer, or <code>null</code> if a matching vehicle manufacturer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleManufacturer fetchByName_First(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle manufacturer in the ordered set where name = &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle manufacturer
    * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a matching vehicle manufacturer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleManufacturer findByName_Last(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleManufacturerException;

    /**
    * Returns the last vehicle manufacturer in the ordered set where name = &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle manufacturer, or <code>null</code> if a matching vehicle manufacturer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleManufacturer fetchByName_Last(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle manufacturers before and after the current vehicle manufacturer in the ordered set where name = &#63;.
    *
    * @param vehicleManufacturerId the primary key of the current vehicle manufacturer
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle manufacturer
    * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a vehicle manufacturer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleManufacturer[] findByName_PrevAndNext(
        long vehicleManufacturerId, java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleManufacturerException;

    /**
    * Removes all the vehicle manufacturers where name = &#63; from the database.
    *
    * @param name the name
    * @throws SystemException if a system exception occurred
    */
    public void removeByName(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle manufacturers where name = &#63;.
    *
    * @param name the name
    * @return the number of matching vehicle manufacturers
    * @throws SystemException if a system exception occurred
    */
    public int countByName(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicle manufacturers where name LIKE &#63;.
    *
    * @param name the name
    * @return the matching vehicle manufacturers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleManufacturer> findByNameLike(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle manufacturers where name LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleManufacturerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param name the name
    * @param start the lower bound of the range of vehicle manufacturers
    * @param end the upper bound of the range of vehicle manufacturers (not inclusive)
    * @return the range of matching vehicle manufacturers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleManufacturer> findByNameLike(
        java.lang.String name, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicle manufacturers where name LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleManufacturerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param name the name
    * @param start the lower bound of the range of vehicle manufacturers
    * @param end the upper bound of the range of vehicle manufacturers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle manufacturers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleManufacturer> findByNameLike(
        java.lang.String name, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle manufacturer in the ordered set where name LIKE &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle manufacturer
    * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a matching vehicle manufacturer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleManufacturer findByNameLike_First(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleManufacturerException;

    /**
    * Returns the first vehicle manufacturer in the ordered set where name LIKE &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle manufacturer, or <code>null</code> if a matching vehicle manufacturer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleManufacturer fetchByNameLike_First(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle manufacturer in the ordered set where name LIKE &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle manufacturer
    * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a matching vehicle manufacturer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleManufacturer findByNameLike_Last(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleManufacturerException;

    /**
    * Returns the last vehicle manufacturer in the ordered set where name LIKE &#63;.
    *
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle manufacturer, or <code>null</code> if a matching vehicle manufacturer could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleManufacturer fetchByNameLike_Last(
        java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle manufacturers before and after the current vehicle manufacturer in the ordered set where name LIKE &#63;.
    *
    * @param vehicleManufacturerId the primary key of the current vehicle manufacturer
    * @param name the name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle manufacturer
    * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a vehicle manufacturer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleManufacturer[] findByNameLike_PrevAndNext(
        long vehicleManufacturerId, java.lang.String name,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleManufacturerException;

    /**
    * Removes all the vehicle manufacturers where name LIKE &#63; from the database.
    *
    * @param name the name
    * @throws SystemException if a system exception occurred
    */
    public void removeByNameLike(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle manufacturers where name LIKE &#63;.
    *
    * @param name the name
    * @return the number of matching vehicle manufacturers
    * @throws SystemException if a system exception occurred
    */
    public int countByNameLike(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the vehicle manufacturer in the entity cache if it is enabled.
    *
    * @param vehicleManufacturer the vehicle manufacturer
    */
    public void cacheResult(
        de.humance.eco.profile.model.VehicleManufacturer vehicleManufacturer);

    /**
    * Caches the vehicle manufacturers in the entity cache if it is enabled.
    *
    * @param vehicleManufacturers the vehicle manufacturers
    */
    public void cacheResult(
        java.util.List<de.humance.eco.profile.model.VehicleManufacturer> vehicleManufacturers);

    /**
    * Creates a new vehicle manufacturer with the primary key. Does not add the vehicle manufacturer to the database.
    *
    * @param vehicleManufacturerId the primary key for the new vehicle manufacturer
    * @return the new vehicle manufacturer
    */
    public de.humance.eco.profile.model.VehicleManufacturer create(
        long vehicleManufacturerId);

    /**
    * Removes the vehicle manufacturer with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleManufacturerId the primary key of the vehicle manufacturer
    * @return the vehicle manufacturer that was removed
    * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a vehicle manufacturer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleManufacturer remove(
        long vehicleManufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleManufacturerException;

    public de.humance.eco.profile.model.VehicleManufacturer updateImpl(
        de.humance.eco.profile.model.VehicleManufacturer vehicleManufacturer)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle manufacturer with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleManufacturerException} if it could not be found.
    *
    * @param vehicleManufacturerId the primary key of the vehicle manufacturer
    * @return the vehicle manufacturer
    * @throws de.humance.eco.profile.NoSuchVehicleManufacturerException if a vehicle manufacturer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleManufacturer findByPrimaryKey(
        long vehicleManufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleManufacturerException;

    /**
    * Returns the vehicle manufacturer with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param vehicleManufacturerId the primary key of the vehicle manufacturer
    * @return the vehicle manufacturer, or <code>null</code> if a vehicle manufacturer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleManufacturer fetchByPrimaryKey(
        long vehicleManufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicle manufacturers.
    *
    * @return the vehicle manufacturers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleManufacturer> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle manufacturers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleManufacturerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle manufacturers
    * @param end the upper bound of the range of vehicle manufacturers (not inclusive)
    * @return the range of vehicle manufacturers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleManufacturer> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicle manufacturers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleManufacturerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle manufacturers
    * @param end the upper bound of the range of vehicle manufacturers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of vehicle manufacturers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleManufacturer> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the vehicle manufacturers from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle manufacturers.
    *
    * @return the number of vehicle manufacturers
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
