package de.humance.eco.profile.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import de.humance.eco.profile.model.Vehicle;

/**
 * The persistence interface for the vehicle service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehiclePersistenceImpl
 * @see VehicleUtil
 * @generated
 */
public interface VehiclePersistence extends BasePersistence<Vehicle> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link VehicleUtil} to access the vehicle persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the vehicles where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverId(
        long driverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicles where driverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverId(
        long driverId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicles where driverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverId(
        long driverId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle in the ordered set where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByDriverId_First(
        long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the first vehicle in the ordered set where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByDriverId_First(
        long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle in the ordered set where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByDriverId_Last(
        long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the last vehicle in the ordered set where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByDriverId_Last(
        long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where driverId = &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle[] findByDriverId_PrevAndNext(
        long vehicleId, long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Removes all the vehicles where driverId = &#63; from the database.
    *
    * @param driverId the driver ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByDriverId(long driverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicles where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public int countByDriverId(long driverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicles where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverIdAndOrgaId(
        long driverId, long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicles where driverId = &#63; and organizationId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverIdAndOrgaId(
        long driverId, long organizationId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicles where driverId = &#63; and organizationId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverIdAndOrgaId(
        long driverId, long organizationId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByDriverIdAndOrgaId_First(
        long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the first vehicle in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByDriverIdAndOrgaId_First(
        long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByDriverIdAndOrgaId_Last(
        long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the last vehicle in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByDriverIdAndOrgaId_Last(
        long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle[] findByDriverIdAndOrgaId_PrevAndNext(
        long vehicleId, long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Removes all the vehicles where driverId = &#63; and organizationId = &#63; from the database.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByDriverIdAndOrgaId(long driverId, long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicles where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public int countByDriverIdAndOrgaId(long driverId, long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicles where typeId = &#63;.
    *
    * @param typeId the type ID
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByTypeId(
        long typeId) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicles where typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param typeId the type ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByTypeId(
        long typeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicles where typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param typeId the type ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByTypeId(
        long typeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByTypeId_First(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the first vehicle in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByTypeId_First(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByTypeId_Last(long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the last vehicle in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByTypeId_Last(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where typeId = &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle[] findByTypeId_PrevAndNext(
        long vehicleId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Removes all the vehicles where typeId = &#63; from the database.
    *
    * @param typeId the type ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByTypeId(long typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicles where typeId = &#63;.
    *
    * @param typeId the type ID
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public int countByTypeId(long typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicles where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverIdAndTypeId(
        long driverId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicles where driverId = &#63; and typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverIdAndTypeId(
        long driverId, long typeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicles where driverId = &#63; and typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverIdAndTypeId(
        long driverId, long typeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByDriverIdAndTypeId_First(
        long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the first vehicle in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByDriverIdAndTypeId_First(
        long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByDriverIdAndTypeId_Last(
        long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the last vehicle in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByDriverIdAndTypeId_Last(
        long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle[] findByDriverIdAndTypeId_PrevAndNext(
        long vehicleId, long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Removes all the vehicles where driverId = &#63; and typeId = &#63; from the database.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByDriverIdAndTypeId(long driverId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicles where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public int countByDriverIdAndTypeId(long driverId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicles where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByManufacturerId(
        long manufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicles where manufacturerId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param manufacturerId the manufacturer ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByManufacturerId(
        long manufacturerId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicles where manufacturerId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param manufacturerId the manufacturer ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByManufacturerId(
        long manufacturerId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle in the ordered set where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByManufacturerId_First(
        long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the first vehicle in the ordered set where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByManufacturerId_First(
        long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle in the ordered set where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByManufacturerId_Last(
        long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the last vehicle in the ordered set where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByManufacturerId_Last(
        long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where manufacturerId = &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle[] findByManufacturerId_PrevAndNext(
        long vehicleId, long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Removes all the vehicles where manufacturerId = &#63; from the database.
    *
    * @param manufacturerId the manufacturer ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByManufacturerId(long manufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicles where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public int countByManufacturerId(long manufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicles where modelName = &#63;.
    *
    * @param modelName the model name
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByModelName(
        java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicles where modelName = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param modelName the model name
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByModelName(
        java.lang.String modelName, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicles where modelName = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param modelName the model name
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByModelName(
        java.lang.String modelName, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle in the ordered set where modelName = &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByModelName_First(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the first vehicle in the ordered set where modelName = &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByModelName_First(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle in the ordered set where modelName = &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByModelName_Last(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the last vehicle in the ordered set where modelName = &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByModelName_Last(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where modelName = &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle[] findByModelName_PrevAndNext(
        long vehicleId, java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Removes all the vehicles where modelName = &#63; from the database.
    *
    * @param modelName the model name
    * @throws SystemException if a system exception occurred
    */
    public void removeByModelName(java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicles where modelName = &#63;.
    *
    * @param modelName the model name
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public int countByModelName(java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicles where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByModelNameLike(
        java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicles where modelName LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param modelName the model name
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByModelNameLike(
        java.lang.String modelName, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicles where modelName LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param modelName the model name
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByModelNameLike(
        java.lang.String modelName, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle in the ordered set where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByModelNameLike_First(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the first vehicle in the ordered set where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByModelNameLike_First(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle in the ordered set where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByModelNameLike_Last(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the last vehicle in the ordered set where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByModelNameLike_Last(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where modelName LIKE &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle[] findByModelNameLike_PrevAndNext(
        long vehicleId, java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Removes all the vehicles where modelName LIKE &#63; from the database.
    *
    * @param modelName the model name
    * @throws SystemException if a system exception occurred
    */
    public void removeByModelNameLike(java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicles where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public int countByModelNameLike(java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicles where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByLicensePlate(
        java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicles where licensePlate = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param licensePlate the license plate
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByLicensePlate(
        java.lang.String licensePlate, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicles where licensePlate = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param licensePlate the license plate
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByLicensePlate(
        java.lang.String licensePlate, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle in the ordered set where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByLicensePlate_First(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the first vehicle in the ordered set where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByLicensePlate_First(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle in the ordered set where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByLicensePlate_Last(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the last vehicle in the ordered set where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByLicensePlate_Last(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where licensePlate = &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle[] findByLicensePlate_PrevAndNext(
        long vehicleId, java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Removes all the vehicles where licensePlate = &#63; from the database.
    *
    * @param licensePlate the license plate
    * @throws SystemException if a system exception occurred
    */
    public void removeByLicensePlate(java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicles where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public int countByLicensePlate(java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicles where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByLicensePlateLike(
        java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicles where licensePlate LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param licensePlate the license plate
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByLicensePlateLike(
        java.lang.String licensePlate, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicles where licensePlate LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param licensePlate the license plate
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findByLicensePlateLike(
        java.lang.String licensePlate, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle in the ordered set where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByLicensePlateLike_First(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the first vehicle in the ordered set where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByLicensePlateLike_First(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle in the ordered set where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByLicensePlateLike_Last(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the last vehicle in the ordered set where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByLicensePlateLike_Last(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where licensePlate LIKE &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle[] findByLicensePlateLike_PrevAndNext(
        long vehicleId, java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Removes all the vehicles where licensePlate LIKE &#63; from the database.
    *
    * @param licensePlate the license plate
    * @throws SystemException if a system exception occurred
    */
    public void removeByLicensePlateLike(java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicles where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public int countByLicensePlateLike(java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the vehicle in the entity cache if it is enabled.
    *
    * @param vehicle the vehicle
    */
    public void cacheResult(de.humance.eco.profile.model.Vehicle vehicle);

    /**
    * Caches the vehicles in the entity cache if it is enabled.
    *
    * @param vehicles the vehicles
    */
    public void cacheResult(
        java.util.List<de.humance.eco.profile.model.Vehicle> vehicles);

    /**
    * Creates a new vehicle with the primary key. Does not add the vehicle to the database.
    *
    * @param vehicleId the primary key for the new vehicle
    * @return the new vehicle
    */
    public de.humance.eco.profile.model.Vehicle create(long vehicleId);

    /**
    * Removes the vehicle with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleId the primary key of the vehicle
    * @return the vehicle that was removed
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle remove(long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    public de.humance.eco.profile.model.Vehicle updateImpl(
        de.humance.eco.profile.model.Vehicle vehicle)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleException} if it could not be found.
    *
    * @param vehicleId the primary key of the vehicle
    * @return the vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle findByPrimaryKey(long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException;

    /**
    * Returns the vehicle with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param vehicleId the primary key of the vehicle
    * @return the vehicle, or <code>null</code> if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.Vehicle fetchByPrimaryKey(
        long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicles.
    *
    * @return the vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicles.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicles.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.Vehicle> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the vehicles from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicles.
    *
    * @return the number of vehicles
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
