package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.VehicleType;
import de.humance.eco.profile.service.VehicleTypeLocalServiceUtil;

/**
 * @author Humance
 * @generated
 */
public abstract class VehicleTypeActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public VehicleTypeActionableDynamicQuery() throws SystemException {
        setBaseLocalService(VehicleTypeLocalServiceUtil.getService());
        setClass(VehicleType.class);

        setClassLoader(de.humance.eco.profile.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("vehicleTypeId");
    }
}
