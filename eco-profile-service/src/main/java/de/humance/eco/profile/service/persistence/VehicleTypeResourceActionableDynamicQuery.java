package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.VehicleTypeResource;
import de.humance.eco.profile.service.VehicleTypeResourceLocalServiceUtil;

/**
 * @author Humance
 * @generated
 */
public abstract class VehicleTypeResourceActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public VehicleTypeResourceActionableDynamicQuery()
        throws SystemException {
        setBaseLocalService(VehicleTypeResourceLocalServiceUtil.getService());
        setClass(VehicleTypeResource.class);

        setClassLoader(de.humance.eco.profile.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("vehicleTypeResourceId");
    }
}
