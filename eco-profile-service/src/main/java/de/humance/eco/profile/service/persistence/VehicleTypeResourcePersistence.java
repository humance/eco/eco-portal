package de.humance.eco.profile.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import de.humance.eco.profile.model.VehicleTypeResource;

/**
 * The persistence interface for the vehicle type resource service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehicleTypeResourcePersistenceImpl
 * @see VehicleTypeResourceUtil
 * @generated
 */
public interface VehicleTypeResourcePersistence extends BasePersistence<VehicleTypeResource> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link VehicleTypeResourceUtil} to access the vehicle type resource persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the vehicle type resources where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @return the matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeId(
        long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle type resources where vehicleTypeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @return the range of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeId(
        long vehicleTypeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicle type resources where vehicleTypeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeId(
        long vehicleTypeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource findByVehicleTypeId_First(
        long vehicleTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException;

    /**
    * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource fetchByVehicleTypeId_First(
        long vehicleTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource findByVehicleTypeId_Last(
        long vehicleTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException;

    /**
    * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource fetchByVehicleTypeId_Last(
        long vehicleTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle type resources before and after the current vehicle type resource in the ordered set where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeResourceId the primary key of the current vehicle type resource
    * @param vehicleTypeId the vehicle type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource[] findByVehicleTypeId_PrevAndNext(
        long vehicleTypeResourceId, long vehicleTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException;

    /**
    * Removes all the vehicle type resources where vehicleTypeId = &#63; from the database.
    *
    * @param vehicleTypeId the vehicle type ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByVehicleTypeId(long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle type resources where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @return the number of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public int countByVehicleTypeId(long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicle type resources where vehicleTypeId = &#63; and country = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @return the matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndCountry(
        long vehicleTypeId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle type resources where vehicleTypeId = &#63; and country = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @return the range of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndCountry(
        long vehicleTypeId, java.lang.String country, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicle type resources where vehicleTypeId = &#63; and country = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndCountry(
        long vehicleTypeId, java.lang.String country, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource findByVehicleTypeIdAndCountry_First(
        long vehicleTypeId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException;

    /**
    * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource fetchByVehicleTypeIdAndCountry_First(
        long vehicleTypeId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource findByVehicleTypeIdAndCountry_Last(
        long vehicleTypeId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException;

    /**
    * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource fetchByVehicleTypeIdAndCountry_Last(
        long vehicleTypeId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle type resources before and after the current vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63;.
    *
    * @param vehicleTypeResourceId the primary key of the current vehicle type resource
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource[] findByVehicleTypeIdAndCountry_PrevAndNext(
        long vehicleTypeResourceId, long vehicleTypeId,
        java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException;

    /**
    * Removes all the vehicle type resources where vehicleTypeId = &#63; and country = &#63; from the database.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @throws SystemException if a system exception occurred
    */
    public void removeByVehicleTypeIdAndCountry(long vehicleTypeId,
        java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle type resources where vehicleTypeId = &#63; and country = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @return the number of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public int countByVehicleTypeIdAndCountry(long vehicleTypeId,
        java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicle type resources where vehicleTypeId = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @return the matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndLanguage(
        long vehicleTypeId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle type resources where vehicleTypeId = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @return the range of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndLanguage(
        long vehicleTypeId, java.lang.String language, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicle type resources where vehicleTypeId = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndLanguage(
        long vehicleTypeId, java.lang.String language, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource findByVehicleTypeIdAndLanguage_First(
        long vehicleTypeId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException;

    /**
    * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource fetchByVehicleTypeIdAndLanguage_First(
        long vehicleTypeId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource findByVehicleTypeIdAndLanguage_Last(
        long vehicleTypeId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException;

    /**
    * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource fetchByVehicleTypeIdAndLanguage_Last(
        long vehicleTypeId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle type resources before and after the current vehicle type resource in the ordered set where vehicleTypeId = &#63; and language = &#63;.
    *
    * @param vehicleTypeResourceId the primary key of the current vehicle type resource
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource[] findByVehicleTypeIdAndLanguage_PrevAndNext(
        long vehicleTypeResourceId, long vehicleTypeId,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException;

    /**
    * Removes all the vehicle type resources where vehicleTypeId = &#63; and language = &#63; from the database.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @throws SystemException if a system exception occurred
    */
    public void removeByVehicleTypeIdAndLanguage(long vehicleTypeId,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle type resources where vehicleTypeId = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @return the number of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public int countByVehicleTypeIdAndLanguage(long vehicleTypeId,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicle type resources where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @return the matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndLocale(
        long vehicleTypeId, java.lang.String country, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle type resources where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @return the range of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndLocale(
        long vehicleTypeId, java.lang.String country,
        java.lang.String language, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicle type resources where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndLocale(
        long vehicleTypeId, java.lang.String country,
        java.lang.String language, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource findByVehicleTypeIdAndLocale_First(
        long vehicleTypeId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException;

    /**
    * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource fetchByVehicleTypeIdAndLocale_First(
        long vehicleTypeId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource findByVehicleTypeIdAndLocale_Last(
        long vehicleTypeId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException;

    /**
    * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource fetchByVehicleTypeIdAndLocale_Last(
        long vehicleTypeId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle type resources before and after the current vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleTypeResourceId the primary key of the current vehicle type resource
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource[] findByVehicleTypeIdAndLocale_PrevAndNext(
        long vehicleTypeResourceId, long vehicleTypeId,
        java.lang.String country, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException;

    /**
    * Removes all the vehicle type resources where vehicleTypeId = &#63; and country = &#63; and language = &#63; from the database.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @throws SystemException if a system exception occurred
    */
    public void removeByVehicleTypeIdAndLocale(long vehicleTypeId,
        java.lang.String country, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle type resources where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @return the number of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public int countByVehicleTypeIdAndLocale(long vehicleTypeId,
        java.lang.String country, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the vehicle type resource in the entity cache if it is enabled.
    *
    * @param vehicleTypeResource the vehicle type resource
    */
    public void cacheResult(
        de.humance.eco.profile.model.VehicleTypeResource vehicleTypeResource);

    /**
    * Caches the vehicle type resources in the entity cache if it is enabled.
    *
    * @param vehicleTypeResources the vehicle type resources
    */
    public void cacheResult(
        java.util.List<de.humance.eco.profile.model.VehicleTypeResource> vehicleTypeResources);

    /**
    * Creates a new vehicle type resource with the primary key. Does not add the vehicle type resource to the database.
    *
    * @param vehicleTypeResourceId the primary key for the new vehicle type resource
    * @return the new vehicle type resource
    */
    public de.humance.eco.profile.model.VehicleTypeResource create(
        long vehicleTypeResourceId);

    /**
    * Removes the vehicle type resource with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleTypeResourceId the primary key of the vehicle type resource
    * @return the vehicle type resource that was removed
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource remove(
        long vehicleTypeResourceId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException;

    public de.humance.eco.profile.model.VehicleTypeResource updateImpl(
        de.humance.eco.profile.model.VehicleTypeResource vehicleTypeResource)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle type resource with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleTypeResourceException} if it could not be found.
    *
    * @param vehicleTypeResourceId the primary key of the vehicle type resource
    * @return the vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource findByPrimaryKey(
        long vehicleTypeResourceId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException;

    /**
    * Returns the vehicle type resource with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param vehicleTypeResourceId the primary key of the vehicle type resource
    * @return the vehicle type resource, or <code>null</code> if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public de.humance.eco.profile.model.VehicleTypeResource fetchByPrimaryKey(
        long vehicleTypeResourceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicle type resources.
    *
    * @return the vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicle type resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @return the range of vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicle type resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the vehicle type resources from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicle type resources.
    *
    * @return the number of vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
