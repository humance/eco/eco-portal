package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import de.humance.eco.profile.model.VehicleTypeResource;

import java.util.List;

/**
 * The persistence utility for the vehicle type resource service. This utility wraps {@link VehicleTypeResourcePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehicleTypeResourcePersistence
 * @see VehicleTypeResourcePersistenceImpl
 * @generated
 */
public class VehicleTypeResourceUtil {
    private static VehicleTypeResourcePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(VehicleTypeResource vehicleTypeResource) {
        getPersistence().clearCache(vehicleTypeResource);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<VehicleTypeResource> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<VehicleTypeResource> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<VehicleTypeResource> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static VehicleTypeResource update(
        VehicleTypeResource vehicleTypeResource) throws SystemException {
        return getPersistence().update(vehicleTypeResource);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static VehicleTypeResource update(
        VehicleTypeResource vehicleTypeResource, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(vehicleTypeResource, serviceContext);
    }

    /**
    * Returns all the vehicle type resources where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @return the matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeId(
        long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByVehicleTypeId(vehicleTypeId);
    }

    /**
    * Returns a range of all the vehicle type resources where vehicleTypeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @return the range of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeId(
        long vehicleTypeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByVehicleTypeId(vehicleTypeId, start, end);
    }

    /**
    * Returns an ordered range of all the vehicle type resources where vehicleTypeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeId(
        long vehicleTypeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleTypeId(vehicleTypeId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource findByVehicleTypeId_First(
        long vehicleTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException {
        return getPersistence()
                   .findByVehicleTypeId_First(vehicleTypeId, orderByComparator);
    }

    /**
    * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource fetchByVehicleTypeId_First(
        long vehicleTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleTypeId_First(vehicleTypeId, orderByComparator);
    }

    /**
    * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource findByVehicleTypeId_Last(
        long vehicleTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException {
        return getPersistence()
                   .findByVehicleTypeId_Last(vehicleTypeId, orderByComparator);
    }

    /**
    * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource fetchByVehicleTypeId_Last(
        long vehicleTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleTypeId_Last(vehicleTypeId, orderByComparator);
    }

    /**
    * Returns the vehicle type resources before and after the current vehicle type resource in the ordered set where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeResourceId the primary key of the current vehicle type resource
    * @param vehicleTypeId the vehicle type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource[] findByVehicleTypeId_PrevAndNext(
        long vehicleTypeResourceId, long vehicleTypeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException {
        return getPersistence()
                   .findByVehicleTypeId_PrevAndNext(vehicleTypeResourceId,
            vehicleTypeId, orderByComparator);
    }

    /**
    * Removes all the vehicle type resources where vehicleTypeId = &#63; from the database.
    *
    * @param vehicleTypeId the vehicle type ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByVehicleTypeId(long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByVehicleTypeId(vehicleTypeId);
    }

    /**
    * Returns the number of vehicle type resources where vehicleTypeId = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @return the number of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static int countByVehicleTypeId(long vehicleTypeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByVehicleTypeId(vehicleTypeId);
    }

    /**
    * Returns all the vehicle type resources where vehicleTypeId = &#63; and country = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @return the matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndCountry(
        long vehicleTypeId, java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleTypeIdAndCountry(vehicleTypeId, country);
    }

    /**
    * Returns a range of all the vehicle type resources where vehicleTypeId = &#63; and country = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @return the range of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndCountry(
        long vehicleTypeId, java.lang.String country, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleTypeIdAndCountry(vehicleTypeId, country,
            start, end);
    }

    /**
    * Returns an ordered range of all the vehicle type resources where vehicleTypeId = &#63; and country = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndCountry(
        long vehicleTypeId, java.lang.String country, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleTypeIdAndCountry(vehicleTypeId, country,
            start, end, orderByComparator);
    }

    /**
    * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource findByVehicleTypeIdAndCountry_First(
        long vehicleTypeId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException {
        return getPersistence()
                   .findByVehicleTypeIdAndCountry_First(vehicleTypeId, country,
            orderByComparator);
    }

    /**
    * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource fetchByVehicleTypeIdAndCountry_First(
        long vehicleTypeId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleTypeIdAndCountry_First(vehicleTypeId,
            country, orderByComparator);
    }

    /**
    * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource findByVehicleTypeIdAndCountry_Last(
        long vehicleTypeId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException {
        return getPersistence()
                   .findByVehicleTypeIdAndCountry_Last(vehicleTypeId, country,
            orderByComparator);
    }

    /**
    * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource fetchByVehicleTypeIdAndCountry_Last(
        long vehicleTypeId, java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleTypeIdAndCountry_Last(vehicleTypeId, country,
            orderByComparator);
    }

    /**
    * Returns the vehicle type resources before and after the current vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63;.
    *
    * @param vehicleTypeResourceId the primary key of the current vehicle type resource
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource[] findByVehicleTypeIdAndCountry_PrevAndNext(
        long vehicleTypeResourceId, long vehicleTypeId,
        java.lang.String country,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException {
        return getPersistence()
                   .findByVehicleTypeIdAndCountry_PrevAndNext(vehicleTypeResourceId,
            vehicleTypeId, country, orderByComparator);
    }

    /**
    * Removes all the vehicle type resources where vehicleTypeId = &#63; and country = &#63; from the database.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @throws SystemException if a system exception occurred
    */
    public static void removeByVehicleTypeIdAndCountry(long vehicleTypeId,
        java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByVehicleTypeIdAndCountry(vehicleTypeId, country);
    }

    /**
    * Returns the number of vehicle type resources where vehicleTypeId = &#63; and country = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @return the number of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static int countByVehicleTypeIdAndCountry(long vehicleTypeId,
        java.lang.String country)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByVehicleTypeIdAndCountry(vehicleTypeId, country);
    }

    /**
    * Returns all the vehicle type resources where vehicleTypeId = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @return the matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndLanguage(
        long vehicleTypeId, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleTypeIdAndLanguage(vehicleTypeId, language);
    }

    /**
    * Returns a range of all the vehicle type resources where vehicleTypeId = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @return the range of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndLanguage(
        long vehicleTypeId, java.lang.String language, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleTypeIdAndLanguage(vehicleTypeId, language,
            start, end);
    }

    /**
    * Returns an ordered range of all the vehicle type resources where vehicleTypeId = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndLanguage(
        long vehicleTypeId, java.lang.String language, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleTypeIdAndLanguage(vehicleTypeId, language,
            start, end, orderByComparator);
    }

    /**
    * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource findByVehicleTypeIdAndLanguage_First(
        long vehicleTypeId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException {
        return getPersistence()
                   .findByVehicleTypeIdAndLanguage_First(vehicleTypeId,
            language, orderByComparator);
    }

    /**
    * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource fetchByVehicleTypeIdAndLanguage_First(
        long vehicleTypeId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleTypeIdAndLanguage_First(vehicleTypeId,
            language, orderByComparator);
    }

    /**
    * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource findByVehicleTypeIdAndLanguage_Last(
        long vehicleTypeId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException {
        return getPersistence()
                   .findByVehicleTypeIdAndLanguage_Last(vehicleTypeId,
            language, orderByComparator);
    }

    /**
    * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource fetchByVehicleTypeIdAndLanguage_Last(
        long vehicleTypeId, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleTypeIdAndLanguage_Last(vehicleTypeId,
            language, orderByComparator);
    }

    /**
    * Returns the vehicle type resources before and after the current vehicle type resource in the ordered set where vehicleTypeId = &#63; and language = &#63;.
    *
    * @param vehicleTypeResourceId the primary key of the current vehicle type resource
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource[] findByVehicleTypeIdAndLanguage_PrevAndNext(
        long vehicleTypeResourceId, long vehicleTypeId,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException {
        return getPersistence()
                   .findByVehicleTypeIdAndLanguage_PrevAndNext(vehicleTypeResourceId,
            vehicleTypeId, language, orderByComparator);
    }

    /**
    * Removes all the vehicle type resources where vehicleTypeId = &#63; and language = &#63; from the database.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @throws SystemException if a system exception occurred
    */
    public static void removeByVehicleTypeIdAndLanguage(long vehicleTypeId,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByVehicleTypeIdAndLanguage(vehicleTypeId, language);
    }

    /**
    * Returns the number of vehicle type resources where vehicleTypeId = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param language the language
    * @return the number of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static int countByVehicleTypeIdAndLanguage(long vehicleTypeId,
        java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByVehicleTypeIdAndLanguage(vehicleTypeId, language);
    }

    /**
    * Returns all the vehicle type resources where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @return the matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndLocale(
        long vehicleTypeId, java.lang.String country, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleTypeIdAndLocale(vehicleTypeId, country,
            language);
    }

    /**
    * Returns a range of all the vehicle type resources where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @return the range of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndLocale(
        long vehicleTypeId, java.lang.String country,
        java.lang.String language, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleTypeIdAndLocale(vehicleTypeId, country,
            language, start, end);
    }

    /**
    * Returns an ordered range of all the vehicle type resources where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findByVehicleTypeIdAndLocale(
        long vehicleTypeId, java.lang.String country,
        java.lang.String language, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByVehicleTypeIdAndLocale(vehicleTypeId, country,
            language, start, end, orderByComparator);
    }

    /**
    * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource findByVehicleTypeIdAndLocale_First(
        long vehicleTypeId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException {
        return getPersistence()
                   .findByVehicleTypeIdAndLocale_First(vehicleTypeId, country,
            language, orderByComparator);
    }

    /**
    * Returns the first vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource fetchByVehicleTypeIdAndLocale_First(
        long vehicleTypeId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleTypeIdAndLocale_First(vehicleTypeId, country,
            language, orderByComparator);
    }

    /**
    * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource findByVehicleTypeIdAndLocale_Last(
        long vehicleTypeId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException {
        return getPersistence()
                   .findByVehicleTypeIdAndLocale_Last(vehicleTypeId, country,
            language, orderByComparator);
    }

    /**
    * Returns the last vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle type resource, or <code>null</code> if a matching vehicle type resource could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource fetchByVehicleTypeIdAndLocale_Last(
        long vehicleTypeId, java.lang.String country,
        java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByVehicleTypeIdAndLocale_Last(vehicleTypeId, country,
            language, orderByComparator);
    }

    /**
    * Returns the vehicle type resources before and after the current vehicle type resource in the ordered set where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleTypeResourceId the primary key of the current vehicle type resource
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource[] findByVehicleTypeIdAndLocale_PrevAndNext(
        long vehicleTypeResourceId, long vehicleTypeId,
        java.lang.String country, java.lang.String language,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException {
        return getPersistence()
                   .findByVehicleTypeIdAndLocale_PrevAndNext(vehicleTypeResourceId,
            vehicleTypeId, country, language, orderByComparator);
    }

    /**
    * Removes all the vehicle type resources where vehicleTypeId = &#63; and country = &#63; and language = &#63; from the database.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @throws SystemException if a system exception occurred
    */
    public static void removeByVehicleTypeIdAndLocale(long vehicleTypeId,
        java.lang.String country, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByVehicleTypeIdAndLocale(vehicleTypeId, country, language);
    }

    /**
    * Returns the number of vehicle type resources where vehicleTypeId = &#63; and country = &#63; and language = &#63;.
    *
    * @param vehicleTypeId the vehicle type ID
    * @param country the country
    * @param language the language
    * @return the number of matching vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static int countByVehicleTypeIdAndLocale(long vehicleTypeId,
        java.lang.String country, java.lang.String language)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByVehicleTypeIdAndLocale(vehicleTypeId, country,
            language);
    }

    /**
    * Caches the vehicle type resource in the entity cache if it is enabled.
    *
    * @param vehicleTypeResource the vehicle type resource
    */
    public static void cacheResult(
        de.humance.eco.profile.model.VehicleTypeResource vehicleTypeResource) {
        getPersistence().cacheResult(vehicleTypeResource);
    }

    /**
    * Caches the vehicle type resources in the entity cache if it is enabled.
    *
    * @param vehicleTypeResources the vehicle type resources
    */
    public static void cacheResult(
        java.util.List<de.humance.eco.profile.model.VehicleTypeResource> vehicleTypeResources) {
        getPersistence().cacheResult(vehicleTypeResources);
    }

    /**
    * Creates a new vehicle type resource with the primary key. Does not add the vehicle type resource to the database.
    *
    * @param vehicleTypeResourceId the primary key for the new vehicle type resource
    * @return the new vehicle type resource
    */
    public static de.humance.eco.profile.model.VehicleTypeResource create(
        long vehicleTypeResourceId) {
        return getPersistence().create(vehicleTypeResourceId);
    }

    /**
    * Removes the vehicle type resource with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleTypeResourceId the primary key of the vehicle type resource
    * @return the vehicle type resource that was removed
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource remove(
        long vehicleTypeResourceId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException {
        return getPersistence().remove(vehicleTypeResourceId);
    }

    public static de.humance.eco.profile.model.VehicleTypeResource updateImpl(
        de.humance.eco.profile.model.VehicleTypeResource vehicleTypeResource)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(vehicleTypeResource);
    }

    /**
    * Returns the vehicle type resource with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleTypeResourceException} if it could not be found.
    *
    * @param vehicleTypeResourceId the primary key of the vehicle type resource
    * @return the vehicle type resource
    * @throws de.humance.eco.profile.NoSuchVehicleTypeResourceException if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource findByPrimaryKey(
        long vehicleTypeResourceId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleTypeResourceException {
        return getPersistence().findByPrimaryKey(vehicleTypeResourceId);
    }

    /**
    * Returns the vehicle type resource with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param vehicleTypeResourceId the primary key of the vehicle type resource
    * @return the vehicle type resource, or <code>null</code> if a vehicle type resource with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.VehicleTypeResource fetchByPrimaryKey(
        long vehicleTypeResourceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(vehicleTypeResourceId);
    }

    /**
    * Returns all the vehicle type resources.
    *
    * @return the vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the vehicle type resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @return the range of vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the vehicle type resources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleTypeResourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicle type resources
    * @param end the upper bound of the range of vehicle type resources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.VehicleTypeResource> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the vehicle type resources from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of vehicle type resources.
    *
    * @return the number of vehicle type resources
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static VehicleTypeResourcePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (VehicleTypeResourcePersistence) PortletBeanLocatorUtil.locate(de.humance.eco.profile.service.ClpSerializer.getServletContextName(),
                    VehicleTypeResourcePersistence.class.getName());

            ReferenceRegistry.registerReference(VehicleTypeResourceUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(VehicleTypeResourcePersistence persistence) {
    }
}
