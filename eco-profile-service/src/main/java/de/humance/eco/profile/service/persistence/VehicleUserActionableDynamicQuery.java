package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import de.humance.eco.profile.model.VehicleUser;
import de.humance.eco.profile.service.VehicleUserLocalServiceUtil;

/**
 * @author Humance
 * @generated
 */
public abstract class VehicleUserActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public VehicleUserActionableDynamicQuery() throws SystemException {
        setBaseLocalService(VehicleUserLocalServiceUtil.getService());
        setClass(VehicleUser.class);

        setClassLoader(de.humance.eco.profile.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("vehicleUserId");
    }
}
