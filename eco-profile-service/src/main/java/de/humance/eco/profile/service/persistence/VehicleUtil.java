package de.humance.eco.profile.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import de.humance.eco.profile.model.Vehicle;

import java.util.List;

/**
 * The persistence utility for the vehicle service. This utility wraps {@link VehiclePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Humance
 * @see VehiclePersistence
 * @see VehiclePersistenceImpl
 * @generated
 */
public class VehicleUtil {
    private static VehiclePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Vehicle vehicle) {
        getPersistence().clearCache(vehicle);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Vehicle> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Vehicle> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Vehicle> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Vehicle update(Vehicle vehicle) throws SystemException {
        return getPersistence().update(vehicle);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Vehicle update(Vehicle vehicle, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(vehicle, serviceContext);
    }

    /**
    * Returns all the vehicles where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverId(
        long driverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDriverId(driverId);
    }

    /**
    * Returns a range of all the vehicles where driverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverId(
        long driverId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDriverId(driverId, start, end);
    }

    /**
    * Returns an ordered range of all the vehicles where driverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverId(
        long driverId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDriverId(driverId, start, end, orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByDriverId_First(
        long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence().findByDriverId_First(driverId, orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByDriverId_First(
        long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByDriverId_First(driverId, orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByDriverId_Last(
        long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence().findByDriverId_Last(driverId, orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByDriverId_Last(
        long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByDriverId_Last(driverId, orderByComparator);
    }

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where driverId = &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param driverId the driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle[] findByDriverId_PrevAndNext(
        long vehicleId, long driverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByDriverId_PrevAndNext(vehicleId, driverId,
            orderByComparator);
    }

    /**
    * Removes all the vehicles where driverId = &#63; from the database.
    *
    * @param driverId the driver ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByDriverId(long driverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByDriverId(driverId);
    }

    /**
    * Returns the number of vehicles where driverId = &#63;.
    *
    * @param driverId the driver ID
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static int countByDriverId(long driverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByDriverId(driverId);
    }

    /**
    * Returns all the vehicles where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverIdAndOrgaId(
        long driverId, long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDriverIdAndOrgaId(driverId, organizationId);
    }

    /**
    * Returns a range of all the vehicles where driverId = &#63; and organizationId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverIdAndOrgaId(
        long driverId, long organizationId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDriverIdAndOrgaId(driverId, organizationId, start, end);
    }

    /**
    * Returns an ordered range of all the vehicles where driverId = &#63; and organizationId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverIdAndOrgaId(
        long driverId, long organizationId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDriverIdAndOrgaId(driverId, organizationId, start,
            end, orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByDriverIdAndOrgaId_First(
        long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByDriverIdAndOrgaId_First(driverId, organizationId,
            orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByDriverIdAndOrgaId_First(
        long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByDriverIdAndOrgaId_First(driverId, organizationId,
            orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByDriverIdAndOrgaId_Last(
        long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByDriverIdAndOrgaId_Last(driverId, organizationId,
            orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByDriverIdAndOrgaId_Last(
        long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByDriverIdAndOrgaId_Last(driverId, organizationId,
            orderByComparator);
    }

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where driverId = &#63; and organizationId = &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle[] findByDriverIdAndOrgaId_PrevAndNext(
        long vehicleId, long driverId, long organizationId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByDriverIdAndOrgaId_PrevAndNext(vehicleId, driverId,
            organizationId, orderByComparator);
    }

    /**
    * Removes all the vehicles where driverId = &#63; and organizationId = &#63; from the database.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByDriverIdAndOrgaId(long driverId,
        long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByDriverIdAndOrgaId(driverId, organizationId);
    }

    /**
    * Returns the number of vehicles where driverId = &#63; and organizationId = &#63;.
    *
    * @param driverId the driver ID
    * @param organizationId the organization ID
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static int countByDriverIdAndOrgaId(long driverId,
        long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByDriverIdAndOrgaId(driverId, organizationId);
    }

    /**
    * Returns all the vehicles where typeId = &#63;.
    *
    * @param typeId the type ID
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByTypeId(
        long typeId) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTypeId(typeId);
    }

    /**
    * Returns a range of all the vehicles where typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param typeId the type ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByTypeId(
        long typeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTypeId(typeId, start, end);
    }

    /**
    * Returns an ordered range of all the vehicles where typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param typeId the type ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByTypeId(
        long typeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByTypeId(typeId, start, end, orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByTypeId_First(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence().findByTypeId_First(typeId, orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByTypeId_First(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByTypeId_First(typeId, orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByTypeId_Last(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence().findByTypeId_Last(typeId, orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByTypeId_Last(
        long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByTypeId_Last(typeId, orderByComparator);
    }

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where typeId = &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle[] findByTypeId_PrevAndNext(
        long vehicleId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByTypeId_PrevAndNext(vehicleId, typeId,
            orderByComparator);
    }

    /**
    * Removes all the vehicles where typeId = &#63; from the database.
    *
    * @param typeId the type ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByTypeId(long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByTypeId(typeId);
    }

    /**
    * Returns the number of vehicles where typeId = &#63;.
    *
    * @param typeId the type ID
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static int countByTypeId(long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByTypeId(typeId);
    }

    /**
    * Returns all the vehicles where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverIdAndTypeId(
        long driverId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDriverIdAndTypeId(driverId, typeId);
    }

    /**
    * Returns a range of all the vehicles where driverId = &#63; and typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverIdAndTypeId(
        long driverId, long typeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDriverIdAndTypeId(driverId, typeId, start, end);
    }

    /**
    * Returns an ordered range of all the vehicles where driverId = &#63; and typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByDriverIdAndTypeId(
        long driverId, long typeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDriverIdAndTypeId(driverId, typeId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByDriverIdAndTypeId_First(
        long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByDriverIdAndTypeId_First(driverId, typeId,
            orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByDriverIdAndTypeId_First(
        long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByDriverIdAndTypeId_First(driverId, typeId,
            orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByDriverIdAndTypeId_Last(
        long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByDriverIdAndTypeId_Last(driverId, typeId,
            orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByDriverIdAndTypeId_Last(
        long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByDriverIdAndTypeId_Last(driverId, typeId,
            orderByComparator);
    }

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where driverId = &#63; and typeId = &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param driverId the driver ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle[] findByDriverIdAndTypeId_PrevAndNext(
        long vehicleId, long driverId, long typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByDriverIdAndTypeId_PrevAndNext(vehicleId, driverId,
            typeId, orderByComparator);
    }

    /**
    * Removes all the vehicles where driverId = &#63; and typeId = &#63; from the database.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByDriverIdAndTypeId(long driverId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByDriverIdAndTypeId(driverId, typeId);
    }

    /**
    * Returns the number of vehicles where driverId = &#63; and typeId = &#63;.
    *
    * @param driverId the driver ID
    * @param typeId the type ID
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static int countByDriverIdAndTypeId(long driverId, long typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByDriverIdAndTypeId(driverId, typeId);
    }

    /**
    * Returns all the vehicles where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByManufacturerId(
        long manufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByManufacturerId(manufacturerId);
    }

    /**
    * Returns a range of all the vehicles where manufacturerId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param manufacturerId the manufacturer ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByManufacturerId(
        long manufacturerId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByManufacturerId(manufacturerId, start, end);
    }

    /**
    * Returns an ordered range of all the vehicles where manufacturerId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param manufacturerId the manufacturer ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByManufacturerId(
        long manufacturerId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByManufacturerId(manufacturerId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByManufacturerId_First(
        long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByManufacturerId_First(manufacturerId, orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByManufacturerId_First(
        long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByManufacturerId_First(manufacturerId,
            orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByManufacturerId_Last(
        long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByManufacturerId_Last(manufacturerId, orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByManufacturerId_Last(
        long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByManufacturerId_Last(manufacturerId, orderByComparator);
    }

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where manufacturerId = &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param manufacturerId the manufacturer ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle[] findByManufacturerId_PrevAndNext(
        long vehicleId, long manufacturerId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByManufacturerId_PrevAndNext(vehicleId, manufacturerId,
            orderByComparator);
    }

    /**
    * Removes all the vehicles where manufacturerId = &#63; from the database.
    *
    * @param manufacturerId the manufacturer ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByManufacturerId(long manufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByManufacturerId(manufacturerId);
    }

    /**
    * Returns the number of vehicles where manufacturerId = &#63;.
    *
    * @param manufacturerId the manufacturer ID
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static int countByManufacturerId(long manufacturerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByManufacturerId(manufacturerId);
    }

    /**
    * Returns all the vehicles where modelName = &#63;.
    *
    * @param modelName the model name
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByModelName(
        java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByModelName(modelName);
    }

    /**
    * Returns a range of all the vehicles where modelName = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param modelName the model name
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByModelName(
        java.lang.String modelName, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByModelName(modelName, start, end);
    }

    /**
    * Returns an ordered range of all the vehicles where modelName = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param modelName the model name
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByModelName(
        java.lang.String modelName, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByModelName(modelName, start, end, orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where modelName = &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByModelName_First(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByModelName_First(modelName, orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where modelName = &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByModelName_First(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByModelName_First(modelName, orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where modelName = &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByModelName_Last(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByModelName_Last(modelName, orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where modelName = &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByModelName_Last(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByModelName_Last(modelName, orderByComparator);
    }

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where modelName = &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle[] findByModelName_PrevAndNext(
        long vehicleId, java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByModelName_PrevAndNext(vehicleId, modelName,
            orderByComparator);
    }

    /**
    * Removes all the vehicles where modelName = &#63; from the database.
    *
    * @param modelName the model name
    * @throws SystemException if a system exception occurred
    */
    public static void removeByModelName(java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByModelName(modelName);
    }

    /**
    * Returns the number of vehicles where modelName = &#63;.
    *
    * @param modelName the model name
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static int countByModelName(java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByModelName(modelName);
    }

    /**
    * Returns all the vehicles where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByModelNameLike(
        java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByModelNameLike(modelName);
    }

    /**
    * Returns a range of all the vehicles where modelName LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param modelName the model name
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByModelNameLike(
        java.lang.String modelName, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByModelNameLike(modelName, start, end);
    }

    /**
    * Returns an ordered range of all the vehicles where modelName LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param modelName the model name
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByModelNameLike(
        java.lang.String modelName, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByModelNameLike(modelName, start, end, orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByModelNameLike_First(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByModelNameLike_First(modelName, orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByModelNameLike_First(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByModelNameLike_First(modelName, orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByModelNameLike_Last(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByModelNameLike_Last(modelName, orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByModelNameLike_Last(
        java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByModelNameLike_Last(modelName, orderByComparator);
    }

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where modelName LIKE &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param modelName the model name
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle[] findByModelNameLike_PrevAndNext(
        long vehicleId, java.lang.String modelName,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByModelNameLike_PrevAndNext(vehicleId, modelName,
            orderByComparator);
    }

    /**
    * Removes all the vehicles where modelName LIKE &#63; from the database.
    *
    * @param modelName the model name
    * @throws SystemException if a system exception occurred
    */
    public static void removeByModelNameLike(java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByModelNameLike(modelName);
    }

    /**
    * Returns the number of vehicles where modelName LIKE &#63;.
    *
    * @param modelName the model name
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static int countByModelNameLike(java.lang.String modelName)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByModelNameLike(modelName);
    }

    /**
    * Returns all the vehicles where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByLicensePlate(
        java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByLicensePlate(licensePlate);
    }

    /**
    * Returns a range of all the vehicles where licensePlate = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param licensePlate the license plate
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByLicensePlate(
        java.lang.String licensePlate, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByLicensePlate(licensePlate, start, end);
    }

    /**
    * Returns an ordered range of all the vehicles where licensePlate = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param licensePlate the license plate
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByLicensePlate(
        java.lang.String licensePlate, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByLicensePlate(licensePlate, start, end,
            orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByLicensePlate_First(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByLicensePlate_First(licensePlate, orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByLicensePlate_First(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByLicensePlate_First(licensePlate, orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByLicensePlate_Last(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByLicensePlate_Last(licensePlate, orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByLicensePlate_Last(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByLicensePlate_Last(licensePlate, orderByComparator);
    }

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where licensePlate = &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle[] findByLicensePlate_PrevAndNext(
        long vehicleId, java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByLicensePlate_PrevAndNext(vehicleId, licensePlate,
            orderByComparator);
    }

    /**
    * Removes all the vehicles where licensePlate = &#63; from the database.
    *
    * @param licensePlate the license plate
    * @throws SystemException if a system exception occurred
    */
    public static void removeByLicensePlate(java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByLicensePlate(licensePlate);
    }

    /**
    * Returns the number of vehicles where licensePlate = &#63;.
    *
    * @param licensePlate the license plate
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static int countByLicensePlate(java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByLicensePlate(licensePlate);
    }

    /**
    * Returns all the vehicles where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByLicensePlateLike(
        java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByLicensePlateLike(licensePlate);
    }

    /**
    * Returns a range of all the vehicles where licensePlate LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param licensePlate the license plate
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByLicensePlateLike(
        java.lang.String licensePlate, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByLicensePlateLike(licensePlate, start, end);
    }

    /**
    * Returns an ordered range of all the vehicles where licensePlate LIKE &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param licensePlate the license plate
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findByLicensePlateLike(
        java.lang.String licensePlate, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByLicensePlateLike(licensePlate, start, end,
            orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByLicensePlateLike_First(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByLicensePlateLike_First(licensePlate, orderByComparator);
    }

    /**
    * Returns the first vehicle in the ordered set where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByLicensePlateLike_First(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByLicensePlateLike_First(licensePlate,
            orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByLicensePlateLike_Last(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByLicensePlateLike_Last(licensePlate, orderByComparator);
    }

    /**
    * Returns the last vehicle in the ordered set where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByLicensePlateLike_Last(
        java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByLicensePlateLike_Last(licensePlate, orderByComparator);
    }

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where licensePlate LIKE &#63;.
    *
    * @param vehicleId the primary key of the current vehicle
    * @param licensePlate the license plate
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle[] findByLicensePlateLike_PrevAndNext(
        long vehicleId, java.lang.String licensePlate,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence()
                   .findByLicensePlateLike_PrevAndNext(vehicleId, licensePlate,
            orderByComparator);
    }

    /**
    * Removes all the vehicles where licensePlate LIKE &#63; from the database.
    *
    * @param licensePlate the license plate
    * @throws SystemException if a system exception occurred
    */
    public static void removeByLicensePlateLike(java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByLicensePlateLike(licensePlate);
    }

    /**
    * Returns the number of vehicles where licensePlate LIKE &#63;.
    *
    * @param licensePlate the license plate
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public static int countByLicensePlateLike(java.lang.String licensePlate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByLicensePlateLike(licensePlate);
    }

    /**
    * Caches the vehicle in the entity cache if it is enabled.
    *
    * @param vehicle the vehicle
    */
    public static void cacheResult(de.humance.eco.profile.model.Vehicle vehicle) {
        getPersistence().cacheResult(vehicle);
    }

    /**
    * Caches the vehicles in the entity cache if it is enabled.
    *
    * @param vehicles the vehicles
    */
    public static void cacheResult(
        java.util.List<de.humance.eco.profile.model.Vehicle> vehicles) {
        getPersistence().cacheResult(vehicles);
    }

    /**
    * Creates a new vehicle with the primary key. Does not add the vehicle to the database.
    *
    * @param vehicleId the primary key for the new vehicle
    * @return the new vehicle
    */
    public static de.humance.eco.profile.model.Vehicle create(long vehicleId) {
        return getPersistence().create(vehicleId);
    }

    /**
    * Removes the vehicle with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicleId the primary key of the vehicle
    * @return the vehicle that was removed
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle remove(long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence().remove(vehicleId);
    }

    public static de.humance.eco.profile.model.Vehicle updateImpl(
        de.humance.eco.profile.model.Vehicle vehicle)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(vehicle);
    }

    /**
    * Returns the vehicle with the primary key or throws a {@link de.humance.eco.profile.NoSuchVehicleException} if it could not be found.
    *
    * @param vehicleId the primary key of the vehicle
    * @return the vehicle
    * @throws de.humance.eco.profile.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle findByPrimaryKey(
        long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException,
            de.humance.eco.profile.NoSuchVehicleException {
        return getPersistence().findByPrimaryKey(vehicleId);
    }

    /**
    * Returns the vehicle with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param vehicleId the primary key of the vehicle
    * @return the vehicle, or <code>null</code> if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static de.humance.eco.profile.model.Vehicle fetchByPrimaryKey(
        long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(vehicleId);
    }

    /**
    * Returns all the vehicles.
    *
    * @return the vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the vehicles.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the vehicles.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link de.humance.eco.profile.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<de.humance.eco.profile.model.Vehicle> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the vehicles from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of vehicles.
    *
    * @return the number of vehicles
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static VehiclePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (VehiclePersistence) PortletBeanLocatorUtil.locate(de.humance.eco.profile.service.ClpSerializer.getServletContextName(),
                    VehiclePersistence.class.getName());

            ReferenceRegistry.registerReference(VehicleUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(VehiclePersistence persistence) {
    }
}
