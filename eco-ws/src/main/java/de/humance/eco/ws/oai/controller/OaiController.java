package de.humance.eco.ws.oai.controller;

import java.io.ByteArrayOutputStream;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

import de.humance.eco.ws.oai.exception.OaiBadArgumentException;
import de.humance.eco.ws.oai.exception.OaiBadResumptionTokenException;
import de.humance.eco.ws.oai.exception.OaiBadVerbException;
import de.humance.eco.ws.oai.exception.OaiCannotDisseminateFormatException;
import de.humance.eco.ws.oai.exception.OaiIdDoesNotExistException;
import de.humance.eco.ws.oai.exception.OaiNoMetadataFormatsException;
import de.humance.eco.ws.oai.exception.OaiNoRecordsMatchException;
import de.humance.eco.ws.oai.exception.OaiNoSetHierarchyException;
import de.humance.eco.ws.oai.model.ErrorDocument;
import de.humance.eco.ws.oai.model.OaipmhDocument;
import de.humance.eco.ws.oai.processor.OaiProcessor;
import de.humance.eco.ws.util.OaiConstants;
import de.humance.eco.ws.util.OaiErrorCode;
import de.humance.eco.ws.util.OaiVerb;

@Controller
public class OaiController {
	private Log log = LogFactoryUtil.getLog(getClass());
	private Marshaller jaxbMarshaller = null;

	private void checkJaxbMarshaller() throws JAXBException {
		if (jaxbMarshaller != null) {
			return;
		}

		JAXBContext jaxbContext = JAXBContext
				.newInstance("de.humance.eco.ws.oai.model" + StringPool.COLON
						+ "de.humance.eco.ws.oai.model.dc" + StringPool.COLON
						+ "de.humance.eco.ws.oai.model.lom" + StringPool.COLON
						+ "de.humance.eco.ws.oai.model.eco");
		jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,
				OaiConstants.SCHEMA_LOCATION + StringPool.SPACE
						+ OaiConstants.OAI_DC_NAMESPACE + StringPool.SPACE
						+ OaiConstants.OAI_DC_SCHEMA + StringPool.SPACE
						+ OaiConstants.LOM_NAMESPACE + StringPool.SPACE
						+ OaiConstants.LOM_SCHEMA);
	}

	@RequestMapping(value = "/oai", method = RequestMethod.GET, produces = "application/xml")
	@ResponseBody
	public String doOaiOperation(@RequestParam(required = true) String verb,
			@RequestParam(required = false) String resumptionToken,
			@RequestParam(required = false) String identifier,
			@RequestParam(required = false) String metadataPrefix,
			@RequestParam(required = false) String from,
			@RequestParam(required = false) String until,
			@RequestParam(required = false) String set)
			throws OaiBadVerbException, OaiBadArgumentException,
			OaiBadResumptionTokenException,
			OaiCannotDisseminateFormatException, OaiIdDoesNotExistException,
			OaiNoMetadataFormatsException, OaiNoRecordsMatchException,
			OaiNoSetHierarchyException, JAXBException, Exception {
		// Check the availability of the JAXB marshaller
		checkJaxbMarshaller();

		// Generate the OAI-PMH document
		OaipmhDocument document = null;
		if (verb.equals(OaiVerb.IDENTIFY.getXmlValue())) {
			// CASE : Identify
			// If there are any arguments, return a BadArgument error
			OaiProcessor.getInstance().checkIdentifyArgs(resumptionToken,
					identifier, metadataPrefix, from, until, set);
			document = OaiProcessor.getInstance().processIdentify();

		} else if (verb.equals(OaiVerb.LIST_IDENTIFIERS.getXmlValue())) {
			// CASE : ListIdentifiers
			OaiProcessor.getInstance().checkListIdentifiersArgs(
					resumptionToken, metadataPrefix, from, until, set);
			document = OaiProcessor.getInstance().processListIdentifiers(
					resumptionToken, metadataPrefix, from, until, set);

		} else if (verb.equals(OaiVerb.LIST_RECORDS.getXmlValue())) {
			// CASE : ListRecords
			OaiProcessor.getInstance().checkListRecordsArgs(resumptionToken,
					metadataPrefix, from, until, set);
			document = OaiProcessor.getInstance().processListRecords(
					resumptionToken, metadataPrefix, from, until, set);

		} else if (verb.equals(OaiVerb.GET_RECORD.getXmlValue())) {
			// CASE : GetRecord
			OaiProcessor.getInstance().checkGetRecordArgs(identifier,
					metadataPrefix);
			document = OaiProcessor.getInstance().processGetRecord(identifier,
					metadataPrefix);

		} else if (verb.equals(OaiVerb.LIST_METADATA_FORMATS.getXmlValue())) {
			// CASE : ListMetadataFormats
			OaiProcessor.getInstance().checkListMetadataFormatsArgs(identifier);
			document = OaiProcessor.getInstance().processListMetadataFormats(
					identifier);

		} else if (verb.equals(OaiVerb.LIST_SETS.getXmlValue())) {
			// CASE : ListSets
			OaiProcessor.getInstance().checkListSetsArgs(resumptionToken);
			document = OaiProcessor.getInstance().processListSets(
					resumptionToken);

		} else {
			// CASE : Verb not recognized, return an error
			throw new OaiBadVerbException(verb);
		}

		// Set the response date
		document.setResponseDate(new Date());

		// Convert the document to string
		String result = convertDocumentToString(document);
		log.debug("OAI-PMH marshalled XML : " + result);
		return result;
	}

	@ExceptionHandler(OaiBadArgumentException.class)
	@ResponseBody
	public String badVerb(OaiBadArgumentException e) throws JAXBException {
		// Generate the error response
		ErrorDocument document = new ErrorDocument(e.getVerb(),
				OaiErrorCode.BAD_ARGUMENT);
		document.setResponseDate(new Date());

		return convertDocumentToString(document);
	}

	@ExceptionHandler(OaiBadVerbException.class)
	@ResponseBody
	public String badVerb(OaiBadVerbException e) throws JAXBException {
		// Generate the error response
		ErrorDocument document = new ErrorDocument(e.getVerb(),
				OaiErrorCode.BAD_VERB);
		document.setResponseDate(new Date());

		return convertDocumentToString(document);
	}

	@ExceptionHandler(OaiBadResumptionTokenException.class)
	@ResponseBody
	public String badVerb(OaiBadResumptionTokenException e)
			throws JAXBException {
		// Generate the error response
		ErrorDocument document = new ErrorDocument(e.getVerb(),
				OaiErrorCode.BAD_RESUMPTION_TOKEN);
		document.setResponseDate(new Date());

		return convertDocumentToString(document);
	}

	@ExceptionHandler(OaiCannotDisseminateFormatException.class)
	@ResponseBody
	public String badVerb(OaiCannotDisseminateFormatException e)
			throws JAXBException {
		// Generate the error response
		ErrorDocument document = new ErrorDocument(e.getVerb(),
				OaiErrorCode.CANNOT_DISSEMINATE_FORMAT);
		document.setResponseDate(new Date());

		return convertDocumentToString(document);
	}

	@ExceptionHandler(OaiIdDoesNotExistException.class)
	@ResponseBody
	public String badVerb(OaiIdDoesNotExistException e) throws JAXBException {
		// Generate the error response
		ErrorDocument document = new ErrorDocument(e.getVerb(),
				OaiErrorCode.ID_DOES_NOT_EXIST);
		document.setResponseDate(new Date());

		return convertDocumentToString(document);
	}

	@ExceptionHandler(OaiNoMetadataFormatsException.class)
	@ResponseBody
	public String badVerb(OaiNoMetadataFormatsException e) throws JAXBException {
		// Generate the error response
		ErrorDocument document = new ErrorDocument(e.getVerb(),
				OaiErrorCode.NO_METADATA_FORMATS);
		document.setResponseDate(new Date());

		return convertDocumentToString(document);
	}

	@ExceptionHandler(OaiNoRecordsMatchException.class)
	@ResponseBody
	public String badVerb(OaiNoRecordsMatchException e) throws JAXBException {
		// Generate the error response
		ErrorDocument document = new ErrorDocument(e.getVerb(),
				OaiErrorCode.NO_RECORDS_MATCH);
		document.setResponseDate(new Date());

		return convertDocumentToString(document);
	}

	@ExceptionHandler(OaiNoSetHierarchyException.class)
	@ResponseBody
	public String badVerb(OaiNoSetHierarchyException e) throws JAXBException {
		// Generate the error response
		ErrorDocument document = new ErrorDocument(e.getVerb(),
				OaiErrorCode.NO_SET_HIERARCHY);
		document.setResponseDate(new Date());

		return convertDocumentToString(document);
	}

	@ExceptionHandler(JAXBException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Internal Server Error")
	public void jaxbFailed(Exception e) {
		log.error("OAI Exception : " + e.getMessage(), e);
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Internal Server Error")
	public void serviceFailed(Exception e) {
		log.error("OAI Exception : " + e.getMessage(), e);
	}

	private String convertDocumentToString(OaipmhDocument document)
			throws JAXBException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		jaxbMarshaller.marshal(document, baos);
		return new String(baos.toByteArray());
	}
}
