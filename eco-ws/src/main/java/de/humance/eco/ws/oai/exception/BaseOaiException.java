package de.humance.eco.ws.oai.exception;

public abstract class BaseOaiException extends Exception {
	private static final long serialVersionUID = 5927168598159692824L;
	private String verb = null;

	public BaseOaiException(String verb) {
		super();

		this.verb = verb;
	}

	public BaseOaiException(String verb, String message) {
		super(message);

		this.verb = verb;
	}

	public BaseOaiException(String verb, Throwable cause) {
		super(cause);

		this.verb = verb;
	}

	public BaseOaiException(String verb, String message, Throwable cause) {
		super(message, cause);

		this.verb = verb;
	}

	public String getVerb() {
		return verb;
	}

	public void setVerb(String verb) {
		this.verb = verb;
	}

}
