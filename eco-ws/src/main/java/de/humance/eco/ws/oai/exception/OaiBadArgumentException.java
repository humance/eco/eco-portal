package de.humance.eco.ws.oai.exception;

public class OaiBadArgumentException extends BaseOaiException {
	private static final long serialVersionUID = 1628099356087112373L;

	public OaiBadArgumentException(String verb) {
		super(verb);
	}

	public OaiBadArgumentException(String verb, String message) {
		super(verb, message);
	}

	public OaiBadArgumentException(String verb, Throwable cause) {
		super(verb, cause);
	}

	public OaiBadArgumentException(String verb, String message, Throwable cause) {
		super(verb, message, cause);
	}
}
