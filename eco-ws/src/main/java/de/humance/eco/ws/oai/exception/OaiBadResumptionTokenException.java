package de.humance.eco.ws.oai.exception;

public class OaiBadResumptionTokenException extends BaseOaiException {
	private static final long serialVersionUID = 1628099356087112373L;

	public OaiBadResumptionTokenException(String verb) {
		super(verb);
	}

	public OaiBadResumptionTokenException(String verb, String message) {
		super(verb, message);
	}

	public OaiBadResumptionTokenException(String verb, Throwable cause) {
		super(verb, cause);
	}

	public OaiBadResumptionTokenException(String verb, String message,
			Throwable cause) {
		super(verb, message, cause);
	}
}
