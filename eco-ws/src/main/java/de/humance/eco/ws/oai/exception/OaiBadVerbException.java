package de.humance.eco.ws.oai.exception;

public class OaiBadVerbException extends BaseOaiException {
	private static final long serialVersionUID = 1628099356087112373L;

	public OaiBadVerbException(String verb) {
		super(verb);
	}

	public OaiBadVerbException(String verb, String message) {
		super(verb, message);
	}

	public OaiBadVerbException(String verb, Throwable cause) {
		super(verb, cause);
	}

	public OaiBadVerbException(String verb, String message, Throwable cause) {
		super(verb, message, cause);
	}

}
