package de.humance.eco.ws.oai.exception;

public class OaiCannotDisseminateFormatException extends BaseOaiException {
	private static final long serialVersionUID = 1628099356087112373L;

	public OaiCannotDisseminateFormatException(String verb) {
		super(verb);
	}

	public OaiCannotDisseminateFormatException(String verb, String message) {
		super(verb, message);
	}

	public OaiCannotDisseminateFormatException(String verb, Throwable cause) {
		super(verb, cause);
	}

	public OaiCannotDisseminateFormatException(String verb, String message,
			Throwable cause) {
		super(verb, message, cause);
	}
}
