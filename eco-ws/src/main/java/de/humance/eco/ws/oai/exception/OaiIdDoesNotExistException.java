package de.humance.eco.ws.oai.exception;

public class OaiIdDoesNotExistException extends BaseOaiException {
	private static final long serialVersionUID = 1628099356087112373L;

	public OaiIdDoesNotExistException(String verb) {
		super(verb);
	}

	public OaiIdDoesNotExistException(String verb, String message) {
		super(verb, message);
	}

	public OaiIdDoesNotExistException(String verb, Throwable cause) {
		super(verb, cause);
	}

	public OaiIdDoesNotExistException(String verb, String message,
			Throwable cause) {
		super(verb, message, cause);
	}
}
