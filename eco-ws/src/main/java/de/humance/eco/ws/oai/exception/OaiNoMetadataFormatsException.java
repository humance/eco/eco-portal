package de.humance.eco.ws.oai.exception;

public class OaiNoMetadataFormatsException extends BaseOaiException {
	private static final long serialVersionUID = 1628099356087112373L;

	public OaiNoMetadataFormatsException(String verb) {
		super(verb);
	}

	public OaiNoMetadataFormatsException(String verb, String message) {
		super(verb, message);
	}

	public OaiNoMetadataFormatsException(String verb, Throwable cause) {
		super(verb, cause);
	}

	public OaiNoMetadataFormatsException(String verb, String message,
			Throwable cause) {
		super(verb, message, cause);
	}
}
