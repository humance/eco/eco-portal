package de.humance.eco.ws.oai.exception;

public class OaiNoRecordsMatchException extends BaseOaiException {
	private static final long serialVersionUID = 1628099356087112373L;

	public OaiNoRecordsMatchException(String verb) {
		super(verb);
	}

	public OaiNoRecordsMatchException(String verb, String message) {
		super(verb, message);
	}

	public OaiNoRecordsMatchException(String verb, Throwable cause) {
		super(verb, cause);
	}

	public OaiNoRecordsMatchException(String verb, String message,
			Throwable cause) {
		super(verb, message, cause);
	}
}
