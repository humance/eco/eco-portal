package de.humance.eco.ws.oai.exception;

public class OaiNoSetHierarchyException extends BaseOaiException {
	private static final long serialVersionUID = 1628099356087112373L;

	public OaiNoSetHierarchyException(String verb) {
		super(verb);
	}

	public OaiNoSetHierarchyException(String verb, String message) {
		super(verb, message);
	}

	public OaiNoSetHierarchyException(String verb, Throwable cause) {
		super(verb, cause);
	}

	public OaiNoSetHierarchyException(String verb, String message,
			Throwable cause) {
		super(verb, message, cause);
	}
}
