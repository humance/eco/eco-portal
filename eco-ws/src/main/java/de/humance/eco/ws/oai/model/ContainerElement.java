package de.humance.eco.ws.oai.model;

public abstract class ContainerElement<T> {
	protected T element = null;

	public ContainerElement() {
	}

	public ContainerElement(T element) {
		this.element = element;
	}

	public T getElement() {
		return this.element;
	}

	public void setElement(T element) {
		this.element = element;
	}
}
