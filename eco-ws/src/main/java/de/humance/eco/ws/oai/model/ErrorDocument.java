package de.humance.eco.ws.oai.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.humance.eco.ws.util.OaiConstants;
import de.humance.eco.ws.util.OaiErrorCode;

@XmlRootElement(name = OaiConstants.DOCUMENT_ROOT_ELEMENT)
public class ErrorDocument extends OaipmhDocument {
	private ErrorElement error = null;

	public ErrorDocument() {
	}

	public ErrorDocument(String verb, OaiErrorCode errorCode) {
		super(verb);

		this.error = new ErrorElement(errorCode);
	}

	@XmlElement(required = true)
	public ErrorElement getError() {
		return error;
	}

	public void setError(ErrorElement error) {
		this.error = error;
	}

}
