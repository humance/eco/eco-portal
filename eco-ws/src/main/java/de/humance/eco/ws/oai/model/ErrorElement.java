package de.humance.eco.ws.oai.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

import de.humance.eco.ws.util.OaiErrorCode;

public class ErrorElement {
	private String code = null;
	private String errorValue = null;

	public ErrorElement() {
	}

	public ErrorElement(OaiErrorCode code) {
		this.code = code.getXmlValue();
		this.errorValue = code.getDescription();
	}

	public ErrorElement(OaiErrorCode code, String errorValue) {
		this.code = code.getXmlValue();
		this.errorValue = errorValue;
	}

	@XmlAttribute(required = true)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCode(OaiErrorCode code) {
		this.code = code.getXmlValue();
	}

	@XmlValue
	public String getErrorValue() {
		return errorValue;
	}

	public void setErrorValue(String errorValue) {
		this.errorValue = errorValue;
	}

	public void setErrorValue(OaiErrorCode code) {
		this.errorValue = code.getDescription();
	}

}
