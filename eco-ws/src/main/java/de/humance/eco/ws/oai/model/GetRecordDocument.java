package de.humance.eco.ws.oai.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.humance.eco.ws.util.DeleteRecord;
import de.humance.eco.ws.util.OaiConstants;
import de.humance.eco.ws.util.OaiVerb;

@XmlRootElement(name = OaiConstants.DOCUMENT_ROOT_ELEMENT)
public class GetRecordDocument extends OaipmhDocument {
	private IdentifyElement identify = null;

	public GetRecordDocument() {
		super(OaiVerb.GET_RECORD);
	}

	public GetRecordDocument(String repositoryName, String baseUrl,
			String protocolVersion, String adminEmail, Date earliestDatestamp,
			DeleteRecord deleteRecord, String granularity) {
		this();

		this.identify = new IdentifyElement(repositoryName, baseUrl,
				protocolVersion, adminEmail, earliestDatestamp, deleteRecord,
				granularity);
	}

	@XmlElement(name = "Identify", required = true)
	public IdentifyElement getIdentify() {
		return identify;
	}

	public void setIdentify(IdentifyElement identify) {
		this.identify = identify;
	}

}
