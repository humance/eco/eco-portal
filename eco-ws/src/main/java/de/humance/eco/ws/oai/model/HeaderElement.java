package de.humance.eco.ws.oai.model;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import de.humance.eco.ws.util.WsUtil;

public class HeaderElement {
	private String status = null;
	private String identifier = null;
	private Date datestamp = null;
	private List<String> specs = null;

	public HeaderElement() {
	}

	public HeaderElement(String identifier, String status, Date datestamp,
			List<String> specs) {
		this.identifier = identifier;
		this.status = status;
		this.datestamp = datestamp;
		this.specs = specs;
	}

	@XmlAttribute(required = false)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@XmlElement
	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	@XmlElement
	public String getDatestamp() {
		return WsUtil.getDateFormatter().format(datestamp);
	}

	public void setDatestamp(Date datestamp) {
		this.datestamp = datestamp;
	}

	@XmlElement(name = "setSpec", required = false)
	public List<String> getSpecs() {
		return specs;
	}

	public void setSpecs(List<String> specs) {
		this.specs = specs;
	}

}
