package de.humance.eco.ws.oai.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

import de.humance.eco.ws.util.DeleteRecord;
import de.humance.eco.ws.util.WsUtil;

public class IdentifyElement {
	private String repositoryName = null;
	private String baseUrl = null;
	private String protocolVersion = null;
	private String adminEmail = null;
	private Date earliestDatestamp = null;
	private String deleteRecord = DeleteRecord.NO.getXmlValue();
	private String granularity = null;

	public IdentifyElement() {
	}

	public IdentifyElement(String repositoryName, String baseUrl,
			String protocolVersion, String adminEmail, Date earliestDatestamp,
			DeleteRecord deleteRecord, String granularity) {
		this.repositoryName = repositoryName;
		this.baseUrl = baseUrl;
		this.protocolVersion = protocolVersion;
		this.adminEmail = adminEmail;
		this.earliestDatestamp = earliestDatestamp;
		this.deleteRecord = deleteRecord.getXmlValue();
		this.granularity = granularity;
	}

	@XmlElement(required = true)
	public String getRepositoryName() {
		return repositoryName;
	}

	public void setRepositoryName(String repositoryName) {
		this.repositoryName = repositoryName;
	}

	@XmlElement(name = "baseURL", required = true)
	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	@XmlElement(required = true)
	public String getProtocolVersion() {
		return protocolVersion;
	}

	public void setProtocolVersion(String protocolVersion) {
		this.protocolVersion = protocolVersion;
	}

	@XmlElement(required = true)
	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}

	@XmlElement(required = true)
	public String getEarliestDatestamp() {
		return WsUtil.getDateFormatter().format(earliestDatestamp);
	}

	public void setEarliestDatestamp(Date earliestDatestamp) {
		this.earliestDatestamp = earliestDatestamp;
	}

	@XmlElement(required = true)
	public String getDeleteRecord() {
		return deleteRecord;
	}

	public void setDeleteRecord(DeleteRecord deleteRecord) {
		this.deleteRecord = deleteRecord.getXmlValue();
	}

	@XmlElement(required = true)
	public String getGranularity() {
		return granularity;
	}

	public void setGranularity(String granularity) {
		this.granularity = granularity;
	}

}
