package de.humance.eco.ws.oai.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.humance.eco.ws.util.OaiConstants;
import de.humance.eco.ws.util.OaiVerb;

@XmlRootElement(name = OaiConstants.DOCUMENT_ROOT_ELEMENT)
public class ListIdentifiersDocument extends OaipmhDocument {
	private ListIdentifiersElement listIdentifiers = null;
	private ResumptionToken resumptionToken = null;

	public ListIdentifiersDocument() {
		super(OaiVerb.LIST_IDENTIFIERS);

		this.listIdentifiers = new ListIdentifiersElement();
	}

	@XmlElement(name = "ListIdentifiers", required = true)
	public ListIdentifiersElement getListIdentifiers() {
		return listIdentifiers;
	}

	public void setListIdentifiers(ListIdentifiersElement listIdentifiers) {
		this.listIdentifiers = listIdentifiers;
	}

	@XmlElement(required = false)
	public ResumptionToken getResumptionToken() {
		return resumptionToken;
	}

	public void setResumptionToken(ResumptionToken resumptionToken) {
		this.resumptionToken = resumptionToken;
	}

}
