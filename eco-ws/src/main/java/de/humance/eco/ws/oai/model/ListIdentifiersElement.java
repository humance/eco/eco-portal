package de.humance.eco.ws.oai.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class ListIdentifiersElement {
	private List<HeaderElement> headers = null;
	private ResumptionToken resumptionToken = null;

	public ListIdentifiersElement() {
		this.headers = new ArrayList<HeaderElement>();
	}

	public ListIdentifiersElement(List<HeaderElement> headers) {
		if (headers != null) {
			this.headers = headers;
		}
	}

	public ListIdentifiersElement(List<HeaderElement> headers,
			ResumptionToken resumptionToken) {
		this(headers);
		this.resumptionToken = resumptionToken;
	}

	@XmlElement(name = "header")
	public List<HeaderElement> getHeaders() {
		return headers;
	}

	public void addHeader(HeaderElement header) {
		this.headers.add(header);
	}

	public void removeHeader(HeaderElement header) {
		this.headers.remove(header);
	}

	public void clearHeaders() {
		this.headers.clear();
	}

	@XmlElement(required = false)
	public ResumptionToken getResumptionToken() {
		return resumptionToken;
	}

	public void setResumptionToken(ResumptionToken resumptionToken) {
		this.resumptionToken = resumptionToken;
	}
}
