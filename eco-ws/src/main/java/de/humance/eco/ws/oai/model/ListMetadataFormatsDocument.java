package de.humance.eco.ws.oai.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.humance.eco.ws.util.OaiConstants;
import de.humance.eco.ws.util.OaiVerb;

@XmlRootElement(name = OaiConstants.DOCUMENT_ROOT_ELEMENT)
public class ListMetadataFormatsDocument extends OaipmhDocument {
	private ListMetadataFormatsElement metadataFormats = null;

	public ListMetadataFormatsDocument() {
		super(OaiVerb.LIST_METADATA_FORMATS);
	}

	public ListMetadataFormatsDocument(MetadataFormatElement... formats) {
		this();

		this.metadataFormats = new ListMetadataFormatsElement(formats);
	}

	@XmlElement(name = "ListMetadataFormats", required = true)
	public ListMetadataFormatsElement getMetadataFormats() {
		return metadataFormats;
	}

}
