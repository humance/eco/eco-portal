package de.humance.eco.ws.oai.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class ListMetadataFormatsElement {
	private List<MetadataFormatElement> formats = new ArrayList<MetadataFormatElement>();

	public ListMetadataFormatsElement() {
	}

	public ListMetadataFormatsElement(MetadataFormatElement... formats) {
		if (formats != null) {
			for (MetadataFormatElement format : formats) {
				this.formats.add(format);
			}
		}
	}

	@XmlElement(name = "metadataFormat")
	public List<MetadataFormatElement> getFormats() {
		return formats;
	}

	public void addFormat(MetadataFormatElement format) {
		this.formats.add(format);
	}

	public void removeFormat(MetadataFormatElement format) {
		this.formats.remove(format);
	}

	public void clearFormats() {
		this.formats.clear();
	}
}
