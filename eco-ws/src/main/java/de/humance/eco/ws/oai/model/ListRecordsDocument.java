package de.humance.eco.ws.oai.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.humance.eco.ws.util.OaiConstants;
import de.humance.eco.ws.util.OaiVerb;

@XmlRootElement(name = OaiConstants.DOCUMENT_ROOT_ELEMENT)
public class ListRecordsDocument extends OaipmhDocument {
	private ListRecordsElement listRecords = null;
	private ResumptionToken resumptionToken = null;

	public ListRecordsDocument() {
		super(OaiVerb.LIST_RECORDS);

		this.listRecords = new ListRecordsElement();
	}

	@XmlElement(name = "ListRecords", required = true)
	public ListRecordsElement getListRecords() {
		return listRecords;
	}

	public void setListRecords(ListRecordsElement listRecords) {
		this.listRecords = listRecords;
	}

	@XmlElement(required = false)
	public ResumptionToken getResumptionToken() {
		return resumptionToken;
	}

	public void setResumptionToken(ResumptionToken resumptionToken) {
		this.resumptionToken = resumptionToken;
	}

}
