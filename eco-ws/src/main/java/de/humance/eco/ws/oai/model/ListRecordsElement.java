package de.humance.eco.ws.oai.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class ListRecordsElement {
	private List<RecordElement<?>> records = null;
	private ResumptionToken resumptionToken = null;

	public ListRecordsElement() {
		this.records = new ArrayList<RecordElement<?>>();
	}

	public ListRecordsElement(List<RecordElement<?>> headers) {
		if (headers != null) {
			this.records = headers;
		}
	}

	public ListRecordsElement(List<RecordElement<?>> headers,
			ResumptionToken resumptionToken) {
		this(headers);
		this.resumptionToken = resumptionToken;
	}

	@XmlElement(name = "record")
	public List<RecordElement<?>> getRecords() {
		return records;
	}

	public void addRecord(RecordElement<?> record) {
		this.records.add(record);
	}

	public void removeRecord(RecordElement<?> record) {
		this.records.remove(record);
	}

	public void clearRecords() {
		this.records.clear();
	}

	@XmlElement(required = false)
	public ResumptionToken getResumptionToken() {
		return resumptionToken;
	}

	public void setResumptionToken(ResumptionToken resumptionToken) {
		this.resumptionToken = resumptionToken;
	}
}
