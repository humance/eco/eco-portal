package de.humance.eco.ws.oai.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.humance.eco.ws.util.DeleteRecord;
import de.humance.eco.ws.util.OaiConstants;
import de.humance.eco.ws.util.OaiVerb;

@XmlRootElement(name = OaiConstants.DOCUMENT_ROOT_ELEMENT)
public class ListSetsDocument extends OaipmhDocument {
	private ListSetsElement listSets = null;

	public ListSetsDocument() {
		super(OaiVerb.LIST_SETS);
	}

	public ListSetsDocument(String repositoryName, String baseUrl,
			String protocolVersion, String adminEmail, Date earliestDatestamp,
			DeleteRecord deleteRecord, String granularity) {
		this();

		this.listSets = new ListSetsElement();
	}

	@XmlElement(name = "ListSets", required = true)
	public ListSetsElement getListSets() {
		return listSets;
	}

	public void setListSets(ListSetsElement listSets) {
		this.listSets = listSets;
	}

}
