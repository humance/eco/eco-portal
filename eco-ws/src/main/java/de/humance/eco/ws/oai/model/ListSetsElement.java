package de.humance.eco.ws.oai.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class ListSetsElement {
	private List<SetElement> sets = null;
	private ResumptionToken resumptionToken = null;

	public ListSetsElement() {
		this.sets = new ArrayList<SetElement>();
	}

	public ListSetsElement(List<SetElement> sets) {
		if (sets != null) {
			this.sets = sets;
		}
	}

	public ListSetsElement(List<SetElement> sets,
			ResumptionToken resumptionToken) {
		this(sets);
		this.resumptionToken = resumptionToken;
	}

	@XmlElement(name = "set")
	public List<SetElement> getSets() {
		return sets;
	}

	public void addSet(SetElement set) {
		this.sets.add(set);
	}

	public void removeSet(SetElement set) {
		this.sets.remove(set);
	}

	public void clearSets() {
		this.sets.clear();
	}

	@XmlElement(required = false)
	public ResumptionToken getResumptionToken() {
		return resumptionToken;
	}

	public void setResumptionToken(ResumptionToken resumptionToken) {
		this.resumptionToken = resumptionToken;
	}
}
