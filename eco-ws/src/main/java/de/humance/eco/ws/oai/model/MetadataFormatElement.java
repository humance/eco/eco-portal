package de.humance.eco.ws.oai.model;

import javax.xml.bind.annotation.XmlElement;

public class MetadataFormatElement {
	private String metadataPrefix = null;
	private String schema = null;
	private String metadataNamespace = null;

	public MetadataFormatElement() {
	}

	public MetadataFormatElement(String metadataPrefix, String schema,
			String metadataNamespace) {
		this.metadataPrefix = metadataPrefix;
		this.schema = schema;
		this.metadataNamespace = metadataNamespace;
	}

	@XmlElement
	public String getMetadataPrefix() {
		return metadataPrefix;
	}

	public void setMetadataPrefix(String metadataPrefix) {
		this.metadataPrefix = metadataPrefix;
	}

	@XmlElement
	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	@XmlElement
	public String getMetadataNamespace() {
		return metadataNamespace;
	}

	public void setMetadataNamespace(String metadataNamespace) {
		this.metadataNamespace = metadataNamespace;
	}

}
