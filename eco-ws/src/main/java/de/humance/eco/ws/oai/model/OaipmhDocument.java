package de.humance.eco.ws.oai.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

import de.humance.eco.ws.util.OaiVerb;
import de.humance.eco.ws.util.WsUtil;

public abstract class OaipmhDocument {
	private Date responseDate = null;
	private RequestElement request = null;

	public OaipmhDocument() {
	}

	public OaipmhDocument(OaiVerb verb) {
		this.request = new RequestElement(verb);
	}

	public OaipmhDocument(String verb) {
		this.request = new RequestElement(verb);
	}

	@XmlElement(required = true)
	public String getResponseDate() {
		return WsUtil.getDateFormatter().format(responseDate);
	}

	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}

	@XmlElement(required = true)
	public RequestElement getRequest() {
		return request;
	}

	public void setRequest(RequestElement request) {
		this.request = request;
	}

}
