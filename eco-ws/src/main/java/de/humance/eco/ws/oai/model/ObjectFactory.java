package de.humance.eco.ws.oai.model;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

	public ErrorDocument createErrorDocument() {
		return new ErrorDocument();
	}

	public IdentifyDocument createIdentifyDocument() {
		return new IdentifyDocument();
	}

	public GetRecordDocument createGetRecordDocument() {
		return new GetRecordDocument();
	}

	public ListMetadataFormatsDocument createListMetadataFormatsDocument() {
		return new ListMetadataFormatsDocument();
	}

	public ListIdentifiersDocument createListIdentifierDocument() {
		return new ListIdentifiersDocument();
	}

	public ListRecordsDocument createListRecordsDocument() {
		return new ListRecordsDocument();
	}

	public ListSetsDocument createListSetsDocument() {
		return new ListSetsDocument();
	}
}
