package de.humance.eco.ws.oai.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import de.humance.eco.ws.oai.model.dc.OaiDcMetadataElement;
import de.humance.eco.ws.oai.model.lom.LomMetadataElement;

@SuppressWarnings("rawtypes")
public class RecordElement<A extends ContainerElement> {
	private HeaderElement header = null;
	private OaiDcMetadataElement metadataOaiDc = null;
	private LomMetadataElement metadataLom = null;
	private List<A> about = null;

	public RecordElement() {
	}

	public RecordElement(String identifier, String status, Date datestamp,
			List<String> specs, OaiDcMetadataElement metadata, List<A> abouts) {
		this.header = new HeaderElement(identifier, status, datestamp, specs);
		this.metadataOaiDc = metadata;
		if (about != null) {
			this.about = new ArrayList<A>();
			for (A about : abouts) {
				this.about.add(about);
			}
		}
	}

	public RecordElement(String identifier, String status, Date datestamp,
			List<String> specs, LomMetadataElement metadata, List<A> abouts) {
		this.header = new HeaderElement(identifier, status, datestamp, specs);
		this.metadataLom = metadata;
		if (about != null) {
			this.about = new ArrayList<A>();
			for (A about : abouts) {
				this.about.add(about);
			}
		}
	}

	public RecordElement(String identifier, String status, Date datestamp,
			List<String> specs) {
		this.header = new HeaderElement(identifier, status, datestamp, specs);
	}

	@XmlElement
	public HeaderElement getHeader() {
		return header;
	}

	public void setHeader(HeaderElement header) {
		this.header = header;
	}

	@XmlElement(name = "metadata", required = false)
	public OaiDcMetadataElement getOaiDcMetadata() {
		return metadataOaiDc;
	}

	public void setMetadata(OaiDcMetadataElement metadata) {
		this.metadataOaiDc = metadata;
	}

	@XmlElement(name = "metadata", required = false)
	public LomMetadataElement getLomMetadata() {
		return metadataLom;
	}

	public void setMetadata(LomMetadataElement metadata) {
		this.metadataLom = metadata;
	}

	@XmlElement(required = false)
	public List<A> getAbout() {
		return about;
	}

	public void setAbout(List<A> about) {
		this.about = about;
	}
}
