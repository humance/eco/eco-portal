package de.humance.eco.ws.oai.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

import de.humance.eco.ws.util.OaiVerb;
import de.humance.eco.ws.util.WsUtil;

public class RequestElement {
	private String verb = null;
	private String resumptionToken = null;
	private String identifier = null;
	private String metadataPrefix = null;
	private String from = null;
	private String until = null;
	private String set = null;
	private String value = WsUtil.getOaiServiceUrl();

	public RequestElement() {
	}

	public RequestElement(OaiVerb verb) {
		this.verb = verb.getXmlValue();
	}

	public RequestElement(String verb) {
		this.verb = verb;
	}

	public RequestElement(OaiVerb verb, String resumptionToken) {
		this.verb = verb.getXmlValue();
		this.resumptionToken = resumptionToken;
	}

	@XmlAttribute(required = true)
	public String getVerb() {
		return verb;
	}

	public void setVerb(OaiVerb verb) {
		this.verb = verb.getXmlValue();
	}

	public void setVerb(String verb) {
		this.verb = verb;
	}

	@XmlAttribute
	public String getResumptionToken() {
		return resumptionToken;
	}

	public void setResumptionToken(String resumptionToken) {
		this.resumptionToken = resumptionToken;
	}

	@XmlAttribute
	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	@XmlAttribute
	public String getMetadataPrefix() {
		return metadataPrefix;
	}

	public void setMetadataPrefix(String metadataPrefix) {
		this.metadataPrefix = metadataPrefix;
	}

	@XmlAttribute
	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	@XmlAttribute
	public String getUntil() {
		return until;
	}

	public void setUntil(String until) {
		this.until = until;
	}

	@XmlAttribute
	public String getSet() {
		return set;
	}

	public void setSet(String set) {
		this.set = set;
	}

	@XmlValue
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
