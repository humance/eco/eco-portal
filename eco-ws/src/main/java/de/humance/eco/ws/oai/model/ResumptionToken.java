package de.humance.eco.ws.oai.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

import de.humance.eco.ws.util.WsUtil;

public class ResumptionToken {
	private Date expirationDate = null;
	private int completeListSize = 0;
	private int cursor = 0;
	private String value = null;

	public ResumptionToken() {
	}

	public ResumptionToken(Date expirationDate, int completeListSize,
			int cursor, String value) {
		this.expirationDate = expirationDate;
		this.completeListSize = completeListSize;
		this.cursor = cursor;
		this.value = value;
	}

	@XmlAttribute(required = false)
	public String getExpirationDate() {
		if (expirationDate != null) {
			return WsUtil.getDateFormatter().format(expirationDate);
		} else {
			return null;
		}
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	@XmlAttribute(required = false)
	public int getCompleteListSize() {
		return completeListSize;
	}

	public void setCompleteListSize(int completeListSize) {
		this.completeListSize = completeListSize;
	}

	@XmlAttribute(required = false)
	public int getCursor() {
		return cursor;
	}

	public void setCursor(int cursor) {
		this.cursor = cursor;
	}

	@XmlValue
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
