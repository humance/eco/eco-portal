package de.humance.eco.ws.oai.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class SetElement {
	private String spec = null;
	private String name = null;
	private List<ContainerElement<?>> descs = null;

	public SetElement() {
	}

	public SetElement(String spec, String name) {
		this.spec = spec;
		this.name = name;
	}

	@XmlElement(name = "setSpec")
	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}

	@XmlElement(name = "setName")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "setDescription", required = false)
	public List<ContainerElement<?>> getDescs() {
		return descs;
	}

	public void setDescs(List<ContainerElement<?>> descs) {
		this.descs = descs;
	}

}
