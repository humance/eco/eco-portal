package de.humance.eco.ws.oai.model.dc;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "dc")
public class DcElement {
	private List<SimpleDcElement> titles = null;
	private List<SimpleDcElement> creators = null;
	private List<SimpleDcElement> subjects = null;
	private List<SimpleDcElement> descriptions = null;
	private List<SimpleDcElement> publishers = null;
	private List<SimpleDcElement> contributors = null;
	private List<SimpleDcElement> dates = null;
	private List<SimpleDcElement> types = null;
	private List<SimpleDcElement> formats = null;
	private List<SimpleDcElement> identifiers = null;
	private List<SimpleDcElement> sources = null;
	private List<SimpleDcElement> languages = null;
	private List<SimpleDcElement> relations = null;
	private List<SimpleDcElement> coverages = null;
	private List<SimpleDcElement> rights = null;

	public DcElement() {
		this.titles = new ArrayList<SimpleDcElement>();
		this.creators = new ArrayList<SimpleDcElement>();
		this.subjects = new ArrayList<SimpleDcElement>();
		this.descriptions = new ArrayList<SimpleDcElement>();
		this.publishers = new ArrayList<SimpleDcElement>();
		this.contributors = new ArrayList<SimpleDcElement>();
		this.dates = new ArrayList<SimpleDcElement>();
		this.types = new ArrayList<SimpleDcElement>();
		this.formats = new ArrayList<SimpleDcElement>();
		this.identifiers = new ArrayList<SimpleDcElement>();
		this.sources = new ArrayList<SimpleDcElement>();
		this.languages = new ArrayList<SimpleDcElement>();
		this.relations = new ArrayList<SimpleDcElement>();
		this.coverages = new ArrayList<SimpleDcElement>();
		this.rights = new ArrayList<SimpleDcElement>();
	}

	@XmlElement(name = "title", required = false)
	public List<SimpleDcElement> getTitles() {
		return titles;
	}

	public void addTitle(String language, String value) {
		this.titles.add(new SimpleDcElement(language, value));
	}

	public void clearTitles() {
		this.titles.clear();
	}

	@XmlElement(name = "creator", required = false)
	public List<SimpleDcElement> getCreators() {
		return creators;
	}

	public void addCreator(String language, String value) {
		this.creators.add(new SimpleDcElement(language, value));
	}

	public void clearCreators() {
		this.creators.clear();
	}

	@XmlElement(name = "subject", required = false)
	public List<SimpleDcElement> getSubjects() {
		return subjects;
	}

	public void addSubject(String language, String value) {
		this.subjects.add(new SimpleDcElement(language, value));
	}

	public void clearSubjects() {
		this.subjects.clear();
	}

	@XmlElement(name = "description", required = false)
	public List<SimpleDcElement> getDescriptions() {
		return descriptions;
	}

	public void addDescription(String language, String value) {
		this.descriptions.add(new SimpleDcElement(language, value));
	}

	public void clearDescriptions() {
		this.descriptions.clear();
	}

	@XmlElement(name = "publisher", required = false)
	public List<SimpleDcElement> getPublishers() {
		return publishers;
	}

	public void addPublisher(String language, String value) {
		this.publishers.add(new SimpleDcElement(language, value));
	}

	public void clearPublishers() {
		this.publishers.clear();
	}

	@XmlElement(name = "contributor", required = false)
	public List<SimpleDcElement> getContributors() {
		return contributors;
	}

	public void addContributor(String language, String value) {
		this.contributors.add(new SimpleDcElement(language, value));
	}

	public void clearContributors() {
		this.contributors.clear();
	}

	@XmlElement(name = "date", required = false)
	public List<SimpleDcElement> getDates() {
		return dates;
	}

	public void addDate(String language, String value) {
		this.dates.add(new SimpleDcElement(language, value));
	}

	public void clearDates() {
		this.dates.clear();
	}

	@XmlElement(name = "type", required = false)
	public List<SimpleDcElement> getTypes() {
		return types;
	}

	public void addType(String language, String value) {
		this.types.add(new SimpleDcElement(language, value));
	}

	public void clearTypes() {
		this.types.clear();
	}

	@XmlElement(name = "format", required = false)
	public List<SimpleDcElement> getFormats() {
		return formats;
	}

	public void addFormat(String language, String value) {
		this.formats.add(new SimpleDcElement(language, value));
	}

	public void clearFormats() {
		this.formats.clear();
	}

	@XmlElement(name = "identifier", required = false)
	public List<SimpleDcElement> getIdentifiers() {
		return identifiers;
	}

	public void addIdentifier(String language, String value) {
		this.identifiers.add(new SimpleDcElement(language, value));
	}

	public void clearIdentifiers() {
		this.identifiers.clear();
	}

	@XmlElement(name = "source", required = false)
	public List<SimpleDcElement> getSources() {
		return sources;
	}

	public void addSource(String language, String value) {
		this.sources.add(new SimpleDcElement(language, value));
	}

	public void clearSources() {
		this.sources.clear();
	}

	@XmlElement(name = "language", required = false)
	public List<SimpleDcElement> getLanguages() {
		return languages;
	}

	public void addLanguage(String language, String value) {
		this.languages.add(new SimpleDcElement(language, value));
	}

	public void clearLanguages() {
		this.languages.clear();
	}

	@XmlElement(name = "relation", required = false)
	public List<SimpleDcElement> getRelations() {
		return relations;
	}

	public void addRelation(String language, String value) {
		this.relations.add(new SimpleDcElement(language, value));
	}

	public void clearRelations() {
		this.relations.clear();
	}

	@XmlElement(name = "coverage", required = false)
	public List<SimpleDcElement> getCoverages() {
		return coverages;
	}

	public void addCoverage(String language, String value) {
		this.coverages.add(new SimpleDcElement(language, value));
	}

	public void clearCoverages() {
		this.coverages.clear();
	}

	@XmlElement(required = false)
	public List<SimpleDcElement> getRights() {
		return rights;
	}

	public void addRight(String language, String value) {
		this.rights.add(new SimpleDcElement(language, value));
	}

	public void clearRights() {
		this.rights.clear();
	}

}
