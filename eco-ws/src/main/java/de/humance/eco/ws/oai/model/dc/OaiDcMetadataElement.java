package de.humance.eco.ws.oai.model.dc;

import javax.xml.bind.annotation.XmlElement;

import de.humance.eco.ws.oai.model.ContainerElement;

public class OaiDcMetadataElement extends ContainerElement<DcElement> {

	public OaiDcMetadataElement() {
		super();
	}

	public OaiDcMetadataElement(DcElement element) {
		super(element);
	}

	@XmlElement(name = "dc")
	public DcElement getElement() {
		return this.element;
	}

}
