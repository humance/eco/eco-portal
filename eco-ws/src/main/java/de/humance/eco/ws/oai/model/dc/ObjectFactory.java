package de.humance.eco.ws.oai.model.dc;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

	public OaiDcMetadataElement createOaiDcMetadataElement() {
		return new OaiDcMetadataElement();
	}

	public DcElement createDcElement() {
		return new DcElement();
	}

	public SimpleDcElement createSimpleDcElement() {
		return new SimpleDcElement();
	}

}
