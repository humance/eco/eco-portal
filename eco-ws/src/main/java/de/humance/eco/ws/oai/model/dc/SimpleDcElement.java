package de.humance.eco.ws.oai.model.dc;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

public class SimpleDcElement {
	private String language = null;
	private String value = null;

	public SimpleDcElement() {
	}

	public SimpleDcElement(String language, String value) {
		this.language = language;
		this.value = value;
	}

	@XmlAttribute(name = "lang", required = false)
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@XmlValue
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
