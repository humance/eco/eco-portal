@XmlSchema(
	namespace = de.humance.eco.ws.util.OaiConstants.OAI_DC_NAMESPACE,
	xmlns = {
		@XmlNs(
			prefix = de.humance.eco.ws.util.OaiConstants.OAI_DC_PREFIX, 
			namespaceURI = de.humance.eco.ws.util.OaiConstants.OAI_DC_NAMESPACE)
	}, 
	location = de.humance.eco.ws.util.OaiConstants.OAI_DC_SCHEMA,
	elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package de.humance.eco.ws.oai.model.dc;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;

