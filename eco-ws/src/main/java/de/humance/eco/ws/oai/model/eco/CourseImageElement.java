package de.humance.eco.ws.oai.model.eco;

import javax.xml.bind.annotation.XmlElement;

public class CourseImageElement {
	private String url = null;
	private String base64 = null;

	public CourseImageElement() {
	}

	public CourseImageElement(String value, boolean isUrl) {
		if (isUrl) {
			url = value;
		} else {
			base64 = value;
		}
	}

	@XmlElement(name = "courseUrl", required = false)
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@XmlElement(name = "courseImageBase64", required = false)
	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

}
