package de.humance.eco.ws.oai.model.eco;

import javax.xml.bind.annotation.XmlValue;

public class NrOfUnitsElement {
	private int value = 0;

	public NrOfUnitsElement() {
	}

	public NrOfUnitsElement(int value) {
		this.value = value;
	}

	@XmlValue
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
