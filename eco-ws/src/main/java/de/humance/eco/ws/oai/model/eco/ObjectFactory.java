package de.humance.eco.ws.oai.model.eco;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

	public NrOfUnitsElement createNrOfUnitsElement() {
		return new NrOfUnitsElement();
	}

	public CourseImageElement createCourseImageElement() {
		return new CourseImageElement();
	}

}
