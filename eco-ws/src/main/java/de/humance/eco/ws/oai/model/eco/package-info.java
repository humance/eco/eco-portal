@XmlSchema(
	namespace = de.humance.eco.ws.util.OaiConstants.ECO_NAMESPACE,
	xmlns = {
		@XmlNs(
			prefix = de.humance.eco.ws.util.OaiConstants.ECO_PREFIX, 
			namespaceURI = de.humance.eco.ws.util.OaiConstants.ECO_NAMESPACE)
	}, 
	elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package de.humance.eco.ws.oai.model.eco;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;

