package de.humance.eco.ws.oai.model.lom;

import javax.xml.bind.annotation.XmlElement;

public class ClassificationElement {
	private PurposeElement purpose = null;
	private TaxonPathElement taxonPath = null;

	@XmlElement(required = false)
	public PurposeElement getPurpose() {
		return purpose;
	}

	public void setPurpose(PurposeElement purpose) {
		this.purpose = purpose;
	}

	@XmlElement(required = false)
	public TaxonPathElement getTaxonPath() {
		return taxonPath;
	}

	public void setTaxonPath(TaxonPathElement taxonPath) {
		this.taxonPath = taxonPath;
	}

}
