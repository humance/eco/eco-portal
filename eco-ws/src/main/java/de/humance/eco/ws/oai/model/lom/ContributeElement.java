package de.humance.eco.ws.oai.model.lom;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import de.humance.eco.ws.util.WsUtil;

public class ContributeElement {
	private static final String VCARD = "<![CDATA[BEGIN:VCARD VERSION:3.0\r\nUID:urn:uuid:%d\r\nN:%s;%s\r\nFN:%s\r\nORG:%s\r\nEMAIL;TYPE=PREF,INTERNET:%s\r\nREV:%s\r\nEND:VCARD]]>";
	private static final String VCARD_NO_ORG = "<![CDATA[BEGIN:VCARD VERSION:3.0\r\nUID:urn:uuid:%d\r\nN:%s;%s\r\nFN:%s\r\nEMAIL;TYPE=PREF,INTERNET:%s\r\nREV:%s\r\nEND:VCARD]]>";
	private static final String VCARD_ORG_ONLY = "<![CDATA[BEGIN:VCARD VERSION:3.0\r\nORG:%s\r\nEND:VCARD]]>";
	private List<RoleElement> roles = null;
	private long userId = 0;
	private String firstName = null;
	private String lastName = null;
	private String email = null;
	private String orgName = null;

	public ContributeElement() {
	}

	public ContributeElement(String source, String value, long userId,
			String firstName, String lastName, String email, String orgName) {
		this.roles = new ArrayList<RoleElement>();
		this.roles.add(new RoleElement(source, value));
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.orgName = orgName;
	}

	public ContributeElement(String source, String value, String orgName) {
		this.roles = new ArrayList<RoleElement>();
		this.roles.add(new RoleElement(source, value));
		this.orgName = orgName;
	}

	@XmlElement(name = "role", required = false)
	public List<RoleElement> getRoles() {
		return roles;
	}

	public void addRole(RoleElement role) {
		this.roles.add(role);
	}

	public void removeRole(RoleElement role) {
		this.roles.remove(role);
	}

	public void clearRoles() {
		this.roles.clear();
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	@XmlElement(name = "entity")
	public String getVcard() {
		if (userId > 0) {
			if (Validator.isNull(orgName)) {
				return String.format(VCARD_NO_ORG, userId, lastName, firstName,
						firstName + StringPool.SPACE + lastName, email, WsUtil
								.getDateFormatter().format(new Date()));
			} else {
				return String.format(VCARD, userId, lastName, firstName,
						firstName + StringPool.SPACE + lastName, orgName,
						email, WsUtil.getDateFormatter().format(new Date()));
			}
		} else {
			if (Validator.isNotNull(orgName)) {
				return String.format(VCARD_ORG_ONLY, orgName);
			} else {
				return null;
			}
		}
	}
}
