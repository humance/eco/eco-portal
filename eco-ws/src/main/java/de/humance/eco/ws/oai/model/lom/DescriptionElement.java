package de.humance.eco.ws.oai.model.lom;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class DescriptionElement {
	private List<StringElement> strings = null;

	public DescriptionElement() {
		this.strings = new ArrayList<StringElement>();
	}

	@XmlElement(name = "string", required = false)
	public List<StringElement> getStrings() {
		return strings;
	}

	public void addString(StringElement string) {
		this.strings.add(string);
	}

	public void removeString(StringElement string) {
		this.strings.remove(string);
	}

	public void clearStrings() {
		this.strings.clear();
	}

}
