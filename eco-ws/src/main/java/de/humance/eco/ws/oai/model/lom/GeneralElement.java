package de.humance.eco.ws.oai.model.lom;

import javax.xml.bind.annotation.XmlElement;

import com.liferay.portal.kernel.util.Validator;

import de.humance.eco.ws.oai.model.eco.CourseImageElement;
import de.humance.eco.ws.oai.model.eco.NrOfUnitsElement;
import de.humance.eco.ws.util.OaiConstants;

public class GeneralElement {
	private IdentifierElement identifier = null;
	private TitleElement title = null;
	private DescriptionElement description = null;
	private String language = null;
	private NrOfUnitsElement nrOfUnits = null;
	private CourseImageElement courseImage = null;

	public GeneralElement() {
	}

	public GeneralElement(String catalog, long courseId, String language,
			String title, String description, int nbUnits) {
		this(catalog, courseId, language, title, description, nbUnits, null);
	}

	public GeneralElement(String catalog, long courseId, String language,
			String title, String description, int nbUnits, String base64Image) {
		this.identifier = new IdentifierElement(catalog,
				Long.toString(courseId));
		this.title = new TitleElement();
		this.title.addString(new StringElement(language, title));
		this.description = new DescriptionElement();
		this.description.addString(new StringElement(language, description));
		this.language = language;
		this.nrOfUnits = new NrOfUnitsElement(nbUnits);
		if (Validator.isNotNull(base64Image)) {
			this.courseImage = new CourseImageElement(base64Image, false);
			this.courseImage.setBase64(base64Image);
		}
	}

	@XmlElement
	public IdentifierElement getIdentifier() {
		return identifier;
	}

	public void setIdentifier(IdentifierElement identifier) {
		this.identifier = identifier;
	}

	@XmlElement
	public TitleElement getTitle() {
		return title;
	}

	public void setTitle(TitleElement title) {
		this.title = title;
	}

	@XmlElement
	public DescriptionElement getDescription() {
		return description;
	}

	public void setDescription(DescriptionElement description) {
		this.description = description;
	}

	@XmlElement
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@XmlElement(namespace = OaiConstants.ECO_NAMESPACE)
	public NrOfUnitsElement getNrOfUnits() {
		return nrOfUnits;
	}

	public void setNrOfUnits(NrOfUnitsElement nrOfUnits) {
		this.nrOfUnits = nrOfUnits;
	}

	@XmlElement(namespace = OaiConstants.ECO_NAMESPACE)
	public CourseImageElement getCourseImage() {
		return courseImage;
	}

	public void setCourseImage(CourseImageElement courseImage) {
		this.courseImage = courseImage;
	}

}
