package de.humance.eco.ws.oai.model.lom;

import javax.xml.bind.annotation.XmlElement;

public class IdentifierElement {
	private String catalog = null;
	private String entry = null;

	public IdentifierElement() {
	}

	public IdentifierElement(String catalog, String entry) {
		this.catalog = catalog;
		this.entry = entry;
	}

	@XmlElement
	public String getCatalog() {
		return catalog;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	@XmlElement
	public String getEntry() {
		return entry;
	}

	public void setEntry(String entry) {
		this.entry = entry;
	}

}
