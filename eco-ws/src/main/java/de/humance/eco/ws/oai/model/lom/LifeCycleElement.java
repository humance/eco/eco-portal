package de.humance.eco.ws.oai.model.lom;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class LifeCycleElement {
	private List<ContributeElement> contributes = null;

	public LifeCycleElement() {
		this.contributes = new ArrayList<ContributeElement>();
	}

	@XmlElement(name = "contribute", required = false)
	public List<ContributeElement> getContributes() {
		return contributes;
	}

	public void addContribute(ContributeElement contribute) {
		this.contributes.add(contribute);
	}

	public void removeContribute(ContributeElement contribute) {
		this.contributes.remove(contribute);
	}

	public void clearContributes() {
		this.contributes.clear();
	}

}
