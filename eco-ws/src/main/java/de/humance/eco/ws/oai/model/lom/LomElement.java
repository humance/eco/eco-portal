package de.humance.eco.ws.oai.model.lom;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "lom")
public class LomElement {
	private GeneralElement general = null;
	private TechnicalElement technical = null;
	private EducationalElement educational = null;
	private LifeCycleElement lifeCycle = null;
	private ClassificationElement classification = null;

	public LomElement() {
	}

	@XmlElement(required = false)
	public GeneralElement getGeneral() {
		return general;
	}

	public void setGeneral(GeneralElement general) {
		this.general = general;
	}

	@XmlElement(required = false)
	public TechnicalElement getTechnical() {
		return technical;
	}

	public void setTechnical(TechnicalElement technical) {
		this.technical = technical;
	}

	@XmlElement(required = false)
	public EducationalElement getEducational() {
		return educational;
	}

	public void setEducational(EducationalElement educational) {
		this.educational = educational;
	}

	@XmlElement(required = false)
	public LifeCycleElement getLifeCycle() {
		return lifeCycle;
	}

	public void setLifeCycle(LifeCycleElement lifeCycle) {
		this.lifeCycle = lifeCycle;
	}

	@XmlElement(required = false)
	public ClassificationElement getClassification() {
		return classification;
	}

	public void setClassification(ClassificationElement classification) {
		this.classification = classification;
	}

}
