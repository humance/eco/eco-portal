package de.humance.eco.ws.oai.model.lom;

import javax.xml.bind.annotation.XmlElement;

import de.humance.eco.ws.oai.model.ContainerElement;

public class LomMetadataElement extends ContainerElement<LomElement> {

	public LomMetadataElement() {
		super();
	}

	public LomMetadataElement(LomElement element) {
		super(element);
	}

	@XmlElement(name = "lom")
	public LomElement getElement() {
		return element;
	}

}
