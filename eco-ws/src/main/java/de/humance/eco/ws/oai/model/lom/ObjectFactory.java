package de.humance.eco.ws.oai.model.lom;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

	public LomMetadataElement createLomMetadataElement() {
		return new LomMetadataElement();
	}

	public LomElement createLomElement() {
		return new LomElement();
	}
}
