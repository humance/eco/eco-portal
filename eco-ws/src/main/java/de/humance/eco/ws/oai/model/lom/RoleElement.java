package de.humance.eco.ws.oai.model.lom;

import javax.xml.bind.annotation.XmlElement;

public class RoleElement {
	private String source = null;
	private String value = null;

	public RoleElement() {
	}

	public RoleElement(String source, String value) {
		this.source = source;
		this.value = value;
	}

	@XmlElement
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@XmlElement
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
