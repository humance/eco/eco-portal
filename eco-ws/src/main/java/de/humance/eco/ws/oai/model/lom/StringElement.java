package de.humance.eco.ws.oai.model.lom;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

public class StringElement {
	private String language = null;
	private String value = null;

	public StringElement() {
	}

	public StringElement(String language, String value) {
		this.language = language;
		this.value = value;
	}

	@XmlAttribute
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@XmlValue
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
