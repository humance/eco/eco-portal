package de.humance.eco.ws.oai.model.lom;

import javax.xml.bind.annotation.XmlElement;

public class TaxonElement {
	private String id = null;
	private EntryElement entry = null;

	@XmlElement(required = false)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@XmlElement(required = false)
	public EntryElement getEntry() {
		return entry;
	}

	public void setEntry(EntryElement entry) {
		this.entry = entry;
	}

}
