package de.humance.eco.ws.oai.model.lom;

import javax.xml.bind.annotation.XmlElement;

public class TaxonPathElement {
	private SourceElement source = null;
	private TaxonElement taxon = null;

	public TaxonPathElement() {
	}

	public TaxonPathElement(SourceElement source, TaxonElement taxon) {
		this.source = source;
		this.taxon = taxon;
	}

	@XmlElement(required = false)
	public SourceElement getSource() {
		return source;
	}

	public void setSource(SourceElement source) {
		this.source = source;
	}

	@XmlElement(required = false)
	public TaxonElement getTaxon() {
		return taxon;
	}

	public void setTaxon(TaxonElement taxon) {
		this.taxon = taxon;
	}

}
