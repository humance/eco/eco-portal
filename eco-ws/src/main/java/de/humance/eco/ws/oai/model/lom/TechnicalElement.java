package de.humance.eco.ws.oai.model.lom;

import javax.xml.bind.annotation.XmlElement;

public class TechnicalElement {
	private String location = null;

	public TechnicalElement() {
	}

	public TechnicalElement(String location) {
		this.location = location;
	}

	@XmlElement
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
