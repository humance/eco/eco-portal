@XmlSchema(
	namespace = de.humance.eco.ws.util.OaiConstants.LOM_NAMESPACE,
	xmlns = {
		@XmlNs(
			prefix = de.humance.eco.ws.util.OaiConstants.LOM_PREFIX, 
			namespaceURI = de.humance.eco.ws.util.OaiConstants.LOM_NAMESPACE)
	}, 
	location = de.humance.eco.ws.util.OaiConstants.LOM_SCHEMA,
	elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package de.humance.eco.ws.oai.model.lom;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;

