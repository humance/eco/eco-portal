@XmlSchema(
	namespace = de.humance.eco.ws.util.OaiConstants.SCHEMA_NAMESPACE, 
	xmlns = {
		@XmlNs(
			prefix = de.humance.eco.ws.util.OaiConstants.SCHEMA_PREFIX, 
			namespaceURI = de.humance.eco.ws.util.OaiConstants.SCHEMA_NAMESPACE)
	}, 
	location = de.humance.eco.ws.util.OaiConstants.SCHEMA_LOCATION,
	elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package de.humance.eco.ws.oai.model;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;

