package de.humance.eco.ws.oai.processor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

import de.humance.eco.ws.oai.exception.OaiBadArgumentException;
import de.humance.eco.ws.oai.exception.OaiBadResumptionTokenException;
import de.humance.eco.ws.oai.exception.OaiCannotDisseminateFormatException;
import de.humance.eco.ws.oai.exception.OaiIdDoesNotExistException;
import de.humance.eco.ws.oai.exception.OaiNoMetadataFormatsException;
import de.humance.eco.ws.oai.exception.OaiNoRecordsMatchException;
import de.humance.eco.ws.oai.exception.OaiNoSetHierarchyException;
import de.humance.eco.ws.oai.model.ContainerElement;
import de.humance.eco.ws.oai.model.HeaderElement;
import de.humance.eco.ws.oai.model.IdentifyDocument;
import de.humance.eco.ws.oai.model.ListIdentifiersDocument;
import de.humance.eco.ws.oai.model.ListMetadataFormatsDocument;
import de.humance.eco.ws.oai.model.ListRecordsDocument;
import de.humance.eco.ws.oai.model.OaipmhDocument;
import de.humance.eco.ws.oai.model.RecordElement;
import de.humance.eco.ws.oai.model.ResumptionToken;
import de.humance.eco.ws.oai.model.dc.OaiDcMetadataElement;
import de.humance.eco.ws.oai.model.lom.LomMetadataElement;
import de.humance.eco.ws.util.DeleteRecord;
import de.humance.eco.ws.util.OaiConstants;
import de.humance.eco.ws.util.OaiVerb;
import de.humance.eco.ws.util.WsUtil;
import de.humance.education.model.Course;
import de.humance.education.service.CourseLocalServiceUtil;
import de.humance.education.util.CourseStatus;

public class OaiProcessor {
	private static OaiProcessor instance = null;
	private static Log log = LogFactoryUtil.getLog(OaiProcessor.class);
	private Map<String, ResumptionToken> resumptionTokens = null;
	private Map<String, List<?>> resumptionResults = null;
	private int tokenTimeToLive = -1;
	private int tokenListSize = 25;

	private OaiProcessor() {
		resumptionTokens = new HashMap<String, ResumptionToken>();
		resumptionResults = new HashMap<String, List<?>>();
	}

	public static OaiProcessor getInstance() {
		if (instance == null) {
			instance = new OaiProcessor();
		}

		return instance;
	}

	public int getTokenTimeToLive() {
		return tokenTimeToLive;
	}

	public int getTokenListSize() {
		return tokenListSize;
	}

	public void checkIdentifyArgs(String... args)
			throws OaiBadArgumentException {
		if (args != null) {
			for (String arg : args) {
				if (Validator.isNotNull(arg)) {
					throw new OaiBadArgumentException(
							OaiVerb.IDENTIFY.getXmlValue());
				}
			}
		}
	}

	public OaipmhDocument processIdentify() {
		return new IdentifyDocument(OaiConstants.REPOSITORY_NAME,
				WsUtil.getOaiServiceUrl(), OaiConstants.PROTOCOL_VERSION,
				"developer@humance.de", OaiConstants.EARLIEST_DATESTAMP,
				DeleteRecord.PERSISTENT, OaiConstants.GRANULARITY);
	}

	public void checkListIdentifiersArgs(String resumptionToken,
			String metadataPrefix, String from, String until, String set)
			throws OaiBadArgumentException, OaiBadResumptionTokenException,
			OaiCannotDisseminateFormatException, OaiNoSetHierarchyException {
		// Check for expired resumption tokens
		purgeExpiredTokens();
		updateParameters();

		if (Validator.isNotNull(resumptionToken)) {
			// CASE : There is a resumption token
			// Check if there are any other args
			if (Validator.isNotNull(from) || Validator.isNotNull(until)
					|| Validator.isNotNull(metadataPrefix)
					|| Validator.isNotNull(set)) {
				// CASE : There are other args, return bad argument error
				throw new OaiBadArgumentException(
						OaiVerb.LIST_IDENTIFIERS.getXmlValue());
			}

			// Check the token
			List<String> extractedToken = extractResumptionToken(resumptionToken);
			String tokenId = extractedToken.get(0).trim();
			if (extractedToken.isEmpty()
					|| !resumptionTokens.containsKey(tokenId)) {
				// CASE : Token is expired or invalid
				throw new OaiBadResumptionTokenException(
						OaiVerb.LIST_IDENTIFIERS.getXmlValue());
			}

		} else {
			// CASE : No resumption token
			if (Validator.isNull(metadataPrefix)) {
				// CASE : No specified metadata prefix
				throw new OaiBadArgumentException(
						OaiVerb.LIST_IDENTIFIERS.getXmlValue());
			}

			if (!metadataPrefix.equalsIgnoreCase(OaiConstants.OAI_DC_PREFIX)
					&& metadataPrefix.equalsIgnoreCase(OaiConstants.LOM_PREFIX)) {
				// CASE : Specified metadata prefix is not supported
				throw new OaiCannotDisseminateFormatException(
						OaiVerb.LIST_IDENTIFIERS.getXmlValue());
			}

			if (Validator.isNotNull(set)) {
				// CASE : Set is specified but is not supported
				throw new OaiNoSetHierarchyException(
						OaiVerb.LIST_IDENTIFIERS.getXmlValue());
			}
		}
	}

	@SuppressWarnings("unchecked")
	public OaipmhDocument processListIdentifiers(String resumptionToken,
			String metadataPrefix, String from, String until, String set)
			throws OaiNoRecordsMatchException, OaiBadArgumentException {
		List<HeaderElement> headers = null;
		String newGeneratedToken = null;
		ResumptionToken associatedToken = null;
		int completeListSize = 0;
		int cursor = 0;
		if (Validator.isNull(resumptionToken)) {
			// CASE : No resumption token
			List<Course> availableCourses = null;
			try {
				availableCourses = CourseLocalServiceUtil.findByOrgId(0L);
			} catch (Exception e) {
				log.error("Could not get all available Courses of organization 'Universal'", e);
			}
			if (availableCourses == null || availableCourses.isEmpty()) {
				// CASE : No courses available
				throw new OaiNoRecordsMatchException(
						OaiVerb.LIST_IDENTIFIERS.getXmlValue());
			}

			// Prepare the headers
			Date fromDate = null;
			if (Validator.isNotNull(from)) {
				try {
					fromDate = WsUtil.getDateFormatter().parse(from);
				} catch (Exception e) {
					throw new OaiBadArgumentException(
							OaiVerb.LIST_IDENTIFIERS.getXmlValue());
				}
			}
			Date untilDate = null;
			if (Validator.isNotNull(until)) {
				try {
					untilDate = WsUtil.getDateFormatter().parse(until);
				} catch (Exception e) {
					throw new OaiBadArgumentException(
							OaiVerb.LIST_IDENTIFIERS.getXmlValue());
				}
			}
			headers = new ArrayList<HeaderElement>();
			for (Course availableCourse : availableCourses) {
				final String recordStatus;
				
				switch (CourseStatus.valueOf(availableCourse.getStatus())) {
				case PUBLISHED:
				case ARCHIVED:
					// CASE : Process the course normally
					recordStatus = null;
					break;
				case DELETED:
					// CASE : Process the course as deleted
					recordStatus = OaiConstants.STATUS_DELETED;
					break;
				default:
					continue;
				}

				if (fromDate != null && availableCourse.getModifiedDate().before(fromDate)) {
					// CASE : course is before the specified date range
					continue;
				}

				if (untilDate != null && availableCourse.getModifiedDate().after(untilDate)) {
					// CASE : course is after the specified date range
					continue;
				}
				
				headers.add(new HeaderElement(WsUtil.getCourseUniversalId(availableCourse), recordStatus,
						availableCourse.getModifiedDate(), null));
			}

			newGeneratedToken = generateResumptionToken(metadataPrefix);
			completeListSize = headers.size();
		} else {
			// CASE : There is a ResumptionToken
			final String tokenId = extractResumptionToken(resumptionToken).get(0);
			headers = (List<HeaderElement>) resumptionResults.get(tokenId);
			associatedToken = resumptionTokens.get(tokenId);
			completeListSize = associatedToken.getCompleteListSize();
			cursor = associatedToken.getCursor();
		}

		int currentPosition = 0;
		int maxCursor = (cursor + tokenListSize > completeListSize ? completeListSize
				: cursor + tokenListSize);
		ListIdentifiersDocument document = new ListIdentifiersDocument();
		for (currentPosition = cursor; currentPosition < maxCursor; currentPosition++) {
			document.getListIdentifiers().addHeader(headers.get(currentPosition));
		}

		// Check if a resumption token should be saved
		if (maxCursor < completeListSize) {
			// CASE : Must prepare/update a resumption token
			ResumptionToken updatedToken = prepareResumptionToken(
					(resumptionToken != null ? resumptionToken
							: newGeneratedToken), completeListSize,
					currentPosition, headers);
			document.setResumptionToken(updatedToken);
		} else {
			// CASE : Make sure to remove the resumption token from the list
			purgeResumptionToken((resumptionToken != null ? resumptionToken : newGeneratedToken));
		}

		// Update some document info
		document.getRequest().setMetadataPrefix(metadataPrefix);
		document.getRequest().setFrom(from);
		document.getRequest().setUntil(until);
		document.getRequest().setResumptionToken(resumptionToken);

		return document;
	}

	public void checkListRecordsArgs(String resumptionToken,
			String metadataPrefix, String from, String until, String set)
			throws OaiBadArgumentException, OaiBadResumptionTokenException,
			OaiCannotDisseminateFormatException, OaiNoSetHierarchyException {
		// Check for expired resumption tokens
		purgeExpiredTokens();
		updateParameters();

		if (Validator.isNotNull(resumptionToken)) {
			// CASE : There is a resumption token
			// Check if there are any other args
			if (Validator.isNotNull(from) || Validator.isNotNull(until)
					|| Validator.isNotNull(metadataPrefix)
					|| Validator.isNotNull(set)) {
				// CASE : There are other args, return bad argument error
				throw new OaiBadArgumentException(
						OaiVerb.LIST_RECORDS.getXmlValue());
			}

			// Check the token
			List<String> extractedToken = extractResumptionToken(resumptionToken);
			if (extractedToken.isEmpty()
					|| !resumptionResults.containsKey(extractedToken.get(0))) {
				// CASE : Token is expired or invalid
				throw new OaiBadResumptionTokenException(
						OaiVerb.LIST_RECORDS.getXmlValue());
			}

		} else {
			// CASE : No resumption token
			if (Validator.isNull(metadataPrefix)) {
				// CASE : No specified metadata prefix
				throw new OaiBadArgumentException(
						OaiVerb.LIST_RECORDS.getXmlValue());
			}

			if (!metadataPrefix.equalsIgnoreCase(OaiConstants.OAI_DC_PREFIX)
					&& !metadataPrefix
							.equalsIgnoreCase(OaiConstants.LOM_PREFIX)) {
				// CASE : Specified metadata prefix is not supported
				throw new OaiCannotDisseminateFormatException(
						OaiVerb.LIST_RECORDS.getXmlValue());
			}

			if (Validator.isNotNull(set)) {
				// CASE : Set is specified but is not supported
				throw new OaiNoSetHierarchyException(
						OaiVerb.LIST_RECORDS.getXmlValue());
			}
		}
	}

	@SuppressWarnings("unchecked")
	public OaipmhDocument processListRecords(String resumptionToken,
			String metadataPrefix, String from, String until, String set)
			throws OaiNoRecordsMatchException, OaiBadArgumentException {
		List<RecordElement<?>> records = null;
		String newGeneratedToken = null;
		ResumptionToken associatedToken = null;
		int completeListSize = 0;
		int cursor = 0;
		if (Validator.isNull(resumptionToken)) {
			// CASE : No resumption token
			List<Course> availableCourses = null;
			try {
				availableCourses = CourseLocalServiceUtil.findByOrgId(0L);
			} catch (Exception e) {
				log.error("Could not get all available Courses", e);
			}
			if (availableCourses == null || availableCourses.isEmpty()) {
				// CASE : No courses available
				throw new OaiNoRecordsMatchException(
						OaiVerb.LIST_RECORDS.getXmlValue());
			}

			// Prepare the headers
			Date fromDate = null;
			if (Validator.isNotNull(from)) {
				try {
					fromDate = WsUtil.getDateFormatter().parse(from);
				} catch (Exception e) {
					throw new OaiBadArgumentException(
							OaiVerb.LIST_RECORDS.getXmlValue());
				}
			}
			Date untilDate = null;
			if (Validator.isNotNull(until)) {
				try {
					untilDate = WsUtil.getDateFormatter().parse(until);
				} catch (Exception e) {
					throw new OaiBadArgumentException(
							OaiVerb.LIST_RECORDS.getXmlValue());
				}
			}
			records = new ArrayList<RecordElement<?>>();
			List<String> processedIdentifications = new ArrayList<String>();
			for (Course availableCourse : availableCourses) {
				final String recordStatus;
				
				switch (CourseStatus.valueOf(availableCourse.getStatus())) {
				case PUBLISHED:
				case ARCHIVED:
					// CASE : Process the course normally
					if (processedIdentifications.contains(availableCourse
							.getIdentification())) {
						// CASE : Course with same id was already declared as not
						// deleted, set all previous versions as deleted
						recordStatus = OaiConstants.STATUS_DELETED;
					} else {
						recordStatus = null;
					}
					break;
				case DELETED:
					// CASE : Course is marked as deleted
					recordStatus = OaiConstants.STATUS_DELETED;
					break;
				default:
					continue;
				}

				if (fromDate != null
						&& availableCourse.getModifiedDate().before(fromDate)) {
					// CASE : course is before the specified date range
					continue;
				}

				if (untilDate != null
						&& availableCourse.getModifiedDate().after(untilDate)) {
					// CASE : course is after the specified date range
					continue;
				}

				List<String> specs = null;
				try {
					User author = UserLocalServiceUtil.getUser(availableCourse
							.getChangeUserId());
					long[] orgIds = author.getOrganizationIds();
					if (ArrayUtil.isNotEmpty(orgIds)) {
						Organization org = OrganizationLocalServiceUtil
								.getOrganization(orgIds[0]);
						if (org != null
								&& org.getName().toLowerCase()
										.contains("Eichler".toLowerCase())) {
							specs = new ArrayList<String>();
							specs.add("EcoBase");
						}
					}
				} catch (Exception e) {
					log.warn("Could not determine if Course with id "
							+ availableCourse.getCourseId()
							+ " is an ECO related Course", e);
				}

				if (metadataPrefix.equalsIgnoreCase(OaiConstants.OAI_DC_PREFIX)) {
					// CASE : Specified metadata prefix is OAI DC
					OaiDcMetadataElement metadataWrapper = null;
					if (Validator.isNull(recordStatus)) {
						metadataWrapper = WsUtil.courseToOaiDc(availableCourse);
						processedIdentifications.add(availableCourse
								.getIdentification());
					}

					records.add(new RecordElement<ContainerElement<Object>>(
							WsUtil.getCourseUniversalId(availableCourse),
							recordStatus, availableCourse.getModifiedDate(),
							specs, metadataWrapper, null));

				} else if (metadataPrefix.equalsIgnoreCase(OaiConstants.LOM_PREFIX)) {
					// CASE : Specified metadata prefix is LOM
					LomMetadataElement metadataWrapper = null;
					if (Validator.isNull(recordStatus)) {
						metadataWrapper = WsUtil.courseToLom(availableCourse);
						processedIdentifications.add(availableCourse
								.getIdentification());
					}

					records.add(new RecordElement<ContainerElement<Object>>(
							WsUtil.getCourseUniversalId(availableCourse),
							recordStatus, availableCourse.getModifiedDate(),
							specs, metadataWrapper, null));
				}
			}

			newGeneratedToken = generateResumptionToken(metadataPrefix);
			completeListSize = records.size();

		} else {
			// CASE : There is a ResumptionToken
			String tokenId = extractResumptionToken(resumptionToken).get(0);
			records = (List<RecordElement<?>>) resumptionResults.get(tokenId);
			associatedToken = resumptionTokens.get(tokenId);
			completeListSize = associatedToken.getCompleteListSize();
			cursor = associatedToken.getCursor();
		}

		int currentPosition = 0;
		int maxCursor = (cursor + tokenListSize > completeListSize ? completeListSize
				: cursor + tokenListSize);
		ListRecordsDocument document = new ListRecordsDocument();
		for (currentPosition = cursor; currentPosition < maxCursor; currentPosition++) {
			document.getListRecords().addRecord(records.get(currentPosition));
		}

		// Check if a resumption token should be saved
		if (maxCursor < completeListSize) {
			// CASE : Must prepare/update a resumption token
			ResumptionToken updatedToken = prepareResumptionToken(
					(resumptionToken != null ? resumptionToken
							: newGeneratedToken), completeListSize,
					currentPosition, records);
			document.setResumptionToken(updatedToken);

		} else {
			// CASE : Make sure to remove the resumption token from the list
			purgeResumptionToken((resumptionToken != null ? resumptionToken
					: newGeneratedToken));
		}

		// Update some document info
		document.getRequest().setMetadataPrefix(metadataPrefix);
		document.getRequest().setFrom(from);
		document.getRequest().setUntil(until);
		document.getRequest().setResumptionToken(resumptionToken);

		return document;
	}

	public void checkGetRecordArgs(String identifier, String metadataPrefix)
			throws OaiBadArgumentException {
		if (Validator.isNull(identifier) || Validator.isNull(metadataPrefix)) {
			// CASE : If one of the args is null
			throw new OaiBadArgumentException(OaiVerb.GET_RECORD.getXmlValue());
		}
	}

	public OaipmhDocument processGetRecord(String identifier,
			String metadataPrefix) throws OaiCannotDisseminateFormatException,
			OaiIdDoesNotExistException {
		// TODO
		throw new OaiIdDoesNotExistException(OaiVerb.GET_RECORD.getXmlValue());
	}

	public void checkListMetadataFormatsArgs(String identifier)
			throws OaiBadArgumentException {
		// Nothing to do
	}

	public OaipmhDocument processListMetadataFormats(String identifier)
			throws OaiIdDoesNotExistException, OaiNoMetadataFormatsException {
		if (Validator.isNotNull(identifier)) {
			// CASE : Identifier is specified, check if the item exists
			List<Course> courses = null;
			try {
				courses = CourseLocalServiceUtil
						.findByIdentification(identifier);
			} catch (Exception e) {
				// Do nothing
			}
			if (courses == null || courses.isEmpty()) {
				throw new OaiIdDoesNotExistException(
						OaiVerb.LIST_METADATA_FORMATS.getXmlValue());
			}
		}

		return new ListMetadataFormatsDocument(OaiConstants.FORMAT_OAI_DC,
				OaiConstants.FORMAT_LOM);
	}

	public void checkListSetsArgs(String resumptionToken)
			throws OaiBadArgumentException, OaiBadResumptionTokenException {
		// Check for expired resumption tokens
		purgeExpiredTokens();
		updateParameters();

		if (Validator.isNull(resumptionToken)) {
			return;
		}

		// CASE : There is a resumption token
		List<String> extractedToken = extractResumptionToken(resumptionToken);
		if (extractedToken.isEmpty()
				|| !resumptionTokens.containsKey(extractedToken.get(0))) {
			// CASE : Token is expired or invalid
			throw new OaiBadResumptionTokenException(
					OaiVerb.LIST_SETS.getXmlValue());
		}
	}

	public OaipmhDocument processListSets(String resumptionToken)
			throws OaiNoSetHierarchyException {
		// Not supported
		throw new OaiNoSetHierarchyException(OaiVerb.LIST_SETS.getXmlValue());
	}

	private void updateParameters() {
		try {
			tokenTimeToLive = WsUtil.getOaiTokenTimeToLive();
		} catch (Exception e) {
			// Do nothing
		}
		try {
			tokenListSize = WsUtil.getOaiTokenListSize();
		} catch (Exception e) {
			// Do nothing
		}
	}

	private synchronized String generateResumptionTokenId() {
		Date now = new Date();
		return Long.toString(now.getTime());
	}

	private String generateResumptionToken(String metadataPrefix) {
		String tokenId = generateResumptionTokenId();
		return tokenId + StringPool.COLON + metadataPrefix;
	}

	private List<String> extractResumptionToken(String resumptionToken) {
		List<String> extractedData = new ArrayList<String>();
		StringTokenizer tokenizer = new StringTokenizer(resumptionToken,
				StringPool.COLON);
		while (tokenizer.hasMoreTokens()) {
			extractedData.add(tokenizer.nextToken());
		}
		return extractedData;
	}

	private void purgeExpiredTokens() {
		if (tokenTimeToLive <= 0) {
			// No expiration
			return;
		}

		Date now = new Date();
		Iterator<String> tokenIterator = resumptionTokens.keySet().iterator();
		while (tokenIterator.hasNext()) {
			String token = tokenIterator.next();
			Date then = new Date(Long.parseLong(token) + tokenTimeToLive);
			if (now.after(then)) {
				tokenIterator.remove();
				resumptionResults.remove(token);
			}
		}
	}

	private void purgeResumptionToken(String token) {
		String tokenId = extractResumptionToken(token).get(0);
		resumptionTokens.remove(tokenId);
		resumptionResults.remove(tokenId);
	}

	private <T> ResumptionToken prepareResumptionToken(String token,
			int completeListSize, int cursor, List<T> results) {
		ResumptionToken resumptionToken = null;
		String tokenId = extractResumptionToken(token).get(0);
		if (resumptionTokens.containsKey(tokenId)) {
			resumptionToken = resumptionTokens.get(tokenId);
		} else {
			resumptionToken = new ResumptionToken();
			resumptionToken.setValue(token);
		}

		// Set the expiration date if there is one
		if (tokenTimeToLive > 0) {
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			cal.add(Calendar.MILLISECOND, tokenTimeToLive);
			resumptionToken.setExpirationDate(cal.getTime());
		}
		resumptionToken.setCompleteListSize(completeListSize);
		resumptionToken.setCursor(cursor);
		resumptionTokens.put(tokenId, resumptionToken);

		// Update the results but should not be necessary
		resumptionResults.put(tokenId, results);

		return resumptionToken;
	}

}
