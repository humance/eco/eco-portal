package de.humance.eco.ws.rest.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.liferay.portal.NoSuchUserException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;

import de.humance.eco.util.OpenIdConnectUtil;
import de.humance.eco.util.UserUtil;
import de.humance.eco.ws.rest.model.CourseAssignment;
import de.humance.eco.ws.rest.model.Description;
import de.humance.eco.ws.rest.model.Heartbeat;
import de.humance.eco.ws.rest.model.Teacher;
import de.humance.eco.ws.util.WsUtil;
import de.humance.education.model.Course;
import de.humance.education.model.CourseProgress;
import de.humance.education.model.LearningProfile;
import de.humance.education.service.CourseLocalServiceUtil;
import de.humance.education.service.CourseProgressLocalServiceUtil;
import de.humance.education.service.LearningProfileLocalServiceUtil;
import de.humance.education.util.CourseType;

@RequestMapping(value = "/rest", produces = "application/json")
@Controller
public class RestController {
	private Log log = LogFactoryUtil.getLog(getClass());

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/users/{userId}/courses", method = RequestMethod.GET)
	@ResponseBody
	public List<CourseAssignment> getUserCourses(@PathVariable String userId)
			throws NoSuchUserException, Exception {
		long companyId = PortalUtil.getDefaultCompanyId();
		String issuerId = OpenIdConnectUtil.getIssuerId(companyId);
		User learner = UserUtil.getUserByOpenId(companyId, issuerId, userId);
		if (learner == null) {
			// Return empty list according to spec and ticket ECO-18
			return Collections.EMPTY_LIST;
		}

		LearningProfile profile = LearningProfileLocalServiceUtil
				.findByUserId(learner.getUserId());
		if (profile == null) {
			// CASE : If there is no learning profile associated, return an
			// empty list (aka no course associated)
			return Collections.emptyList();
		}

		List<Course> associatedCourses = null;
		try {
			associatedCourses = CourseLocalServiceUtil.getAllAvailableCourses(
					CourseType.COURSE, profile.getLearningProfileId());
		} catch (Exception e) {
			// TODO: handle exception
		}
		if (associatedCourses == null || associatedCourses.isEmpty()) {
			return Collections.emptyList();
		}

		List<CourseAssignment> assignments = new ArrayList<CourseAssignment>();
		for (Course associatedCourse : associatedCourses) {
			CourseProgress correspondingProgress = null;
			try {
				correspondingProgress = CourseProgressLocalServiceUtil
						.findByProfileId_CourseId(
								profile.getLearningProfileId(),
								associatedCourse.getCourseId());
			} catch (Exception e) {
				// Do nothing, consider as there is no associated progress
			}

			CourseAssignment assignment = new CourseAssignment();
			assignment.setId(associatedCourse.getCourseId());
			if (correspondingProgress == null) {
				// CASE : No progress found, use default values
				assignment.setProgressPercentage(0.0);
				assignment.setSpentTime(0);
				assignment.setViewCount(0);
				assignment.setFirstViewDate(StringPool.BLANK);
				assignment.setLastViewDate(StringPool.BLANK);
				assignment.setCompletedDate(StringPool.BLANK);

			} else {
				assignment.setProgressPercentage(correspondingProgress
						.getProgressValue());
				assignment.setSpentTime(correspondingProgress.getSpentTime());
				assignment.setViewCount(correspondingProgress.getViewCount());
				assignment.setFirstViewDate(correspondingProgress
						.getFirstViewDate());
				assignment.setLastViewDate(correspondingProgress
						.getLastViewDate());
				assignment.setCompletedDate(correspondingProgress
						.getCompletedDate());
			}

			assignments.add(assignment);
		}

		return assignments;
	}

	@RequestMapping(value = "/teachers/{userId}", method = RequestMethod.GET)
	@ResponseBody
	public Teacher getTeacher(@PathVariable String userId)
			throws NoSuchUserException, Exception {
		long companyId = PortalUtil.getDefaultCompanyId();
		String issuerId = OpenIdConnectUtil.getIssuerId(companyId);
		User teacher = UserUtil.getUserByOpenId(companyId, issuerId, userId);
		if (teacher == null) {
			throw new NoSuchUserException("No User exists with id " + userId
					+ " (issuerID=" + issuerId + ")");
		}

		Description desc = new Description(teacher.getLocale().getLanguage(),
				teacher.getComments());
		return new Teacher(userId, teacher.getFullName(),
				WsUtil.getUserPortraitUrl(teacher.getPortraitId()),
				Arrays.asList(desc));
	}

	@RequestMapping(value = "/heartbeat", method = RequestMethod.GET)
	@ResponseBody
	public Heartbeat getHeartbeat() {
		return new Heartbeat(new Date());
	}

	@ExceptionHandler(NoSuchUserException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "No User found with the specified ID")
	public void noUserFound(NoSuchUserException e) {
		log.error("REST Exception : " + e.getMessage());
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Internal Server Error")
	public void serviceFailed(Exception e) {
		log.error("REST Exception : " + e.getMessage(), e);
	}

}
