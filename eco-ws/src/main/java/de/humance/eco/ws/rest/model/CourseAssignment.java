package de.humance.eco.ws.rest.model;

import java.util.Date;

import com.liferay.portal.kernel.util.StringPool;

import de.humance.eco.ws.util.WsUtil;

public class CourseAssignment {
	private long id = 0;
	private int progressPercentage = 0;
	private long spentTime = 0;
	private int viewCount = 0;
	private String firstViewDate = StringPool.BLANK;
	private String lastViewDate = StringPool.BLANK;
	private String completedDate = StringPool.BLANK;

	public CourseAssignment() {
	}

	public CourseAssignment(long courseId, double progressPercentage,
			long spentTime, int viewCount, Date firstViewDate,
			Date lastViewDate, Date completedDate) {
		this.id = courseId;
		setProgressPercentage(progressPercentage);
		setSpentTime(spentTime);
		this.viewCount = viewCount;
		if (firstViewDate != null) {
			this.firstViewDate = WsUtil.getDateFormatter()
					.format(firstViewDate);
		}
		if (lastViewDate != null) {
			this.lastViewDate = WsUtil.getDateFormatter().format(lastViewDate);
		}
		if (completedDate != null) {
			this.completedDate = WsUtil.getDateFormatter()
					.format(completedDate);
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getProgressPercentage() {
		return progressPercentage;
	}

	public void setProgressPercentage(double progress) {
		// transform from double value to rounded integer value
		progress = progress * 100;
		Long roundedValue = Math.round(progress);
		this.progressPercentage = Integer.valueOf(roundedValue.intValue());
	}

	public long getSpentTime() {
		return spentTime;
	}

	public void setSpentTime(long spentTime) {
		// transform from second to milli seconds
		this.spentTime = spentTime * 1000;
	}

	public int getViewCount() {
		return viewCount;
	}

	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}

	public String getFirstViewDate() {
		return firstViewDate;
	}

	public void setFirstViewDate(String firstViewDate) {
		this.firstViewDate = firstViewDate;
	}

	public void setFirstViewDate(Date firstViewDate) {
		if (firstViewDate != null) {
			this.firstViewDate = WsUtil.getDateFormatter()
					.format(firstViewDate);
		}
	}

	public String getLastViewDate() {
		return lastViewDate;
	}

	public void setLastViewDate(String lastViewDate) {
		this.lastViewDate = lastViewDate;
	}

	public void setLastViewDate(Date lastViewDate) {
		if (lastViewDate != null) {
			this.lastViewDate = WsUtil.getDateFormatter().format(lastViewDate);
		}
	}

	public String getCompletedDate() {
		return completedDate;
	}

	public void setCompletedDate(String completedDate) {
		this.completedDate = completedDate;
	}

	public void setCompletedDate(Date completedDate) {
		if (completedDate != null) {
			this.completedDate = WsUtil.getDateFormatter()
					.format(completedDate);
		}
	}

}
