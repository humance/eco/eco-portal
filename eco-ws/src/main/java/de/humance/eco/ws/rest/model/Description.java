package de.humance.eco.ws.rest.model;

public class Description {
	private String language = null;
	private String label = null;

	public Description() {
	}

	public Description(String language, String label) {
		this.language = language;
		this.label = label;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
