package de.humance.eco.ws.rest.model;

import java.util.Date;

import de.humance.eco.ws.util.WsUtil;

public class Heartbeat {
	private String alive_at = null;

	public Heartbeat() {
	}

	public Heartbeat(Date date) {
		alive_at = WsUtil.getDateFormatter().format(date);
	}

	public String getAlive_at() {
		return alive_at;
	}

	public void setAlive_at(String alive_at) {
		this.alive_at = alive_at;
	}

	public void setAlive_at(Date date) {
		this.alive_at = WsUtil.getDateFormatter().format(date);
	}

}
