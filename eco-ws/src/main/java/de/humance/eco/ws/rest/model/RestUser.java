package de.humance.eco.ws.rest.model;

public class RestUser {
	protected String id = null;
	protected String name = null;
	protected String imageUrl = null;

	public RestUser() {
	}

	public RestUser(String id, String name, String image) {
		this.id = id;
		this.name = name;
		this.imageUrl = image;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String image) {
		this.imageUrl = image;
	}

}
