package de.humance.eco.ws.rest.model;

import java.util.ArrayList;
import java.util.List;

public class Teacher extends RestUser {
	private List<Description> desc = new ArrayList<Description>();

	public Teacher() {
	}

	public Teacher(String id, String name, String imageUrl, List<Description> desc) {
		this.id = id;
		this.name = name;
		this.imageUrl = imageUrl;
		if (desc != null) {
			this.desc.addAll(desc);
		}
	}

	public List<Description> getDesc() {
		return desc;
	}

	public void addDescription(Description desc) {
		this.desc.add(desc);
	}

	public void addDescriptions(List<Description> descs) {
		this.desc.addAll(desc);
	}

	public void removeDescription(Description desc) {
		this.desc.remove(desc);
	}

	public void clearDescriptions() {
		this.desc.clear();
	}

}
