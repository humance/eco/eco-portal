package de.humance.eco.ws.util;

public enum DeleteRecord {
	NO("no"), TRANSIENT("transient"), PERSISTENT("persistent");

	private String xmlValue = null;

	private DeleteRecord(String xmlValue) {
		this.xmlValue = xmlValue;
	}

	public String getXmlValue() {
		return xmlValue;
	}
}
