package de.humance.eco.ws.util;

import java.util.Date;

import de.humance.eco.ws.oai.model.MetadataFormatElement;

public class OaiConstants {
	// XML document related
	public static final String SCHEMA_PREFIX = "oai";
	public static final String SCHEMA_NAMESPACE = "http://www.openarchives.org/OAI/2.0/";
	public static final String SCHEMA_LOCATION = "http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd";
	public static final String DOCUMENT_ROOT_ELEMENT = "OAI-PMH";
	public static final String COURSE_ID_PREFIX = "oai";

	// Identify verb related
	public static final String REPOSITORY_NAME = "Logi Assist";
	public static final String PROTOCOL_VERSION = "2.0";
	public static final Date EARLIEST_DATESTAMP = new Date();
	public static final String GRANULARITY = "YYYY-MM-DDThh:mm:ssZ";

	// List Metadata Formats related
	public static final String OAI_DC_PREFIX = "oai_dc";
	public static final String OAI_DC_NAMESPACE = "http://www.openarchives.org/OAI/2.0/oai_dc/";
	public static final String OAI_DC_SCHEMA = "http://www.openarchives.org/OAI/2.0/oai_dc.xsd";
	public static final String LOM_PREFIX = "oai_lom";
	public static final String LOM_NAMESPACE = "http://ltsc.ieee.org/xsd/LOM";
	public static final String LOM_SCHEMA = "http://standards.ieee.org/downloads/LOM/lomv1.0/xsd/lom.xsd";
	public static final String ECO_PREFIX = "eco";
	public static final String ECO_NAMESPACE = "http://www.ecolearning.eu/xsd/LOM";
	public static final MetadataFormatElement FORMAT_OAI_DC = new MetadataFormatElement(
			OAI_DC_PREFIX, OAI_DC_SCHEMA, OAI_DC_NAMESPACE);
	public static final MetadataFormatElement FORMAT_LOM = new MetadataFormatElement(
			LOM_PREFIX, LOM_SCHEMA, LOM_NAMESPACE);

	// List Records related
	public static final String STATUS_DELETED = "deleted";
}
