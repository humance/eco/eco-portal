package de.humance.eco.ws.util;

public interface OaiPropsKeys {
	public static final String TOKEN_TIME_TO_LIVE = "oai.token.time.to.live";
	public static final String TOKEN_LIST_SIZE = "oai.token.list.size";
}
