package de.humance.eco.ws.util;

public enum OaiVerb {
	GET_RECORD("GetRecord"), 
	IDENTIFY("Identify"), 
	LIST_IDENTIFIERS("ListIdentifiers"), 
	LIST_METADATA_FORMATS("ListMetadataFormats"), 
	LIST_RECORDS("ListRecords"), 
	LIST_SETS("ListSets");

	private String xmlValue = null;

	private OaiVerb(String xmlValue) {
		this.xmlValue = xmlValue;
	}

	public String getXmlValue() {
		return xmlValue;
	}
}
