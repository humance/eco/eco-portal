package de.humance.eco.ws.util;

import java.util.List;
import java.util.Locale;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.util.ISO8601DateFormat;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.PrincipalThreadLocal;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.security.permission.PermissionThreadLocal;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;

import de.humance.eco.util.LayoutConstants;
import de.humance.eco.ws.oai.model.dc.DcElement;
import de.humance.eco.ws.oai.model.dc.OaiDcMetadataElement;
import de.humance.eco.ws.oai.model.lom.ClassificationElement;
import de.humance.eco.ws.oai.model.lom.ContributeElement;
import de.humance.eco.ws.oai.model.lom.EntryElement;
import de.humance.eco.ws.oai.model.lom.GeneralElement;
import de.humance.eco.ws.oai.model.lom.LifeCycleElement;
import de.humance.eco.ws.oai.model.lom.LomElement;
import de.humance.eco.ws.oai.model.lom.LomMetadataElement;
import de.humance.eco.ws.oai.model.lom.PurposeElement;
import de.humance.eco.ws.oai.model.lom.SourceElement;
import de.humance.eco.ws.oai.model.lom.StringElement;
import de.humance.eco.ws.oai.model.lom.TaxonElement;
import de.humance.eco.ws.oai.model.lom.TaxonPathElement;
import de.humance.eco.ws.oai.model.lom.TechnicalElement;
import de.humance.education.NoSuchCourseException;
import de.humance.education.model.Course;
import de.humance.education.service.CourseLocalServiceUtil;
import de.humance.education.util.CourseType;

public class WsUtil {
	private static final String URL_SERVICE_OAI = "/eco-ws/oai";
	private static final String URL_USER_PORTRAIT = "/image/user_portrait?img_id=";
	private static ISO8601DateFormat dateFormatter = new ISO8601DateFormat();
	private static Log log = LogFactoryUtil.getLog(WsUtil.class);

	public static ISO8601DateFormat getDateFormatter() {
		return dateFormatter;
	}

	public static String getPortalBaseUrl() {
		return PortalUtil.getPortalURL(null, PortalUtil.getPortalPort(false),
				false);
	}

	public static String getOaiServiceUrl() {
		return getPortalBaseUrl() + URL_SERVICE_OAI;
	}

	public static String getUserPortraitUrl(long portraitId) {
		return getPortalBaseUrl() + URL_USER_PORTRAIT + portraitId;
	}

	public static String getCourseUrl(long courseId) {
		return getPortalBaseUrl()
				+ LayoutConstants.URL_EDUCATION_COURSE_ASSIGNMENT
				+ StringPool.QUESTION + "courseId=" + courseId;
	}

	public static int getOaiTokenTimeToLive() throws SystemException {
		return PrefsPropsUtil.getInteger(OaiPropsKeys.TOKEN_TIME_TO_LIVE);
	}

	public static int getOaiTokenListSize() throws SystemException {
		return PrefsPropsUtil.getInteger(OaiPropsKeys.TOKEN_LIST_SIZE);
	}

	public static String getCourseCatalog(Course course) {
		String packageName = course.getClass().getPackage().getName();
		// Remove ".model.impl" from the package name
		packageName = packageName.substring(0,
				packageName.lastIndexOf(StringPool.PERIOD));
		packageName = packageName.substring(0,
				packageName.lastIndexOf(StringPool.PERIOD));
		return packageName;
	}

	public static String getCourseUniversalId(Course course) {
		return OaiConstants.COURSE_ID_PREFIX + StringPool.COLON
				+ getCourseCatalog(course) + StringPool.COLON
				+ course.getCourseId();
	}

	public static long getCourseInternalId(String universalId)
			throws NoSuchCourseException, NumberFormatException {
		if (!universalId.startsWith(OaiConstants.COURSE_ID_PREFIX)) {
			// CASE : Not a course from the system
			throw new NoSuchCourseException(
					"No Course found with the specified id: " + universalId);
		}

		int index = universalId.lastIndexOf(StringPool.COLON);
		String extractedInternalId = universalId.substring(index + 1,
				universalId.length());
		return Long.parseLong(extractedInternalId);
	}

	public static OaiDcMetadataElement courseToOaiDc(Course course) {
		DcElement metadata = new DcElement();
		metadata.addTitle(null, escapeHtml(course.getTitle()));
		metadata.addType(null, CourseType.valueOf(course.getType())
				.getCaption());
		metadata.addDescription(null, escapeHtml(course.getSummary()));
		metadata.addIdentifier(null, Long.toString(course.getCourseId()));
		metadata.addCreator(null, escapeHtml(course.getChangerName()));
		metadata.addDate(null, dateFormatter.format(course.getModifiedDate()));
		return new OaiDcMetadataElement(metadata);
	}

	public static LomMetadataElement courseToLom(Course course) {
		LomElement lom = new LomElement();
		// General
		int contentCount = -1;
		try {
			contentCount = CourseLocalServiceUtil.countContents(course
					.getCourseId());
		} catch (Exception e) {
			log.error(
					"Could not count Contents in Course with id "
							+ course.getCourseId(), e);
		}

		String courseBase64Image = null;
		if (course.getImageId() > 0) {
			try {
				// Setup Permission Checker
				setPermissionChecker();

				FileEntry courseImage = DLAppLocalServiceUtil
						.getFileEntry(course.getImageId());

				byte[] courseImagePayload = IOUtils.toByteArray(courseImage
						.getContentStream());

				courseBase64Image = new String(
						Base64.encodeBase64(courseImagePayload));

			} catch (Exception e) {
				log.error(
						"Could not get course image (FileEntry id="
								+ course.getImageId()
								+ ") or convert it to base64 for Course with id "
								+ course.getCourseId(), e);
			}
		}

		if (courseBase64Image == null) {
			log.info("No Base64 Image available for course with id "
					+ course.getCourseId());
		}

		lom.setGeneral(new GeneralElement(getCourseCatalog(course), course
				.getCourseId(), Locale.GERMAN.getLanguage(), escapeHtml(course
				.getTitle()), escapeHtml(course.getDescription()),
				contentCount, courseBase64Image));
		// Technical
		lom.setTechnical(new TechnicalElement(
				getCourseUrl(course.getCourseId())));
		// Lifecycle
		try {
			User author = UserLocalServiceUtil
					.getUser(course.getChangeUserId());
			Organization org = null;
			long[] orgIds = author.getOrganizationIds();
			if (ArrayUtil.isNotEmpty(orgIds)) {
				org = OrganizationLocalServiceUtil.getOrganization(orgIds[0]);
			}
			if (author != null) {
				LifeCycleElement lifecycle = new LifeCycleElement();
				lifecycle.addContribute(new ContributeElement("LOMv1.0",
						"author", author.getUserId(), escapeHtml(author
								.getFirstName()), escapeHtml(author
								.getLastName()), author.getEmailAddress(),
						(org != null ? escapeHtml(org.getName()) : null)));
				lifecycle.addContribute(new ContributeElement("LOMv1.0",
						"content provider", (org != null ? escapeHtml(org
								.getName()) : null)));
				lom.setLifeCycle(lifecycle);
			}
		} catch (Exception e) {
			log.warn("Could not retrieve information for LOM LifeCycle", e);
		}
		// Classification
		SourceElement source = new SourceElement();
		source.addString(new StringElement("en", "ECO Area of Interests"));
		TaxonElement taxon = new TaxonElement();
		taxon.setId("ECO:ES");
		taxon.setEntry(new EntryElement());
		taxon.getEntry().addString(
				new StringElement("en", "Educational Science"));
		ClassificationElement classification = new ClassificationElement();
		classification.setPurpose(new PurposeElement("LOMv1.0", "discipline"));
		classification.setTaxonPath(new TaxonPathElement(source, taxon));
		lom.setClassification(classification);
		return new LomMetadataElement(lom);
	}

	public static String escapeHtml(String source) {
		String result = source.replace("ä", "&auml;");
		result = result.replace("Ä", "&Auml;");
		result = result.replace("ü", "&uuml;");
		result = result.replace("Ü", "&Uuml;");
		result = result.replace("ö", "&ouml;");
		result = result.replace("Ö", "&Ouml;");
		result = result.replace("ß", "&szlig;");
		result = result.replace("«", "&laquo;");
		result = result.replace("»", "&raquo;");
		result = result.replace("„", "&#132;");
		result = result.replace("“", "&#147;");
		result = result.replace("”", "&#148;");
		result = result.replace("°", "&#176;");
		result = result.replace("€", "&euro;");
		return result;
	}

	/**
	 * Sets Liferay Permission Checker to be able to retrieve ContentStream of a
	 * FileEntry
	 */
	private static void setPermissionChecker() {
		Role adminRole = null;
		try {
			adminRole = RoleLocalServiceUtil.getRole(
					PortalUtil.getDefaultCompanyId(), "Administrator");
			List<User> adminUsers = UserLocalServiceUtil.getRoleUsers(adminRole
					.getRoleId());

			PrincipalThreadLocal.setName(adminUsers.get(0).getUserId());
			PermissionChecker permissionChecker = PermissionCheckerFactoryUtil
					.create(adminUsers.get(0));

			PermissionThreadLocal.setPermissionChecker(permissionChecker);
		} catch (Exception e) {
			log.error(
					"Could not set permission checker for retrieving FileEntries",
					e);
		}
	}
}
