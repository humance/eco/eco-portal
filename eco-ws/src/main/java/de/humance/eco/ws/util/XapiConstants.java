package de.humance.eco.ws.util;

public class XapiConstants {
	// Verbs
	public static final String XAPI_VERB_ACCESSED = "http://activitystrea.ms/schema/1.0/access";
	public static final String XAPI_VERB_COMPLETED = "http://adlnet.gov/expapi/verbs/completed";
	public static final String XAPI_VERB_ENROLLED = "http://adlnet.gov/expapi/verbs/registered";
	public static final String XAPI_VERB_RESPONDED = "http://adlnet.gov/expapi/verbs/responded";

	// Activities
	public static final String XAPI_ACTIVITY_ARTICLE = "http://activitystrea.ms/schema/1.0/article";
	public static final String XAPI_ACTIVITY_AUDIO = "http://activitystrea.ms/schema/1.0/audio";
	public static final String XAPI_ACTIVITY_COURSE = "http://adlnet.gov/expapi/activities/course";
	public static final String XAPI_ACTIVITY_LESSON = "http://adlnet.gov/expapi/activities/lesson";
	public static final String XAPI_ACTIVITY_MODULE = "http://adlnet.gov/expapi/activities/module";
	public static final String XAPI_ACTIVITY_VIDEO = "http://activitystrea.ms/schema/1.0/video";

	// JSON FIELDS
	public static final String XAPI_JSON_RESPONSE_RESULT = "result";
	public static final String XAPI_JSON_RESPONSE_PROXY = "proxyId";
	public static final String XAPI_JSON_RESPONSE_LOCKER = "learningLockerResult";
	
}
