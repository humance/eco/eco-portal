package de.humance.eco.ws.util;

import java.util.Date;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.BasicManagedEntity;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.http.HttpStatus;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

import de.humance.common.util.JsonUtil;
import de.humance.eco.util.UserUtil;
import de.humance.eco.ws.xapi.model.XapiAccount;
import de.humance.eco.ws.xapi.model.XapiActor;
import de.humance.eco.ws.xapi.model.XapiContext;
import de.humance.eco.ws.xapi.model.XapiContextActivities;
import de.humance.eco.ws.xapi.model.XapiDefinition;
import de.humance.eco.ws.xapi.model.XapiEntity;
import de.humance.eco.ws.xapi.model.XapiObject;
import de.humance.eco.ws.xapi.model.XapiVerb;
import de.humance.education.model.Content;
import de.humance.education.model.Course;
import de.humance.education.service.ContentLocalServiceUtil;
import de.humance.education.service.CourseLocalServiceUtil;
import de.humance.education.util.ContentType;

public class XapiUtil {
	private static final String REST_URL = "http://eco-xapi-production-server.appspot.com/data-proxy/xAPI/statement/origin/LogiAssist";
	private static final String REST_HEADER_CONTENT_TYPE = "ContentType";
	private static final String REST_HEADER_XAPI_VERSION = "X-Experience-API-Version";
	private static final String REST_HEADER_AUTHORIZATION = "Authorization";
	private static final String REST_HEADER_VALUE_CONTENT_TYPE = "application/json";
	private static final String REST_HEADER_VALUE_XAPI_VERSION = "1.0.0";
	private static final String REST_HEADER_VALUE_AUTHORIZATION = "Basic %s";
	private static final String REST_AUTH_USER_NAME = "9rMkQRCtgY0qFLC2Hj8hiSWLeYDx32bNboV9xAozbOI=";
	private static final String REST_AUTH_PASSWORD = "xfGbjYaduAJmN8vyL0X91UrmEgpWTblDV4mnhKIjs9Q=";
	private static Log log = LogFactoryUtil.getLog(XapiUtil.class);

	public static void sendUserUserAnsweredQuestion(long userId, long courseId,
			long contentId, String stepTitle, String stepDescription) {
		XapiVerb verb = new XapiVerb();
		verb.setId(XapiConstants.XAPI_VERB_RESPONDED);
		verb.setDisplay("Indicates the learner answered a question");

		XapiDefinition definition = new XapiDefinition();
		definition.setName(StringEscapeUtils.escapeJson(stepTitle));
		definition
				.setDescription(StringEscapeUtils.escapeJson(stepDescription));
		definition.setType(XapiConstants.XAPI_ACTIVITY_MODULE);

		sendXapiPostRequest(getUser(userId), getCourse(courseId), verb,
				definition, contentId);
	}

	public static void sendUserAccessedCourseContent(long userId,
			long courseId, long contentId) {
		if (userId <= 0 || courseId <= 0 || contentId <= 0) {
			log.warn("Invalid userId (" + userId + ") or courseId (" + courseId
					+ ") or contentId (" + contentId + ") received!");
			return;
		}
		sendUserAccessedCourseContent(getUser(userId), getCourse(courseId),
				getContent(contentId));
	}

	public static void sendUserAccessedCourseContent(User user, Course course,
			Content content) {
		XapiVerb verb = new XapiVerb();
		verb.setId(XapiConstants.XAPI_VERB_ACCESSED);
		verb.setDisplay("Indicates the learner accessed " + content.getType()
				+ " of a MOOC");

		XapiDefinition definition = getContentDefinition(content);

		sendXapiPostRequest(user, course, verb, definition,
				content.getContentId());
	}

	public static void sendUserCompletedCourseContent(long userId,
			long courseId, long contentId) {
		if (userId <= 0 || courseId <= 0 || contentId <= 0) {
			log.warn("Invalid userId (" + userId + ") or courseId (" + courseId
					+ ") or contentId (" + contentId + ") received!");
			return;
		}
		sendUserCompletedCourseContent(getUser(userId), getCourse(courseId),
				getContent(contentId));
	}

	public static void sendUserCompletedCourseContent(User user, Course course,
			Content content) {
		XapiVerb verb = new XapiVerb();
		verb.setId(XapiConstants.XAPI_VERB_COMPLETED);
		verb.setDisplay("Indicates the learner completed " + content.getType()
				+ " of a MOOC");

		XapiDefinition definition = getContentDefinition(content);

		sendXapiPostRequest(user, course, verb, definition,
				content.getContentId());
	}

	public static void sendUserOpenedCourse(long userId, long courseId) {
		if (userId <= 0 || courseId <= 0) {
			log.warn("Invalid userId (" + userId + ") or courseId (" + courseId
					+ ") received!");
			return;
		}

		sendUserOpenedCourse(getUser(userId), getCourse(courseId));
	}

	public static void sendUserOpenedCourse(User user, Course course) {
		XapiVerb verb = new XapiVerb();
		verb.setId(XapiConstants.XAPI_VERB_ACCESSED);
		verb.setDisplay("Indicates the learner accessed a mooc");

		XapiDefinition definition = getCourseDefinition(course);

		sendXapiPostRequest(user, course, verb, definition, 0);
	}

	public static void sendUserEnrolledInCourse(long userId, long courseId) {
		if (userId <= 0 || courseId <= 0) {
			log.warn("Invalid userId (" + userId + ") or courseId (" + courseId
					+ ") received!");
			return;
		}

		sendUserEnrolledInCourse(getUser(userId), getCourse(courseId));
	}

	public static void sendUserEnrolledInCourse(User user, Course course) {
		XapiVerb verb = new XapiVerb();
		verb.setId(XapiConstants.XAPI_VERB_ENROLLED);
		verb.setDisplay("Indicates the learner enrolls in a MOOC");

		XapiDefinition definition = getCourseDefinition(course);

		sendXapiPostRequest(user, course, verb, definition, 0);
	}

	private static User getUser(long userId) {
		User user = null;
		try {
			user = UserLocalServiceUtil.getUser(userId);
		} catch (Exception e) {
			log.error("Could not get User with id " + userId, e);
		}
		return user;
	}

	private static Course getCourse(long courseId) {
		try {
			Course course = CourseLocalServiceUtil.getCourse(courseId);
			
			if (course.getOrgId() != 0L) {
				log.info("Filtering out course " + courseId + " as it does not belong to 'Universal' organization");
			}
			
			return course;
		} catch (Exception e) {
			log.error("Could not get Course with id " + courseId, e);
			
			return null;
		}
	}

	private static Content getContent(long contentId) {
		Content content = null;
		try {
			content = ContentLocalServiceUtil.getContent(contentId);
		} catch (Exception e) {
			log.error("Could not get Content with id " + contentId, e);
		}
		return content;
	}
	
	private static String getContentActivityType(String type) {
		ContentType contentType = null;
		try {
			contentType = ContentType.valueOf(type);
		} catch (Exception e) {
			log.error("Unable to parse contect type '" + type + "'. Defaulting to TEXT", e);
			contentType = ContentType.TEXT;
		}
		
		switch (contentType) {
		case AUDIO:
			return XapiConstants.XAPI_ACTIVITY_AUDIO;
		case VIDEO:
			return XapiConstants.XAPI_ACTIVITY_VIDEO;
		case FREE_TEXT:
		case PDF:
		case QUIZZ:
		case SURVEY:
		case TEXT:
			return XapiConstants.XAPI_ACTIVITY_ARTICLE;
		default:
			break;
		
		}
		return null;
	}
	
	private static XapiDefinition getCourseDefinition(Course course) {
		final XapiDefinition definition = new XapiDefinition();
		definition.setName(StringEscapeUtils.escapeJson(course.getTitle()));
		definition.setDescription(StringEscapeUtils.escapeJson(course
				.getDescription()));
		definition.setType(XapiConstants.XAPI_ACTIVITY_COURSE);
		
		return definition;
	}
	
	private static XapiDefinition getContentDefinition(Content content) {
		final XapiDefinition definition = new XapiDefinition();
		definition.setName(StringEscapeUtils.escapeJson(content.getTitle()));
		definition.setDescription(StringEscapeUtils.escapeJson(content
				.getType()));
		definition.setType(getContentActivityType(content.getType()));
		
		return definition;
	}

	private static void sendXapiPostRequest(User user, Course course,
			XapiVerb verb, XapiDefinition definition, long contentId) {
		if (user == null || course == null) {
			log.warn("User or Course is null. Skipping sending XAPI statement");
		}

		final XapiAccount account = new XapiAccount();
		String openIdUserId = UserUtil.getOpenIdFromUser(user.getUserId());
		account.setName(openIdUserId);
		account.setHomePage("https://portal.ecolearning.eu?user="
				+ openIdUserId);

		final XapiActor actor = new XapiActor();
		actor.setObjectType("Agent");

		actor.setAccount(account);

		final XapiObject object = new XapiObject();
		object.setObjectType("Activity");
		object.setDefinition(definition);
		
		if (contentId > 0) {
			object.setId(WsUtil.getCourseUniversalId(course) + StringPool.COLON
					+ String.valueOf(contentId));
		} else {
			object.setId(WsUtil.getCourseUniversalId(course));
		}
		
		XapiContext context = null;
		
		if (contentId > 0) {
			final XapiObject parent = new XapiObject();
			parent.setObjectType("Activity");
			parent.setId(WsUtil.getCourseUniversalId(course));
			parent.setDefinition(getCourseDefinition(course));
			
			final XapiContextActivities contextActivities = new XapiContextActivities();
			contextActivities.setParent(new XapiObject[]{parent});
			
			context = new XapiContext();
			context.setContextActivities(contextActivities);
		}
		
		final XapiEntity entity = new XapiEntity();
		entity.setActor(actor);
		entity.setVerb(verb);
		entity.setObject(object);
		entity.setContext(context);

		entity.setTimestamp(WsUtil.getDateFormatter().format(new Date()));
		entity.setVersion(REST_HEADER_VALUE_XAPI_VERSION);

		StringEntity payload = null;
		try {
			payload = new StringEntity(JsonUtil.convertToJson(entity));
			payload.setContentType(REST_HEADER_VALUE_CONTENT_TYPE);
		} catch (Exception e) {
			log.error("Could not prepare JSON payload");
			return;
		}

		HttpPost httppost = new HttpPost(REST_URL);
		httppost.setHeader(REST_HEADER_CONTENT_TYPE,
				REST_HEADER_VALUE_CONTENT_TYPE);
		httppost.setHeader(REST_HEADER_XAPI_VERSION,
				REST_HEADER_VALUE_XAPI_VERSION);
		String basicAuthDecoded = REST_AUTH_USER_NAME + StringPool.COLON
				+ REST_AUTH_PASSWORD;
		httppost.setHeader(
				REST_HEADER_AUTHORIZATION,
				String.format(REST_HEADER_VALUE_AUTHORIZATION,
						Base64.encodeBase64String(basicAuthDecoded.getBytes())));
		httppost.setEntity(payload);

		HttpClient httpClient = new DefaultHttpClient();
		try {
			HttpResponse response = httpClient.execute(httppost);
			if (response.getStatusLine().getStatusCode() == HttpStatus.OK
					.value()) {

				HttpEntity httpEntity = response.getEntity();
				String responseContent = null;
				if (httpEntity instanceof StringEntity) {
					StringEntity stringEntity = (StringEntity) httpEntity;
					responseContent = IOUtils.toString(stringEntity
							.getContent());
				} else if (httpEntity instanceof BasicHttpEntity) {
					BasicHttpEntity basicEntity = (BasicHttpEntity) httpEntity;
					responseContent = IOUtils
							.toString(basicEntity.getContent());
				} else if (httpEntity instanceof BasicManagedEntity) {
					BasicManagedEntity basicManagedEntity = (BasicManagedEntity) httpEntity;
					responseContent = IOUtils.toString(basicManagedEntity
							.getContent());
				} else {
					throw new Exception("Unknown HttpEntity : "
							+ httpEntity.getClass().getName());
				}

				try {
					JSONObject jsonResponse = JSONFactoryUtil
							.createJSONObject(responseContent);
					StringBuilder sb = new StringBuilder();
					sb.append("XAPI post request send successfully: \n");
					sb.append(XapiConstants.XAPI_JSON_RESPONSE_RESULT);
					sb.append(": ");
					sb.append(jsonResponse
							.getString(XapiConstants.XAPI_JSON_RESPONSE_RESULT));
					sb.append(StringPool.SPACE);
					sb.append(XapiConstants.XAPI_JSON_RESPONSE_PROXY);
					sb.append(": ");
					sb.append(jsonResponse
							.getString(XapiConstants.XAPI_JSON_RESPONSE_PROXY));
					sb.append(StringPool.SPACE);
					sb.append(XapiConstants.XAPI_JSON_RESPONSE_LOCKER);
					sb.append(": ");
					sb.append(jsonResponse
							.getString(XapiConstants.XAPI_JSON_RESPONSE_LOCKER));
					log.info(sb.toString());
				} catch (Exception e) {
					log.error("XAPI response could not be parsed: "
							+ responseContent, e);
				}

			} else {
				throw new Exception("HTTP Post failed: status code="
						+ response.getStatusLine().getStatusCode()
						+ " / reason="
						+ response.getStatusLine().getReasonPhrase());
			}

		} catch (Exception e) {
			log.error(
					"Could not send XAPI statement for opening a course with parameteres for user with id "
							+ user.getUserId()
							+ " and course with id "
							+ course.getCourseId(), e);
		} finally {
			httpClient.getConnectionManager().shutdown();
		}
	}
}