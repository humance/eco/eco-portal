package de.humance.eco.ws.xapi.messaging;

import com.liferay.portal.kernel.audit.AuditMessage;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.portal.kernel.messaging.proxy.ProxyRequest;
import com.liferay.portal.kernel.scheduler.SchedulerEngine;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;

import de.humance.eco.ws.util.XapiUtil;
import de.humance.education.util.EducationAuditEventPublisher;

public class AuditEventMessageListener implements MessageListener {
	private static final String EVENT_USER_LOGGED_IN = "login";
	private static Log log = LogFactoryUtil
			.getLog(AuditEventMessageListener.class);

	@Override
	public void receive(Message message) throws MessageListenerException {
		if (!(message.getPayload() instanceof ProxyRequest)) {
			return;
		}

		ProxyRequest proxyRequest = (ProxyRequest) message.getPayload();
		Object[] args = proxyRequest.getArguments();
		if (args == null || args.length <= 0
				|| !(args[0] instanceof AuditMessage)) {
			return;
		}

		AuditMessage auditMsg = (AuditMessage) args[0];
		if (!accept(auditMsg)) {
			return;
		}

		String eventType = auditMsg.getEventType();
		JSONObject extras = auditMsg.getAdditionalInfo();
		long courseId = 0;
		long contentId = 0;
		switch (eventType) {
		case EVENT_USER_LOGGED_IN:
			log.info("User logged in audit event received");
			// TODO
			break;
		case EducationAuditEventPublisher.EVENT_COURSE_OPENED:
			log.info("Course opened audit event received");
			courseId = extras.getLong("courseId");
			XapiUtil.sendUserOpenedCourse(auditMsg.getUserId(), courseId);
			break;
		case EducationAuditEventPublisher.EVENT_COURSE_ASSIGNED:
			log.info("Course assigned audit event received");
			courseId = extras.getLong("courseId");
			XapiUtil.sendUserEnrolledInCourse(auditMsg.getUserId(), courseId);
			break;
		case EducationAuditEventPublisher.EVENT_CONTENT_OPENED:
			log.info("Course content opened audit event received");
			courseId = extras.getLong("courseId");
			contentId = extras.getLong("contentId");
			XapiUtil.sendUserAccessedCourseContent(auditMsg.getUserId(),
					courseId, contentId);
			break;
		case EducationAuditEventPublisher.EVENT_CONTENT_COMPLETED:
			log.info("Course content completed audit event received");
			courseId = extras.getLong("courseId");
			contentId = extras.getLong("contentId");
			XapiUtil.sendUserCompletedCourseContent(auditMsg.getUserId(),
					courseId, contentId);
			break;
		case EducationAuditEventPublisher.EVENT_QUESTION_ANSWERED:
			log.info("Question answered audit event received");
			courseId = extras.getLong("courseId");
			contentId = extras.getLong("contentId");
			String stepTitle = extras.getString("stepTitle");
			String stepDesc = extras.getString("stepDesc");
			XapiUtil.sendUserUserAnsweredQuestion(auditMsg.getUserId(),
					courseId, contentId, stepTitle, stepDesc);
			break;
		default:
			log.info("No Message handling implemented for messages of type "
					+ eventType);
			break;
		}
	}

	private boolean accept(AuditMessage event) {
		if (SchedulerEngine.class.getName().equals(event.getClassName())
				&& "AUDIT_ACTION".equals(event.getEventType())) {
			// TODO Support in core Liferay is added in LPS-29425
			// refactoring SchedulerEngineUtil and adding property for disabling
			// scheduler audits
			return GetterUtil.getBoolean(PropsUtil
					.get("audit.message.scheduler.job"));
		}
		return true;
	}

}
