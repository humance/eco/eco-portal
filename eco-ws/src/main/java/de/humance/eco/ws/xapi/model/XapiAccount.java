package de.humance.eco.ws.xapi.model;

public class XapiAccount {
	private String name = null;
	private String homePage = null;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHomePage() {
		return homePage;
	}
	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}
}
