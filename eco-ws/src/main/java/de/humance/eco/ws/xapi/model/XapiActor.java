package de.humance.eco.ws.xapi.model;

public class XapiActor {
	private String objectType = null;
	private XapiAccount account = null;
	
	public String getObjectType() {
		return objectType;
	}
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
	public XapiAccount getAccount() {
		return account;
	}
	public void setAccount(XapiAccount account) {
		this.account = account;
	}
}
