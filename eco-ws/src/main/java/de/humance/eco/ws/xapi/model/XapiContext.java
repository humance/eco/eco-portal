package de.humance.eco.ws.xapi.model;

public class XapiContext {
	private XapiContextActivities contextActivities;
	
	public XapiContextActivities getContextActivities() {
		return contextActivities;
	}
	
	public void setContextActivities(XapiContextActivities contextActivities) {
		this.contextActivities = contextActivities;
	}
}
