package de.humance.eco.ws.xapi.model;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include = Inclusion.NON_NULL)
public class XapiContextActivities {
	private XapiObject category[];
	private XapiObject grouping[];
	private XapiObject other[];
	private XapiObject parent[];

	public XapiObject[] getCategory() {
		return category;
	}

	public void setCategory(XapiObject category[]) {
		this.category = category;
	}

	public XapiObject[] getGrouping() {
		return grouping;
	}

	public void setGrouping(XapiObject[] grouping) {
		this.grouping = grouping;
	}

	public XapiObject[] getOther() {
		return other;
	}

	public void setOther(XapiObject other[]) {
		this.other = other;
	}

	public XapiObject[] getParent() {
		return parent;
	}

	public void setParent(XapiObject parent[]) {
		this.parent = parent;
	}

}
