package de.humance.eco.ws.xapi.model;

public class XapiDefinition {
	private XapiStringDe name = null;
	private XapiStringDe description = null;
	private String type = null;

	public XapiStringDe getName() {
		return name;
	}

	public void setName(String name) {
		this.name = new XapiStringDe(name);
	}

	public XapiStringDe getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = new XapiStringDe(description);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
