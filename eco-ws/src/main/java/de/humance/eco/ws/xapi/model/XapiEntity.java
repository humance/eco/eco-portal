package de.humance.eco.ws.xapi.model;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include = Inclusion.NON_NULL)
public class XapiEntity {
	private XapiActor actor = null;
	private XapiVerb verb = null;
	private XapiObject object = null;
	private XapiContext context = null;
	private String timestamp = null;
	private String version = null;

	public XapiActor getActor() {
		return actor;
	}

	public void setActor(XapiActor actor) {
		this.actor = actor;
	}

	public XapiVerb getVerb() {
		return verb;
	}

	public void setVerb(XapiVerb verb) {
		this.verb = verb;
	}

	public XapiObject getObject() {
		return object;
	}

	public void setObject(XapiObject object) {
		this.object = object;
	}
	
	public XapiContext getContext() {
		return context;
	}
	
	public void setContext(XapiContext context) {
		this.context = context;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
