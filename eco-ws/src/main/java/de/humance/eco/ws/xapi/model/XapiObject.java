package de.humance.eco.ws.xapi.model;

public class XapiObject {
	private String objectType = null;
	private String id = null;
	private XapiDefinition definition = null;
	
	public String getObjectType() {
		return objectType;
	}
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public XapiDefinition getDefinition() {
		return definition;
	}
	public void setDefinition(XapiDefinition definition) {
		this.definition = definition;
	}
	
	
}
