package de.humance.eco.ws.xapi.model;

import org.codehaus.jackson.annotate.JsonProperty;

public class XapiStringDe {
	private String value = null;

	public XapiStringDe() {
	}

	public XapiStringDe(String value) {
		this.value = value;
	}

	@JsonProperty("de-DE")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
