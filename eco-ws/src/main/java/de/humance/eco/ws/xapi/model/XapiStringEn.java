package de.humance.eco.ws.xapi.model;

import org.codehaus.jackson.annotate.JsonProperty;

public class XapiStringEn {
	private String value = null;

	public XapiStringEn() {
	}

	public XapiStringEn(String value) {
		this.value = value;
	}

	@JsonProperty("en-US")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
