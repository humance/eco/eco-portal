package de.humance.eco.ws.xapi.model;

public class XapiVerb {
	private String id = null;
	private XapiStringEn display = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public XapiStringEn getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = new XapiStringEn(display);
	}
}
